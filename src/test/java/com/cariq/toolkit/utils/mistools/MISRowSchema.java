package com.cariq.toolkit.utils.mistools;

import java.util.List;

import com.cariq.toolkit.utils.KeyValuePair;

/**
 * Row schema abstraction
 * 
 * @author hrishi
 *
 */
public class MISRowSchema extends MISRowColumnSchema {
	String style;
	String label;
	String evaluatorClass;
	List<KeyValuePair> evaluatorArguments;

	public MISRowSchema(String style, String label, String evaluatorClass, List<KeyValuePair> evaluatorArguments) {
		super(style, label, evaluatorClass, evaluatorArguments);
	}
}
