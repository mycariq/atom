package com.cariq.toolkit.utils;

import java.util.LinkedHashMap;
import java.util.Set;

import com.google.common.base.Strings;

/**
 * The Class GenericJSON.
 */
public class GenericJSON extends LinkedHashMap<String, Object> implements GenericJsonable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Creates the.
	 *
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 * @return the generic JSON
	 */
	public static GenericJSON create(String key, Object value) {
		GenericJSON json = new GenericJSON();
		return json.add(key, value);
	}

	/**
	 * Adds the.
	 *
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 * @return the generic JSON
	 */
	public GenericJSON add(String key, Object value) {
		put(key, value);
		return this;
	}

	/**
	 * Subtract.
	 *
	 * @param json
	 *            the json
	 */
	public void subtract(GenericJSON json) {
		if (null == json)
			return;
		
		subtract(json.keySet());
	}

	/**
	 * Subtract.
	 *
	 * @param hashSet
	 *            the hash set
	 */
	public void subtract(Set<String> keySet) {
		if (null == keySet)
			return;

		for (String key : keySet) {
			this.remove(key);
		}
	}

	@Override
	public String toString() {
		return Utils.getJSonString(this);
	}
	
	public static GenericJSON fromString(String jsonString) {
		return Utils.getJSonObject(jsonString, GenericJSON.class);
	}
	
	/**
	 * Checks if the key exists and has valid value
	 * @param key
	 * @return
	 */
	public boolean exists(String key) {
		Object val = get(key);		
		if (val == null)
			return false;
		return (! Strings.isNullOrEmpty(val.toString()));
	}
	
	/** 
	 * Build a GenericJSON with a simplistic multi parameter argument method
	 * @param keyValues
	 * @return
	 */
	public static GenericJSON build(Object ... keyValues) {
		if (keyValues == null || (keyValues.length % 2) != 0)
			Utils.handleException("Cannot build GenericJSON with odd number of parameters!");
			
		
		GenericJSON retval = new GenericJSON();
		for (int i = 0; i < keyValues.length; i+=2) {
			String key = Utils.downCast(String.class, keyValues[i]);
			Object value = keyValues[i+1];
			retval.put(key, value);	
		}

		return retval; //alternate key and value for quick buildup
	}

	/**
	 * Just for completion sake
	 */
	@Override
	public GenericJSON toGenericJSON() {
		return this;
	}

	@Override
	public void fromGenricJSON(GenericJSON json) {
		this.putAll(json);
	}
}

