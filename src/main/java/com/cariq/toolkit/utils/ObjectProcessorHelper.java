package com.cariq.toolkit.utils;


public class ObjectProcessorHelper {
	public static enum ObjectProcessorStage {
		IN_PREPARE, IN_PROCESS, IN_FINISH, PREPARED, PROCESSED, FINISHED
	}
	
	public static void InvalidModelException(String modelId) {
		Utils.handleException("Marked the model invalid: " + modelId);		
	}
}
