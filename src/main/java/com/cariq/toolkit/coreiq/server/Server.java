/**
 * 
 */
package com.cariq.toolkit.coreiq.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.cariq.toolkit.coreiq.CompletionCallback;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.Utils;
import com.google.common.base.Strings;

/**
 * Server is the class holding the current state of the server.
 * It periodically dumps stuff too
 * Auditor is the small scale version of the Server
 * @author hrishi
 *
 */
public class Server {
	/** The logger. */
	static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("Server");

	// static final
	static final String SERVER_LOAD_STATISTICS = "SERVER_LOAD_STATISTICS";

	public static final String AUDIT = "AUDIT";
	public static final String STATISTICS = "STATISTICS";
	public static final String ALL = "ALL";
	public static final String ANY = "ANY";
	
	// List of Audit Categories
	public static final String API_AUDIT = "API";
	public static final String WORKER_AUDIT = "WORKER";
	public static final String REST_CALL_AUDIT = "REST";
	public static final String SQL_AUDIT = "SQL";
	public static final String CRAWLER_AUDIT = "CRAWLER";

	
	// Switch board that controls the Audit capture
	OnOffSwitchBoard auditServiceSwitchBoard = new OnOffSwitchBoard(Arrays.asList(API_AUDIT, WORKER_AUDIT, REST_CALL_AUDIT, SQL_AUDIT, CRAWLER_AUDIT));
	
	Map<ServerActivity, ServerActivity> activityMap = new ConcurrentHashMap<ServerActivity, ServerActivity>();
	Map<String, ServerHistoricActivity> historicActivity = new ConcurrentHashMap<String, ServerHistoricActivity>();

	
	// The singleton server
	private static Server _server;
	
	private static Object instantiationLock = new Object();
	
	String host = "Unknown";
	Date startedOn = new Date();
	
	private Server() {
		String hostName = Utils.getHostName();
		if (!Strings.isNullOrEmpty(hostName))
			host = hostName;
		
		startedOn = new Date();
		logger.debug("Constructed Logger at :" + startedOn);
		
		// initialize with some default values
		initialize();
	}
	
	private void initialize() {
		auditServiceSwitchBoard.switchOn(WORKER_AUDIT);
		auditServiceSwitchBoard.switchOn(SQL_AUDIT);
		auditServiceSwitchBoard.switchOn(API_AUDIT);
		auditServiceSwitchBoard.switchOn(REST_CALL_AUDIT);
		auditServiceSwitchBoard.switchOn(CRAWLER_AUDIT);

		// rest of those can remain off for now - we'll programmatically switch on
	}

	public static Server getInstance() {
		if (_server == null) {
			// double check
			synchronized(instantiationLock) {
				if (_server == null)
					_server = new Server();
			}
		}
		
		return _server;
	}
	
	// Getters
	public String getHost() {
		return host;
	}

	public Date getStartedOn() {
		return startedOn;
	}

	public OnOffSwitchBoard getAuditSwitchBoard() {
		return auditServiceSwitchBoard;
	}
	
	public ServerActivity register(String category, String name) {
		return register(category, null, name, null);
	}
	
	public ServerActivity registerWithSubCategory(String category, String subCategory, String name) {
		return register(category, subCategory, name, null);
	}
	
	public ServerActivity registerWithUserName(String category, String userName, String name) {
		return register(category, null, name, userName);
	}

	public ServerActivity register(String category, String subCategory, String name, String user) {
		if (!auditServiceSwitchBoard.isOn(category))
			return null;
			
		ServerActivity activity = new ServerActivity(category, subCategory, name, user, new CompletionCallback<ServerActivity>() {
			@Override
			public void onComplete(ServerActivity activity) {
//				logger.debug("??? Removing from CALLBACK activity: " + activity);

				remove(activity);
				logger.debug("!!! Removed from CALLBACK activity: " + activity);

			}
		});
		
		logger.debug("Registering activity: " + activity);

		activityMap.put(activity, activity);
		return activity;
	}

	private void recordHistoricActivity(ServerActivity activity) {
		String category = activity.getCategory();
		ServerHistoricActivity historic = historicActivity.get(category);
		if (historic == null)
			historic = new ServerHistoricActivity(category);
		
		historicActivity.put(category, historic.addActivity(activity));
	}
	
	private void remove(ServerActivity activity) {
//		logger.debug("??? Removing activity: " + activity);

		if (activityMap.containsKey(activity))
			activityMap.remove(activity);
		
		recordHistoricActivity(activity);
		
//		logger.debug("??? Removed activity: " + activity);

	}
	
	public int getCount(String category) {
		Utils.checkNotNull(category, "Server.getCount: Category is Null");

		int retVal = 0;
		for (ServerActivity serverActivity : activityMap.keySet()) {
			if (category.equalsIgnoreCase(serverActivity.getCategory()))
				retVal++;
		}
		
		return retVal;
	}
	
	public int getCount(String category, String subCategory) {
		Utils.checkNotNull(category, "Server.getCount: Category is Null", subCategory, "Server.getCount: subCategory is Null");
		int retVal = 0;
		for (ServerActivity serverActivity : activityMap.keySet()) {
			if (category.equals(serverActivity.getCategory()) && subCategory.equalsIgnoreCase(serverActivity.getSubCategory()))
				retVal++;
		}
		
		return retVal;
	}
	
	int getActiveWorkItemCount() {
		return getSubset(WORKER_AUDIT).size();
	}
	
	int getActiveWorkItems(String workerType) {
		int count = 0;
		for (ServerActivity worker : getSubset(WORKER_AUDIT).keySet()) {
			if (worker.subCategory.equals(workerType))
				count++;
		}
		
		return count;
	}

	public ServerLoadStatistics getServerLoadData() {
		return new ServerLoadStatistics(getHost(), getStartedOn(), getSubset(Server.API_AUDIT),
				getSubset(Server.WORKER_AUDIT), getSubset(Server.SQL_AUDIT),
				getSubset(Server.CRAWLER_AUDIT), getSubset(Server.REST_CALL_AUDIT), getServerHistoricActivities());
	}
	
	public Map<ServerActivity, ServerActivity> getSubset(String category) {
		Map<ServerActivity, ServerActivity> retval = new HashMap<ServerActivity, ServerActivity>();
		
		for (ServerActivity serverActivity : activityMap.keySet()) {
			if (serverActivity.getCategory().equalsIgnoreCase(category))
				retval.put(serverActivity, serverActivity);
		}
		return retval;
	}

	public boolean switchOnOff(String switchName, Boolean onOrOff) {
		return auditServiceSwitchBoard.switchOnOrOff(switchName, onOrOff);
	}

	public Collection<ServerHistoricActivity> getServerHistoricActivities() {
		return historicActivity.values();
	}

	// Check if the request is for me - @TODO why static?
	public static void validateExecutionServer(ServerSpecificRequest request) {
		if (Server.ANY.equalsIgnoreCase(request.getServerName()))
			return;
		
		// Get fully qualified current server name
		String currentServer = Server.getInstance().getHost();
		
		// Check if the server name contains suggested name - full or partial
		if (!currentServer.contains(request.getServerName()))
			Utils.handleException("[Server : " + currentServer + "] Request is for different server: " + request.getServerName());		
	}
	
	public List<LoadSummary> getLoadSummary(String category) {
		Utils.checkNotNull(category, "Server.getCount: Category is Null");
		
		Map<LoadSummary, LoadSummary> summaryMap = new HashMap<LoadSummary, LoadSummary>();
		
		for (ServerActivity serverActivity : activityMap.keySet()) {
			if (!category.equals(serverActivity.getCategory()))
				continue;
			
//			Utils.debug_log(logger, "getLoadSummary: " + serverActivity);
			LoadSummary existing = summaryMap.get(new LoadSummary(serverActivity.getCategory(), serverActivity.getSubCategory(), 0));
				
			if (null != existing) {
				existing.incrementCount();
				// should not be needed to add to map again
			} else {
				LoadSummary summary = new LoadSummary(serverActivity.getCategory(), serverActivity.getSubCategory(), 1);
				summaryMap.put(summary, summary);
			}
		}
		
		return new ArrayList<LoadSummary>(summaryMap.values());
	}
	
	public LoadSummary getLoadSummary(String category, String subCategory) {
		Utils.checkNotNull(category, "Server.getCount: Category is Null", subCategory, "Server.getCount: subCategory is Null");

		LoadSummary retval = new LoadSummary(category, subCategory, 0);
		
		for (ServerActivity serverActivity : activityMap.keySet()) {
			if (category.equals(serverActivity.getCategory()) && subCategory.equalsIgnoreCase(serverActivity.getSubCategory()))
				retval.incrementCount();
		}
		
		return retval;
	}

	/**
	 * Dump ServerWorker Load
	 * @param logger
	 */
	public void logServerLoad(String type, CarIQLogger logger) {
		Utils.debug_log(logger, "======== Server Workers Load Dump Start =========");
		
		for (ServerActivity worker : getSubset(type).keySet()) {			
			Utils.debug_log(logger, worker.toString());
		}
		
		Utils.debug_log(logger, "======== Server Workers Load Dump End =========");	
	}
}
