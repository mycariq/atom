package com.cariq.toolkit.coreiq.tableUtil.application.mis;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import com.cariq.toolkit.coreiq.tableUtil.interfaces.CellValueAribiterIfc;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

/**
 * General purpose Helper to do MIS work
 * @author hrishi
 *
 */
public class MISHelper {
	// Construct Arbiter from EvaluatorClass String and argumentJSON String
	public static CellValueAribiterIfc constructArbiter(String evaluatorClass, String argumentJSON) {
		Utils.checkNotNullOrEmpty(evaluatorClass, "Evaluator Class cannot be null for CellValueArbiter");

		try {
			Class<?> indicatorClass = Class.forName(evaluatorClass);
			Constructor<?> cons = indicatorClass.getConstructor(List.class);

			return (CellValueAribiterIfc) cons.newInstance(Utils.getJSonObject(argumentJSON, List.class));

		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException | SecurityException
				| ClassNotFoundException e) {
			Utils.handleException(e);
		}

		return null;	
	}

	// Construct Arbiter from EvaluatorClass String and argumentJSON
	public static CellValueAribiterIfc constructArbiter(String evaluatorClass, List argumentJSON) {
		Utils.checkNotNullOrEmpty(evaluatorClass, "Evaluator Class cannot be null for CellValueArbiter");

		try {
			Class<?> indicatorClass = Class.forName(evaluatorClass);
			Constructor<?> cons = indicatorClass.getConstructor(List.class);

			return (CellValueAribiterIfc) cons.newInstance(argumentJSON);

		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException | SecurityException
				| ClassNotFoundException e) {
			Utils.handleException(e);
		}

		return null;
	}

}
