package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterWrapper;
import com.cariq.toolkit.coreiq.exporter.CarIQWrappedExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.ExportDataEnhancerTemplate;
import com.cariq.toolkit.coreiq.exporter.ExporterDataEnhancer;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.model.Timeline;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.ProxyObject;
import com.cariq.toolkit.model.Tag.Tag_Out;
import com.cariq.toolkit.model.Timeline.Timeline_Out;
import com.cariq.toolkit.service.TaggedProxyService;
import com.cariq.toolkit.service.TimelineService;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.utils.service.CarIQRestClientService;

@Configurable
public class OnboardingProcessExporter extends CarIQWrappedExporterTemplate {

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("OnboardingProcessExporter");
	
	private static final String debugStaticPolicy = "3001/147518460/00/B00";
	private static final String debugStaticPolicyState = "Not Verified";
	
	private static final String TheExporter = "Onboarding Report";
	
	private static final String TheExporterDescription = "This is Onboarding Report";
	
	/** The Constant attributeAliasMap. */
	private static final List<String> attributeAliasMap = new ArrayList<>();

	private static final String createdAfter = "CreatedAfter";
	

	private static final String QueryType_allCustomers = "All Users";
	private static final String QueryType_nonContactable = "Not contactable";
	private static final String QueryType_nonInterested = "Not interrested";
	private static final String QueryType_feedback = "Feedback";
	private static final String QueryType_notVerified = "Not Verified";
	private static final String QueryType_userCreated = "User Created";
	private static final String QueryType_deviceRequested = "Device Requested";
	private static final String QueryType_deviceShipped = "Device Shipped";
	private static final String QueryType_deviceOnBoarded = "Device OnBoarded";
	private static final String QueryType_carConnected = "Car Connected";
	private static final String QueryType_KeywordSearch = "KeywordSearch";
	private static final String QueryType_FieldAssistRequested = "FieldAssist Requested";
	private static final String QueryType_ByPolicyNumber = "ByPolicyNumber";
			
	
	
	private static final String TAG_NOT_INTERESTED = "NOT_INTERESTED";
	private static final String TAG_NOT_CONTACTABLE = "NOT_CONTACTABLE";
	private static final String TAG_FEEDBACK = "FEEDBACK";
	
	private static final String nonContactableQueryTypeDescription = "Customers not contactable";
	
	private static final String nonInterestedQueryTypeDescription = "Customers not interreested!";
	
	private static final String feedbackQueryTypeDescription = "Customers who has given feedback";
	
	private static final String wrappedExporterName = "StagingAndInsurancePolicyDetailsExporter";
	
	private static final String wrappedExporterDescription = "This is Onboarding Report";
	
	private static final String proxyType = "InsurancePolicy";
	
	private static final String NONE = "None";
	private static final String callFilter = "callFilter";
	private static final String KEY_priority = "priority";
	
	private static final String KEY_TIMELINE = "timeline";
	private static final String KEY_POLICY_NO = "policyNumber";
	private static final String KEY_DOMAIN_ID = "domainId";
	private static final String KEY_DOMAIN_NAME = "domainName";
	private static final String KEY_TAGS = "tags";
	private static final String KEY_NC_NI = "NC_NI";
	private static final String KEY_OWNER = "owner";
	private static final String KEY_OWNER_ALL = "All Users";
	
	private static final String COLUMN_TAG = "tag";
	private static final String COLUMN_TYPE = "type";
	
	private static final String PRIORITY_ALL = "All";
	private static final String PRIORITY_MEDIUM = "Medium";
	private static final String PRIORITY_HIGH = "High";
	private static final String PRIORITY_LOW = "Low";
	
	/********	TAT columns ***************************/
	private static final String KEY_Verified_TAT = "Verified_TAT";
	private static final String KEY_Device_Requested_TAT = "Device_Requested_TAT";
	private static final String KEY_Device_Shipped_TAT = "Device_Shipped_TAT";
	private static final String KEY_Car_Connected_TAT = "Car_Connected_TAT";
	
	/** The rest service. */
	@Autowired
	private CarIQRestClientService restService;
	
	@Autowired
	private TimelineService timelineService;
	
	@Autowired
	private TaggedProxyService taggedProxyService;

	public OnboardingProcessExporter() {
		super(TheExporter, TheExporterDescription);

		addExportAttributes(attributeAliasMap);
		
		addQueryType(QueryType_nonContactable, nonContactableQueryTypeDescription,
				Arrays.asList(new QueryDefinitionAttribute(createdAfter, createdAfter, Date.class)));
		
		addQueryType(QueryType_nonInterested, nonInterestedQueryTypeDescription,
				Arrays.asList(new QueryDefinitionAttribute(createdAfter, createdAfter, Date.class)));
		
		addQueryType(QueryType_feedback, feedbackQueryTypeDescription,
				Arrays.asList(new QueryDefinitionAttribute(createdAfter, createdAfter, Date.class)));
		

		// Support roles
		addSupportedRoles(
				Arrays.asList(Utils.ADMIN_ROLE, Utils.USER_ROLE, Utils.ROLE_FLEET_ADMIN, Utils.ROLE_GROUP_OPERATOR));

	}

	

	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {

		try (ProfilePoint _doExport_ = ProfilePoint
				.profileAction("ProfAction_doExport_" + this.getClass().getCanonicalName())) {
			// For all local query types, createdAfter query type will be used
			// for wrapper call.
			if (queryType.equalsIgnoreCase(QueryType_nonContactable)
					|| queryType.equalsIgnoreCase(QueryType_nonInterested)
					|| queryType.equalsIgnoreCase(QueryType_feedback)
					|| queryType.equalsIgnoreCase(QueryType_allCustomers)) {
				return postProcess(queryType, json, getWrapper().doExport(createdAfter, json, pageNo, pageSize));
			}

			return super.doExport(queryType, json, pageNo, pageSize);
		}
	}



	@Override
	protected CarIQExporterWrapper getWrapper() {
		CarIQExporterWrapper wrapper = new CarIQExporterWrapper(wrappedExporterName, wrappedExporterDescription);
		return wrapper;
	}


	@Override
	protected List<GenericJSON> postProcess(String queryType, GenericJSON inputJson, List<GenericJSON> outputJson) {

		try (ProfilePoint _doExport_ = ProfilePoint
				.profileAction("ProfAction_postProcess_" + this.getClass().getCanonicalName())) {

			List<GenericJSON> outputJSON = super.postProcess(queryType, inputJson, outputJson);
			List<GenericJSON> finalOutputJSON = new ArrayList<GenericJSON>();

			Integer mCallFilter = (Integer) inputJson.get(callFilter);
			String priorityFilter = (String) inputJson.get(KEY_priority);
			String ownerFilter = (String) inputJson.get(KEY_OWNER);

			// Retrive over output json, read policy number from json. If
			// timeline exists
			// for policy number then add timeline list into policy json and
			// return output
			for (GenericJSON json : outputJSON) {

				String policyNumber = (String) json.get(KEY_POLICY_NO);
				Long domainId = (Long) json.get(KEY_DOMAIN_ID);
				String domainName = (String) json.get(KEY_DOMAIN_NAME);
				
				if(debugStaticPolicy.equals(policyNumber)) {
					logger.debug("Found verified and not-contactable policy");
				}
				if(debugStaticPolicyState.equalsIgnoreCase((String) json.get("State"))) {
					logger.debug("Policy with " + debugStaticPolicyState);
				}
				
				ProxyObject proxy;
				try (ProfilePoint _proxy_getByDomain = ProfilePoint.profileAction("ProfAction_proxy_getByDomain")) {
					proxy = ProxyObject.getByDomainSignatureObjectType(Domain.findObjectById(domainId), policyNumber,
							proxyType);
				}
				String owner = getOwner(proxy);
				json.put(KEY_OWNER, getOwner(proxy));		
				
				/**
				 * input ownerFilter is = "preeti"
				 * If ownerFilter is not "preeti" then continue
				 * If owner is not "None" then continue.
				 * If ownerFilter is not "All Users"  then continue.
				 */
				if(!owner.equalsIgnoreCase(NONE)) {
					if(ownerFilter != null && !ownerFilter.equalsIgnoreCase(owner)) {
						if(!ownerFilter.equalsIgnoreCase(KEY_OWNER_ALL)) {
							continue;
						}
					}
				}
				
				List<Tag_Out> tags = taggedProxyService.getTagsByProxySignature(domainName, policyNumber, proxyType);
				
				List<Timeline_Out> timeline = timelineService.getTimeline(policyNumber, domainId, proxyType);
				// with proxy object not id
				json.put(KEY_TIMELINE, timeline);
				json.put(KEY_TAGS, tags);
				
				//calculate and append TAT data of this policy
				json = calculateAndAppendTATs(json);
				
				// If query type is nonContactable then filter result
				if (queryType.equalsIgnoreCase(QueryType_nonContactable)) {
//					logger.debug("Query type is : " + queryType + ", policyNumber : " + policyNumber + ", timeline : " + timeline);
					if (isContainGivenObject(timeline, TAG_NOT_CONTACTABLE, COLUMN_TAG)) {
						json.put(KEY_NC_NI, "NC");
						finalOutputJSON.add(json);
					}
				} else if (queryType.equalsIgnoreCase(QueryType_nonInterested)) {
					if (isContainGivenObject(timeline, TAG_NOT_INTERESTED, COLUMN_TAG)) {
						json.put(KEY_NC_NI, "NI");
						finalOutputJSON.add(json);
					}
				} else if( queryType.equalsIgnoreCase(QueryType_feedback)) {
					if(isContainGivenObject(timeline, TAG_FEEDBACK, COLUMN_TYPE)) {
						finalOutputJSON.add(json);
					}
				} else {
					// Apply callFilter and priority filter to this object
					PriorityDescription priority = isMatchingPriority(priorityFilter, queryType, json);
					if (canAddObjectWithFilter(json, mCallFilter) && (priority.isValid
							|| priorityFilter.equalsIgnoreCase(PRIORITY_ALL))) {
						//If this object is not-contactable or not-interested then don't add it in list
						//object will be displayed in seperate not-contactle/interested list.
						if(!(isContainGivenObject(timeline, TAG_NOT_INTERESTED, COLUMN_TAG)
							|| isContainGivenObject(timeline, TAG_NOT_CONTACTABLE, COLUMN_TAG)) 
								|| queryType.equalsIgnoreCase(QueryType_KeywordSearch)
								|| queryType.equalsIgnoreCase(QueryType_ByPolicyNumber)) {
							json.put(KEY_priority, priority.priority);
							finalOutputJSON.add(json);
						}
					}
				}
			}

			return finalOutputJSON;
		}
	}

	
	/**
	 * Get username from proxy object
	 * 
	 * @param proxy
	 */
	private String getOwner(ProxyObject proxy) {
		if(proxy != null)
			return proxy.getOwner() != null ? proxy.getOwner().getUsername() : NONE;
		return NONE;
	}



	/**
	 * Calculate and append TAT for given policy
	 * 
	 * @param json		policy to append TAT data.
	 * @return			improved policy json
	 */
	private GenericJSON calculateAndAppendTATs(GenericJSON json) {
		
		String entryDate = (String) json.get("StagingCreatedOn");
		
		json.put(KEY_Verified_TAT, StagingAndPolicyHelper.calculateTAT(entryDate, (String) json.get("Policy_CreationTime")));
		json.put(KEY_Device_Requested_TAT, StagingAndPolicyHelper.calculateTAT(entryDate, (String) StagingAndPolicyHelper.getDeviceRequestedOnDate(json)));
		json.put(KEY_Device_Shipped_TAT, StagingAndPolicyHelper.calculateTAT(entryDate,
				(String) StagingAndPolicyHelper.getDeviceShippedOnDate(json)));
		json.put(KEY_Car_Connected_TAT, StagingAndPolicyHelper.calculateTAT(entryDate, (String) json.get("Car_FirstSeenOn")));
		
		return json;
	}



	/**
	 * Find whether given priority is matching or not 
	 * 
	 * @param priorityFilter	priority input(Medium, High, Low)
	 * @param tags				json
	 * @return					isMatching priority
	 */
	private PriorityDescription isMatchingPriority(String priorityFilter, String queryType, GenericJSON json) {
		
		//on user details page this filter is not required, then return true
		if(priorityFilter == null) {
			return new PriorityDescription(true, PRIORITY_HIGH);
		}
		
		if(queryType.equalsIgnoreCase(QueryType_notVerified)) {
			return isMatchingPriority((String) json.get("StagingCreatedOn"), priorityFilter);
		} else if(queryType.equalsIgnoreCase(QueryType_userCreated)){
			return isMatchingPriority((String) json.get("Policy_CreationTime"), priorityFilter);
		} else if(queryType.equalsIgnoreCase(QueryType_deviceRequested)) {
			return isMatchingPriority((String) json.get("Shipped_Or_Delivered_On"), priorityFilter);
		} else if(queryType.equalsIgnoreCase(QueryType_FieldAssistRequested)) {
			return isMatchingPriority((String) json.get("Shipped_Or_Delivered_On"), priorityFilter);
		}else if(queryType.equalsIgnoreCase(QueryType_deviceShipped)) {
			return isMatchingPriority((String) json.get("Shipped_Or_Delivered_On"), priorityFilter);
		} else if(queryType.equalsIgnoreCase(QueryType_deviceOnBoarded)) {
			return isMatchingPriority((String) json.get("Device_OnboardedOn"), priorityFilter);
		} else if(queryType.equalsIgnoreCase(QueryType_carConnected)) {
			Double carTravelled = (Double) json.get("Car_Distance_Since_Connected");
			if(carTravelled == null)
				return new PriorityDescription(true, PRIORITY_LOW);
			if(carTravelled > 50 && carTravelled < 100) {
				return new PriorityDescription(true, PRIORITY_LOW);
			} else if(carTravelled > 100 && carTravelled < 200) {
				return new PriorityDescription(true, PRIORITY_MEDIUM);	
			} else if(carTravelled > 200) {
				return new PriorityDescription(true, PRIORITY_HIGH);	
			}
			return new PriorityDescription(false, null);
		}
		
		return new PriorityDescription(false, PRIORITY_HIGH);
	}
	
	/**
	 * Hold priority and Hold flag indicating this object should be
	 * added in final list or not.
	 */
	private static class PriorityDescription {
		boolean isValid;
		String priority;
		public PriorityDescription(boolean isValid, String priority) {
			this.isValid = isValid;
			this.priority = priority;
		}
	}
	
	/**
	 * Find whether priority matching based on date
	 * 
	 * @param dateStr
	 * @param priorityFilter
	 * @return
	 */
	private PriorityDescription isMatchingPriority(String dateStr, String priorityFilter) {
		Date date = Utils.getDate(dateStr);
		String priority = getPriority(date);
		if(priority.equalsIgnoreCase(priorityFilter))
			return new PriorityDescription(true, priority);
		return new PriorityDescription(false, priority);
	}

	
	/**
	 * Get priority for given date
	 * 
	 * @param date
	 * @return
	 */
	private String getPriority(Date date) {
		if(date == null) 
			return PRIORITY_HIGH;
		Date today = new Date();
		int diff = Utils.getDiffInDays(date, today);
		if(diff < 3) 
			return PRIORITY_LOW;
		if(diff > 3 && diff < 6)
			return PRIORITY_MEDIUM;
		return PRIORITY_HIGH;
	}
	

	@Override
	protected ExporterDataEnhancer getEnhancer() {
		return new OnboardingProcessEnhancer();
	}


	/**
	 * Find timeline contain given object
	 * 
	 * @param timeline
	 * @return
	 */
	private static boolean isContainGivenObject(List<Timeline_Out> timeline, String object, String objectType) {
				
		if(timeline == null || timeline.isEmpty()) 
			return false;
		
		//If timeline tag is equal to object then return true;
		for(Timeline_Out t : timeline) {
			logger.debug("Timeline type  : " + t.getType()  
			+  ", signature : " + t.getProxyObject().getSignature() + ",  object : " + object + ", objectType : " + objectType);
			if(t.getTag() != null && t.getTag().equalsIgnoreCase(object) && objectType.equals(COLUMN_TAG)) {
				return true;
			}				
			
			if(t.getType() != null && t.getType().equalsIgnoreCase(object) && objectType.equals(COLUMN_TYPE)) {
				return true;
			}
		}
		return false;
	}


	/**
	 * Find how many calls has been done againt given user, If filter is same 
	 * as 
	 * 
	 * @param json
	 * @param callFilter
	 * @return
	 */
	private static boolean canAddObjectWithFilter(GenericJSON json, Integer callCountInput) {
		
		@SuppressWarnings("unchecked")
		List<Timeline_Out> timeline = (List<Timeline_Out>) json.get(KEY_TIMELINE);
		
		int callCount = Timeline.getCallCountFromTimeline(timeline);
		
		//If input is not valid return true
		if(callCountInput == null)
			return true;
		
		//-1 call count means all calls
		if(callCountInput == -1)
			return true;
		
		if(callCount == callCountInput) {
			return true;
		}		
		return false;
	}
	
	private static class OnboardingProcessEnhancer extends ExportDataEnhancerTemplate {

		private static final String CAR_CONNECTED = "Car Connected";
		private static final String DEVICE_REQUESTED = "Device Shipped";
		private static final String DEVICE_REQUESTED_OR_SHIPPED_ON = "Shipped_Or_Delivered_On";
		private static final String CAR_DISTANCE_SINCE_CONNECTED = "Car_Distance_Since_Connected";
		
		private static final Double MIN_KM_DRIVEN_FOR_FEEDBACK = 50.0;

		@Override
		protected GenericJSON doEnhance(String queryType, GenericJSON inputJSON, GenericJSON exportJSON) {
			throw new UnsupportedOperationException("ExportDataEnhancerTemplate.doEnhance() is not implemented");
		}

		@Override
		protected List<GenericJSON> doEnhance(String queryType, GenericJSON inputJSON, GenericJSON previous,
				GenericJSON current) {
			throw new UnsupportedOperationException("ExportDataEnhancerTemplate.doEnhance() is not implemented");
		}

		@Override
		protected List<GenericJSON> doEnhance(String queryType, GenericJSON inputJSON, List<GenericJSON> exportJSON) {

			try(ProfilePoint OnboardingProcessEnhancer_doEnhance = ProfilePoint.profileAction("ProfAction_OnboardingProcessEnhancer_doEnhance")) {
				List<GenericJSON> outputValues = new ArrayList<GenericJSON>();
				
				if(queryType.equalsIgnoreCase(DEVICE_REQUESTED)) {
					outputValues.addAll(filterShipments(exportJSON));
				} else if(queryType.equalsIgnoreCase(CAR_CONNECTED)) {
					outputValues.addAll(filterFeedbacks(exportJSON));
				} else {
					return exportJSON;
				}
				return outputValues;
			}
		}

		/**
		 * Return those users who have connected car before 7 days
		 * 
		 * @param exportJSON
		 * @return
		 */
		private Collection<? extends GenericJSON> filterFeedbacks(List<GenericJSON> exportJSON) {
			List<GenericJSON> outputValues = new ArrayList<GenericJSON>();

			for(GenericJSON genericJSON : exportJSON) {
				Double kmDriven = (Double) genericJSON.get(CAR_DISTANCE_SINCE_CONNECTED);
				if(kmDriven == null)
					continue;
				if(kmDriven > MIN_KM_DRIVEN_FOR_FEEDBACK) {
					outputValues.add(genericJSON);
				}
			}
			
			return outputValues;
		}

		
		/**
		 * Return those users who have requested device before 7 days.
		 * 
		 * @param exportJSON
		 * @return
		 */
		private Collection<? extends GenericJSON> filterShipments(List<GenericJSON> exportJSON) {
			List<GenericJSON> outputValues = new ArrayList<GenericJSON>();
			for(GenericJSON genericJSON : exportJSON) {
				String deviceRequestedDateString = (String) genericJSON.get(DEVICE_REQUESTED_OR_SHIPPED_ON);
				Date deviceRequestedDate = Utils.parseDate(deviceRequestedDateString);

				if(deviceRequestedDate == null)
					continue;
				
				//If user has requested device 7 days back then add that user in list
				if(Utils.getDiffInDays(deviceRequestedDate, new Date()) > 7) {
					outputValues.add(genericJSON);
				}
			}

			return outputValues;
		}

		@Override
		protected List<GenericJSON> doEnhance(String queryType, GenericJSON inputJSON, GenericJSON previous,
				List<GenericJSON> exportJSON) {
			throw new UnsupportedOperationException("ExportDataEnhancerTemplate.doEnhance() is not implemented");
		}
		
	}
}
