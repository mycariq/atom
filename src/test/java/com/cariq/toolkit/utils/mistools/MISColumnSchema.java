package com.cariq.toolkit.utils.mistools;

import java.util.List;

import com.cariq.toolkit.utils.KeyValuePair;

/**
 * Column schema abstraction
 * 
 * @author hrishi
 *
 */
public class MISColumnSchema extends MISRowColumnSchema {
	String style;
	String label;
	String evaluatorClass;
	List<KeyValuePair> evaluatorArguments;

	public MISColumnSchema(String style, String label, String evaluatorClass, List<KeyValuePair> evaluatorArguments) {
		super(style, label, evaluatorClass, evaluatorArguments);
	}

}
