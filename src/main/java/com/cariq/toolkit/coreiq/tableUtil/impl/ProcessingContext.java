package com.cariq.toolkit.coreiq.tableUtil.impl;

import com.cariq.toolkit.utils.GenericJSON;

/**
 * Context in which the Report tree is processed. This is useful in getting the
 * default values based on current Report, Table, Row, Column, Cell being
 * processed
 * 
 * @author hrishi
 *
 */
public interface ProcessingContext {

	Report getCurrentReport();

	Table getCurrentTable();

	RowColumn getCurrentRow();

	RowColumn getCurrentColumn();

	Cell getCurrentCell();

	GenericJSON getAdditionalInfo();
}