package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import com.cariq.toolkit.coreiq.CarIQRawQuery;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.model.Appointment;
import com.cariq.toolkit.model.ProxyObject;
import com.cariq.toolkit.model.StageStatistics;
import com.cariq.toolkit.model.Timeline;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.Day;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.model.Domain;

/**
 * TrendsExporter
 * 
 * Gives trending numbers in Atom. Like, Connected cars, appointments, Tagged entities, etc.
 * 
 * @author sagar
 *
 */
public class TrendsExporter extends CarIQExporterTemplate {
	
	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("TrendsExporter");
	
	public static final String TheExporter = "TrendsExporter";
	private static final String ExporterDescription = "Gives trending numbers in Atom. Like, Connected cars, appointments, Tagged entities, etc.";
	
	static final LinkedHashMap<String, String> NewOrdersAliasMap = new LinkedHashMap<String, String>();
	static final LinkedHashMap<String, String> NewOrdersTableAliasMap = new LinkedHashMap<String, String>();
	
	static final LinkedHashMap<String, String> PreShipAliasMap = new LinkedHashMap<String, String>();
	static final LinkedHashMap<String, String> PreShipTableAliasMap = new LinkedHashMap<String, String>();
	
	static final LinkedHashMap<String, String> PostShipAliasMap = new LinkedHashMap<String, String>();
	static final LinkedHashMap<String, String> PostShipTableAliasMap = new LinkedHashMap<String, String>();
	
	static final LinkedHashMap<String, String> ConnectedAliasMap = new LinkedHashMap<String, String>();
	static final LinkedHashMap<String, String> ConnectedTableAliasMap = new LinkedHashMap<String, String>();
	
	static final LinkedHashMap<String, String> appointmentAliasMap = new LinkedHashMap<String, String>();
	static final List<String> appointmentTableAliasMap = new ArrayList<String>();
	
	static final LinkedHashMap<String, String> NIAliasMap = new LinkedHashMap<String, String>();
	static final LinkedHashMap<String, String> NITableAliasMap = new LinkedHashMap<String, String>();
	
	static final LinkedHashMap<String, String> NCAliasMap = new LinkedHashMap<String, String>();
	static final LinkedHashMap<String, String> NCTableAliasMap = new LinkedHashMap<String, String>();
	
	static final LinkedHashMap<String, String> ActionsAliasMap = new LinkedHashMap<String, String>();
	static final List<String> ActionsTableAliasMap = new ArrayList<String>();

	/** The Constant DEF_VALUE. */
	public final static long DEF_VALUE = 0;
	
	//Number of days to compute trend
	public final static int TRENDS_FOR_DAYS = 7;
	
	private static final String QueryType_ILAssist = Utils.DOMAIN_ILASSIST, 
			QueryType_Driven = Utils.DOMAIN_DRIVEN, QueryType_Drivesmart = Utils.DOMAIN_DRIVESMART,
			QueryType_Tagic = Utils.DOMAIN_TAGIC;
	
	private static final String QueryTypeDescription = "All trends information of domain ";
	
	public final static String DAY = "Day",	NEW_ORDERS="NewOrders", PRE_SHIP="Requested",
			POST_SHIP="Delivered", CONNECTED = "Connected", APPOINTMENTS="Appointments",
			NC="NC", NI="NI",ACTIONS="Actions";
	
	public final static String DOMAIN = "domain";
	
	static {
		
		//New Order alias map
		NewOrdersAliasMap.put("DATE_FORMAT(stats.entry_date, '%Y-%m-%d')", Utils.DATE);
		NewOrdersAliasMap.put("COUNT(1)", NEW_ORDERS);

		//Post ship alias.(we are not storing user created date in stats, need to store it.
		//Till then, we will compute preshipment based on device_requested_date
		PreShipAliasMap.put("DATE_FORMAT(stats.device_requested_date, '%Y-%m-%d')", Utils.DATE);
		PreShipAliasMap.put("COUNT(1)", PRE_SHIP);
		
		PostShipAliasMap.put("DATE_FORMAT(stats.device_delivered_on_date, '%Y-%m-%d')", Utils.DATE);
		PostShipAliasMap.put("COUNT(1)", POST_SHIP);
		
		//Car connected alias
		ConnectedAliasMap.put("DATE_FORMAT(stats.car_connected_date, '%Y-%m-%d')", Utils.DATE);
		ConnectedAliasMap.put("COUNT(1)", CONNECTED);
		
		//Appointments alias map.
		appointmentAliasMap.put("DATE_FORMAT(appointment.created_on, '%Y-%m-%d')", Utils.DATE);
		appointmentAliasMap.put("COUNT(1)", APPOINTMENTS);
		
		//NI proxies
		NIAliasMap.put("DATE_FORMAT(proxy.modified_on, '%Y-%m-%d')", Utils.DATE);
		NIAliasMap.put("COUNT(1)", NI);
		
		//NC proxies
		NCAliasMap.put("DATE_FORMAT(proxy.modified_on, '%Y-%m-%d')", Utils.DATE);
		NCAliasMap.put("COUNT(1)", NC);

		//Timeline actions
		ActionsAliasMap.put("DATE_FORMAT(timeline.created_on, '%Y-%m-%d')", Utils.DATE);
		ActionsAliasMap.put("COUNT(1)", ACTIONS);
		
		
		//Add table aliases
		NewOrdersTableAliasMap.put("stage_statistics", "stats");
		PreShipTableAliasMap.put("stage_statistics", "stats");
		PostShipTableAliasMap.put("stage_statistics", "stats");
		ConnectedTableAliasMap.put("stage_statistics", "stats");
		
		appointmentTableAliasMap.add("appointment appointment");
		appointmentTableAliasMap.add("left join proxy_object proxy on proxy.id = appointment.proxy");
		
		NITableAliasMap.put("proxy_object", "proxy");
		NCTableAliasMap.put("proxy_object", "proxy");
		
		ActionsTableAliasMap.add("timeline timeline");
		ActionsTableAliasMap.add("left join proxy_object proxy on proxy.id = timeline.proxy");
	}
	
	/**
	 * Default constructor
	 */
	public TrendsExporter() {
		super(TheExporter, ExporterDescription);
		
		//ILAssist query type
		addQueryType(QueryType_ILAssist, QueryTypeDescription + Utils.DOMAIN_ILASSIST,
				Arrays.asList(new QueryDefinitionAttribute(DOMAIN, "Name of domain.", String.class)));
		
		//Drivesmart query type
		addQueryType(QueryType_Drivesmart, QueryTypeDescription + Utils.DOMAIN_DRIVESMART,
				Arrays.asList(new QueryDefinitionAttribute(DOMAIN, "Name of domain.", String.class)));
		
		//Driven query type
		addQueryType(QueryType_Driven, QueryTypeDescription + Utils.DOMAIN_DRIVEN,
				Arrays.asList(new QueryDefinitionAttribute(DOMAIN, "Name of domain.", String.class)));
		
		//Tagic query type
		addQueryType(QueryType_Tagic, QueryTypeDescription + Utils.DOMAIN_TAGIC,
				Arrays.asList(new QueryDefinitionAttribute(DOMAIN, "Name of domain.", String.class)));
	}

	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		
		//This exporter response is 1 page only
		if (pageNo > 1)
			return null;
		
		try (ProfilePoint _doExport_ = ProfilePoint
				.profileAction("ProfAction_TrendsExporter_doExport_" + this.getClass().getCanonicalName())) {
			// create list of output json and return
			List<GenericJSON> retval = new ArrayList<GenericJSON>();
			
			Domain domain = Domain.getByName(queryType);
			if(domain == null)
				Utils.handleException("Given domain " + queryType + " does not exists!");

			// Start computing trends from today
			Day day = new Day(new Date(), null);
			int dayDiffFromDay = 1;

			/**
			 * iterate from today till {@link TrendsExporter#TRENDS_FOR_DAYS}
			 */
			while (dayDiffFromDay <= TRENDS_FOR_DAYS) {
				try (ProfilePoint _forDay_ = ProfilePoint
						.profileAction("ProfAction_TrendsExporter_forDay_" + this.getClass().getCanonicalName())) {
					// find stats for day
					retval.add(getTrendDataForDay(day, domain));
					day = day.getPreviousDay();

				} finally {
					// to prevent infinite loop
					dayDiffFromDay++;
				}
			}
			//Reverse values
			Collections.reverse(retval);
			return retval;
		}
	}

	/**
	 * Return trends data for given day.
	 * @param domain2 
	 * 
	 * @param day2
	 * @return
	 */
	private GenericJSON getTrendDataForDay(Day day, Domain domain) {
		logger.debug("Get trends for day: " + day.toString() + ", for domain " + domain.getName());
		Date date = day.getStart();
		
		//Get new orders count
		GenericJSON newOrderCountJson = getNewOrdersCountForDate(date, domain);
		
		//Get preshipment count
		GenericJSON preShipCountJson = getPreShipCountForDate(date, domain);
		
		//Get post shipment count
		GenericJSON postShipCountJson = getPostShipCountForDate(date, domain);
		
		//Get connected cars count
		GenericJSON connectedCountJson = getConnectedCountForDate(date, domain);
		
		//Get appointment count
		GenericJSON appointmentCountJson = getAppointmentCountForDate(date, domain);
		
		//Get NI count
		GenericJSON niCountJson = getNICountForDate(date, domain);
		
		//Get NC Count
		GenericJSON ncCountJson = getNCCountForDate(date, domain);
		
		//Get timelines actions count
		GenericJSON actionCountJson = getActionCountForDate(date, domain);
		
		// add all data into json and return json
		GenericJSON outJSON = new GenericJSON();
		outJSON.put(DAY, Utils.convertToyyyy_mm_dd_Format(date));
		outJSON.put(NEW_ORDERS, newOrderCountJson != null ? newOrderCountJson.get(NEW_ORDERS) : DEF_VALUE);
		outJSON.put(PRE_SHIP, preShipCountJson != null ? preShipCountJson.get(PRE_SHIP) : DEF_VALUE);
		outJSON.put(POST_SHIP, postShipCountJson != null ? postShipCountJson.get(POST_SHIP) : DEF_VALUE);
		outJSON.put(CONNECTED, connectedCountJson != null ? connectedCountJson.get(CONNECTED) : DEF_VALUE);
		outJSON.put(APPOINTMENTS, appointmentCountJson != null ? appointmentCountJson.get(APPOINTMENTS) : DEF_VALUE);
		outJSON.put(NI, niCountJson != null ? niCountJson.get(NI) : DEF_VALUE);
		outJSON.put(NC, ncCountJson != null ? ncCountJson.get(NC) : DEF_VALUE);
		outJSON.put(ACTIONS, actionCountJson != null ? actionCountJson.get(ACTIONS) : DEF_VALUE);
		
		return outJSON;
	}

	/**
	 * Get Actions count for given date
	 * 
	 * @param date
	 * @param domain2 
	 * @return
	 */
	private GenericJSON getActionCountForDate(Date date, Domain domain) {
		CarIQRawQuery ncCountQry = new CarIQRawQuery("getActionCountForDateQuery", Timeline.entityManager(),
				ActionsAliasMap, ActionsTableAliasMap);
		ncCountQry.addRawCondition("date(timeline.created_on) = " + Utils.getSQLDateOnly(date));
		ncCountQry.addRawCondition("proxy.domain = " + domain.getId());
		return ncCountQry.getSingleResult();
	}

	/**
	 * Get NC count for given date
	 * @param date
	 * @param domain2 
	 * @return
	 */
	private GenericJSON getNCCountForDate(Date date, Domain domain) {
		CarIQRawQuery ncCountQry = new CarIQRawQuery("getNCCountForDateQuery", ProxyObject.entityManager(),
				NCAliasMap, NCTableAliasMap);
		ncCountQry.addRawCondition("date(proxy.modified_on) = " + Utils.getSQLDateOnly(date));
		ncCountQry.addRawCondition("proxy.latest_desposition = 'NOT_CONTACTABLE'");
		ncCountQry.addRawCondition("proxy.domain = " + domain.getId());
		return ncCountQry.getSingleResult();
	}

	/**
	 * Get NI cases count for given date
	 * 
	 * @param date
	 * @param domain2 
	 * @return
	 */
	private GenericJSON getNICountForDate(Date date, Domain domain) {
		CarIQRawQuery niCountQry = new CarIQRawQuery("getNICountForDateQuery", ProxyObject.entityManager(),
				NIAliasMap, NITableAliasMap);
		niCountQry.addRawCondition("date(proxy.modified_on) = " + Utils.getSQLDateOnly(date));
		niCountQry.addRawCondition("proxy.latest_desposition = 'NOT_INTERESTED'");
		niCountQry.addRawCondition("proxy.domain = " + domain.getId());
		return niCountQry.getSingleResult();
	}

	/**
	 * Get appointments count for given date
	 * @param date
	 * @param domain2 
	 * @return
	 */
	private GenericJSON getAppointmentCountForDate(Date date, Domain domain) {
		CarIQRawQuery appointmentsQry = new CarIQRawQuery("getAppointmentCountForDateQuery", Appointment.entityManager(),
				appointmentAliasMap, appointmentTableAliasMap);
		appointmentsQry.addRawCondition("date(appointment.modified_on) = " + Utils.getSQLDateOnly(date)
						+ " AND appointment.status <> 'COMPLETE'");
		appointmentsQry.addRawCondition("proxy.domain = " + domain.getId());
		return appointmentsQry.getSingleResult();
	}

	/**
	 * Get cars connected count for given date
	 * 
	 * @param date
	 * @param domain2 
	 * @return
	 */
	private GenericJSON getConnectedCountForDate(Date date, Domain domain) {
		CarIQRawQuery getConnectedCountForDateQuery = new CarIQRawQuery("getConnectedCountForDateQuery", StageStatistics.entityManager(),
				ConnectedAliasMap, ConnectedTableAliasMap);
		getConnectedCountForDateQuery.addRawCondition("date(stats.car_connected_date) = " + Utils.getSQLDateOnly(date));
		getConnectedCountForDateQuery.addRawCondition("stats.domain = " + domain.getId());
		return getConnectedCountForDateQuery.getSingleResult();
	}

	/**
	 * Get count of cases in postship stage for given date
	 * 
	 * @param date
	 * @param domain2 
	 * @return
	 */
	private GenericJSON getPostShipCountForDate(Date date, Domain domain) {
		CarIQRawQuery getPostShipCountForDateQuery = new CarIQRawQuery("getPostShipCountForDateQuery", StageStatistics.entityManager(),
				PostShipAliasMap, PostShipTableAliasMap);
		getPostShipCountForDateQuery.addRawCondition("date(stats.device_delivered_on_date) = " + Utils.getSQLDateOnly(date));
		getPostShipCountForDateQuery.addRawCondition("stats.domain = " + domain.getId());
		return getPostShipCountForDateQuery.getSingleResult();
	}

	/**
	 * Get count of cases in preship stage for given date
	 * 
	 * @param date
	 * @param domain2 
	 * @return
	 */
	private GenericJSON getPreShipCountForDate(Date date, Domain domain) {
		CarIQRawQuery getPreShipCountForDateQuery = new CarIQRawQuery("getPreShipCountForDateQuery", StageStatistics.entityManager(),
				PreShipAliasMap, PreShipTableAliasMap);
		getPreShipCountForDateQuery.addRawCondition("date(stats.device_requested_date) = " + Utils.getSQLDateOnly(date));
		getPreShipCountForDateQuery.addRawCondition("stats.domain = " + domain.getId());
		return getPreShipCountForDateQuery.getSingleResult();
	}

	/**
	 * Get count of new orders for given date
	 * 
	 * @param date
	 * @param domain2 
	 * @return
	 */
	private GenericJSON getNewOrdersCountForDate(Date date, Domain domain) {
		CarIQRawQuery getNewOrdersCountForDateQuery = new CarIQRawQuery("getNewOrdersCountForDateQuery", StageStatistics.entityManager(),
				NewOrdersAliasMap, NewOrdersTableAliasMap);
		getNewOrdersCountForDateQuery.addRawCondition("date(stats.entry_date) = " + Utils.getSQLDateOnly(date));
		getNewOrdersCountForDateQuery.addRawCondition("stats.domain = " + domain.getId());
		return getNewOrdersCountForDateQuery.getSingleResult();
	}
}
