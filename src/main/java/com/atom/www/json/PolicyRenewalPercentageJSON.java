package com.atom.www.json;

/**
 * The Class PolicyRenewalPercentageJSON.
 */
public class PolicyRenewalPercentageJSON {

	/** The policy renewal percentage. */
	private double policyRenewalPercentage;

	/**
	 * Instantiates a new policy renewal percentage JSON.
	 */
	public PolicyRenewalPercentageJSON() {
	}

	/**
	 * Instantiates a new policy renewal percentage JSON.
	 *
	 * @param policyRenewalPercentage the policy renewal percentage
	 */
	public PolicyRenewalPercentageJSON(double policyRenewalPercentage) {
		this.policyRenewalPercentage = policyRenewalPercentage;
	}

	/**
	 * Gets the policy renewal percentage.
	 *
	 * @return the policy renewal percentage
	 */
	public double getPolicyRenewalPercentage() {
		return policyRenewalPercentage;
	}

	/**
	 * Sets the policy renewal percentage.
	 *
	 * @param policyRenewalPercentage the new policy renewal percentage
	 */
	public void setPolicyRenewalPercentage(double policyRenewalPercentage) {
		this.policyRenewalPercentage = policyRenewalPercentage;
	}
}