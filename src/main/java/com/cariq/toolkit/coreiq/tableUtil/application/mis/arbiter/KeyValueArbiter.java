package com.cariq.toolkit.coreiq.tableUtil.application.mis.arbiter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

public class KeyValueArbiter extends MISCellValueArbiterBase {
	String key;
	List<String> values = new ArrayList<String>();

	public KeyValueArbiter(List list) {
		super(list);
		
		// take the first item of the list and map it to key value
		List<GenericJSON> jsonList = Utils.getJSonObjectList(list.toString(), GenericJSON.class);
		if (Utils.isNullOrEmpty(jsonList))
			Utils.handleException("Argument not passed to KeyValueArbiter in correct format!");
		
		GenericJSON first = jsonList.get(0);
		initialize(first);
	}

	public KeyValueArbiter(GenericJSON json) {
		super(Arrays.asList(json));
		
		initialize(json);
	}
	
	private void initialize(GenericJSON json) {	
		key = Utils.getValue(String.class, json, "key");
		if (null != Utils.getValue(List.class, json, "value")) {
			values = Utils.getValue(List.class, json, "value");
		} else if (null != Utils.getValue(String.class, json, "value")) {
			values.add(Utils.getValue(String.class, json, "value"));
		} else
			Utils.handleException("Can not handle value in the JSON: " + json.get("value"));
	}

	@Override
	public boolean isApplicable(GenericJSON input) {
		if (!input.containsKey(key))
			return false;
		
		String val = Utils.getValue(String.class, input, key);
		return values.contains(val);
	}
}
