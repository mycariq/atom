package com.cariq.toolkit.utils.wwwservice.core;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.apache.commons.lang.time.DateUtils;

import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * @author HVN
 *
 */
public class JDBCQuery implements QueryInterface {
	/**
	 * Select Map to get Objects based on index
	 * 
	 * @author HVN
	 *
	 */

	public static class SelectParams {
		List<String> paramlist = new ArrayList<String>();
		Map<String, Integer> paramIndexes = new HashMap<String, Integer>();

		public SelectParams(String[] params) {
			add(params);
		}

		public SelectParams() {
		}

		public String getSelectString() {
			return makeCommaSeparatedString(paramlist);
		}

		public int getIndex(String key) {
			return paramIndexes.get(key);
		}

		/**
		 * Add the parameter to Select List
		 * 
		 * @param param
		 * @return
		 */
		public SelectParams add(String param) {
			if (paramIndexes.containsKey(param))
				return this;

			int indx = paramlist.size();
			paramlist.add(param);
			paramIndexes.put(param, indx);

			return this;
		}

		public SelectParams add(Set<String> params) {
			for (String param : params) {
				add(param);
			}
			return this;
		}

		public SelectParams add(String[] params) {
			for (String param : params) {
				add(param);
			}
			return this;
		}

	}

	/**
	 * Capture Result into an object for easy lookup
	 * 
	 * @author HVN
	 *
	 */
	public static class Results {
		Object[] objects;
		SelectParams params;

		public Results(Object[] objects, SelectParams params) {
			super();
			this.objects = objects;
			this.params = params;
		}

		public String getString(String param) {
			try {
				return objects[params.getIndex(param)].toString();
			} catch (Exception e) {
				return null;
			}
		}

		public Long getLong(String param) {
			try {
				return Long.parseLong(objects[params.getIndex(param)]
						.toString());
			} catch (Exception e) {
				return 0L;
			}
		}

		public Double getDouble(String param) {
			try {
				return Double.parseDouble(objects[params.getIndex(param)]
						.toString());
			} catch (Exception e) {
				return 0.0;
			}
		}

		public Object getObject(String param) {
			try {
				return objects[params.getIndex(param)];
			} catch (Exception e) {
				return null;
			}
		}

		public static Date parseDateFormat(String dateStr) {
			Date date = null;
			try {
				String[] datePatterns = new String[] { "yyyy-MM-dd HH:mm:ss.S" };
				date = DateUtils.parseDate(dateStr, datePatterns);
			} catch (ParseException e) {
				Utils
						.handleException("Date must be in (YYYY-MM-DD hh:mm:ss.S) format");
			}
			return date;
		}

		public Date getDate(String param) {
			try {
				return parseDateFormat(objects[params.getIndex(param)]
						.toString());
			} catch (Exception e) {
				return null;
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.hvn.wwwservice.core.QueryInterface#execute(java.util.List,
	 * com.hvn.wwwservice.core.Region, com.hvn.wwwservice.core.TimeRange,
	 * java.util.Set)
	 */
	@Override
	public QueryResult execute(EntityManager em, String table, List<Identity> ids, Region region,
			TimeRange timeRange, QueryRangeRestriction range, Set<String> params) {
		SelectParams selectParameters = new SelectParams();

		// Add Tuple and then select params
		// <HVN> Add id by default - comes handy
		selectParameters.add(
				new String[] { "id", "its_car", "its_time_stamp", "latitude",
						"longitude" }).add(params);

		// Populate queries for the Tuple of Datapoint
		String idQuery = populateIDQuery("its_car", ids);
		String regionQuery = populateRegionQuery("latitude", "longitude",
				region);
		String timeQuery = populateTimeQuery("its_time_stamp", timeRange);

		String rowIdQuery = populateRowIdQuery("id", range);
		// Build the Query
		StringBuilder query = new StringBuilder("SELECT "
				+ selectParameters.getSelectString()).append(" FROM " + table);

		if (idQuery != null || timeQuery != null || regionQuery != null
				|| rowIdQuery != null)
			query.append(" WHERE ");

		boolean addAnd = false;
		if (idQuery != null) {
			if (addAnd)
				query.append(" AND ");

			query.append(idQuery);
			addAnd = true;
		}

		if (regionQuery != null) {
			if (addAnd)
				query.append(" AND ");

			query.append(regionQuery);
			addAnd = true;
		}

		if (timeQuery != null) {
			if (addAnd)
				query.append(" AND ");

			query.append(timeQuery);
			addAnd = true;
		}

		if (rowIdQuery != null) {
			if (addAnd)
				query.append(" AND ");

			query.append(rowIdQuery);
			addAnd = true;
		}

		return executeQuery(em, query.toString(), selectParameters, params, range);
	}


	private static QueryResult executeQuery(EntityManager em, String sql,
			SelectParams selectParams, Set<String> params,
			QueryRangeRestriction range) {
		try (ProfilePoint _JDBCQuery_execute = ProfilePoint
				.profileAction("ProfAction_JDBCQuery_execute")) {

			
			Query query = em.createNativeQuery(sql);
			
			@SuppressWarnings("unchecked")
			List<Object[]> res = query.getResultList();

			return new JDBCQueryResult(res, selectParams, params, range);
		}
	}


	private String populateTimeQuery(String columnName, TimeRange range) {
		if (range == null)
			return null;

		String startDate = range.getStart().toString();
		String endDate = range.getEnd().toString();
		return columnName + " >= '" + startDate + "' AND " + columnName
				+ " <= '" + endDate + "'";
	}

	/**
	 * Based on Primary Key Id
	 * 
	 * @param columnName
	 * @param range
	 * @return
	 */
	private String populateRowIdQuery(String columnName,
			QueryRangeRestriction range) {
		// return early
		if (range == null)
			return null;

		// return early
		if (range.getMinRowId() == QueryRangeRestriction.UNKNOWN_ROW_ID
				&& range.getMaxRowId() == QueryRangeRestriction.UNKNOWN_ROW_ID)
			return null;

		long startId = range.getMinRowId();
		long endId = range.getMaxRowId();

		StringBuilder str = new StringBuilder();
		if (startId != QueryRangeRestriction.UNKNOWN_ROW_ID)
			str.append(columnName + " >= " + startId);

		if (endId != QueryRangeRestriction.UNKNOWN_ROW_ID) {
			if (str.length() > 0)
				str.append(" AND ");

			str.append(columnName + " <= " + endId);
		}

		return str.toString();
	}

	private String populateRegionQuery(String latColumn, String longColumn,
			Region region) {
		if (region == null)
			return null;

		String lowerLeftLat = String.valueOf(region.getLowerLeft()
				.getLatitude());
		String lowerLeftLong = String.valueOf(region.getLowerLeft()
				.getLongitude());
		String upperRightLat = String.valueOf(region.getUpperRight()
				.getLatitude());
		String upperRightLong = String.valueOf(region.getUpperRight()
				.getLongitude());

		return latColumn + " >= " + lowerLeftLat + " AND " + latColumn + " <= "
				+ upperRightLat + " AND " + longColumn + " >= " + lowerLeftLong
				+ " AND " + longColumn + " <= " + upperRightLong;
	}

	private String populateIDQuery(String idColumn, List<Identity> ids) {
		if (ids == null || ids.isEmpty())
			return null;

		List<String> strList = new ArrayList<String>();
		for (Identity id : ids)
			strList.add(String.valueOf(id.getId()));

		return idColumn + " IN (" + makeCommaSeparatedString(strList) + ")";
	}

	public static String makeCommaSeparatedString(Collection<String> params) {
		StringBuilder builder = new StringBuilder();
		boolean first = true;
		for (String param : params) {
			if (first) {
				builder.append(param);
				first = false;
				continue;
			}

			builder.append(",").append(param);
		}

		return builder.toString();
	}
}
