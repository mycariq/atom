package com.cariq.toolkit.serviceimpl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.coreiq.CarIQRawQuery;
import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.model.CarAggregateStatistics;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.StageStatistics;
import com.cariq.toolkit.service.AggregateDataService;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Transactional
@Configurable
public class AggregateDataServiceImplTest {

	/** The Constant TOTAL_DISTANCE. */
	private static final String BY_STATE = "ByState", CARS_COUNT = "CarsCount", TOTAL_DISTANCE = "TotalDistance",
			RENEWAL_PERCENTAGE = "RenewalPercentage", CONTEXT = "context", CREATED_ON = "createdOn",
			SUBSCRIPTION_RENEWED = "SubscriptionRenewed", SUBSCRIPTION_STATE = "subscriptionState",
			SUBSCRIPTION_EXPIRED = "SubscriptionExpired", SUBSCRIPTION_DEACTIVATED = "SubscriptionDeactivated",
			TOTAL_COUNTS = "TotalCounts";;

	@Autowired
	AggregateDataService service;

	@Test
	public void getTotalCounts() {
		Domain domain = Domain.getByName("IL-Assist");
		CarIQSimpleQuery<CarAggregateStatistics> query = new CarIQSimpleQuery<CarAggregateStatistics>("getAggregateTotalCounts",
				CarAggregateStatistics.entityManager(), CarAggregateStatistics.class);
		query.addCondition(CONTEXT, CarIQSimpleQuery.EQ, BY_STATE);
		query.orderBy(CREATED_ON, CarIQSimpleQuery.DESC);

		CarAggregateStatistics statistics = query.getSingleResult();
		List<GenericJSON> jsonList = Utils.getJSonObjectList(statistics.getValueJson(), GenericJSON.class);

		Long totalCarsCount = 0L;
		Double totalDistance = 0.0;

		for (GenericJSON json : jsonList) {
			totalCarsCount += Utils.getValue(Long.class, json, CARS_COUNT);
			totalDistance = totalDistance + Utils.getValue(Double.class, json, TOTAL_DISTANCE);
		}

		LinkedHashMap<String, String> attributeAliasMap = new LinkedHashMap<String, String>();
		List<String> tableAliasArray = new ArrayList<String>();
		attributeAliasMap.put("st.subscription_state", "subscriptionState");
		attributeAliasMap.put("count(1)", "TotalCounts");

		tableAliasArray.add("stage_statistics st");

		// get total counts of renewed policies,expired policies, deactivated policies
		CarIQRawQuery rowQuery = new CarIQRawQuery("getTotalCounts", StageStatistics.entityManager(),
				attributeAliasMap, tableAliasArray);
		rowQuery.addRawCondition("st.domain = " + domain.getId() + " group by st.subscription_state");

		int subscriptionRenewedCount = 0, subscriptionExpiredCount = 0, subscriptionDeactivatedCount = 0;
		List<GenericJSON> resultList = rowQuery.getResult();
		for (GenericJSON json : resultList) {
			String value = Utils.getValue(String.class, json, SUBSCRIPTION_STATE);
			int totalCount = Utils.getValue(Integer.class, json, TOTAL_COUNTS);
			if (SUBSCRIPTION_RENEWED.equalsIgnoreCase(value))
				subscriptionRenewedCount = totalCount;
			else if (SUBSCRIPTION_EXPIRED.equalsIgnoreCase(value))
				subscriptionExpiredCount = totalCount;
			else if (SUBSCRIPTION_DEACTIVATED.equalsIgnoreCase(value))
				subscriptionDeactivatedCount = totalCount;
		}

		int totalSubscription = (subscriptionRenewedCount + subscriptionExpiredCount + subscriptionDeactivatedCount);
		double renewalPercentage = 0;
		
		// calculate renewal percentage.
		if(totalSubscription > 0)
			renewalPercentage = Utils.downCast(Double.class,((subscriptionRenewedCount * 100)/ totalSubscription));
		
		GenericJSON result = new GenericJSON();
		result.add(CARS_COUNT, totalCarsCount);
		result.add(TOTAL_DISTANCE, totalDistance);
		result.add(RENEWAL_PERCENTAGE, renewalPercentage);


		System.out.println("--------------AggregatedDataServiceImplTest--------------");
		System.out.println("---------------------Result--------------" + result.toString());
	}

	@Test
	public void getStatisticsData() {
		CarIQSimpleQuery<CarAggregateStatistics> query = new CarIQSimpleQuery<CarAggregateStatistics>(
				"getStatisticsData", CarAggregateStatistics.entityManager(), CarAggregateStatistics.class);
		query.addCondition(CONTEXT, CarIQSimpleQuery.EQ, BY_STATE);
		query.orderBy(CREATED_ON, CarIQSimpleQuery.DESC);

		CarAggregateStatistics statistics = query.getSingleResult();
		List<GenericJSON> jsonList = Utils.getJSonObjectList(statistics.getValueJson(), GenericJSON.class);

		for (GenericJSON json : jsonList) {
			double saftyIndex = Utils.getValue(Double.class, json, "AvgDrivingScore") / 10;
			double safetyDistance = (saftyIndex * Utils.getValue(Double.class, json, TOTAL_DISTANCE));
			json.add("SafetyDistance", safetyDistance);
		}

		System.out.println("----------------getStatisticsData------------" + jsonList.toString());

	}
}