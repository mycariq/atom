package com.cariq.toolkit.coreiq.tableUtil.application.mis;

import com.cariq.toolkit.coreiq.tableUtil.impl.ProcessingContext;
import com.cariq.toolkit.coreiq.tableUtil.impl.Report;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.CellAddressIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.CellIfc;

/**
 * A static helper class to locate Cell from the Report given address and
 * Context of processing
 * 
 * @author hrishi
 *
 */
public class MISTableCellLocator {
	/**
	 * Intelligently call the locator service from the address specs.
	 * 
	 * @param processingContext
	 * @param cellAddress
	 * @return
	 */
	public static CellIfc locate(ProcessingContext processingContext, CellAddressIfc cellAddress) {
		// Get current Report from the context and start from there.
		Report currentReport = processingContext.getCurrentReport();

		// Address may have different report, but then there is no meaning anyway
		return currentReport.locate(processingContext, cellAddress);
	}
}
