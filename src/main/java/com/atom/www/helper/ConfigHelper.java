package com.atom.www.helper;

import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Configurable;

@Configurable
public class ConfigHelper {
	/** The config properties. */
	@Resource(name = "configProperties")
	private Properties configProperties;
	
	/** The host. */
	private String host;

	/** The port. */
	private String port;

	/** The security token. */
	private String securityToken;
	
	public ConfigHelper() {
		
	}
	
	@PostConstruct
	public void init() {
		this.host = configProperties.getProperty("cariqBaseUrl");
		this.port = configProperties.getProperty("cariqPort");
		this.securityToken = configProperties.getProperty("atomAdminAuth");
	}

	/**
	 * Get Host of CarIQ service
	 * @return
	 */
	public String getHost() {
		return host;
	}

	/**
	 * Get port of CarIQ service
	 * @return
	 */
	public String getPort() {
		return port;
	}

	/**
	 * Get security token to access CarIQ service
	 * @return
	 */
	public String getSecurityToken() {
		return securityToken;
	}
	
	
	/**
	 * Make url.
	 *
	 * @param url
	 *            the url
	 * @return the string
	 */
	public String makeUrl(String url) {
		//return "https://ecu-bg-dev.mycariq.com/" + url;
		// return "https://ecu.mycariq.com/" + url;
		return "https://" + host + ":" + port + "/" + url;
	}

	/**
	 * Make url.
	 *
	 * @param url
	 *            the url
	 * @return the string
	 */
	public String makeUrl(String url, String mHost) {
		//return "https://ecu-bg-dev.mycariq.com/" + url;
		// return "https://ecu.mycariq.com/" + url;
		return "https://" + mHost + ":" + port + "/" + url;
	}
}
