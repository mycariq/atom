/**
 * 
 */
package com.cariq.toolkit.utils.jsonProcessor;

/**
 * @author hrishi
 *
 */
public interface JSONProcessor {
	void process(JSONProcessorContext context, JSONPath node, Object obj);
}
