package com.cariq.toolkit.model;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.atom.www.helper.DomainHelper;
import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.User.User_In;
import com.cariq.toolkit.model.User.User_Out;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;
import com.cariq.toolkit.utils.Utils;
import com.google.common.base.Strings;
import com.cariq.toolkit.model.Authority;

/**
 * This class implements "user" in the producer context. Users are provided
 * access to specific domains.
 * 
 * @author amita
 *
 */

@Entity
@Configurable
public class User extends PersistentObject<User> implements UserDetails, JSONable<User_In, User_Out, IdJSON> {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	// ---------------------------------------------
	// SECTION 1 - Code for Persistence
	// ---------------------------------------------

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@Version
	Integer version;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new User().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null)
			this.entityManager = entityManager();
		return entityManager;
	}

	/**
	 * @param id
	 * @return
	 */
	public static User find(Long id) {
		return entityManager().find(User.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public User findById(Long id) {
		return find(id);
	}

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/**
	 * Model properties
	 */

	@NotNull(message = "@NotNull User Name")
	@Column(unique = true)
	@Size(max = 50, min = 2)
	String username;

	@NotNull(message = "@NotNull Password")
	@Size(max = 50, min = 2)
	String password;

	String role;

	@Size(max = 100, min = 2)
	String firstName;

	@Size(max = 50, min = 2)
	String lastName;

	@Column(unique = true)
	String cellNumber;

	@Column(unique = true)
	String email;

	String countryCode;

	Long timeZoneId;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	// Default constructor is a must!
	public User() {
		super();
	}

	/**
	 * Parameterized constructor
	 * 
	 * @param username
	 * @param password
	 * @param role
	 * @param firstName
	 * @param lastName
	 * @param cellNumber
	 * @param email
	 * @param countryCode
	 * @param timeZoneId
	 * @param createdOn
	 * @param modifiedOn
	 */
	public User(String username, String password, String role, String firstName, String lastName, String cellNumber,
			String email, String countryCode, long timeZoneId, Date createdOn, Date modifiedOn) {
		super();
		this.username = username;
		this.password = password;
		this.role = role;
		this.firstName = firstName;
		this.lastName = lastName;
		this.cellNumber = cellNumber;
		this.email = email;
		this.countryCode = countryCode;
		this.timeZoneId = timeZoneId;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
	}

	/**
	 * Getters Setters
	 */
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCellNumber() {
		return cellNumber;
	}

	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public long getTimeZoneId() {
		return timeZoneId;
	}

	public void setTimeZoneId(long timeZoneId) {
		this.timeZoneId = timeZoneId;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------

	// Implementation of toString and hashCode and equals are advisable for all
	// Model classes
	@Override
	public String toString() {
		return "User [id=" + id + ", version=" + version + ", username=" + username + ", password=" + password
				+ ", role=" + role + ", firstName=" + firstName + ", lastName=" + lastName + ", cellNumber="
				+ cellNumber + ", email=" + email + ", countryCode=" + countryCode + ", timeZoneId=" + timeZoneId
				+ ", createdOn=" + createdOn.toString() + ", modifiedOn=" + modifiedOn.toString() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((cellNumber == null) ? 0 : cellNumber.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((countryCode == null) ? 0 : countryCode.hashCode());
		result = prime * result + ((timeZoneId == null) ? 0 : timeZoneId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (cellNumber == null) {
			if (other.cellNumber != null)
				return false;
		} else if (!cellNumber.equals(other.cellNumber))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (countryCode == null) {
			if (other.countryCode != null)
				return false;
		} else if (!countryCode.equals(other.countryCode))
			return false;
		if (timeZoneId == null) {
			if (other.timeZoneId != null)
				return false;
		} else if (!timeZoneId.equals(other.timeZoneId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * org.springframework.security.core.userdetails.UserDetails#getAuthorities(
	 * )
	 */
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Arrays.asList(new Authority(role));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetails#
	 * isAccountNonExpired()
	 */
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetails#
	 * isAccountNonLocked()
	 */
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.security.core.userdetails.UserDetails#
	 * isCredentialsNonExpired()
	 */
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.security.core.userdetails.UserDetails#isEnabled()
	 */
	@Override
	public boolean isEnabled() {
		return true;
	}

	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API
	// interaction

	// Special input class while updating user. Password should not be updated
	// here.
	public static class UpdateUser_In {
		String username;
		String role;
		String firstName;
		String lastName;
		String cellNumber;
		String email;
		String countryCode;
		Long timeZoneId;

		public UpdateUser_In() {
			super();
		}

		public UpdateUser_In(String username, String role, String firstName, String lastName, String cellNumber,
				String email, String countryCode, Long timeZoneId) {
			super();
			this.username = username;
			this.role = role;
			this.firstName = firstName;
			this.lastName = lastName;
			this.cellNumber = cellNumber;
			this.email = email;
			this.countryCode = countryCode;
			this.timeZoneId = timeZoneId;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getRole() {
			return role;
		}

		public void setRole(String role) {
			this.role = role;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getCellNumber() {
			return cellNumber;
		}

		public void setCellNumber(String cellNumber) {
			this.cellNumber = cellNumber;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getCountryCode() {
			return countryCode;
		}

		public void setCountryCode(String countryCode) {
			this.countryCode = countryCode;
		}

		public Long getTimeZoneId() {
			return timeZoneId;
		}

		public void setTimeZoneId(Long timeZoneId) {
			this.timeZoneId = timeZoneId;
		}
	}

	// In Class (for construction)
	public static class User_In {
		String username;
		String password;
		String role;
		String firstName;
		String lastName;
		String cellNumber;
		String email;
		String countryCode;
		Long timeZoneId;

		public User_In() {
		}

		/**
		 * @param username
		 * @param password
		 * @param subscriptionName
		 * @param role
		 * @param firstName
		 * @param lastName
		 * @param cellNumber
		 * @param email
		 * @param countryCode
		 * @param timeZoneId
		 */
		public User_In(String username, String password, String role, String firstName, String lastName,
				String cellNumber, String email, String countryCode, long timeZoneId) {
			this.username = username;
			this.password = password;
			this.role = role;
			this.firstName = firstName;
			this.lastName = lastName;
			this.cellNumber = cellNumber;
			this.email = email;
			this.countryCode = countryCode;
			this.timeZoneId = timeZoneId;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getRole() {
			return role;
		}

		public void setRole(String role) {
			this.role = role;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getCellNumber() {
			return cellNumber;
		}

		public void setCellNumber(String cellNumber) {
			this.cellNumber = cellNumber;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getCountryCode() {
			return countryCode;
		}

		public void setCountryCode(String countryCode) {
			this.countryCode = countryCode;
		}

		public Long getTimeZoneId() {
			return timeZoneId;
		}

		public void setTimeZoneId(Long timeZoneId) {
			this.timeZoneId = timeZoneId;
		}

	}

	// Out class for output
	public static class User_Out extends User_In {
		Long id;
		String createdOn;
		String modifiedOn;

		public User_Out() {
			super();
		}

		/**
		 * @param username
		 * @param password
		 * @param subscriptionName
		 * @param role
		 * @param firstName
		 * @param lastName
		 * @param cellNumber
		 * @param email
		 * @param countryCode
		 * @param timeZoneId
		 * @param createdOn
		 * @param modifiedOn
		 */
		public User_Out(String username, String password, String role, String firstName, String lastName,
				String cellNumber, String email, String countryCode, long timeZoneId, String createdOn,
				String modifiedOn, Long id) {
			super(username, password, role, firstName, lastName, cellNumber, email, countryCode, timeZoneId);
			this.id = id;
			this.createdOn = createdOn;
			this.modifiedOn = modifiedOn;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toJSON()
	 */
	@Override
	public User_Out toJSON() {
		// password should not be out to JSON
		return new User_Out(username, null, role, firstName, lastName, cellNumber, email, countryCode, timeZoneId,
				createdOn.toString(), modifiedOn.toString(), id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#fromJSON(java.lang.Object)
	 */
	@Override
	public void fromJSON(User_In json) {
		this.username = json.username;
		this.password = json.password;
		this.role = json.role;
		this.firstName = json.firstName;
		this.lastName = json.lastName;
		this.cellNumber = json.cellNumber;
		this.email = json.email;
		this.countryCode = json.countryCode;
		this.timeZoneId = json.timeZoneId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}

	// ---------------------------------------------
	// SECTION 5 - Query and Class specific finders and static utility methods
	// ---------------------------------------------
	public static User findObjectById(Long id) {
		return find(id);
	}

	public static List<User> getAllObjects(int pageNo, int pageSize) {
		CarIQSimpleQuery<User> qry = new CarIQSimpleQuery<User>("getAllObjects", entityManager(), User.class);
		qry.setPaging(pageNo, pageSize);

		return qry.getResultList();
	}

	public static User getByUsername(String username) {
		if (Strings.isNullOrEmpty(username))
			return null; // callers beware!
		
		CarIQSimpleQuery<User> qry = new CarIQSimpleQuery<User>("getByUsername", entityManager(), User.class);
		qry.addCondition("username", "=", username);

		return qry.getSingleResult();
	}

	public void updateUser(UpdateUser_In json) {
		if (json.username != null)
			this.username = json.username;
		if (json.role != null)
			this.role = json.role;
		if (json.firstName != null)
			this.firstName = json.firstName;
		if (json.lastName != null)
			this.lastName = json.lastName;
		if (json.cellNumber != null)
			this.cellNumber = json.cellNumber;
		if (json.email != null)
			this.email = json.email;
		if (json.countryCode != null)
			this.countryCode = json.countryCode;
		if (json.timeZoneId != null)
			this.timeZoneId = json.timeZoneId;
		this.modifiedOn = new Date();
	}

	public static User_Out getUserAuth(String username, String password) {
		CarIQSimpleQuery<User> qry = new CarIQSimpleQuery<User>("FetchLoggedInUser", User.entityManager(), User.class);
		qry.addCondition("username", "=", username);
		qry.addCondition("password", "=", password);

		return qry.getSingleResult().toJSON();
	}

	public static String getCurrentUserRole() {
		String username = Utils.getCurrentUserName();
		User user = User.getByUsername(username);
		if (user != null) {
			System.out.println("Role : " + user.getRole());
			return user.getRole();
		}
		return null;
	}

	public static User getLoggedInUser() {
		return User.getByUsername(Utils.getCurrentUserName());
	}

	public String getCurrentDomainName(String domainName) {
		return new DomainHelper().getDomainOfLoggedInUser(domainName).getName();
	}
	
	/**
	 * Get users by usernames
	 * 
	 * @param usernames
	 * @return
	 */
	public static List<User> getUsers(List<String> usernames) {
		CarIQSimpleQuery<User> qry = new CarIQSimpleQuery<User>("getUsers", entityManager(), User.class);
		qry.addRawCondition("username in " 
				+ Utils.getCommaSeperatedValuesForQuery(Utils.getCommaSeperatedString(usernames)));
		
		return qry.getResultList();
	}
}
