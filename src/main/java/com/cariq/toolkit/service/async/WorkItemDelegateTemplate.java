package com.cariq.toolkit.service.async;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.cariq.toolkit.coreiq.server.Server;
import com.cariq.toolkit.coreiq.server.ServerActivity;
import com.cariq.toolkit.model.async.WorkItem;
import com.cariq.toolkit.serviceimpl.async.WIHelper;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * The Class WorkItemDelegateTemplate.
 */
public abstract class WorkItemDelegateTemplate implements Job {

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger(WorkItemDelegateTemplate.class.getName());

	/**
	 * Do execute.
	 *
	 * @param workItem
	 *            the work item
	 * @param updater
	 *            the updater
	 * @throws Exception
	 */
	abstract protected void doExecute(WorkItem workItem, WorkItemUpdater updater) throws Exception;

	/**
	 * Gets the input JSON.
	 *
	 * @param <T>
	 *            the generic type
	 * @param workItem
	 *            the work item
	 * @param clazz
	 *            the clazz
	 * @return the input JSON
	 */
	static final protected <T> T getInputJSON(WorkItem workItem, Class<T> clazz) {
		String inputJson = workItem.getInputJson();

		return Utils.getJSonObject(inputJson, clazz);
	}

	/**
	 * Gets the input JSON.
	 *
	 * @param workItem
	 *            the work item
	 * @return the input JSON
	 */
	static final protected GenericJSON getInputJSON(WorkItem workItem) {
		return getInputJSON(workItem, GenericJSON.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
	 */
	//	@Transactional
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.debug("Executing WorkItem: " + this.getClass().getSimpleName());
		WorkItemUpdater updater = WIHelper.getUpdater(context);
		try (ProfilePoint _WorkItemExecution = ProfilePoint
				.profileActivity("ProfAction_WorkItemExecution" + this.getClass().getSimpleName())) {
			if (updater == null || updater.getItem() == null)
				Utils.handleException("No WorkItem Details found for workitem: " + this.getClass().getSimpleName());
			WorkItem item = updater.getItem();
			logger.getLogger("DBG20200228").debug("executing item " + item.toDebugString());

			// RAII pattern to manage serverWorker.
			try (ServerActivity serverActivity = Server.getInstance().registerWithSubCategory(Server.WORKER_AUDIT, item.getType(), item.getName())) {
				updater.start();
//				logger.debug("DBG20200228 Start doExecute: " + this.getClass().getSimpleName());
				doExecute(item, updater);
//				logger.debug("DBG20200228 Finish doExecute: " + this.getClass().getSimpleName());
				updater.setPercentComplete(100);
				updater.complete();
			} catch (Exception e) {
				updater.fail(Utils.getExceptionMessage(e));
				throw new JobExecutionException(e);
			}
		}
	}
}
