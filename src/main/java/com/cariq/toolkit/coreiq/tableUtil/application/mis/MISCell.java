package com.cariq.toolkit.coreiq.tableUtil.application.mis;

import com.cariq.toolkit.coreiq.tableUtil.application.mis.MISProcessingContext.ContextSetter;
import com.cariq.toolkit.coreiq.tableUtil.impl.Cell;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.CellValueEvaluatorIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.ColumnIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.RowIfc;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

/**
 * The smallest unit of the table - made up of {row,column} tuple
 * @author hrishi
 *
 */
public class MISCell extends Cell implements MISProcessable {

	// This is currently unused - since Cell value is purely based on applicability
	CellValueEvaluatorIfc evaluator = null;
	
	public MISCell(RowIfc row, ColumnIfc column) {
		super(row, column);
		// TODO Auto-generated constructor stub
	}

	public MISCell(RowIfc row, ColumnIfc column, Object value) {
		super(row, column, value);
	}

	@Override
	public MISCell process(MISProcessingContext context, GenericJSON json) {
		// Basic implementation - ideally should be through the evaluator
		MISRow row = (MISRow) getItsRow();
		MISColumn col = (MISColumn) getItsColumn();

		// Set the context before processing
		try (ContextSetter<MISRow> _rowsetter = context.getContextSetter(row)) {
			try (ContextSetter<MISColumn> _colsetter = context.getContextSetter(col)) {
				try (ContextSetter<MISCell> _cellsetter = context.getContextSetter(this)) {

					// If rule of Row and Rule of Column is applicable, increment the value
					if (row.isApplicable(json) && col.isApplicable(json)) {
						Integer currentVal = Utils.downCast(Integer.class, getValue());

						// Check if Row or Column have an evaluator
						Integer newValue = row.evaluateCellValue(context, json, this);
						if (newValue == null)
							newValue = col.evaluateCellValue(context, json, this);

						// else invoke default evaluator - which will just increment the value
						if (newValue == null)
							newValue = currentVal == null ? 1 : currentVal + 1;

						setValue(newValue);
					}

					return this;
				}
			}
		}
	}
}
