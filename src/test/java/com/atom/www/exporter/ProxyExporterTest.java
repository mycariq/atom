package com.atom.www.exporter;

import org.junit.runner.RunWith;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.coreiq.exporter.CarIQExporter;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterRegistry;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Transactional
@Configurable
public class ProxyExporterTest {
	
	@Before
	public void init() {
		Utils.setupSecurityContext("preeti", "875d48192ccbbb18f32a13cec13cac14", "a1");
	}

	@Test 
	public void testOwnerQueryType() {
		CarIQExporter exporter = CarIQExporterRegistry.getExporter("ProxyExporter");
		GenericJSON json = new GenericJSON();
		json.put("CreatedAfter", "2018-10-01 12:00:00");
		json.put("Domain", "IL-Assist");
		json.put("Owner", "preeti");
		
		System.out.println(exporter.export("owner", json, 1, 2000));
	}
	
	public void testStateQueryType() {
		
	}
	
	public void testStageQueryType() {
		
	}
}
