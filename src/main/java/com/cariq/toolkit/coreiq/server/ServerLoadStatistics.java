package com.cariq.toolkit.coreiq.server;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

import com.cariq.toolkit.model.async.WorkItem;

public class ServerLoadStatistics implements ServerSpecificResponse {
	
	public static final String SERVER_LOAD_INDEX = "ServerLoadIndex";
	
	// Host name and start time
	String serverName;
	Date serverStartTime;

	LoadUnit apiLoad, workerLoad, queryLoad, taskLoad, restCallLoad;
	Collection<ServerHistoricActivity> historicActivities;

	public ServerLoadStatistics(String serverName, Date serverStartTime, Map apiLoad, Map workerLoad, Map queryLoad,
			Map taskLoad, Map restCallLoad, Collection<ServerHistoricActivity> historicActivities) {
		this.serverName = serverName;
		this.serverStartTime = serverStartTime;
		this.apiLoad = new LoadUnit("apiLoad", apiLoad);
		this.workerLoad = new LoadUnit("workerLoad", workerLoad);
		this.queryLoad = new LoadUnit("queryLoad", queryLoad);
		this.taskLoad = new LoadUnit("taskLoad", taskLoad);
		this.restCallLoad = new LoadUnit("restCallLoad", restCallLoad);
		this.historicActivities = historicActivities;
	}


	
	
	// utility APIs to be used by dispenser etc.

	public String getServerName() {
		return serverName;
	}




	public Date getServerStartTime() {
		return serverStartTime;
	}




	public LoadUnit getApiLoad() {
		return apiLoad;
	}




	public LoadUnit getWorkerLoad() {
		return workerLoad;
	}




	public LoadUnit getQueryLoad() {
		return queryLoad;
	}




	public LoadUnit getTaskLoad() {
		return taskLoad;
	}




	public LoadUnit getRestCallLoad() {
		return restCallLoad;
	}




	public Collection<ServerHistoricActivity> getHistoricActivities() {
		return historicActivities;
	}




	public int getMicroWorkerCount() {
		int count = 0;
		if (workerLoad == null)
			return count;
		
		
		for (ServerActivity activity : workerLoad.getItems()) {
			if (WorkItem.MICRO.equals(activity.getSubCategory()))
					count++;
		}

		return count;
	}

	public int getWorkerCount() {
		int count = 0;
		if (workerLoad == null)
			return count;
		
		return workerLoad.getCount();
		
	}
}
