package com.cariq.toolkit.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_In;
import com.cariq.toolkit.model.Timeline.Timeline_In;
import com.cariq.toolkit.model.Timeline.Timeline_Out;
import com.cariq.toolkit.model.User.User_In;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;

/**
 * This class implements "ProxyObject" concept. We use proxy calls to obtain objects from "remote" services (e.g calls to CarIQ/W4 API layers). 
 * These objects can be retained in the current service/layer using their lighter version(just identification). 
 * These proxyobjects can be used to enhance functionality in the current service.
 * 
 * @author sagar
 *
 */

@Entity
@Configurable
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "proxy", "createdOn" }))
public class Timeline extends PersistentObject<Timeline> implements JSONable<Timeline_In, Timeline_Out, IdJSON> {

	// SECTION 1 -Code for Persistence
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@Version
	Integer version;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new Timeline().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null)
			this.entityManager = entityManager();
		return entityManager;
	}

	/**
	 * @param id
	 * @return
	 */
	protected static Timeline find(Long id) {
		return entityManager().find(Timeline.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public Timeline findById(Long id) {
		return find(id);
	}

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/**
	 * Model properties
	 */

	@NotNull
	@ManyToOne
	ProxyObject proxy;
	
	@Column(length = 5100)
	String message;

	String type;
	
	String tag;
	
	String stage;
	
	@NotNull
	@ManyToOne
	User user;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	// Default constructor is a must!
	public Timeline() {
		super();
	}


	public Timeline(ProxyObject proxy, String message, String type, String tag, String stage, User user, Date createdOn,
			Date modifiedOn) {
		super();
		this.proxy = proxy;
		this.message = message;
		this.type = type;
		this.tag = tag;
		this.stage = stage;
		this.user = user;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
	}

	/**
	 * Getters Setters
	 */
	

	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------

	// Implementation of toString and hashCode and equals are advisable for all
	// Model classes
	@Override
	public String toString() {
		return "ProxyObject [id=" + id + ", version=" + version + ", proxy =" + proxy.getId() + 
				", message =" + message + ", type =" + type + ", tag=" + tag + ", stage=" + stage +
				", user=" + user.getId() +
				", createdOn=" + createdOn + ", modifiedOn=" + modifiedOn + "]";
	}

	public ProxyObject getProxy() {
		return proxy;
	}

	public void setProxy(ProxyObject proxy) {
		this.proxy = proxy;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((proxy == null) ? 0 : proxy.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((tag == null) ? 0 : tag.hashCode());
		result = prime * result + ((stage == null) ? 0 : stage.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Timeline other = (Timeline) obj;
		if (proxy == null) {
			if (other.proxy != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!tag.equals(other.tag))
			return false;
		if (stage == null) {
			if (other.stage != null)
				return false;
		} else if (!user.getId().equals(other.user.getId()))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		return true;
	}

	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API
	// interaction
	// In Class (for construction)
	public static class Timeline_In {
		ProxyObject_In proxyObject;
		String message;
		String type;	
		String tag;		
		String stage;		
		String username;
		Date tCreatedOn;
		
		String nextAction;
		public Timeline_In() {
			super();
		}

		/**
		 * Parameterized constructor
		 * 
		 * @param proxy
		 * @param message
		 * @param type
		 * @param tag
		 * @param stage
		 * @param user
		 */
		public Timeline_In(ProxyObject_In proxyId, String message, String type,
				String tag, String stage, String username, Long domainId) {
			this(proxyId, message, type, tag, stage, username, domainId, null, null);
		}
		
		public Timeline_In(ProxyObject_In proxyId, String message, String type,
				String tag, String stage, String username, Long domainId, Date tCreatedOn,
				String nextAction) {
			this.proxyObject = proxyId;
			this.message = message;
			this.type = type;
			this.tag = tag;
			this.stage = stage;
			this.username = username;
			this.tCreatedOn = tCreatedOn;
			this.nextAction = nextAction;
		}		

		public ProxyObject_In getProxyObject() {
			return proxyObject;
		}

		public void setProxyObject(ProxyObject_In proxyObject) {
			this.proxyObject = proxyObject;
		}

		public String getMessage() {
			return this.message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getTag() {
			return this.tag;
		}

		public void setTag(String tag) {
			this.tag = tag;
		}

		public String getStage() {
			return this.stage;
		}

		public void setStage(String stage) {
			this.stage = stage;
		}

		public String getUsername() {
			return this.username;
		}

		public void setUser(String user) {
			this.username = user;
		}

		public Date gettCreatedOn() {
			return tCreatedOn;
		}

		public void settCreatedOn(Date tCreatedOn) {
			this.tCreatedOn = tCreatedOn;
		}

		public String getNextAction() {
			return nextAction;
		}

		public void setNextAction(String nextAction) {
			this.nextAction = nextAction;
		}
		
		
	}

	// Out class for output
	public static class Timeline_Out extends Timeline_In {
		Long id;
		User.User_In user;
		String createdOn;
		String modifiedOn;

		public Timeline_Out() {
			super();
		}

		/**
		 * Constructor
		 * 
		 * @param proxyId
		 * @param message
		 * @param type
		 * @param tag
		 * @param stage
		 * @param userId
		 * @param id
		 * @param user
		 * @param proxy
		 * @param createdOn
		 * @param modifiedOn
		 */
		public Timeline_Out(ProxyObject_In proxyId, String message, String type, String tag, String stage, String username, Long id,
				User_In user, ProxyObject.ProxyObject_In proxy, String createdOn,
				String modifiedOn) {
			super(proxyId, message, type, tag, stage, username, null);
			this.id = id;
			this.user = user;
			this.createdOn = createdOn;
			this.modifiedOn = modifiedOn;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public User_In getUser() {
			return user;
		}

		public void setUser(User.User_In user) {
			this.user = user;
		}

		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toJSON()
	 */
	@Override
	public Timeline_Out toJSON() {
		
		String createdOnStr = createdOn == null ? null : createdOn.toString();
		String modifiedOnStr = modifiedOn == null ? null : modifiedOn.toString();
		
		return new Timeline_Out(proxy.toJSON(), message, type, tag, stage, user.getUsername(), id, user.toJSON(), proxy.toJSON(), createdOnStr, modifiedOnStr);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#fromJSON(java.lang.Object)
	 */
	@Override
	public void fromJSON(Timeline_In json) {
		
		Domain domain = null;
		
		if(json.getProxyObject().domainId != null)
			domain = Domain.findObjectById(json.getProxyObject().domainId);
		else 
			domain = Domain.getByName(json.getProxyObject().getDomainName());
		
		this.message = json.getMessage();
		this.stage = json.getStage();
		this.tag = json.getTag();
		this.type = json.getType();
		this.user = json.getUsername() == null ? null : User.getByUsername(json.getUsername());
		if(json.gettCreatedOn() == null)
			this.createdOn = new Date();
		else 
			this.createdOn = json.gettCreatedOn();
		
		this.proxy = ProxyObject.createOrGet(domain, 
				json.getProxyObject().objectId, json.getProxyObject().objectType, 
				json.getProxyObject().signature, null, json.getTag());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}

	// ---------------------------------------------
	// SECTION 5 - Query and Class specific finders and static utility methods
	// ---------------------------------------------
	public static Timeline findObjectById(Long id) {
		return find(id);
	}

	/**
	 * Get objects by using pagination
	 * 
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public static List<Timeline> getAllObjects(int pageNo, int pageSize) {
		CarIQSimpleQuery<Timeline> qry = new CarIQSimpleQuery<Timeline>("getAllObjects", entityManager(), Timeline.class);
		qry.setPaging(pageNo, pageSize);

		return qry.getResultList();
	}

	/**
	 * Get sorted timeline for given user.
	 * 
	 * @param userId
	 * @return
	 */
	public static List<Timeline> getUserTimeline(ProxyObject proxy) {
		
		if(proxy == null)
			return null;
		
		CarIQSimpleQuery<Timeline> qry = new CarIQSimpleQuery<Timeline>("getUserTimeline", entityManager(), Timeline.class);
		qry.addCondition("proxy", "=", proxy);
		qry.orderBy("createdOn", "DESC");
		return qry.getResultList();
	}
	
	
	/**
	 * Get list of timelines for all proxy object 
	 * 		
	 * @param proxyObjects		proxy objects for which timelines need to fetch.
	 * @return					list of timelines
	 */
	public static List<Timeline> getUserTimelines(List<ProxyObject> proxyObjects) {
		
		if(proxyObjects == null || proxyObjects.isEmpty())
			return null;
		
		CarIQSimpleQuery<Timeline> qry = new CarIQSimpleQuery<Timeline>("getUserTimeline", entityManager(), Timeline.class);
		qry.addCondition("proxy", "IN", proxyObjects);
		qry.orderBy("createdOn", "DESC");
		return qry.getResultList();		
	}
	
	public static int getCallCountFromTimeline(List<Timeline_Out> timeline) {
		
		int callCount = 0;
		
		//if input timeline is invalid or empty then return count 0;
		if(timeline == null || timeline.isEmpty())
			return callCount;
		
		for(Timeline_Out timelineItem : timeline) {
			String type = timelineItem.getType();
			if(type != null && type.equalsIgnoreCase("Phone call"))
				callCount++;
		}
		return callCount;
	}
	
	public static ProxyObject getProxyByObjectId(Long objectId) {
		CarIQSimpleQuery<ProxyObject> qry = new CarIQSimpleQuery<ProxyObject>("getProxyByObjectId", entityManager(), ProxyObject.class);
        qry.addCondition("objectId", "=", objectId);
               
		if(qry.getResultList().isEmpty())
			return null;
		            
		return qry.getSingleResult();
	}

	/**
	 * Find timeline by proxy object and created on date
	 * 
	 * @param proxy
	 * @param createdOn
	 * @return
	 */
	public static Timeline findByProxyAndCreatedOn(ProxyObject proxy, Date createdOn) {
		CarIQSimpleQuery<Timeline> qry = new CarIQSimpleQuery<Timeline>("getUserTimeline", entityManager(), Timeline.class);
		qry.addCondition("proxy", "=", proxy);
		qry.addCondition("createdOn", "=", createdOn);
		return qry.getSingleResult();
	}
}
