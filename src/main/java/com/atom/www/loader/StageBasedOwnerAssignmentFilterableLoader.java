package com.atom.www.loader;

import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.coreiq.loader.CarIQLoaderContext;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.google.common.base.Strings;

public class StageBasedOwnerAssignmentFilterableLoader extends StageBasedOwnerAssignmentLoader {

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("StageBasedOwnerAssignmentFilterableLoader");
	
	public static final String TheLoader = "StageBasedOwnerAssignmentFilterableLoader";
	public static final String TheDescription = "This is extention of StageBasedOwnerAssignmentLoader"
			+ ", but adds feature of filtering cases based on specific column and it's value.";
	
	private static final String COLUMN_NAME = "Column Name";
	private static final String COLUMN_VALUE = "Column Value";
	
	
	/**
	 * Default constructor.
	 */
	public StageBasedOwnerAssignmentFilterableLoader() {
		super(TheLoader, TheDescription);
		
		//Two new values to be loaded
		addAttribute(COLUMN_NAME, "Name of the column which you want to add filter on.", false);
		addAttribute(COLUMN_VALUE, "Value of the column which you want to add filter on.", false);
	}
	
	@Override
	protected void doLoad(GenericJSON json, CarIQLoaderContext context) {
		
		String columnName = Utils.getValue(json, COLUMN_NAME);
		String columnValue = Utils.getValue(json, COLUMN_VALUE);
		
		//Handle null or empty
		if(!Utils.notNullAndEmpty(columnName)) {
			Utils.handleException("Column name should not be empty!");
		}

		//Handle null or empty
		if(!Utils.notNullAndEmpty(columnValue)) {
			Utils.handleException("Column value should not be empty!");
		}
		
		super.doLoad(json, context);
	}
	
	@Override
	List<GenericJSON> filterWithCondition(List<GenericJSON> policies, GenericJSON inputJSON) {
		List<GenericJSON> filteredList = super.filterWithCondition(policies, inputJSON);
		
		List<GenericJSON> finalFilteredList = new ArrayList<>();
		
		String columnName = Utils.getValue(inputJSON, COLUMN_NAME);
		String columnValue = Utils.getValue(inputJSON, COLUMN_VALUE);
		
		for(GenericJSON json: filteredList) {
			String columnValueOfCase = Utils.getValue(json, columnName);
			if (Strings.isNullOrEmpty(columnValueOfCase))
				continue;
			
			//If object has column value same as that of input column value
			//then add in final list.
			if(columnValueOfCase.equalsIgnoreCase(columnValue))
				finalFilteredList.add(json);
		}
		
		return finalFilteredList;
	}
}
