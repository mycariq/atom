package com.atom.www.exporter;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.cariq.toolkit.coreiq.exporter.CarIQExporter;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterRegistry;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.model.Spotlight;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * Spotlight exporter exports data to be shown in spotlight.
 * @author sagar
 *
 */
public class SpotlightExporter extends CarIQExporterTemplate {

	private static final String TheExporter = "Spotlight";
	private static final String TheDescription = "Spotlight exporter exports data to be shown in spotlight.";
	
	private static final String QueryType_default = "Default";
	private static final String defaultQueryTypeDescription = "Default query type description";
	
	private static final String createdAfter = "CreatedAfter";
	private static final String createdAfterQueryTypeDescription = "Policies created after date";
	
	private static final String Domain = "domain";
	private static final String DomainDescription = "Domain(Ex. IL-Assist, Driven, TAGIC, Drivesmart, etc...)";
	
	private static final String Owner = "owner";
	private static final String OwnerDescription = "Username of owner.";
	
	private static final String NAME = "name";
	private static final String NAME_DESCRIPTION = "Name of spotlight.";
	
	/**
	 * Constructor
	 */
	public SpotlightExporter() {
		super(TheExporter, TheDescription);
		
		//query type default
		addQueryType(QueryType_default, defaultQueryTypeDescription,
				Arrays.asList(new QueryDefinitionAttribute(createdAfter, createdAfterQueryTypeDescription, Date.class),
								new QueryDefinitionAttribute(Domain, DomainDescription, String.class),
								new QueryDefinitionAttribute(Owner, OwnerDescription, String.class),
								new QueryDefinitionAttribute(NAME, NAME_DESCRIPTION, String.class)));
	}

	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON inputJson, int pageNo, int pageSize) {
		try (ProfilePoint _doExportSpotlightExporter =
				ProfilePoint.profileAction("ProfAction_doExportSpotlightExporter")) {
			
			//find spotlight by name
			Spotlight spotlight = Spotlight.getByName(Utils.getValue(inputJson, NAME));
			
			//get datastream exporter by using spotlight datastream.
			CarIQExporter exporter = CarIQExporterRegistry.getExporter(spotlight.getDataStream());
			
			//Export in one shot(One page)
			//and in input json there must be owner, domain
			return exporter.export(queryType, inputJson, 1, spotlight.getSize());
		}
	}
}
