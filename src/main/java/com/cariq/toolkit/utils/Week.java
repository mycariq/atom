package com.cariq.toolkit.utils;

import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * Capturing concept of a Week
 * @author Abhijit
 *
 */
public class Week {
	// Weekday definition as per JODA DateTime
	public enum WeekDay {
		Monday(1), // 1
		Tuesday(2), // 2
		Wednesday(3), // 3
		Thursday(4), // 4
		Friday(5), // 5
		Saturday(6), // 6	
		Sunday(7); // 0
		
	    private final int value;
	    private WeekDay(int value) {
	        this.value = value;
	    }

	    public int getValue() {
	        return value;
	    }
	}
	
	public static final WeekDay StartDayOfTheWeek = WeekDay.Sunday;
	
	public static final int DAYS_OF_WEEK = 7;
	Day startDay;
	
	public Week(Day startDate) {
		startDay = startDate;
	}

	/**
	 * Get Week object from start date - Static Construction
	 * @param day
	 * @return
	 */
	public static Week getWeekStartingFrom(Day day) {
		return new Week(day);
	}

	/**
	 * Get Week object from end date - Static Construction
	 * @param day
	 * @return
	 */
	public static Week getWeekEndingOn(Day day) {
		return new Week(day.getDaysBefore(7));
	}
	
	public String getTimeZone() {
		return startDay.getTimeZone();
	}

	public Day getStartDay() {
		return startDay;
	}
	
	public Day getEndDay() {
		return startDay.getDaysAfter(7);
	}
	
	public Date getStartTime() {
		return startDay.getStart();
	}
	
	public Date getEndTime() {
		return getEndDay().getEnd();
	}
	
	/**
	 * If Day lies in the week
	 * @param day
	 * @return
	 */
	public boolean isInTheWeek(Day day) {
		return isInTheWeek(day.getStart());

	}
	
	/**
	 * If TimeStamp lies in the week
	 * @param day
	 * @return
	 */
	public boolean isInTheWeek(Date timeStamp) {
		return Day.isWithinDays(getStartDay(), getEndDay(), timeStamp);

	}
	
	/**
	 * Get the Day's date from this or previous week on given Day
	 * @param weekDay
	 * @return
	 */
	public static Day getLastWeekDay(Date date, WeekDay weekDay, String timeZone) {
		DateTime dateTime = (date == null) ? new DateTime(DateTimeZone.forID(timeZone == null? Utils.INDIA_TIME_ZONE : timeZone)) : new DateTime(date);

		DateTime weekdayTime = dateTime.withDayOfWeek(weekDay.getValue());
		if (weekdayTime.isAfter(dateTime)) {
			weekdayTime = weekdayTime .minusWeeks(1);
		}
		
		return new Day(weekdayTime.toDate(), timeZone);
	}

	public static Week getCurrentWeek(Date date, String timeZone) {
		DateTime dateTime = (date == null) ? new DateTime(DateTimeZone.forID(timeZone == null? Utils.INDIA_TIME_ZONE : timeZone)) : new DateTime(date);
		
		Day sunday = getLastWeekDay(dateTime.toDate(), StartDayOfTheWeek, timeZone);
		
		return getWeekStartingFrom(sunday);
	}
	
	public static Week getLastWeek(Date date, String timeZone) {
		DateTime dateTime = (date == null) ? new DateTime(DateTimeZone.forID(timeZone == null? Utils.INDIA_TIME_ZONE : timeZone)) : new DateTime(date);
		
		// go one week back
		return getCurrentWeek(dateTime .minusWeeks(1).toDate(), timeZone);
	}

}
