package com.cariq.toolkit.coreiq.tableUtil.impl;

import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.coreiq.tableUtil.interfaces.CellAddressIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.CellIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.NameIndexIdentifiable;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.ReportIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.TableIfc;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.GenericJsonable;
import com.cariq.toolkit.utils.ObjectBuilder;
import com.cariq.toolkit.utils.Utils;

/**
 * Basic Table interface. It can be built incrementally by adding rows and columns
 * Cells are automatically created based on Rows and Columns
 * @author hrishi
 *
 */
public class Report implements ReportIfc, ObjectBuilder<Object>, GenericJsonable {
	public static final String TABLE = "TABLE";
	
	String name = "Report";
	List<TableIfc> tables = new ArrayList<TableIfc>();

	public Report(String name) {
		this.name = name;
	}

	public Report() {
		// TODO Auto-generated constructor stub
	}

	public Report addTable(TableIfc table) {
		tables.add(table);
		return this;
	}
	
	@Override
	public Report configure(String setting, Object value) {
		Utils.notImplementedException("Table.configure");
		return null;
	}

	@Override
	public Report add(Object value) {
		Utils.notImplementedException("Table.add");
		return null;
	}

	@Override
	public Report add(String parameter, Object value) {
		if (TABLE.equalsIgnoreCase(parameter)) {
			addTable(Utils.downCast(TableIfc.class, value));
		} else {
			Utils.handleException("Invalid parameter type for Table Builder: " + parameter);
		}
		
		// TODO Auto-generated method stub
		return this;
	}

	@Override
	public Report build() {
		for (TableIfc tableIfc : tables) {
			Table table = Utils.downCast(Table.class, tableIfc);
			table = table.build();
		}
		return this;
	}

	/**
	 * The output that will be rendered in html
	 */
	@Override
	public GenericJSON toGenericJSON() {
		List<GenericJSON> tableJSONs = new ArrayList<GenericJSON>();
		for (TableIfc table : tables) {
			tableJSONs.add(table.toGenericJSON());
		}
		// Go through the table and spit out
		return GenericJSON.build("name", name, "tables", tableJSONs);
	}

	@Override
	public void fromGenricJSON(GenericJSON json) {
		Utils.notImplementedException("MISReport.fromGenericJSON");
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public List<TableIfc> getTables() {
		return tables;
	}

	@Override
	public CellIfc locate(ProcessingContext processingContext, CellAddressIfc cellAddress) {
		// If address doesn't have this report, return null
		if (!isMatching(processingContext, cellAddress))
			return null;
		
		// Check if Tabel address is given - else locate in current table
		if (cellAddress.getTableIdentifier() == null)
			return processingContext.getCurrentTable().locate(processingContext, cellAddress);
		
		// Check if Index given
		NameIndexIdentifiable tableId = cellAddress.getTableIdentifier();
		if (tableId.getIndex() != null) {
			// locate based on index - index is 1 based
			TableIfc tableIfc = tables.get(tableId.getIndex() - 1);
			return tableIfc.locate(processingContext, cellAddress);
		}

		// else to through each table and check - feels a little unnatural
		for (TableIfc tableIfc : tables) {
			if (!tableIfc.isMatching(processingContext, cellAddress))
				continue;
			
			CellIfc cell = tableIfc.locate(processingContext, cellAddress);
			
			// Try to locate till we find the right table
			if (cell != null)
				return cell;
		}

		// Otherwise return null
		return null;
	}

	@Override
	public boolean isMatching(ProcessingContext processingContext, CellAddressIfc cellAddress) {
		NameIndexIdentifiable addressReportIdentifier = cellAddress.getReportIdentifier();

		// By default match
		if ( addressReportIdentifier == null)
			return true;
		
		// check for name
		String addressReportName = cellAddress.getReportIdentifier().getName();
		if ( addressReportName == null || addressReportName.equalsIgnoreCase(name))
			return true;
		
		// @TODO finally check from ProcessingContext - but not important here.
		// don't check for index
		return false;
	}
}
