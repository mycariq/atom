package com.atom.www.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cariq.toolkit.model.BillingAccount.BillingAccount_In;
import com.cariq.toolkit.model.BillingAccount.BillingAccount_Out;
import com.cariq.toolkit.model.BillingContract.BillingContract_In;
import com.cariq.toolkit.model.BillingContract.BillingContract_Out;
import com.cariq.toolkit.model.BillingTerm.BillingTerm_In;
import com.cariq.toolkit.model.BillingTerm.BillingTerm_Out;
import com.cariq.toolkit.model.Payment.Payment_In;
import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.publicapi.CarIQPublicAPIParameter;
import com.cariq.toolkit.service.BillingService;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.utils.CarIQMutableAPI;
import com.cariq.toolkit.utils.JsonValidator;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;

/**
 * The Class BillingController.
 */
@CarIQPublicAPI(name = "Billing", description = "Everything related to Billing")
@Controller
@RequestMapping("/billing")
public class BillingController {
	/** The BillingService. */
	@Autowired
	private BillingService billingservice;

	/*
	 * APIs for BillingAccount
	 */
	
	@CarIQPublicAPIMethod(description = "Create new billing account", responseClass = ResponseWithIdJson.class, internal = true)
	@RequestMapping(value = "/account/add", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public ResponseWithIdJson addBillingAccount(@RequestBody @Valid BillingAccount_In params,  BindingResult result) {
		try (CarIQMutableAPI _addBillingAccount = new CarIQMutableAPI("CARIQ_addBillingAccount", params)) {
			JsonValidator.validate(result);
			return billingservice.addBillingAccount(params);
		}
	}

	@CarIQPublicAPIMethod(description = "Get all billing accounts", responseClass = BillingAccount_Out.class, internal = true)
	@RequestMapping(value = "/account/getAll/{pageNo}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public List<BillingAccount_Out> getAllBillingAccounts(			
			@CarIQPublicAPIParameter(name = "PageNo", description = "Page Number") @PathVariable int pageNo, 
			@CarIQPublicAPIParameter(name = "PageSize", description = "Page Size") @PathVariable int pageSize) {
		try (CarIQAPI _getBillingAccounts = new CarIQAPI("CARIQ_getAllBillingAccounts_with_pagination")) {
			return billingservice.getAllBillingAccounts(pageNo, pageSize);
		}
	}
	
	@CarIQPublicAPIMethod(description = "Get billing account by name", responseClass = BillingAccount_Out.class, internal = true)
	@RequestMapping(value = "/account/{name}", method = RequestMethod.GET)
	@ResponseBody
	public BillingAccount_Out getBillingAccount(
			@CarIQPublicAPIParameter(name = "name", description = "BillingAccount Name")@PathVariable String name) {
		try (CarIQAPI _getBillingAccount = new CarIQAPI("CARIQ_getBillingAccount")) {
			return billingservice.getBillingAccount(name);
		}
	}

	@CarIQPublicAPIMethod(description = "Update billing account", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/account/update", method = RequestMethod.PUT)
	@ResponseBody
	@Transactional
	public ResponseJson updateBillingAccount(
			@RequestBody @Valid BillingAccount_In params, BindingResult result) {
		try (CarIQMutableAPI _removeBillingAccount = new CarIQMutableAPI("CARIQ_removeBillingAccount", params)) {
			JsonValidator.validate(result);
			return billingservice.updateBillingAccount(params);
		}
	}
	
	@CarIQPublicAPIMethod(description = "Delete billing account by id", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/account/remove/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	@Transactional
	public ResponseJson removeBillingAccount(
			@CarIQPublicAPIParameter(name = "id", description = "BillingAccount Id") @PathVariable Long id) {
		try (CarIQMutableAPI _removeBillingAccount = new CarIQMutableAPI("CARIQ_removeBillingAccount", id)) {
			return billingservice.deleteBillingAccount(id);
		}
	}
	
	/*
	 * APIs for BillingContract
	 */
	
	@CarIQPublicAPIMethod(description = "Create new billing contract", responseClass = ResponseWithIdJson.class, internal = true)
	@RequestMapping(value = "/contract/add", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public ResponseWithIdJson addBillingContract(@RequestBody @Valid BillingContract_In params,  BindingResult result) {
		try (CarIQMutableAPI _addBillingContract = new CarIQMutableAPI("CARIQ_addBillingContract", params)) {
			JsonValidator.validate(result);
			return billingservice.addBillingContract(params);
		}
	}
	
	@CarIQPublicAPIMethod(description = "Get billing contract by id", responseClass = BillingAccount_Out.class, internal = true)
	@RequestMapping(value = "/contract/{id}", method = RequestMethod.GET)
	@ResponseBody
	public BillingContract_Out getBillingContract(
			@CarIQPublicAPIParameter(name = "id", description = "BillingContract Id") @PathVariable Long id) {
		try (CarIQAPI _getBillingContract = new CarIQAPI("CARIQ_getBillingContract")) {
			return billingservice.getBillingContract(id);
		}
	}
	
	@CarIQPublicAPIMethod(description = "Update billing contract", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/contract/update/{id}", method = RequestMethod.PUT)
	@ResponseBody
	@Transactional
	public ResponseJson updateBillingContract(
			@RequestBody @Valid BillingContract_In params,  BindingResult result) {
		try (CarIQMutableAPI _updateBillingContract = new CarIQMutableAPI("CARIQ_updateBillingContract", params)) {
			JsonValidator.validate(result);
			return billingservice.updateBillingContract(params);
		}
	}
	
	@CarIQPublicAPIMethod(description = "Delete billing contract by id", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/contract/remove/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	@Transactional
	public ResponseJson removeBillingContract(
			@CarIQPublicAPIParameter(name = "id", description = "BillingContract Id") @PathVariable Long id) {
		try (CarIQMutableAPI _removeBillingContract = new CarIQMutableAPI("CARIQ_removeBillingContract", id)) {
			return billingservice.deleteBillingContract(id);
		}
	}
	
	/*
	 * APIs for BillingTerm
	 */
	
	@CarIQPublicAPIMethod(description = "Create new billing term", responseClass = ResponseWithIdJson.class, internal = true)
	@RequestMapping(value = "/term/add", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public ResponseWithIdJson addBillingTerm(@RequestBody @Valid BillingTerm_In params,  BindingResult result) {
		try (CarIQMutableAPI _addBillingTerm = new CarIQMutableAPI("CARIQ_addBillingTerm", params)) {
			JsonValidator.validate(result);
			return billingservice.addBillingTerm(params);
		}
	}
	
	@CarIQPublicAPIMethod(description = "Get billing term by name", responseClass = BillingAccount_Out.class, internal = true)
	@RequestMapping(value = "/term/{name}", method = RequestMethod.GET)
	@ResponseBody
	public BillingTerm_Out getBillingTerm(
			@CarIQPublicAPIParameter(name = "name", description = "BillingTerm Name") @PathVariable String name) {
		try (CarIQAPI _getBillingAccount = new CarIQAPI("CARIQ_getBillingTerm")) {
			return billingservice.getBillingTerm(name);
		}
	}
	
	@CarIQPublicAPIMethod(description = "Update billing term", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/term/update", method = RequestMethod.PUT)
	@ResponseBody
	@Transactional
	public ResponseJson updateBillingTerm(
			@RequestBody @Valid BillingTerm_In params,  BindingResult result) {
		try (CarIQMutableAPI _updateBillingTerm = new CarIQMutableAPI("CARIQ_updateBillingTerm", params)) {
			JsonValidator.validate(result);
			return billingservice.updateBillingTerm(params);
		}
	}
	
	@CarIQPublicAPIMethod(description = "Delete billing term by id", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/term/remove/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	@Transactional
	public ResponseJson removeBillingTerm(
			@CarIQPublicAPIParameter(name = "id", description = "BillingTerm Id") @PathVariable Long id) {
		try (CarIQMutableAPI _removeBillingTerm = new CarIQMutableAPI("CARIQ_removeBillingTerm", id)) {
			return billingservice.deleteBillingTerm(id);
		}
	}
	
	/*
	 * APIs for Payment
	 */
	
	@CarIQPublicAPIMethod(description = "Create new payment", responseClass = ResponseWithIdJson.class, internal = true)
	@RequestMapping(value = "/payment/add", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public ResponseWithIdJson addPayment(@RequestBody @Valid Payment_In params,  BindingResult result) {
		try (CarIQMutableAPI _addPayment = new CarIQMutableAPI("CARIQ_addPayment", params)) {
			JsonValidator.validate(result);
			return billingservice.addPayment(params);
		}
	}
	
	@CarIQPublicAPIMethod(description = "Delete payment by id", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/payment/remove/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	@Transactional
	public ResponseJson removePayment(
			@CarIQPublicAPIParameter(name = "id", description = "Payment Id") @PathVariable Long id) {
		try (CarIQMutableAPI _removePayment = new CarIQMutableAPI("CARIQ_removePayment", id)) {
			return billingservice.deletePayment(id);
		}
	}
	
	/*
	 * APIs for Invoice
	 */
	
	@CarIQPublicAPIMethod(description = "Update Invoice Status", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/invoice/updateStatus/{id}/{status}", method = RequestMethod.PUT)
	@ResponseBody
	@Transactional
	public ResponseJson updateInvoiceStatus(
			@CarIQPublicAPIParameter(name = "id", description = "Invoice Id") @PathVariable Long id, 
			@CarIQPublicAPIParameter(name = "status", description = "Invoice Status(APPROVED, PAID, IN_REVIEW, CANCELLED)") @PathVariable String status) {
		try (CarIQMutableAPI _updateInvoiceStatus = new CarIQMutableAPI("CARIQ_updateInvoiceStatus", id)) {
			return billingservice.updateInvoiceStatus(id, status);
		}
	}
	
	@CarIQPublicAPIMethod(description = "Delete invoice by id", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/invoice/remove/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	@Transactional
	public ResponseJson removeInvoice(
			@CarIQPublicAPIParameter(name = "id", description = "Invoice Id") @PathVariable Long id) {
		try (CarIQMutableAPI _removeBillingTerm = new CarIQMutableAPI("CARIQ_removeInvoice", id)) {
			return billingservice.destroyInvoice(id);
		}
	}
}