package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import com.cariq.toolkit.coreiq.CarIQRawQuery;
import com.cariq.toolkit.coreiq.exporter.MonthlyAggregationExporterTemplate;
import com.cariq.toolkit.model.Timeline;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Month;
import com.cariq.toolkit.utils.Pair;
import com.cariq.toolkit.utils.Utils;

/**
 * The Class BusinessVerticalStatusExporter.
 */
public class VerificationStatusExporter extends MonthlyAggregationExporterTemplate {

	/** The Constant TierB. */
	public static final String EChannel = "EChannel", RetailTelesale = "RetailTelesale", Agency = "Agency", Geo = "Geo",
			Others = "Others", TierA = "tier-A", TierB = "tier-B";

	/** The Constant EXPORTER. */
	public static final String EChannelExporter = "EChannelBusinessVerticalStatus",
			RetailTelesaleExporter = "RetailTelesaleBusinessVerticalStatus",
			AgencyExporter = "AgencyBusinessVerticalStatus", GeoExporter = "GeoBusinessVerticalStatus",
			OthersExporter = "OthersBusinessVerticalStatus", TierAExporter = "TierAStatus",
			TierBExporter = "TierBStatus";

	public static final String EChannelShortName = "EChannel Business",
			RetailTelesaleShortName = "Retail Telesale Business", AgencyShortName = "Agency Business",
			GeoShortName = "Geo Business", OthersShortName = "Others Business", TierAShortName = "Tier-A Status",
			TierBShortName = "Tier-B Status";

	/** The Constant NotInterested. */
	private static final String MONTH = "Month", UnderVerification = "UnderVerification",
			DownloadedApp = "DownloadedApp", UserCreated = "UserCreated", DeviceRequested = "DeviceRequested",
			FieldAssigned = "FieldAssigned", DeviceShipped = "DeviceShipped", DeviceDelivered = "DeviceDelivered",
			DeviceOnboarded = "DeviceOnboarded", CarConnected = "CarConnected", NotContactable = "NotContactable",
			NotInterested = "NotInterested";

	/** The field. */
	private String field;

	/** The field value. */
	private String fieldValue;

	/**
	 * Instantiates a new business vertical status exporter.
	 *
	 * @param name
	 *            the name
	 * @param fieldValue
	 *            the field value
	 */
	public VerificationStatusExporter(String name, String shortName, Pair<String, String> fieldValue) {
		super(name, shortName, "The " + name);
		this.field = fieldValue.getFirst();
		this.fieldValue = fieldValue.getSecond();
	}

	/**
	 * Calculate TAG count.
	 *
	 * @param month
	 *            the month
	 * @param outJson
	 *            the out json
	 * @param tag
	 *            the tag
	 * @param state
	 *            the state
	 */
	private void calculateTAGCount(Month month, GenericJSON outJson, String tag, String state) {
		List<String> tableAliasArray = new ArrayList<String>();
		tableAliasArray.add("timeline timeline");
		tableAliasArray.add("left join proxy_object proxy on timeline.proxy=proxy.id");
		tableAliasArray.add("left join domain domain on proxy.domain=domain.id");

		LinkedHashMap<String, String> attributeAliasMap = new LinkedHashMap<String, String>();
		attributeAliasMap.put("count(timeline.id)", state + Count);
		attributeAliasMap.put("count(timeline.id)/" + Utils.getValue(Long.class, outJson, GrandTotal) + "*100",
				state + Percentage);

		CarIQRawQuery query = new CarIQRawQuery("VerificationStatusExporter.calculateTAGCount",
				Timeline.entityManager(), attributeAliasMap, tableAliasArray);
		query.addCondition("timeline.tag", "=", tag);
		query.addCondition("timeline.stage", "=", "Not Verified");
		query.addCondition("proxy.domain", "=", Utils.getValue(outJson, "domainId"));

		for (GenericJSON json : query.getResult()) {
			outJson.putAll(json);
		}
	}
}
