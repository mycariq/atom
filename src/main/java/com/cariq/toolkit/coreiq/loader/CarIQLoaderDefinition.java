package com.cariq.toolkit.coreiq.loader;

import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.utils.CarIQAttrDefinition;

public class CarIQLoaderDefinition implements Comparable<CarIQLoaderDefinition>{
	
	public static String HIDDEN_CATEGORY = "_HIDDEN_";
	public static String MISCELLANEOUS_CATEGORY = "Miscellaneous";
	
	String name;
	String description;
	List<CarIQAttrDefinition> attrDefinitions;
	String category = MISCELLANEOUS_CATEGORY;
	
	public CarIQLoaderDefinition(String name, String description, String category) {
		this(name, description, new ArrayList<CarIQAttrDefinition>(), category);
	}
	
	public CarIQLoaderDefinition(String name, String description) {
		this(name, description, new ArrayList<CarIQAttrDefinition>(), MISCELLANEOUS_CATEGORY);
	}

	public CarIQLoaderDefinition(String name, String description, List<CarIQAttrDefinition> attributes, String category) {
		this.name = name;
		this.description = description;
		this.attrDefinitions = attributes;
		this.category = category;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<CarIQAttrDefinition> getAttrDefinition() {
		return attrDefinitions;
	}
	
	public CarIQAttrDefinition getAttrDefinition(String attr) {
		return attrDefinitions.get(attrDefinitions.indexOf(attr));
	}

	public void setAttrDefinition(List<CarIQAttrDefinition> attrDefinitions) {
		this.attrDefinitions = attrDefinitions;
	}

	public CarIQLoaderDefinition addAttrDefinition(CarIQAttrDefinition attrDef) {
		attrDefinitions.add(attrDef);
		return this;
	}
	
	public CarIQLoaderDefinition removeAttrDefinition(CarIQAttrDefinition attrDef) {
		attrDefinitions.remove(attrDef);
		return this;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(CarIQLoaderDefinition other) {
		return getName().compareTo(other.getName());
	}
}
