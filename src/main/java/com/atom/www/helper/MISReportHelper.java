package com.atom.www.helper;

import java.io.StringWriter;
import java.util.Date;
import java.util.Map;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.cariq.toolkit.coreiq.exporter.ExportedData;
import com.cariq.toolkit.coreiq.exporter.ExportedData.ExporterDataJSON_In;
import com.cariq.toolkit.coreiq.exporter.ExportedData.ExporterDataJSON_Out;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.serviceimpl.async.WIHelper;
import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.CarIQZipFile;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.service.FileHandlerService;
import com.google.common.base.Strings;

@Configurable
public class MISReportHelper {
	
	/** The velocity engine. */
	@Autowired
	private VelocityEngine velocityEngine;
	
	@Autowired
	private FileHandlerService fileHandlerService;
	
	public static final String MISREPORT_VM = "velocity/MISReport.vm";
	
	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("MISReportHelper");
	
	public MISReportHelper() {
		super();
	}
	
	private Domain currentDomain = null;
	private User currentUser = null;

	public User getCurrentUser() {
		return currentUser;
	}

	public Domain getCurrentDomain() {
		return currentDomain;
	}

	private void setCurrentDomain(GenericJSON inputJson) {
		// Check if domain is provided in the input JSON
		String domainName = Utils.getValue(inputJson, "domain");
		if (Strings.isNullOrEmpty(domainName)) // Otherwise, get with logged in user
			domainName = User.getLoggedInUser().getCurrentDomainName(null);

		currentDomain = Domain.getByName(domainName);
	}
	
	private void setCurrentUser(GenericJSON inputJson) {
		// Check if username is provided in the input JSON
		String userName = Utils.getValue(inputJson, "username");	
		if (! Strings.isNullOrEmpty(userName))
			currentUser = User.getByUsername(userName);
		else
			currentUser = User.getLoggedInUser();
	}
	
	public void configureDomainUser(GenericJSON inputJSON) {
		// Set current user and domain from input JSON if given.
		setCurrentDomain(inputJSON);
		if (null == currentDomain)
			Utils.handleException("Domain does not exists.");
		setCurrentUser(inputJSON);
	}

	private ExporterDataJSON_Out persistFileDetails(String exportName, String queryType, String localFilePath, String serverFileName, 
			String description) {
		
		// file URL
		String fileUrl = WIHelper.getCarLogPath(serverFileName);
		//String fileName = serverFileName; //FilenameUtils.getBaseName(localFilePath);
		
		String inputJson = (Strings.isNullOrEmpty(description)) ? "{}" : "{description:" + description + "}";
		// persist data in DB.
		ExportedData exportedData = new ExportedData();
		ExporterDataJSON_In json = new ExporterDataJSON_In(exportName, queryType, serverFileName,
				"html", inputJson, new Date(), currentUser.getId(), serverFileName, fileUrl, ExportedData.COMPLETE,
				currentDomain.getId());
		exportedData.fromJSON(json);
		exportedData.persist();
		
		logger.debug("===> Persisted to: " + exportedData.getId() + ", url: " + exportedData.getFileUrl());
		return exportedData.toJSON();
	}
	
	public GenericJSON exportCompressedHTMLReport(String exporteName, String queryType, String localHTMLFilePath, 
			String serverFileName, String reportDetails) {
		GenericJSON outJSON = new GenericJSON();
		try {
			String zipExportFileName = localHTMLFilePath + ".zip";
			String zipServerFileName =  serverFileName + ".zip";

			// Create a ZIP file
			CarIQZipFile zipFile = new CarIQZipFile(zipExportFileName);
			zipFile.add(serverFileName, localHTMLFilePath);
			zipFile = zipFile.build();
			
			// upload the zip file
			fileHandlerService.uploadLocalFile(zipFile.getPath(), zipServerFileName, "exporter");

			ExporterDataJSON_Out exportedData = persistFileDetails(exporteName, queryType, localHTMLFilePath, 
					zipServerFileName, reportDetails);
			outJSON.add("ExportedDataID", exportedData.getId());
			outJSON.add("ExportedDataURL", exportedData.getFileUrl());
		} catch(Exception e) {
			Utils.handleException(e);
		}
		
		return outJSON;
	}

	public GenericJSON exportHTMLReport(String exporteName, String queryType, String localHTMLFilePath, String serverFileName, 
			String reportDetails) {
		GenericJSON outJSON = new GenericJSON();
		try {
			// upload the file
			fileHandlerService.uploadLocalFile(localHTMLFilePath, serverFileName, "exporter");
			//String serverFilePath = "exporter/" + fileName;
			ExporterDataJSON_Out exportedData = persistFileDetails(exporteName, queryType, localHTMLFilePath, serverFileName, reportDetails);
			outJSON.add("ExportedDataID", exportedData.getId());
			outJSON.add("ExportedDataURL", exportedData.getFileUrl());
		} catch(Exception e) {
			Utils.handleException(e);
		}
		
		return outJSON;
	}

	public String getLatestTATURL() {
		ExportedData exportedTAT = ExportedData.findExportedDataLatest(currentDomain, "TATForMISExporter", 
				null, null, "csv");
		if (null == exportedTAT || (null == exportedTAT.getFileUrl())) {
			Utils.handleException("Failed to find the latest TATReport URL");
		}
		return exportedTAT.getFileUrl();
	}
	
	public String getLatestTATURL(String exporter, String fileType) {
		ExportedData exportedData = ExportedData.getLatestExportedData(currentDomain, currentUser, exporter, fileType);
		if (null == exportedData || null == exportedData.getFileUrl())
			Utils.handleException("Exported data not found for current user");

		return exportedData.getFileUrl();
	}
	
	private String getHTMLContent(final Map<String, Object> model, String vmFile) {
		StringWriter writer = new StringWriter();
		try {
			return VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, vmFile, "UTF-8",	model);
		} catch (Exception e) {
			Utils.handleException(e);
		}

		// return template merged string.
		return writer.toString();
	}
	
	public String convertJSONToHTMLFile(String reportJSON, String fileName, String vmFile) {
		String localHTMLFilePath = null;
		
		try {
			//Render HTML file from VM
			Map<String, Object> datamap = Utils.getJSonObject(reportJSON, Map.class);
			if (null == vmFile)
				vmFile = MISREPORT_VM;
			String htmlContent = getHTMLContent(datamap, vmFile);
			
			// Create temp file path
			localHTMLFilePath = CarIQFileUtils.getTempFile(fileName);
			
			// save to html file			
			CarIQFileUtils.saveToFile(localHTMLFilePath, htmlContent);
		} catch(Exception e) {
			Utils.handleException(e);
		}
		
		return localHTMLFilePath;
	}
}
