package com.atom.www.web;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cariq.toolkit.model.ProxyObject.ProxyObject_In;
import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.service.ProxyObjectService;
import com.cariq.toolkit.service.TimelineService;
import com.cariq.toolkit.utils.CarIQMutableAPI;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.utils.Utils;

@CarIQPublicAPI(name = "ProxyObject", description = "Everything related to proxy object")
@Controller
@RequestMapping("/proxyobject")
public class ProxyObjectController {
	
	private static final String TYPE_TIMELINE = "Comment";
	
	@Autowired
	ProxyObjectService proxyObjectService;
	
	@Autowired
	TimelineService timelineService;

	@CarIQPublicAPIMethod(description = "Update proxy object", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseWithIdJson updateProxyObject(@RequestBody ProxyObject_In params) {
		try (CarIQMutableAPI _updateProxyObject = new CarIQMutableAPI("CARIQ_updateProxyObject", params)) {
			
			//Update proxy with input input params
			ResponseWithIdJson response = proxyObjectService.createOrGetProxyObject(params);
			
			//add timeline 
//			timelineService.addSimpleTimeline(params, "Task assigned to @" + params.getUsername(),
//					TYPE_TIMELINE, null, null, Utils.getCurrentUserName());
			return response;
		}
	}
}
