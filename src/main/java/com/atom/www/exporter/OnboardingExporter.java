package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterWrapper;
import com.cariq.toolkit.coreiq.exporter.CarIQWrappedExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.model.ProxyObject;
import com.cariq.toolkit.model.Timeline;
import com.cariq.toolkit.model.Timeline.Timeline_Out;
import com.cariq.toolkit.service.TaggedProxyService;
import com.cariq.toolkit.service.TimelineService;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.utils.service.CarIQRestClientService;

@Configurable
public class OnboardingExporter extends CarIQWrappedExporterTemplate {
	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("OnboardingExporter");
	
	private static final String debugStaticPolicy = "3001/144114931/00/B00";
	private static final String debugStaticPolicyState = "Not Verified";
	
	private static final String TheExporter = "OnboardingExporter";
	
	private static final String TheExporterDescription = "This is Onboarding Report";
	
	/** The Constant attributeAliasMap. */
	private static final List<String> attributeAliasMap = new ArrayList<>();

	private static final String createdAfter = "CreatedAfter";
	

	private static final String QueryType_allCustomers = "All Users";
	private static final String QueryType_nonContactable = "Not contactable";
	private static final String QueryType_nonInterested = "Not interrested";
	private static final String QueryType_feedback = "Feedback";
	private static final String QueryType_notVerified = "Not Verified";
	private static final String QueryType_userCreated = "User Created";
	private static final String QueryType_deviceRequested = "Device Requested";
	private static final String QueryType_deviceShipped = "Device Shipped";
	private static final String QueryType_deviceOnBoarded = "Device OnBoarded";
	private static final String QueryType_carConnected = "Car Connected";
	private static final String QueryType_KeywordSearch = "KeywordSearch";
	private static final String QueryType_FieldAssistRequested = "FieldAssist Requested";
	private static final String QueryType_ByPolicyNumber = "ByPolicyNumber";
	private static final String QueryType_TestUser = "test-users";
	private static final String QueryType_Duplicate_User = "duplicate-users";		
	private static final String QueryType_Cancelled_Policy = "cancelled-policies";
	private static final String QueryType_Snoozed_Policy = "snoozed-users";
	private static final String QueryType_Invalid_Policy = "invalid-users";
	
	public static final String TAG_NOT_INTERESTED = "NOT_INTERESTED";
	public static final String TAG_NOT_CONTACTABLE = "NOT_CONTACTABLE";
	private static final String TAG_FEEDBACK = "FEEDBACK";
	private static final String TAG_TEST_USER = "Test";
	private static final String TAG_DUPLICATE_USER = "DUPLICATE";
	private static final String TAG_CANCELLED_POLICY = "Cancelled";
	private static final String TAG_SNOOZE = "Snooze";
	
	private static final String nonContactableQueryTypeDescription = "Customers not contactable";
	private static final String nonInterestedQueryTypeDescription = "Customers not interreested!";
	private static final String feedbackQueryTypeDescription = "Customers who has given feedback";
	private static final String duplicateQueryTypeDescription = "All duplicate marked customers";
	private static final String testUsersQueryTypeDescription = "All test marked customers";
	private static final String cancelledQueryTypeDescription = "All cancelled marked customers";
	private static final String snoozQueryTypeDescription = "All snooze marked customers";
	private static final String invalidQueryTypeDescription = "All invalid customers";
	
	private static final String wrappedExporterName = "StagingAndInsurancePolicyDetailsExporter";
	
	private static final String wrappedExporterDescription = "This is Onboarding Report";
	
	private static final String proxyType = "InsurancePolicy";
	
	private static final String NONE = "None";
	private static final String callFilter = "callFilter";
	private static final String KEY_priority = "priority";
	
	private static final String KEY_TIMELINE = "timeline";
	private static final String KEY_POLICY_NO = "policyNumber";
	private static final String KEY_DOMAIN_ID = "domainId";
	private static final String KEY_DOMAIN_NAME = "domainName";
	private static final String KEY_TAGS = "tags";
	private static final String KEY_TAG = "tag";
	private static final String KEY_OWNER = "owner";
	private static final String OWNER_ALL = "All Users";
	private static final String OWNER_NONE = "None";
	private static final String KEY_APPOINTMENTS = "appointments";
	private static final String KEY_LATEST_DESPOSITION = "latest_desposition";
	
	private static final String COLUMN_TAG = "tag";
	private static final String COLUMN_TYPE = "type";
	
	private static final String PRIORITY_ALL = "All";
	private static final String PRIORITY_MEDIUM = "Medium";
	private static final String PRIORITY_HIGH = "High";
	private static final String PRIORITY_LOW = "Low";
	
	/********	TAT columns ***************************/
	private static final String KEY_Verified_TAT = "Verified_TAT";
	private static final String KEY_Device_Requested_TAT = "Device_Requested_TAT";
	private static final String KEY_Device_Shipped_TAT = "Device_Shipped_TAT";
	private static final String KEY_Car_Connected_TAT = "Car_Connected_TAT";

	
	/** The rest service. */
	@Autowired
	private CarIQRestClientService restService;
	
	@Autowired
	private TimelineService timelineService;
	
	@Autowired
	private TaggedProxyService taggedProxyService;

	public OnboardingExporter() {
		super(TheExporter, TheExporterDescription);

		addExportAttributes(attributeAliasMap);
		
		addQueryType(QueryType_nonContactable, nonContactableQueryTypeDescription,
				Arrays.asList(new QueryDefinitionAttribute(createdAfter, nonContactableQueryTypeDescription, Date.class)));
		
		addQueryType(QueryType_nonInterested, nonInterestedQueryTypeDescription,
				Arrays.asList(new QueryDefinitionAttribute(createdAfter, nonInterestedQueryTypeDescription, Date.class)));
		
		addQueryType(QueryType_feedback, feedbackQueryTypeDescription,
				Arrays.asList(new QueryDefinitionAttribute(createdAfter, createdAfter, Date.class)));
		
		addQueryType(QueryType_Duplicate_User, duplicateQueryTypeDescription,
				Arrays.asList(new QueryDefinitionAttribute(createdAfter, feedbackQueryTypeDescription, Date.class)));
		
		addQueryType(QueryType_TestUser, testUsersQueryTypeDescription,
				Arrays.asList(new QueryDefinitionAttribute(createdAfter, testUsersQueryTypeDescription, Date.class)));

		addQueryType(QueryType_Cancelled_Policy, cancelledQueryTypeDescription,
				Arrays.asList(new QueryDefinitionAttribute(createdAfter, cancelledQueryTypeDescription, Date.class)));

		addQueryType(QueryType_Snoozed_Policy, snoozQueryTypeDescription,
				Arrays.asList(new QueryDefinitionAttribute(createdAfter, snoozQueryTypeDescription, Date.class)));
		
		addQueryType(QueryType_Invalid_Policy, invalidQueryTypeDescription,
				Arrays.asList(new QueryDefinitionAttribute(createdAfter, invalidQueryTypeDescription, Date.class)));
		
		// Support roles
		addSupportedRoles(
				Arrays.asList(Utils.ADMIN_ROLE, Utils.USER_ROLE, Utils.ROLE_FLEET_ADMIN, Utils.ROLE_GROUP_OPERATOR));

	}

	

	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {

		try (ProfilePoint _doExport_ = ProfilePoint
				.profileAction("ProfAction_doExport_" + this.getClass().getCanonicalName())) {
			// For all local query types, createdAfter query type will be used
			// for wrapper call.
			return super.doExport(queryType, json, pageNo, pageSize);
		}
	}



	@Override
	protected CarIQExporterWrapper getWrapper() {
		CarIQExporterWrapper wrapper = new CarIQExporterWrapper(wrappedExporterName, wrappedExporterDescription);
		return wrapper;
	}


	@Override
	protected List<GenericJSON> postProcess(String queryType, GenericJSON inputJson, List<GenericJSON> outputJson) {
		try (ProfilePoint _postProcessOnboardingExporter = ProfilePoint.profileActivity("ProfActivity_postProcessOnboardingExporter")) {
			
			logger.debug("OnboaringExporter: postProcess: outputJson count: " + (outputJson != null ? outputJson.size() : 0));
			
			List<GenericJSON> policiesWithTimeline = enhanceWithTimelines(outputJson, inputJson);
			logger.debug("OnboaringExporter: postProcess: outputJson count after enhancing: " + (policiesWithTimeline != null ? policiesWithTimeline.size() : 0));
			
			List<OnboardingFilter> filters = new ArrayList<>();
			
			//For AtomX related query types we don't use filters.
			if(Utils.isValueIn(queryType, StagingAndPolicyHelper.QueryType_PreShipment,
					StagingAndPolicyHelper.QueryType_PostShipment,
					StagingAndPolicyHelper.QueryType_NotConnected,
					StagingAndPolicyHelper.QueryType_Renewal,
					StagingAndPolicyHelper.QueryType_Retrieval,
					StagingAndPolicyHelper.QueryType_PoliciesToRenew,
					StagingAndPolicyHelper.QueryType_PoliciesToRetrieve,
					StagingAndPolicyHelper.QueryType_createdAfterIncludeInvalid,
					StagingAndPolicyHelper.QueryType_ByPolicyNumber)) {
				logger.debug("Returning without filters...");
				return policiesWithTimeline;
			}
			
			filters.add(new PhoneCallFilter());
			filters.add(new PriorityFilter());
			filters.add(new OwnerFilter());
			filters.add(new NCFilter());
			filters.add(new NIFilter());
			filters.add(new TestUsersFilter());
			filters.add(new DuplicateUserFilter());
			filters.add(new CancelledPolicyFilter());
			filters.add(new FeedbackFilter());
			filters.add(new SnoozeFilter());
			filters.add(new InvalidFilter());
			
			CompositeFilter filter = new CompositeFilter(filters);
			List<GenericJSON> finalOutput = new ArrayList<>();
			for(GenericJSON json : policiesWithTimeline) {
				
				if(debugStaticPolicy.equals(json.get(KEY_POLICY_NO))) {
					logger.debug("Found debug policy");
				}
				
				GenericJSON filteredJSON = filter.filter(queryType, inputJson, json);
				if(filteredJSON != null) { 
					filteredJSON = calculateAndAppendTATs(filteredJSON);
					finalOutput.add(filteredJSON);
				}
			}
			
			return finalOutput;
		}
	}
	
	
	/**
	 * Add timeline objects to policy objects.
	 * 
	 * @param outputJson	policy objects
	 * @param inputJson 
	 * @return
	 */
	private List<GenericJSON> enhanceWithTimelines(List<GenericJSON> outputJson, GenericJSON inputJson) {
		try (ProfilePoint _enhanceWithTimelines = ProfilePoint.profileActivity("ProfActivity_enhanceWithTimelines")) {
			AtomSignatureRecordsExporter exporter = new AtomSignatureRecordsExporter();
			String policyNumbers = getPolicyNumbers(outputJson);
			if(policyNumbers == null)
				return new ArrayList<GenericJSON>();
			
			GenericJSON firstObject = outputJson.get(0);
			String domainName = (String) firstObject.get(KEY_DOMAIN_NAME);
			String owner = Utils.getValue(inputJson, "username");
			owner = owner == null ? Utils.getValue(inputJson, "owner") : owner;
			
			GenericJSON json = new GenericJSON();
			json.put("signatures", policyNumbers);
			json.put("domain", domainName);
			json.put("policy_type", "InsurancePolicy");
			json.put("owner", owner);
			
			List<GenericJSON> timelineJSONs = exporter.doExport("All", json, 1, 1000000);
			return attachTimelinesToObjects(outputJson, timelineJSONs);
		}
	}

	
	/**
	 * Attach timelines to correct object
	 * 
	 * @param outputJson
	 * @param timelineJSONs
	 * @return
	 */
	private List<GenericJSON> attachTimelinesToObjects(List<GenericJSON> outputJson, List<GenericJSON> timelineJSONs) {
		try (ProfilePoint _attachTimelinesToObjects = ProfilePoint.profileActivity("ProfActivity_attachTimelinesToObjects")) {
			List<GenericJSON> finalList = new ArrayList<>();
			
			HashMap<String, GenericJSON> map = new HashMap<>();
			for(GenericJSON json : timelineJSONs)
				map.put((String) json.get("signature"), json);		
			
			java.util.Iterator<GenericJSON> iterator = outputJson.iterator();
			while(iterator.hasNext()) {
				GenericJSON json = iterator.next();
				String signature = (String) json.get(KEY_POLICY_NO);
				GenericJSON timelineObj = map.get(signature != null ? signature.trim() : signature);
				if(timelineObj != null) {
					json.put(KEY_TIMELINE, timelineObj.get("timeline"));
					json.put(KEY_TAGS, timelineObj.get("tags"));
					ProxyObject proxyObject = Utils.getValue(ProxyObject.class, timelineObj, "proxy_object");
					json.put(KEY_OWNER, getOwner(proxyObject));
					json.put(KEY_LATEST_DESPOSITION, proxyObject.getLatestDesposition());
					json.put(KEY_APPOINTMENTS, timelineObj.get("appointments"));
				} else {
					json.put(KEY_TIMELINE, new ArrayList<>());  //for backword compatibility
					json.put(KEY_TAGS, new ArrayList<>());		//for backword compatibility
				}
				finalList.add(json);			
			}
			return finalList;
		}
	}



	/**
	 * Get comma seperated string of policy numbers.
	 * 
	 * @param outputJson	list of objects with contain policy numbers
	 * @return
	 */
	private String getPolicyNumbers(List<GenericJSON> outputJson) {
		if(outputJson.isEmpty())
			return null;
		StringBuilder builder = new StringBuilder();
		for(GenericJSON json : outputJson) {
			String policyNumber = (String) json.get("policyNumber");
			builder.append(policyNumber);
			builder.append(',');
		}
		builder.deleteCharAt(builder.lastIndexOf(","));
		return builder.toString();	
	}
	
	/**
	 * Get username from proxy object
	 * 
	 * @param proxy
	 */
	private String getOwner(ProxyObject proxy) {
		if(proxy != null)
			return proxy.getOwner() != null ? proxy.getOwner().getUsername() : NONE;
		return NONE;
	}
	
	
	/**
	 * Onboarding data filter. It filter given object based 
	 * on filter, queryType and input json
	 * 
	 * @author sagar
	 *
	 */
	public interface OnboardingFilter {
		GenericJSON filter(String queryType, GenericJSON inputJSON, GenericJSON exportedObject);
	}

	
	/**
	 * Composite filter. 
	 * @author sagar
	 */
	public class CompositeFilter implements OnboardingFilter {
		
		private List<OnboardingFilter> filters;
		public CompositeFilter(List<OnboardingFilter> filters) {
			this.filters = filters;
		}

		@Override
		public GenericJSON filter(String queryType, GenericJSON inputJSON, GenericJSON exportedObject) {
			try (ProfilePoint _filterCompositeFilter = ProfilePoint.profileActivity("ProfActivity_filterCompositeFilter")) {
				GenericJSON json = exportedObject;
				for(OnboardingFilter filter : filters) {
					json = filter.filter(queryType, inputJSON, json);
				}
				return json;
			}
		}
	}
	
	
	/**
	 * It filters object based on number of calls done to user.
	 * Call1, Call2, Call3, Call4, Call5 .....
	 * @author sagar
	 */
	public class PhoneCallFilter implements OnboardingFilter {

		@Override
		public GenericJSON filter(String queryType, GenericJSON inputJSON, GenericJSON exportedObject) {
			if(exportedObject == null)
				return exportedObject;
			
			Integer callCountInput = (Integer) inputJSON.get(callFilter);
			@SuppressWarnings("unchecked")
			List<Timeline_Out> timeline = (List<Timeline_Out>) exportedObject.get(KEY_TIMELINE);
			
			int callCount = Timeline.getCallCountFromTimeline(timeline);
			
			//If input is not valid return true
			if(callCountInput == null)
				return exportedObject;
			
			//-1 call count means all calls
			if(callCountInput == -1)
				return exportedObject;
			
			if(callCount == callCountInput) {
				return exportedObject;
			}		
			return null;
		}		
	}
	
	
	/**
	 * It filters object based on prioirty of object.
	 * LOW, MEDIUM, NORMAL, ALL.
	 * @author sagar
	 */
	public class PriorityFilter implements OnboardingFilter {
		@Override
		public GenericJSON filter(String queryType, GenericJSON inputJSON, GenericJSON exportedObject) {
			if(exportedObject == null)
				return null;
			
			String priorityInput = (String) inputJSON.get(KEY_priority);
			String priority = PRIORITY_ALL;
				
			if(queryType.equalsIgnoreCase(QueryType_notVerified)) {
				priority = getPriority((String) exportedObject.get("StagingCreatedOn"));
			} else if(queryType.equalsIgnoreCase(QueryType_userCreated)){
				priority = getPriority((String) exportedObject.get("Policy_CreationTime"));
			} else if(queryType.equalsIgnoreCase(QueryType_deviceRequested)) {
				priority = getPriority((String) exportedObject.get("Shipped_Or_Delivered_On"));
			} else if(queryType.equalsIgnoreCase(QueryType_FieldAssistRequested)) {
				priority = getPriority((String) exportedObject.get("Shipped_Or_Delivered_On"));
			}else if(queryType.equalsIgnoreCase(QueryType_deviceShipped)) {
				priority = getPriority((String) exportedObject.get("Shipped_Or_Delivered_On"));
			} else if(queryType.equalsIgnoreCase(QueryType_deviceOnBoarded)) {
				priority = getPriority((String) exportedObject.get("Device_OnboardedOn"));
			} else if(queryType.equalsIgnoreCase(QueryType_carConnected)) {
				Double carTravelled = (Double) exportedObject.get("Car_Distance_Since_Connected");
				if(carTravelled == null) {
					priority = PRIORITY_LOW;
				} else if(carTravelled > 50 && carTravelled < 100) {
					priority = PRIORITY_LOW;
				} else if(carTravelled > 100 && carTravelled < 200) {
					priority = PRIORITY_MEDIUM;
				} else if(carTravelled > 200) {
					priority = PRIORITY_HIGH;
				}
			}
			//If input priority input is not available then return object with priority
			if(priorityInput == null || priorityInput.equals(PRIORITY_ALL)) {
				exportedObject.put(KEY_priority, priority);
				return exportedObject;
			}
			//If priority not match then return null
			if(!priorityInput.equals(priority))
				return null;
			return exportedObject;
		}		
		/**
		 * Get priority for given date
		 * 
		 * @param date
		 * @return
		 */
		private String getPriority(String dateStr) {
			Date date = Utils.getDate(dateStr);
			if(date == null) 
				return PRIORITY_HIGH;
			Date today = new Date();
			int diff = Utils.getDiffInDays(date, today);
			if(diff < 3) 
				return PRIORITY_LOW;
			if(diff > 3 && diff < 6)
				return PRIORITY_MEDIUM;
			return PRIORITY_HIGH;
		}
	}
	
	
	/**
	 * It filters object based owner of object. 
	 * Names of owner or Everyone as owner
	 * @author sagar
	 */
	public class OwnerFilter implements OnboardingFilter {
		@Override
		public GenericJSON filter(String queryType, GenericJSON inputJSON, GenericJSON exportedObject) {
			if(exportedObject == null)
				return null;
			String owner = Utils.getValue(exportedObject, KEY_OWNER);
			String ownerFilter = (String) inputJSON.get(KEY_OWNER);
			
			if(ownerFilter == null) 
				return exportedObject;
			
			//Owner not found! and requested owner of type return this object.
			if(owner == null && ownerFilter.equalsIgnoreCase(OWNER_NONE))
				return exportedObject;
			
			//If input owner is not available then return object by assuming filter as everyone
			if(ownerFilter == null)
				return exportedObject;
			
			//If requested owner is everyone then return this object
			if(ownerFilter.equalsIgnoreCase(OWNER_ALL))
				return exportedObject;
			
			//owner matched, return object
			if(ownerFilter.equals(owner))
				return exportedObject;
			return null;
		}
	}

	
	/**
	 * Tag filter
	 * @author sagar
	 *
	 */
	public abstract class TagFilter implements OnboardingFilter {
		@SuppressWarnings("unchecked")
		@Override
		public GenericJSON filter(String queryType, GenericJSON inputJSON, GenericJSON exportedObject) {
			if(exportedObject == null)
				return null;
			
			if(queryType.equalsIgnoreCase(QueryType_KeywordSearch)
					|| queryType.equalsIgnoreCase(QueryType_ByPolicyNumber)
					|| queryType.equalsIgnoreCase(createdAfter))
				return exportedObject;
			
			List<Timeline_Out> timelines = (List<Timeline_Out>) exportedObject.get(KEY_TIMELINE);
			boolean isNotInterested = doesTimelineCommentObjectWithType(timelines, getTagName(), COLUMN_TAG);

			//if marked as given tag then remove object from every other filter.
			if(isNotInterested && !queryType.equals(getTagQueryType())) {
				return null;
			} else if(isNotInterested && queryType.equals(getTagQueryType())) {
				//if marked as given tag then add object only in non-interested filter.
				exportedObject.put(KEY_TAG, getTagName());
				return exportedObject;				
			} else if(!queryType.equals(getTagQueryType())) {
				//this object is of given tag name type and should be added into every other list except
				//not-interested list
				return exportedObject;
			}
			return null;
		}
		
		//Get tag name
		protected abstract String getTagName();
		
		//get query type supported by tag 
		protected abstract String getTagQueryType();
	}

	/**
	 * Non contactable filter. It filter object which is non contactable based
	 * on timeline
	 * @author sagar
	 */
	public class NCFilter extends TagFilter {
		@Override
		protected String getTagName() {
			return TAG_NOT_CONTACTABLE;
		}
		@Override
		protected String getTagQueryType() {
			return QueryType_nonContactable;
		}
	}
	
	/**
	 * Non interested filter. It filter object which is non interested based
	 * on timeline
	 * @author sagar
	 */
	public class NIFilter extends TagFilter {
		@Override
		protected String getTagName() {
			return TAG_NOT_INTERESTED;
		}
		@Override
		protected String getTagQueryType() {
			return QueryType_nonInterested;
		}
	}

	/**
	 * Test user filter
	 * @author sagar
	 */
	private class TestUsersFilter extends TagFilter {
		@Override
		protected String getTagName() {
			return TAG_TEST_USER;
		}
		@Override
		protected String getTagQueryType() {
			return QueryType_TestUser;
		}
	}
	
	
	/**
	 * Duplicate user filter
	 * @author sagar
	 */
	private class DuplicateUserFilter extends TagFilter {
		@Override
		protected String getTagName() {
			return TAG_DUPLICATE_USER;
		}
		@Override
		protected String getTagQueryType() {
			return QueryType_Duplicate_User;
		}		
	}
	
	
	/**
	 * Duplicate user filter
	 * @author sagar
	 */
	private class CancelledPolicyFilter extends TagFilter {
		@Override
		protected String getTagName() {
			return TAG_CANCELLED_POLICY;
		}
		@Override
		protected String getTagQueryType() {
			return QueryType_Cancelled_Policy;
		}		
	}
	
	
	
	/**
	 * Find timeline contain object with given type of not
	 * 
	 * @param timeline		Timeline to find  object with type
	 * @param object		object
	 * @param objectType	timeline type
	 * @return
	 */
	public static boolean doesTimelineCommentObjectWithType(List<Timeline_Out> timeline, String object, String objectType) {
		if (timeline == null || timeline.isEmpty())
			return false;
		
		Timeline_Out timelineOut = StagingAndPolicyHelper.getLatestTimelineWithTag(timeline);
		if(objectType.equals(COLUMN_TAG) && 
				(timelineOut != null && object.equalsIgnoreCase(timelineOut.getTag())))
			return true;

		// If timeline tag is equal to object then return true;
		for (Timeline_Out singltTimeline : timeline) {
//			logger.debug("Timeline type  : " + singltTimeline.getType() + ", signature : " + singltTimeline.getProxyObject().getSignature()
//					+ ",  object : " + object + ", objectType : " + objectType);
			
			if (singltTimeline.getType() != null && singltTimeline.getType().equalsIgnoreCase(object) && objectType.equals(COLUMN_TYPE)) {
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * Filter object based on feedback. Whether given object has feedback or not
	 * based on timeline of object 
	 * @author sagar
	 */
	public class FeedbackFilter extends TagFilter {
		@Override
		protected String getTagName() {
			return TAG_FEEDBACK;
		}
		@Override
		protected String getTagQueryType() {
			return QueryType_feedback;
		}
	}

	/**
	 * Snooz filter
	 * 
	 * @author sagar
	 */
	public class SnoozeFilter extends TagFilter {
		@Override
		protected String getTagName() {
			return TAG_SNOOZE;
		}

		@Override
		protected String getTagQueryType() {
			return QueryType_Snoozed_Policy;
		}
	}
	
	/***
	 * Filter out invalid policies
	 * @author sagar
	 */
	public class InvalidFilter implements OnboardingFilter {
		@Override
		public GenericJSON filter(String queryType, GenericJSON inputJSON, GenericJSON exportedObject) {
			if(exportedObject == null)
				return null;

			if(queryType.equalsIgnoreCase(QueryType_KeywordSearch)
					|| queryType.equalsIgnoreCase(QueryType_ByPolicyNumber)
					|| queryType.equalsIgnoreCase(createdAfter)
					|| queryType.equalsIgnoreCase(QueryType_allCustomers))
				return exportedObject;
			
			//if query type is invalid and policy is invalid then return order
			if(queryType.equalsIgnoreCase(QueryType_Invalid_Policy)
					&& StagingAndPolicyHelper.POLICY_STATE_INVALID
					.equalsIgnoreCase(StagingAndPolicyHelper.getPolicyState(exportedObject)))
				return exportedObject;
			//if query type is not invalid and policy is invalid then $remove$ it from every other bucket
			else if(!queryType.equalsIgnoreCase(QueryType_Invalid_Policy)
					&& StagingAndPolicyHelper.POLICY_STATE_INVALID
					.equalsIgnoreCase(StagingAndPolicyHelper.getPolicyState(exportedObject)))
				return null;
			else if(queryType.equalsIgnoreCase(QueryType_Invalid_Policy))
				return null;
			//query type is other and policy is not invalid
			return exportedObject;
		}		
	}
	
	
	/**
	 * Calculate and append TAT for given policy
	 * 
	 * @param json		policy to append TAT data.
	 * @return			improved policy json
	 */
	private GenericJSON calculateAndAppendTATs(GenericJSON json) {
		
		String entryDate = (String) json.get("StagingCreatedOn");
		
		json.put(KEY_Verified_TAT, StagingAndPolicyHelper.calculateTAT(entryDate, (String) json.get("Policy_CreationTime")));
		json.put(KEY_Device_Requested_TAT, StagingAndPolicyHelper.calculateTAT(entryDate, (String) StagingAndPolicyHelper.getDeviceRequestedOnDate(json)));
		json.put(KEY_Device_Shipped_TAT, StagingAndPolicyHelper.calculateTAT(entryDate,
				(String) StagingAndPolicyHelper.getDeviceShippedOnDate(json)));
		json.put(KEY_Car_Connected_TAT, StagingAndPolicyHelper.calculateTAT(entryDate, (String) json.get("Car_FirstSeenOn")));
		
		return json;
	}
}