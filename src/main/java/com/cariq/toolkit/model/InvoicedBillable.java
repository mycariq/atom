package com.cariq.toolkit.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.InvoicedBillable.InvoicedBillable_In;
import com.cariq.toolkit.model.InvoicedBillable.InvoicedBillable_Out;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.GenericJsonable;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;
import com.cariq.toolkit.utils.Utils;

/**
 * This class implements "Invoiced billable"
 * This is the intersection model mapping invoiceUnits and billables. InvoiceUnit includes list of unique billables. 
 * While one billable can participate into multiple invoice units.
 * Each "InvoicedBillable" is unique "billable" charged "x amount" only once under "InvoiceUnit" (specific billing term) 
 * @author amita
 *
 */

@Entity
@Configurable
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "its_invoice_unit", "its_billable"}))
public class InvoicedBillable extends PersistentObject<InvoicedBillable> implements JSONable<InvoicedBillable_In, InvoicedBillable_Out, IdJSON>, GenericJsonable {

	// SECTION 1 -Code for Persistence
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@Version
	Integer version;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new InvoicedBillable().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null)
			this.entityManager = entityManager();
		return entityManager;
	}

	/**
	 * @param id
	 * @return
	 */
	protected static InvoicedBillable find(Long id) {
		return entityManager().find(InvoicedBillable.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public InvoicedBillable findById(Long id) {
		return find(id);
	}

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/**
	 * Model properties
	 */
	@NotNull
	@ManyToOne
	InvoiceUnit itsInvoiceUnit;

	@NotNull
	@ManyToOne
	Billable itsBillable;
	
	Long amount;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	// Default constructor is a must!
	public InvoicedBillable() {
		super();
	}

	/**
	 * 
	 * @param itsInvoice
	 * @param itsBillable
	 * @param createdOn
	 * @param modifiedOn
	 */
	public InvoicedBillable(InvoiceUnit itsInvoiceUnit, Billable itsBillable, Long amount, Date createdOn, Date modifiedOn) {
		super();
		this.itsInvoiceUnit = itsInvoiceUnit;
		this.itsBillable = itsBillable;
		this.amount = amount;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
	}

	/**
	 * Getters Setters
	 */	
	
	public InvoiceUnit getItsInvoiceUnit() {
		return itsInvoiceUnit;
	}

	public void setItsInvoiceUnit(InvoiceUnit itsInvoiceUnit) {
		this.itsInvoiceUnit = itsInvoiceUnit;
	}

	public Billable getItsBillable() {
		return itsBillable;
	}

	public void setItsBillable(Billable itsBillable) {
		this.itsBillable = itsBillable;
	}

	public Long getAmount() {
		return this.amount;
	}
	
	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------

	// Implementation of toString and hashCode and equals are advisable for all
	// Model classes
	@Override
	public String toString() {
		return "InvoicedBillable [id=" + id + ", version=" + version + ", itsInvoiceUnit=" + itsInvoiceUnit.toString() + ", itsBillable=" + itsBillable.toString()
				+ ", amount=" + amount + ", createdOn=" + createdOn + ", modifiedOn=" + modifiedOn + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((itsInvoiceUnit == null) ? 0 : itsInvoiceUnit.getId().hashCode());
		result = prime * result + ((itsBillable == null) ? 0 : itsBillable.getId().hashCode());
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvoicedBillable other = (InvoicedBillable) obj;
		if (itsInvoiceUnit == null) {
			if (other.itsInvoiceUnit != null)
				return false;
		} else if (!itsInvoiceUnit.equals(other.itsInvoiceUnit))
			return false;
		if (itsBillable == null) {
			if (other.itsBillable != null)
				return false;
		} else if (!itsBillable.equals(other.itsBillable))
			return false;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		return true;
	}

	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API
	// interaction
	
	// In Class (for construction)
	public static class InvoicedBillable_In {
		Long itsInvoiceUnitId;
		Long itsBillableId;
		Long amount;
		Integer thresholdConditionMatched;

		public InvoicedBillable_In() {
			super();
		}

		/**
		 * 
		 * @param itsInvoiceUnitId
		 * @param itsBillableId
		 * @param amount
		 */
		public InvoicedBillable_In(Long itsInvoiceUnitId, Long itsBillableId, Long amount) {
			super();
			this.itsInvoiceUnitId = itsInvoiceUnitId;
			this.itsBillableId = itsBillableId;
			this.amount = amount;
		}

		public Long getItsInvoiceUnitId() {
			return itsInvoiceUnitId;
		}

		public void setItsInvoiceId(Long itsInvoiceUnitId) {
			this.itsInvoiceUnitId = itsInvoiceUnitId;
		}

		public Long getItsBillableId() {
			return itsBillableId;
		}

		public void setItsBillableId(Long itsBillableId) {
			this.itsBillableId = itsBillableId;
		}
		
		public Long getAmount() {
			return amount;
		}

		public void setAmount(Long amount) {
			this.amount = amount;
		}
		
		public Integer getThresholdConditionMatched() {
			return thresholdConditionMatched;
		}

		public void setThresholdConditionMatched(Integer thresholdConditionMatched) {
			this.thresholdConditionMatched = thresholdConditionMatched;
		}
	}

	// Out class for output
	public static class InvoicedBillable_Out extends InvoicedBillable_In {
		Long id;
		String createdOn;
		String modifiedOn;

		public InvoicedBillable_Out() {
			super();
		}

		public InvoicedBillable_Out(Long itsInvoiceUnitId, Long itsBillableId, Long amount, String createdOn, String modifiedOn, Long id) {
			super(itsInvoiceUnitId, itsBillableId, amount);
			this.id = id;
			this.createdOn = createdOn;
			this.modifiedOn = modifiedOn;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toJSON()
	 */
	@Override
	public InvoicedBillable_Out toJSON() {
		return new InvoicedBillable_Out(itsInvoiceUnit.getId(), itsBillable.getId(), amount, 
				createdOn.toString(), modifiedOn.toString(), id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#fromJSON(java.lang.Object)
	 */
	@Override
	public void fromJSON(InvoicedBillable_In json) {
		this.itsInvoiceUnit =  json.getItsInvoiceUnitId() != null ? InvoiceUnit.find(json.getItsInvoiceUnitId()) : null;
		this.itsBillable =  json.getItsBillableId() != null ? Billable.find(json.getItsBillableId()) : null;
		this.amount =  json.getAmount();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}

	@Override
	public GenericJSON toGenericJSON() {
		return GenericJSON.build("itsInvoiceUnitId", itsInvoiceUnit.getId(), "itsBillable", itsBillable.toGenericJSON(), "Amount", amount, "CreatedOn", createdOn, "ModifiedOn", modifiedOn, "Id", id);
	}
	
	@Override
	public void fromGenricJSON(GenericJSON json) {
		Utils.notImplementedException("InvoicedBillable.fromGenricJSON");
	}
	
	// ---------------------------------------------
	// SECTION 5 - Query and Class specific finders and static utility methods
	// ---------------------------------------------
	public static InvoicedBillable findObjectById(Long id) {
		return find(id);
	}
	
	public static List<InvoicedBillable> getAllObjects(int pageNo, int pageSize) {
		CarIQSimpleQuery<InvoicedBillable> qry = new CarIQSimpleQuery<InvoicedBillable>("getAllObjects", entityManager(), InvoicedBillable.class);
		qry.setPaging(pageNo, pageSize);

		return qry.getResultList();
	}

	public static List<InvoicedBillable> getByInvoiceUnit(InvoiceUnit invoiceUnit) {
		CarIQSimpleQuery<InvoicedBillable> query = new CarIQSimpleQuery<InvoicedBillable>("getByInvoiceUnit", entityManager(), InvoicedBillable.class);
		query.addCondition("itsInvoiceUnit", CarIQSimpleQuery.EQ, invoiceUnit);

		return query.getResultList();
	}
	
	public static List<InvoicedBillable> getByInvoiceUnitAndThresholdConditionMatched(InvoiceUnit invoiceUnit, Integer thresholdConditionMatched) {
		CarIQSimpleQuery<InvoicedBillable> query = new CarIQSimpleQuery<InvoicedBillable>("getByInvoiceUnit", entityManager(), InvoicedBillable.class);
		query.addCondition("itsInvoiceUnit", CarIQSimpleQuery.EQ, invoiceUnit);
		query.addCondition("thresholdConditionMatched", CarIQSimpleQuery.EQ, thresholdConditionMatched);

		return query.getResultList();
	}
	
	public static List<InvoicedBillable> getByBillable(Billable billable) {
		CarIQSimpleQuery<InvoicedBillable> query = new CarIQSimpleQuery<InvoicedBillable>("getByBillable", entityManager(), InvoicedBillable.class);
		query.addCondition("itsBillable", CarIQSimpleQuery.EQ, billable);

		return query.getResultList();
	}
	
	public static InvoicedBillable getByIuAndBillable(InvoiceUnit invoiceUnit, Billable billable) {
		CarIQSimpleQuery<InvoicedBillable> query = new CarIQSimpleQuery<InvoicedBillable>("getByIuAndBillable", entityManager(), InvoicedBillable.class);
		query.addCondition("itsBillable", CarIQSimpleQuery.EQ, billable);
		query.addCondition("itsInvoiceUnit", CarIQSimpleQuery.EQ, invoiceUnit);
		
		if (query.getResultList().isEmpty())
			return null;
		return query.getSingleResult();
	}
}