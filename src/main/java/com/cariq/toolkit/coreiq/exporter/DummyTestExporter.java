package com.cariq.toolkit.coreiq.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

public class DummyTestExporter {
	public static class DummyTATCreator extends CarIQExporterTemplate {
		public static final String TheExporter = "DummyTATCreator";

		/**
		 * @param name
		 * @param description
		 */
		public DummyTATCreator() {
			super(TheExporter, TheExporter);
			super.addExportAttributes(Arrays.asList("a", "b", "createdOn"));
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.cariq.www.coreiq.exporter.CarIQExporterTemplate#doExport(java.
		 * lang.String, com.cariq.www.utils.GenericJSON, int, int)
		 */
		@Override
		protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
			if (pageNo > 1)
				return null;
			
			List<GenericJSON> jsonList = new ArrayList<GenericJSON>();
			for (int i = 0; i < 10; i++) {
				jsonList.add(GenericJSON.build("a", i+1, "b", Utils.getRandomInRange(1, 50), "c", Utils.getDateTimeString(new Date(), null)));
			}

			return jsonList;
		}

	}

	public static class DummyTATProcessor extends CarIQExporterTemplate {
		public static final String TheExporter = "DummyTATProcessor";
		/**
		 * @param name
		 * @param description
		 */
		public DummyTATProcessor() {
			super(TheExporter, TheExporter);
			super.addExportAttributes(Arrays.asList("p", "q", "createdOn"));
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.cariq.www.coreiq.exporter.CarIQExporterTemplate#doExport(java.
		 * lang.String, com.cariq.www.utils.GenericJSON, int, int)
		 */
		@Override
		protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
			if (pageNo > 1)
				return null;
			
			List<GenericJSON> jsonList = new ArrayList<GenericJSON>();
			for (int i = 0; i < 50; i++) {
				jsonList.add(GenericJSON.build("p", i+1, "q", Utils.getRandomInRange(40, 100), "createdOn", Utils.getDateTimeString(new Date(), null)));
			}

			return jsonList;
		}

	}

}
