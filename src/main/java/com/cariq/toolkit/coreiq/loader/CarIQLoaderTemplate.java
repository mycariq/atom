package com.cariq.toolkit.coreiq.loader;

import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.utils.CarIQAttrDefinition;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.profile.ProfilePoint;

public abstract class CarIQLoaderTemplate implements CarIQLoader {
	CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("CarIQLoader");

	CarIQLoaderDefinition loaderDefinition;
	
	abstract protected void doLoad(GenericJSON json, CarIQLoaderContext context);
	
	public CarIQLoaderTemplate(String name, String description) {
		super();
		loaderDefinition = new CarIQLoaderDefinition(name, description);
	}
	
	public CarIQLoaderTemplate(String name, String description, String category) {
		super();
		loaderDefinition = new CarIQLoaderDefinition(name, description, category);
	}
	
	@Override
	public String getName() {
		return loaderDefinition.getName();
	}

	@Override
	public String getDescription() {
		return loaderDefinition.getDescription();
	}

	protected final void addAttribute(String name, String description, boolean isOptional) {
		loaderDefinition.addAttrDefinition(new CarIQAttrDefinition(name, description, isOptional));
	}
	
	protected final void addAttribute(String name, String description) {
		addAttribute(new CarIQAttrDefinition(name, description, true));
	}
	
	protected final  void addAttribute(CarIQAttrDefinition attr) {
		loaderDefinition.addAttrDefinition(attr);
	}
	
	protected final  void removeAttribute(String attr) {
		CarIQAttrDefinition attrDef = loaderDefinition.getAttrDefinition(attr);
		loaderDefinition.removeAttrDefinition(attrDef);
	}
	
	protected final  void removeAttribute(CarIQAttrDefinition attr) {
		loaderDefinition.removeAttrDefinition(attr);
	}
	
	@Override
	public CarIQLoaderDefinition getLoaderDefinition() {
		return loaderDefinition;
	}

	@Override
	public List<CarIQAttrDefinition> getAttributes() {
		return loaderDefinition.getAttrDefinition();
	}
	
	public void clearAttributes() {
	 loaderDefinition.setAttrDefinition(new ArrayList<CarIQAttrDefinition>());
	}

	@Override
	public void load(GenericJSON json, CarIQLoaderContext context) {
		try (ProfilePoint _Loader_load = ProfilePoint.profileAction("ProfAction_Loader_load_" + this.getClass().getName())) {
			logger.debug("Loader: " + this.getClass().getName() + " Input: " + json.toString());
			
			CarIQAttrDefinition.validateJSON(json, getAttributes());
			doLoad(json, context);
		}
	}
	
}
