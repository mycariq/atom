package com.cariq.toolkit.utils.mistools;

import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.utils.KeyValuePair;

/**
 * SectionGroup: SectionDefinition1, SectionDefinition2, ....
 * Tier: null, Tier-A, Tier-B
 * Vertical: E Channel, Others, Agency
 * Stage: Car Connected,Device OnBoarded,Device RTO,Device Delivered,Device Shipped,Device Requested,Downloaded App,User Created,Under verification
 * Subscription Status: SubscriptionActive,SubscriptionCancelled,SubscriptionDeactivated,SubscriptionExpired,SubscriptionRenewed
 * Policy State: Active,GOT_DEVICE_BACK,READY_TO_RETRIEVE,NOT_CONTACTABLE,NOT_INTERESTED,Invalid
 * TAT: 0-7 DAYS,8-14 DAYS,15-21 DAYS,22-30 DAYS,> 30 DAYS
 * Policy Status: INVALID_CARNOT,INVALID_TRANSACTION,NOT_VERIFIED,NOT_VERIFIED_NC,NOT_VERIFIED_NI,PROCESSED,null

 * @author hrishi
 *
 */
public class MISSectionDefinition {
	MISSectionGroup itsGroup;

	String label;
	String value;
	public MISSectionDefinition(MISSectionGroup itsGroup, String label) {
		this(itsGroup, label, label);
	}

	public MISSectionDefinition(MISSectionGroup itsGroup, String label, String value) {
		super();
		this.itsGroup = itsGroup;
		this.label = label;
		this.value = value;
	}
	
	public MISSectionGroup getItsGroup() {
		return itsGroup;
	}
	
	public String getLabel() {
		return label;
	}
	
	public String getValue() {
		return value;
	}

	// All Definitions together for Arguments
	public static List<KeyValuePair> collectKeyValues(List<MISSectionDefinition> applicableSectionDefs) {
		List<KeyValuePair> retVal = new ArrayList<KeyValuePair>();
		
		// Collect all Key Value pairs
		for (MISSectionDefinition misSectionDefinition : applicableSectionDefs) {
			retVal.add(misSectionDefinition.getKeyValuePair());
		}
		
		return retVal;
	}

	public KeyValuePair getKeyValuePair() {
		String key = getItsGroup().getKey();
		String val = getValue();
		
		return new KeyValuePair(key, val);
	}
	
	

}
