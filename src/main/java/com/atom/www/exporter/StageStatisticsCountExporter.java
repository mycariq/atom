package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import com.cariq.toolkit.coreiq.CarIQRawQuery;
import com.cariq.toolkit.coreiq.CarIQSimpleAggregationQuery;
import com.cariq.toolkit.coreiq.CarIQSimpleAggregationQuery.AggregationType;
import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.StageStatistics;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * The Class StageStatisticsCountExporter.
 *
 * @author nitin
 */
public class StageStatisticsCountExporter extends CarIQExporterTemplate {

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("StageStatisticsCountExporter");

	/** The Constant EXPORTER. */
	private static final String EXPORTER = "StageStatisticsCountExporter";

	/** The Constant All. */
	public static final String ALL = "All", ALL_CUSTOMERS = "AllCustomers", DOMAIN = "domain", CATEGORY = "category",
			TOTAL_COUNT = "TotalCount", ENTRY_DATE = "entry_date", STAGE = "stage", STATE = "state",
			FROM_DATE = "fromDate", TO_DATE = "toDate", BY_MONTH = "ByMonth", CATEGORY_TYPE = "categoryType",
			MONTH = "Month", YEAR = "Year", COUNT = "Count", BY_STAGING_DATES = "ByStagingDates",
			DEVICE_ONBOARDED_CATEGORY = "Device OnBoarded", CAR_CONNECTED_CATEGORY = "Car Connected", BY_MONTH_CUMULATIVE = "ByMonthCumulative";

	// Policy stages
	public static final String DEVICE_DELIVERED = "DevicDelivered";
	public static final String DEVICE_ONBOARDED = "DevicOnboarded";
	public static final String DEVICE_REQUESTED = "DevicRequested";
	public static final String DEVICE_SHIPPED = "DevicShipped";
	public static final String NOT_INTERESTED = "NotInterested";
	public static final String NOT_CONTACTABLE = "NotContactable";
	public static final String CAR_CONNECTED = "CarConnected";
	public static final String VERIFIED = "Verified";

	/** The Constant attributeAliasMap. */
	static final LinkedHashMap<String, String> attributeAliasMap = new LinkedHashMap<String, String>();

	/** The Constant tableAliasMap. */
	static final LinkedHashMap<String, String> tableAliasMap = new LinkedHashMap<String, String>();

	static {
		attributeAliasMap.put("MONTH(ss.entry_date)", MONTH);
		attributeAliasMap.put("YEAR(ss.entry_date)", YEAR);
		attributeAliasMap.put("count(*)", COUNT);
		tableAliasMap.put("stage_statistics", "ss");
	}

	/**
	 * 
	 * Instantiates a new stage statistics count exporter.
	 */
	public StageStatisticsCountExporter() {
		super(EXPORTER, "Stage Statistics Count Exporter");

		// Query definitions
		addQueryType(TOTAL_COUNT,
				"Exports total count of policies of specific category and issued/entered in the given date range",
				Arrays.asList(new QueryDefinitionAttribute(CATEGORY_TYPE, "Type - Stage or State"),
						new QueryDefinitionAttribute(CATEGORY,
								"Category - AllCustomers, NOT_INTERESTED , NOT_CONTACTABLE, Car Connected"),
						new QueryDefinitionAttribute(DOMAIN,
								"Domain names - IL-Assist, Drivesmart, Driven-Dev, Driven, TAGIC"),
						new QueryDefinitionAttribute(FROM_DATE, "From date in yyyy-MM-dd [HH:mm:ss] format",
								Date.class),
						new QueryDefinitionAttribute(TO_DATE, "To date in yyyy-MM-dd [HH:mm:ss] format", Date.class)));

		addQueryType(BY_MONTH,
				"Exports count of policies of specific category that are issued/entered across months in the given date range",
				Arrays.asList(new QueryDefinitionAttribute(CATEGORY_TYPE, "Type - Stage or State"),
						new QueryDefinitionAttribute(CATEGORY, CATEGORY),
						new QueryDefinitionAttribute(DOMAIN,
								"Domain names - IL-Assist, Drivesmart, Driven-Dev, Driven, TAGIC"),
						new QueryDefinitionAttribute(FROM_DATE, "From date in yyyy-MM-dd [HH:mm:ss] format",
								Date.class),
						new QueryDefinitionAttribute(TO_DATE, "To date in yyyy-MM-dd [HH:mm:ss] format", Date.class)));
		
		addQueryType(BY_MONTH_CUMULATIVE,
				"Exports cumulative count of policies of specific category that are issued/entered across months in the given date range",
				Arrays.asList(new QueryDefinitionAttribute(CATEGORY_TYPE, "Type - Stage or State"),
						new QueryDefinitionAttribute(CATEGORY, CATEGORY),
						new QueryDefinitionAttribute(DOMAIN,
								"Domain names - IL-Assist, Drivesmart, Driven-Dev, Driven, TAGIC"),
						new QueryDefinitionAttribute(FROM_DATE, "From date in yyyy-MM-dd [HH:mm:ss] format",
								Date.class),
						new QueryDefinitionAttribute(TO_DATE, "To date in yyyy-MM-dd [HH:mm:ss] format", Date.class)));

		addQueryType(BY_STAGING_DATES,
				"Exports count of policies at specific stage across months in the given date range",
				Arrays.asList(new QueryDefinitionAttribute(STAGE,
						"Stages - DevicDelivered,DevicOnboarded,DevicRequested,DevicShipped,NotInterested,NotContactable,Verified"),
						new QueryDefinitionAttribute(DOMAIN,
								"Domain names - IL-Assist, Drivesmart, Driven-Dev, Driven, TAGIC"),
						new QueryDefinitionAttribute(FROM_DATE, "From date in yyyy-MM-dd [HH:mm:ss] format",
								Date.class),
						new QueryDefinitionAttribute(TO_DATE, "To date in yyyy-MM-dd [HH:mm:ss] format", Date.class)));

		// Support roles
		addSupportedRoles(Arrays.asList(Utils.ADMIN_ROLE, Utils.USER_ROLE));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate#doExport(java.lang.
	 * String, com.cariq.toolkit.utils.GenericJSON, int, int)
	 */
	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		logger.debug("doExport - queryType : " + queryType + " pageNo : " + pageNo + " pageSize : " + pageSize
				+ " json :" + json);
		// validate query types
		if (!TOTAL_COUNT.equalsIgnoreCase(queryType) && !BY_MONTH.equalsIgnoreCase(queryType)
				&& !BY_STAGING_DATES.equalsIgnoreCase(queryType) && !BY_MONTH_CUMULATIVE.equalsIgnoreCase(queryType) )
			Utils.handleException("Invalid query type :" + queryType);

		return getCount(queryType, json, pageNo, pageSize);
	}

	/**
	 * Gets the count.
	 *
	 * @param queryType
	 *            the query type
	 * @param json
	 *            the json
	 * @param pageNo
	 *            the page no
	 * @param pageSize
	 *            the page size
	 * @return the count
	 */
	private List<GenericJSON> getCount(String queryType, GenericJSON json, int pageNo, int pageSize) {
		logger.debug("getCount - queryType : " + queryType + " pageNo : " + pageNo + " pageSize : " + pageSize
				+ " json :" + json);
		if (pageNo > 1)
			return null;
		CarIQSimpleQuery<StageStatistics> query = new CarIQSimpleQuery<StageStatistics>(EXPORTER + "getCount",
				StageStatistics.entityManager(), StageStatistics.class);

		String DomainName = Utils.getValue(String.class, json, DOMAIN);
		Domain domain = Domain.getByName(DomainName);
		if (null == domain)
			Utils.handleException("Domain does not exists.");

		Date fromDate = Utils.getValue(Date.class, json, FROM_DATE);
		if (null == fromDate)
			Utils.handleException("From date should be in yyyy-MM-dd [HH:mm:ss] format.");

		Date toDate = Utils.getValue(Date.class, json, TO_DATE);
		if (null == toDate)
			Utils.handleException("To date should be in yyyy-MM-dd [HH:mm:ss] format.");

		// get category and stage name
		String category = Utils.getValue(String.class, json, CATEGORY);
		String stageDate = Utils.getValue(String.class, json, STAGE);

		// if query type ByStagingDates then get month wise count.
		if (BY_STAGING_DATES.equalsIgnoreCase(queryType))
			return getMothWiseCountByStages(domain, stageDate, fromDate, toDate);

		// get CategoryType name
		String CategoryType = Utils.getValue(String.class, json, CATEGORY_TYPE);
		if (!STATE.equalsIgnoreCase(CategoryType) && !STAGE.equalsIgnoreCase(CategoryType))
			Utils.handleException("Invalid type.");

		// if query type ByMonth then get month wise count.
		if (BY_MONTH.equalsIgnoreCase(queryType))
			return getMothWiseCount(domain, CategoryType, category, fromDate, toDate);

		// if query type ByMonthCumulative then get month wise cumulative count.
		if (BY_MONTH_CUMULATIVE.equalsIgnoreCase(queryType))
			return getMonthWiseCumulativeCount(domain, CategoryType, category, fromDate, toDate);
		
		if (!ALL_CUSTOMERS.equalsIgnoreCase(category))
			query.addCondition(CategoryType, CarIQSimpleQuery.EQ, category);

		query.addCondition(DOMAIN, CarIQSimpleQuery.EQ, domain);
		query.addCondition("entryDate", CarIQSimpleQuery.GT_EQ, fromDate);
		query.addCondition("entryDate", CarIQSimpleQuery.LT_EQ, toDate);
		query.setPaging(pageNo, pageSize);

		List<GenericJSON> retVal = new ArrayList<GenericJSON>();
		retVal.add(new GenericJSON().add(category, query.getResultList().size()));

		return retVal;
	}

	/**
	 * Gets the moth wise count by stages.
	 *
	 * @param domain
	 *            the domain
	 * @param stageDate
	 *            the stage date
	 * @param fromDate
	 *            the from date
	 * @param toDate
	 *            the to date
	 * @return the moth wise count by stages
	 */
	private List<GenericJSON> getMothWiseCountByStages(Domain domain, String stageDate, Date fromDate, Date toDate) {
		if (DEVICE_DELIVERED.equalsIgnoreCase(stageDate))
			stageDate = "device_delivered_on_date";
		else if (DEVICE_ONBOARDED.equalsIgnoreCase(stageDate))
			stageDate = "device_onboarded_date";
		else if (DEVICE_REQUESTED.equalsIgnoreCase(stageDate))
			stageDate = "device_requested_date";
		else if (DEVICE_SHIPPED.equalsIgnoreCase(stageDate))
			stageDate = "device_shipped_on_date";
		else if (NOT_INTERESTED.equalsIgnoreCase(stageDate))
			stageDate = "ni_date";
		else if (NOT_CONTACTABLE.equalsIgnoreCase(stageDate))
			stageDate = "nc_date";
		else if (VERIFIED.equalsIgnoreCase(stageDate))
			stageDate = "verified_date";
		else
			Utils.handleException("Invalid stage");

		LinkedHashMap<String, String> attributeAliasMap = new LinkedHashMap<String, String>();
		attributeAliasMap.put("MONTH(ss." + stageDate + ")", MONTH);
		attributeAliasMap.put("YEAR(ss." + stageDate + ")", YEAR);
		attributeAliasMap.put("count(*)", COUNT);

		try (ProfilePoint _getMothWiseCount = ProfilePoint
				.profileAction("ProfAction_StageStatisticsCountExporter_getMothWiseCountByStages")) {

			CarIQRawQuery qry = new CarIQRawQuery("StageStatisticsCountExporter", StageStatistics.entityManager(),
					attributeAliasMap, tableAliasMap);
			qry.addGroupBy("MONTH(ss." + stageDate + "),YEAR(ss." + stageDate + ")");

			qry.addCondition(DOMAIN, CarIQSimpleQuery.EQ, domain);
			qry.addCondition(stageDate, CarIQSimpleQuery.GT_EQ, fromDate);
			qry.addCondition(stageDate, CarIQSimpleQuery.LT_EQ, toDate);

			return qry.getResult();
		}
	}

	/**
	 * Gets the moth wise count.
	 *
	 * @param domain the domain
	 * @param type the type
	 * @param category the category
	 * @param fromDate the from date
	 * @param toDate the to date
	 * @return the moth wise count
	 */
	private List<GenericJSON> getMothWiseCount(Domain domain, String type, String category, Date fromDate,
			Date toDate) {
		try (ProfilePoint _getMothWiseCount = ProfilePoint
				.profileAction("ProfAction_StageStatisticsCountExporter_getMothWiseCount")) {

			// database field name like entry_date or device_onboarded_date or car_connected_date
			String fieldName = ENTRY_DATE;
			if (DEVICE_ONBOARDED_CATEGORY.equals(category))
				fieldName = "device_onboarded_date";
			else if (CAR_CONNECTED_CATEGORY.equals(category))
				fieldName = "car_connected_date";
			
			LinkedHashMap<String, String> attributeAliasMap = new LinkedHashMap<String, String>();
			attributeAliasMap.put("MONTH(ss." + fieldName + ")", MONTH);
			attributeAliasMap.put("YEAR(ss." + fieldName + ")", YEAR);
			attributeAliasMap.put("count(*)", COUNT);

			CarIQRawQuery qry = new CarIQRawQuery("StageStatisticsCountExporter", StageStatistics.entityManager(),
					attributeAliasMap, tableAliasMap);
			qry.addGroupBy("MONTH(ss." + fieldName + "),YEAR(ss." + fieldName + ")");

			if (!ALL_CUSTOMERS.equalsIgnoreCase(category) && !DEVICE_ONBOARDED_CATEGORY.equalsIgnoreCase(category))
				qry.addCondition(type, CarIQSimpleQuery.EQ, category);

			qry.addCondition(DOMAIN, CarIQSimpleQuery.EQ, domain);
			qry.addCondition(fieldName, CarIQSimpleQuery.GT_EQ, fromDate);
			qry.addCondition(fieldName, CarIQSimpleQuery.LT_EQ, toDate);
			qry.orderBy(fieldName, CarIQSimpleQuery.ASC);

			return qry.getResult();
		}
	}
	
	/**
	 * Gets the month wise cumulative count.
	 *
	 * @param domain the domain
	 * @param type the type
	 * @param category the category
	 * @param fromDate the from date
	 * @param toDate the to date
	 * @return the month wise cumulative count
	 */
	private List<GenericJSON> getMonthWiseCumulativeCount(Domain domain, String type, String category, Date fromDate,
			Date toDate) {
		try (ProfilePoint _getMonthWiseCumulativeCount = ProfilePoint
				.profileAction("ProfAction_StageStatisticsCountExporter_getMonthWiseCumulativeCount")) {

			if (ALL.equalsIgnoreCase(category))
				return getALLCategoryCumulativeCount(domain, type, fromDate, toDate);

			return getCategoryCumulativeCount(domain, type, category, fromDate, toDate);
		}
	}

	/**
	 * Gets the ALL category cumulative count.
	 *
	 * @param domain the domain
	 * @param type the type
	 * @param fromDate the from date
	 * @param toDate the to date
	 * @return the ALL category cumulative count
	 */
	private List<GenericJSON> getALLCategoryCumulativeCount(Domain domain, String type, Date fromDate, Date toDate) {
		try (ProfilePoint _getALLCategoryCumulativeCount = ProfilePoint
				.profileAction("ProfAction_StageStatisticsCountExporter_getALLCategoryCumulativeCount")) {
			GenericJSON json = new GenericJSON();
			List<GenericJSON> allCategoryCumulativeCount = new ArrayList<GenericJSON>();
			json.add(ALL_CUSTOMERS,
					getCategoryCumulativeCount(domain, type, ALL_CUSTOMERS, fromDate, toDate).toString());
			json.add(DEVICE_ONBOARDED,
					getCategoryCumulativeCount(domain, type, DEVICE_ONBOARDED_CATEGORY, fromDate, toDate).toString());
			json.add(CAR_CONNECTED,
					getCategoryCumulativeCount(domain, type, CAR_CONNECTED_CATEGORY, fromDate, toDate).toString());

			allCategoryCumulativeCount.add(json);

			return allCategoryCumulativeCount;
		}
	}
	
	/**
	 * Gets the category cumulative count.
	 *
	 * @param domain the domain
	 * @param type the type
	 * @param category the category
	 * @param fromDate the from date
	 * @param toDate the to date
	 * @return the category cumulative count
	 */
	private List<GenericJSON> getCategoryCumulativeCount(Domain domain, String type, String category, Date fromDate,
			Date toDate) {
		try (ProfilePoint _getCategoryCumulativeCount = ProfilePoint
				.profileAction("ProfAction_StageStatisticsCountExporter_getCategoryCumulativeCount")) {
			// database field name like entry_date or device_onboarded_date or car_connected_date
			String fieldName = "entryDate";
			if (DEVICE_ONBOARDED_CATEGORY.equals(category))
				fieldName = "deviceOnboardedDate";
			else if (CAR_CONNECTED_CATEGORY.equals(category))
				fieldName = "carConnectedDate";

			CarIQSimpleAggregationQuery<Long> query = new CarIQSimpleAggregationQuery<Long>("getTotalCount",
					StageStatistics.entityManager(), StageStatistics.class, Long.class, AggregationType.Count,
					fieldName);

			query.addCondition(DOMAIN, CarIQSimpleQuery.EQ, domain);
			query.addCondition(type, CarIQSimpleQuery.EQ, category);
			query.addCondition(fieldName, CarIQSimpleQuery.LT, fromDate);

			Long totalCount = query.getSingleResult();

			// get month wise count
			List<GenericJSON> retVal = getMothWiseCount(domain, type, category, fromDate, toDate);
			for (GenericJSON json : retVal) {
				totalCount = totalCount + Utils.getValue(Long.class, json, COUNT);
				json.put(COUNT, totalCount);
			}

			return retVal;
		}
	}
}