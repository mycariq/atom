package com.cariq.toolkit.coreiq.tableUtil.impl;

/**
 * Basic Row from Table.. It can have style and it has 0 based index in the table
 * And it has cells - 1 per column
 * @author hrishi
 *
 */
public class Row extends RowColumn {
	public Row() {
		this(null);
	}
	
	public Row(String style) {
		super(style);
		this.style = style;
	}
}
