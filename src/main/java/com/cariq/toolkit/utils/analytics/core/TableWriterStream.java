package com.cariq.toolkit.utils.analytics.core;

import java.io.IOException;

public abstract class TableWriterStream extends TableStream implements AutoCloseable {
	public TableWriterStream(String fileName) {
		super(fileName);
	}

	public abstract void write(Record record)  throws IOException;
	
	public abstract void flush() throws IOException;

	public abstract void writeHeader() throws IOException;
}
