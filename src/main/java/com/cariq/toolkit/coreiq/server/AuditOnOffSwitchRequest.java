package com.cariq.toolkit.coreiq.server;

import com.cariq.toolkit.coreiq.server.Server;

/**
 * Server Specific Request
 * @author hrishi
 *
 */
public class AuditOnOffSwitchRequest implements ServerSpecificRequest {
	String serverName;
	OnOffSwitch onOffSwitch;
	
	
	public AuditOnOffSwitchRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AuditOnOffSwitchRequest(OnOffSwitch onOffSwitch) {
		this(Server.ANY, onOffSwitch);
	}

	public AuditOnOffSwitchRequest(String serverName, OnOffSwitch onOffSwitch) {
		super();
		this.serverName = serverName;
		this.onOffSwitch = onOffSwitch;
	}

	@Override
	public String getServerName() {
		return serverName;
	}
	
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	
	public OnOffSwitch getOnOffSwitch() {
		return onOffSwitch;
	}
	
	public void setOnOffSwitch(OnOffSwitch onOffSwitch) {
		this.onOffSwitch = onOffSwitch;
	}
}
