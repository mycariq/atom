package com.cariq.toolkit.coreiq.tableUtil.interfaces;

import java.util.List;

/**
 * 
 * @author hrishi
 * Report->Table->Row->Column (CellIfc)
 */
public interface ReportIfc extends CellLocatorIfc {
	String getName();
	List<TableIfc> getTables();
}
