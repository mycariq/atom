package com.atom.www.exporter;

import java.util.List;

import org.springframework.beans.factory.annotation.Configurable;

import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.model.Timeline.Timeline_Out;
import com.cariq.toolkit.model.User.User_In;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

@Configurable
public class StagingAndPolicyDetailsMiniReportExporter extends StagingAndPolicyDetailsReportExporter {

	public static final String TheExporter = "TATReport";
	private static final String TheExporterDescription = "ICICI Lombard TAT report gives details of statistics of onboarding";
	
	protected static final String KEY_Policy_Number = "Policy Number";
	protected static final String KEY_Policy_Stage = "Policy Stage";
	protected static final String KEY_Policy_State = "Policy State";
	protected static final String KEY_desposition = "Disposition";
	private static final String KEY_State_date = "Policy state date";
	protected static final String KEY_Policy_Entry_Date = "CarIQ Entry Date";
	private static final String KEY_Policy_Issue_Date = "Policy Issue Date";
	private static final String Key_PolicyExpiryDate = "Policy Expiry Date";
	private static final String KEY_NC_Date = "NC Date";
	private static final String KEY_NI_Date = "NI Date";
	private static final String KEY_Customer_Verified_Date = "Customer Verified Date";
	private static final String KEY_Device_Requested_Date = "Device Requested Date";
	private static final String KEY_Device_Shipped_Or_Field_Agent_Assigned_Date = "Shipped or Field Assigned Date";
	protected static final String KEY_Device_Onboarded_Date = "Device Added Date";
	private static final String KEY_Car_Connected_Date = "Car Connected Date";
	private static final String KEY_Verified_Status= "Verified Status";
	private static final String KEY_Device_Requested_Status = "Device Requested Status";
	private static final String KEY_Device_Shipped_Or_Field_Agent_Assigned_Status = "Shipped or Field Assigned Status"; 
	private static final String KEY_Devive_Onboarded_Status = "Device Added Status";
	private static final String KEY_Car_Connected_Status = "Car Connected Status";
	private static final String KEY_Agency = "Agency";
	protected static final String KEY_Customer_First_Name = "Customer First Name";
	private static final String KEY_Customer_Last_Name = "Customer Last Name";
	private static final String KEY_Customer_Mobile_Number = "Customer Mobile Number";
	protected static final String KEY_Customer_Email = "Customer Email";
	protected static final String KEY_Customer_City = "Customer City";
	protected static final String KEY_Customer_State = "Customer State";
	protected static final String KEY_Customer_PinCode = "Customer PinCode";
	protected static final String KEY_Car_Make = "Car Make";
	protected static final String KEY_Car_Model = "Car Model";
	protected static final String KEY_Car_Registration_Number = "Car Registration Number";
	protected static final String KEY_Device_Id = "Device Id";
	protected static final String Key_IMSI_NUMBER = "Sim IMSI Number";
	protected static final String KEY_Tier_City = "City Tier";
	protected static final String KEY_Policy_Entry_Month = "CarIQ Entry Month";
	private static final String KEY_WEEKS = "weeks";
	private static final String POLICY_STATUS_TEST = "Test";
	private static final String NOT_AVAILABLE = "Not available";
	
	private static final String KEY_SUBSCRIPTION_STATE = "SubscriptionState";
	private static final String KEY_NEXT_ACTION = "NextAction";
	private static final String KEY_POLICY_DEACTIVED_ON = "PolicyDeactivatedOn";
	private static final String KEY_POLICY_RENEWED_ON = "PolicyRenewedOn";
	private static final String KEY_SHIPMENT_STATUS = "Shipment_Status";
	private static final String KEY_EXPIRY_MONTH = "Expiry Month";
	private static final String KEY_EXPIRY_YEAR = "Expiry Year";
	private static final String KEY_ISSUE_MONTH = "Issue Month";
	private static final String KEY_ISSUE_YEAR = "Issue Year";
	private static final String KEY_POLICY_CATEGORY = "PolicyCategory";
	
	private static final String KEY_TIMELINE = "timeline";	
	private static final String POLICY_STATE = "State";
	
	/********	TAT columns ***************************/
	private static final String KEY_Verified_TAT = "Verified TAT";
	private static final String KEY_Device_Requested_TAT = "Device Requested TAT";
	private static final String KEY_Device_Shipped_TAT = "Device Shipped TAT";
	private static final String KEY_Car_Connected_TAT = "Car Connected TAT";
	
	private static final String TIER_A = "Tier-A";
	private static final String TIER_B = "Tier-B";
	private static final String KEY_CHANNEL = "Vertical";
	private static final String SUB_VERTICAL = "SubVertical";	
	
	private static final String NA = "N/A";
	
	private static final String STAGING_CHANNEL = "StagingChannel"; 
	private static final String STAGING_SUB_CHANNEL = "StagingSubChannel";
	private static final String STAGING_TAGS = "StagingTags";
	
	private static final String OWNER_FIRST_NAME = "firstName";
	private static final String OWNER_LAST_NAME = "lastName";
	
	private static final String NONE = "None";
	
	/**
	 * Default constructor
	 */
	public StagingAndPolicyDetailsMiniReportExporter() {
		super(TheExporter, TheExporterDescription);
	}
	
	
	/**
	 * Parameterized constructor
	 */
	public StagingAndPolicyDetailsMiniReportExporter(String exporterName, String exporterDescription) {
		super(exporterName, exporterDescription);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected GenericJSON getEnhancedObject(GenericJSON json, String queryType, GenericJSON inputJson) {
		GenericJSON miniJSON = new GenericJSON();
		
		List<Timeline_Out> timelines = (List<Timeline_Out>) json.get(KEY_TIMELINE);
		
		String policyNumber = (String) json.get(KEY_POLICY_NO);		
		if(policyNumber.equalsIgnoreCase(debugPolicy)) {
			System.out.println("Found dummy policy");
		}
		
		miniJSON.put(KEY_Policy_Number, json.get("policyNumber"));
		
		miniJSON.put(KEY_Policy_Stage, StagingAndPolicyHelper.getPolicyStage(json));
		miniJSON.put(KEY_Policy_State, StagingAndPolicyHelper.getPolicyState(json));
		miniJSON.put(KEY_SUBSCRIPTION_STATE, json.get(KEY_SUBSCRIPTION_STATE));
		miniJSON.put(KEY_State_date, StagingAndPolicyHelper.getDespositionDate(json));
		miniJSON.put(KEY_POLICY_CATEGORY, getPolicyCategory(json));
		
		/**** All dates here ************/
		String entryDate = (String) json.get("StagingCreatedOn");
		miniJSON.put(KEY_Policy_Entry_Date, entryDate);
		miniJSON.put(KEY_Policy_Issue_Date, json.get("Staging_policyIssueDate"));
		miniJSON.put(Key_PolicyExpiryDate, json.get("Staging_policyExpiryDate"));
		miniJSON.put(KEY_POLICY_DEACTIVED_ON, json.get(KEY_POLICY_DEACTIVED_ON));
		miniJSON.put(KEY_POLICY_RENEWED_ON, json.get(KEY_POLICY_RENEWED_ON));
		miniJSON.put(KEY_NC_Date, StagingAndPolicyHelper.getNotContactableDate(timelines));
		miniJSON.put(KEY_NI_Date, StagingAndPolicyHelper.getNotInterestedDate(timelines));
		miniJSON.put(KEY_Customer_Verified_Date, (String) json.get("Policy_CreationTime"));
		miniJSON.put(KEY_Device_Requested_Date, StagingAndPolicyHelper.getDeviceRequestedOnDate(json));
		
		String tierCity = Utils.getValue(json, "TierCity");
		miniJSON.put(KEY_Device_Shipped_Or_Field_Agent_Assigned_Date,
				(TIER_A.equalsIgnoreCase(tierCity)) ? StagingAndPolicyHelper.getDeviceRequestedOnDate(json)
						: StagingAndPolicyHelper.getDeviceShippedOnDate(json));
			
		miniJSON.put(KEY_Device_Onboarded_Date, json.get("Device_OnboardedOn"));
		miniJSON.put(KEY_Car_Connected_Date, json.get("Car_FirstSeenOn"));
		miniJSON.put(KEY_EXPIRY_MONTH, 
				StagingAndPolicyHelper.getMonth(Utils.getValue(json, "Staging_policyExpiryDate"))
				+ "-"
				+ StagingAndPolicyHelper.getYear(Utils.getValue(json, "Staging_policyExpiryDate")));
		
		miniJSON.put(KEY_ISSUE_MONTH, 
				StagingAndPolicyHelper.getMonth(Utils.getValue(json, "Staging_policyIssueDate"))
				+ "-"
				+ StagingAndPolicyHelper.getYear(Utils.getValue(json, "Staging_policyIssueDate")));
		
		miniJSON.put("PolicyOrderDate", Utils.getValue(json, "PolicyOrderDate"));
		miniJSON.put("PolicyOrderMonth", 
				StagingAndPolicyHelper.getMonth(Utils.getValue(json, "PolicyOrderDate"))
				+ "-"
				+ StagingAndPolicyHelper.getYear(Utils.getValue(json, "PolicyOrderDate")));
		miniJSON.put("ShippingMonth", 
				StagingAndPolicyHelper.getMonth(Utils.getValue(json, "ShippedOn"))
				+ "-"
				+ StagingAndPolicyHelper.getYear(Utils.getValue(json, "ShippedOn")));
		
		miniJSON.put("ConnectionMonth", 
				StagingAndPolicyHelper.getMonth(Utils.getValue(json, "Car_FirstSeenOn"))
				+ "-"
				+ StagingAndPolicyHelper.getYear(Utils.getValue(json, "Car_FirstSeenOn")));
		
		/*****All status here *****************************/
		miniJSON.put(KEY_Verified_Status, json.get("Verified"));
		miniJSON.put(KEY_Device_Requested_Status, StagingAndPolicyHelper.getDeviceRequestedOnDate(json) != null ? true : false);
		miniJSON.put(KEY_Device_Shipped_Or_Field_Agent_Assigned_Status,
				(TIER_A.equalsIgnoreCase(tierCity))
						? (StagingAndPolicyHelper.getDeviceRequestedOnDate(json) != null ? true : false)
						: (StagingAndPolicyHelper.getDeviceShippedOnDate(json) != null ? true : false));
		
		miniJSON.put(KEY_Devive_Onboarded_Status, json.get("Device_OnboardedOn") != null ? true : false);
		miniJSON.put(KEY_Car_Connected_Status, json.get("Car_FirstSeenOn") != null ? true : false);
		miniJSON.put(KEY_SHIPMENT_STATUS, json.get(KEY_SHIPMENT_STATUS));
		
		/****** Agency details ***************/
		miniJSON.put(KEY_Agency, json.get("Agency"));
		
		/****** Customer details **********************/
		
		miniJSON.put(KEY_Customer_First_Name, json.get("Staging_customerFirstName"));
		miniJSON.put(KEY_Customer_Last_Name, json.get("Staging_customerLastName"));
		miniJSON.put(KEY_Customer_City, json.get("Staging_customerCity"));
		miniJSON.put(KEY_Customer_State, json.get("Staging_customerState"));
		miniJSON.put(KEY_Customer_PinCode, json.get("Staging_customerPinCode"));
		miniJSON.put(KEY_Customer_Mobile_Number, json.get("Staging_customerMobileNumber"));
		miniJSON.put(KEY_Customer_Email, json.get("Staging_customerEmail"));
		
		/******** Car details *********************/
		
		miniJSON.put(KEY_Car_Make, json.get("carMake"));
		miniJSON.put(KEY_Car_Model, json.get("carModel"));
		miniJSON.put("CarId", json.get("CarId"));
		miniJSON.put(KEY_Car_Registration_Number, json.get("carRegistrationNumber"));
		miniJSON.put(KEY_Device_Id, json.get("DeviceId"));
		miniJSON.put(Key_IMSI_NUMBER, json.get("Sim_IMSI_Number"));
		miniJSON.put("KmsDriven", json.get("Car_Distance_Since_Connected"));
		
		
		miniJSON.put(KEY_Tier_City, tierCity);
		miniJSON.put(KEY_Policy_Entry_Month, json.get("StagingMonth"));
		miniJSON.put(KEY_WEEKS, StagingAndPolicyHelper.getWeeksFromStagingCreatedDate(json));
		
		/********* TAT details *************************/
		
		miniJSON.put(KEY_Verified_TAT, StagingAndPolicyHelper.calculateTAT(entryDate, (String) json.get("Policy_CreationTime")));
		miniJSON.put(KEY_Device_Requested_TAT, StagingAndPolicyHelper.calculateTAT(entryDate, (String) StagingAndPolicyHelper.getDeviceRequestedOnDate(json)));
		miniJSON.put(KEY_Device_Shipped_TAT, StagingAndPolicyHelper.calculateTAT(entryDate,
				(String) StagingAndPolicyHelper.getDeviceShippedOnDate(json)));
		miniJSON.put(KEY_Car_Connected_TAT, StagingAndPolicyHelper.calculateTAT(entryDate, (String) json.get("Car_FirstSeenOn")));
		miniJSON.put("PolicyRenewalCount", Utils.getValue(json, "PolicyRenewalCount"));
		miniJSON.put(KEY_CHANNEL, Utils.getValue(json, STAGING_CHANNEL) == null ? NA : Utils.getValue(json, STAGING_CHANNEL));
		miniJSON.put(SUB_VERTICAL, Utils.getValue(json, STAGING_SUB_CHANNEL) == null ? NA : Utils.getValue(json, STAGING_SUB_CHANNEL));
		miniJSON.put(KEY_NEXT_ACTION, json.get(KEY_NEXT_ACTION));
		miniJSON.put(STAGING_TAGS,  Utils.getValue(json, STAGING_TAGS));
		miniJSON.put(KEY_desposition, StagingAndPolicyHelper.getDesposition(json));
		
		User_In user = getUser(timelines);
		miniJSON.put(OWNER_FIRST_NAME, user != null ? user.getFirstName() : NONE);
		miniJSON.put(OWNER_LAST_NAME, user != null ? user.getLastName() : NONE);
		
		return miniJSON;
	}
	
		
	/**
	 * Gets the user.
	 *
	 * @param timelines the timelines
	 * @return the user
	 */
	private User_In getUser(List<Timeline_Out> timelines) {

		for (Timeline_Out timeline_Out : timelines) {
			if (null != timeline_Out.getUser())
				return timeline_Out.getUser();
		}

		return null;
	}

	/**
	 * Get policy category from json.
	 * 
	 * @param json
	 * @return
	 */
	public static String getPolicyCategory(GenericJSON json) {
		String subscriptionState = Utils.getValue(json, KEY_SUBSCRIPTION_STATE);
		Boolean isValid = Utils.getValue(Boolean.class, json, "Valid");
		String state = StagingAndPolicyHelper.getPolicyState(json);
		String stagingTags = Utils.getValue(json, "StagingTags");
		
		//If subscription state is not null then processed
		if(subscriptionState != null) 
			return StagingAndPolicyHelper.CATEGORY_PROCESSED;
		
		//If NC and valid then not-verified-nc
		if(StagingAndPolicyHelper.TAG_NOT_CONTACTABLE.equals(state) && isValid)
			return StagingAndPolicyHelper.CATEGORY_NOT_VERIFIED_NC;
		
		//If NI and valid then not-verified-ni
		if(StagingAndPolicyHelper.TAG_NOT_INTERESTED.equals(state) && isValid)
			return StagingAndPolicyHelper.CATEGORY_NOT_VERIFIED_NI;
		
		//If subscription state is null and policy is valid then not verified
		if(subscriptionState == null && isValid)
			return StagingAndPolicyHelper.CATEGORY_NOT_VERIFIED;
		
		///If carnot and invalid then invalid-carnot
		if(StagingAndPolicyHelper.STAGING_TAG_CARNOT.equals(stagingTags) && !isValid)
			return StagingAndPolicyHelper.CATEGORY_INVALID_CARNOT;
		
		//If invalid transaction or new invalid and not valid then invalid-transaction.
		if((StagingAndPolicyHelper.STAGING_TAG_INVALID_TRANSACTION.equals(stagingTags) 
				|| StagingAndPolicyHelper.STAGING_TAG_NEW_INVALID.equals(stagingTags)) && !isValid)
			return StagingAndPolicyHelper.CATEGORY_INVALID_TRANSACTION;
		
		//If staging tag is duplicate car and not valid then invalid-dup-car.
		if(StagingAndPolicyHelper.STAGING_TAG_DUP_CAR.equals(stagingTags) && !isValid)
			return StagingAndPolicyHelper.CATEGORY_INVALID_DUP_CAR;
		
		return "";
	}
}
