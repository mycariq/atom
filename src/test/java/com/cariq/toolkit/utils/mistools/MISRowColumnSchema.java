package com.cariq.toolkit.utils.mistools;

import java.util.List;

import com.cariq.toolkit.utils.CQObjectConvertor;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.GenericJsonSchemable;
import com.cariq.toolkit.utils.KeyValuePair;
import com.cariq.toolkit.utils.Utils;

public abstract class MISRowColumnSchema implements GenericJsonSchemable {
    String style;
    String label;
    String evaluatorClass;
    List<KeyValuePair> evaluatorArguments;
    
    public MISRowColumnSchema(String style, String label, String evaluatorClass, List<KeyValuePair> evaluatorArguments) {
		super();
		this.style = style;
		this.label = label;
		this.evaluatorClass = evaluatorClass;
		this.evaluatorArguments = evaluatorArguments;
	}



	/** RowColumn output */
    @Override
	public GenericJSON getSchema() {
		return GenericJSON.build("style", style, "label", label, "evaluatorClass", evaluatorClass, "argumentJSON",
				Utils.convertList(evaluatorArguments, new CQObjectConvertor<KeyValuePair, GenericJSON>() {
					@Override
					public GenericJSON convert(KeyValuePair in) {
						return in.toGenericJSON();
					}
				}));
	}

}
