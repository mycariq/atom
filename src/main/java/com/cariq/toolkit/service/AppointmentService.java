package com.cariq.toolkit.service;

import com.cariq.toolkit.model.Appointment;
import com.cariq.toolkit.model.Appointment.Appointment_In;
import com.cariq.toolkit.model.Appointment.Appointment_Out;
import com.cariq.toolkit.utils.ResponseWithIdJson;

/**
 * Appointment service to perform various operations on appointment
 * @author sagar
 *
 */
public interface AppointmentService {
	
	/**
	 * Create appointment
	 * 
	 * @param appointmentIn 	Input json to create appointment
	 * @return					Response with id json
	 */
	ResponseWithIdJson create(Appointment_In appointmentIn);
	
	/**
	 * Update appointment with given id
	 * 
	 * @param appointmentId		Id of existing appointment to update    
	 * @param appointmentIn     Input json of updated values            
	 * @return                  Response with id json                   
	 */
	ResponseWithIdJson update(Long appointmentId, Appointment_In appointmentIn);
}
