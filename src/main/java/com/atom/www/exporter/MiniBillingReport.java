package com.atom.www.exporter;

import org.springframework.beans.factory.annotation.Configurable;

import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

@Configurable
public class MiniBillingReport extends StagingAndPolicyDetailsMiniReportExporter {

	private static final String KEY_CAR_DISTANCE_SINCE_CONNECTED = "Car Distance Since Connected";
	private static final String KEY_CAR_LAST_SEEN_ON = "Car Last Seen On";
	private static final String KEY_CAR_FIRST_SEEN_ON = "Car First Seen On";
	private static final String KEY_CAR_HAS_DEVICE = "Car Has Device";
	private static final String KEY_SHIPMENT_AWB_NUMBER = "Shipment AWB Number";
	private static final String KEY_CAR_FUEL_TYPE = "Car Fuel Type";
	private static final String KEY_CAR_MANUFACTURING_YEAR = "Car Manufacturing Year";
	private static final String KEY_POLICY_EXPIRY_DATE = "Policy Expiry Date";
	private static final String KEY_POLICY_ISSUE_DATE = "Policy Issue Date";
	private static final String KEY_CUSTOMER_MOBILE = "Customer Mobile";
	private static final String KEY_CUSTOMER_EMAIL = "Customer Email";
	private static final String KEY_CUSTOMER_NAME = "Customer Name";
	private static final String KEY_CITY_TIER = "City Tier";
	private static final String KEY_ORDER_STAGE = "Order Stage";
	private static final String TheExporter = "BillingReport";
	private static final String TheExporterDescription = "Mini billing report exports only billing related fields";
	
	/**
	 * Default constructor
	 */
	public MiniBillingReport() {
		super(TheExporter, TheExporterDescription);
	}
	
	@Override
	protected GenericJSON getEnhancedObject(GenericJSON exportedObject, String queryType, GenericJSON json) {
		
		GenericJSON miniJSON = new GenericJSON();
		miniJSON.put(KEY_Policy_Number, exportedObject.get("policyNumber"));
		miniJSON.put(KEY_ORDER_STAGE, StagingAndPolicyHelper.getPolicyStage(exportedObject));
		miniJSON.put(KEY_Device_Id, exportedObject.get("DeviceId"));
		miniJSON.put(KEY_CITY_TIER, exportedObject.get("TierCity"));
		miniJSON.put(KEY_Policy_Entry_Month, exportedObject.get("StagingMonth"));
		miniJSON.put(KEY_CUSTOMER_NAME, exportedObject.get("Staging_customerFirstName"));
		miniJSON.put(KEY_Customer_City, exportedObject.get("Staging_customerCity"));
		miniJSON.put(KEY_Customer_State, exportedObject.get("Staging_customerState"));
		miniJSON.put(KEY_Customer_PinCode, exportedObject.get("Staging_customerPinCode"));
		
		miniJSON.put(KEY_CUSTOMER_EMAIL, exportedObject.get("Staging_customerEmail"));
		
		miniJSON.put(KEY_CUSTOMER_MOBILE, exportedObject.get("Staging_customerMobileNumber"));
		
		miniJSON.put(KEY_POLICY_ISSUE_DATE, exportedObject.get("Staging_policyIssueDate"));
		
		miniJSON.put(KEY_POLICY_EXPIRY_DATE, exportedObject.get("Staging_policyExpiryDate"));
		
		miniJSON.put(KEY_Car_Make, exportedObject.get("carMake"));
		miniJSON.put(KEY_Car_Model, exportedObject.get("carModel"));
		miniJSON.put(KEY_Car_Registration_Number, exportedObject.get("carRegistrationNumber"));
		
		miniJSON.put(KEY_CAR_MANUFACTURING_YEAR, exportedObject.get("Staging_carManufacturingYear"));
		
		miniJSON.put(KEY_CAR_FUEL_TYPE, exportedObject.get("Staging_carFuelType"));
		
		miniJSON.put(KEY_Policy_Entry_Date, exportedObject.get("StagingCreatedOn"));
		miniJSON.put(KEY_SHIPMENT_AWB_NUMBER, exportedObject.get("Shipment_AWB"));
		miniJSON.put(KEY_CAR_HAS_DEVICE, exportedObject.get("Car_Has_Device"));
		miniJSON.put(KEY_Device_Onboarded_Date, exportedObject.get("Device_OnboardedOn"));
		miniJSON.put(KEY_CAR_FIRST_SEEN_ON, exportedObject.get("Car_FirstSeenOn"));
		miniJSON.put(KEY_CAR_LAST_SEEN_ON, exportedObject.get("Car_lastSeenOn"));
		miniJSON.put(KEY_CAR_DISTANCE_SINCE_CONNECTED, exportedObject.get("Car_Distance_Since_Connected"));
		
		return miniJSON;
	}
}
