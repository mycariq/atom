package com.atom.www.helper;

import java.util.Calendar;
import java.util.Date;

import com.cariq.toolkit.utils.Utils;

/**
 * @author hrishi
 *
 */
public class SimpleTimeSlot {
	public enum Interval {
		YEAR, MONTH, DAY, HOUR, MINUTE
	}

	// Timeslot split according to interval
	int year, month, day, hour, minute;
	Interval interval;
	

	public SimpleTimeSlot(Interval interval, Date dt) {
		this.interval = interval;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dt);
		
		switch (interval) {
		case MINUTE:
			minute = calendar.get(Calendar.MINUTE);

		case HOUR:
			hour = calendar.get(Calendar.HOUR_OF_DAY);

		case DAY:
			day = calendar.get(Calendar.DAY_OF_MONTH);

		case MONTH:
			month = calendar.get(Calendar.MONTH); // Note: zero based!

		case YEAR:
			year = calendar.get(Calendar.YEAR);

		}
	}
	
	public boolean contains(Date dt) {
		return equals(new SimpleTimeSlot(interval, dt));
	}
	
	public Date getStartTime() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.DAY_OF_MONTH, day);
		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE, minute);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public Date getEndTime() {
		Date startTime = getStartTime();
		switch (interval) {
		case MINUTE:
			return Utils.addMinutes(startTime, 1);

		case HOUR:
			return Utils.addHours(startTime, 1);

		case DAY:
			return Utils.addDays(startTime, 1);

		case MONTH:
			return Utils.addMonths(startTime, 1);

		case YEAR:
			return Utils.addYears(startTime, 1);
		}

		Utils.handleException("Unknown interval: " + interval.toString());	
		
		return null;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + day;
		result = prime * result + hour;
		result = prime * result + ((interval == null) ? 0 : interval.hashCode());
		result = prime * result + minute;
		result = prime * result + month;
		result = prime * result + year;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleTimeSlot other = (SimpleTimeSlot) obj;
		if (interval != other.interval)
			return false;
		if (year != other.year)
			return false;
		if (month != other.month)
			return false;
		if (day != other.day)
			return false;
		if (hour != other.hour)
			return false;
		if (minute != other.minute)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SimpleTimeSlot [startTime =" + getStartTime() + ", endTime =" + getEndTime() + ", interval=" + interval + "]";
	}
}
