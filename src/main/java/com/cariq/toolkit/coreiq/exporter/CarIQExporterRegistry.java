package com.cariq.toolkit.coreiq.exporter;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.atom.www.exporter.AccountManagerStatistics;
import com.atom.www.exporter.AggregateStatisticsExporter;
import com.atom.www.exporter.AppointmentExporter;
import com.atom.www.exporter.AppointmentReport;
import com.atom.www.exporter.AppointmentReportExporter;
import com.atom.www.exporter.AtomSignatureRecordsExporter;
import com.atom.www.exporter.BillingInvoiceExporter;
import com.atom.www.exporter.CSVDataExporter;
import com.atom.www.exporter.GetDeviceBackExporter;
import com.atom.www.exporter.HeatMapExporter;
import com.atom.www.exporter.IndividualLoadMatrixReportExporter;
import com.atom.www.exporter.InvoiceDetailsExporter;
import com.atom.www.exporter.MISDetailsExporter;
import com.atom.www.exporter.MISReport;
import com.atom.www.exporter.MISTATEnhancerExporter;
import com.atom.www.exporter.MISTATExporter;
import com.atom.www.exporter.MiniBillingReport;
import com.atom.www.exporter.MiniPolicyDetailsExporter;
import com.atom.www.exporter.MonthlyMISReportExporter;
import com.atom.www.exporter.OnboardingExporter;
import com.atom.www.exporter.OverallSpotlightExporter;
import com.atom.www.exporter.PolicyCarPerformanceExporter;
import com.atom.www.exporter.PostShipmentExporter;
import com.atom.www.exporter.PreShipmentExporter;
import com.atom.www.exporter.ProxyExporter;
import com.atom.www.exporter.RenewalSpotlight;
import com.atom.www.exporter.RetrievalSpotlight;
import com.atom.www.exporter.SpotlightExporter;
import com.atom.www.exporter.StageStatisticsCountExporter;
import com.atom.www.exporter.StagingAndPolicyDetailsMiniReportExporter;
import com.atom.www.exporter.SupportTATReport;
import com.atom.www.exporter.SystemPerformanceMatrixReportExporter;
import com.atom.www.exporter.TATForMISExporter;
import com.atom.www.exporter.TimeLineExporter;
import com.atom.www.exporter.TimeLineReportExporter;
import com.atom.www.exporter.TrendsExporter;
import com.atom.www.exporter.VerificationStatusExporter;
import com.atom.www.exporter.VerificationTATExporter;
import com.atom.www.exporter.WeeklyMISReportExporter;
import com.cariq.toolkit.coreiq.exporter.DummyTestExporter.DummyTATCreator;
import com.cariq.toolkit.coreiq.exporter.DummyTestExporter.DummyTATProcessor;
import com.cariq.toolkit.utils.Pair;

public class CarIQExporterRegistry {
	static Map<String, CarIQExporter> map = new HashMap<String, CarIQExporter>();
	static Map<String, CarIQExporter> reportMap = new HashMap<String, CarIQExporter>();

	static void add(CarIQExporter exporter) {
		map.put(exporter.getName().toUpperCase(), exporter);
	}

	static void addReport(CarIQExporter exporter) {
		reportMap.put(exporter.getName().toUpperCase(), exporter);
	}

	static {
//		add(new OnboardingProcessExporter());
//		add(new StagingAndPolicyDetailsReportExporter());
		add(new StagingAndPolicyDetailsMiniReportExporter());
		add(new MiniPolicyDetailsExporter());
		add(new MISDetailsExporter());
		add(new AtomSignatureRecordsExporter());
//		add(new AtomSignatureTestExporter());
		add(new OnboardingExporter());
		add(new MiniBillingReport());
		add(new DemoMultiCompositeExporter());
		add(new SupportTATReport());	
		add(new ProxyExporter());
		add(new AccountManagerStatistics());
		add(new TATForMISExporter());
		add(new TimeLineExporter());
		add(new TimeLineReportExporter());
		add(new SystemPerformanceMatrixReportExporter());
		add(new IndividualLoadMatrixReportExporter());
		add(new AppointmentReport());
		add(new AppointmentReportExporter());
		add(new InvoiceDetailsExporter());
		
		//MIS aggregate exporters
		add(new VerificationTATExporter());
		add(new VerificationStatusExporter(VerificationStatusExporter.EChannelExporter,
				VerificationStatusExporter.EChannelShortName,
				new Pair<String, String>("channel", VerificationStatusExporter.EChannel)));
		add(new VerificationStatusExporter(VerificationStatusExporter.RetailTelesaleExporter,
				VerificationStatusExporter.RetailTelesaleShortName,
				new Pair<String, String>("channel", VerificationStatusExporter.RetailTelesale)));
		add(new VerificationStatusExporter(VerificationStatusExporter.AgencyExporter,
				VerificationStatusExporter.AgencyShortName,
				new Pair<String, String>("channel", VerificationStatusExporter.Agency)));
		add(new VerificationStatusExporter(VerificationStatusExporter.GeoExporter,
				VerificationStatusExporter.GeoShortName,
				new Pair<String, String>("channel", VerificationStatusExporter.Geo)));
		add(new VerificationStatusExporter(VerificationStatusExporter.OthersExporter,
				VerificationStatusExporter.OthersShortName,
				new Pair<String, String>("channel", VerificationStatusExporter.Others)));
		add(new VerificationStatusExporter(VerificationStatusExporter.TierAExporter,
				VerificationStatusExporter.TierAShortName,
				new Pair<String, String>("tier", VerificationStatusExporter.TierA)));
		add(new VerificationStatusExporter(VerificationStatusExporter.TierBExporter,
				VerificationStatusExporter.TierBShortName,
				new Pair<String, String>("tier", VerificationStatusExporter.TierB)));
		//keep this at last of all mini MIS exporters
		add(new MISReport());
		
		//spotlight exporter
		add(new SpotlightExporter());
		add(new OverallSpotlightExporter());
		add(new AppointmentExporter());
		add(new PreShipmentExporter());
		add(new PostShipmentExporter());
		add(new TrendsExporter());
		add(new RenewalSpotlight());
		add(new RetrievalSpotlight());
		
		// Stage Statistics Count Exporter
		add(new StageStatisticsCountExporter());
		
		// HeatMap Exporter
		add(new HeatMapExporter());
		
		//Get device back exporter
		add(new GetDeviceBackExporter());
		
		// Aggregate statistics exporter
		add(new AggregateStatisticsExporter());
		
		// PolicyCar performance Exporter
		add(new PolicyCarPerformanceExporter());
		
		add(new WeeklyMISReportExporter());
		add(new MonthlyMISReportExporter());
		add(new BillingInvoiceExporter());
		
		// MIS Dummy Exporters
		add(new DummyTATCreator());
		add(new DummyTATProcessor());
		
		// added new exporter for temp table data
		add(new MISTATExporter());
		add(new CSVDataExporter());
		add(new MISTATEnhancerExporter());
		
	}

	public static CarIQExporter getExporter(String exporterName) {
		CarIQExporter exporter = map.get(exporterName.toUpperCase());
		if (exporter == null)
			exporter = reportMap.get(exporterName.toUpperCase());

		return exporter;
	}

	public static Collection<CarIQExporter> getAllExporters() {
		return map.values();
	}

	public static Collection<CarIQExporter> getAllReporters() {
		return reportMap.values();
	}
}