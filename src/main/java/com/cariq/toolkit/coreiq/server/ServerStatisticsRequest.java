package com.cariq.toolkit.coreiq.server;

import com.cariq.toolkit.coreiq.server.Server;
import com.google.common.base.Strings;

public class ServerStatisticsRequest implements ServerSpecificRequest {

	private String serverName = Server.ANY;
	
	public ServerStatisticsRequest() {
		this(Server.ANY);
	}

	public ServerStatisticsRequest(String serverName) {
		super();
		this.serverName = Strings.isNullOrEmpty(serverName) ? Server.ANY : serverName;
	}

	@Override
	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}	
}
