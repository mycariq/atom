package com.cariq.toolkit.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.InvoiceUnit.InvoiceUnit_In;
import com.cariq.toolkit.model.InvoiceUnit.InvoiceUnit_Out;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.GenericJsonable;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;
import com.cariq.toolkit.utils.Utils;

/**
 * This class implements "InvoiceUnit"
 * InvoiceUnit is the smallest unit of billing, generated as outcome of billing operation. Multiple invoice units can combine to form parent Invoice.
 * It determines the total billables and its amount billed against specific billing term for given month of the year.
 * @author amita
 *
 */

@Entity
@Configurable
public class InvoiceUnit extends PersistentObject<InvoiceUnit> implements JSONable<InvoiceUnit_In, InvoiceUnit_Out, IdJSON>, GenericJsonable {

	/**
	 * The Enum InvoiceUnitStatusEnum.
	 */
	public static enum InvoiceUnitStatusEnum {
		IN_PROGRESS, READY, PAID, INVALID, CANCELLED 
	};

	
	// SECTION 1 -Code for Persistence
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@Version
	Integer version;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new InvoiceUnit().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null)
			this.entityManager = entityManager();
		return entityManager;
	}

	/**
	 * @param id
	 * @return
	 */
	protected static InvoiceUnit find(Long id) {
		return entityManager().find(InvoiceUnit.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public InvoiceUnit findById(Long id) {
		return find(id);
	}

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/**
	 * Model properties
	 */
	@NotNull
	@ManyToOne
	Invoice itsInvoice;

	@NotNull
	@ManyToOne
	BillingTerm itsBillingTerm;
	
	@NotNull
	String billingMonth;
	
	Long numberOfBillables;
	
	Long amount;
	
	String status;

	@Size(max = 4000)
	String infoJson;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	// Default constructor is a must!
	public InvoiceUnit() {
		super();
	}

	/**
	 * 	
	 * @param itsInvoice
	 * @param itsBillingTerm
	 * @param billingMonth
	 * @param billingYear
	 * @param numberOfBillables
	 * @param amount
	 * @param status
	 * @param createdOn
	 * @param modifiedOn
	 */
	public InvoiceUnit(Invoice itsInvoice, BillingTerm itsBillingTerm, String billingMonth,
			Long numberOfBillables, Long amount, String status, Date createdOn,	Date modifiedOn) {
		super();
		this.itsInvoice = itsInvoice;
		this.itsBillingTerm = itsBillingTerm;
		this.billingMonth = billingMonth;
		this.numberOfBillables = numberOfBillables;
		this.amount = amount;
		this.status = status;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
	}

	public Invoice getItsInvoice() {
		return itsInvoice;
	}

	public void setItsInvoice(Invoice itsInvoice) {
		this.itsInvoice = itsInvoice;
	}

	public BillingTerm getItsBillingTerm() {
		return itsBillingTerm;
	}

	public void setitsBillingTerm(BillingTerm itsBillingTerm) {
		this.itsBillingTerm = itsBillingTerm;
	}
	
	public String getBillingMonth() {
		return billingMonth;
	}

	public void setBillingMonth(String billingMonth) {
		this.billingMonth = billingMonth;
	}

	public Long getNumberOfBillables() {
		return numberOfBillables;
	}

	public void setNumberOfBillables(Long numberOfBillables) {
		this.numberOfBillables = numberOfBillables;
	}
	
	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getinfoJson() {
		return infoJson;
	}

	public void setinfoJson(String infoJson) {
		this.infoJson = infoJson;
	}
	
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}		

	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------

	// Implementation of toString and hashCode and equals are advisable for all
	// Model classes
	@Override
	public String toString() {
		return "InvoiceUnit [id=" + id + ", version=" + version + ", itsInvoice=" + itsInvoice.getId()
				+ ", itsBillingTerm=" + itsBillingTerm.getName() + ", billingMonth=" + billingMonth 
				+ ", numberOfBillables=" + numberOfBillables + ", amount=" + amount 
				+ ", status=" + status + ", infoJson=" + infoJson + ", createdOn=" + createdOn + ", modifiedOn=" + modifiedOn + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((itsInvoice == null) ? 0 : itsInvoice.getId().hashCode());
		result = prime * result + ((itsBillingTerm == null) ? 0 : itsBillingTerm.getId().hashCode());
		result = prime * result + ((billingMonth == null) ? 0 : billingMonth.hashCode());
		result = prime * result + ((numberOfBillables == null) ? 0 : numberOfBillables.hashCode());
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((infoJson == null) ? 0 : infoJson.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InvoiceUnit other = (InvoiceUnit) obj;
		if (itsInvoice == null) {
			if (other.itsInvoice != null)
				return false;
		} else if (!itsInvoice.equals(other.itsInvoice))
			return false;
		if (itsBillingTerm == null) {
			if (other.itsBillingTerm != null)
				return false;
		} else if (!itsBillingTerm.equals(other.itsBillingTerm))
			return false;
		if (billingMonth == null) {
			if (other.billingMonth != null)
				return false;
		} else if (!billingMonth.equals(other.billingMonth))
			return false;
		if (numberOfBillables == null) {
			if (other.numberOfBillables != null)
				return false;
		} else if (!numberOfBillables.equals(other.numberOfBillables))
			return false;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;		
		if (infoJson == null) {
			if (other.infoJson != null)
				return false;
		} else if (!infoJson.equals(other.infoJson))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		return true;
	}

	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API
	// interaction
	
	// In Class (for construction)
	public static class InvoiceUnit_In {
		Long itsBillingTermId;
		Long itsInvoiceId;
		String billingMonth;
		Long numberOfBillables;
		Long amount;
		String status;
		String infoJson;

		public InvoiceUnit_In() {
			super();
		}

		/**
		 * 
		 * @param itsBillingTermId
		 * @param itsInvoiceId
		 * @param billingMonth
		 * @param billingYear
		 * @param numberOfBillables
		 * @param amount
		 * @param status
		 */
		public InvoiceUnit_In(Long itsBillingTermId, Long itsInvoiceId, String billingMonth,
				Long numberOfBillables, Long amount, String status, String infoJson) {
			super();
			this.itsBillingTermId = itsBillingTermId;
			this.itsInvoiceId = itsInvoiceId;
			this.billingMonth = billingMonth;
			this.numberOfBillables = numberOfBillables;
			this.amount = amount;
			this.status = status;
			this.infoJson = infoJson;
		}

		public Long getItsInvoiceId() {
			return itsInvoiceId;
		}

		public void setItsInvoiceId(Long itsInvoiceId) {
			this.itsInvoiceId = itsInvoiceId;
		}
		
		public Long getItsBillingTermId() {
			return itsBillingTermId;
		}

		public void setitsBillingTermId(Long itsBillingTermId) {
			this.itsBillingTermId = itsBillingTermId;
		}

		public String getBillingMonth() {
			return billingMonth;
		}

		public void setBillingMonth(String billingMonth) {
			this.billingMonth = billingMonth;
		}

		public Long getNumberOfBillables() {
			return numberOfBillables;
		}

		public void setNumberOfBillables(Long numberOfBillables) {
			this.numberOfBillables = numberOfBillables;
		}

		public Long getAmount() {
			return amount;
		}

		public void setAmount(Long amount) {
			this.amount = amount;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getInfoJson() {
			return infoJson;
		}

		public void setInfoJson(String infoJson) {
			this.infoJson = infoJson;
		}
	}

	// Out class for output
	public static class InvoiceUnit_Out extends InvoiceUnit_In {
		Long id;
		String createdOn;
		String modifiedOn;

		public InvoiceUnit_Out() {
			super();
		}

		public InvoiceUnit_Out(Long itsInvoiceId, Long itsBillingTermId, String billingMonth, 
				Long numberOfBillables, Long amount, String status, String infoJson, String createdOn, String modifiedOn, Long id) {
			super(itsInvoiceId, itsBillingTermId, billingMonth, numberOfBillables, amount, status, infoJson);
			this.id = id;
			this.createdOn = createdOn;
			this.modifiedOn = modifiedOn;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toJSON()
	 */
	@Override
	public InvoiceUnit_Out toJSON() {
		return new InvoiceUnit_Out(itsInvoice.getId(), itsBillingTerm.getId(), billingMonth, 
				numberOfBillables, amount, status, infoJson, createdOn.toString(), modifiedOn.toString(), id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#fromJSON(java.lang.Object)
	 */
	@Override
	public void fromJSON(InvoiceUnit_In json) {
		this.itsInvoice = json.getItsInvoiceId() != null ? Invoice.find(json.getItsInvoiceId()) : null;
		this.itsBillingTerm = json.getItsBillingTermId() != null ? BillingTerm.find(json.getItsBillingTermId()) : null;
		this.billingMonth = json.getBillingMonth() != null ? json.getBillingMonth() : null;
		this.numberOfBillables = json.getNumberOfBillables() != null ? json.getNumberOfBillables() : null;
		this.amount = json.getAmount() != null ? json.getAmount() : null;
		this.status = json.getStatus() != null ? json.getStatus() : null;
		this.infoJson = json.getInfoJson() != null ? json.getInfoJson() : null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}

	@Override
	public GenericJSON toGenericJSON() {
		List<GenericJSON> invoicedBillables = new ArrayList<GenericJSON>();
		for (InvoicedBillable ib : InvoicedBillable.getByInvoiceUnit(this)) {
			invoicedBillables.add(ib.toGenericJSON());
		}
		
		return GenericJSON.build("itsInvoiceId", itsInvoice.getId(), "itsBillingTerm", itsBillingTerm.getName(), 
				"BillingMonth", billingMonth, "NumberOfBillables", numberOfBillables, "Amount", amount, 
				"Status", status, "InfoJson", infoJson, "CreatedOn", createdOn.toString(), "ModifiedOn", modifiedOn.toString(), 
				"Id", id, "invoicedBillables", invoicedBillables);
	}
	
	@Override
	public void fromGenricJSON(GenericJSON json) {
		Utils.notImplementedException("InvoiceUnit.fromGenricJSON");
	}
	
	// ---------------------------------------------
	// SECTION 5 - Query and Class specific finders and static utility methods
	// ---------------------------------------------
	public static InvoiceUnit findObjectById(Long id) {
		return find(id);
	}
	
	public static List<InvoiceUnit> getAllObjects(int pageNo, int pageSize) {
		CarIQSimpleQuery<InvoiceUnit> qry = new CarIQSimpleQuery<InvoiceUnit>("getAllObjects", entityManager(), InvoiceUnit.class);
		qry.setPaging(pageNo, pageSize);

		return qry.getResultList();
	}

	public static List<InvoiceUnit> getByItsInvoice(Invoice invoice) {
		CarIQSimpleQuery<InvoiceUnit> query = new CarIQSimpleQuery<InvoiceUnit>("getByItsInvoice", entityManager(), InvoiceUnit.class);
		query.addCondition("itsInvoice", CarIQSimpleQuery.EQ, invoice);
		
		return query.getResultList();
	}
	
	public static List<InvoiceUnit> getByItsInvoiceMonthYear(Invoice invoice, Integer month, Integer year) {
		CarIQSimpleQuery<InvoiceUnit> query = new CarIQSimpleQuery<InvoiceUnit>("getByItsInvoiceMonthYear", entityManager(), InvoiceUnit.class);
		query.addCondition("itsInvoice", CarIQSimpleQuery.EQ, invoice);
		query.addCondition("billingMonth", CarIQSimpleQuery.EQ, month);
		query.addCondition("billingYear", CarIQSimpleQuery.EQ, year);
		
		return query.getResultList();
	}
	
	public static InvoiceUnit getByItsInvoiceAndTerm(Invoice invoice, BillingTerm term) {	
		CarIQSimpleQuery<InvoiceUnit> query = new CarIQSimpleQuery<InvoiceUnit>("getByItsInvoiceAndTerm", entityManager(), InvoiceUnit.class);
		query.addCondition("itsInvoice", CarIQSimpleQuery.EQ, invoice);
		query.addCondition("itsBillingTerm", CarIQSimpleQuery.EQ, term);

		if (query.getResultList().isEmpty())
			return null;
		return query.getSingleResult();
	}
}