package com.cariq.toolkit.coreiq.tableUtil.interfaces;

import java.util.List;

import com.cariq.toolkit.utils.GenericJsonable;

/**
 * 
 * @author hrishi
 *
 */
public interface RowIfc extends GenericJsonable, CellLocatorIfc {
	int getIndex();
	void setIndex(int indx);

	List<CellIfc> getCells();

	String getStyle();
	void add(CellIfc cell);
}
