package com.test.www.service;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import junit.framework.Assert;

import com.atom.www.helper.AppointmentStatus;
import com.cariq.toolkit.model.Appointment;
import com.cariq.toolkit.model.Appointment.Appointment_In;
import com.cariq.toolkit.model.Appointment.Appointment_Out;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_In;
import com.cariq.toolkit.service.AppointmentService;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.utils.Utils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Transactional
@Configurable
public class AppointmentServiceTest {
	
	@Autowired
	private AppointmentService appointmentService;
	
	@Test
	public void createAppointment() {
		
		String dateStr = "2018-01-18 15:55:00";
		Date date = Utils.getDate(dateStr);
		
		Appointment appointment = createTestAppointment(date, AppointmentStatus.CREATED.name());
		Assert.assertNotNull(appointment);
	}
	
	/**
	 * Create new dummy appointment for testing
	 * @return
	 */
	private Appointment createTestAppointment(Date date, String status) {
		ProxyObject_In proxyObjectIn = new ProxyObject_In("IL-Assist", null,
				"InsurancePolicy", "3001/127221338/01/000", null, 1L);
		
		String dateStr = Utils.convertToyyyymmdd_hhmmssFormat(date);
		Appointment_In appointmentIn = new Appointment_In(proxyObjectIn, dateStr, status, "sweta", "Device Requested");
		ResponseWithIdJson responseJson = appointmentService.create(appointmentIn);
		Appointment appointment = Appointment.find(Long.valueOf(responseJson.getId()));
		return appointment;
	}
	
	@Test
	public void updateAppointment() {
		Appointment appointment = createTestAppointment(new Date(), AppointmentStatus.CREATED.name());
		Appointment_Out outJson = appointment.toJSON();
		
		String dateStr = Utils.convertToyyyymmdd_hhmmssFormat(Utils.addDays(new Date(), 5));
		
		Appointment_In appointmentIn = new Appointment_In(outJson.getProxyObjectIn(),
				dateStr, AppointmentStatus.RESCHEDULED.name(), "sweta", "Device Requested");
		ResponseWithIdJson response = appointmentService.update(appointment.getId(), appointmentIn);
		
		Appointment updatedAppointment = Appointment.find(Long.valueOf(response.getId()));
		Assert.assertEquals(AppointmentStatus.RESCHEDULED.name(), updatedAppointment.getStatus());
		Assert.assertEquals(Utils.addDays(new Date(), 5).getDay(), updatedAppointment.getAppointmentOn().getDay());
	}
}
