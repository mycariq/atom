/**
 * 
 */
package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.springframework.core.io.ClassPathResource;

import com.atom.www.helper.MISReportHelper;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.ExportedData;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.analytics.application.TimeLineReporter;
import com.cariq.toolkit.utils.analytics.core.AnalyticsContext;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsCookbook;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsProcess;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsRecipe;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.google.common.base.Strings;

/**
 * The Class TimeLineReportExporter.
 *
 * @author santosh
 */
public class TimeLineReportExporter extends CarIQExporterTemplate {
	
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("TimeLineReportExporter");
	
	public static final String EXPORTER = "TimeLineReportExporter";
	private static final String EXPORTER_DESC = "Support Performance Report";

	
	private static final String TIMELINE_REPORT_BUILDER_JSON = "json/TimeLineReportBulilder.json";
	public static final String TIMELINE_REPORT_RECIPE = "TimeLineReportRecipe";
	public static final  String TIMELINE_REPORT = "TimeLineReport";
	
	public static final String FROM_FILE_URL = "fromFileURL", FROM_LATEST_FILE_URL = "fromLatestFileURL";
	public static final String FILE_PATH = "fileURL";

	MISReportHelper reportHelper = new MISReportHelper();
	
	
	public TimeLineReportExporter() {
		super(EXPORTER, EXPORTER_DESC);

		// Query definitions
		addQueryType(FROM_FILE_URL, "Timeline-Support Performance Report from given report file url",
				Arrays.asList(new QueryDefinitionAttribute(FILE_PATH, "Absolute file path to timeline csv file")));

		addQueryType(FROM_LATEST_FILE_URL,
				"Generates Timeline-Support Performance Report from latest timeline report file url", new String[] {});

		// Support roles
		addSupportedRoles(Arrays.asList(Utils.ADMIN_ROLE));
	}

	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		try (ProfilePoint _doExport = ProfilePoint.profileAction("TimeLineReportExporter_doExport")) {
			String createdOn = Utils.convertToyyyy_mm_dd_Format(new Date()) + "_" + new Random().nextInt(99);
			String fileURL = null;

			// Ignore pageNo, ignorepageSize, just return alerts of each type
			if (pageNo > 1)
				return null;

			if (FROM_FILE_URL.equalsIgnoreCase(queryType)) {
				fileURL = Utils.getValue(json, FILE_PATH);
			} else if (FROM_LATEST_FILE_URL.equalsIgnoreCase(queryType)) {
				ExportedData exportedData = ExportedData.getLatestExportedData(TimeLineExporter.EXPORTER,
						CarIQFileUtils.CSV);
				if (null != exportedData)
					fileURL = exportedData.getFileUrl();
			} else
				Utils.handleException("QueryType '" + queryType + "' is not supported.");

			Utils.checkNotNullOrEmpty(fileURL, "file url should not be empty");

			// removing spaces from string
			createdOn = createdOn.replace(" ", "_");

			return getTimeLineReport(queryType, json, fileURL, createdOn);
		}
	}
	
	private List<GenericJSON> getTimeLineReport(String queryType, GenericJSON json, String fileURL, String createdOn) {
		try (ProfilePoint _getTimeLineReport = ProfilePoint.profileAction("TimeLineReportExporter_getTimeLineReport")) {
			List<GenericJSON> retList = new ArrayList<GenericJSON>();
			logger.debug("input json for report:>> " + json);
			reportHelper.configureDomainUser(json);

			String localFilePath = CarIQFileUtils.getLocalFilePathFromURL(fileURL);
			if (Strings.isNullOrEmpty(localFilePath) || !CarIQFileUtils.exists(localFilePath))
				Utils.handleException("Invalid report file: " + localFilePath);

			// Process the TimeLine CSV and get the report JSON 
			String reportJSONStr = analyzeReport(localFilePath);

			// Convert processed JSON to local HTML file
			String fileName = TIMELINE_REPORT + "-" + createdOn + ".html";
			String htmlFilePath = reportHelper.convertJSONToHTMLFile(reportJSONStr, fileName, null);
			if (!CarIQFileUtils.exists(htmlFilePath))
				Utils.handleException("Failed to create HTML report from processed JSON");

			// upload and persist HTML file
			GenericJSON outJSON = reportHelper.exportHTMLReport(TIMELINE_REPORT, queryType, htmlFilePath, fileName,
					json.toString());
			// reportHelper.exportCompressedHTMLReport(TIMELINE_REPORT, queryType, htmlFilePath, fileName, json.toString());

			retList.add(outJSON);
			return retList;
		}
	}

	private String analyzeReport(String timeLineFile) {
		try (ProfilePoint _analyzeReport = ProfilePoint.profileAction("TimeLineReportExporter_analyzeReport")) {
			String outJSONString = null;

			try {
				String builderFilePath = new ClassPathResource(TIMELINE_REPORT_BUILDER_JSON).getFile()
						.getAbsolutePath();

				AnalyticsContext ctx = new AnalyticsContext();
				ctx.put(TimeLineReporter.TIMELINE_REPORT_JSON_PATH, builderFilePath);
				AnalyticsRecipe recipe = AnalyticsCookbook.getInstance().getRecipe(TIMELINE_REPORT_RECIPE);

				AnalyticsProcess process = new AnalyticsProcess(recipe, ctx, timeLineFile);
				process.execute();
				// get output JSON
				outJSONString = process.getContext().get(TimeLineReporter.TIMELINE_JSON).toString();
				//close
				process.close();
			} catch (Exception e) {
				Utils.handleException(e);
			}

			return outJSONString;
		}
	}
}