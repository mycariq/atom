package com.cariq.toolkit.utils;

/**
 * @author Hrishi
 * Declare schema in Generic JSON format
 */
public interface GenericJsonSchemable {
	
	/**
	 * Convert any object to GenericJSON type.
	 * @return
	 */
	public GenericJSON getSchema();
	
}
