package com.cariq.toolkit.coreiq.formula.impl;

import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.coreiq.formula.helper.ArithmeticEvaluator;
import com.cariq.toolkit.coreiq.formula.interfaces.FormulaIfc;
import com.cariq.toolkit.coreiq.formula.interfaces.VariableIfc;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.GenericJsonable;
import com.cariq.toolkit.utils.ObjectBuilder;
import com.cariq.toolkit.utils.Utils;

/**
 * Formula for evaluation from literal 
 * @author hrishi
 *
 */
public class Formula implements FormulaIfc, ObjectBuilder<VariableIfc>, GenericJsonable {
	public static final String ADD = "ADD";
	public static final String SUBTRACT = "SUBTRACT";
	public static final String MULTIPLY = "MULTIPLY";
	public static final String DIVIDE = "DIVIDE";
	public static final String MODULUS = "MODULUS";
	public static final String POWER = "POWER";
	public static final String PERCENTAGE = "PERCENTAGE";
	
	
	String expression;
	List<VariableIfc> variables = new ArrayList<VariableIfc>();
	
	// Every GenericJSONable must have a default constructor
	public Formula() {
		super();
	}

	public Formula(String formulaStr) {
		super();
		this.expression = formulaStr;
	}

	@Override
	public String getExpression() {
		return expression;
	}

	@Override
	public List<VariableIfc> getVariables() {
		return variables;
	}

	@Override
	public Object evaluate(Object context) throws Exception {
		// Don't mutate the object - hold the string
		String arithmeticFormula = substituteMathOperations(expression);
		
		for (VariableIfc variable : variables) {
			Object value = variable.resolve(context, true);
			// null value will throw NPE - but that is ok
//			String variableRegexp = "\\{[ \t]*"+variable.getName()+"[ \t]*\\}";
			String variableRegexp = "[ \t]*"+variable.getName()+"[ \t]*";
			arithmeticFormula = arithmeticFormula.replaceAll(variableRegexp, value.toString());
		}
		
		// Finally Invoke ArithmaticEvaluator
		return ArithmeticEvaluator.getInstance().eval(arithmeticFormula);
	}

	private String substituteMathOperations(String expr) {
		String retval = expr.replaceAll(ADD, "+");
		retval = retval.replaceAll(SUBTRACT, "-");
		retval = retval.replaceAll(MULTIPLY, "*");
		retval = retval.replaceAll(DIVIDE, "/");
		retval = retval.replaceAll(MODULUS, "%");
		retval = retval.replaceAll(POWER, "^");
		retval = retval.replaceAll(PERCENTAGE , " * 100 / ");


		return retval;
	}

	@Override
	public ObjectBuilder<VariableIfc> configure(String setting, Object value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Formula add(VariableIfc value) {
		variables.add(value);
		return this;
	}

	@Override
	public ObjectBuilder<VariableIfc> add(String parameter, VariableIfc value) {
		variables.add(value);
		return this;
	}

	@Override
	public ObjectBuilder<VariableIfc> build() {
		return this;
	}

	@Override
	public GenericJSON toGenericJSON() {
		Utils.notImplementedException("Formula.toGenericJSON");
		return null;
	}

	@Override
	public void fromGenricJSON(GenericJSON json) {
		expression = Utils.getValue(String.class, json, "expression");

		// This double conversion seems to be needed for reading the list properly.
		List varList = Utils.getValue(List.class, json, "variables");

		// @TODO This approach of converting to string then reading from string needs to be
		// looked again
		List<GenericJSON> variablesList = Utils.getJSonObjectList(varList.toString(), GenericJSON.class);
		if (Utils.isNullOrEmpty(variablesList))
			return;

		for (GenericJSON genericJSON : variablesList) {
			Variable variable = Utils.constructJSONable(Variable.class, genericJSON);
			variable.fromGenricJSON(genericJSON);
			variables.add(variable);
		}
	}

}
