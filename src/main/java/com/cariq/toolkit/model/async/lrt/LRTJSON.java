package com.cariq.toolkit.model.async.lrt;

import java.util.Date;
import java.util.UUID;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

/**
 * JSON which contains details of Long Running Task
 * This is done with the help of Workitem
 * 
 * @author Hrishi
 *
 */
public class LRTJSON {
	public static final String LRTToken = "lrtToken";

	String token;
	String name;
	GenericJSON request;
	GenericJSON response;
	Date startTime;
	Date endTime;
	String createdBy;
	String status;
	
	
	public LRTJSON() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LRTJSON(String token, String name, GenericJSON request, GenericJSON response, Date startTime, Date endTime, String createdBy,
			String status) {
		super();
		this.token = token;
		this.name = name;
		this.request = request;
		this.response = response;
		this.startTime = startTime;
		this.endTime = endTime;
		this.createdBy = createdBy;
		this.status = status;
	}

	public LRTJSON(String token, String name, String request, String response, Date startTime, Date endTime, String createdBy,
			String status) {
		this(token, name, Utils.getJSonObject(request, GenericJSON.class), Utils.getJSonObject(response, GenericJSON.class), startTime, endTime, createdBy,
			status);
	}

	public LRTJSON(String name, GenericJSON input, String createdBy) {
		this(UUID.randomUUID().toString(), name, input, null, new Date(), null, createdBy, null);
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public GenericJSON getRequest() {
		return request;
	}

	public void setRequest(GenericJSON request) {
		this.request = request;
	}
	
	public GenericJSON getResponse() {
		return response;
	}

	public void setResponse(GenericJSON response) {
		this.response = response;
	}


	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "LRT [token=" + token + ", name=" + name + ", response=" + response + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", createdBy=" + createdBy + ", status=" + status + "]";
	}
}
