package com.cariq.toolkit.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.Payment.Payment_In;
import com.cariq.toolkit.model.Payment.Payment_Out;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;
import com.cariq.toolkit.utils.Utils;

/**
 * This class implements "payment". Payment is tracked against one or more invoices.
 * @author amita
 *
 */

@Entity
@Configurable
public class Payment extends PersistentObject<Payment> implements JSONable<Payment_In, Payment_Out, IdJSON> {

	// SECTION 1 -Code for Persistence
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@Version
	Integer version;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new Payment().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null)
			this.entityManager = entityManager();
		return entityManager;
	}

	/**
	 * @param id
	 * @return
	 */
	protected static Payment find(Long id) {
		return entityManager().find(Payment.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public Payment findById(Long id) {
		return find(id);
	}

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/**
	 * Model properties
	 */
	@NotNull
	@ManyToOne
	Invoice itsInvoice;
	
	Long amount;
	
	String note;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date receivedOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	// Default constructor is a must!
	public Payment() {
		super();
	}

	/**
	 * 
	 * @param itsInvoice
	 * @param amount
	 * @param note
	 * @param receivedOn
	 * @param createdOn
	 * @param modifiedOn
	 */
	public Payment(Invoice itsInvoice, Long amount, String note, Date receivedOn, Date createdOn, Date modifiedOn) {
		super();
		this.itsInvoice = itsInvoice;
		this.amount = amount;
		this.note = note;
		this.receivedOn = receivedOn;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
	}

	public Invoice getItsInvoice() {
		return itsInvoice;
	}

	public void setItsInvoice(Invoice itsInvoice) {
		this.itsInvoice = itsInvoice;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getReceived_on() {
		return receivedOn;
	}

	public void setReceived_on(Date receivedOn) {
		this.receivedOn = receivedOn;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------



	// Implementation of toString and hashCode and equals are advisable for all
	// Model classes
	@Override
	public String toString() {
		return "Payment [id=" + id + ", version=" + version + ", itsInvoice=" + itsInvoice.getId()
				+ ", amount=" + amount + ", note=" + note + ", receivedOn=" + receivedOn
				+ ", createdOn=" + createdOn + ", modifiedOn=" + modifiedOn + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((itsInvoice == null) ? 0 : itsInvoice.getId().hashCode());
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((note == null) ? 0 : note.hashCode());
		result = prime * result + ((receivedOn == null) ? 0 : receivedOn.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Payment other = (Payment) obj;
		if (itsInvoice == null) {
			if (other.itsInvoice != null)
				return false;
		} else if (!itsInvoice.equals(other.itsInvoice))
			return false;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (note == null) {
			if (other.note != null)
				return false;
		} else if (!note.equals(other.note))
			return false;
		if (receivedOn == null) {
			if (other.receivedOn != null)
				return false;
		} else if (!receivedOn.equals(other.receivedOn))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		return true;
	}

	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API
	// interaction
	
	// In Class (for construction)
	public static class Payment_In {
		Long itsInvoiceId;
		Long amount;
		String note;
		String receivedOn;

		public Payment_In() {
			super();
		}

		/**
		 * 
		 * @param itsInvoiceId
		 * @param amount
		 * @param note
		 * @param receivedOn
		 */
		public Payment_In(Long itsInvoiceId, Long amount, String note, String receivedOn) {
			super();
			this.itsInvoiceId = itsInvoiceId;
			this.amount = amount;
			this.note = note;
			this.receivedOn = receivedOn;
		}
		
		public Long getItsInvoiceId() {
			return itsInvoiceId;
		}

		public void setItsInvoiceId(Long itsInvoiceId) {
			this.itsInvoiceId = itsInvoiceId;
		}

		public Long getAmount() {
			return amount;
		}

		public void setAmount(Long amount) {
			this.amount = amount;
		}

		public String getNote() {
			return note;
		}

		public void setNote(String note) {
			this.note = note;
		}

		public String getReceivedOn() {
			return receivedOn;
		}

		public void setReceivedOn(String receivedOn) {
			this.receivedOn = receivedOn;
		}

	}

	// Out class for output
	public static class Payment_Out extends Payment_In {
		Long id;
		String createdOn;
		String modifiedOn;

		public Payment_Out() {
			super();
		}

		public Payment_Out(Long itsInvoiceId, Long amount, String note, String receivedOn, String createdOn, String modifiedOn, Long id) {
			super(itsInvoiceId, amount, note, receivedOn);
			this.id = id;
			this.createdOn = createdOn;
			this.modifiedOn = modifiedOn;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toJSON()
	 */
	@Override
	public Payment_Out toJSON() {
		return new Payment_Out(itsInvoice.getId(), amount, note, receivedOn.toString(), createdOn.toString(), modifiedOn.toString(), id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#fromJSON(java.lang.Object)
	 */
	@Override
	public void fromJSON(Payment_In json) {
		this.itsInvoice = json.getItsInvoiceId() != null ? Invoice.find(json.getItsInvoiceId()) : null;
		this.amount = json.getAmount() != null ? json.getAmount() : null;
		this.note = json.getNote() != null ? json.getNote() : null;
		this.receivedOn = json.getReceivedOn() != null ?  Utils.getDate(json.getReceivedOn()) : null;
		this.createdOn = new Date();
		this.modifiedOn = new Date();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}

	// ---------------------------------------------
	// SECTION 5 - Query and Class specific finders and static utility methods
	// ---------------------------------------------
	public static Payment findObjectById(Long id) {
		return find(id);
	}
	
	public static List<Payment> getAllObjects(int pageNo, int pageSize) {
		CarIQSimpleQuery<Payment> qry = new CarIQSimpleQuery<Payment>("getAllObjects", entityManager(), Payment.class);
		qry.setPaging(pageNo, pageSize);

		return qry.getResultList();
	}

	public static List<Payment> getByItsInvoice(Invoice invoice) {
		CarIQSimpleQuery<Payment> query = new CarIQSimpleQuery<Payment>("getByItsInvoice", entityManager(), Payment.class);
		query.addCondition("itsInvoice", CarIQSimpleQuery.EQ, invoice);
		
		return query.getResultList();
	}
	
	public static List<Payment> getByReceivedOn(Date receivedOn) {
		CarIQSimpleQuery<Payment> qry = new CarIQSimpleQuery<Payment>("getByReceivedOn", entityManager(), Payment.class);
		qry.addCondition("receivedOn", CarIQSimpleQuery.EQ, receivedOn);

		return qry.getResultList();
	}
}