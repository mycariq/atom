package com.atom.www.loader;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.cariq.toolkit.coreiq.loader.CarIQLoaderContext;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderWrapper;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderWrapper.STATUS;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderWrapperImpl;
import com.cariq.toolkit.coreiq.loader.CarIQWrappedLoaderTemplate;
import com.cariq.toolkit.model.ProxyObject;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_In;
import com.cariq.toolkit.model.Timeline;
import com.cariq.toolkit.model.Timeline.Timeline_In;
import com.cariq.toolkit.service.TimelineService;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

@Configurable
public class AtomNotificationLoader extends CarIQWrappedLoaderTemplate {
	
	private static final String TheLoader = "Notification Loader";
	
	private static final String TheLoaderDescription = "Loads notification to send it to the specified user";
	
	private static final String KEY_PROXYOBJECT = "proxyObject";
	
	private static final String KEY_OBJECTTYPE = "objectType";
	
	private static final String KEY_SIGNATURE = "signature";
	
	private static final String KEY_DOMAINID = "domainId";
	
	private static final String KEY_MESSAGE = "longMessage";
	private static final String KEY_TYPE = "notificationType";
	private static final String KEY_TAG = "tag";
	private static final String KEY_STAGE = "stage";
	
	@Autowired
	private TimelineService timelineService;

	public AtomNotificationLoader() {
		super(TheLoader, TheLoaderDescription);
	}

	@Override
	protected CarIQLoaderWrapper getWrapper() {
		return new CarIQLoaderWrapperImpl(TheLoader, TheLoaderDescription);
	}

	@Override
	protected void doLoad(GenericJSON json, CarIQLoaderContext context, STATUS status) {
		
		//if loading task has failed then do not create timline and return immediately.
		if(status.equals(STATUS.FAILED))
			return;
		
		String atomUser = Utils.getCurrentUserName();
		
		//Create and save timeline for notification which is sent to the user
		
		//Create proxyObject by using input json
		@SuppressWarnings("unchecked")
		LinkedHashMap<String, Object> proxyObjectJSON = (LinkedHashMap<String, Object>) json.get(KEY_PROXYOBJECT);
		String proxyType = (String) proxyObjectJSON.get(KEY_OBJECTTYPE);
		String signature = (String) proxyObjectJSON.get(KEY_SIGNATURE);
		Integer domainId = (Integer) proxyObjectJSON.get(KEY_DOMAINID);
		ProxyObject_In proxyObject = new ProxyObject.ProxyObject_In(null, null, proxyType, signature, atomUser, new Long(domainId));
		
		//Retrive values from json to create timeline
		String message = (String) json.get(KEY_MESSAGE);//longMessage
		String type = (String) json.get(KEY_TYPE);//notificationType
		String tag = (String) json.get(KEY_TAG);
		String stage = (String) json.get(KEY_STAGE);
		
		//Create timeline and save it
		Timeline_In timeline = new Timeline.Timeline_In(proxyObject, message, type, tag, stage, atomUser, new Long(domainId));
		
		timelineService.addTimelineItem(timeline);
	}

}
