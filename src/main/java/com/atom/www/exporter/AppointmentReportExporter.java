/**
 * 
 */
package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.core.io.ClassPathResource;

import com.atom.www.helper.MISReportHelper;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.ExportedData;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.analytics.application.AppointmentReporter;
import com.cariq.toolkit.utils.analytics.core.AnalyticsContext;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsCookbook;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsProcess;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsRecipe;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.google.common.base.Strings;

/**
 * The Class AppointmentReportExporter.
 *
 * @author santosh
 */
public class AppointmentReportExporter extends CarIQExporterTemplate {
	
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("AppointmentReportExporter");
	
	public static final String EXPORTER = "AppointmentReportExporter";
	private static final String EXPORTER_DESC = "Appointment Status Report";

	private static final String APPOINTMENT_REPORT_BUILDER_JSON = "json/AppointmentReportBulilder.json";
	public static final String APPOINTMENT_REPORT_RECIPE = "AppointmentReportRecipe";
	public static final  String APPOINTMENT_REPORT = "AppointmentReport";
	
	public static final String FROM_FILE_URL = "fromFileURL", FROM_LATEST_FILE_URL = "fromLatestFileURL";
	public static final String FILE_PATH = "fileURL";

	MISReportHelper reportHelper = new MISReportHelper();
	
	
	public AppointmentReportExporter() {
		super(EXPORTER, EXPORTER_DESC);

		// Query definitions
		addQueryType(FROM_FILE_URL, "Appointment Report from given report file url",
				Arrays.asList(new QueryDefinitionAttribute(FILE_PATH, "Absolute file path to appointment csv file")));

		addQueryType(FROM_LATEST_FILE_URL, "Generates Appointment Report from latest appointment report file url",
				new String[] {});

		// Support roles
		addSupportedRoles(Arrays.asList(Utils.ADMIN_ROLE));
	}

	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		try (ProfilePoint _doExport = ProfilePoint.profileAction("AppointmentReportExporter_doExport")) {
			String fileURL = null;

			// Ignore pageNo, ignorepageSize, just return alerts of each type
			if (pageNo > 1)
				return null;

			if (FROM_FILE_URL.equalsIgnoreCase(queryType)) {
				fileURL = Utils.getValue(json, FILE_PATH);
			} else if (FROM_LATEST_FILE_URL.equalsIgnoreCase(queryType)) {
				ExportedData exportedData = ExportedData.getLatestExportedData(AppointmentReport.REPORT,
						CarIQFileUtils.CSV);
				if (null != exportedData)
					fileURL = exportedData.getFileUrl();
			} else
				Utils.handleException("QueryType '" + queryType + "' is not supported.");

			Utils.checkNotNullOrEmpty(fileURL, "file url should not be empty");

			return getAppointmentReport(queryType, json, fileURL);
		}
	}
	
	private List<GenericJSON> getAppointmentReport(String queryType, GenericJSON json, String fileURL) {
		try (ProfilePoint _getAppointmentReport = ProfilePoint
				.profileAction("AppointmentReportExporter_getAppointmentReport")) {
			List<GenericJSON> retList = new ArrayList<GenericJSON>();
			logger.debug("input json for report:>> " + json);
			reportHelper.configureDomainUser(json);

			String localFilePath = CarIQFileUtils.getLocalFilePathFromURL(fileURL);
			if (Strings.isNullOrEmpty(localFilePath) || !CarIQFileUtils.exists(localFilePath))
				Utils.handleException("Invalid report file: " + localFilePath);

			// Process the TimeLine CSV and get the report JSON 
			String reportJSONStr = analyzeReport(localFilePath);

			// Convert processed JSON to local HTML file
			String fileName = APPOINTMENT_REPORT + "-" + Utils.createDateId(true) + ".html";
			String htmlFilePath = reportHelper.convertJSONToHTMLFile(reportJSONStr, fileName, null);
			if (!CarIQFileUtils.exists(htmlFilePath))
				Utils.handleException("Failed to create HTML report from processed JSON");

			// upload and persist HTML file
			GenericJSON outJSON = reportHelper.exportHTMLReport(APPOINTMENT_REPORT, queryType, htmlFilePath, fileName,
					json.toString());
			// reportHelper.exportCompressedHTMLReport(TIMELINE_REPORT, queryType, htmlFilePath, fileName, json.toString());

			retList.add(outJSON);
			return retList;
		}
	}

	private String analyzeReport(String timeLineFile) {
		try (ProfilePoint _analyzeReport = ProfilePoint.profileAction("AppointmentReportExporter_analyzeReport")) {
			String outJSONString = null;

			try {
				String builderFilePath = new ClassPathResource(APPOINTMENT_REPORT_BUILDER_JSON).getFile()
						.getAbsolutePath();

				AnalyticsContext ctx = new AnalyticsContext();
				ctx.put(AppointmentReporter.APPOINTMENT_JSON_PATH, builderFilePath);
				AnalyticsRecipe recipe = AnalyticsCookbook.getInstance().getRecipe(APPOINTMENT_REPORT_RECIPE);

				AnalyticsProcess process = new AnalyticsProcess(recipe, ctx, timeLineFile);
				process.execute();
				// get output JSON
				outJSONString = process.getContext().get(AppointmentReporter.APPOINTMENT_JSON).toString();
				//close
				process.close();
			} catch (Exception e) {
				Utils.handleException(e);
			}

			return outJSONString;
		}
	}
}