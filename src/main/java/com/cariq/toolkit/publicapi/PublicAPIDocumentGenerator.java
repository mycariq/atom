/**
 * 
 */
package com.cariq.toolkit.publicapi;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;

import com.atom.www.web.AggregateDataController;
import com.atom.www.web.AtomExporterController;
import com.atom.www.web.AtomLoaderController;
import com.atom.www.web.CarIQProxyController;
import com.atom.www.web.DomainController;
import com.atom.www.web.ExportedDataController;
import com.atom.www.web.ProxyObjectController;
import com.atom.www.web.ServerAuditController;
import com.atom.www.web.TagController;
import com.atom.www.web.TimelineController;
import com.atom.www.web.UserController;
import com.cariq.toolkit.publicapi.swagger.SwaggerPublicAPIDefinition;



/**
 * @author hrishi
 *
 */
public class PublicAPIDocumentGenerator {
	static PublicAPIDocumentGenerator _instance;

	@Resource(name = "configProperties")
	private Properties configProperties;

	private SwaggerPublicAPIDefinition publicAPI;
	private SwaggerPublicAPIDefinition internalAPI;

	static List<Class<?>> controllers = new ArrayList<Class<?>>();

	static {
		
/*		controllers.add(ReportController.class);
		controllers.add(CarIQExporterController.class);
		controllers.add(CarIQCompositeAPIController.class);
		controllers.add(CarIQLoaderController.class);
		controllers.add(WorkItemController.class);
		controllers.add(JobController.class);*/
		
		controllers.add(UserController.class);
		controllers.add(DomainController.class);
		controllers.add(TagController.class);
		controllers.add(TimelineController.class);
		controllers.add(AtomExporterController.class);
		controllers.add(AtomLoaderController.class);
		controllers.add(ProxyObjectController.class);
		controllers.add(ExportedDataController.class);
		controllers.add(CarIQProxyController.class);
		controllers.add(ServerAuditController.class);
		//controllers.add(BillingController.class);
		controllers.add(AggregateDataController.class);
	}

	/**
	 * 
	 */
	public PublicAPIDocumentGenerator() {
		PublicAPIBuilder builder = new PublicAPIBuilder();

		// from configurator, load the Controllers from comma separated list - for now, hardcoded list in static block

		for (Class<?> controller : controllers) {
			builder.add(controller);
		}

		builder.init();
		builder.process();
		builder.postProcess();

		publicAPI = builder.getPublicAPIDefinition();
		internalAPI = builder.getInternalAPIDefinition();
	}

	/**
	 * @return
	 */
	public static PublicAPIDocumentGenerator getInstance() {
		//		// Temp - for debugging
		//		return new PublicAPIDocumentGenerator();
		//		@TODO Temp
		if (_instance == null) {
			_instance = new PublicAPIDocumentGenerator();
		}

		return _instance;
	}

	/**
	 * @return
	 */
	public SwaggerPublicAPIDefinition getPublicAPI() {
		return publicAPI;
	}

	/**
	 * @return
	 */
	public SwaggerPublicAPIDefinition getInternalAPI() {
		return internalAPI;
	}

}