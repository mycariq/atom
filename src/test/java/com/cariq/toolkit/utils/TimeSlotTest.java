package com.cariq.toolkit.utils;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.atom.www.helper.Slot;
import com.atom.www.helper.TimeSlot;

import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
public class TimeSlotTest {
	
	@Test
	public void findMorningSlot() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.HOUR_OF_DAY, 11);
		
		Date date = calendar.getTime();
		
		System.out.println("Hours: " + Utils.getHours(date));
		
		TimeSlot timeSlot = new TimeSlot(date);
		Slot slot = timeSlot.getSlot();
		System.out.println(slot);
		Assert.assertEquals(Slot.MORNING, slot);
	}
	
	@Test
	public void findAfternoonSlot() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.HOUR_OF_DAY, 14);
		
		Date date = calendar.getTime();
		
		System.out.println("Hours: " + Utils.getHours(date));
		
		TimeSlot timeSlot = new TimeSlot(date);
		Slot slot = timeSlot.getSlot();
		System.out.println(slot);
		Assert.assertEquals(Slot.AFTERNOON, slot);
	}
	
	@Test
	public void isAfter() {
		
		//=============================================================================
		//find one timeslot is after other timeslot. Input timeslot having different days 
		Date firstDate = Utils.DateByHoursBack(new Date(), 24 * 7); //seven days back
		Date currentDate = new Date();
		
		TimeSlot firstTimeSlot = TimeSlot.getByDate(firstDate);
		TimeSlot secondTimeSlot = TimeSlot.getByDate(currentDate);
		
		//this is not after. first time slot is before second timeslot....expected to be false
		boolean isAfter = firstTimeSlot.isAfter(secondTimeSlot);
		Assert.assertEquals(false, isAfter);
		//=============================================================================
		//=============================================================================
		//find one timeslot is after other timeslot. Input timeslot having different days 
		Date beforeDate = Utils.DateByHoursBack(new Date(), 24 * 7); //seven days back
		Date secondCurrentDate = new Date();
		
		TimeSlot beforeTimeSlot = TimeSlot.getByDate(beforeDate);
		TimeSlot currentTimeSlot = TimeSlot.getByDate(secondCurrentDate);
		
		//this is after.....expected true
		boolean isItAfter = currentTimeSlot.isAfter(beforeTimeSlot);
		Assert.assertEquals(true, isItAfter);
		//============================================================================
		//find one timeslot is after other timeslot. Input timeslot having same day but different slots
		Calendar currentDateMorning = Calendar.getInstance();
		currentDateMorning.setTime(new Date());
		currentDateMorning.set(Calendar.HOUR_OF_DAY, 12);
		
		Calendar currentDateAfternoon = Calendar.getInstance();
		currentDateAfternoon.setTime(new Date());
		currentDateAfternoon.set(Calendar.HOUR_OF_DAY, 18);
		
		TimeSlot morningTimeSlot = TimeSlot.getByDate(currentDateMorning.getTime());
		TimeSlot afternoonTimeSlot = TimeSlot.getByDate(currentDateAfternoon.getTime());
		
		boolean isAfternoonSlotAfterMorning = afternoonTimeSlot.isAfter(morningTimeSlot);
		Assert.assertEquals(true, isAfternoonSlotAfterMorning);
		
		//============================================================================
		boolean isMorningSlotAfterAfternoon = morningTimeSlot.isAfter(afternoonTimeSlot);
		Assert.assertEquals(false, isMorningSlotAfterAfternoon);
	}
	
	@Test
	public void equals() {
		//both values same
		Date current = new Date();
		TimeSlot morningTimeSlot = TimeSlot.getByDate(current);
		TimeSlot afternoonTimeSlot = TimeSlot.getByDate(current);
		
		boolean actual = morningTimeSlot.equals(afternoonTimeSlot);
		Assert.assertEquals(true, actual);
		//============================================================================
		//two different days
		Date currentDate = new Date();
		Date sevenDaysBack = Utils.DateByHoursBack(new Date(), 24 * 7); //seven days back
		
		TimeSlot firstSlot = TimeSlot.getByDate(currentDate);
		TimeSlot secondSlot = TimeSlot.getByDate(sevenDaysBack);
		
		boolean actualWithDifferentDays = firstSlot.equals(secondSlot);
		Assert.assertEquals(false, actualWithDifferentDays);

		//============================================================================
		//same day but different slot....equals should return false
		Calendar currentDateMorning = Calendar.getInstance();
		currentDateMorning.setTime(new Date());
		currentDateMorning.set(Calendar.HOUR_OF_DAY, 12);
		
		Calendar currentDateAfternoon = Calendar.getInstance();
		currentDateAfternoon.setTime(new Date());
		currentDateAfternoon.set(Calendar.HOUR_OF_DAY, 18);
		
		boolean actualWithdifferentSlots = TimeSlot.getByDate(currentDateMorning.getTime())
				.equals(TimeSlot.getByDate(currentDateAfternoon.getTime()));
		Assert.assertEquals(false, actualWithDifferentDays);
	}
	
	@Test
	public void nextSlot() {
		//Get next slot from morning slot. Expected slot is of same day but afternoon
		Calendar currentDateMorning = Calendar.getInstance();
		currentDateMorning.setTime(new Date());
		currentDateMorning.set(Calendar.HOUR_OF_DAY, 13);
		
		TimeSlot morningSlot = TimeSlot.getByDate(currentDateMorning.getTime());
		TimeSlot nextSlot = morningSlot.nextSlot();
		
		Assert.assertEquals(Slot.EVENING, nextSlot.getSlot());
		//============================================================================
		//Get next slot from afternoon slot. Expected slot is of next day morning slot
		Calendar afternoonDateMorning = Calendar.getInstance();
		afternoonDateMorning.setTime(new Date());
		afternoonDateMorning.set(Calendar.HOUR_OF_DAY, 11);
		
		TimeSlot afternoonSlot = TimeSlot.getByDate(afternoonDateMorning.getTime());
		TimeSlot nextSlotFromAfternoon = afternoonSlot.nextSlot();
		Assert.assertEquals(Slot.AFTERNOON, nextSlotFromAfternoon.getSlot());
		// =======================================================================
		// Get next slot from afternoon slot. Expected slot is of next day
		// morning slot
		Calendar eveningDateMorning = Calendar.getInstance();
		eveningDateMorning.setTime(new Date());
		eveningDateMorning.set(Calendar.HOUR_OF_DAY, 19);
		
		TimeSlot eveningSlot = TimeSlot.getByDate(eveningDateMorning.getTime());
		TimeSlot nextSlotFromEvening = eveningSlot.nextSlot();
		Assert.assertEquals(Slot.MORNING, nextSlotFromEvening.getSlot());
		Assert.assertEquals(eveningSlot.getDay().getNextsDay(), nextSlotFromEvening.getDay());
	}
	
	@Test
	public void getStartTime() {
		//test start time for morning slot
		Calendar morning = Calendar.getInstance();
		morning.setTime(new Date());
		morning.set(Calendar.HOUR_OF_DAY, 3);
		
		TimeSlot morningSlot = TimeSlot.getByDate(morning.getTime());
		Date date = morningSlot.getStartTime();
		Assert.assertEquals(morningSlot.getDay().getStart(), date);
		//========================================================================
		//test start time for afternoon slot
		Calendar afternoon = Calendar.getInstance();
		afternoon.setTime(new Date());
		afternoon.set(Calendar.HOUR_OF_DAY, (TimeSlot.MORNING_SLOT_HOURS + TimeSlot.AFTERNOON_SLOT_HOURS));
		
		TimeSlot afternoonSlot = TimeSlot.getByDate(afternoon.getTime());
		Date afternoonStartTime = afternoonSlot.getStartTime();
		Assert.assertEquals(new Date(afternoonSlot.getDay().getStart().getTime() + Utils.hourMiliSeconds * TimeSlot.MORNING_SLOT_HOURS), afternoonStartTime);
		//========================================================================
		//test start time for evening slot
		Calendar evening = Calendar.getInstance();
		evening.setTime(new Date());
		evening.set(Calendar.HOUR_OF_DAY, 20);
		
		TimeSlot eveningSlot = TimeSlot.getByDate(evening.getTime());
		Date eveningStartTime = eveningSlot.getStartTime();
		Assert.assertEquals(new Date(eveningSlot.getDay().getStart().getTime() + Utils.hourMiliSeconds * (TimeSlot.MORNING_SLOT_HOURS + TimeSlot.AFTERNOON_SLOT_HOURS)), eveningStartTime);
	}
	
	@Test
	public void getEndTime() {
		//test end time for morning slot
		Calendar morning = Calendar.getInstance();
		morning.setTime(new Date());
		morning.set(Calendar.HOUR_OF_DAY, 3);
		
		TimeSlot morningSlot = TimeSlot.getByDate(morning.getTime());
		Date date = morningSlot.getEndTime();
		Assert.assertEquals(new Date(morningSlot.getDay().getStart().getTime() + Utils.hourMiliSeconds * TimeSlot.MORNING_SLOT_HOURS), date);
		//========================================================================
		//test end time for afternoon slot
		Calendar afternoon = Calendar.getInstance();
		afternoon.setTime(new Date());
		afternoon.set(Calendar.HOUR_OF_DAY, (TimeSlot.MORNING_SLOT_HOURS + TimeSlot.AFTERNOON_SLOT_HOURS));
		
		TimeSlot afternoonSlot = TimeSlot.getByDate(afternoon.getTime());
		Date afternoonStartTime = afternoonSlot.getEndTime();
		Assert.assertEquals(new Date(morningSlot.getDay().getStart().getTime() + Utils.hourMiliSeconds * (TimeSlot.MORNING_SLOT_HOURS + TimeSlot.AFTERNOON_SLOT_HOURS)), afternoonStartTime);
		//========================================================================
		//test end time for evening slot
		Calendar evening = Calendar.getInstance();
		evening.setTime(new Date());
		evening.set(Calendar.HOUR_OF_DAY, 19);
		
		TimeSlot eveningSlot = TimeSlot.getByDate(evening.getTime());
		Date eveningEndTime = eveningSlot.getEndTime();
		Assert.assertEquals(eveningSlot.getDay().getEnd(), eveningEndTime);
	}
	
	@Test
	public void getDifferenceInDays() {
		//date having different days
		Date currentDate = new Date();
		Date sevenDaysBack = Utils.DateByHoursBack(currentDate, 24 * 7);//date back seven days
		
		TimeSlot currentSlot = TimeSlot.getByDate(currentDate);
		TimeSlot backSlot = TimeSlot.getByDate(sevenDaysBack);
		
		int diff = currentSlot.getDifferenceInDays(backSlot);
		Assert.assertEquals(8, diff);
		//==================================================
		Date today = new Date();
		TimeSlot todaysSlot = TimeSlot.getByDate(currentDate);
		TimeSlot otherSlot = TimeSlot.getByDate(today);
		
		int diffOfSameDay = todaysSlot.getDifferenceInDays(otherSlot);
		Assert.assertEquals(1, diffOfSameDay);
	}
	
	@Test
	public void getDifferenceInSlots() {
		//difference between slots having different days
		Date currentDate = new Date();
		Date sevenDaysBack = Utils.DateByHoursBack(currentDate, 24 * 7);//date back seven days
		
		TimeSlot currentSlot = TimeSlot.getByDate(currentDate);
		TimeSlot backSlot = TimeSlot.getByDate(sevenDaysBack);
		
		int diff = currentSlot.getDifferenceInSlots(backSlot);
		Assert.assertEquals(8 * TimeSlot.SLOTS_IN_A_DAY, diff);
		//==================================================		
		//difference between slots having same day but different slots
		Calendar morning = Calendar.getInstance();
		morning.setTime(new Date());
		morning.set(Calendar.HOUR_OF_DAY, 3);
		
		Calendar afternoon = Calendar.getInstance();
		afternoon.setTime(new Date());
		afternoon.set(Calendar.HOUR_OF_DAY, 16);
		
		TimeSlot morningSlot = TimeSlot.getByDate(morning.getTime());
		TimeSlot afternoonSlot = TimeSlot.getByDate(afternoon.getTime());
		
		int diffOfSameDay = morningSlot.getDifferenceInSlots(afternoonSlot);
		Assert.assertEquals(2, diffOfSameDay);
	}
}
