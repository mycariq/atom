package com.atom.www.helper;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.Domain.Domain_Out;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.service.DomainAccessService;
import com.cariq.toolkit.utils.Utils;

@Configurable
public class DomainHelper {

	/** Domain service **/
	@Autowired
	private DomainAccessService domainAccessService;

	/**
	 * Get domain of logged in user, It returns first domain from list
	 * 
	 * 1. MOve this into helper. 2. It should take domain name as argument 3. If
	 * domain name is null, then it should return first domain. 4. If given
	 * domain is not present
	 * 
	 * @return {@link Domain_Out}
	 */
	public Domain_Out getDomainOfLoggedInUser(String domainName) {

		if (domainName != null) {
			domainName = Utils.removeWhiteSpaces(domainName);
			return Domain.getByName(domainName).toJSON();
		}

		return getDomainByUser(User.getLoggedInUser());
	}

	public Domain_Out getDomainByUser(User user) {
		// if user is null, return null - it can be admin. Won't have access to any domain specific loader
		if (null == user)
			return null;
		
		// Get domains of this user.
		List<Domain_Out> domains = domainAccessService.getAccessibleDomainsForUser(user.getUsername());

		// Check domain is valid
		if (domains.size() <= 0)
			return null;

		// Get first domain and work on it.
		Domain_Out domain = domains.get(0);

		return domain;
	}
	
	public static Domain getDomainFromUser(User user) {
		DomainHelper domainHelper = new DomainHelper();
		Domain_Out domainOut = domainHelper.getDomainByUser(user);
		Domain domain = Domain.getByName(domainOut.getName());
		
		return domain;
	}
}
