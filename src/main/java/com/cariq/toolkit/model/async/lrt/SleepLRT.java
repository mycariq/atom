//package com.cariq.toolkit.model.async.lrt;
//
//import com.cariq.toolkit.utils.GenericJSON;
//import com.cariq.toolkit.utils.Utils;
//
///**
// * Test timeout by making a request that takes long time to return.
// *
// * @param responseTime
// *            the response time
// * @param response
// *            the response
// * @return the response json
// */
//
//public class SleepLRT extends LRT {
//	public static final String TaskDurationMS = "taskDurationMS";
//
//	@Override
//	protected GenericJSON executeLRT(GenericJSON inputJSON) {
//		Integer taskDurationMS = Utils.getValue(Integer.class, inputJSON, TaskDurationMS);
//		if (taskDurationMS == null)
//			taskDurationMS = 5000;
//
//		Utils.sleep(taskDurationMS);
//
//		return GenericJSON.build("message", "LRT Completed after " + taskDurationMS + " Miliseconds!");
//	}
//}
