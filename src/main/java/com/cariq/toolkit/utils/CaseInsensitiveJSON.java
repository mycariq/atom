package com.cariq.toolkit.utils;

import java.util.Map;
import java.util.TreeMap;

public class CaseInsensitiveJSON extends TreeMap<String, Object> {

	public CaseInsensitiveJSON(Map inputMap) {
		super(String.CASE_INSENSITIVE_ORDER);
		
		for (Object key : inputMap.keySet()) {
			put(key.toString(), inputMap.get(key));
		}
	}

	public Object getAny(String[] tokens) {
		Object retval = null;
		for (String token : tokens) {
			retval = get(token);
			if (retval != null)
				break;
		}
		
		return retval;
	}
	
	

}
