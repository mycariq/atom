package com.atom.www.loader;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderContext;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderTemplate;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.DomainAccess;
import com.cariq.toolkit.model.ProxyObject;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_In;
import com.cariq.toolkit.model.Timeline.Timeline_Out;
import com.cariq.toolkit.service.ProxyObjectService;
import com.cariq.toolkit.service.TimelineService;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.utils.Utils;

@Configurable
public class ProxyUpdater extends CarIQLoaderTemplate{
	
	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("ProxyUpdater");

	private static final String TheLoader = "ProxyUpdater";
	private static final String Description = "Manipulate proxy owner and state and records timeline about action done.";
	private static final String SIGNATURE = "signature";
	private static final String OWNER = "owner";
	private static final String STAGE = "stage";
	private static final String DOMAIN = "domain";
	private static final String TYPE = "type";
	private static final String STATE = "state";
	
	private static final String TIMELINE_TYPE = "system";
	
	/** Proxy Object service to update proxy object **/
	@Autowired
	private ProxyObjectService proxyObjectService;
	
	/** Timeline service to create timeline after updating data **/ 
	@Autowired
	private TimelineService timelineService;
	
	/**
	 * Default constructor
	 */
	public ProxyUpdater() {
		super(TheLoader, Description);
		
		//add all attributes of this loader
		addAttribute(SIGNATURE, "Proxy signature", false);
		addAttribute(OWNER, "Username of owner of proxy", false);
		addAttribute(STAGE, "Stage of Order(Car Connected, Device Delivered....", false);
		addAttribute(DOMAIN, "Name of domain(IL-Assist, Driven, TAGIC, Drivesmart....", false);
		addAttribute(TYPE, "Proxy type", false);
		addAttribute(STATE, "State of Order(Active, Invalid, Snooze...", true);
	}

	@Override
	protected void doLoad(GenericJSON json, CarIQLoaderContext context) {
		logger.debug("doLoad: json : " + json.toString());
		
		//retrive all values from json
		String signature = Utils.getValue(json, SIGNATURE);
		String owner = Utils.getValue(json, OWNER);
		String stage = Utils.getValue(json, STAGE);
		String domainName = Utils.getValue(json, DOMAIN);
		String type = Utils.getValue(json, TYPE);
		String state = Utils.getValue(json, STATE);
		
		//Check given domain exists or not
		Domain domain = Domain.getByName(domainName);
		if(domain == null) {
			context.addMessage("Domain not found for domain name : " + domainName);
			return;
		}
		//Check given domain is accessible to given user
		if(DomainAccess.getObjectByDomainUSer(domain, User.getByUsername(owner)) == null) {
			logger.debug("Domain " + domain.getName() + " is not accessible for given user " + owner);
			context.addMessage("Domain " + domain.getName() + " is not accessible for given user " + owner);
			return;
		}
		
		//If owner not provided and valid state provided then just mark proxy
		//and return.
		if(owner == null && state != null) {
			ProxyObject_In proxyObjectIn = new ProxyObject_In(domainName, null, type, signature, owner, domain.getId());
			// message for timeline
			String message = "Marked as " + state;
			addTimeline(proxyObjectIn, message, TIMELINE_TYPE, state, stage);
			return;
		}
		
		//else update owner, mark proxy and add task assignment message on timeline
		createOrUpdateProxy(domain, type, signature, owner, stage, state);
	}
	
	/**
	 * Create or update proxy and write timeline for same activity
	 * 
	 * @param domainNane
	 * @param proxyType
	 * @param signature
	 * @param owner
	 * @param domainId
	 */
	private void createOrUpdateProxy(Domain domain, String proxyType, String signature, String owner, String stage, String tag) {
		logger.debug("createOrUpdateProxy: domainName: " + domain.getName() + ", signature: " + signature
				+ ", owner: " + owner + ", domainId: " + domain.getId() + ", stage: " + stage);
		
		//create proxyObject input object
		ProxyObject_In proxyObjectIn = new ProxyObject_In(domain.getName(), null, proxyType, signature, owner, domain.getId());
		
		//create or get proxy object
		ResponseWithIdJson response = proxyObjectService.createOrGetProxyObject(proxyObjectIn);
		if(response != null) 
			logger.debug(response.toString());
		
		//message for timeline
		String message = owner == null ? "Task Assigned to owner none" : "Task assigned to owner @" + owner; 
		addTimeline(proxyObjectIn, message, TIMELINE_TYPE, tag, stage);
	}
	
	/**
	 * Add timeline
	 * 
	 * @param proxyObject
	 * @param message
	 * @param type
	 * @param tag
	 * @param state
	 * @param username
	 * @param domainId
	 */
	private void addTimeline(ProxyObject_In proxyObject, String message, String type, String tag, String stage) {
		logger.debug("addTimeline: message: " 
			+ message + ", type: " + type + ", tag: " +	tag + ", stage: " + stage);
		
		String username = Utils.getCurrentUserName();
		
		//create timeline
		timelineService.addSimpleTimeline(proxyObject, message, type, stage, tag, username);		
	}
}
