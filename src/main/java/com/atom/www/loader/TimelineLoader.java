package com.atom.www.loader;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.cariq.toolkit.coreiq.exporter.CarIQExporterWrapper;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderContext;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderTemplate;
import com.cariq.toolkit.model.ProxyObject;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_In;
import com.cariq.toolkit.model.Timeline;
import com.cariq.toolkit.model.Timeline.Timeline_In;
import com.cariq.toolkit.service.TimelineService;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.utils.Utils;


/**
 * TimelineLoader to load timelines in Atom. Timeline include information about 
 * SMS, Email, Normal comment, phone call or Feedback type comment which were 
 * communicated with user
 * 
 * 
 * @author sagar
 *
 */
@Configurable
public class TimelineLoader extends CarIQLoaderTemplate {

	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("TimelineLoader");
	
	private static final String TIMELINE_LOADER = "TimelineLoader";
	private static final String LoaderDescription = "Timeline to load comments, phone calls, sms which sent to the user while communication with user";
	
	private static final String TAG = "Tag";
	private static final String TYPE = "Type";
	private static final String COMMENT = "Comment";
	private static final String DATE = "Date";
	private static final String POLICY_NO = "Policy No.";
	private static final String USERNAME = "username";
	private static final String STAGE = "stage";
	
	private static final String DEFAULT_TYPE = "Comment";
	
	private static final String KEY_domainId = "domainId";
	private static final String KEY_state = "State";
	
	private static final String KEYWORD_SEARCH = "Keyword search";
	private static final String KEYWORD_QUERY_TYPE = "KeywordSearch";
	
	private static final String PROXY_TYPE = "InsurancePolicy";
	
	private static final String wrappedExporterName = "StagingAndInsurancePolicyDetailsExporter";
	
	private static final String wrappedExporterDescription = "This is Onboarding Report";
	
	private static final String STAGE_NOT_VERIFIED = "Not Verified";
	private static final String STAGE_USER_CREATED = "User Created";
	private static final String STAGE_DEVICE_REQUESTED = "Device Requested";
	private static final String STAGE_DEVICE_SHIPPED = "Device Shipped";
	private static final String STAGE_DEVICE_ONBOARDED = "Device OnBoarded";
	private static final String STAGE_CAR_CONNECTED = "Device OnBoarded";
	
	private CarIQExporterWrapper wrapper;
	
	@Autowired
	private TimelineService timelineService;
	
	public TimelineLoader() {
		super(TIMELINE_LOADER, LoaderDescription);
		
		addAttribute(POLICY_NO, "Policy number of user", false);
		addAttribute(DATE, "Date of timeline", false);
		addAttribute(COMMENT, "Comment against this user. It can be raw text of sms, email, or normal comment", false);
		addAttribute(TYPE, "Type of timeline(Phone Call, SMS, Email, Comment)", true);
		addAttribute(TAG, "Tag for this timeline if any", true);
		addAttribute(STAGE, "Stage of the user when comment added", false);
		addAttribute(USERNAME, "Username of Atom user who has noted this timeline");
		
		wrapper = new CarIQExporterWrapper(wrappedExporterName, wrappedExporterDescription);
	}

	@Override
	protected void doLoad(GenericJSON json, CarIQLoaderContext context) {

		// Retrive all raw information of timeline
		String policyNo = (String) json.get(POLICY_NO);
		String username = (String) json.get(USERNAME);
		String tag = (String) json.get(TAG);
		String type = (String) json.get(TYPE);
		String timelineDate = (String) json.get(DATE);
		String comment = (String) json.get(COMMENT);
		String stage = Utils.getValue(json, STAGE);
		try {

			GenericJSON policyJSON = getPolicyByPolicyNo(policyNo, username);

			if (policyJSON == null) {
				String errorMsg = "Error while loading timeline, policy not found with policyNumber " + policyNo;
				context.addMessage(errorMsg);
				logger.error(errorMsg);
				return;
			}

			Date date = Utils.getDate(timelineDate);
			
			//if input stage is not availble then find stage based
			//on user current details(like onboarding dates)
			if(stage == null)
				stage = getTimelineStage(policyJSON, date);
		
			//if input type is not available then set to Comment
			if(type == null)
				type = DEFAULT_TYPE;
			Long domainId = (Long) policyJSON.get(KEY_domainId);

			ProxyObject_In proxyObject = new ProxyObject.ProxyObject_In(null, null, PROXY_TYPE, policyNo,
					username, domainId);

			// Create timeline and save it
			Timeline_In timeline = new Timeline.Timeline_In(proxyObject, comment, type, tag, stage, username,
					new Long(domainId), date, null);

			ResponseWithIdJson idJSON = timelineService.addTimelineItem(timeline);
			logger.debug("Timeline Added successfuly with id : " + idJSON.toString());
		} catch (Exception e) {
			e.printStackTrace();
			String errorMsg = "Error while loading timeline for policyNo " + policyNo + " of type " + type
					+ " with comment : " + comment + ". Error : " + e.getMessage();
			logger.error(errorMsg);
			context.addMessage(errorMsg);
		}
	}

	
	/**
	 * Get timeline stage by using timeline date. This timeline date will be used to find
	 * exactly at which stage given conversation happened. This way we can find stage.
	 * 
	 * @param policyJSON		Policy JSON containing dates of various stages for comparison
	 * @param timelineDate		timeline date to compare
	 * @return					Stage in string.
	 */
	private String getTimelineStage(GenericJSON policyJSON, Date timelineDate) {

		Date policyCreateOn = Utils.getDate((String) policyJSON.get("Policy_CreationTime"));
		Date deviceRequestedOn = Utils.getDate((String) policyJSON.get("Shipped_Or_Delivered_On"));
		Date deviceOnboardedOn = Utils.getDate((String) policyJSON.get("Device_OnboardedOn"));
		Date carLastSeenOn = Utils.getDate((String) policyJSON.get("Car_lastSeenOn"));
		
		//If all dates are null then we cannot find in which stage this timeline happened
		//in this case we will return current stage of timeline
		if(policyCreateOn == null && deviceRequestedOn == null && deviceOnboardedOn == null 
				&& carLastSeenOn == null)
			return (String) policyJSON.get(KEY_state);
		
		if(deviceRequestedOn != null && deviceRequestedOn.after(timelineDate)) 
			return STAGE_USER_CREATED;
		if(deviceOnboardedOn != null && deviceOnboardedOn.after(timelineDate))
			return STAGE_DEVICE_REQUESTED;
		if(carLastSeenOn != null && carLastSeenOn.after(timelineDate))
			return STAGE_DEVICE_ONBOARDED;
		if(carLastSeenOn != null && carLastSeenOn.before(timelineDate))
			return STAGE_CAR_CONNECTED;
		
		//Default stage not verified
		return STAGE_NOT_VERIFIED;
	}

	
	
	/**
	 * Get policy JSON by calling REST API to CarIQ
	 * 
	 * @param policyNo		policy JSON
	 * @param username		username for wrapper to find on which domain this request should go.
	 * @return				Policy JSON
	 */
	private GenericJSON getPolicyByPolicyNo(String policyNo, String username) {

		//Add policy number to be searched for post request
		GenericJSON genericJSON = new GenericJSON();
		genericJSON.put(KEYWORD_SEARCH, policyNo);
		genericJSON.put(USERNAME, username);
		
		//get list of policies and return first policy from this list
		List<GenericJSON> searchResult = wrapper.doExport(KEYWORD_QUERY_TYPE, genericJSON, 1, 10);
		
		if(searchResult == null || searchResult.isEmpty())
			return null;
		
		return searchResult.get(0);
	}

}
