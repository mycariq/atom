package com.atom.www.web;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.atom.www.json.LoginRequestJson;
import com.atom.www.json.MessageJson;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.service.UserAuthenticationService;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.utils.Utils;

@CarIQPublicAPI(name = "Authentication", description = "user authentication")
@Controller
public class AuthenticationController {

	/** The authentication service. */
	@Autowired
	private UserAuthenticationService authenticationService;

	
	/**
	 * Authenticate.
	 *
	 * @param loginRequestJson
	 *            the login request json
	 * @param result
	 *            the result
	 * @param request
	 *            the request
	 * @return the object
	 */
	@CarIQPublicAPIMethod(description = "Authenticate User", responseClass = MessageJson.class)
	@RequestMapping(method = RequestMethod.POST, value = "/authenticate")
	public @ResponseBody Object authenticate(@RequestBody @Valid LoginRequestJson loginRequestJson,
			BindingResult result, HttpServletRequest request) {
		try (CarIQAPI _authenticate = new CarIQAPI("CARIQ_authenticate", loginRequestJson.getUsername())) {

			User.User_Out user = authenticationService.getUserAuth(loginRequestJson.getUsername(),
					loginRequestJson.getPassword());
			if (user == null) {
				Utils.handleException("Invalid Username and Password");
			}
			return user;
		}
	}
}
