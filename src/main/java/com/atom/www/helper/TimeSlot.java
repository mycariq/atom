package com.atom.www.helper;

import java.util.Date;
import java.util.List;

import com.cariq.toolkit.utils.Day;
import com.cariq.toolkit.utils.Utils;

/**
 * Timeslot captures slot in a day.
 * @author sagar
 *
 */
public class TimeSlot {
	
	public static final int SLOTS_IN_A_DAY = 3;
	public static final int MORNING_SLOT_HOURS = 12;
	public static final int AFTERNOON_SLOT_HOURS = 3;
	public static final int EVENING_SLOT_HOURS = 9;
	
	/** A day **/
	private Day day;
	
	/** Slot in a day **/
	private Slot slot;
	
	/**
	 * Parameterized constructor
	 * @param date
	 */
	public TimeSlot(Date date) {
		day = new Day(date, null);
		int hours = Utils.getDiffInHours(date, day.getStart());
		slot = findSlot(hours);
	}
	
	/**
	 * Parameterized constructor
	 * @param day
	 * @param slot
	 */
	public TimeSlot(Day day, Slot slot) {
		this.day = day;
		this.slot = slot;
	}
	
	/**
	 * Find slot from date
	 * @param date
	 * @return
	 */
	public static Slot findSlot(int hours) {
		if(hours < MORNING_SLOT_HOURS)
			return Slot.MORNING;
		else if(hours < (MORNING_SLOT_HOURS + AFTERNOON_SLOT_HOURS))
			return Slot.AFTERNOON;	
		return Slot.EVENING;
	}

	/**
	 * Get day of timeslot
	 * @return
	 */
	public Day getDay() {
		return day;
	}

	/**
	 * Get timeslot based on start time and endtime
	 * @return
	 */
	public Slot getSlot() {
		return slot;
	}
	
	/**
	 * Is current timeslot is after given timeslot
	 * @param other
	 * @return
	 */
	public boolean isAfter(TimeSlot other) {
		//If given timeslot is equal to this timeslot then return false
		if(equals(other))
			return false;
		
		return Utils.getDiffInHours(getStartTime(), other.getStartTime()) > 0;
	}
	
	/**
	 * Is given timeslot equals current Timeslot
	 * @param other
	 * @return
	 */
	public boolean equals(TimeSlot other) {
		if (this == other)
			return true;
		if (other == null)
			return false;
		if (getClass() != other.getClass())
			return false;
		if(!getDay().equals(other.getDay()))
			return false;
		if(!getSlot().equals(other.getSlot()))
			return false;
		return true;
	}
	
	/**
	 * Get next timeslot
	 * @return
	 */
	public TimeSlot nextSlot() {
		if(getSlot().equals(Slot.MORNING))
			return new TimeSlot(getDay(), Slot.AFTERNOON);
		else if(getSlot().equals(Slot.AFTERNOON))
			return new TimeSlot(getDay(), Slot.EVENING);
		return new TimeSlot(getDay().getNextsDay(), Slot.MORNING);
	}
	
	/**
	 * Get start time
	 * @return
	 */
	public Date getStartTime() {
		//get start time for morning slot
		if(getSlot().equals(Slot.MORNING))
			return this.day.getStart();
		//find start time for afternoon slot
		else if(getSlot().equals(Slot.AFTERNOON))
			return Utils.getDateHoursAfter(day.getStart(), MORNING_SLOT_HOURS); 
		
		//get start time for evening slot
		return Utils.getDateHoursAfter(day.getStart(), MORNING_SLOT_HOURS + AFTERNOON_SLOT_HOURS);
	}
	
	/**
	 * Get end time
	 * @return
	 */
	public Date getEndTime() {
		//get end time for morning slot
		if(getSlot().equals(Slot.MORNING))
			return Utils.getDateHoursAfter(day.getStart(), MORNING_SLOT_HOURS);
		//get end time for afternoon slot
		else if(getSlot().equals(Slot.AFTERNOON)) 
			return Utils.getDateHoursAfter(day.getStart(), MORNING_SLOT_HOURS + AFTERNOON_SLOT_HOURS);
		//get end time for evening slot
		return day.getEnd();
	}
	
	/**
	 * Get timeslot by date
	 * 
	 * @param date
	 * @return
	 */
	public static TimeSlot getByDate(Date date) {
		return new TimeSlot(date);
	}
	
	/**
	 * Get current timeslot
	 * @return
	 */
	public static TimeSlot getCurrentSlot() {
		return new TimeSlot(new Date());
	}
	
	/**
	 * Get difference in days between this timeslot and other timeslot
	 * 
	 * @param other
	 * @return
	 */
	public int getDifferenceInDays(TimeSlot other) {
		List<Day> days = Day.getDays(getDay().getStart(), other.getDay().getEnd());
		return days.size();
	}
	
	/**
	 * Get difference between two timeslots in slots
	 * @param other
	 * @return
	 */
	public int getDifferenceInSlots(TimeSlot other) {
		if (equals(other))
			return 0;
		
		int differenceInDays = getDifferenceInDays(other);
		if(differenceInDays <= 1)
			return SLOTS_IN_A_DAY - 1;
		return differenceInDays * SLOTS_IN_A_DAY;
	}

	@Override
	public String toString() {
		return "Slot: " + slot.name() +
				", StartTime: " + getStartTime().toString() +
				", EndTime: " + getEndTime().toString();
	}
	
	
}
