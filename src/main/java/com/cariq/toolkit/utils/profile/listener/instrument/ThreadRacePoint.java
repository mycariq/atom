package com.cariq.toolkit.utils.profile.listener.instrument;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.annotation.Resource;

import com.cariq.toolkit.utils.profile.BasicCSVRendererIfc;
import com.cariq.toolkit.utils.profile.ProfActivity;
import com.cariq.toolkit.utils.profile.listener.DiagnosticInstrument;
import com.cariq.toolkit.utils.profile.listener.DiagnosticInstrumentFactory;

/**
 * This ThreadRacePoint will simulate racing between threads at the points
 * defined in code through ProfilePoint. To simulate race ,we need to define the
 * racePoint [name of ProfilePoint], no of participants, and timeout period.
 * These will be defined in the wt.properties under the
 * 'com.cariq.utils.profilepoint.profile.ThreadRacePoint.Race' property. The
 * format of the property will be 'ProfActivity__ImportFetch:4:100' where
 * 'ProfActivity__ImportFetch' will be profActivityName , 4 - no of
 * participants, and 100 Timeout in milliseconds.
 *
 */
public class ThreadRacePoint extends DiagnosticInstrumentFactory {

	protected static BasicCSVRendererIfc renderer = null;
	private static String[] singleRaceInfo = null;
	private static Map<String, ThreadRace> raceMap = new ConcurrentHashMap<String, ThreadRace>();

	// HVN TODO - do it formally
	@Resource(name = "cariqProfileProperties")
	private static Properties cariqProfileProperties;

	/**
	 * Read the property from the wt.properties file and create the
	 * corresponding number of ThreadRace definitions and store it Map for
	 * retrieved.
	 */
	static {
		try {
			String racesInfo = null;

			racesInfo = cariqProfileProperties
					.getProperty("com.cariq.utils.profilepoint.profile.ThreadRacePoint.Race");
			singleRaceInfo = racesInfo.split(",");

			if (singleRaceInfo != null) {

				for (String raceInfo : singleRaceInfo) {
					if ((raceInfo != null) && (!raceInfo.isEmpty())) {
						String[] info = raceInfo.split(":");
						if ((info == null) || (info.length < 3)) {
							continue;
						}
						ThreadRace race1 = new ThreadRace(info[0],
								Integer.parseInt(info[1]),
								Integer.parseInt(info[2]), renderer);
						raceMap.put(info[0], race1);
					}
				}
			}
		} catch (Throwable execption) {
			execption.printStackTrace(System.err);
			throw new ExceptionInInitializerError(execption);
		}

	}

	public ThreadRacePoint(BasicCSVRendererIfc renderer) {
		super(renderer);
		ThreadRacePoint.renderer = renderer;
		renderer.render("ThreadPoint", "ThreadName", "State", "Time(long)");
	}

	/**
	 * This method is invoked when the ProfilePoint is hit. If the race is
	 * defined in the properties , it will be present in the Map. The
	 * corresponding ThreadRace object is retrieved from the Map and the
	 * startparticipant Method is invoked on it.
	 */
	@Override
	public DiagnosticInstrument getDiagnosticInstrument(
			ProfActivity rootActivity, String newAction) {
		ThreadRace threadRace = null;
		if (raceMap.containsKey(newAction)) {
			threadRace = raceMap.get(newAction);
			threadRace.startParticipant();
		}

		return threadRace;

	}

	public void close() throws Exception {

	}

}

/**
 * This class represents a ThreadRaceDefinition. Every Threadrace will have an
 * cyclicBarrier at which the threads will wait for all the participants to
 * arrive or timeout after the timeout specified is over.
 * 
 */
class ThreadRace extends DiagnosticInstrument {
	String name;
	CyclicBarrier participantBarrier;
	int participants;
	int timeout;

	public ThreadRace(String raceName, int participants, int timeout,
			BasicCSVRendererIfc renderer) {
		super(renderer);
		this.name = raceName;
		this.participants = participants;
		this.timeout = timeout;
		participantBarrier = new CyclicBarrier(participants);
	}

	String getName() {
		return name;
	}

	/**
	 * This will be for the participants. They will call this method and wait at
	 * the barrier
	 */
	void startParticipant() {
		try {

			ThreadRacePoint.renderer.render("ThreadPoint", Thread
					.currentThread().getName(), "Waiting", String
					.valueOf(System.currentTimeMillis()));
			participantBarrier.await(timeout, TimeUnit.MILLISECONDS);
			ThreadRacePoint.renderer.render("ThreadPoint", Thread
					.currentThread().getName(), "Started", String
					.valueOf(System.currentTimeMillis()));
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (BrokenBarrierException e) {
			participantBarrier.reset();
		} catch (TimeoutException e) {
			ThreadRacePoint.renderer.render("ThreadPoint", Thread
					.currentThread().getName(), "TimeOut", String
					.valueOf(System.currentTimeMillis()));
			e.printStackTrace();
		}
	}

	@Override
	public String getDescription() {
		return "Organizes the Race among threads";
	}

}
