package com.cariq.toolkit.service;

import java.util.List;

import com.atom.www.json.PolicyRenewalPercentageJSON;
import com.cariq.toolkit.utils.GenericJSON;

/**
 * The Interface AggregateDataService.
 */
public interface AggregateDataService {

	/**
	 * Gets the total counts.
	 *
	 * @param domainName the domain name
	 * @return the total counts
	 */
	GenericJSON getTotalCounts(String domainName);

	/**
	 * Gets the statistics data.
	 *
	 * @param domainName the domain name
	 * @param context the context
	 * @return the statistics data
	 */
	List<GenericJSON> getStatisticsData(String domainName, String context);

	/**
	 * Gets the policy renewal percentage.
	 *
	 * @param domain the domain
	 * @param fromDate the from date
	 * @param toDate the to date
	 * @return the policy renewal percentage
	 */
	PolicyRenewalPercentageJSON getPolicyRenewalPercentage(String domain, String fromDate, String toDate);
}