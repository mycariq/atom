package com.cariq.toolkit.coreiq.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.utils.APIContext;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.HtmlValueRenderer;
import com.cariq.toolkit.utils.SimplePOJOs.CountPOJO;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * Wrapper Exporter template uses CarIQExporterWrapper to export data and
 * exporter definition from CarIQ.
 * 
 * @author sagar
 *
 */
public abstract class CarIQWrappedExporterTemplate implements CarIQExporter {

	protected CarIQExporterDefinition definition;

	private String name;
	private String description;
	private String shortName;

	public CarIQWrappedExporterTemplate(String name, String description) {
		this(name, name, description);
	}

	public CarIQWrappedExporterTemplate(String name, String shortName, String description) {
		super();
		this.name = name;
		this.description = description;
		definition = new CarIQExporterDefinition(name, description);
		this.shortName = shortName;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public CarIQExporterDefinition getExporterDefinition() {
		CarIQExporterDefinition wrappedDefinition = getWrapper().getExporterDefinition();

		List<String> wrappedAttrList = wrappedDefinition.getExportAttrDefinition();
		List<CarIQQueryDefinition> queryDefinitions = wrappedDefinition.getQueryDefinitions();
		
		//Add all query definitions
		for (String exportAttributeName : wrappedAttrList)
			definition.add(CarIQExporterDefinition.EXPORT_PARAM, exportAttributeName);

		//Add all query definitions
		for (CarIQQueryDefinition def : queryDefinitions)
			definition.add(CarIQExporterDefinition.QUERY_DEF, def);

		return definition;
	}

	@Override
	public boolean isAuthorised() {
		return true;
	}

	protected void addQueryType(String name, String description, String[] attrList) {
		definition.add(CarIQExporterDefinition.QUERY_DEF, new CarIQQueryDefinition(name, description, attrList));
	}

	protected void addQueryType(String name, String description, List<QueryDefinitionAttribute> attrList) {
		definition.add(CarIQExporterDefinition.QUERY_DEF, new CarIQQueryDefinition(name, description, attrList));
	}

	protected void addExportAttribute(String exportAttributeName) {
		definition.add(CarIQExporterDefinition.EXPORT_PARAM, exportAttributeName);
	}

	protected void addExportAttributes(List<String> exportAttributeNames) {
		for (String name : exportAttributeNames) {
			addExportAttribute(name);
		}
	}

	protected void addSupportedRole(String supportedRole) {
		definition.add(CarIQExporterDefinition.ROLE, supportedRole);
	}

	protected void addSupportedRoles(List<String> supportedRoles) {
		for (String name : supportedRoles) {
			addSupportedRole(name);
		}
	}

	@Override
	public List<GenericJSON> export(String queryType, GenericJSON json, int pageNo, int pageSize) {
		try (ProfilePoint _export = ProfilePoint
				.profileAction("ProfAction_export_" + this.getClass().getCanonicalName())) {
			List<GenericJSON> list = getEnhancer().enhance(queryType, json,
					doExport(queryType, json, pageNo, pageSize));
			if (list == null) {
				list = new ArrayList<>();
			}
			return list;
		}
	}

	@Override
	public HtmlValueRenderer getHtmlRenderer() {
		return getWrapper().getHtmlRenderer();
	}

	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		try (ProfilePoint _doExport_ = ProfilePoint
				.profileAction("ProfAction_doExport_" + this.getClass().getCanonicalName())) {
			try (ProfilePoint _postProcess = ProfilePoint
					.profileAction("ProfAction_doExportProcess_" + this.getClass().getCanonicalName())) {
				return postProcess(queryType, json,	getWrapper()
						.doExport(StagingAndPolicyHelper.getWrappedQueryType(queryType),
								StagingAndPolicyHelper.getWrappedJson(queryType, json, pageNo, pageSize), pageNo, pageSize));
			}
		}
	}

	protected abstract CarIQExporterWrapper getWrapper();

	protected GenericJSON getInput(GenericJSON json) {
		return json;
	}

	protected List<GenericJSON> postProcess(String queryType, GenericJSON inputJson, List<GenericJSON> outputJson) {
		return outputJson;
	}

	// Enhancer
	protected ExporterDataEnhancer getEnhancer() {
		return new UnityExportEnhancer();
	}

	@Override
	public List<CarIQExporter> getChildExporters() {
		return Arrays.<CarIQExporter>asList(this);
	}

	@Override
	public List<GenericJSON> exportRecords(String queryType, GenericJSON json, int startIndex, int endIndex) {
		try (ProfilePoint _exporter_export = ProfilePoint
				.profileActivity("ProfActivity_exporter_export_" + this.getClass().getName())) {
			setApiContext("ExportRecords", "Exporter", Utils.UTC);

			json = Utils.trimStringValues(json);
			return getEnhancer().enhance(queryType, json, doExportRecords(queryType, json, startIndex, endIndex));
		}
	}

	protected List<GenericJSON> doExportRecords(String queryType, GenericJSON json, int startIndex, int endIndex) {
		int pageSize = endIndex - startIndex;
		int pageNo = startIndex / pageSize;

		return doExport(queryType, json, pageNo, pageSize);
	}

	@Override
	public CountPOJO getExportCount(String queryType, GenericJSON json) {
		return new CountPOJO(-1);
	}

	private void setApiContext(String operation, String userName, String timeZone) {
		APIContext context = new APIContext();
		context.setValue(APIContext.USER_NAME, userName);
		context.setValue(APIContext.OPERATION, operation);
		context.setValue(APIContext.TIMEZONE, timeZone);
		APIContext.setContext(context);
	}

	@Override
	public String getShortName() {
		return this.shortName;
	}
}