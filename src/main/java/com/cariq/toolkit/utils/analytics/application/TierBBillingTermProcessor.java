package com.cariq.toolkit.utils.analytics.application;

import com.cariq.toolkit.model.BillingTerm;
import com.cariq.toolkit.model.InvoiceUnit;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.google.common.base.Strings;

public class TierBBillingTermProcessor extends BillingTermProcessor {
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("TierBBillingTermProcessor");
	
	
	private static final String BILLING_TERM_DEF_THRESHOLD_PERCENTAGE = "ThresholdPercentage";
	private static final String BILLING_TERM_DEF_THRESHOLD_CONDITION_KEY = "ThresholdConditionKey";
	private static final String BILLING_TERM_DEF_THRESHOLD_CONDITION_VALUE = "ThresholdConditionValue";
	
	public static final String TIERB_IU_DETAILS_PERCENTAGE = "Percentage";
	public static final String TIERB_IU_DETAILS_DEVICES_SHIPPED = "Devices Shipped";
	public static final String TIERB_IU_DETAILS_CAR_CONNECTED = "Car Connected";
	

	private Integer numberOfInvoicedBillablesEligibleForThresholdCondition = 0;
	
	public TierBBillingTermProcessor(BillingTerm bt, InvoiceUnit iu) {
		super(bt, iu);
	}
	
	private boolean recordMatchedThresholdCondition(GenericJSON inputJSON) {
		GenericJSON defJSON = GenericJSON.fromString(getModel().getDefinitionJson());
		
		// If condition key-value is provided, then only match the condition. Else return true.
		String btConditionKey = Utils.getValue(defJSON, BILLING_TERM_DEF_THRESHOLD_CONDITION_KEY);
		if (! Strings.isNullOrEmpty(btConditionKey)) {
			String btConditionValue = Utils.getValue(defJSON, BILLING_TERM_DEF_THRESHOLD_CONDITION_VALUE);
			if (! Strings.isNullOrEmpty(btConditionValue)) {
				String recordConditionValue = Utils.getValue(inputJSON, btConditionKey);
				if (! recordConditionValue.equalsIgnoreCase(btConditionValue))
					return false;
			}
		}
		
		return true;
	}
	
	@Override
	protected boolean isSpecialConditionKey(String btKey) {
		if (btKey.equalsIgnoreCase(BILLING_TERM_DEF_ANCHOR_MONTH_KEY)
				|| btKey.equalsIgnoreCase(BILLING_TERM_DEF_PROCESSORCLASS_KEY)
				|| btKey.equalsIgnoreCase(BILLING_TERM_DEF_THRESHOLD_PERCENTAGE)
				|| btKey.equalsIgnoreCase(BILLING_TERM_DEF_THRESHOLD_CONDITION_KEY)
				|| btKey.equalsIgnoreCase(BILLING_TERM_DEF_THRESHOLD_CONDITION_VALUE))
			return true;
		return false;
	}
	
	@Override
	protected boolean isRecordApplicableForBillingTerm(GenericJSON inputJSON) {	
		// Validate the required keys in definition with input record 
		for (String btKey : defJSON.keySet()) {
			if (isSpecialConditionKey(btKey))
				continue;
			
			if (!matchKeyValue(btKey, inputJSON))
				return false;
		}
		
		numberOfInvoicedBillables += 1;
		if (recordMatchedThresholdCondition(inputJSON)) {
			numberOfInvoicedBillablesEligibleForThresholdCondition += 1;
			//logger.debug("***Matched policy with Special condition: " + Utils.getValue(inputJSON, TAT_COL_POLICY_NUMBER));
		}
		//logger.debug("***Matched policy: " + Utils.getValue(inputJSON, TAT_COL_POLICY_NUMBER));
		return true;
	}

	@Override
	protected void validateFinishingCriteria() {
		// Validate threshold criteria for its billing term.
		if (null != defJSON && ! Strings.isNullOrEmpty( Utils.getValue(defJSON, BILLING_TERM_DEF_THRESHOLD_PERCENTAGE))) {
			logger.debug("==> Checking threshold condition for iu: " + invoiceUnit.getId() + ", bt: " + getModel().getName());
			logger.debug("==> numberOfInvoicedBillables: " + numberOfInvoicedBillables 
					+ ", numberOfInvoicedBillablesEligibleForThresholdCondition: " + numberOfInvoicedBillablesEligibleForThresholdCondition);
			int thresholdPercentageRequired = Integer.parseInt(Utils.getValue(defJSON, BILLING_TERM_DEF_THRESHOLD_PERCENTAGE));
			int percentage = (int)Math.round((Double.parseDouble(numberOfInvoicedBillablesEligibleForThresholdCondition.toString()) / 
					Double.parseDouble(numberOfInvoicedBillables.toString())) * 100);	
			logger.debug("==> Tier-B connection percentage: " + percentage);
			// Preserve the details in IU
			GenericJSON iu_details = GenericJSON.build(TIERB_IU_DETAILS_PERCENTAGE, percentage, 
					TIERB_IU_DETAILS_DEVICES_SHIPPED, numberOfInvoicedBillables, 
					TIERB_IU_DETAILS_CAR_CONNECTED, numberOfInvoicedBillablesEligibleForThresholdCondition);
			invoiceUnit.setinfoJson(iu_details.toString());
			invoiceUnit.merge();
			if (percentage < thresholdPercentageRequired)
				throw new RuntimeException("THRESHOLD_CRITERIA_FAILED for billing term: " + getModel().getName());
			logger.debug("==> THRESHOLD_CRITERIA_PASSED for bt: " + getModel().getId() + ", threshold_percentage: " + percentage);
		} 
	}
}
