package com.cariq.toolkit.coreiq.tableUtil.impl;

import com.cariq.toolkit.coreiq.tableUtil.interfaces.CellAddressIfc;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.GenericJsonable;
import com.cariq.toolkit.utils.Utils;

public class CellAddress implements CellAddressIfc, GenericJsonable {
	NameIndexIdentifier reportIdentifier, tableIdentifier, rowIdentifier, columnIdentifier;
	
	public CellAddress(NameIndexIdentifier reportIdentifier, NameIndexIdentifier tableIdentifier,
			NameIndexIdentifier rowIdentifier, NameIndexIdentifier columnIdentifier) {
		super();
		this.reportIdentifier = reportIdentifier;
		this.tableIdentifier = tableIdentifier;
		this.rowIdentifier = rowIdentifier;
		this.columnIdentifier = columnIdentifier;
	}

	// To construct out of GenericJSON
	public CellAddress() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.tableUtil.impl.CellAddressIfc#getReportIdentifier()
	 */
	@Override
	public NameIndexIdentifier getReportIdentifier() {
		return reportIdentifier;
	}

	public void setReportIdentifier(NameIndexIdentifier reportIdentifier) {
		this.reportIdentifier = reportIdentifier;
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.tableUtil.impl.CellAddressIfc#getTableIdentifier()
	 */
	@Override
	public NameIndexIdentifier getTableIdentifier() {
		return tableIdentifier;
	}

	public void setTableIdentifier(NameIndexIdentifier tableIdentifier) {
		this.tableIdentifier = tableIdentifier;
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.tableUtil.impl.CellAddressIfc#getRowIdentifier()
	 */
	@Override
	public NameIndexIdentifier getRowIdentifier() {
		return rowIdentifier;
	}

	public void setRowIdentifier(NameIndexIdentifier rowIdentifier) {
		this.rowIdentifier = rowIdentifier;
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.tableUtil.impl.CellAddressIfc#getColumnIdentifier()
	 */
	@Override
	public NameIndexIdentifier getColumnIdentifier() {
		return columnIdentifier;
	}

	public void setColumnIdentifier(NameIndexIdentifier columnIdentifier) {
		this.columnIdentifier = columnIdentifier;
	}

	@Override
	public GenericJSON toGenericJSON() {
		Utils.notImplementedException("CellAddress.toGenericJSON");
		return null;
	}

	/**
	 * JSON of following kind should be loaded into address 
	 * { "table": "Table 1 -ABC-XYZ Data", "row": 4}
	 * In addition Column may be given, Report may be given and Row Name may be given
	 * Since we are developing generic engine, everything should be loadable.
	 * Don't think procedurally. There are beautiful cascadable arrangement of everything.
	 */

	@Override
	public void fromGenricJSON(GenericJSON json) {
		reportIdentifier = readJSONNode(json, "report");
		tableIdentifier = readJSONNode(json, "table");
		rowIdentifier = readJSONNode(json, "row");
		columnIdentifier = readJSONNode(json, "column");
	}

	private NameIndexIdentifier readJSONNode(GenericJSON json, String key) {
		Object identifier = json.get(key);
		if (identifier == null)
			return null;
		
		// Check if index is specified
		if (identifier instanceof Integer)
			return new NameIndexIdentifier(null, Utils.downCast(Integer.class, identifier));

		// check if name is specified
		if (identifier instanceof String)
			return new NameIndexIdentifier(Utils.downCast(String.class, identifier), null);
		
		Utils.handleException("Invalid Identifier for " + key + ": " + identifier.toString());

		// can't reach here
		return null;
	}
}
