package com.cariq.toolkit.coreiq.loader;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.atom.www.loader.AtomDemoLoader;
import com.atom.www.loader.AtomNotificationLoader;
import com.atom.www.loader.AtomStagingAndVerificationDetailsUpdater;
import com.atom.www.loader.BillingGenerator;
import com.atom.www.loader.BillingInvoiceGenerator;
import com.atom.www.loader.ConditionBasedOwnerAssignmentLoader;
import com.atom.www.loader.DummyExporterChainLoader;
import com.atom.www.loader.MonthlyMISGenerator;
import com.atom.www.loader.MonthlyTATAndMISGenerator;
import com.atom.www.loader.OwnerAssignmentLoader;
import com.atom.www.loader.PerformanceReportGenerator;
import com.atom.www.loader.ProxyUpdater;
import com.atom.www.loader.StageBasedOwnerAssignmentFilterableLoader;
import com.atom.www.loader.StageBasedOwnerAssignmentLoader;
import com.atom.www.loader.TimelineLoader;
import com.atom.www.loader.WeeklyAndMonthlyMISGenerator;
import com.atom.www.loader.WeeklyAndMonthlyTATAndMISGenerator;
import com.atom.www.loader.WeeklyMISGenerator;
import com.atom.www.loader.WeeklyTATAndMISGenerator;
import com.cariq.toolkit.model.billing.ICICIInvoiceBillableLoader;

public class CarIQLoaderRegistry {
	static Map<String, CarIQLoader> map = new HashMap<String, CarIQLoader>();

	static void add(CarIQLoader loader) {
		map.put(loader.getName().toUpperCase(), loader);
	}

	static {
		// Enlist all the loaders to be made available to Admin through REST call

				add(new AtomDemoLoader());
				add(new AtomNotificationLoader());
				add(new AtomStagingAndVerificationDetailsUpdater());
				add(new TimelineLoader());
				add(new ProxyUpdater());
				add(new OwnerAssignmentLoader());
				add(new StageBasedOwnerAssignmentLoader());
				add(new ConditionBasedOwnerAssignmentLoader());
				add(new CarAggregateStatisticsLoader());
				add(new BillingGenerator());
				add(new StageBasedOwnerAssignmentFilterableLoader());
				
				// MIS Loaders
				add(new DummyExporterChainLoader());
				add(new WeeklyMISGenerator());
				add(new MonthlyMISGenerator());
				add(new WeeklyAndMonthlyMISGenerator());
				add(new BillingInvoiceGenerator());
//				add(new SuccessTeamMISGenerator());
				add(new PerformanceReportGenerator());
				
				// Billing Invoice Loader
				add(new ICICIInvoiceBillableLoader());
				
				// new tat mis report
				add(new WeeklyTATAndMISGenerator());
				add(new MonthlyTATAndMISGenerator());
				add(new WeeklyAndMonthlyTATAndMISGenerator());
	}

	public static CarIQLoader getLoader(String loaderName) {
		return map.get(loaderName.toUpperCase());
	}

	static Collection<CarIQLoader> getAllLoaders() {
		return map.values();
	}
}
