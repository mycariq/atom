package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.List;

import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.ProxyObject;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

/**
 * Spotlight displays all policies which are due to renewal.
 * 
 * @author sagar
 *
 */
public class RenewalSpotlight extends ShipmentBasedSpotlightTemplate {
	
	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("RenewalSpotlight");
	
	public static final String TheSpotlight = "RenewalSpotlight";
	public static final String TheSpotlightDescription = "Spotlight displays all policies which are due to renewal.";
	
	public static final String KEY_EXPIRY_DATE = "Staging_policyExpiryDate";

	/**
	 * Default constructor
	 */
	public RenewalSpotlight() {
		super(TheSpotlight, TheSpotlightDescription);
	}

	@Override
	protected List<String> getTagsToExclude() {
		List<String> tags = new ArrayList<>();
		tags.add(StagingAndPolicyHelper.TAG_READY_TO_RENEW);
		
		return tags;
	}
	
	/**
	 * Get tagged proxies.
	 * 
	 * @param user
	 * @param domain
	 * @return
	 */
	@Override
	protected List<ProxyObject> getTaggedProxies(User user, Domain domain) {
		String commSepTags = Utils.getCommaSeperatedValuesForQuery(Utils.getCommaSeperatedString(getTagsToExclude()));
		
		// Get NI Proxy Objects
		List<ProxyObject> taggedProxies = ProxyObject.getByLatestTagsAndDomainAndOwner(commSepTags,
				domain, user, "renewalDisposition", 1, Integer.MAX_VALUE);
		
		return taggedProxies;
	}

	@Override
	protected List<GenericJSON> enhanceByEffectiveDates(List<GenericJSON> input) {
		logger.debug("Enhancing response with effective dates...");
		List<GenericJSON> retValues = new ArrayList<>();
		for(GenericJSON item : input) {
			String effectiveDate = Utils.getValue(item, KEY_EXPIRY_DATE);
			item.add(Effective_Date, effectiveDate);
			retValues.add(item);
		}
		return retValues;
	}

	@Override
	protected String getQueryType() {
		return StagingAndPolicyHelper.QueryType_PoliciesToRenew;
	}
}
