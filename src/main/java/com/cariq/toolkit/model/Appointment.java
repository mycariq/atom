package com.cariq.toolkit.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.atom.www.helper.AppointmentStatus;
import com.atom.www.helper.TimeSlot;
import com.cariq.toolkit.coreiq.CarIQRawQuery;
import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_In;
import com.cariq.toolkit.model.Timeline.Timeline_In;
import com.cariq.toolkit.model.User.User_In;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.model.Appointment.Appointment_In;
import com.cariq.toolkit.model.Appointment.Appointment_Out;

@Entity
@Configurable
public class Appointment extends PersistentObject<Appointment> implements JSONable<Appointment_In, Appointment_Out, IdJSON> {
	
	// SECTION 1 -Code for Persistence
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@Version
	Integer version;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new Timeline().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null)
			this.entityManager = entityManager();
		return entityManager;
	}

	/**
	 * @param id
	 * @return
	 */
	public static Appointment find(Long id) {
		return entityManager().find(Appointment.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public Appointment findById(Long id) {
		return find(id);
	}
	
	
	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/**
	 * Model properties
	 */
	
	/** Appointment of account manager taken by this proxy **/
	@NotNull
	@ManyToOne
	ProxyObject proxy;
	
	/**
	 * User who created this appointment.
	 */
	@ManyToOne
	User createdBy;
	
	/** Stage while creating appointment **/
	String creationStage;
	
	/** Appointment date **/
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	@NotNull
	Date appointmentOn;
	
	/** Status of appointment **/
	String status;
	
	/** Created On date **/
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	/** Modified On date **/
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;
	
	
	/**
	 * Default constructor
	 */
	public Appointment() {
		super();
	}
	
	/**
	 * Parameterized constructor
	 * 
	 * @param proxyObject
	 * @param appointmentOn
	 */
	public Appointment(ProxyObject proxyObject, Date appointmentOn) {
		super();
		this.proxy = proxyObject;
		this.appointmentOn = appointmentOn;
	}
	
	public ProxyObject getProxy() {
		return proxy;
	}

	public void setProxy(ProxyObject proxy) {
		this.proxy = proxy;
	}

	public Date getAppointmentOn() {
		return appointmentOn;
	}

	public void setAppointmentOn(Date appointmentOn) {
		this.appointmentOn = appointmentOn;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreationStage() {
		return creationStage;
	}

	public void setCreationStage(String creationStage) {
		this.creationStage = creationStage;
	}
	
	/**
	 * Check whether appointment is outdated or not.
	 * 
	 * @param currentStageOfPolicy (delieverd, onboarded etc).
	 * @return
	 */
	public boolean isOutdated(String currentStageOfPolicy) {
		//1. If status of this appointment is alredy 'OUTDATED' then return true
		String appointmentStatus = getStatus();	
		
		if(AppointmentStatus.OUTDATED.name().equalsIgnoreCase(appointmentStatus))
			return true;
		
		//2. If difference between now and appointment date is > x then TRUE
		Date today = new Date();
		if(appointmentOn.before(today)) {
			int days = Utils.getDiffInDays(appointmentOn, today);
			if(days > Utils.THRESHOLD_TO_OUTDATE_APPOINTMENT_IN_DAYS)
				return true;
		}

		//3. Compare stage of appointment with stage of case -> If don't match then OUTDATED, return TRUE;
		String appointmentStage = getCreationStage();
		
		if(currentStageOfPolicy != null && appointmentStage != null) {
			if(!currentStageOfPolicy.equalsIgnoreCase(appointmentStage))
				return true;
		}
		
		//4. If either of stage is null then don't mark OUTDATED, return FALSE;
		
		return false;
	}
	
	/**
	 * Get reason, why policy has been marked outdated. 
	 * 
	 * @param currentStageOfPolicy
	 * @return
	 */
	public String getOudatedReason(String currentStageOfPolicy) {
		//1. If status of this appointment is alredy 'OUTDATED' then return true
		String appointmentStatus = getStatus();	
		
		if(AppointmentStatus.OUTDATED.name().equalsIgnoreCase(appointmentStatus))
			return "Policy was already marked OUTDATED.";
		
		//2. If difference between now and appointment date is > x then TRUE
		Date today = new Date();
		if(appointmentOn.before(today)) {
			int days = Utils.getDiffInDays(appointmentOn, today);
			if(days > Utils.THRESHOLD_TO_OUTDATE_APPOINTMENT_IN_DAYS)
				return "Current appointment is older than " +  Utils.THRESHOLD_TO_OUTDATE_APPOINTMENT_IN_DAYS + " days.";
		}

		//3. Compare stage of appointment with stage of case -> If don't match then OUTDATED, return TRUE;
		String appointmentStage = getCreationStage();
		
		if(currentStageOfPolicy != null && appointmentStage != null) {
			if(!currentStageOfPolicy.equalsIgnoreCase(appointmentStage))
				return "Current stage of policy is different than stage of appointment.";
		}
		
		//4. If either of stage is null then don't mark OUTDATED, return FALSE;
		
		return "Unknown reason.";
	}

	@Override
	public Appointment_Out toJSON() {
		String dateStr = Utils.convertToyyyymmdd_hhmmssFormat(appointmentOn);
		return new Appointment_Out(id, proxy.toJSON(), dateStr, status, createdBy != null ? createdBy.getUsername() : null, creationStage);
	}

	@Override
	public void fromJSON(Appointment_In json) {
		Domain domain = Domain.getByName(json.getProxyObjectIn().domainName);
		this.proxy = ProxyObject.createOrGet(domain, 
				json.getProxyObjectIn().objectId, json.getProxyObjectIn().objectType, 
				json.getProxyObjectIn().signature, User.getByUsername(json.getProxyObjectIn().getUsername()), null);
		this.appointmentOn = Utils.convertToDate(json.getAppointmentOn(), Utils.YYYY_mm_dd_hh_mm_ss);
		this.status = json.getStatus();
		this.createdOn = new Date();
		this.modifiedOn = new Date();
		this.createdBy = User.getByUsername(json.getCreatedBy());
		this.creationStage = json.getCreationStage();
	}
	
	
	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------

	// Implementation of toString and hashCode and equals are advisable for all
	// Model classes
	@Override
	public String toString() {
		return "Appointment [id=" + id + ", version=" + version + ", proxy =" + proxy.getId() + 
				", appointmentOn =" + appointmentOn + ", createdBy =" + createdBy.getId() + ", creationStage =" + creationStage +
				", createdOn=" + createdOn + ", modifiedOn=" + modifiedOn + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((proxy == null) ? 0 : proxy.hashCode());
		result = prime * result + ((appointmentOn == null) ? 0 : appointmentOn.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
		result = prime * result + ((creationStage == null) ? 0 : creationStage.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Appointment other = (Appointment) obj;
		if (proxy == null) {
			if (other.proxy != null)
				return false;
		} else if (!appointmentOn.equals(other.appointmentOn))
			return false;
		if (createdBy == null) {
			if (other.createdBy != null)
				return false;
		} else if (!createdBy.equals(other.createdBy))
			return false;
		if (creationStage == null) {
			if (other.creationStage != null)
				return false;
		} else if (!creationStage.equals(other.creationStage))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		return true;
	}
	
	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API
	// interaction
	// In Class (for construction)
	public static class Appointment_In {
		ProxyObject_In proxyObjectIn;
		String appointmentOn;
		String status;
		String createdBy;
		String creationStage;
		
		public Appointment_In() {
			super();
		}
		
		public Appointment_In(ProxyObject_In proxyObjectIn, String appointmentOn,
				String status, String createdByIn, String creationStageIn) {
			this.proxyObjectIn = proxyObjectIn;
			this.appointmentOn = appointmentOn;
			this.status = status;
			this.createdBy = createdByIn;
			this.creationStage = creationStageIn;
		}

		public ProxyObject_In getProxyObjectIn() {
			return proxyObjectIn;
		}

		public void setProxyObjectIn(ProxyObject_In proxyObjectIn) {
			this.proxyObjectIn = proxyObjectIn;
		}

		public String getAppointmentOn() {
			return appointmentOn;
		}

		public void setAppointmentOn(String appointmentOn) {
			this.appointmentOn = appointmentOn;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getCreatedBy() {
			return createdBy;
		}

		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}

		public String getCreationStage() {
			return creationStage;
		}

		public void setCreationStage(String creationStage) {
			this.creationStage = creationStage;
		}
	}
	
	/**
	 * Appointment Out jsonable model
	 */
	public static class Appointment_Out extends Appointment_In {
		Long id;
		public Appointment_Out() {
			super();
		}
		public Appointment_Out(Long id, ProxyObject_In proxyObjectIn, String appointmentOn, String status,
				String createdByIn, String creationStageIn) {
			super(proxyObjectIn, appointmentOn, status, createdByIn, creationStageIn);
			this.id = id;
		}
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		@Override
		public String toString() {
			return "Appointment_Out [id=" + id + ", proxyObjectIn=" + proxyObjectIn + ", appointmentOn=" + appointmentOn
					+ "]";
		}
	}

	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}

	
	/**
	 * Identify timeslot for this appointment
	 * 
	 * @return
	 */
	public TimeSlot identifyTimeSlot() {
		return TimeSlot.getByDate(appointmentOn);
	}
	
	/**
	 * Create or Get appointment
	 * @param timeSlotIn
	 * @return
	 */
	public static Appointment createOrGet(Appointment_In appointment_In) {
		
		//Get proxyObject
		ProxyObject_In proxyObjectIn = appointment_In.getProxyObjectIn();
		Domain domain = Domain.getByName(proxyObjectIn.getDomainName());
		ProxyObject proxyObject = ProxyObject.getByDomainSignatureObjectType(domain,
				proxyObjectIn.getSignature(), proxyObjectIn.getObjectType());
		
		//find appointment on proxy and appointmentOn
		Date date = Utils.convertToDate(appointment_In.getAppointmentOn(), Utils.YYYY_mm_dd_hh_mm_ss);
		Appointment appointment = Appointment
				.getActiveAppointment(proxyObject);
		if(appointment != null)
			return appointment;
		
		//if appointment is null then create a new one
		Appointment newAppointment = new Appointment();
		newAppointment.fromJSON(appointment_In);
		newAppointment.persist();
		return newAppointment;
	}
	
	public static Appointment getActiveAppointment(ProxyObject proxy) {
		//get appointment where proxy and appointmentOn match
		CarIQSimpleQuery<Appointment> getActiveAppointment = 
				new CarIQSimpleQuery<Appointment>("getActiveAppointment", Appointment.entityManager(), Appointment.class);
				
		getActiveAppointment.addCondition("proxy", "=", proxy);
		getActiveAppointment.addRawCondition("status in ('CREATED', 'RESCHEDULED')");
				
		return getActiveAppointment.getSingleResult();
	}
	
	/**
	 * Get appointment by proxy and date
	 * 
	 * @param proxy
	 * @param appointmentOn
	 * @return
	 */
	public static Appointment getByProxyAndAppointmentOn(ProxyObject proxy, Date appointmentOn) {
		//get appointment where proxy and appointmentOn match
		CarIQSimpleQuery<Appointment> getByProxyAndAppointmentOn = 
				new CarIQSimpleQuery<Appointment>("getByProxyAndAppointmentOn", Appointment.entityManager(), Appointment.class);
		
		getByProxyAndAppointmentOn.addCondition("proxy", "=", proxy);
		getByProxyAndAppointmentOn.addCondition("appointmentOn", "=", appointmentOn);
		
		return getByProxyAndAppointmentOn.getSingleResult();
	}
	
	/**
	 * Get appointment by proxy, date and status
	 * 
	 * @param proxy
	 * @param appointmentOn
	 * @return
	 */
	public static Appointment getByProxyAndAppointmentOnAndStatus(ProxyObject proxy, Date appointmentOn, String status) {
		//get appointment where proxy and appointmentOn match
		CarIQSimpleQuery<Appointment> getByProxyAndAppointmentOn = 
				new CarIQSimpleQuery<Appointment>("getByProxyAndAppointmentOn", Appointment.entityManager(), Appointment.class);
		
		getByProxyAndAppointmentOn.addCondition("proxy", "=", proxy);
		getByProxyAndAppointmentOn.addCondition("appointmentOn", "=", appointmentOn);
		getByProxyAndAppointmentOn.addCondition("status", "=", status);
		
		return getByProxyAndAppointmentOn.getSingleResult();
	}
	
	
	/**
	 * Get all appointments for given proxy, but not completed. 
	 * 
	 * @param proxy
	 * @return
	 */
	public static List<Appointment> getByProxyAndNotCompleted(ProxyObject proxy) {
		CarIQSimpleQuery<Appointment> getByProxyAndNotCompleted = 
				new CarIQSimpleQuery<Appointment>("getByProxyAndNotCompleted", Appointment.entityManager(), Appointment.class);
				
		getByProxyAndNotCompleted.addCondition("proxy", "=", proxy);
//		getByProxyAndNotCompleted.addCondition("status", "<>", AppointmentStatus.COMPLETE.name());
		getByProxyAndNotCompleted.addRawCondition("status not in ('COMPLETE', 'OUTDATED')");
		
		return getByProxyAndNotCompleted.getResultList();
	}
	
	/**
	 * Get all appointments for given timeslot
	 * 
	 * @param timeSlot
	 * @return
	 */
	public static List<Appointment> getForSlot(TimeSlot timeSlot, User user, Domain domain, int pageNo, int pageSize) {
	
		try (ProfilePoint _getForSlot = ProfilePoint
				.profileAction("ProfAction_Appointment_getForSlot")) {
			
			TypedQuery<Appointment> query = Appointment.entityManager().createQuery(
					"SELECT app FROM Appointment AS app, ProxyObject AS p WHERE app.proxy=p.id AND p.owner=:user AND p.domain=:domain AND app.appointmentOn > :startDate AND app.appointmentOn < :endDate AND app.status NOT IN (:status) ORDER BY appointment_on",
					Appointment.class);
			query.setParameter("user", user);
			query.setParameter("domain", domain);
			query.setParameter("startDate", timeSlot.getStartTime());
			query.setParameter("endDate", timeSlot.getEndTime());
			query.setParameter("status", Arrays.asList(AppointmentStatus.COMPLETE.name(), AppointmentStatus.OUTDATED.name()));
			query.setFirstResult(Utils.validatePagination(pageSize, pageNo));
			query.setMaxResults(pageSize);

			return query.getResultList();
		}
	}
	
	/**
	 * Get all for given slot and and all delayed
	 * 
	 * @param timeSlot
	 * @param user
	 * @param domain
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public static List<Appointment> getForSlotAndAllDelayed(TimeSlot timeSlot, User user, Domain domain,
			int pageNo, int pageSize) {
		
		try (ProfilePoint _getForSlotAndAllDelayed = ProfilePoint
				.profileAction("ProfAction_Appointment_getForSlotAndAllDelayed")) {

			TypedQuery<Appointment> query = Appointment.entityManager().createQuery(
					"SELECT app FROM Appointment AS app, ProxyObject AS p WHERE app.proxy=p.id AND p.owner=:user AND p.domain=:domain AND app.appointmentOn < :endDate AND app.status NOT IN (:status) group by app.proxy ORDER BY appointment_on",
					Appointment.class);
			query.setParameter("user", user);
			query.setParameter("domain", domain);
			query.setParameter("endDate", timeSlot.getEndTime());
			query.setParameter("status", Arrays.asList(AppointmentStatus.COMPLETE.name(), AppointmentStatus.OUTDATED.name()));
			query.setFirstResult(Utils.validatePagination(pageSize, pageNo));
			query.setMaxResults(pageSize);

			return query.getResultList();
		}
	}
	
	/**
	 * Get all for given slot and and all delayed
	 * 
	 * @param timeSlot
	 * @param user
	 * @param domain
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public static List<Appointment> getForSlotAndAllDelayed(TimeSlot timeSlot, Domain domain,
			int pageNo, int pageSize) {
		
		try (ProfilePoint _getForSlotAndAllDelayed = ProfilePoint
				.profileAction("ProfAction_Appointment_getForSlotAndAllDelayed")) {

			TypedQuery<Appointment> query = Appointment.entityManager().createQuery(
					"SELECT app FROM Appointment AS app, ProxyObject AS p WHERE app.proxy=p.id AND p.domain=:domain AND app.appointmentOn < :endDate AND app.status NOT IN (:status) group by app.proxy ORDER BY appointment_on",
					Appointment.class);
			query.setParameter("domain", domain);
			query.setParameter("endDate", timeSlot.getEndTime());
			query.setParameter("status", Arrays.asList(AppointmentStatus.COMPLETE.name(), AppointmentStatus.OUTDATED.name()));
			query.setFirstResult(Utils.validatePagination(pageSize, pageNo));
			query.setMaxResults(pageSize);

			return query.getResultList();
		}
	}
	
	
	/**
	 * Get all appointments for given user
	 * 
	 * @param user
	 * @return
	 */
	public static List<Appointment> getForUser(User user, Domain domain, int pageNo, int pageSize) {
		try (ProfilePoint _getForUser = ProfilePoint.profileAction("ProfAction_Appointment_getForUser")) {

			TypedQuery<Appointment> query = Appointment.entityManager().createQuery(
					"SELECT app FROM Appointment AS app, ProxyObject AS p WHERE app.proxy=p.id AND p.owner=:user AND p.domain=:domain AND app.status NOT IN (:status) ORDER BY appointment_on",
					Appointment.class);
			query.setParameter("user", user);
			query.setParameter("domain", domain);
			query.setParameter("status", Arrays.asList(AppointmentStatus.COMPLETE.name(), AppointmentStatus.OUTDATED.name()));
			query.setFirstResult(Utils.validatePagination(pageSize, pageNo));
			query.setMaxResults(pageSize);

			return query.getResultList();
		}
	}
	
	/**
	 * Get Proxies from Appointments
	 * 
	 * @param appointments
	 * @return
	 */
	public static List<ProxyObject> getProxies(List<Appointment> appointments) {
		List<ProxyObject> proxies = new ArrayList<>();
		
		if(Utils.isNullOrEmpty(appointments))
			return proxies; 
		
		for(Appointment appointment : appointments) {
			proxies.add(appointment.getProxy());
		}
		return proxies;
	}
	
	/**
	 * Get signatures from appointments
	 * 
	 * @param appointments
	 * @return
	 */
	public static List<String> getSignatures(List<Appointment> appointments) {
		List<String> signatures = new ArrayList<>();
		for(Appointment appointment : appointments) {
			ProxyObject proxyObject = appointment.getProxy();
			signatures.add(proxyObject.getSignature());
		}
		return signatures;
	}
}
