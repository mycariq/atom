package com.atom.www.loader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderContext;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;


/**
 * This loader assigns owner to proxies on stage basis. It takes current owners, stage
 * and assigns those proxies to new owners.
 * @author sagar
 */
public class StageBasedOwnerAssignmentLoader extends OwnerTransferLoaderTemplate {
	
	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("StageBasedOwnerAssignmentLoader");
	
	public static final String TheLoader = "StageBasedOwnerAssignmentLoader";
	public static final String TheDescription = "StageBasedOwnerAssignmentLoader Loader assigns "
			+ "proxies of current owners to new owners in given stage.";

	
	/** Stage of proxy **/
	public static final String STAGE = "stage";

	public  static final String STATE = "State";
	
	/**
	 * Default constructor.
	 */
	public StageBasedOwnerAssignmentLoader() {
		this(TheLoader, TheDescription);
	}
	
	public StageBasedOwnerAssignmentLoader(String name, String description) {
		super(name, description);
		
		addAttribute(STAGE, "Comma seperated names of stages.", false);
		addAttribute("CreatedAfter", "Date of proxies created after in YYYY-MM-DD HH:MM format.", false);
	}
	
	
	@Override
	protected void doLoad(GenericJSON json, CarIQLoaderContext context) {
		
		String stages = Utils.getValue(json, STAGE);
		
		//2.1 Validate for null values.
		if(Utils.isValueIn(null, stages))
			Utils.handleException("Please enter valid input details");
		
		//3. Validate comma separated stages
		if(!StagingAndPolicyHelper.stageExists(StagingAndPolicyHelper.stages, stages))
			Utils.handleException("Invalid input stages!");
		
		super.doLoad(json, context);
	}
	

	@Override
	List<GenericJSON> filterWithCondition(List<GenericJSON> policies, GenericJSON inputJSON) {
		
		String stages = Utils.getValue(inputJSON, STAGE);
		
		List<GenericJSON> retValues = new ArrayList<GenericJSON>();
		
		//convert case of all stages for comparison.
		stages = stages.toLowerCase();
		
		//split input comma separated list of stages
		List<String> stageList = Arrays.asList(stages.split(Utils.COMMA));
		
		for(GenericJSON json : policies) {
			String stageOfObject = Utils.getValue(json, STATE);
			
			//convert case to lowercase.
			stageOfObject = stageOfObject.toLowerCase();
			if(StagingAndPolicyHelper.stageExists(stageList, stageOfObject)) {
				logger.debug("Found matching object with stage: " + stageOfObject);
				retValues.add(json);
			}
		}
		return retValues;
	}


	@Override
	String getQueryType() {
		return StagingAndPolicyHelper.QueryType_NotConnected;
	}

}
