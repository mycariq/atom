package com.atom.www.loader;

import com.cariq.toolkit.coreiq.loader.CarIQLoaderContext;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderTemplate;
import com.cariq.toolkit.utils.GenericJSON;

/**
 * NotificationLoader in CarIQ
 * @author sagar
 *
 * username, notificationType (Email, SMS, Push), String subject, String shortMessage (for SMS), String longMessage; 
 */

public class AtomDemoLoader extends CarIQLoaderTemplate {

	
	
	public AtomDemoLoader() {
		super("AtomDemoLoader", "This is demo loader");

		addAttribute("studentName", "This is student name description", false);
		addAttribute("studentAddress", "This is student address description", false);
	}

	@Override
	protected void doLoad(GenericJSON json, CarIQLoaderContext context) {
		
		for(String str : json.keySet()) {
			System.out.println(json.get(str));
		}		
	}	
}
