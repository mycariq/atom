package com.cariq.toolkit.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cariq.toolkit.utils.analytics.core.CarIQFile;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.utils.service.FileHandlerService;
import com.cariq.toolkit.serviceimpl.async.WIHelper;

/**
 * The Class CarIQFileServer.
 */
@Component
public class CarIQFileServer {

	/** The file handler service. */
	@Autowired
	private FileHandlerService fileHandlerService; // = new FileHandlerServiceImpl();

	/** The logger. */
	private CarIQLogger logger = WIHelper.getLogger(CarIQFileServer.class);

	/**
	 * Upload.
	 *
	 * @param namedFile
	 *            the File with name
	 * @param container
	 *            the container
	 */
	public void upload(CarIQFile namedFile, String container) {
		logger.debug("[CALLED],[upload],\"upload(report=" + namedFile + ",container=" + container + ")\"");

		try (ProfilePoint _CarIQFileServer_uploadFile = ProfilePoint
				.profileAction("ProfAction_CarIQFileServer_uploadFile")) {
			try {

				fileHandlerService.uploadLocalFile(namedFile.getPath(),
						namedFile.getName(), container);
				logger.info("[INFO],[upload],\"Uploaded " + namedFile.getPath()
						+ " file on " + container + " container\"");

			} catch (Exception e) {

				logger.info("[INFO],[upload],\"Failed to upload "
						+ namedFile.getPath() + " file on " + container
						+ " container\"");
				Utils.logException(logger, e, "Uploading report to container");
				throw new RuntimeException(e);
			}
		}
	}

	/**
	 * Download.
	 *
	 * @param serverPath
	 *            the server path
	 * @param localPath
	 *            the local path
	 */
	public void download(String serverPath, String localPath) {
		throw new RuntimeException("Yet to implement");
	}

	/**
	 * Exists.
	 *
	 * @param path
	 *            the path
	 * @return true, if successful
	 */
	public boolean exists(String path) {
		throw new RuntimeException("Yet to implement");
	}

	/**
	 * Exists.
	 *
	 * @param report
	 *            the report
	 * @return true, if successful
	 */
	public boolean exists(CarIQFile report) {
		throw new RuntimeException("Yet to implement");
	}

	/**
	 * Ping.
	 *
	 * @return the long
	 */
	public long ping() {
		throw new RuntimeException("Yet to implement");
	}
}
