package com.cariq.toolkit.utils;

import java.util.Date;

import org.junit.Test;

import com.cariq.toolkit.coreiq.formula.impl.Formula;
import com.cariq.toolkit.coreiq.formula.impl.Variable;
import com.cariq.toolkit.coreiq.formula.interfaces.VariableResolverIfc;

public class FormulaTest {
	@Test
	public void greenTest() {
		System.out.println("Success to start with...!");
	}
	
	@Test
	public void basicFormulaEvaluation() throws Exception {
		Formula f = new Formula("(TWENTY_THREE % 2)  PERCENTAGE (2 ^ 2 * TIME_YEAR)");
		f.add(new Variable("TWENTY_THREE", GenericJSON.build("value", 23), new VariableResolverIfc() {
			
			@Override
			public Object resolve(Object context, GenericJSON valueDefinition) {
				// TODO Auto-generated method stub
				return valueDefinition.get("value");
			}
		}));

		f.add(new Variable("TIME_YEAR", GenericJSON.build("parameter", "TIME_YEAR"), new VariableResolverIfc() {
			
			@Override
			public Object resolve(Object context, GenericJSON valueDefinition) {
				// TODO Auto-generated method stub
				if ("TIME_YEAR".equalsIgnoreCase(valueDefinition.get("parameter").toString())) {
					Date now = new Date();
					return now.getTime()/(Utils.yearSeconds * 1000);
				}
				
				// something default 
				return 2;
			}
		}));
		
		System.out.println("Value of: " + f.getExpression() + " = " + f.evaluate(null).toString());

	}
}
