package com.cariq.toolkit.model.async.lrt;
//package com.cariq.www.coreiq.lrt;
//
//import com.cariq.www.utils.GenericJSON;
//import com.cariq.www.workitem.model.WorkItem;
//import com.cariq.www.workitem.model.WorkItemUpdater;
//import com.cariq.www.workitem.worker.WorkItemDelegateTemplate;
//
///**
// * Abstract base of Running Long Running Task as a workitem
// * @author Hrishi
// *
// */
//public abstract class LRTRunnable extends WorkItemDelegateTemplate {
//
//	@Override
//	protected void doExecute(WorkItem workItem, WorkItemUpdater updater) throws Exception {
//		updater.setOutputJson(executeLRT(workItem.fetchInputJson()).toString());		
//	}
//
//	abstract protected GenericJSON executeLRT(GenericJSON fetchInputJson);
//
//}
