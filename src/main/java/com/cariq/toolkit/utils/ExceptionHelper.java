package com.cariq.toolkit.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

public class ExceptionHelper {

	public static List<String> exceptionHandler(ConstraintViolationException e) {
		List<String> exceptionArray = new ArrayList<String>();
		for (Iterator<ConstraintViolation<?>> iter = e
				.getConstraintViolations().iterator(); iter.hasNext();) {
			final ConstraintViolation<?> cv = iter.next();
			exceptionArray.add(cv.getPropertyPath()+ " " + cv.getMessage());
		}
		return exceptionArray;
	}
	
	public static List<String> convertStringToerror(String error)
	{
		List<String> errorArray = new ArrayList<String>();
		errorArray.add(error);
		return errorArray;
	}
}