package com.cariq.toolkit.coreiq.tableUtil.impl;

import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.coreiq.tableUtil.interfaces.CellAddressIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.CellIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.ColumnIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.NameIndexIdentifiable;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.RowIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.TableIfc;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.ObjectBuilder;
import com.cariq.toolkit.utils.Utils;

/**
 * Basic Table interface. It can be built incrementally by adding rows and columns
 * Cells are automatically created based on Rows and Columns
 * @author hrishi
 *
 */
public class Table implements TableIfc, ObjectBuilder<Object> {
	public static final String ROW = "ROW";
	public static final String COLUMN = "COLUMN";
	public static final String HEADING = "HEADING";
	
	protected String heading;
	List<RowIfc> rows = new ArrayList<RowIfc>();
	List<ColumnIfc> columns = new ArrayList<ColumnIfc>();

	public Table(String heading) {
		this.heading = heading;
	}

	public Table() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getHeading() {
		return heading;
	}

	@Override
	public List<RowIfc> getRows() {
		return rows;
	}

	@Override
	public List<ColumnIfc> getColumns() {
		return columns;
	}

	public TableIfc addRow(RowIfc row) {
		Utils.checkNotNull(row, "Row passed to Table.addRow is null!");
		rows.add(row);
		row.setIndex(rows.size());

		return this;
	}

	public TableIfc addColumn(ColumnIfc col) {
		Utils.checkNotNull(col, "Column passed to Table.addRow is null!");

		columns.add(col);
		col.setIndex(columns.size());

		return this;
	}

	@Override
	public Table configure(String setting, Object value) {
		Utils.notImplementedException("Table.configure");
		return null;
	}

	@Override
	public Table add(Object value) {
		Utils.notImplementedException("Table.add");
		return null;
	}

	@Override
	public Table add(String parameter, Object value) {
		if (ROW.equalsIgnoreCase(parameter)) {
			addRow(Utils.downCast(RowIfc.class, value));
		} else if (COLUMN.equalsIgnoreCase(parameter)) {
			addColumn(Utils.downCast(ColumnIfc.class, value));
		} else if (HEADING.equalsIgnoreCase(parameter)) {
			this.heading = Utils.downCast(String.class, value);
		} else {
			Utils.handleException("Invalid parameter type for Table Builder: " + parameter);
		}
		
		// TODO Auto-generated method stub
		return this;
	}

	@Override
	public Table build() {
		// Construct the cells 
		for (RowIfc row : rows) {
			for (ColumnIfc column : columns) {
				Cell cell = new Cell(row, column);
				row.add(cell);
				column.add(cell);
			}
		}
		return this;
	}


	@Override
	public GenericJSON toGenericJSON() {
		List<GenericJSON> rowJSONs = new ArrayList<GenericJSON>();
		for (RowIfc row : rows) {
			rowJSONs.add(row.toGenericJSON());
		}
		// Go through the table and spit out
		return GenericJSON.build("heading", heading, "rows", rowJSONs);
	}

	@Override
	public void fromGenricJSON(GenericJSON json) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public CellIfc locate(ProcessingContext processingContext, CellAddressIfc cellAddress) {
		// Check if Tabel address is given - else locate in current table
		if (cellAddress.getRowIdentifier() == null)
			return processingContext.getCurrentRow().locate(processingContext, cellAddress);
		
		// Check if Index given
		NameIndexIdentifiable rowId = cellAddress.getRowIdentifier();
		if (rowId.getIndex() != null) {
			// locate based on index - index is 1 based
			RowIfc rowIfc = rows.get(rowId.getIndex() - 1);
			return rowIfc.locate(processingContext, cellAddress);
		}

		// else to through each table and check - feels a little unnatural
		for (RowIfc rowIfc : rows) {
			if (!rowIfc.isMatching(processingContext, cellAddress))
				continue;
			
			CellIfc cell = rowIfc.locate(processingContext, cellAddress);
			
			// Try to locate till we find the right table
			if (cell != null)
				return cell;
		}

		// Otherwise return null
		return null;
	}

	@Override
	public boolean isMatching(ProcessingContext processingContext, CellAddressIfc cellAddress) {
		// Table does not have index, it has name - so check name only
		// If address doesn't have this report, return null
		String addressTableName = cellAddress.getTableIdentifier().getName();
		if ( addressTableName == null || addressTableName.equalsIgnoreCase(heading))
			return true;
		
		return false;
	}
}
