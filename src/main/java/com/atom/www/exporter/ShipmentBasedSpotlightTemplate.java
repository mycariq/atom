package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.atom.www.helper.DomainHelper;
import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.model.Appointment;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.ProxyObject;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.model.Domain.Domain_Out;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * Template for Shipment Based spotlight exporters
 * 
 * @author sagar
 *
 */
public abstract class ShipmentBasedSpotlightTemplate extends SpotlightTemplate {

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("ShipmentBasedSpotlightTemplate");

	public static final String TheExporter = "PreShipmentExporter";

	public static final String TheDescription = "Exports all order of all stages before shipment.";

	public static final String QueryType_default = "Default";
	public static final String defaultQueryTypeDescription = "Get appointments by owner and domain.";

	public static final String DOMAIN = "domain";
	protected static final String KEY_OWNER = "owner";
	
	protected static final String STATE = "State";
	
	protected static final String Effective_Date = "Effective_Date";

	private DomainHelper domainHelper;
	
	static {
		
	}
	
	/**
	 * Default constructor
	 * 
	 * @param name
	 * @param description
	 */
	public ShipmentBasedSpotlightTemplate(String name, String description) {
		super(name, description);
		
		domainHelper = new DomainHelper();
		
		addQueryType(QueryType_default, defaultQueryTypeDescription,
				Arrays.asList(new QueryDefinitionAttribute(KEY_OWNER, "Username of owner", String.class),
						new QueryDefinitionAttribute(DOMAIN, "Name of domain", String.class)));
	}
	
	/**
	 * Get tags to exclude from spotlight.
	 * 
	 * @return
	 */
	protected List<String> getTagsToExclude() {
		
		List<String> tags = new ArrayList<>();
		
		//Add all tags in list. These tags will be used to exclude policies.
		tags.add(StagingAndPolicyHelper.TAG_NOT_CONTACTABLE);
		tags.add(StagingAndPolicyHelper.TAG_NOT_INTERESTED);
		tags.add(StagingAndPolicyHelper.TAG_CANCELLED);
		tags.add(StagingAndPolicyHelper.TAG_DUPLICATE);
		tags.add(StagingAndPolicyHelper.TAG_INVALID_ORDER);
		tags.add(StagingAndPolicyHelper.TAG_TEST);
		
		return tags;
	}
	
	public String getSignatures(String queryType, GenericJSON json, int pageNo, int pageSize) {
		try (ProfilePoint _getSignatures_ = ProfilePoint
				.profileAction("ProfAction_getSignatures_" + this.getClass().getCanonicalName())) {

			String owner = Utils.getValue(json, KEY_OWNER);
			User user = owner == null ? User.getLoggedInUser() : User.getByUsername(owner);
			
			String domainName = Utils.getValue(json, DOMAIN);
			Domain domain = Domain.getByName(domainName);
			if(domain == null) {
				Domain_Out domainOut = domainHelper.getDomainByUser(user);
				domain = Domain.getByName(domainOut.getName());
			}

			//Get policies to exclude for this api call
			List<ProxyObject> proxiesToExclude = getProxiesToExcludeFromQuery(user, domain);
			
			//Get Proxy object by excluding given proxies
			List<ProxyObject> proxiesForQuery = ProxyObject.getByOwnerWithComplement(proxiesToExclude, user, domain);
			
			//Get comma seperated signatures
			String signatures = Utils.getCommaSeperatedString(ProxyObject.getSignatures(proxiesForQuery));
			logger.debug("Proxies after substracting: " + signatures);
			
			return signatures;
		}
	}
	
	@Override
	protected List<GenericJSON> postProcess(String queryType, GenericJSON inputJson, List<GenericJSON> outputJson, int pageNo, int pageSize) {
	
		//Add effective date for each list item.
		List<GenericJSON> itemsWithEffectiveDate = enhanceByEffectiveDates(outputJson);
		
		Collections.sort(itemsWithEffectiveDate, new Comparator<GenericJSON>() {
			@Override
			public int compare(GenericJSON o1, GenericJSON o2) {
				Date firstDate = Utils.getDate(Utils.getValue(o1, Effective_Date));
				Date secondDate = Utils.getDate(Utils.getValue(o2, Effective_Date));					
				return firstDate.compareTo(secondDate);
			}
		});
		
		//Apply pagination on items			
		return itemsWithEffectiveDate.subList(0, Math.min(itemsWithEffectiveDate.size(), pageSize));
	}

	/**
	 * Output json need to be enhanced by effective date. Effective date varies based on
	 * Stage of policy.
	 * 
	 * @param input
	 * @return
	 */
	protected abstract List<GenericJSON> enhanceByEffectiveDates(List<GenericJSON> input);
	
	
	/**
	 * Get tagged proxies.
	 * 
	 * @param user
	 * @param domain
	 * @return
	 */
	protected List<ProxyObject> getTaggedProxies(User user, Domain domain) {
		String commSepTags = Utils.getCommaSeperatedValuesForQuery(Utils.getCommaSeperatedString(getTagsToExclude()));
		
		// Get NI Proxy Objects
		List<ProxyObject> taggedProxies = ProxyObject.getByLatestTagsAndDomainAndOwner(commSepTags,
				domain, user, "latestDesposition", 1, Integer.MAX_VALUE);
		
		return taggedProxies;
	}
	

	/**
	 * Get list of proxies to exclude from final query to cariq.
	 * 
	 * @param user
	 * @param domain
	 * @return
	 */
	private List<ProxyObject> getProxiesToExcludeFromQuery(User user, Domain domain) {
		logger.debug("getProxiesToExcludeFromQuery: username=" + user.getUsername() + ", domain=" + domain.getName());

		try (ProfilePoint _doExport_ = ProfilePoint.profileAction(
				"ProfAction_getProxiesToExcludeFromQuery_" + PreShipmentExporter.class.getCanonicalName())) {
			
			
			List<ProxyObject> taggedProxies = getTaggedProxies(user, domain);
			logger.debug("taggedProxies: " + ProxyObject.getSignatures(taggedProxies));

			// Get scheduled appointments
			List<ProxyObject> appointmentScheduledProxies = Appointment
					.getProxies(Appointment.getForUser(user, domain, 1, Integer.MAX_VALUE));
			logger.debug("Appointments: " + ProxyObject.getSignatures(appointmentScheduledProxies));

			// add all proxies in list and return
			List<ProxyObject> allProxies = new ArrayList<>();
			allProxies.addAll(taggedProxies);
			allProxies.addAll(appointmentScheduledProxies);

			return allProxies;
		}
	}
}
