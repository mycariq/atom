package com.cariq.toolkit.utils.scoring;

/**
 * Will be loaded from XML
 * @author Abhijit
 *
 */
public class ParameterDefinition {
	String parameterName;
	double weightage;
	double minValue, maxValue, defaultValue;


	public ParameterDefinition(String parameterName, double weightage,
			double minValue, double maxValue, double defaultValue) {
		super();
		this.parameterName = parameterName;
		this.weightage = weightage;
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.defaultValue = defaultValue;
	}
	
	public String getParameterName() {
		return parameterName;
	}
	public double getWeightage() {
		return weightage;
	}
	public double getMinValue() {
		return minValue;
	}
	public double getMaxValue() {
		return maxValue;
	}

	public double getDefaultValue() {
		return defaultValue;
	}

	public double validateValue(double value) {
		if (value < minValue)
			return minValue;
		
		if (value > maxValue)
			return maxValue;

		return value;
	}
}
