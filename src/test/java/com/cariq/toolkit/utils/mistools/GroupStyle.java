package com.cariq.toolkit.utils.mistools;

public class GroupStyle {
	String groupName;
	String style;
	
	
	public GroupStyle(String groupName) {
		this(groupName, null);
	}


	public GroupStyle(String groupName, String style) {
		super();
		this.groupName = groupName;
		this.style = style;
	}


	public String getGroupName() {
		return groupName;
	}


	public String getStyle() {
		return style;
	}
	
	
}	
