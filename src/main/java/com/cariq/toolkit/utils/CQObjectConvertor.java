/**
 * 
 */
package com.cariq.toolkit.utils;

/**
 * @author hrishi
 *
 */
public interface CQObjectConvertor<IN, OUT> {
	OUT convert(IN in);
}
