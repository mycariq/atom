package com.cariq.toolkit.utils.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.mail.internet.MimeMessage;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.cariq.toolkit.model.User;
import com.cariq.toolkit.utils.APIHelper;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * The Class NotificationServiceImpl.
 */
@Service
public class NotificationServiceImpl implements NotificationService {

	/** The Constant queryToSupport. */
	protected static final String queryToSupport = "support@mycariq.com";// @TMPL protected static final String queryToSupport = "[% ROOT.servers.email.support %]"; @TMPL

	/** The logger. */
	private static CarIQLogger logger = APIHelper.logger.getLogger("NotificationImpl");

	/** The config properties. */
	@Resource(name = "configProperties")
	private Properties configProperties;

	/** The mail template. */
	@Autowired
	private MailSender mailTemplate;

	/** The mail sender. */
	@Autowired
	private JavaMailSender mailSender;

	/** The template message. */
	@Autowired
	private SimpleMailMessage templateMessage;

	/** The velocity engine. */
	@Autowired
	private VelocityEngine velocityEngine;

	/** The notification templates. */
	// Static map of velocity templates and their types.
	private static Map<CarIQNotificationType, String> notificationTemplates = new HashMap<CarIQNotificationType, String>();

	static {
		notificationTemplates.put(CarIQNotificationType.WelcomeToFleetIQ, "velocity/welcomeToFleetIQ.vm");
		notificationTemplates.put(CarIQNotificationType.ForgotPasswordRequest, "velocity/forgot.vm");
		notificationTemplates.put(CarIQNotificationType.Hello, "velocity/hello.vm");
		notificationTemplates.put(CarIQNotificationType.AlertNotification, "velocity/out_of_sync.vm");
		notificationTemplates.put(CarIQNotificationType.ExporterAsyncCompletion, "velocity/exporter_async_completion.vm");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.toolkit.utils.service.NotificationService#sendMessage(java.lang
	 * .String, java.lang.String)
	 */
	@Override
	public void sendMessage(String mailTo, String message) {
		try (ProfilePoint _NotificationService_setMessage = ProfilePoint
				.profileAction("ProfAction_NotificationService_setMessage")) {
			Utils.logAction(logger, "NOTIFICATION_EMAIL", mailTo, null);
			org.springframework.mail.SimpleMailMessage mailMessage = new org.springframework.mail.SimpleMailMessage(
					templateMessage);
			mailMessage.setTo(mailTo);
			mailMessage.setText(message);
			mailTemplate.send(mailMessage);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.toolkit.utils.service.NotificationService#
	 * sendWelComeEmailNotification(com.fleetiq.www.model.Admin)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void sendWelComeEmailNotification(final User user) {
		try (ProfilePoint _sendWelcomeEmailNotification = ProfilePoint
				.profileAction("ProfAction_sendWelcomeEmailNotification")) {
			Utils.logAction(logger, "NOTIFICATION_MAIL_WELCOME", user.getEmail(), "FleetIQ Welcome Mail");
			MimeMessagePreparator preparator = new MimeMessagePreparator() {
				public void prepare(MimeMessage mimeMessage) throws Exception {
					MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
					message.setTo(user.getEmail());
					message.setFrom(emailFrom);
					message.setReplyTo(mailReplyTo);
					message.setSubject("Welcome to FleetIQ");
					Map model = new HashMap();
					model.put("user", user);

					String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
							"velocity/welcomeToFleetIQ.vm", "UTF-8", model);
					message.setText(text, true);
				}
			};
			mailSender.send(preparator);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.toolkit.utils.service.NotificationService#getNotificationText(
	 * com.cariq.toolkit.utils.service.NotificationService.
	 * CarIQNotificationType, java.util.Map)
	 */
	@Override
	public String getNotificationText(final CarIQNotificationType type, final Map<String, Object> model) {
		try (ProfilePoint _sendNotificationEmail = ProfilePoint
				.profileAction("ProfAction_getNotificationMessageText")) {
			model.put("logo", configProperties.getProperty("companyLog"));
			model.put("company", configProperties.getProperty("companyName"));
			model.put("replyTo", mailReplyTo);
			model.put("SMS", false);

			return VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, notificationTemplates.get(type), "UTF-8",
					model);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.toolkit.utils.service.NotificationService#sendNotificationEmail
	 * (java.lang.String, java.lang.String,
	 * com.cariq.toolkit.utils.service.NotificationService.
	 * CarIQNotificationType, java.util.Map)
	 */
	@Override
	public void sendNotificationEmail(final String emailTo, final String subject, final CarIQNotificationType type,
			final Map<String, Object> model) {
		try (ProfilePoint _sendNotificationEmail = ProfilePoint.profileAction("ProfAction_sendNotificationEmail")) {
			// validate email address is null
			if (emailTo == null)
				throw new RuntimeException("Unable to send mail because, Email is null");
			logger.info("Sending email to=" + emailTo + "  subject=" + subject + " velocity="
					+ notificationTemplates.get(type));

			Utils.logAction(logger, "NOTIFICATION_MAIL", emailTo, subject);

			MimeMessagePreparator preparator = new MimeMessagePreparator() {
				public void prepare(MimeMessage mimeMessage) throws Exception {
					MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
					message.setTo(emailTo);
					message.setFrom(emailFrom);
					message.setReplyTo(mailReplyTo);
					message.setSubject(subject);
					message.setText(getNotificationText(type, model), true);
				}
			};
			mailSender.send(preparator);
		}
	}
}