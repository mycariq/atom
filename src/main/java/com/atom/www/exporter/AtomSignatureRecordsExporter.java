package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.atom.www.helper.TimeSlot;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.model.Appointment;
import com.cariq.toolkit.model.Appointment.Appointment_Out;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.ProxyObject;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_In;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_Out;
import com.cariq.toolkit.model.TaggedProxy.TaggedProxy_Out;
import com.cariq.toolkit.model.Timeline.Timeline_Out;
import com.cariq.toolkit.service.TaggedProxyService;
import com.cariq.toolkit.service.TimelineService;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;



/**
 * Retrieve all database records for given signature and add in json and return 
 * 
 * @author sagar
 *
 */
@Configurable
public class AtomSignatureRecordsExporter extends CarIQExporterTemplate {

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("OnboardingProcessExporter");
	
	private static final String TheExporter = "AtomSignatureRecordsExporter";
	private static final String ExporterDescription = "Export all database records for given signature";

	private static final String SIGNATURES = "signatures";
	private static final String DOMAIN = "domain";
	private static final String POLICY_TYPE = "policy_type";
	
	@Autowired
	private TimelineService timelineService;
	
	@Autowired
	private TaggedProxyService taggedProxyService;

	/**
	 * Default constructor
	 */
	public AtomSignatureRecordsExporter() {
		super(TheExporter, ExporterDescription);
		
		// Export All
		addQueryType("All", "Everything", 
				Arrays.asList(new QueryDefinitionAttribute(SIGNATURES, "Comma seperated signatures.", String.class),
						new QueryDefinitionAttribute(DOMAIN, "Domain of given signatures.", String.class),
						new QueryDefinitionAttribute(POLICY_TYPE, "Type of policy of given signatures.", String.class)));
		// Support roles
		addSupportedRoles(
				Arrays.asList(Utils.ADMIN_ROLE, Utils.USER_ROLE, Utils.ROLE_FLEET_ADMIN, Utils.ROLE_GROUP_OPERATOR));
	}

	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON inputJSON, int pageNo, int pageSize) {
		try (ProfilePoint _doExport = ProfilePoint.profileActivity("ProfAction_doExport_AtomSignatureRecordsExporter")) {
						
			String signatures = Utils.getValue(inputJSON, SIGNATURES);
			String domainName = Utils.getValue(inputJSON, DOMAIN);
			String policyType = Utils.getValue(inputJSON, POLICY_TYPE);
			String owner = Utils.getValue(inputJSON, "owner");
			
			List<ProxyObject> proxyObjects;
			
			//retrive all proxy objects for all given signatures
			try (ProfilePoint _getProxyObjectsAtomSignatureRecordsExporter = ProfilePoint
					.profileAction("ProfAction_getProxyObjectsAtomSignatureRecordsExporter")) {
				proxyObjects = ProxyObject.getAllObjectsForSignatures(signatures, Domain.getByName(domainName),
						policyType, pageNo, pageSize);
			}

			//retrieve all timelines for all given signatures
			List<Timeline_Out> timelines;
			try (ProfilePoint _getTimelinesAtomSignatureRecordsExporter = ProfilePoint
					.profileAction("ProfAction_getTimelinesAtomSignatureRecordsExporter")) {
				timelines = timelineService.getTimelines(proxyObjects);
			}

			//retrieve all tagged proxies for all given signatures
			List<TaggedProxy_Out> taggedProxies;
			try (ProfilePoint _getTagsForProxiesAtomSignatureRecordsExporter = ProfilePoint
					.profileAction("ProfAction__getTagsForProxiesAtomSignatureRecordsExporter")) {
				taggedProxies = taggedProxyService.getTagsForProxies(proxyObjects);
			}
			
			List<Appointment> appointments;
			try (ProfilePoint _getTagsForProxiesAtomSignatureRecordsExporter = ProfilePoint
					.profileAction("ProfAction__appointmentsByTimeSlotAtomSignatureRecordsExporter")) {
				appointments = Appointment.getForSlotAndAllDelayed(TimeSlot.getCurrentSlot(),
						Domain.getByName(domainName), pageNo, pageSize);
			}
			
			//chain objects with singatures and return
			return new ObjectChain(signatures).createChain(proxyObjects, timelines, taggedProxies, appointments);
		}
	}
	
	
	/**
	 * Chain objects with specific signature
	 * 
	 * @author sagar
	 *
	 */
	public class ObjectChain {
		private String signatures;
		private ConcurrentHashMap<String, SignatureData> signatureValues;
		
		/**
		 * Constructor
		 * @param signatures
		 */
		public ObjectChain(String signatures) {
			this.signatures = signatures;
			signatureValues = new ConcurrentHashMap<>();
		}
		
		/**
		 * Chain given objects with signatures
		 * 
		 * @param proxyObjects			proxy objects for chaining with signature
		 * @param timelines				timelines for chaining with signature
		 * @param taggedProxies			tagged proxies for chaining
		 * @param appointments 
		 * @return						list of generic jsons having all objects against 
		 * 								perticular signature
		 */
		public List<GenericJSON> createChain(List<ProxyObject> proxyObjects, List<Timeline_Out> timelines,
					List<TaggedProxy_Out> taggedProxies, List<Appointment> appointments) {
			
			try (ProfilePoint _createChain = ProfilePoint.profileAction("ProfAction_createChain")) {
				//Iterate and attach proxy object with signature
				for (ProxyObject object : proxyObjects) {
					SignatureData signatureData = new SignatureData(object.getSignature());
					signatureValues.put(object.getSignature(), signatureData);
					if (signatureData != null) {
						signatureData.setProxyObject(object);
						signatureData.merge();
					}
				}
				
				//iterate and attach timeline with signature
				for (Timeline_Out timeline : timelines) {
					ProxyObject_In proxyObject = timeline.getProxyObject();
					SignatureData signatureData = signatureValues.get(proxyObject.getSignature());
					if (signatureData != null) {
						signatureData.addTimeline(timeline);
						signatureData.merge();
					}
				}
				
				//iterate and attach tagged proxies with signature
				for (TaggedProxy_Out taggedProxy : taggedProxies) {
					ProxyObject_Out proxyObject = taggedProxy.getProxyJSON();
					SignatureData signatureData = signatureValues.get(proxyObject.getSignature());
					if (signatureData != null) {
						signatureData.addProxy(taggedProxy);
						signatureData.merge();
					}
				}
				
				//iterate on appointments and attach
				for(Appointment appointment : appointments) {
					ProxyObject object = appointment.getProxy();
					SignatureData signatureData = signatureValues.get(object.getSignature());
					if(signatureData != null) {
						signatureData.addAppointment(appointment.toJSON());
						signatureData.merge();
					}
				}
				
				//convert signatures into list and return
				List<GenericJSON> finalValues = new ArrayList<GenericJSON>(signatureValues.values());
				return finalValues;
			} 
		}
	}
	
	
	/**
	 * Signature data holder
	 * 
	 * @author sagar
	 *
	 */
	public class SignatureData extends GenericJSON {
		private static final String KEY_SIGNATURE="signature",
				KEY_PROXYOBJECT="proxy_object", KEY_TIMELINE="timeline", KEY_TAGS="tags";
		private static final String KEY_APPOINTMENTS = "appointments";
		private String signature;
		private ProxyObject proxyObject;
		private List<Timeline_Out> timelines;
		private List<TaggedProxy_Out> taggedProxies;
		private List<Appointment_Out> appointments;
		
		/**
		 * constructor
		 * @param signature		signature which will have data from different entities
		 */
		public SignatureData(String signature) {
			this.signature = signature;
			timelines = new ArrayList<>();
			taggedProxies = new ArrayList<>();
			appointments = new ArrayList<>();
			merge();
		}
		
		public void addAppointment(Appointment_Out appointment) {
			appointments.add(appointment);
		}

		/**
		 * Get signature 
		 * @return
		 */
		public String getSignature() {
			return signature;
		}
		
		/**
		 * Add timeline to list
		 * @param timeline
		 * 					timeline of proxy
		 */
		public void addTimeline(Timeline_Out timeline) {
			timelines.add(timeline);
		}
		
		/**
		 * Add tagged proxy object to list
		 * @param taggedProxy
		 * 					tagged proxy
		 */
		public void addProxy(TaggedProxy_Out taggedProxy) {
			taggedProxies.add(taggedProxy);
		}
		
		/**
		 * If proxy object does not exist then it is emtpy object
		 * @return	
		 * 			is emtpy or not
		 */
		public boolean isEmtpy() {
			return proxyObject == null;
		}
		
		/**
		 * Get proxy object associated with signature
		 * @return
		 */
		public ProxyObject getProxyObject() {
			return proxyObject;
		}
		
		/**
		 * Set proxy object with this signature object
		 * @param proxyObject
		 */
		public void setProxyObject(ProxyObject proxyObject) {
			this.proxyObject = proxyObject;
		}
		
		/**
		 * Get list of timelines
		 * @return
		 */
		public List<Timeline_Out> getTimelines() {
			return timelines;
		}
		
		/**
		 * merge current object with hashmap. Put all current values to hashmap
		 */
		public void merge() {
			put(KEY_SIGNATURE, signature);
			put(KEY_PROXYOBJECT, proxyObject);
			put(KEY_TIMELINE, timelines);
			put(KEY_TAGS, taggedProxies);
			put(KEY_APPOINTMENTS, appointments);
		}
		
		public List<Appointment_Out> getAppointments() {
			return appointments;
		}

		/**
		 * Get tagged proxy
		 */
		public List<TaggedProxy_Out> getTaggedProxies() {
			return taggedProxies;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			SignatureData s = (SignatureData) obj;
			if(this.signature.equals(s.getSignature()))
				return true;
			return false;
		}
	}
}
