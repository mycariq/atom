package com.cariq.toolkit.utils.analytics.core;

import java.io.IOException;

import com.cariq.toolkit.utils.profile.ProfilePoint;


public abstract class RecordProcessorTemplate implements RecordProcessor {
	AnalyticsContext processorContext;
	
	public RecordProcessorTemplate(AnalyticsContext processorContext) {
		super();
		this.processorContext = processorContext;
	}


	@Override
	public void close() {
	}

	@Override
	public void init() throws IOException {
		TableWriterStream writerStream = getTableWriterStream();
		if (writerStream != null)
			writerStream.addColumns(getTableReaderStream().getColumns());
	}

	@Override
	public AnalyticsContext getProcessorContext() {
		return processorContext;
	}

	@Override
	public Record process(Record record) throws IOException {
		try (ProfilePoint _RecordProcessor_Process = ProfilePoint
				.profileAction("ProfAction_RecordProcessor_Process")) {
			return doProcess(record);
		}
	}

	protected abstract Record doProcess(Record record) throws IOException;
	
	protected final TableWriterStream getTableWriterStream() {
		return (TableWriterStream) getProcessorContext().get(AnalyticsHelper.TABLE_WRITER_STREAM);
	}
	
	protected TableReaderStream getTableReaderStream() {
		return (TableReaderStream) getProcessorContext().get(AnalyticsHelper.TABLE_READER_STREAM);
	}
	
	protected void addColumn(String columnName) {
		addColumn(new Column(columnName));
	}
	
	protected void addColumn(Column column) {
		TableWriterStream writer = getTableWriterStream();
		if (writer != null)
			getTableWriterStream().addColumn(column);
	}
	
	protected void removeAllColumns() {
		TableWriterStream writer = getTableWriterStream();
		if (writer != null)
			getTableWriterStream().removeAllColumns();
	}
}
