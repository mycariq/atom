package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Configurable;

import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.model.Timeline.Timeline_Out;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

@Configurable
public class StagingAndPolicyDetailsReportExporter extends CarIQExporterTemplate  {

	public static final String debugPolicy = "3001/171759209/00/000";
	
	private static final String TheExporter = "StaginAndPolicyDetailsReportExporter";
	private static final String TheExporterDescription = "Staging and policy details report exporter.";
	
	private static final String wrappedExporterName = "StagingAndInsurancePolicyDetailsExporter";
	private static final String wrappedExporterDescription = "This is Onboarding Report";
	
	private static final String QueryType_createdAfter = "CreatedAfter";
	public static final String QueryType_createdAfterIncludeInvalid = "CreatedAfterIncludeInvalid";
	private static final String QueryType_byMonth = "ForMonth";
	private static final String QueryType_deviceRequested = "Device Requested";
	
	private static final String createdAfterQueryTypeDescription = "All policies created after given date.";
	private static final String createdAfterIncludeInvalidQueryTypeDescription = "All policies created after given date including all invalid.";
	private static final String monthQueryTypeDescriptioin = "All policy details in given month(Month input format : MMYYYY)";
	
	/** The Constant attributeAliasMap. */
	private static final List<String> attributeAliasMap = new ArrayList<>();
	
	public static final String createdAfter = "CreatedAfter";
	private static final String ForMonth = "ForMonth";
	
	private static final String KEY_TIMELINE = "timeline";
	private static final String KEY_NI = "NI";
	private static final String KEY_NC = "NC";
	private static final String KEY_DESPOSITION = "Desposition";
	private static final String KEY_ISSUED_ON = "Policy issued on";
	private static final String KEY_NC_CREATED_ON = "NC on";
	private static final String KEY_NI_CREATED_ON = "NI on";
	private static final String KEY_VERIFIED_ON = "Policy verified on";
	private static final String KEY_REQUESTED_ON = "Device requested on";
	private static final String KEY_SHIPPED_ON = "Device shipped on";
	private static final String KEY_ONBOARDED_ON = "Device onboarded on";
	private static final String KEY_CONNECTED_ON = "Car connected on";
	private static final String KEY_WEEK_NO = "Weeks";
	private static final String KEY_POLICY_STATUS = "Policy_Status";
	
	
	protected static final String KEY_POLICY_NO = "policyNumber";
	protected static final String KEY_DOMAIN_ID = "domainId";
	
	protected static final String proxyType = "InsurancePolicy";
	
	
	public StagingAndPolicyDetailsReportExporter() {
		this(TheExporter, TheExporterDescription);
	}
	
	public StagingAndPolicyDetailsReportExporter(String name, String description) {
		super(name, description);
		
		addExportAttributes(attributeAliasMap);
		
		addQueryType(QueryType_createdAfterIncludeInvalid, createdAfterIncludeInvalidQueryTypeDescription,
				Arrays.asList(new QueryDefinitionAttribute(createdAfter, createdAfterIncludeInvalidQueryTypeDescription, Date.class)));
		
		addQueryType(QueryType_createdAfter, createdAfterQueryTypeDescription,
				Arrays.asList(new QueryDefinitionAttribute(createdAfter, createdAfterQueryTypeDescription, Date.class)));
		
		addQueryType(QueryType_byMonth, monthQueryTypeDescriptioin,
				Arrays.asList(new QueryDefinitionAttribute(ForMonth, monthQueryTypeDescriptioin, String.class)));
		
		// Support roles
		addSupportedRoles(
				Arrays.asList(Utils.ADMIN_ROLE, Utils.USER_ROLE));
	}
	

	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		
		try (ProfilePoint _doExportStagingAndPolicyDetailsReportExporter =
				ProfilePoint.profileAction("ProfAction_doExportStagingAndPolicyDetailsReportExporter")) {
			OnboardingExporter exporter = new OnboardingExporter();
			return postProcess(queryType, json, exporter.doExport(queryType, json, pageNo, pageSize));
		}
	}
	

	/**
	 * Post process data
	 * 
	 * @param queryType
	 * @param inputJson
	 * @param outputJson
	 * @return
	 */
	protected List<GenericJSON> postProcess(String queryType, GenericJSON inputJson, List<GenericJSON> outputJson) {
		
		try (ProfilePoint _postProcess = ProfilePoint
				.profileAction("ProfAction_postProcess_" + this.getClass().getCanonicalName())) {
		
			List<GenericJSON> finalOutputJSON = new ArrayList<GenericJSON>();

			try (ProfilePoint _getAllEnhancedObject = ProfilePoint
					.profileAction("ProfAction_getAllEnhancedObject_" + this.getClass().getCanonicalName())) {
				for (GenericJSON json : outputJson) {

					try (ProfilePoint _getEnhancedObject = ProfilePoint
							.profileAction("ProfAction_getEnhancedObject_" + this.getClass().getCanonicalName())) {
						GenericJSON finalObject = getEnhancedObject(json, queryType, inputJson);
						if(finalObject != null)
							finalOutputJSON.add(finalObject);
					}
				}
			}
		
			return finalOutputJSON;
		}
	}

	
	/**
	 * Enhance object by adding more information to obejct
	 * 
	 * @param json
	 * @param queryType
	 * @param inputJson
	 * @return
	 */
	protected GenericJSON getEnhancedObject(GenericJSON json, String queryType, GenericJSON inputJson) {
		String policyNumber = (String) json.get(KEY_POLICY_NO);
		Long domainId = (Long) json.get(KEY_DOMAIN_ID);
		
		if(policyNumber.equals(debugPolicy)) {
			System.out.println("Found dummy policy");
		}
			
		List<Timeline_Out> timelines = (List<Timeline_Out>) json.get(KEY_TIMELINE);
		
		int isNotContactable = StagingAndPolicyHelper.isNotContactable(timelines) ? 1 : 0;
		int isNotInterested = StagingAndPolicyHelper.isNotInterested(timelines) ? 1 : 0;
		String desposition = getDesposition(timelines);
		
		//Number of weeks between staging created date and now
		int week = StagingAndPolicyHelper.getWeeksFromStagingCreatedDate(json);
		
		//Policy issued on date
		String issuedOn = (String) json.get("StagingCreatedOn");
		
		//Retrieve date on which user became not contactable
		String ncCreatedOn = StagingAndPolicyHelper.getNotContactableDate(timelines);
		
		//Retrieve date on which user became not interested
		String niCreatedOn = StagingAndPolicyHelper.getNotInterestedDate(timelines);
		
		//Retrieve date on which staging verified
		String verifiedOn = (String) json.get("Policy_CreationTime");
		
		//Get device requested on date
		String requestedOn = StagingAndPolicyHelper.getDeviceRequestedOnDate(json);
		
		//Get device shipped on date
		String shippedOn = StagingAndPolicyHelper.getDeviceShippedOnDate(json);
		
		//Get device onboarded on date
		String onboardedOn = (String) json.get("Device_OnboardedOn");
		
		//Get car connected on date
		String connectedOn = (String) json.get("Car_FirstSeenOn");
		
		String policyStatus = StagingAndPolicyHelper.getPolicyStatus(json);
		
		json.put(KEY_NC, isNotContactable);
		json.put(KEY_NI, isNotInterested);
		json.put(KEY_DESPOSITION, desposition);
		json.put(KEY_WEEK_NO, week);
		
		json.put(KEY_ISSUED_ON, issuedOn);
		json.put(KEY_NC_CREATED_ON, ncCreatedOn);
		json.put(KEY_NI_CREATED_ON, niCreatedOn);
		json.put(KEY_VERIFIED_ON, verifiedOn);
		json.put(KEY_REQUESTED_ON, requestedOn);
		json.put(KEY_SHIPPED_ON, shippedOn);
		json.put(KEY_ONBOARDED_ON, onboardedOn);
		json.put(KEY_CONNECTED_ON, connectedOn);
		json.put(KEY_POLICY_STATUS, policyStatus);
		
		//remove duplicate keys(duplicate key added to bring date column on right side)
		json.remove("Device_OnboardedOn");
		
		return json;
	}

	

	/**
	 * Return all tags of all timelines as desposition
	 * 
	 * @param timelines
	 * @return
	 */
	protected String getDesposition(List<Timeline_Out> timelines) {
		if(timelines == null)
			return null;
		StringBuilder sb = new StringBuilder();
		for(Timeline_Out t : timelines) {
			if(t.getTag() != null && !t.getTag().isEmpty()) {
				sb.append(t.getTag());
				sb.append(',');
			}
		}
		if(sb.length() > 0)
			sb.deleteCharAt(sb.length() - 1);
		return sb.toString();
	}	
}
