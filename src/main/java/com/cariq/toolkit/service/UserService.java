package com.cariq.toolkit.service;

import java.util.List;

import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.model.User.UpdateUser_In;
import com.cariq.toolkit.model.User.User_In;
import com.cariq.toolkit.model.User.User_Out;

/**
 * The Interface UserService.
 */
public interface UserService {

	/**
	 * Adds the user.
	 *
	 * @param params
	 *            the params
	 * @return the response with id json
	 */
	public ResponseWithIdJson addUser(User_In params);

	/**
	 * Gets the user.
	 *
	 * @param username
	 *            the username
	 * @return the user
	 */
	public User_Out getUser(String username);

	
	/**
	 * Update user.
	 *
	 * @param params
	 *            the params
	 * @param username
	 *            the username
	 * @return the response json
	 */
	public ResponseJson updateUser(UpdateUser_In params, String username);

	/**
	 * Delete user.
	 *
	 * @param username
	 *            the username
	 * @return the response json
	 */
	public ResponseJson deleteUser(String username);

	/**
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public List<User_Out> getAllUsers(int pageNo, int pageSize);
}