/**
 * 
 */
package com.cariq.toolkit.coreiq.server;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Configurable;

import com.cariq.toolkit.model.async.WorkItem;
import com.cariq.toolkit.service.async.WorkItemDelegateTemplate;
import com.cariq.toolkit.service.async.WorkItemUpdater;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;


/**
 * @author hrishi
 *
 */
@Configurable
public class DummyTimespanWorker extends WorkItemDelegateTemplate {
	static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("DummyTimespanWorker");

	
	/* (non-Javadoc)
	 * @see com.cariq.www.workitem.worker.WorkItemDelegateTemplate#doExecute(com.cariq.www.workitem.model.WorkItem, com.cariq.www.workitem.model.WorkItemUpdater)
	 */
	@Override
	protected void doExecute(WorkItem workItem, WorkItemUpdater updater) {
		try {
			// Create 10000 small objects
//			List<SmallObject> lst = createSmallObjectsOnHeap(10000);
			GenericJSON timespanJSON = workItem.fetchInputJson();
			Long timeSpanSec = Utils.downCast(Long.class, timespanJSON.get("ExecutionTimeSec"));
			logger.debug("Sleeping for " + timeSpanSec + " Milliseconds!");
			Thread.sleep(timeSpanSec * 1000);
			logger.debug("Complete!");
		} catch (Exception e) {
			Utils.handleException(e);
		}
	}

	static class SmallObject {
		Date createdOn;
		int index;
		public SmallObject(int index) {
			super();
			this.index = index;
			createdOn = new Date();
		}
		
	}
	private List<SmallObject> createSmallObjectsOnHeap(int count) {
		List<SmallObject> retval = new ArrayList<SmallObject>();
		
		for (int i = 0; i < count; i++) {
			retval.add(new SmallObject(i));
		}

		return retval;
	}

}
