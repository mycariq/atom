package com.atom.www.loader;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.atom.www.exporter.StagingAndPolicyDetailsReportExporter;
import com.atom.www.exporter.TATForMISExporter;
import com.atom.www.exporter.WeeklyMISReportExporter;
import com.cariq.toolkit.coreiq.exporter.ExporterWorkDefinition;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

/**
 * Generate MIS from TAT - both operations should be run one after the other
 * This loader does series of Export operations in async to generate weekly MIS
 * Input to this is in the form of Start Date (typically 2001-01-01) . If not provided, we'll take it.
 * @author hrishi
 *
 */
public class WeeklyMISGenerator extends MultipleExporterAsyncLoader {
	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("WeeklyMISGenerator");

	public static final String TheLoader = "WeeklyMISGenerator";
	public static final String CreatedAfter = "CreatedAfter";
	
	public WeeklyMISGenerator() {
		super(TheLoader, "Generator to generate Weekly MIS Report");

		// Add Attribute definition(s)
		addAttribute(CreatedAfter, "based on data created After", true);
	}

	@Override
	protected List<ExporterWorkDefinition> getExporterWorkerDefinitions(GenericJSON json) {
		List<ExporterWorkDefinition> retval = new ArrayList<ExporterWorkDefinition>();
		// This requires 2 Exporters, first is TAT exporter, and second is MIS
		String createdAfter = Utils.getValue(String.class, json, CreatedAfter);
		if (createdAfter == null)
			createdAfter = "2001-01-01";
		
		retval.add(new ExporterWorkDefinition(TATForMISExporter.TheExporter,
				StagingAndPolicyDetailsReportExporter.QueryType_createdAfterIncludeInvalid, "csv",
				GenericJSON.build(StagingAndPolicyDetailsReportExporter.createdAfter, createdAfter)));
		
		// second is the MIS report exporter
		retval.add(new ExporterWorkDefinition(WeeklyMISReportExporter.EXPORTER,
				WeeklyMISReportExporter.FROM_TAT_URL_LATEST, "html", new GenericJSON()));

		return retval;
	}

}
