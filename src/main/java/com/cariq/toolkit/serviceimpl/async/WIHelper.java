package com.cariq.toolkit.serviceimpl.async;

import org.apache.log4j.Logger;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;

import com.cariq.toolkit.utils.CSVLogger;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.service.async.WorkItemUpdater;

public class WIHelper {
	public static CarIQLogger logger = new CSVLogger(Logger.getLogger("WorkItem"), "WORKITEM");

	public static final String exporterCDN = "exporter";

	// @TODO use configuration
	public static final String azureBlobPath = "https://cinsharedstorageaccount.blob.core.windows.net";

	public static CarIQLogger getLogger(String innerContext) {
		return logger.getLogger(innerContext);
	}

	public static CarIQLogger getLogger(Class<?> clazz) {
		return getLogger(clazz.getCanonicalName());
	}

	public static WorkItemUpdater getUpdater(JobExecutionContext context) {
		if (null == context)
			return null;

		JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
		if (null == jobDataMap)
			return null;

		WorkItemUpdater updater = (WorkItemUpdater) jobDataMap.get(CarIQJobExecutor.UPDATER);
		if (null == updater)
			return null;
		return updater;
	}

	public static String getCarLogPath(String logName) {
		return azureBlobPath + "/" + exporterCDN + "/" + logName;
	}
}
