package com.cariq.toolkit.coreiq.tableUtil.application.mis.arbiter;

import java.util.List;

import com.cariq.toolkit.utils.GenericJSON;

public class CellValueArbiterDefaultOff extends MISCellValueArbiterBase {

	public CellValueArbiterDefaultOff(List list) {
		super(list);
	}

	@Override
	public boolean isApplicable(GenericJSON input) {
		return false;
	}

}
