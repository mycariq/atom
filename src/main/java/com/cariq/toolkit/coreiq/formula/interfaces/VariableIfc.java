package com.cariq.toolkit.coreiq.formula.interfaces;

import com.cariq.toolkit.utils.GenericJSON;

public interface VariableIfc {
	String getName();
	GenericJSON getValueDefinition();
	VariableResolverIfc getVariableResolver();
	Object resolve(Object context, boolean force);
}
