package com.cariq.toolkit.coreiq.tableUtil.application.mis;

import com.cariq.toolkit.coreiq.tableUtil.impl.ProcessingContext;
import com.cariq.toolkit.coreiq.tableUtil.impl.RowColumn;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.CellAddressIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.NameIndexIdentifiable;

/**
 * MISRow is responsible for managing "Row" in MIS Table.
 * @author hrishi
 *
 */
public class MISRow extends MISRowColumn {
	public MISRow(String style, String label, String evaluatorClass, String argumentJSON) {
		super(style, label, evaluatorClass, argumentJSON);
	}
	
	public MISRow(String label, String evaluatorClass, String argumentJSON) {
		this(null, label, evaluatorClass, argumentJSON);
	}

	public MISRow() {
		super();
	}

	@Override
	public boolean isMatching(ProcessingContext processingContext, CellAddressIfc cellAddress) {
		// If cellAddress is not there, based on processing Context, check the current cells index
		NameIndexIdentifiable rowIdentifier = cellAddress.getRowIdentifier();
		if (rowIdentifier == null) {
			// from processingContext, get the row under process.
			RowColumn currentRow = processingContext.getCurrentRow();
			return currentRow.getIndex() == this.getIndex();
		}
		
		// If identifier specified, use it
		// check index
		Integer colIndex = rowIdentifier.getIndex();
		if (colIndex != null)
			return colIndex == this.getIndex();
		
		String name = rowIdentifier.getName();
		if (name != null)
			return name.equalsIgnoreCase(this.getLabel());
		
		return false;
	}
}

