/**
 * 
 */
package com.atom.www.exporter;

/**
 * @author santosh
 *
 */
public class MISTATExporter extends MISTATEnhancerExporter {

	public static final String EXPORTER = "MIS-TAT-Exporter", DESCRIPTION = "New tat Report to generate MIS Report";

	public MISTATExporter() {
		super(EXPORTER, DESCRIPTION);
	}
}