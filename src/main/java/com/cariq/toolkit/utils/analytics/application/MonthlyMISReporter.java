package com.cariq.toolkit.utils.analytics.application;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;

import com.cariq.toolkit.coreiq.tableUtil.application.mis.MISProcessingContext;
import com.cariq.toolkit.coreiq.tableUtil.application.mis.MISReport;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.analytics.core.AnalyticsContext;
import com.cariq.toolkit.utils.analytics.core.Column;
import com.cariq.toolkit.utils.analytics.core.Record;
import com.cariq.toolkit.utils.analytics.core.RecordProcessorTemplate;
import com.cariq.toolkit.utils.jsonProcessor.JSONObjectBuilder;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.google.common.base.Strings;

public class MonthlyMISReporter extends RecordProcessorTemplate {
	public static final String MIS_REPORT_JSON_PATH = "MISReportJsonPath";
	public static final String MONTHLY_MIS_JSON = "MonthlyMISJSON";
	
	private int dummyCount = 0;
	
	// Months constants
	private static final String[] MONTHS_MMM = new String[] {"Jan", "Feb", "Mar", "Apr", "May", "Jun", 
				"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	// Columns constants
	private static final String TAT_COL_ISSUE_MONTH = "Issue Month";
	private static final String TAT_COL_VERTICAL = "Vertical";
	private static final String TAT_COL_SUBVERTICAL = "SubVertical";
	private static final String TAT_COL_COMBOVERTICAL = "Combo Vertical";
	private static final String TAT_COL_DEVICE_VERIFICATION_BUCKET = "Device verification TAT Bucket";
	private static final String TAT_COL_DEVICE_REQ_BUCKET = "Device req TAT Bucket";
	private static final String TAT_COL_DEVICE_SHIPPED_BUCKET = "Device shipped TAT Bucket";
	private static final String TAT_COL_CAR_CONNECTED_BUCKET = "Car connected TAT Bucket";
	private static final String TAT_COL_DISPATCH_STATUS = "Dispatch Status";
	private static final String TAT_COL_COMPLETED_ONE_YEAR = "CompletedOneYear";
	
	private static final String TAT_COL_CITY_TIER = "City Tier";
	private static final String TAT_COL_SUBSCRIPTION_STATE = "SubscriptionState";
	private static final String TAT_COL_VERIFIED_TAT = "Verified TAT"; 
	private static final String TAT_COL_DEVICE_REQ_TAT = "Device Requested TAT";
	private static final String TAT_COL_DEVICE_SHIPPED_TAT = "Device Shipped TAT";
	private static final String TAT_COL_CAR_CONNECTED_TAT = "Car Connected TAT";
	private static final String TAT_COL_POLICY_STAGE = "Policy Stage";
	private static final String TAT_COL_POLICY_NUMBER = "Policy Number";
	private static final String TAT_COL_POLICY_EXPIRY_DATE = "Policy Expiry Date";
	
	private MISReport report;
	
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("MonthlyMISReporter");

	public MonthlyMISReporter(AnalyticsContext context) {
		super(context);
	}
	
	/**
	 * Keep only the relevant columns - don't get rest
	 */
	@Override
	public void init() throws IOException {
		super.init();
		// Extra columns to be added to the input TAT
		super.addColumn(TAT_COL_COMBOVERTICAL);
		super.addColumn(TAT_COL_DEVICE_VERIFICATION_BUCKET);
		super.addColumn(TAT_COL_DEVICE_REQ_BUCKET);
		super.addColumn(TAT_COL_DEVICE_SHIPPED_BUCKET);
		super.addColumn(TAT_COL_CAR_CONNECTED_BUCKET);
		super.addColumn(TAT_COL_DISPATCH_STATUS);
		super.addColumn(TAT_COL_COMPLETED_ONE_YEAR);
		
		//Init the MISReport table and add to the context
		String jsonPath= getProcessorContext().get(MIS_REPORT_JSON_PATH).toString();
		if (Strings.isNullOrEmpty(jsonPath)) {
			Utils.handleException("MonthlyMISReporter.init(): Failed to get " + MIS_REPORT_JSON_PATH);
		}
				
		JSONObjectBuilder builder = new JSONObjectBuilder(jsonPath);

		report = new MISReport();
		logger.debug("===> registering builder json");
		builder.register(report.getJSONLoadable());
		logger.debug("===> processing builder json");
		builder.process();
	}
	
	@Override
	public void close() {
		GenericJSON outputJSON = report.toGenericJSON();
		getProcessorContext().put(MONTHLY_MIS_JSON, Utils.getJSonString(outputJSON));
		logger.debug("===> Skipped Dummy policies from TAT: " + dummyCount);
		//logger.debug("===> OutputJSON: " + Utils.getJSonString(outputJSON));
	}

	private Record replaceNullValue(Record record, Column col) {
		String value = record.getStringValue(col);
		if (Strings.isNullOrEmpty(value)) {
			record.set(col, "NA");
		}
		return(record);	
	}
	
	private Record replaceMonthValue(Record record, Column col) {
		String value = record.getStringValue(col);
		
		if (! Strings.isNullOrEmpty(value)) {
			if (value.equalsIgnoreCase("Not Available-Not Available")) {
				record.set(col, "NA");
			} else if (value.indexOf('/') > 0) { // Replace values like 02/20 to Feb-20
				String newValue = value.replace('/','-');
				int index = Integer.parseInt(newValue.split("-")[0]);
				int year = Integer.parseInt(newValue.split("-")[1]);
				record.set(col, MONTHS_MMM[index - 1] + "-" + year);
			} else if(value.indexOf('-') > 0) { // Replace values like February-2020 to Feb-20
				if (Arrays.toString(MONTHS_MMM).contains(value.substring(0, 3))) { // Specifically check if value has month
					String [] parts = value.split("-");
					String monthVal = parts[0].substring(0, 3) + "-" + parts[1].substring(2, 4);
					record.set(col, monthVal);
				}
			}
		}
		
		return(record);	
	}
	
	private String findTATBucket(Long days) {
		String tatBucket = "Above 30 DAYS";
		
		if (days < 8) 
			tatBucket = "0-7 DAYS";
		else if (days < 15)
			tatBucket = "8-14 DAYS";
		else if (days < 22)
			tatBucket = "15-21 DAYS";
		else if (days < 31)
			tatBucket = "22-30 DAYS";
		
		return tatBucket;
	}
	
	private String getTATBucketValue(Record record, Column tatColumn) {
		String tatBucketValue = "ERROR";
		String tatValue = record.getStringValue(tatColumn);
		
		if (! Strings.isNullOrEmpty(tatValue)) {
			if (tatValue.equalsIgnoreCase("N/A"))
				tatBucketValue = "NA";
			else if (Long.parseLong(tatValue) >= 0) {
				tatBucketValue = findTATBucket(Long.parseLong(tatValue));
			}
		}
		
		return tatBucketValue;
	}
	
	private String getComboVerticalValue(Record record) {
		String comboValue = "Others";
		String subVertical = record.getStringValue(new Column(TAT_COL_SUBVERTICAL));
		String vertical = record.getStringValue(new Column(TAT_COL_VERTICAL));
		
		if(!Strings.isNullOrEmpty(subVertical) && !subVertical.equalsIgnoreCase("N/A")) {
			if(subVertical.equalsIgnoreCase("TELESALES") || subVertical.equalsIgnoreCase("AGENCY"))
				comboValue = subVertical;
		} else if (!Strings.isNullOrEmpty(vertical) && !vertical.equalsIgnoreCase("N/A")) {	
			if(vertical.equalsIgnoreCase("E Channel") || vertical.equalsIgnoreCase("AGENCY") || 
					vertical.equalsIgnoreCase("Others"))
				comboValue = vertical;
		}
		
		return comboValue;
	}
	
	private String getDeviceDispatchStatus(Record record) {
		String status = "Device Not Dispatched";
		String policyStage = record.getStringValue(new Column(TAT_COL_POLICY_STAGE));
		
		if (! Strings.isNullOrEmpty(policyStage)) {
			if (policyStage.equalsIgnoreCase("Car Connected") ||  policyStage.equalsIgnoreCase("Device OnBoarded")
				|| policyStage.equalsIgnoreCase("Device Delivered") || policyStage.equalsIgnoreCase("Device RTO")
				|| policyStage.equalsIgnoreCase("Device Shipped"))
				status = "Device Dispatched";
		}
		
		return status;
	}
	
	private String isCompletedOneYear(Record record) {
		String dateValue = record.getStringValue(new Column(TAT_COL_POLICY_EXPIRY_DATE));
		if (Strings.isNullOrEmpty(dateValue))
			return "no";
		
		Date expiryDate = Utils.parseDate(dateValue);	
		if (null == expiryDate)
			return "no";
		
		// Check iF the policy has completed 1 year in last month
		Date prevMonthDate = Utils.getDaysBefore(new Date(), 30);
		if (prevMonthDate.getMonth() == expiryDate.getMonth() && prevMonthDate.getYear() == expiryDate.getYear())
			return "yes";
		
		return "no";
	}
	
	private Record updateRecord(Record record) {
		record = replaceMonthValue(record, new Column(TAT_COL_ISSUE_MONTH));
		record = replaceNullValue(record, new Column(TAT_COL_CITY_TIER));
		record = replaceNullValue(record, new Column(TAT_COL_SUBSCRIPTION_STATE));
		record.append(TAT_COL_COMBOVERTICAL, getComboVerticalValue(record));
		record.append(TAT_COL_DEVICE_VERIFICATION_BUCKET, getTATBucketValue(record,  new Column(TAT_COL_VERIFIED_TAT)));
		record.append(TAT_COL_DEVICE_REQ_BUCKET, getTATBucketValue(record, new Column(TAT_COL_DEVICE_REQ_TAT)));
		record.append(TAT_COL_DEVICE_SHIPPED_BUCKET, getTATBucketValue(record, new Column(TAT_COL_DEVICE_SHIPPED_TAT)));
		record.append(TAT_COL_CAR_CONNECTED_BUCKET, getTATBucketValue(record, new Column(TAT_COL_CAR_CONNECTED_TAT)));
		record.append(TAT_COL_DISPATCH_STATUS, getDeviceDispatchStatus(record));
		record.append(TAT_COL_COMPLETED_ONE_YEAR, isCompletedOneYear(record));
		
		return record;
	}
	
	/**
	 * Copy the contents of the record for relevant columns in the output Later,
	 * investigate specific values (e.g. speed etc.)
	 */
	@Override
	protected Record doProcess(Record record) {
		try (ProfilePoint _MonthlyMISReporter_doProcess = ProfilePoint.profileAction("ProfAction_MonthlyMISReporter_doProcess")) {
			// Placeholder - need a better pattern
			MISProcessingContext context = new MISProcessingContext();
			
			GenericJSON recordJSON = new GenericJSON();
			
			// Skip dummy policies
			String policyNumber = record.getStringValue(new Column(TAT_COL_POLICY_NUMBER));
			if (!Strings.isNullOrEmpty(policyNumber) && policyNumber.toLowerCase().contains("dummy")) {
				dummyCount++;
				return null;
			}
			
			// Update the record
			record = updateRecord(record);
			
			// Update the report
			recordJSON.putAll(record.jsonize());
			report.process(context, recordJSON);
			
			return record;
		}
	}
}
