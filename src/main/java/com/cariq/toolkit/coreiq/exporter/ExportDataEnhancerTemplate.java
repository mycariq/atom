/**
 * 
 */
package com.cariq.toolkit.coreiq.exporter;

import java.util.Arrays;
import java.util.List;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.profile.ProfilePoint;



/**
 * @author hrishi
 *
 */
public abstract class ExportDataEnhancerTemplate implements ExporterDataEnhancer {
	// Template pattern
	protected abstract GenericJSON doEnhance(String queryType, GenericJSON inputJSON, GenericJSON exportJSON);
	protected abstract List<GenericJSON> doEnhance(String queryType, GenericJSON inputJSON, GenericJSON previous, GenericJSON current);
	protected abstract List<GenericJSON> doEnhance(String queryType, GenericJSON inputJSON, List<GenericJSON> exportJSON);
	protected abstract List<GenericJSON> doEnhance(String queryType, GenericJSON inputJSON, GenericJSON previous, List<GenericJSON> exportJSON);

	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.exporter.ExporterDataEnhancer#enhance(com.cariq.www.utils.GenericJSON)
	 */
	@Override
	public GenericJSON enhance(String queryType, GenericJSON inputJSON, GenericJSON exportJSON) {
		try (ProfilePoint _ExportDataEnhancer_enhance1 = ProfilePoint.profileAction("ProfAction_ExportDataEnhancer_enhance1")) {
			return doEnhance(queryType, inputJSON, exportJSON);
		} catch (Exception e) {
			return exportJSON;
		}
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.exporter.ExporterDataEnhancer#enhance(com.cariq.www.utils.GenericJSON, com.cariq.www.utils.GenericJSON)
	 */
	@Override
	public List<GenericJSON> enhance(String queryType, GenericJSON inputJSON, GenericJSON previous, GenericJSON current) {
		try (ProfilePoint _ExportDataEnhancer_enhance2 = ProfilePoint.profileAction("ProfAction_ExportDataEnhancer_enhance2")) {
			return doEnhance(queryType, inputJSON, previous, current);
		} catch (Exception e) {
			return Arrays.asList(current);
		}	
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.exporter.ExporterDataEnhancer#enhance(java.util.List)
	 */
	@Override
	public List<GenericJSON> enhance(String queryType, GenericJSON inputJSON, List<GenericJSON> exportJSON) {
		try (ProfilePoint _ExportDataEnhancer_enhance3 = ProfilePoint.profileAction("ProfAction_ExportDataEnhancer_enhance3")) {
			return doEnhance(queryType, inputJSON, exportJSON);
		} catch (Exception e) {
			return exportJSON;
		}	
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.exporter.ExporterDataEnhancer#enhance(com.cariq.www.utils.GenericJSON, java.util.List)
	 */
	@Override
	public List<GenericJSON> enhance(String queryType, GenericJSON inputJSON, GenericJSON previous, List<GenericJSON> exportJSON) {
		try (ProfilePoint _ExportDataEnhancer_enhance4 = ProfilePoint.profileAction("ProfAction_ExportDataEnhancer_enhance4")) {
			return doEnhance(queryType, inputJSON, previous, exportJSON);
		} catch (Exception e) {
			return exportJSON;
		}	
	}
}
