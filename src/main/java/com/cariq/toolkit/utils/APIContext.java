package com.cariq.toolkit.utils;

/**
 * The Class APIContext.
 */
public class APIContext {

	public static final String CAR_ID = "CAR_ID", USER_NAME = "USER_NAME", TIMEZONE = "TIMEZONE",
			OPERATION = "OPERATION";

	/** The thread local. */
	private static ThreadLocal<APIContext> threadLocal = new ThreadLocal<APIContext>() {
		synchronized protected APIContext initialValue() {
			return null;
		};

	};

	/** The value map. */
	private GenericJSON valueMap;

	/**
	 * Instantiates a new API context.
	 */
	public APIContext() {
		valueMap = new GenericJSON();
	}

	/**
	 * Sets the context.
	 *
	 * @param context
	 *            the new context
	 */
	public static void setContext(APIContext context) {
		if (threadLocal.get() != null)
			Utils.logAnomaly(APIHelper.logger, "APICONTEXT", null, "Replacing api context");
		threadLocal.set(context);
	}

	/**
	 * Gets the context.
	 *
	 * @return the context
	 */
	public static APIContext getContext() {
		return threadLocal.get();
	}

	/**
	 * Gets the value.
	 *
	 * @param key
	 *            the key
	 * @return the value
	 */
	public Object getValue(String key) {
		return valueMap.get(key);
	}

	/**
	 * Sets the value.
	 *
	 * @param key
	 *            the key
	 * @param value
	 *            the value
	 */
	public void setValue(String key, Object value) {
		if (value == null)
			return;
		valueMap.put(key, value);
	}

	/**
	 * Removes the.
	 */
	public static void remove() {
		threadLocal.remove();
	}
}
