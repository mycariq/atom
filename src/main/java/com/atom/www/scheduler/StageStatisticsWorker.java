package com.atom.www.scheduler;

import java.util.Date;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Configurable;

import com.atom.www.exporter.OnboardingExporter;
import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.ProxyObject;
import com.cariq.toolkit.model.StageStatistics;
import com.cariq.toolkit.model.StageStatistics.StageStats_In;
import com.cariq.toolkit.model.Timeline.Timeline_Out;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.serviceimpl.async.CarIqJobWorker;
import com.cariq.toolkit.serviceimpl.async.WIHelper;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * This crawler retrieves one month before policies from cariq and 
 * updates {@link StageStatistics } model. 
 * 
 * @author sagar
 *
 */
@Configurable
public class StageStatisticsWorker extends CarIqJobWorker {

	private static final CarIQLogger logger = WIHelper.getLogger("StageStatsCrawler");
	
	private static final int pageNumber = 1;
	private static final int pageSize = 200000;
	
	private static final String KEY_DOMAIN = "domain";
	private static final String KEY_TIMELINE = "timeline";
	protected static final String KEY_POLICY_NO = "policyNumber";
	private static final String PROXY_TYPE = "InsurancePolicy";
	private static final String STAGE = "State";
	
	//query type to fetch all policies after given date
	private static final String CreatedAfter = "CreatedAfter";
	
	/** Policies to process after this date **/
	private Date policiesToProcessAfter = Utils.dateMonthBefore(new Date(), 1); //1 Month
	
	private static String commaSepDomains = null;

	@Override
	protected void executeInternal(JobExecutionContext context) throws Exception {
		logger.debug("executeInternal: ");
		
		try (ProfilePoint _postProcess = ProfilePoint
				.profileAction("ProfAction_executeInternal_" + this.getClass().getCanonicalName())) {
		
			commaSepDomains = Domain.getCommaSepDomains(pageNumber, pageSize);

			OnboardingExporter onboardingExporter = new OnboardingExporter();
			GenericJSON inputJSON = new GenericJSON();
			inputJSON.put(CreatedAfter, Utils.changeDateFormat(policiesToProcessAfter, Utils.YYYY_mm_dd_hh_mm_ss));
			inputJSON.put(KEY_DOMAIN, commaSepDomains);

			List<GenericJSON> response = onboardingExporter.export(CreatedAfter, inputJSON, pageNumber, pageSize);
			
			logger.debug("Number of tage statistics entries to update: " + (response != null ? response.size() : 0));
			
			try (ProfilePoint _createAndSaveStatistics = ProfilePoint
					.profileAction("ProfAction_createAndSaveStatistics_" + this.getClass().getCanonicalName())) {
				createAndSaveStatistics(response);
			}
		}
	}

	/**
	 * Create and save statistics for each json
	 * @param response
	 */
	@SuppressWarnings("unchecked")
	private void createAndSaveStatistics(List<GenericJSON> response) {
		//iterate on list
		for(GenericJSON json : response) {
			//retrieve all values for single json
			List<Timeline_Out> timelines = (List<Timeline_Out>) json.get(KEY_TIMELINE);
			String signature = Utils.getValue(json, KEY_POLICY_NO);
			String domainName = Utils.getValue(json, "domainName");
			
			//find owner
			ProxyObject proxy = ProxyObject.getByDomainSignatureObjectType
					(Domain.getByName(domainName), signature, PROXY_TYPE);
			User owner = proxy != null ?  proxy.getOwner() : null;
			String ownerUsername = owner != null ? owner.getUsername() : null;
			
			String stage = Utils.getValue(json, STAGE);
			
			//latest disposition as state. 
			String state = proxy != null ? proxy.getLatestDesposition() : null;
			String subscriptionState = Utils.getValue(json, "SubscriptionState");
			String nextAction = Utils.getValue(json, "NextAction");
			String refPolicyNumber = Utils.getValue(json, "ReferencePolicyNumber");
			
			String entryDate = Utils.getValue(json, "StagingCreatedOn");
			String verifiedDate = Utils.getValue(json, "Policy_CreationTime");
			String deviceRequestedDate = StagingAndPolicyHelper.getDeviceRequestedOnDate(json);
			String deviceShippedOnDate = Utils.getValue(json, "ShippedOn");
			String deviceDeliveredOnDate = Utils.getValue(json, "DeliveredOn");
			String deviceOnboardedDate = Utils.getValue(json, "Device_OnboardedOn");
			String carConnectedDate = Utils.getValue(json, "Car_FirstSeenOn");
			String niDate = StagingAndPolicyHelper.getNotInterestedDate(timelines);
			String ncDate = StagingAndPolicyHelper.getNotContactableDate(timelines);
			String deactivatedOn = Utils.getValue(json, "PolicyDeactivatedOn");
			String renewedOn = Utils.getValue(json, "PolicyRenewedOn");
			
			//make input json
			StageStats_In inJson = new StageStats_In(signature, domainName, ownerUsername, stage, 
					state, subscriptionState, nextAction, refPolicyNumber, entryDate, verifiedDate, deviceRequestedDate, deviceShippedOnDate,
					deviceDeliveredOnDate, deviceOnboardedDate, carConnectedDate, niDate, ncDate,
					deactivatedOn, renewedOn);
			
			logger.debug("Updating entry with signature: " + signature);
			
			//create or update
			StageStatistics.createOrUpdate(inJson);
		}
	}
}
