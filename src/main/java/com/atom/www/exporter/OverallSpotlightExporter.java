package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.cariq.toolkit.coreiq.exporter.CarIQExporter;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterRegistry;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * Overall spotlight exporter. Get items from all other spotlights.
 * 
 * @author sagar
 */
public class OverallSpotlightExporter extends CarIQExporterTemplate {

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("OverallSpotlightExporter");
	
	public static final String TheExporter = "OverallSpotlightExporter";
	private static final String TheDescription = "It gives overall spotlight items";
	
	public static final String QueryType_default = "ByOwnerAndDomain";
	public static final String defaultQueryTypeDescription = "Get appointments by owner and domain.";
	
	public static final String DOMAIN = "domain";
	protected static final String KEY_OWNER = "owner";
	
	/**
	 * Default constructor 
	 */
	public OverallSpotlightExporter() {
		super(TheExporter, TheDescription);
		
		addQueryType(QueryType_default, defaultQueryTypeDescription,
				Arrays.asList(new QueryDefinitionAttribute(KEY_OWNER, "Username of owner", String.class),
						new QueryDefinitionAttribute(DOMAIN, "Name of domain", String.class)));
	}

	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		logger.debug("doExport: " + queryType + ", json: " + json.toString() 
		+ ", pageNo: " + pageNo + ", pageSize: " + pageSize);
		
		try (ProfilePoint _doExportOverallSpotlightExporter =
				ProfilePoint.profileAction("ProfAction_doExportOverallSpotlightExporter")) {

			//Get all exporters
			CarIQExporter preShipment = CarIQExporterRegistry.getExporter(PreShipmentExporter.TheExporter);
			CarIQExporter postShipment = CarIQExporterRegistry.getExporter(PostShipmentExporter.TheExporter);
			CarIQExporter appointmentExporter = CarIQExporterRegistry.getExporter(AppointmentExporter.TheExporter);

			List<GenericJSON> retValues = new ArrayList<>();
			
			//Get one page from preshipment exporter
			List<GenericJSON> preShipValues = preShipment.export(PreShipmentExporter.QueryType_default, json, pageNo, pageSize * 5);
			
			//Get one page from postshipment exporter
			List<GenericJSON> postShipValues = postShipment.export(PostShipmentExporter.QueryType_default, json, pageNo, pageSize * 5);
			
			//Get one page from appointment exporter.
			List<GenericJSON> appointments = appointmentExporter.export(AppointmentExporter.QueryType_default, json, pageNo, pageSize * 5);
			
			int index = 0;
			//Do this while return list fills completely
			while(retValues.size() <= pageSize) {
				logger.debug("OverallExporterValuesFill_Loop: ");
				
				//Add one item from preShipment list
				if(!Utils.isNullOrEmpty(preShipValues))
					retValues.add(preShipValues.remove(index));
				
				//Add one item from post shipment list
				if(!Utils.isNullOrEmpty(postShipValues))
					retValues.add(postShipValues.remove(index));
				
				//Add one item from appointment list
				if(!Utils.isNullOrEmpty(appointments))
					retValues.add(appointments.remove(index));
				
				//If all lists are empty then break.(To avoid recursion).
				if(Utils.isNullOrEmpty(preShipValues) 
						&& Utils.isNullOrEmpty(postShipValues) && Utils.isNullOrEmpty(appointments)) {
					logger.debug("OverallExporterValuesFill_Loop: All lists are emtpy!. Break Loop!!!!!");
					break;
				}
			}
			
			return retValues.subList(0, Math.min(pageSize, retValues.size()));
		}
	}
}
