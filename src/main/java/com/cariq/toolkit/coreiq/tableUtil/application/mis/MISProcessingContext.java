package com.cariq.toolkit.coreiq.tableUtil.application.mis;

import com.cariq.toolkit.coreiq.tableUtil.impl.ProcessingContext;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

/**
 * Processing Context to manage the information while processing table
 * Can hold current 
 * @author hrishi
 *
 */
public class MISProcessingContext implements ProcessingContext  {
	MISReport currentReport;
	MISTable currentTable;
	MISRow currentRow;
	MISColumn currentColumn;
	MISCell currentCell;
	GenericJSON additionalInfo;
	

	public MISProcessingContext() {
		super();
	}
	
	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.tableUtil.application.mis.ProcessingContext#getCurrentReport()
	 */
	@Override
	public MISReport getCurrentReport() {
		return currentReport;
	}
	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.tableUtil.application.mis.ProcessingContext#set(com.cariq.toolkit.coreiq.tableUtil.application.mis.MISReport)
	 */
	public void set(MISReport currentReport) {
		this.currentReport = currentReport;
	}
	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.tableUtil.application.mis.ProcessingContext#getCurrentTable()
	 */
	@Override
	public MISTable getCurrentTable() {
		return currentTable;
	}
	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.tableUtil.application.mis.ProcessingContext#set(com.cariq.toolkit.coreiq.tableUtil.application.mis.MISTable)
	 */
	public void set(MISTable currentTable) {
		this.currentTable = currentTable;
	}
	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.tableUtil.application.mis.ProcessingContext#getCurrentRow()
	 */
	@Override
	public MISRow getCurrentRow() {
		return currentRow;
	}
	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.tableUtil.application.mis.ProcessingContext#set(com.cariq.toolkit.coreiq.tableUtil.application.mis.MISRow)
	 */
	public void set(MISRow currentRow) {
		this.currentRow = currentRow;
	}
	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.tableUtil.application.mis.ProcessingContext#getCurrentColumn()
	 */
	@Override
	public MISColumn getCurrentColumn() {
		return currentColumn;
	}
	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.tableUtil.application.mis.ProcessingContext#set(com.cariq.toolkit.coreiq.tableUtil.application.mis.MISColumn)
	 */
	public void set(MISColumn currentColumn) {
		this.currentColumn = currentColumn;
	}
	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.tableUtil.application.mis.ProcessingContext#getCurrentCell()
	 */
	@Override
	public MISCell getCurrentCell() {
		return currentCell;
	}
	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.tableUtil.application.mis.ProcessingContext#set(com.cariq.toolkit.coreiq.tableUtil.application.mis.MISCell)
	 */
	public void set(MISCell currentCell) {
		this.currentCell = currentCell;
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.tableUtil.application.mis.ProcessingContext#getAdditionalInfo()
	 */
	@Override
	public GenericJSON getAdditionalInfo() {
		return additionalInfo;
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.tableUtil.application.mis.ProcessingContext#setAdditionalInfo(com.cariq.toolkit.utils.GenericJSON)
	 */
	public void setAdditionalInfo(GenericJSON additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
	
	// Context Setter with RAII pattern... for safty and fun
	// Inner class for setters - so that caller does not forget to reset the context
	public abstract class ContextSetter<T> implements AutoCloseable {		
		public ContextSetter(T newValue) {
			super();
			setValue(newValue);
		}

		protected abstract void setValue(T value);

		@Override
		public void close() {
			setValue(null);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.tableUtil.application.mis.ProcessingContext#getContextSetter(T)
	 */
	@SuppressWarnings("unchecked")
	public <T> ContextSetter<T> getContextSetter(T report) {
		Class<?> clzz = report.getClass();
		if (clzz.equals(MISReport.class)) {
			return (ContextSetter<T>) new ContextSetter<MISReport>((MISReport)report) {
				protected void setValue(MISReport value) {set(value);}
			};
		}

		if (clzz.equals(MISTable.class)) {
			return (ContextSetter<T>) new ContextSetter<MISTable>((MISTable)report) {
				protected void setValue(MISTable value) { set(value);}
			};
		}
		
		if (clzz.equals(MISRow.class)) {
			return (ContextSetter<T>) new ContextSetter<MISRow>((MISRow)report) {
				protected void setValue(MISRow value) { set(value);}
			};
		}
		
		if (clzz.equals(MISColumn.class)) {
			return (ContextSetter<T>) new ContextSetter<MISColumn>((MISColumn)report) {
				protected void setValue(MISColumn value) { set(value);}
			};
		}
		
		if (clzz.equals(MISCell.class)) {
			return (ContextSetter<T>) new ContextSetter<MISCell>((MISCell)report) {
				protected void setValue(MISCell value) { set(value);}
			};
		}
		
		Utils.handleException("Invalid class for ProcessingContext: " + clzz.getCanonicalName());
		return null;
	}
}
