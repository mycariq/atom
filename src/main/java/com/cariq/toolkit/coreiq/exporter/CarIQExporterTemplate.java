package com.cariq.toolkit.coreiq.exporter;

import java.util.Arrays;
import java.util.List;

import com.cariq.toolkit.utils.APIContext;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.HtmlValueIdentityRenderer;
import com.cariq.toolkit.utils.HtmlValueRenderer;
import com.cariq.toolkit.utils.SimplePOJOs.CountPOJO;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

public abstract class CarIQExporterTemplate implements CarIQExporter {
	protected static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("CarIQExporter");
	CarIQExporterDefinition definition;

	public CarIQExporterTemplate(String name, String description) {
		super();
		definition = new CarIQExporterDefinition(name, description);
	}

	@Override
	public String getName() {
		return definition.getName();
	}

	@Override
	public String getDescription() {
		return definition.getDescription();
	}

	@Override
	public String getShortName() {
		return definition.getShortName();
	}

	@Override
	public CarIQExporterDefinition getExporterDefinition() {
		return definition;
	}

	protected void addQueryType(String name, String description, String[] attrList) {
		definition.add(CarIQExporterDefinition.QUERY_DEF, new CarIQQueryDefinition(name, description, attrList));
	}

	protected void addQueryType(String name, String description, List<QueryDefinitionAttribute> attrList) {
		definition.add(CarIQExporterDefinition.QUERY_DEF, new CarIQQueryDefinition(name, description, attrList));
	}

	protected void addExportAttribute(String exportAttributeName) {
		definition.add(CarIQExporterDefinition.EXPORT_PARAM, exportAttributeName);
	}

	protected void addExportAttributes(List<String> exportAttributeNames) {
		for (String name : exportAttributeNames) {
			addExportAttribute(name);
		}
	}

	protected void addSupportedRole(String supportedRole) {
		definition.add(CarIQExporterDefinition.ROLE, supportedRole);
	}

	protected void addSupportedRoles(List<String> supportedRoles) {
		for (String name : supportedRoles) {
			addSupportedRole(name);
		}
	}

	@Override
	public boolean isAuthorised() {
		return getExporterDefinition().isAuthorized();
	}

	@Override
	public List<GenericJSON> export(String queryType, GenericJSON json, int pageNo, int pageSize) {
		try (ProfilePoint _exporter_export = ProfilePoint
				.profileActivity("ProfActivity_exporter_export_" + this.getClass().getName())) {
			json = Utils.trimStringValues(json);
			List<GenericJSON> list = doExport(queryType, json, pageNo, pageSize);
			return getEnhancer().enhance(queryType, json, list);
		}
	}

	protected ExporterDataEnhancer getEnhancer() {
		return new UnityExportEnhancer();
	}

	@Override
	public HtmlValueRenderer getHtmlRenderer() {
		return new HtmlValueIdentityRenderer();
	}

	protected abstract List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize);

	@Override
	public List<CarIQExporter> getChildExporters() {
		return Arrays.<CarIQExporter>asList(this);
	}

	protected void setQueryDefinitions(List<CarIQQueryDefinition> queryDefs) {
		for (CarIQQueryDefinition carIQQueryDefinition : queryDefs) {
			definition.add(CarIQExporterDefinition.QUERY_DEF, carIQQueryDefinition);
		}
	}

	@Override
	public CountPOJO getExportCount(String queryType, GenericJSON json) {
		return new CountPOJO(-1);
	}

	@Override
	public List<GenericJSON> exportRecords(String queryType, GenericJSON json, int startIndex, int endIndex) {
		try (ProfilePoint _exporter_export = ProfilePoint
				.profileActivity("ProfActivity_exporter_export_" + this.getClass().getName())) {
			setApiContext("ExportRecords", "Exporter", Utils.UTC);

			json = Utils.trimStringValues(json);
			return getEnhancer().enhance(queryType, json, doExportRecords(queryType, json, startIndex, endIndex));
		}
	}

	protected List<GenericJSON> doExportRecords(String queryType, GenericJSON json, int startIndex, int endIndex) {
		int pageSize = endIndex - startIndex;
		int pageNo = startIndex / pageSize;

		return doExport(queryType, json, pageNo, pageSize);
	}

	private void setApiContext(String operation, String userName, String timeZone) {
		APIContext context = new APIContext();
		context.setValue(APIContext.USER_NAME, userName);
		context.setValue(APIContext.OPERATION, operation);
		context.setValue(APIContext.TIMEZONE, timeZone);
		APIContext.setContext(context);
	}
}
