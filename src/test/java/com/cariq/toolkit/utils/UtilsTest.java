package com.cariq.toolkit.utils;

import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.utils.OptionPicker.Option;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Transactional
@Configurable
public class UtilsTest {
	
	//@Test
	public void testMaskEmailAndPhone() {
		System.out.println("Masking email: hrishikesh.nene@gmail.com : " + Utils.maskEmail("hrishikesh.nene@gmail.com"));
		System.out.println("Masking email: abc@gmail.com : " + Utils.maskEmail("abc@gmail.com"));
		System.out.println("Masking email: a@gmail.com : " + Utils.maskEmail("a@gmail.com"));
		System.out.println("Masking email: hrishi@mycariq.com : " + Utils.maskEmail("hrishi@mycariq.com"));
		System.out.println("Masking phone: 9822619868 : " + Utils.maskPhoneNo("9822619868"));
		System.out.println("Masking phone: +919850754680 : " + Utils.maskPhoneNo("+919850754680"));
	}
	
	@Test
	public void weekTest() {
		//Date now = Utils.getDate("2020-06-18");
		Date now = new Date();
		System.out.println("Date: " + now);
		
		Date previousDate = Utils.getDate("2020-06-24");
		System.out.println("previous Date: " + previousDate);

		Date yesterdayDate = Utils.getDaysBefore(now, 1);
		System.out.println("yesterday Date: " + yesterdayDate);
		
		if (DateUtils.isSameDay(previousDate, yesterdayDate))
			System.out.println("Yes");
		else
			System.out.println("no");

		Week previousWeek = Week.getLastWeek(now, null);
		System.out.println("Week Start Date: " + previousWeek.getStartTime());
		System.out.println("Week Start Date: " + previousWeek.getEndTime());
		System.out.println("isInWeek: " + previousWeek.isInTheWeek(now));
	}
	
	
	@Test
	public void OptionsPickerTest() {
		OptionPicker Categories = new OptionPicker(new Option("Hardware-1", "Hardware 1", "Hardware1"),
				new Option("Hardware-2", "Hardware 2", "Hardware2"), 
				new Option("Software", "Software 0", "Software0"),
				new Option("Hardware-Full", "Hardware Full", "HardwareFull", "Hardware"),
				new Option("Software-1", "Software 1", "Software1"), 
				new Option("Software-2", "Software 2", "Software2"),
				new Option("Software-3", "Software 3", "Software3"), 
				new Option("Software-4", "Software 4", "Software4"));

		System.out.println("Match for Hardware1 : " + Categories.match("Hardware1"));
		System.out.println("Match for Hardware2 : " + Categories.match("Hardware2"));
		System.out.println("Match for Hardware : " + Categories.match("Hardware"));
		System.out.println("Match for Software 4 : " + Categories.match("Software 4"));
		System.out.println("Match for Hardware 3 : " + Categories.match("Hardware 3"));
	}
}
