package com.cariq.toolkit.coreiq.exporter;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.exporter.ExportedData.ExporterDataJSON_In;
import com.cariq.toolkit.coreiq.exporter.ExportedData.ExporterDataJSON_Out;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class ExportedData implements JSONable<ExporterDataJSON_In, ExporterDataJSON_Out, IdJSON> {

	public static final String ALL = "All";
	public static final String USER = "User";

	public static final String IN_PROGRESS = "In Progress";
	public static final String COMPLETE = "Complete";
	public static final String FAILED = "Failed";
	public static final String USERNAME = "username";
	

	/**
	 */
	private String exporterName;

	/**
	 */
	private String queryType;

	/**
	 */
	@Column(length = 4000)
	private String inputJSON;

	/**
	 */
	private String fileName;

	/**
	 */
	@Column(length = 1000)
	private String fileUrl;

	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date exportedOn;

	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date createdOn;

	// file format CSV or HTML
	private String fileFormat;

	private String reportId; // Client generated Identifier of the report

	@ManyToOne
	private User user;

	private String status;
	
	/** Domain of proxy  **/
	@NotNull(message = "Domain cannot be null")
	@ManyToOne
	private Domain domain;

	public String getExporterName() {
		return exporterName;
	}

	public String getQueryType() {
		return queryType;
	}

	public String getInputJSON() {
		return inputJSON;
	}

	public String getFileName() {
		return fileName;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public Date getExportedOn() {
		return exportedOn;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public String getFileFormat() {
		return fileFormat;
	}

	public String getReportId() {
		return reportId;
	}

	public User getUser() {
		return user;
	}

	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status){
		 this.status= status; 
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public ExportedData() {
		super();
	}

	public ExportedData(String exporterName, String queryType, String inputJSON, String fileName, String fileUrl,
			Date exportedOn, String fileFormat, String reportId, User user, String status) {
		super();
		this.exporterName = exporterName;
		this.queryType = queryType;
		this.inputJSON = inputJSON;
		this.fileName = fileName;
		this.fileUrl = fileUrl;
		this.exportedOn = exportedOn;
		this.createdOn = new Date();
		this.fileFormat = fileFormat;
		this.reportId = reportId;
		this.user = user;
		this.status = status;
	}

	public ExportedData(String exporterName, String queryType, String inputJSON, String fileName, String fileUrl,
			Date exportedOn, String fileFormat, String reportId, User user, String status,
			Domain domain) {
		super();
		this.exporterName = exporterName;
		this.queryType = queryType;
		this.inputJSON = inputJSON;
		this.fileName = fileName;
		this.fileUrl = fileUrl;
		this.exportedOn = exportedOn;
		this.createdOn = new Date();
		this.fileFormat = fileFormat;
		this.reportId = reportId;
		this.user = user;
		this.status = status;
		this.domain = domain;
	}

	public ExportedData(ExporterRequest exporterRequest) {
		this(exporterRequest.getExporterName(), exporterRequest.getQueryType(),
				exporterRequest.getInputJSON().toString(), exporterRequest.getOutputFileName(), null,
				exporterRequest.getCreatedOn(), exporterRequest.getFileFormat(), exporterRequest.getReportId(),
				(exporterRequest.getUserId() == null) ? null : User.findObjectById(exporterRequest.getUserId()), IN_PROGRESS,
				Domain.getByName(Utils.getValue(exporterRequest.getInputJSON(), "domain")));
	}

	@Override
	public ExporterDataJSON_Out toJSON() {
		return new ExporterDataJSON_Out(exporterName, queryType, fileName, fileFormat, inputJSON, exportedOn,
				user == null ? null : user.getId(), reportId, fileUrl, getId(), createdOn, status, domain == null ? null : domain.getId());
	}

	@Override
	public void fromJSON(ExporterDataJSON_In json) {
		this.exporterName = json.exporterName;
		this.queryType = json.queryType;
		this.inputJSON = json.inputJSON;
		this.fileName = json.outputFileName;
		this.fileFormat = json.fileFormat;
		this.fileUrl = json.fileUrl;
		this.exportedOn = json.exportedOn;
		this.user = User.findObjectById(json.getUserId());
		this.reportId = json.reportId;
		this.createdOn = new Date();
		this.status = json.status;
		this.domain = Domain.findObjectById(json.getDomainId());
	}

	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}

	public static class ExporterDataJSON_Out extends ExporterDataJSON_In {
		Long id;
		Date createdOn;
		String exporterDescription;
		

		/**
		 * @param exporterName
		 * @param queryType
		 * @param outputFileName
		 * @param inputJSON
		 * @param exportedOn
		 * @param userId
		 * @param reportId
		 * @param id
		 * @param createdOn
		 */
		public ExporterDataJSON_Out(String exporterName, String queryType, String outputFileName, String fileFormat,
				String inputJSON, Date exportedOn, Long userId, String reportId, String fileUrl, Long id,
				Date createdOn, String status) {
			super(exporterName, queryType, outputFileName, fileFormat, inputJSON, exportedOn, userId, reportId,
					fileUrl, status);
			this.id = id;
			this.createdOn = createdOn;
		}
		
		public ExporterDataJSON_Out(String exporterName, String queryType, String outputFileName, String fileFormat,
				String inputJSON, Date exportedOn, Long userId, String reportId, String fileUrl, Long id,
				Date createdOn, String status, Long domainId) {
			super(exporterName, queryType, outputFileName, fileFormat, inputJSON, exportedOn, userId, reportId,
					fileUrl, status, domainId);
			this.id = id;
			this.createdOn = createdOn;
			
			// get exporter from registry 
			CarIQExporter exporter = CarIQExporterRegistry.getExporter(exporterName);
			this.exporterDescription = (null == exporter)? null : exporter.getDescription();
		}
		
		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public Date getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(Date createdOn) {
			this.createdOn = createdOn;
		}

		public String getExporterDescription() {
			return exporterDescription;
		}

		public void setExporterDescription(String exporterDescription) {
			this.exporterDescription = exporterDescription;
		}
	}

	public static class ExporterDataJSON_In {
		String exporterName, queryType, outputFileName, fileFormat;
		String inputJSON;
		Date exportedOn;
		Long userId;
		String reportId;
		String fileUrl;
		String status;
		Long domainId;

		/**
		 * 
		 */
		public ExporterDataJSON_In() {
			super();
		}

		/**
		 * @param exporterName
		 * @param queryType
		 * @param outputFileName
		 * @param inputJSON
		 * @param exportedOn
		 * @param userId
		 * @param reportId
		 */
		public ExporterDataJSON_In(String exporterName, String queryType, String outputFileName, String fileFormat,
				String inputJSON, Date exportedOn, Long userId, String reportId, String fileUrl, String status) {
			this.exporterName = exporterName;
			this.queryType = queryType;
			this.outputFileName = outputFileName;
			this.fileFormat = fileFormat;
			this.inputJSON = inputJSON;
			this.exportedOn = exportedOn;
			this.userId = userId;
			this.reportId = reportId;
			this.fileUrl = fileUrl;
			this.status = status;
		}

		public ExporterDataJSON_In(String exporterName, String queryType, String outputFileName, String fileFormat,
				String inputJSON, Date exportedOn, Long userId, String reportId, String fileUrl, String status,
				Long domainId) {
			super();
			this.exporterName = exporterName;
			this.queryType = queryType;
			this.outputFileName = outputFileName;
			this.fileFormat = fileFormat;
			this.inputJSON = inputJSON;
			this.exportedOn = exportedOn;
			this.userId = userId;
			this.reportId = reportId;
			this.fileUrl = fileUrl;
			this.status = status;
			this.domainId = domainId;
		}

		public String getExporterName() {
			return exporterName;
		}

		public void setExporterName(String exporterName) {
			this.exporterName = exporterName;
		}

		public String getQueryType() {
			return queryType;
		}

		public void setQueryType(String queryType) {
			this.queryType = queryType;
		}

		public String getOutputFileName() {
			return outputFileName;
		}

		public void setOutputFileName(String outputFileName) {
			this.outputFileName = outputFileName;
		}

		public String getInputJSON() {
			return inputJSON;
		}

		public void setInputJSON(String inputJSON) {
			this.inputJSON = inputJSON;
		}

		public Date getExportedOn() {
			return exportedOn;
		}

		public void setExportedOn(Date exportedOn) {
			this.exportedOn = exportedOn;
		}

		public Long getUserId() {
			return userId;
		}

		public void setUserId(Long userId) {
			this.userId = userId;
		}

		public String getReportId() {
			return reportId;
		}

		public void setReportId(String reportId) {
			this.reportId = reportId;
		}

		public String getFileUrl() {
			return fileUrl;
		}

		public void setFileUrl(String fileUrl) {
			this.fileUrl = fileUrl;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getFileFormat() {
			return fileFormat;
		}

		public void setFileFormat(String fileFormat) {
			this.fileFormat = fileFormat;
		}

		public Long getDomainId() {
			return domainId;
		}

		public void setDomainId(Long domainId) {
			this.domainId = domainId;
		}
	}

	/**
	 * Get all reports of given user
	 * 
	 * @param user
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public static List<ExportedData> findExportedData(User user, int pageNo, int pageSize) {
		try (ProfilePoint _findExportedDataOfUser = ProfilePoint.profileAction("ProfAction_findExportedDataOfUser")) {
			CarIQSimpleQuery<ExportedData> qry = new CarIQSimpleQuery<ExportedData>("findReportByUser", entityManager(),
					ExportedData.class);
			if (user != null)
				qry.addCondition("user", "=", user);
			else
				qry.addRawCondition("user is null");
			qry.setPaging(pageNo, pageSize);
			qry.orderBy("exportedOn", "desc");
			return qry.getResultList();
		}
	}

	/**
	 * Get all reports
	 * 
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public static List<ExportedData> findExportedData(int pageNo, int pageSize) {
		try (ProfilePoint _findExportedData = ProfilePoint.profileAction("ProfAction_findExportedData")) {
			CarIQSimpleQuery<ExportedData> qry = new CarIQSimpleQuery<ExportedData>("findReportByUser", entityManager(),
					ExportedData.class);
			qry.setPaging(pageNo, pageSize);
			qry.orderBy("exportedOn", "desc");
			return qry.getResultList();
		}
	}

	/**
	 * Get report based on reportId
	 * 
	 * @param reportId
	 * @return
	 */
	public static ExportedData findByReportId(String reportId) {
		try (ProfilePoint _findByReportId = ProfilePoint.profileAction("ProfAction_findByReportId")) {
			CarIQSimpleQuery<ExportedData> qry = new CarIQSimpleQuery<ExportedData>("findReportByUser", entityManager(),
					ExportedData.class);
			qry.addCondition("reportId", "=", reportId);
			if (qry.getResultList().isEmpty())
				return null;
			return qry.getSingleResult();
		}
	}

	/**
	 * Find uploaded file exported data.
	 *
	 * @param domain
	 *            the domain
	 * @param queryType
	 *            the query type
	 * @param startDate
	 *            the start date
	 * @param endDate
	 *            the end date
	 * @param pageNo
	 *            the page no
	 * @param pageSize
	 *            the page size
	 * @return the list
	 */
	public static List<ExportedData> findUploadedFileExportedData(Domain domain, String queryType, Date startDate,
			Date endDate, int pageNo, int pageSize) {
		try (ProfilePoint _findExportedData = ProfilePoint.profileAction("ProfAction_findUploadedFileExportedData")) {
			CarIQSimpleQuery<ExportedData> query = new CarIQSimpleQuery<ExportedData>("findUploadedFileExportedData",
					entityManager(), ExportedData.class);

			query.addCondition("domain", CarIQSimpleQuery.EQ, domain);
			query.addCondition("queryType", CarIQSimpleQuery.EQ, queryType);
			query.addCondition("createdOn", CarIQSimpleQuery.GT_EQ, startDate);
			query.addCondition("createdOn", CarIQSimpleQuery.LT_EQ, endDate);
			query.setPaging(pageNo, pageSize);
			query.orderBy("createdOn", CarIQSimpleQuery.DESC);

			return query.getResultList();
		}
	}

	/**
	 * Find uploaded files by key word.
	 *
	 * @param domain the domain
	 * @param queryType the query type
	 * @param keyWord the key word
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @return the list
	 */
	public static List<ExportedData> findUploadedFilesByKeyWord(Domain domain, String queryType, String keyWord,
			int pageNo, int pageSize) {
		try (ProfilePoint _findExportedData = ProfilePoint.profileAction("ProfAction_findUploadedFilesByKeyWord")) {
			CarIQSimpleQuery<ExportedData> query = new CarIQSimpleQuery<ExportedData>("findUploadedFilesByKeyWord",
					entityManager(), ExportedData.class);

			query.addCondition("domain", CarIQSimpleQuery.EQ, domain);
			query.addCondition("queryType", CarIQSimpleQuery.EQ, queryType);
			query.addRawCondition("(file_name like '%" + keyWord + "%' or inputjson like '%" + keyWord + "%')");
			query.setPaging(pageNo, pageSize);

			return query.getResultList();
		}
	}
	
	/**
	 * Find exported data by user and domain.
	 *
	 * @param user the user
	 * @param domain the domain
	 * @param startDate the start date
	 * @param endDate the end date
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @return the list
	 */
	public static List<ExportedData> findExportedDataByUserAndDomain(User user, Domain domain,Date startDate,
			Date endDate,int pageNo, int pageSize) {
		try (ProfilePoint _findExportedDataOfUser = ProfilePoint.profileAction("ProfAction_findExportedDataByUserAndDomain")) {
			CarIQSimpleQuery<ExportedData> query = new CarIQSimpleQuery<ExportedData>("findExportedDataByUserAndDomain", entityManager(),
					ExportedData.class);
			
			query.addCondition("user", CarIQSimpleQuery.EQ, user);
			query.addCondition("domain", CarIQSimpleQuery.EQ, domain);
			query.addCondition("exportedOn", CarIQSimpleQuery.GT_EQ, startDate);
			query.addCondition("exportedOn", CarIQSimpleQuery.LT_EQ, endDate);
			query.setPaging(pageNo, pageSize);
			query.orderBy("exportedOn", CarIQSimpleQuery.DESC);
			
			return query.getResultList();
		}
	}
	
	public static ExportedData findExportedDataLatest(Domain domain, String exporter, String queryType,
			String inputDetails, String format) {
		try (ProfilePoint _findExportedDataOfUser = ProfilePoint.profileAction("ProfAction_findExportedDataLatest")) {
			CarIQSimpleQuery<ExportedData> query = new CarIQSimpleQuery<ExportedData>("findExportedDataLatest", entityManager(),
					ExportedData.class);
			
			if (null != domain) query.addCondition("domain", CarIQSimpleQuery.EQ, domain);
			if (null != exporter) query.addCondition("exporterName", CarIQSimpleQuery.EQ, exporter);
			if (null != queryType)query.addCondition("queryType", CarIQSimpleQuery.EQ, queryType);
			if (null != format)query.addCondition("fileFormat", CarIQSimpleQuery.EQ, format);
			if (null != inputDetails)query.addRawCondition("inputjson like '%" + inputDetails + "%'");
			
			query.addCondition("status", CarIQSimpleQuery.EQ, COMPLETE);
			query.orderBy("exportedOn", CarIQSimpleQuery.DESC);
			query.setPaging(1, 1);
			
			return query.getSingleResult();
		}
	}
	
	public static ExportedData getLatestExportedData(Domain domain, User user, String exporter, String fileFormat) {
		try (ProfilePoint _getLatestExportedData = ProfilePoint.profileAction("ExportedData_getLatestExportedData")) {
			CarIQSimpleQuery<ExportedData> query = new CarIQSimpleQuery<ExportedData>("findExportedDataLatest",
					ExportedData.entityManager(), ExportedData.class);

			query.addCondition("domain", CarIQSimpleQuery.EQ, domain);
			query.addCondition("exporterName", CarIQSimpleQuery.EQ, exporter);
			query.addCondition("fileFormat", CarIQSimpleQuery.EQ, fileFormat);
			query.addCondition("status", CarIQSimpleQuery.EQ, COMPLETE);

			query.orderBy("id", CarIQSimpleQuery.DESC);
			query.setPaging(1, 1);

			return query.getSingleResult();
		}
	}
	
	public static ExportedData getLatestExportedData(String exporter, String format) {
		try (ProfilePoint _getLatestExportedData = ProfilePoint.profileAction("ExportedData_getLatestExportedData")) {
			CarIQSimpleQuery<ExportedData> query = new CarIQSimpleQuery<ExportedData>("getLatestExportedData",
					entityManager(), ExportedData.class);
			query.addCondition("exporterName", CarIQSimpleQuery.EQ, exporter);
			query.addCondition("fileFormat", CarIQSimpleQuery.EQ, format);
			query.addCondition("status", CarIQSimpleQuery.EQ, COMPLETE);
			query.orderBy("exportedOn", CarIQSimpleQuery.DESC);
			query.setPaging(1, 1);

			return query.getSingleResult();
		}
	}
}