package com.cariq.toolkit.coreiq.server;

import java.util.Date;

import com.cariq.toolkit.coreiq.CompletionCallback;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.GenericJsonable;
import com.cariq.toolkit.utils.Utils;



public class ServerActivity implements AutoCloseable, GenericJsonable {
	String category;
	String subCategory;
	String name;
	String user;

	Date invokedOn;
//	Map<T, T> serverActivityMap;
	CompletionCallback<ServerActivity> completionCallback;
	
	
	protected ServerActivity(String category, String subCategory, String name, String user, CompletionCallback<ServerActivity> completionCallback) {
		super();
		this.category = category;
		this.subCategory = subCategory;
		this.name = name;
		this.user = user;
		this.invokedOn = new Date();
		this.completionCallback = completionCallback;
	}
	
	protected ServerActivity(String category, String subCategory, String name, CompletionCallback<ServerActivity> completionCallback) {
		this(category, subCategory, name, null, completionCallback);
	}


	public String getCategory() {
		return category;
	}

	public String getName() {
		return name;
	}

	public String getUser() {
		return user;
	}

	public Date getInvokedOn() {
		return invokedOn;
	}
	
	public String getSubCategory() {
		return subCategory;
	}

	@Override
	public void close() throws Exception {
		completionCallback.onComplete(this);
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((invokedOn == null) ? 0 : invokedOn.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServerActivity other = (ServerActivity) obj;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (invokedOn == null) {
			if (other.invokedOn != null)
				return false;
		} else if (!invokedOn.equals(other.invokedOn))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public GenericJSON toGenericJSON() {
		GenericJSON json = new GenericJSON();
		json.add("category", category);
		json.add("name", name);
		
		if (user != null) 
			json.add("user", user);
		
		json.add("elapsedTimeMS", calculateElapsedTime());

		return json;
	}

	@Override
	public void fromGenricJSON(GenericJSON json) {
		Utils.notImplementedException("ServerActivity.fromGenericJSON");
	}

	public long calculateElapsedTime() {
		return Utils.getDiffInMilliSeconds(new Date(), invokedOn);
	}

	@Override
	public String toString() {
		return "ServerActivity [category=" + category + ", subCategory=" + subCategory + ", name=" + name + ", user="
				+ user + ", invokedOn=" + invokedOn + "]";
	}

}

