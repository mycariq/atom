package com.cariq.toolkit.utils.analytics.application;

import java.util.Date;

import com.cariq.toolkit.model.BillingOperation;
import com.cariq.toolkit.model.BillingOperation.BillingStatusEnum;
import com.cariq.toolkit.model.Invoice;
import com.cariq.toolkit.model.Invoice.InvoiceStatusEnum;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.ObjectProcessor;
import com.cariq.toolkit.utils.ObjectProcessorTemplate;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

public class BillingOperationProcessor extends ObjectProcessorTemplate<BillingOperation, GenericJSON> {
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("BillingOperationProcessor");
	
	public BillingOperationProcessor(BillingOperation billingOperation) {
		super(billingOperation);
	}
	
	@Override
	public void doStartPrepare() {	
		try (ProfilePoint _doStartPrepare = ProfilePoint.profileAction("ProfAction__doStartPrepare_" + this.getClass().getName())) {
			logger.debug("*** BillingOperationProcessor: doStartPrepare()....");

			// Create invoice
			BillingOperation billingOp = getModel();
			Invoice invoice = new Invoice(billingOp.getRaisedbyAccount(), billingOp.getAgainstAccount(), billingOp.getItsContract(),
					billingOp, billingOp.getForBillingTerms(), billingOp.getBillingStartMonth(), billingOp.getBillingEndMonth(),
					null, InvoiceStatusEnum.IN_PROGRESS.toString(), null, new Date(), new Date());
			invoice.persist();
			
			// Create Child Processors, only one invoice required.
			InvoiceProcessor invoiceProcessor = new InvoiceProcessor(invoice);
			addChildProcessor(invoiceProcessor);
		}
	}
	
	@Override
	public void doEndFinish() {
		try (ProfilePoint _doStartPrepare = ProfilePoint.profileAction("ProfAction__doEndFinish_" + this.getClass().getName())) {
			logger.debug("*** BillingOperationProcessor: doEndFinish()....");

			// Check its invoice status
			BillingOperation billingOp = getModel();
			Invoice invoice = Invoice.getByItsOperation(billingOp);
			if (null == invoice)
				logger.debug("***!!! NO VALID INVOICE ***");
			
			if (null != invoice && invoice.getStatus().equals(InvoiceStatusEnum.READY.toString())) {
				logger.debug("*** Invoice is READY to review: " + invoice.toString());
				// Mark billing operation as successful
				billingOp.updateResult(BillingStatusEnum.SUCCESSFUL.toString());
				billingOp.persist();
			} else {				
				// Mark billing operation as invalid
				billingOp.updateResult(BillingStatusEnum.INVALID.toString());
				billingOp.persist();
			}
		}
	}
	
	@Override
	public void doProcessChildFinishException(ObjectProcessor invoiceProcessor, RuntimeException e) {
		try (ProfilePoint _doProcessChildFinishException = ProfilePoint.profileAction("ProfAction__doProcessChildFinishException_" + this.getClass().getName())) {
			logger.debug("*** BillingOperationProcessor: doProcessChildFinishException()...");
			logger.debug("*** Caught exception: " + e.getMessage());
			invoiceProcessor.destroyModel();
		}
	}
}
