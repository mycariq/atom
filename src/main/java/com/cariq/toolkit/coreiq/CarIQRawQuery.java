package com.cariq.toolkit.coreiq;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * Simple Query mechanism using Entity Manager by specifying Where clause and
 * order
 * 
 * @author Abhijit
 *
 * @param
 */
public class CarIQRawQuery {
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("CarIQRawQuery");
	String name = "RawQuery";

	enum OuterJoinCondition {
		None,
		LetfOuterJoin,
		RightOuterJoin
	};
	
	static class Parameter {
		String key;
		Object value;

		public Parameter(String key, Object value) {
			super();
			this.key = key;
			this.value = value;
		}
	}

	String condition = "";
	String orderBy = "";
	String groupBy = "";

	LinkedHashMap<String, String> selectParameters;
	List<Parameter> queryParameters = new ArrayList<Parameter>();
	LinkedHashMap<String, String> tablesWithAliases = null;
	List<String> tablesWithAliasesRaw = null;
	EntityManager entityManager;
	int paramId;
	int rowNum = -1, pageSize = -1;
	
	// union support
	CarIQRawQuery unionQuery;

	public CarIQRawQuery(String name, EntityManager entityManager, LinkedHashMap<String, String> attributealiasmap, List<String> tablealiasarray) {
		this.name = name;
		this.entityManager = entityManager;
		this.selectParameters = attributealiasmap;
		this.tablesWithAliasesRaw = tablealiasarray;
	}	
	
	public CarIQRawQuery(String name, EntityManager entityManager, LinkedHashMap<String, String> attributealiasmap, LinkedHashMap<String, String> tablealiasMap) {
		this.name = name;
		this.entityManager = entityManager;
		this.selectParameters = attributealiasmap;
		this.tablesWithAliases = tablealiasMap;
	}

	public CarIQRawQuery addCondition(String parameter, String booleanOperation, Object value) {
		if (condition.isEmpty())
			condition = " where ";
		else
			condition = condition + " and ";

		String paramName = parameter + paramId++;
		condition = condition + parameter + " " + booleanOperation + " :" + paramName;

		queryParameters.add(new Parameter(paramName, value));
		// logger.debug("Query Condition: " + parameter + " " + booleanOperation
		// + " " + value.toString());
		return this;
	}
	
	public CarIQRawQuery addJoinCondition(String parameter1, String booleanOperation, String parameter2) {
		if (condition.isEmpty())
			condition = " where ";
		else
			condition = condition + " and ";

		condition = condition + parameter1 + " " + booleanOperation + " " + parameter2;

		return this;
	}

	public CarIQRawQuery addNotNullCondition(String parameter) {
		if (condition.isEmpty())
			condition = " where ";
		else
			condition = condition + " and ";

		condition = condition + parameter + " is not null ";

		return this;
	}

	public void orderBy(String parameter, String order) {
		if (orderBy.isEmpty())
			orderBy = parameter + " " + order;
		else
			orderBy += ", " + parameter + " " + order;
	}

	public void setPaging(int pageNo, int pageSize) {
		this.rowNum = Utils.validatePagination(pageSize, pageNo);
		this.pageSize = pageSize;
	}

	private Query getQuery() {
		Query query = (Query) entityManager.createNativeQuery(constructQueryString());

		for (Parameter parameter : queryParameters) {
			query.setParameter(parameter.key, parameter.value);
		}

		if (rowNum >= 0)
			query.setFirstResult(rowNum);
		if (pageSize >= 1)
			query.setMaxResults(pageSize);

		return query;
	}

	// Select the Object - override in the Aggregation query
	protected String getSelectClause() {
		StringBuilder builder = new StringBuilder();
		boolean first = true;
		for (String selectParam : selectParameters.keySet()) {
			if (first)
				first = false;
			else
				builder.append(", ");

			builder.append(selectParam + " " + selectParameters.get(selectParam));
		}
		return builder.toString();
	}

	// Select the Object - override in the Aggregation query
	protected String getFromClause() {
		StringBuilder builder = new StringBuilder();

		// if raw from clause is given, use it
		if (tablesWithAliasesRaw != null) {
			for (String tableWithAliasRaw : tablesWithAliasesRaw) {
					builder.append(" ").append(tableWithAliasRaw);
			}
			return builder.toString();
		}
		
		// Construct from from the Table Alias map
		boolean first = true;
		for (String tableWithAlias : tablesWithAliases.keySet()) {
			if (first)
				first = false;
			else
				builder.append(", ");

			builder.append(tableWithAlias + " " + tablesWithAliases.get(tableWithAlias));
		}
		return builder.toString();
	}

	private String constructQueryString() {
		// build select clause polymorphically
		StringBuilder builder = new StringBuilder(
				"select " + getSelectClause() + " from " + getFromClause());

		if (!condition.isEmpty())
			builder.append(condition);
		
		if (!groupBy.isEmpty())
			builder.append(" group by ").append(groupBy);
		
		if (!orderBy.isEmpty())
			builder.append(" order by ").append(orderBy);

		// append union query if one exists
		if (unionQuery != null)
			builder.append(" union ").append(unionQuery.constructQueryString());
			

		String retval = builder.toString();
		logger.debug("[CarIQRawQuery - " + name + "] " + retval);
		return retval;
	}

	public CarIQRawQuery addRawCondition(String parameter, String rawCondition) {
		if (condition.isEmpty())
			condition = " where ";
		else
			condition = condition + " and ";

		condition = condition + "obj" + "." + parameter + " " + rawCondition;

		return this;
	}

	public CarIQRawQuery addRawCondition(String rawCondition) {
		if (condition.isEmpty())
			condition = " where ";
		else
			condition = condition + " and ";

		condition = condition + rawCondition;

		return this;
	}
	
	public CarIQRawQuery addUnionQuery(CarIQRawQuery other) {
		if (unionQuery != null)
			Utils.handleException("Union Query already exists!!");
			
			
		unionQuery = other;
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return constructQueryString();
		// StringBuilder bldr = new StringBuilder(constructQueryString());
		// bldr.append("; parameters: ");
		// for (Parameter parameter : parameters) {
		// bldr.append(parameter.key).append(" = ").append(parameter.value);
		// }
		//
		// return bldr.toString();
	}

	@SuppressWarnings("unchecked")
	public GenericJSON getSingleResult() {
		try (ProfilePoint _CarIQRawQuery_getSingleResult = ProfilePoint
				.profileAction("ProfAction_CarIQRawQuery_getSingleResult-" + name)) {
			Query q = getQuery();
			List<Object[]> resultList = q.getResultList();
			if (resultList.isEmpty())
				return null;
			Object[] res = (Object[]) q.getSingleResult();
			List<String> columns = new ArrayList<>(selectParameters.values());
			GenericJSON json = new GenericJSON();
			for (int i = 0; i < res.length; i++) {
				json.put(columns.get(i), res[i]);
			}
			return json;
		}

	}

	@SuppressWarnings("unchecked")
	public List<GenericJSON> getResult() {
		try (ProfilePoint _CarIQRawQuery_getResult = ProfilePoint.profileAction("ProfAction_CarIQRawQuery_getResult-" + name)) {
			Query q = getQuery();
			List<Object[]> resultList = q.getResultList();
			if (resultList == null || resultList.isEmpty())
				return null;
			List<String> columns = new ArrayList<>(selectParameters.values());
			List<GenericJSON> retVal = new ArrayList<GenericJSON>();
			for (Object[] result : resultList) {
				GenericJSON json = new GenericJSON();
				for (int i = 0; i < result.length; i++) {
					json.put(columns.get(i), result[i]);
				}

				retVal.add(json);
			}
			return retVal;
		}
	}

	public void addGroupBy(String parameter) {
		if (groupBy.isEmpty())
			groupBy = parameter;
		else
			groupBy += ", " + parameter;		
	}
}
