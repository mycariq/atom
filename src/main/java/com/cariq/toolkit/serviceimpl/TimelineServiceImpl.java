package com.cariq.toolkit.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cariq.toolkit.coreiq.model.ActionLog;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.ProxyObject;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_In;
import com.cariq.toolkit.model.Timeline;
import com.cariq.toolkit.model.Timeline.Timeline_In;
import com.cariq.toolkit.model.Timeline.Timeline_Out;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.service.TimelineService;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

@Service
public class TimelineServiceImpl implements TimelineService {

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("TimelineServiceImpl");
	
	@Override
	public ResponseWithIdJson addTimelineItem(Timeline_In timelineIn) {
		logger.debug("addTimelineItem called");
		
		Timeline timeline = null;
		try (ProfilePoint _addTimelineItem = ProfilePoint
				.profileActivity("ProfActivity_addTimelineItem" + this.getClass().getName())) {
			try {

				timeline = new Timeline();
				timeline.fromJSON(timelineIn);
				
				User owner = null;
				Long domainId = timelineIn.getProxyObject().getDomainId();
				String signature = timelineIn.getProxyObject().getSignature();
				Long objectId = timelineIn.getProxyObject().getObjectId();
				String objectType = timelineIn.getProxyObject().getObjectType();
				
				//If this is first timeline to create then first assign owner
				//to proxy and then create timeline
				List<Timeline_Out> timelines = getTimeline(signature, domainId, objectType);
				if(timelines != null && timelines.isEmpty()) {
					logger.debug("This is first timeline, thus, assigning owner to proxy");
					owner = User.getByUsername(timelineIn.getUsername());
				}
				
				
				ProxyObject dbProxy = ProxyObject.getByDomainSignatureObjectType(Domain.findObjectById(domainId), signature, objectType);
				//If already proxy created but owner is not assigned then, user who creates next timeline
				//becomes owner of this proxy
				if(dbProxy != null && dbProxy.getOwner() == null) {
					logger.debug("Proxy already created but owner not assigned, "
							+ "Assigning owner to proxy with signature: " + dbProxy.getSignature());
					owner = User.getByUsername(timelineIn.getUsername());
				}
				
				//Create or get proxy object.
				ProxyObject proxy = ProxyObject.createOrGet(Domain.findObjectById(domainId),
						objectId, objectType, signature, owner, timelineIn.getTag());
				
				//Update proxy object.
				ProxyObject.update(Domain.findObjectById(domainId),
						objectId, objectType, signature, owner, timelineIn.getTag(), timelineIn.getNextAction());
				
				if(Timeline.findByProxyAndCreatedOn(proxy, timelineIn.gettCreatedOn()) == null) {
					// persist object
					timeline.persist();	
				} else {
					logger.error("Timeline already exists for proxy : " + proxy.getSignature());
					Utils.handleException("Timeline already exists for proxy : " + proxy.getSignature());
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				Utils.handleException("Error while creating timeline.", e);
			}
		}
		//logging status of operation
		logger.debug("Timeline created successfully with id : " + timeline.getId());
		ActionLog.log(Utils.createContext("TimelineServiceImpl.addTimelineItem"), "addTimelineItem",
				"Timeline", timeline.getId(), timeline);
		
		// Return response
		return new ResponseWithIdJson(timeline.getId().toString(), "Timeline created successfully.");
	}

	@Override
	public List<Timeline_Out> getTimeline(Long proxyObjectId) {
		throw new UnsupportedOperationException("getTimeline is not implemented yet!");
	}

	@Override
	public List<Timeline_Out> getTimeline(String policyNumber, Long domainId, String proxyType) {
		logger.debug("getTimeline called");

		//Return empty list in case database timeline list is null;
		List<Timeline_Out> timeline = new ArrayList<Timeline_Out>();
		
		try (ProfilePoint _getTimeline = ProfilePoint
				.profileActivity("ProfActivity_getTimeline" + this.getClass().getName())) {
			Domain domain = Domain.findObjectById(domainId);
			ProxyObject proxy = ProxyObject.getByDomainSignatureObjectType(domain, policyNumber, proxyType);
			
			//retrive timeline by using proxy id
			List<Timeline> timelineList = Timeline.getUserTimeline(proxy);
			if(timelineList == null)
				return timeline;
			
			//Add timeline json into json list
			for(Timeline timelineItem : timelineList) {
				timeline.add(timelineItem.toJSON());
			}
		}
		
		return timeline;
	}

	@Override
	public ResponseWithIdJson deleteTimeline(Long timelineId) {
		Timeline timeline = null;
		try (ProfilePoint deleteTimeline = ProfilePoint
				.profileActivity("ProfActivity_deleteTimeline" + this.getClass().getName())) {
			timeline = Timeline.findObjectById(timelineId);
			if(timeline == null) {
				String errorMsg = "Could not find timeline with id " + timelineId + ", and date ";
				Utils.handleException(errorMsg);
			}
			//Delete timeline
			timeline.remove();
			ActionLog.log(Utils.createContext("TimelineServiceImpl.deleteTimeline"), "deleteTimeline",
					"Timeline", timelineId, null);
		}
		return new ResponseWithIdJson(timeline.getId().toString(), "Timeline removed successfully.");
	}

	@Override
	public List<Timeline_Out> getTimelines(List<ProxyObject> proxyObjects) {
		
		try (ProfilePoint _getTimelines = ProfilePoint.profileActivity("ProfActivity_getTimelines")) {
			List<Timeline> timelines = Timeline.getUserTimelines(proxyObjects);
			if(timelines == null)
				return new ArrayList<Timeline_Out>();
			return Utils.convertList(timelines, Timeline_Out.class);
		}
	}

	@Override
	public ResponseWithIdJson addSimpleTimeline(ProxyObject_In proxyObject, String message, String type, String stage, String tag, String username) {
		logger.debug("addSimpleTimeline: message: " + message + ", type: " + type + ", stage: " + stage
				+ ", tag: " + tag + ", user: " + username);
		try (ProfilePoint _addSimpleTimeline = ProfilePoint.profileActivity("ProfActivity_addSimpleTimeline")) {
			Timeline_In in = new Timeline_In();
			in.setMessage(message);
			in.setType(type);
			in.setTag(tag);
			in.setStage(stage);
			in.setProxyObject(proxyObject);
			in.setUser(username);
			
			Timeline timeline = new Timeline();
			timeline.fromJSON(in);
			
			timeline.persist();
			timeline.flush();
			
			// Return response
			return new ResponseWithIdJson(timeline.getId().toString(), "Timeline created successfully.");
		}
	}
}
