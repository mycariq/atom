package com.cariq.toolkit.coreiq.tableUtil.application.mis;

import com.cariq.toolkit.utils.GenericJSON;

/**
 * Generic interface to process input JSON.
 * Context contains Configuration parameters stored for entire operation
 * @author hrishi
 *
 */
public interface MISProcessable {
	MISProcessable process(MISProcessingContext context, GenericJSON json);
}
