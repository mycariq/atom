package com.atom.www.web;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cariq.toolkit.CQVersion;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterService;
import com.cariq.toolkit.coreiq.server.AuditOnOffSwitchBoard;
import com.cariq.toolkit.coreiq.server.AuditOnOffSwitchRequest;
import com.cariq.toolkit.coreiq.server.DummyTaskRequest;
import com.cariq.toolkit.coreiq.server.DumpRequest;
import com.cariq.toolkit.coreiq.server.DumpResponse;
import com.cariq.toolkit.coreiq.server.Server;
import com.cariq.toolkit.coreiq.server.ServerAuditService;
import com.cariq.toolkit.coreiq.server.ServerFlushRequest;
import com.cariq.toolkit.coreiq.server.ServerLoadStatistics;
import com.cariq.toolkit.coreiq.server.ServerSpecificRequest;
import com.cariq.toolkit.coreiq.server.ServerSpecificResponseJSON;
import com.cariq.toolkit.coreiq.server.ServerStatisticsRequest;
import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.publicapi.CarIQPublicAPIParameter;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.utils.CarIQMutableAPI;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.JsonValidator;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.Utils;

/**
 * The Class TestController.
 */
@CarIQPublicAPI(name = "ServerAudit", description = "API related to Server Audit")
@RequestMapping("/server")
@Controller
public class ServerAuditController {

	/** The test service. */
	@Autowired
	private ServerAuditService auditService;

	@Autowired
	CarIQExporterService exporterService;
	
	/**
	 * Test Server Version
	 * 
	 * @param responseTime
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/version", method = RequestMethod.GET)
	@ResponseBody
	public Object getVersionInfo(HttpServletResponse response) {
		try (CarIQAPI getVersionInfo = new CarIQAPI("getVersionInfo")) {
			GenericJSON retval = new GenericJSON();
			retval.put("Domain", CQVersion.Domain);
			retval.put("BuildDate", CQVersion.buildDate);
			retval.put("DateCode", CQVersion.dateCode);
			retval.put("Configuration", CQVersion.getConfig());

			return retval;
		} catch (Exception e) {
			Utils.handleException(e);
		}

		return null;
	}
	
	/**
	 * Create dummy API Item which takes given amount of time
	 * 
	 * @param litmusRequest
	 * @param response
	 * @return
	 */
	@CarIQPublicAPIMethod(description = "Create Dummy API for specified time", responseClass = ServerSpecificResponseJSON.class)
	@RequestMapping(value = "/server/api/dummy", method = RequestMethod.POST)
	@ResponseBody
	public ServerSpecificResponseJSON dummyAPIRequest(@RequestBody @Valid DummyTaskRequest dummyTaskRequest, HttpServletResponse response,
			BindingResult result) {
		JsonValidator.validate(result);
		try (CarIQMutableAPI _dummyAPIRequest = new CarIQMutableAPI("dummyAPIRequest", dummyTaskRequest)) {
			Server.validateExecutionServer(dummyTaskRequest);
			return auditService.dummyAPIRequest(dummyTaskRequest.getExecutionTimeSec());
		}
	}

	/**
	 * Create dummy API Item which takes given amount of time
	 * 
	 * @param litmusRequest
	 * @param response
	 * @return
	 */
	@CarIQPublicAPIMethod(description = "Create Dummy Worker for specified time", responseClass = ResponseJson.class)
	@RequestMapping(value = "/server/worker/dummy", method = RequestMethod.POST)
	@ResponseBody
	public ResponseJson dummyWorkerRequest(@RequestBody @Valid DummyTaskRequest dummyTaskRequest, HttpServletResponse response,
			BindingResult result) {
		JsonValidator.validate(result);
		try (CarIQMutableAPI _startLitmusDeepCheck = new CarIQMutableAPI("dummyWorkerRequest", dummyTaskRequest)) {
			return auditService.dummyWorkerRequest(dummyTaskRequest);
		}
	}
	

	/**
	 * Create dummy API Item which takes given amount of time
	 * 
	 * @param litmusRequest
	 * @param response
	 * @return
	 */
	@CarIQPublicAPIMethod(description = "Create Dummy Worker for specified time", responseClass = ResponseJson.class)
	@RequestMapping(value = "/server/worker/chain/dummy", method = RequestMethod.POST)
	@ResponseBody
	public ResponseJson dummyWorkerChainRequest(@RequestBody @Valid DummyTaskRequest dummyTaskRequest, HttpServletResponse response,
			BindingResult result) {
		JsonValidator.validate(result);
		try (CarIQMutableAPI _dummyWorkerChainRequest = new CarIQMutableAPI("dummyWorkerChainRequest", dummyTaskRequest)) {
			return auditService.dummyWorkerChainRequest(dummyTaskRequest);
		}
	}
	
	/**
	 * Create dummy API Item which takes given amount of time
	 * 
	 * @param litmusRequest
	 * @param response
	 * @return
	 */
	@CarIQPublicAPIMethod(description = "Flush Server Statistics", responseClass = ServerSpecificResponseJSON.class)
	@RequestMapping(value = "/server/flush", method = RequestMethod.POST)
	@ResponseBody
	public ServerSpecificResponseJSON flushServerData(@RequestBody @Valid ServerFlushRequest serverFlushRequest, HttpServletResponse response,
			BindingResult result) {
		JsonValidator.validate(result);
		try (CarIQMutableAPI _flushServerData = new CarIQMutableAPI("flushServerData", serverFlushRequest)) {
			Server.validateExecutionServer(serverFlushRequest);
			return auditService.flushServerData(serverFlushRequest);
		}
	}
	
	
	/**
	 * Create dummy API Item which takes given amount of time
	 * 
	 * @param litmusRequest
	 * @param response
	 * @return
	 */
//	@CarIQPublicAPIMethod(description = "Get Server Load Statistics", responseClass = GenericJSON.class)
//	@RequestMapping(value = "/server/load", method = RequestMethod.GET)
//	@ResponseBody
//	public GenericJSON getServerLoadData(HttpServletResponse response) {
//		try (CarIQAPI _getServerLoadData = new CarIQAPI("getServerLoadData")) {
//			return auditService.getServerLoadData();
//		}
//	}

	@CarIQPublicAPIMethod(description = "Dump the Current load and Historical work done by a server", responseClass = ServerLoadStatistics.class)
	@RequestMapping(value = "/audit/statistics", method = RequestMethod.POST)
	@ResponseBody
	public ServerLoadStatistics getLoadStatistics(@RequestBody @Valid ServerStatisticsRequest auditStatisticsRequest, HttpServletResponse response,
			BindingResult result) {
		try (CarIQAPI sendSMSToDevice = new CarIQAPI("ServerAuditController_getAuditStatistics")) {
			Server.validateExecutionServer(auditStatisticsRequest);
			return auditService.getServerLoadStatistics();
		}
	}
	
	@CarIQPublicAPIMethod(description = "Switch ON or Off Particular Auditing Service", responseClass = ServerSpecificResponseJSON.class)
	@RequestMapping(value = "/audit/switchboard/onOff", method = RequestMethod.POST)
	@ResponseBody
	public ServerSpecificResponseJSON setAuditOnOff(@RequestBody @Valid AuditOnOffSwitchRequest switchOnOffRequest, HttpServletResponse response,
			BindingResult result) {
		try (CarIQAPI sendSMSToDevice = new CarIQAPI("ServerAuditController_setAuditOnOff")) {
			Server.validateExecutionServer(switchOnOffRequest);
			return auditService.auditOnOff(switchOnOffRequest.getOnOffSwitch());
		}
	}
	
	@CarIQPublicAPIMethod(description = "Get Current On-Off status of Audit Capturing", responseClass = AuditOnOffSwitchBoard.class)
	@RequestMapping(value = "/audit/switchboard/status/{serverName}", method = RequestMethod.GET)
	@ResponseBody
	public AuditOnOffSwitchBoard getAuditSwitchBoard(@CarIQPublicAPIParameter(name = "serverName", description = "Name of server or ANY") @PathVariable("serverName") final String serverName,
			HttpServletResponse response) {
		try (CarIQAPI sendSMSToDevice = new CarIQAPI("ServerAuditController_getAuditOnOff")) {
			ServerSpecificRequest request = new ServerSpecificRequest() {
				@Override
				public String getServerName() {
					return serverName;
				}
			};
			
			// Validate for server name
			Server.validateExecutionServer(request);

			return auditService.getAuditSwitchBoard(request);
		}
	}
	
	@CarIQPublicAPIMethod(description = "Get Threaddump of Heap Dump from given server", responseClass = GenericJSON.class)
	@RequestMapping(value = "/dump", method = RequestMethod.POST)
	@ResponseBody
	public DumpResponse getDump(@RequestBody @Valid DumpRequest dumpRequest, HttpServletResponse response,
			BindingResult result) {
		try (CarIQAPI sendSMSToDevice = new CarIQAPI("ServerAuditController_getDump")) {
			return auditService.getDump(dumpRequest);
		}
	}
}
