package com.cariq.toolkit.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

public class BeanDefinition {
	Class<?> beanClass;
	
	public BeanDefinition(Class<?> beanClass) {
		this.beanClass = beanClass;
	}

	boolean validate(BeanAttributeBag attributeBag) {
		// check the incoming bag against the definition
		return true;
		
	}
	
	Class<?> getBeanClass() {
		return beanClass;
	}
	
	/**
	 * Expose resource creation (class creation) logic to external world
	 * @return
	 */
	Map<String, Object> getAttributeMap() {
		Map<String, Object> attrMap = new HashMap<String, Object>();
	       Field[] fields = getBeanClass().getDeclaredFields();
	        System.out.printf("%d fields:%n", fields.length);
	        for (Field field : fields) {
	            System.out.printf("%s %s %s%n",
	                Modifier.toString(field.getModifiers()),
	                field.getType().getSimpleName(),
	                field.getName()
	            );
	            attrMap.put(field.getName(), field.getType().getSimpleName());
	        }
	        return attrMap;
	}
}
