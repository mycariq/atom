package com.atom.www.loader;

import java.util.ArrayList;
import java.util.List;

import com.atom.www.exporter.MonthlyMISReportExporter;
import com.atom.www.exporter.StagingAndPolicyDetailsReportExporter;
import com.atom.www.exporter.WeeklyMISReportExporter;
import com.cariq.toolkit.coreiq.exporter.DummyTestExporter.DummyTATCreator;
import com.cariq.toolkit.coreiq.exporter.DummyTestExporter.DummyTATProcessor;
import com.cariq.toolkit.coreiq.exporter.ExporterWorkDefinition;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;

/**
 * Generate MIS from TAT - both operations should be run one after the other
 * This loader does series of Export operations in async to generate weekly MIS
 * Input to this is in the form of Start Date (typically 2001-01-01) . If not provided, we'll take it.
 * @author hrishi
 *
 */
public class DummyExporterChainLoader extends MultipleExporterAsyncLoader {

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("DummyExporterChainLoader");

	public static final String TheLoader = "DummyExporterChainLoader";
	
	public DummyExporterChainLoader() {
		super(TheLoader, "Dummy Loader to do 2 things");

		// Add Attribute definition(s)
	}

	@Override
	protected List<ExporterWorkDefinition> getExporterWorkerDefinitions(GenericJSON json) {
		List<ExporterWorkDefinition> retval = new ArrayList<ExporterWorkDefinition>();
		// This requires 2 Exporters, first is TAT exporter, and second is MIS
		
		retval.add(new ExporterWorkDefinition(DummyTATCreator.TheExporter));
		retval.add(new ExporterWorkDefinition(DummyTATProcessor.TheExporter));
		
		return retval;
	}
}
