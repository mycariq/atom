package com.cariq.toolkit.utils.profile.listener;

import com.cariq.toolkit.utils.profile.listener.instrument.FailurePoint.FailureTime;

/**
 * Exception thrown by FailurePoint instrument
 * @TODO shoud have been inside FailurePoint itself.
 * @author Abhijit
 *
 */
@SuppressWarnings("serial")
public class FailurePointException extends RuntimeException {

	public FailurePointException(FailureTime failureTime, String action) {
		super("FailurePoint!!! - Exception " + failureTime + " doing operation: " + action);
	}

}
