package com.cariq.toolkit.utils;

import java.util.ArrayList;
import java.util.List;

public class TopItems<T> implements ObjectBuilder <ProximityComparable<T>>{
	int proximityThreshold = 0; // unless specified
	List<ProximityComparable<T>> list = new ArrayList<ProximityComparable<T>>();
	int maxItems;
	
	enum ActionEnum {
		None,
		Set,
		Add,
		Drop
	}
	
	public TopItems(int maxItems) {
		this(maxItems, 0);
	}
	
	public TopItems(int maxItems, int proximityThreshold) {
		super();
		this.proximityThreshold = proximityThreshold;
		this.maxItems = maxItems;
	}


	@Override
	public ObjectBuilder<ProximityComparable<T>> configure(String setting,
			Object value) {
		if (setting.equals(Utils.PROXIMITY_THRESHOLD))
			proximityThreshold = (int) value;
		
		return this;
	}

	@Override
	public TopItems<T> add(ProximityComparable<T> value) {
		for (int i = 0; i < maxItems; i++) {
			ActionEnum action = getAction(i, value);
			switch (action) {
			case Add: // insert into the list, moving the list down
				list.add(i, value);
				return this;
				
			case Set: // replace current item
				list.set(i, value);
				return this;
				
			case None:
				break; // break to next item in list
			
			case Drop: // no action to be taken
				return this;
			
			default:
				Utils.handleException("Top calculator: No action found");
				break;
			}
		}
			
		return this;
	}

	private ActionEnum getAction(int index, ProximityComparable<T> value) {
		if (list.size() <= index)
			return ActionEnum.Add;
		
		ProximityComparable<T> original = list.get(index);
		int comparison = original.compareTo((T) value);
		
		// Check if list has an element that is greater
		boolean originalIsGreater = (comparison >= 0);
		// Check the proximity of the new value with the one in list
		boolean inProximity = Math.abs(comparison) <= proximityThreshold;
		
		if (inProximity) {
			if(originalIsGreater)
				return ActionEnum.Drop;

			return ActionEnum.Set;
		}
		
		// not in proximity
		if (!originalIsGreater)
			return ActionEnum.Add;
		
		return ActionEnum.None;
	}
				
	@Override
	public TopItems<T> build() {
		// Build a new list with only first "maxCount" objects
		if (list.size() <= maxItems)
			return this;
		
		// truncate list to keep only maxItems
		while(list.size() > maxItems)
			list.remove(list.size()-1);
		
		return this;
	}

	@Override
	public TopItems<T> add(String parameter,
			ProximityComparable value) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("ObjectBuilder<ProximityComparable>.add() is not implemented");
	}
	
	public List<ProximityComparable<T>> getTopItems() {
		return list;
	}
}
