package com.cariq.toolkit.utils;

import com.cariq.toolkit.CQVersion;
import com.cariq.toolkit.coreiq.server.Server;
import com.cariq.toolkit.coreiq.server.ServerActivity;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.utils.profile.ThroughputCalculator;

/**
 * All CarIQ APIs must be called through this class. It's a RAII class to
 * capture entry and exit conditions. It will be enhanced over time. To start
 * with, it contains logging and ProfilePoint
 * 
 * @author HVN
 *
 *         == Eclipse CarIQAPI macro == try (CarIQAPI _${myvar} = new
 *         CarIQAPI("CARIQ_${myvar}")) { ${line_selection}${cursor} }
 */

public class CarIQAPI implements AutoCloseable {
	String apiName;
	ProfilePoint profilePoint;
	String userName;
	long startTime;
	ServerActivity serverAPI;
	
	private static int activeApiCallCount = 0;

	static {
		ProfilePoint.setRenderer(new ProfilePointLogRenderer());
		ThroughputCalculator.setRenderer(new ProfilePointLogRenderer());
	}

	public CarIQAPI(String apiName) {
		this(apiName, Utils.getUserNameForCarIQApi());
	}

	public CarIQAPI(String apiName, String userName) {
		this.apiName = apiName;
		this.userName = userName;
		this.startTime = System.currentTimeMillis();
		
		// verify platform expiry date
		if (CQVersion.isPlatformExpired())
			Utils.handleException("Crossed platform expiry date. Please contact CarIQ Support support@mycariq.com!");

		try {
			//This should on top
			setApiContext();
			if (apiName != null)
				profilePoint = ProfilePoint.profileActivity(apiName);

			if (apiName != null && userName != null) {
				APIHelper.logger.getLogger("API START").debug(apiName + ",Caller," + userName);

				serverAPI = Server.getInstance().registerWithUserName(Server.API_AUDIT, userName, apiName);
			}
			
			incrementActiveAPICount();
			CarIQToolkitHelper.logger.getLogger("API ACTIVE").debug("Count: " + activeApiCallCount);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setApiContext() {
		APIContext context = new APIContext();
		context.setValue(APIContext.USER_NAME, userName);
		context.setValue(APIContext.OPERATION, apiName);
		context.setValue(APIContext.TIMEZONE, Utils.INDIA_TIME_ZONE);
		APIContext.setContext(context);
	}

	@Override
	public void close() {
		try {
			if (profilePoint != null)
				profilePoint.close();

			if (apiName != null && userName != null) {
				long elapsed = (System.currentTimeMillis() - startTime);
				
				APIHelper.logger.getLogger("API END").debug(apiName + ",Caller," + userName);
				APIHelper.logger.getLogger("CarIQAPI")
						.debug(apiName + "," + userName + "," + elapsed);
				
				// APIAuditor.register(apiName, elapsed);
				
				if (serverAPI != null)
					serverAPI.close();

				//this should done at last
				APIContext.remove();
			}
			decrementActiveAPICount();
			CarIQToolkitHelper.logger.getLogger("API DEACTIVE").debug("Count: " + activeApiCallCount);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getUserName() {
		return userName;
	}
	
	private synchronized void incrementActiveAPICount() {
		activeApiCallCount++;
	}
	
	private synchronized void decrementActiveAPICount() {
		activeApiCallCount--;
	}
}
