package com.cariq.toolkit.service;

import java.util.List;

import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.model.Domain.Domain_Out;
import com.cariq.toolkit.model.User.User_Out;

/**
 * The Interface DomainService.
 */
public interface DomainAccessService {

	/**
	 * Associates User with Domain
	 * @param userName
	 * @param domainName
	 * @return
	 */
	public ResponseJson grantDomainAccessToUser(String userName, String domainName);
	
	/**
	 * Removes Domain-user association
	 * @param userName
	 * @param domainName
	 * @return
	 */
	public ResponseJson removeDomainAccessForUser(String userName, String domainName);
	
	/**
	 * Lists domains accessible for given user
	 * @param userName
	 * @return
	 */
	public List<Domain_Out> getAccessibleDomainsForUser(String userName);
	
	/**
	 * Lists users allowed to access the given domain 
	 * @param domainName
	 * @return
	 */
	public List<User_Out> getPrivilegedUsersForDomain(String domainName);
}

