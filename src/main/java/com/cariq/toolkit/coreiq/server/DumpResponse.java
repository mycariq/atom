package com.cariq.toolkit.coreiq.server;

import java.util.Date;

import com.cariq.toolkit.utils.GenericJSON;


public class DumpResponse extends DumpRequest implements ServerSpecificResponse {

	private String serverName; // server which actually performed the request
	private Date serverStartTime; // When server started
	private GenericJSON response = new GenericJSON();

	public DumpResponse() {}

	public DumpResponse(DumpRequest request, String serverName, Date serverStartTime, GenericJSON response) {
		super(request.getDumpType(), request.getServerName());
		this.serverName = serverName;
		this.serverStartTime = serverStartTime;
		this.response = response;
	}

	public GenericJSON getResponse() {
		return response;
	}

	public void setResponse(GenericJSON response) {
		this.response = response;
	}

	@Override
	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	@Override
	public Date getServerStartTime() {
		return serverStartTime;
	}

	public void setServerStartTime(Date serverStartTime) {
		this.serverStartTime = serverStartTime;
	}

	
}
