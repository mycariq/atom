package com.cariq.toolkit.utils.wwwservicetest.core;

import org.junit.Test;

import com.cariq.toolkit.utils.wwwservice.core.BitOperations;

public class BitOperationsTest {

	@Test
	public void ShiftLeftRightTest() {
		int number = 1;
		for (int i = 0; i < 64; i++) {
			long num = BitOperations.shiftLeft(number, i);
			System.out.println("1 << " + i + " = " + num);
			System.out.println(num + " >> " + i + " = " + BitOperations.shiftRight(num, i));
		}
	}
	
	@Test
	public void testMasking() {
		int left = 2500000, right = 1500000;
		long num = BitOperations.shiftLeft(left, 32) + right;
		System.out.println("Composite of " + left + "," + right + " = " + num);
		
		int leftNum = (int) BitOperations.valueOf(num, 32, 64);
		int rightNum = (int) BitOperations.valueOf(num, 0, 32);
		
		System.out.println("After conversion left = " + leftNum + " and right = " + rightNum);

	}

}
