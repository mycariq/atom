package com.atom.www.exporter;

import java.util.Arrays;

import com.cariq.toolkit.coreiq.exporter.MonthlyAggregationExporterTemplate;

/**
 * The Class VerificationTATExporter.
 */
public class VerificationTATExporter extends MonthlyAggregationExporterTemplate {

	/** The Constant EXPORTER. */
	public static final String EXPORTER = "VerificationTATExporter", SHORT_NAME = "Verification TAT";

	/** The Constant FifthWeekPer. */
	private static final String MONTH = "Month", FirstWeekCount = "0-7 Days Count", FirstWeekPer = "0-7 Days %",
			SecondWeekCount = "8-14 Days Count", SecondWeekPer = "8-14 Days %", ThirdWeekCount = "15-21 Days Count",
			ThridWeekPer = "15-21 Days %", FourthWeekCount = "22-28 Days Count", FourthWeekPer = "22-28 Days %",
			FifthWeekCount = "29-31 Days Count", FifthWeekPer = "29-31 Days %";

	/** The Constant RANGE_HEADER. */
	private static final String RANGE_HEADER[] = { "0-7 Days", "8-14 Days", "15-21 Days", "22-28 Days", ">29 Days" };

	/**
	 * Instantiates a new ICICI verification TAT exporter.
	 */
	public VerificationTATExporter() {
		super(EXPORTER, SHORT_NAME, "The " + EXPORTER);

		addExportAttributes(Arrays.asList(MONTH, FirstWeekCount, FirstWeekPer, SecondWeekCount, SecondWeekPer,
				ThirdWeekCount, ThridWeekPer, FourthWeekCount, FourthWeekPer, FifthWeekCount, FifthWeekPer));
	}
}
