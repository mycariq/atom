package com.cariq.toolkit.utils;

import java.io.IOException;

import com.cariq.toolkit.utils.analytics.core.AnalyticsContext;
import com.cariq.toolkit.utils.analytics.core.Column;
import com.cariq.toolkit.utils.analytics.core.ExcelTableWriterStream;
import com.cariq.toolkit.utils.analytics.core.Record;
import com.cariq.toolkit.utils.analytics.core.RecordProcessorTemplate;
import com.cariq.toolkit.utils.analytics.core.TableProcessor;
import com.cariq.toolkit.utils.analytics.core.TableProcessorImpl;
import com.cariq.toolkit.utils.analytics.core.TableWriterStream;

/**
 * The Class CsvToExcelConvertor.
 */
public class CsvToExcelConvertor {

	/** The sheet name. */
	private String sheetName;

	/**
	 * Instantiates a new csv to excel convertor.
	 *
	 * @param sheetName
	 *            the sheet name
	 */
	public CsvToExcelConvertor(String sheetName) {
		this.sheetName = sheetName;
	}

	/**
	 * Convert.
	 *
	 * @param csvFile
	 *            the csv file
	 * @param excelFile
	 *            the excel file
	 * @throws Exception
	 *             the exception
	 */
	public void convert(String csvFile, final String excelFile) throws Exception {
		if (!CarIQFileUtils.exists(csvFile))
			return;

		AnalyticsContext ctx = new AnalyticsContext();

		try (TableWriterStream writer = new ExcelTableWriterStream(excelFile, sheetName)) {

			executeBatchProcessor(ctx, csvFile, "CsvToExcel", new RecordProcessorTemplate(ctx) {
				boolean first = true;

				@Override
				protected Record doProcess(Record record) throws IOException {

					if (first) {
						writeHeader(writer, record);
						first = false;
					}

					writer.write(record);

					return record;
				}
			});

		}
	}

	/**
	 * Write header.
	 *
	 * @param writer
	 *            the writer
	 * @param record
	 *            the record
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private void writeHeader(TableWriterStream writer, Record record) throws IOException {
		for (Column column : record.getColumns()) {
			writer.addColumn(column);
		}
		writer.writeHeader();
	}

	/**
	 * Execute batch processor.
	 *
	 * @param ctx
	 *            the ctx
	 * @param inputFile
	 *            the input file
	 * @param processId
	 *            the process id
	 * @param recordProcessorTemplate
	 *            the record processor template
	 * @return the int
	 * @throws Exception
	 *             the exception
	 */
	private void executeBatchProcessor(AnalyticsContext ctx, String inputFile, String processId,
			RecordProcessorTemplate recordProcessorTemplate) throws Exception {

		// Process the File using simple Analytics processor
		TableProcessor processor = new TableProcessorImpl(ctx, processId, inputFile, null);

		processor.setForgiveException(false);
		processor.add(recordProcessorTemplate);
		processor.init();
		processor.process();
		processor.close();
	}
}
