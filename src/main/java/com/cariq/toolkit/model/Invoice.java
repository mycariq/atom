package com.cariq.toolkit.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.Invoice.Invoice_In;
import com.cariq.toolkit.model.Invoice.Invoice_Out;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.GenericJsonable;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;
import com.cariq.toolkit.utils.Utils;
import com.google.common.base.Strings;

/**
 * This class implements "invoice"
 * Invoice is generated as outcome of billing operation. It determines the total amount billed against specific account.
 * Invoice comprises of one or more invoice units.
 * @author amita
 *
 */

@Entity
@Configurable
public class Invoice extends PersistentObject<Invoice> implements JSONable<Invoice_In, Invoice_Out, IdJSON>, GenericJsonable{
	
	/**
	 * The Enum InvoiceStatusEnum.
	 */
	public static enum InvoiceStatusEnum {
		IN_PROGRESS, READY, INVALID, IN_REVIEW, APPROVED, PAID, CANCELLED 
	};

	// SECTION 1 -Code for Persistence
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@Version
	Integer version;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new Invoice().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null)
			this.entityManager = entityManager();
		return entityManager;
	}

	/**
	 * @param id
	 * @return
	 */
	protected static Invoice find(Long id) {
		return entityManager().find(Invoice.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public Invoice findById(Long id) {
		return find(id);
	}

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/**
	 * Model properties
	 */
	@NotNull
	@ManyToOne
	BillingAccount raisedbyAccount;

	@NotNull
	@ManyToOne
	BillingAccount againstAccount;
	
	@NotNull
	@ManyToOne
	BillingContract itsBillingContract;
	
	@NotNull
	@ManyToOne
	BillingOperation itsBillingOperation;

	@Size(max = 4000)
	String billingTerms; 
	
	// MMMM-yyyy (February-2020)
	String startMonth;
	
	// MMMM-yyyy (October-2021)
	String endMonth;
	
	Long amount;
	
	String status;
	
	String urlToDocument;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	// Default constructor is a must!
	public Invoice() {
		super();
	}


	/**
	 * 
	 * @param raisedbyAccount
	 * @param againstAccount
	 * @param itsBillingOperation
	 * @param billingTerms
	 * @param startMonth
	 * @param endMonth
	 * @param amount
	 * @param status
	 * @param urlToDocument
	 * @param createdOn
	 * @param modifiedOn
	 */
	public Invoice(BillingAccount raisedbyAccount, BillingAccount againstAccount, BillingContract itsBillingContract, 
			BillingOperation itsBillingOperation, String billingTerms, String startMonth, String endMonth, 
			Long amount, String status, String urlToDocument, Date createdOn, Date modifiedOn) {
		super();
		this.raisedbyAccount = raisedbyAccount;
		this.againstAccount = againstAccount;
		this.itsBillingOperation = itsBillingOperation;
		this.itsBillingContract = itsBillingContract;
		this.billingTerms = billingTerms;
		this.startMonth = startMonth;
		this.endMonth = endMonth;
		this.amount = amount;
		this.status = status;
		this.urlToDocument = urlToDocument;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
	}

	public BillingAccount getRaisedbyAccount() {
		return raisedbyAccount;
	}

	public void setRaisedbyAccount(BillingAccount raisedbyAccount) {
		this.raisedbyAccount = raisedbyAccount;
	}

	public BillingAccount getAgainstAccount() {
		return againstAccount;
	}

	public void setAgainstAccount(BillingAccount againstAccount) {
		this.againstAccount = againstAccount;
	}

	public BillingContract getItsBillingContract() {
		return itsBillingContract;
	}

	public void setItsBillingContract(BillingContract itsBillingContract) {
		this.itsBillingContract = itsBillingContract;
	}
	
	public BillingOperation getItsBillingOperation() {
		return itsBillingOperation;
	}

	public void setItsBillingOperation(BillingOperation itsBillingOperation) {
		this.itsBillingOperation = itsBillingOperation;
	}

	public String getBillingTerms() {
		return billingTerms;
	}

	public void setBillingTerms(String billingTerms) {
		this.billingTerms = billingTerms;
	}

	public String getStartMonth() {
		return startMonth;
	}

	public void setStartMonth(String startMonth) {
		this.startMonth = startMonth;
	}

	public String getEndMonth() {
		return endMonth;
	}

	public void setEndMonth(String endMonth) {
		this.endMonth = endMonth;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUrlToDocument() {
		return urlToDocument;
	}

	public void setUrlToDocument(String urlToDocument) {
		this.urlToDocument = urlToDocument;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
	
	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------


	// Implementation of toString and hashCode and equals are advisable for all
	// Model classes
	@Override
	public String toString() {
		return "Invoice [id=" + id + ", version=" + version + ", raisedbyAccount=" + raisedbyAccount.getName() 
				+ ", againstAccount=" + againstAccount.getName() + ", itsBillingContract=" + itsBillingContract.getId() 
				+ ", itsBillingOperation=" + itsBillingOperation.getId()
				+ ", billingTerms=" + billingTerms + ", startMonth=" + startMonth + ", endMonth" + endMonth
				+ ", amount=" + amount + ", status=" + status + ", urlToDocument=" + urlToDocument 
				+ ", createdOn=" + createdOn + ", modifiedOn=" + modifiedOn + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((raisedbyAccount == null) ? 0 : raisedbyAccount.getName().hashCode());
		result = prime * result + ((againstAccount == null) ? 0 : againstAccount.getName().hashCode());
		result = prime * result + ((itsBillingContract == null) ? 0 : itsBillingContract.getId().hashCode());
		result = prime * result + ((itsBillingOperation == null) ? 0 : itsBillingOperation.getId().hashCode());
		result = prime * result + ((billingTerms == null) ? 0 : billingTerms.hashCode());
		result = prime * result + ((startMonth == null) ? 0 : startMonth.hashCode());
		result = prime * result + ((endMonth == null) ? 0 : endMonth.hashCode());
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((urlToDocument == null) ? 0 : urlToDocument.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Invoice other = (Invoice) obj;
		if (raisedbyAccount == null) {
			if (other.raisedbyAccount != null)
				return false;
		} else if (!raisedbyAccount.equals(other.raisedbyAccount))
			return false;
		if (againstAccount == null) {
			if (other.againstAccount != null)
				return false;
		} else if (!againstAccount.equals(other.againstAccount))
			return false;
		if (itsBillingContract == null) {
			if (other.itsBillingContract != null)
				return false;
		} else if (!itsBillingContract.equals(other.itsBillingContract))
			return false;
		if (itsBillingOperation == null) {
			if (other.itsBillingOperation != null)
				return false;
		} else if (!itsBillingOperation.equals(other.itsBillingOperation))
			return false;
		if (billingTerms == null) {
			if (other.billingTerms != null)
				return false;
		} else if (!billingTerms.equals(other.billingTerms))
			return false;
		if (startMonth == null) {
			if (other.startMonth != null)
				return false;
		} else if (!startMonth.equals(other.startMonth))
			return false;
		if (endMonth == null) {
			if (other.endMonth != null)
				return false;
		} else if (!endMonth.equals(other.endMonth))
			return false;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (urlToDocument == null) {
			if (other.urlToDocument != null)
				return false;
		} else if (!urlToDocument.equals(other.urlToDocument))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		return true;
	}

	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API
	// interaction
	
	// In Class (for construction)
	public static class Invoice_In {
		String raisedbyAccountName;
		String againstAccountName;
		Long itsBillingContractId;
		Long itsBillingOperationId;
		String billingTerms;
		String startMonth; 
		String endMonth;
		Long amount;
		String status;
		String urlToDocument;

		public Invoice_In() {
			super();
		}

		/**
		 * 
		 * @param raisedbyAccountName
		 * @param againstAccountName
		 * @param itsBillingOperationId
		 * @param billingTerms
		 * @param startMonth
		 * @param endMonth
		 * @param amount
		 * @param status
		 * @param urlToDocument
		 */
		public Invoice_In(String raisedbyAccountName, String againstAccountName, Long itsBillingContractId, Long itsBillingOperationId,
				String billingTerms, String startMonth, String endMonth, Long amount, String status, String urlToDocument) {
			super();
			this.raisedbyAccountName = raisedbyAccountName;
			this.againstAccountName = againstAccountName;
			this.itsBillingContractId = itsBillingContractId;
			this.itsBillingOperationId = itsBillingOperationId;
			this.billingTerms = billingTerms;
			this.startMonth = startMonth;
			this.endMonth = endMonth;
			this.amount = amount;
			this.status = status;
			this.urlToDocument = urlToDocument;
		}

		/**
		 * Getters - Setters
		 */
		
		public String getRaisedbyAccountName() {
			return raisedbyAccountName;
		}

		public void setRaisedbyAccountName(String raisedbyAccountName) {
			this.raisedbyAccountName = raisedbyAccountName;
		}

		public String getAgainstAccountName() {
			return againstAccountName;
		}

		public void setAgainstAccountName(String againstAccountName) {
			this.againstAccountName = againstAccountName;
		}

		public Long getItsBillingContractId() {
			return itsBillingContractId;
		}

		public void setItsBillingContractId(Long itsBillingContractId) {
			this.itsBillingContractId = itsBillingContractId;
		}
		
		public Long getItsBillingOperationId() {
			return itsBillingOperationId;
		}

		public void setItsBillingOperationId(Long itsBillingOperationId) {
			this.itsBillingOperationId = itsBillingOperationId;
		}
		
		public String getBillingTerms() {
			return billingTerms;
		}

		public void setBillingTerms(String billingTerms) {
			this.billingTerms = billingTerms;
		}

		public String getStartMonth() {
			return startMonth;
		}

		public void setStartMonth(String startMonth) {
			this.startMonth = startMonth;
		}

		public String getEndMonth() {
			return endMonth;
		}

		public void setEndMonth(String endMonth) {
			this.endMonth = endMonth;
		}
		
		public Long getAmount() {
			return amount;
		}

		public void setAmount(Long amount) {
			this.amount = amount;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getUrlToDocument() {
			return urlToDocument;
		}

		public void setUrlToDocument(String urlToDocument) {
			this.urlToDocument = urlToDocument;
		}	
	}

	// Out class for output
	public static class Invoice_Out extends Invoice_In {
		Long id;
		String createdOn;
		String modifiedOn;

		public Invoice_Out() {
			super();
		}

		public Invoice_Out(String raisedbyAccountName, String againstAccountName, Long itsBillingContractId,
				Long itsBillingOperationId, String billingTerms, String startMonth, String endMonth, Long amount, String status, 
				String urlToDocument, String createdOn, String modifiedOn, Long id) {
			super(raisedbyAccountName, againstAccountName, itsBillingContractId, itsBillingOperationId, billingTerms, startMonth, endMonth, amount, status, urlToDocument);
			this.id = id;
			this.createdOn = createdOn;
			this.modifiedOn = modifiedOn;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toJSON()
	 */
	@Override
	public Invoice_Out toJSON() {
		return new Invoice_Out(raisedbyAccount.getName(), againstAccount.getName(), itsBillingContract.getId(), itsBillingOperation.getId(), 
				billingTerms, startMonth, endMonth, amount, status, urlToDocument, createdOn.toString(), modifiedOn.toString(), id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#fromJSON(java.lang.Object)
	 */
	@Override
	public void fromJSON(Invoice_In json) {
		this.raisedbyAccount =  json.getRaisedbyAccountName() != null ? BillingAccount.getByName(json.getRaisedbyAccountName()) : null;
		this.againstAccount =  json.getAgainstAccountName() != null ? BillingAccount.getByName(json.getAgainstAccountName()) : null;
		this.itsBillingContract = json.getItsBillingContractId() != null ? BillingContract.find(json.getItsBillingContractId()) : null;
		this.itsBillingOperation = json.getItsBillingOperationId() != null ? BillingOperation.find(json.getItsBillingOperationId()) : null;
		this.billingTerms =  json.getBillingTerms();
		this.startMonth = json.getStartMonth();
		this.endMonth = json.getEndMonth();
		this.amount = json.getAmount() != null ? json.getAmount() : null;
		this.status = json.getStatus() != null ? json.getStatus() : null;
		this.urlToDocument = json.getUrlToDocument() != null ?  json.getUrlToDocument() : null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}
	
	@Override
	public GenericJSON toGenericJSON() {
		List<GenericJSON> invoiceUnits = new ArrayList<GenericJSON>();
		List<BillingTerm> btList = new ArrayList<BillingTerm>();
		
		String summary = "";
		for (InvoiceUnit iu : InvoiceUnit.getByItsInvoice(this)) {
			invoiceUnits.add(iu.toGenericJSON());
			BillingTerm bt = iu.getItsBillingTerm();
			if (null != bt && !btList.contains(bt)) {
				btList.add(bt);
				if (! Strings.isNullOrEmpty(bt.getNote()))
					summary += bt.getNote() + ". ";
			}
		}
		
		return GenericJSON.build("Summary", summary, "RaisedbyAccount", raisedbyAccount.toGenericJSON(), 
				"AgainstAccount", againstAccount.toGenericJSON(), 
				"itsBillingContract", itsBillingContract.toGenericJSON(),
				"itsBillingOperationId", itsBillingOperation.getId(), "BillingTerms", billingTerms, 
				"StartMonth", startMonth, "EndMonth", endMonth, "Amount", amount, 
				"Status", status, "urlToDocument", urlToDocument, "CreatedOn", createdOn.toString(), 
				"ModifiedOn", modifiedOn.toString(), "Id", id, "invoiceUnits", invoiceUnits);
	}

	
	@Override
	public void fromGenricJSON(GenericJSON json) {
		Utils.notImplementedException("Invoice.fromGenricJSON");
	}

	// ---------------------------------------------
	// SECTION 5 - Query and Class specific finders and static utility methods
	// ---------------------------------------------
	public static Invoice findObjectById(Long id) {
		return find(id);
	}
	
	public static List<Invoice> getAllObjects(int pageNo, int pageSize) {
		CarIQSimpleQuery<Invoice> qry = new CarIQSimpleQuery<Invoice>("getAllObjects", entityManager(), Invoice.class);
		qry.setPaging(pageNo, pageSize);

		return qry.getResultList();
	}

	public static List<Invoice> getAgainstAccount(BillingAccount account) {
		CarIQSimpleQuery<Invoice> query = new CarIQSimpleQuery<Invoice>("getAgainstAccount", entityManager(), Invoice.class);
		query.addCondition("againstAccount", CarIQSimpleQuery.EQ, account);
		
		return query.getResultList();
	}
	
	public static List<Invoice> getRaisedByAccount(BillingAccount account) {
		CarIQSimpleQuery<Invoice> query = new CarIQSimpleQuery<Invoice>("getRaisedByAccount", entityManager(), Invoice.class);
		query.addCondition("raisedbyAccount", CarIQSimpleQuery.EQ, account);
		
		return query.getResultList();
	}

	public static Invoice getByItsOperation(BillingOperation operation) {
		CarIQSimpleQuery<Invoice> query = new CarIQSimpleQuery<Invoice>("getByItsOperation", entityManager(), Invoice.class);
		query.addCondition("itsBillingOperation", CarIQSimpleQuery.EQ, operation);
		
		if (query.getResultList().isEmpty())
			return null;
		return query.getSingleResult();
	}
	
	public static Invoice getLatestInvoice() {
		CarIQSimpleQuery<Invoice> qry = new CarIQSimpleQuery<Invoice>("getLatestInvoice", entityManager(),
				Invoice.class);
		qry.addCondition("status", CarIQSimpleQuery.EQ, InvoiceStatusEnum.READY.toString());
		qry.orderBy("id", CarIQSimpleQuery.DESC);

		List<Invoice> invoiceList = qry.getResultList();
		if (!Utils.isNullOrEmpty(invoiceList))
			return invoiceList.get(0);

		return null;
	}
}