package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.atom.www.helper.DomainHelper;
import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.Domain.Domain_Out;
import com.cariq.toolkit.model.ProxyObject;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

/**
 * Exporter exports cases where customer has agreed to return device. This cases will be
 * handed over to device retrieval team. And team will retrieve the device.
 * 
 * @author sagar
 *
 */
public class GetDeviceBackExporter extends SpotlightTemplate {
	
	public static final String TheExporter = GetDeviceBackExporter.class.getSimpleName();
	
	public static final String TheDescription = "Exporter exports cases where customer has agreed to return device. This cases will be " +
					"handed over to device retrieval team. And team will retrieve the device.";
	
	public static final String DOMAIN = "domain";
	
	public static final String AWB_NUMBER = "AWBNo";
	public static final String CUSTOMER_FIRST_NAME = "CustomerFirstName";
	public static final String CUSTOMER_ADDRESS = "CustomerAddress";
	public static final String CUSTOMER_STATE = "CustomerState";
	public static final String CUSTOMER_CITY = "CustomerCity";
	public static final String CUSTOMER_PHONE_NO = "CustomerPhoneNo";
	public static final String CUSTOMER_PIN_CODE = "CustomerPinCode";
	public static final String INSTRUCTION = "Instruction";
	public static final String ORDER_NO = "OrderNo";
	public static final String PRODUCT_ID = "ProductID";
	public static final String PRODUCT_QTY = "ProductQty";
	public static final String PRODUCT_NAME = "ProductName";
	public static final String PRODUCT_MRP = "ProductMRP";
	public static final String RETURN_ADDRESS = "ReturnAddress";
	public static final String RETURN_STATE = "ReturnState";
	public static final String RETURN_CITY = "ReturnCity";
	public static final String RETURN_PIN_CODE = "ReturnPinCode";
	public static final String RETURN_PHONE_NO = "ReturnPhoneNo";
	public static final String EXPECTED_WEIGHT = "ExpectedWeight";
	public static final String NET_PAYMENT = "NetPayment";
	
	public static final String QueryType_default = "Default";
	private static final String defaultQueryTypeDescription = "Get ready to retrieve device cases by owner and domain.";
	
	private DomainHelper domainHelper;

	/**
	 * Get device back exporter constructor.
	 */
	public GetDeviceBackExporter() {
		super(TheExporter, TheDescription);
		
		domainHelper = new DomainHelper();
		
		addQueryType(QueryType_default, defaultQueryTypeDescription,
				Arrays.asList(new QueryDefinitionAttribute(DOMAIN, "Name of domain", String.class)));
		
		// Support roles
		addSupportedRoles(
				Arrays.asList(Utils.ADMIN_ROLE, Utils.USER_ROLE));
	}

	@Override
	protected String getSignatures(String queryType, GenericJSON json, int pageNo, int pageSize) {
		
		//Find valid owner
		String owner = Utils.getValue(json, KEY_OWNER);
		User user = owner == null ? User.getLoggedInUser() : User.getByUsername(owner);
		
		//Find domain
		String domainName = Utils.getValue(json, DOMAIN);
		Domain domain = Domain.getByName(domainName);
		if(domain == null) {
			Domain_Out domainOut = domainHelper.getDomainByUser(user);
			domain = Domain.getByName(domainOut.getName());
		}
		
		String commaSeperatedStringForQuery = Utils.getCommaSeperatedValuesForQuery(StagingAndPolicyHelper.TAG_READY_TO_RETRIEVE);
		
		//Find Proxies marked as Ready to retrieve tag 
		List<ProxyObject> readyToRetrieveProxies = ProxyObject.getByLatestTagsAndDomain(commaSeperatedStringForQuery,
				domain, pageNo, pageSize);
		
		//Convert proxies into signatures.
		String signatures = Utils.getCommaSeperatedString(ProxyObject.getSignatures(readyToRetrieveProxies));
		logger.debug("Ready To retrieve signatures: " + signatures);
		
		//return signatures
		return signatures;
	}

	@Override
	protected String getQueryType() {
		return StagingAndPolicyHelper.QueryType_ByPolicyNumber;
	}

	@Override
	protected List<GenericJSON> postProcess(String queryType, GenericJSON inputJson, List<GenericJSON> outputJson,
			int pageNo, int pageSize) {
		List<GenericJSON> processedJSONs = new ArrayList<>();

		for(GenericJSON oJson : outputJson) {
			GenericJSON json = new GenericJSON();
			
			json.put(AWB_NUMBER, "");
			json.put(CUSTOMER_FIRST_NAME, Utils.getValue(oJson, "Staging_customerFirstName"));
			json.put(CUSTOMER_ADDRESS, Utils.getValue(oJson, "Staging_customerAddress"));
			json.put(CUSTOMER_STATE, Utils.getValue(oJson, "Staging_customerState"));
			json.put(CUSTOMER_CITY, Utils.getValue(oJson, "Staging_customerCity"));
			json.put(CUSTOMER_PHONE_NO, Utils.getValue(oJson, "Staging_customerMobileNumber"));
			json.put(CUSTOMER_PIN_CODE, Utils.getValue(oJson, "Staging_customerPinCode"));
			json.put(INSTRUCTION, "Close box");
			json.put(ORDER_NO, "");
			json.put(PRODUCT_ID, Utils.getValue(oJson, "policyNumber"));
			json.put(PRODUCT_QTY, "1");
			json.put(PRODUCT_NAME, "IL Assist");
			json.put(PRODUCT_MRP, "3000");
			json.put(RETURN_ADDRESS, Utils.CARIQ_ADDRESS);
			json.put(RETURN_STATE, Utils.CARIQ_STATE);
			json.put(RETURN_CITY, Utils.CARIQ_CITY);
			json.put(RETURN_PIN_CODE, Utils.CARIQ_PIN_CODE);
			json.put(RETURN_PHONE_NO, "9049455400");
			json.put(EXPECTED_WEIGHT, "200");
			json.put(NET_PAYMENT, "3000");
			
			processedJSONs.add(json);
		}
		return processedJSONs;
	}
	
	
}
