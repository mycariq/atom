package com.cariq.toolkit.coreiq.tableUtil.application.mis;

import com.cariq.toolkit.coreiq.formula.interfaces.VariableResolverIfc;
import com.cariq.toolkit.coreiq.tableUtil.impl.CellAddress;
import com.cariq.toolkit.coreiq.tableUtil.impl.ProcessingContext;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.CellIfc;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

/**
 * Resolve the variable value for the formula
 * @author hrishi
 *
 */
public class MISTableCellValueResolver implements VariableResolverIfc {
	@Override
	public Object resolve(Object context, GenericJSON valueDefinition) {
		ProcessingContext processingContext = Utils.downCast(ProcessingContext.class, context);
		if (processingContext == null)
			Utils.handleException("Processing Context is null for  MISTableCellValueResolver");

		// Load the address
		try {
			CellIfc cell = MISTableCellLocator.locate(processingContext,
					Utils.constructJSONable(CellAddress.class, valueDefinition));
			
			// if cell is found...
			if (cell != null)
				return cell.getValue();
		} catch (Exception e) {
			Utils.handleException("Unable to get Cell Value from " + Utils.getJSonString(valueDefinition));
		}

		return null;
	}

}
