package com.atom.www.loader;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.coreiq.loader.CarIQLoader;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderContext;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderRegistry;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Transactional
@Configurable
public class StageBasedOwnerAssignmentTest {
	
	@Before
	public void init() {
		Utils.setupSecurityContext("preeti", "875d48192ccbbb18f32a13cec13cac14", "a1");
	}

	@Test
	public void testLoader() {
		CarIQLoader loader = CarIQLoaderRegistry.getLoader(StageBasedOwnerAssignmentLoader.TheLoader);
		
		GenericJSON json = new GenericJSON();
		json.add(OwnerTransferLoaderTemplate.DOMAIN, Utils.DOMAIN_ILASSIST);
		json.add(StageBasedOwnerAssignmentLoader.STAGE, StagingAndPolicyHelper.StateUnVerified 
				+ Utils.COMMA + StagingAndPolicyHelper.StateUserCreated);
		
		json.add(OwnerTransferLoaderTemplate.CURRENT_OWNERS, "prachi@icici,devyani@icici");
		
		json.add(OwnerTransferLoaderTemplate.NEW_OWNERS, "sweta,preeti");
		
		CarIQLoaderContext context = new CarIQLoaderContext();
		
		loader.load(json, context);
		
		System.out.println("Context: " + context.toString());
	}
}
