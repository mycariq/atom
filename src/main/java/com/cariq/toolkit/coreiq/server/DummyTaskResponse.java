package com.cariq.toolkit.coreiq.server;

public class DummyTaskResponse extends DummyTaskRequest {

	private String message;

	public DummyTaskResponse() {}

	public DummyTaskResponse(String taskType, long executionTimeSec, String message) {
		super(taskType, executionTimeSec);
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
