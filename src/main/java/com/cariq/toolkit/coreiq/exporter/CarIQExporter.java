package com.cariq.toolkit.coreiq.exporter;

import java.util.List;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.HtmlValueRenderer;
import com.cariq.toolkit.utils.SimplePOJOs.CountPOJO;

/**
 * Interface of Exporter. This defines what the Exporter expects and provides
 * Using the Exporter, CSV file can be created
 * 
 * @author root
 *
 */
public interface CarIQExporter {
	String getName();

	String getDescription();

	// Based on the condition (one of supported ones) and GenericJSON, get List of Generic JSONs.
	List<GenericJSON> export(String queryType, GenericJSON json, int pageNo, int pageSize);

	// get Definition of the Exporter where allowable conditions and exported columns are specified
	CarIQExporterDefinition getExporterDefinition();

	// get HTML Value Renderer for rendering values - hyperlink/background color/font etc.
	HtmlValueRenderer getHtmlRenderer();

	// Check if the current user has authority to this exporter
	boolean isAuthorised();

	// Return list of exporters that are part of this
	List<CarIQExporter> getChildExporters();
	
	CountPOJO getExportCount(String queryType, GenericJSON json);
	
	List<GenericJSON> exportRecords(String queryType, GenericJSON json, int startIndex, int endIndex);
	
	String getShortName();


}
