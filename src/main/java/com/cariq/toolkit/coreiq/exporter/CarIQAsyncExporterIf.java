/**
 * 
 */
package com.cariq.toolkit.coreiq.exporter;

import java.util.List;
import java.util.concurrent.Future;

import com.cariq.toolkit.utils.GenericJSON;


/**
 * @author hrishi
 *
 */
public interface CarIQAsyncExporterIf extends CarIQExporter {
	// Based on the condition (one of supported ones) and GenericJSON, get List of Generic JSONs.
	Future<List<GenericJSON>> exportAsync(final String queryType, final GenericJSON json, final int pageNo, final int pageSize);
}
