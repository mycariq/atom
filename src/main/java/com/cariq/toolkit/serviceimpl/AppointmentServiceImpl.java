package com.cariq.toolkit.serviceimpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atom.www.helper.AppointmentStatus;
import com.atom.www.helper.DomainHelper;
import com.atom.www.helper.TimeSlot;
import com.cariq.toolkit.model.Appointment;
import com.cariq.toolkit.model.Appointment.Appointment_In;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.Domain.Domain_Out;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.service.AppointmentService;
import com.cariq.toolkit.service.TimelineService;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * Appointment service implementation
 * 
 * @author sagar
 *
 */
@Service
public class AppointmentServiceImpl implements AppointmentService {

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("AppointmentServiceImpl");
	
	private static final String TIMELINE_TYPE = "appointment";
	
	@Autowired
	private TimelineService timelineService;
	
	@Override
	public ResponseWithIdJson create(Appointment_In appointmentIn) {
		logger.debug("createAppointment: " + appointmentIn.toString());
		try (ProfilePoint _createOrGet = ProfilePoint
				.profileActivity("ProfActivity_createOrGet" + this.getClass().getName())) {
			
			//validate proxy object and appointment date
			if(appointmentIn.getProxyObjectIn() == null || appointmentIn.getAppointmentOn() == null)
				Utils.handleException("Proxy object or appointment date cannot be empty.");
			
			//validate status of appointment
			if(!Utils.isValueIn(appointmentIn.getStatus(), AppointmentStatus.CREATED.name()))
				Utils.handleException("Invalid appointment status: " + appointmentIn.getStatus());
			
			User user = User.getByUsername(appointmentIn.getCreatedBy());
			Date date = Utils.convertToDate(appointmentIn.getAppointmentOn(), Utils.YYYY_mm_dd_hh_mm_ss);
			
			List<Appointment> appointmentsForCurrentSlot = 
					Appointment.getForSlot(TimeSlot.getByDate(date), user,
							DomainHelper.getDomainFromUser(user), 1, Integer.MAX_VALUE);
			
			if(appointmentsForCurrentSlot.size() > Utils.MAX_APPOINTMENTS_PER_SLOT) 
				Utils.handleException("Slot is currently full with appointments, Please select other slot.");
			
			//create appointment
			Appointment appointment = Appointment.createOrGet(appointmentIn);
			logger.debug("Appointment created with id " + appointment.getId());
			
			//Add appointment entry on timeline			
			String username = Utils.getCurrentUserName();
			String message = "Appointment scheduled at " + appointmentIn.getAppointmentOn()
								+ " by @" + username;
			ResponseWithIdJson timelineRes = timelineService.addSimpleTimeline(appointmentIn.getProxyObjectIn(),
					message, TIMELINE_TYPE, null, null, username);
			logger.debug(TIMELINE_TYPE + " type timeline entry added with id: " + timelineRes.getId());
			
			return new ResponseWithIdJson(String.valueOf(appointment.getId()), "Appointment created with id " + appointment.getId());
		}
	}

	@Override
	public ResponseWithIdJson update(Long appointmentId, Appointment_In appointmentIn) {
		logger.debug("updateAppointment: id = " + appointmentId + ", inJson = " + appointmentIn.toString());

		try (ProfilePoint _update = ProfilePoint.profileActivity("ProfActivity_update" + this.getClass().getName())) {
			Appointment appointment = Appointment.find(appointmentId);
			
			//if appointment with given id is not present then throw exception
			if (appointment == null)
				Utils.handleException("Could not find appointment with id " + appointmentId + " to update.");

			//validate input status
			if (!Utils.isValueIn(appointmentIn.getStatus(), AppointmentStatus.RESCHEDULED.name(),
					AppointmentStatus.COMPLETE.name()))
				Utils.handleException("Invalid appointment status: " + appointmentIn.getStatus());
			
			User user = User.getByUsername(appointmentIn.getCreatedBy());
			
			//Check if slot is full or not. If slot is full then reschedule is not allowed.
			if(!appointmentIn.getStatus().equalsIgnoreCase(AppointmentStatus.COMPLETE.name())) {
				
				Date date = Utils.convertToDate(appointmentIn.getAppointmentOn(), Utils.YYYY_mm_dd_hh_mm_ss);
				List<Appointment> appointmentsForCurrentSlot = 
						Appointment.getForSlot(TimeSlot.getByDate(date), user,
								DomainHelper.getDomainFromUser(user), 1, Integer.MAX_VALUE);
				
				if(appointmentsForCurrentSlot.size() > Utils.MAX_APPOINTMENTS_PER_SLOT) 
					Utils.handleException("Slot is currently full with appointments, Please select other slot.");
			}

			//If Appointment is about to complete, then assign existing date. else, update date
			//for reschedule purpose.
			Date date = appointmentIn.getStatus().equalsIgnoreCase(AppointmentStatus.COMPLETE.name()) ?
					appointment.getAppointmentOn() : Utils.convertToDate(appointmentIn.getAppointmentOn(), Utils.YYYY_mm_dd_hh_mm_ss);
					
			//finally update details
			appointment.setAppointmentOn(date);
			appointment.setStatus(appointmentIn.getStatus());
			appointment.setModifiedOn(new Date());
			
			
			appointment.setCreatedBy(user);
			appointment.setCreationStage(appointmentIn.getCreationStage());
			
			String message = "Appointment with id " + appointmentId + " updated with status " + appointmentIn.getStatus()
			+ ", and appointmentOn " + appointmentIn.getAppointmentOn();
			logger.debug(message);
			
			//Add timeline entry 
			ResponseWithIdJson timelineRes = timelineService.addSimpleTimeline(appointmentIn.getProxyObjectIn(),
					message, TIMELINE_TYPE, null, null, Utils.getCurrentUserName());
			logger.debug(TIMELINE_TYPE + " type timeline entry added with id: " + timelineRes.getId());
			
			//merge new updated values
			appointment = appointment.merge();
			return new ResponseWithIdJson(String.valueOf(appointment.getId()), message);
		}
	}

}
