package com.cariq.toolkit.serviceimpl.async;

import org.json.JSONException;
import org.json.JSONObject;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.model.async.CarIqJob;
import com.cariq.toolkit.service.async.WorkItemUpdater;

/**
 * WorkItem Worker is implemented in terms of Quartz Job This is a stateless
 * entity - loaded from the String at Runtime using reflection This framework is
 * quite reliable to do multi-threaded operation
 * 
 * @author Abhijit
 *
 */
public class CarIQJobExecutor implements Job {
	public static final String JOBCLASS = "JobClass";
	public static final String INPUT_JSON = "InputJSON";
	public static final String UPDATER = "Updater";
	CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("CarIQJobExecutor");

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// try (ProfilePoint _jobExecutorWorker_execute = ProfilePoint
		// .profileActivity("ProfActivity_jobExecutorWorker_execute")) {
		JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
		String jobClassJSon = (String) jobDataMap.get(INPUT_JSON);
		WorkItemUpdater updater = (WorkItemUpdater) jobDataMap.get(UPDATER);
		int jobFrequency = 30; // just a default
		CarIqJobWorker job = null;
		try {
			// Get the CarIqJob to be executed.
			// This is mainly to reuse existing code.
			// The "Beanness" and other properties of the Job are not in use
			job = getCarIQJobWorker(jobClassJSon);
			if (job == null)
				Utils.handleException("Invalid Job: " + jobClassJSon);
			// execute is the polymorphic method implemented in the CarIqJob
			// class derivatives

			// switch off job execution
			jobFrequency = unsetFrequency(job);

			job.execute(context);
			updater.complete();

			// Switch On - use Constructor-destructor pattern later
			resetFrequency(job, jobFrequency);

		} catch (Exception e) {
			Utils.logException(logger, e, "Could not execute job: " + jobClassJSon);
			updater.fail(Utils.getExceptionMessage(e));
			Utils.emailException(e);
			throw new JobExecutionException(e);
		} finally {
			if (job != null) // make sure the frequency is back to normal
				resetFrequency(job, jobFrequency);
		}
	}

	/**
	 * Get the CarIqJob instance from the JSon identifying its class
	 * 
	 * @param jsonString
	 * @return
	 * @throws JSONException
	 * @throws ClassNotFoundException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	@SuppressWarnings("unchecked")
	private CarIqJobWorker getCarIQJobWorker(String jsonString)
			throws JSONException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		JSONObject jsonObj = new JSONObject(jsonString);
		String jobClassName = (String) jsonObj.get(JOBCLASS);
		Class<? extends CarIqJobWorker> jobClass = (Class<? extends CarIqJobWorker>) Class.forName(jobClassName);

		return jobClass.newInstance();
	}

	/**
	 * @param context
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	private int unsetFrequency(CarIqJobWorker jobWorker) {
		String jobName = jobWorker.getClass().getName();
		logger.debug("+=+=+= Unsetting Job Frequency of " + jobName);

		// update job with this class name to make frequency -1
		CarIqJob job = CarIqJob.findJobByJobName(jobName);
		if (job == null)
			Utils.handleException("Invalid Job: " + jobName);

		try {
			// Get OriginalFrequency of the job
			int originalFrequency = job.getFrequency();
			job.setFrequency(CarIqJob.JOB_FREQUENCY_NONE);
			job.merge();
			return originalFrequency;
		} catch (Exception e) {
			Utils.logException(logger, e, "Switching off job: " + jobName);
		}

		return CarIqJob.JOB_FREQUENCY_NONE;
	}

	/**
	 * @param context
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	private void resetFrequency(CarIqJobWorker jobWorker, int jobFrequency) {
		String jobName = jobWorker.getClass().getName();
		logger.debug("+=+=+= Resetting Job Frequency of " + jobName + " to: " + jobFrequency);

		// update job with this class name to make frequency -1
		CarIqJob job = CarIqJob.findJobByJobName(jobName);
		if (job == null)
			Utils.handleException("Invalid Job: " + jobName);

		try {
			// Set OriginalFrequency of the job
			job.setFrequency(jobFrequency);
		} catch (Exception e) {
			Utils.logException(logger, e, "Switching ON job: " + jobName + " to frequency: " + jobFrequency);
		}
	}

}
