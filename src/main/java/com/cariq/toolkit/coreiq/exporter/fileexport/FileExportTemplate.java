/**
 * 
 */
package com.cariq.toolkit.coreiq.exporter.fileexport;

import java.io.File;

import com.cariq.toolkit.coreiq.exporter.CarIQExporter;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterRegistry;
import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * @author hrishi
 *
 */
public abstract class FileExportTemplate implements FileExport {
	CarIQExporter exporter;
	String queryType;
	GenericJSON inputJSON;
	String fileFormat;

	/**
	 * @param exporter
	 * @param queryType
	 * @param inputJSON
	 * @param fileFormat
	 */
	public FileExportTemplate(CarIQExporter exporter, String queryType, GenericJSON inputJSON, String fileFormat) {
		this.exporter = exporter;
		this.queryType = queryType;
		this.inputJSON = inputJSON;
		this.fileFormat = fileFormat;
	}

	@Override
	public CarIQExporter getExporter() {
		return exporter;
	}

	@Override
	public String getQueryType() {
		return queryType;
	}

	@Override
	public GenericJSON getInputJSON() {
		return inputJSON;
	}

	@Override
	public String getFileFormat() {
		return fileFormat;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.exporter.fileexport.FileExport#execute()
	 */
	@Override
	public File execute() {
		try (ProfilePoint _FileExport_execute = ProfilePoint.profileAction("ProfAction_FileExport_execute")) {
			return doExecute();
		}
	}

	/**
	 * 
	 */
	protected abstract File doExecute();

	/**
	 * @param exporterName
	 * @return
	 */
	private static boolean isMultiComposite(CarIQExporter exporter) {
		return exporter.getChildExporters().size() > 1;
	}

	/**
	 * Create a polymorphic object to export file in particular format
	 * 
	 * @param exporterName
	 * @param queryType2
	 * @param json
	 * @param fileType
	 * @return
	 */
	public static FileExport getFileExport(String exporterName, String queryType, GenericJSON json, String fileType) {
		CarIQExporter exporter = CarIQExporterRegistry.getExporter(exporterName);
		Utils.checkNotNull(exporter, "Exporter with name: " + exporterName + " not Found!!");

		// initialize fileType
		String fType = determineFileType(fileType);

		// if composite, determine fileType
		if (isMultiComposite(exporter))
			fType = determineZipFileType(fileType);

		switch (fType) {
		case CarIQFileUtils.CSV:
			return new CSVFileExport(exporter, queryType, json);

		case CarIQFileUtils.HTML:
			return new HtmlFileExport(exporter, queryType, json);

		case CarIQFileUtils.EXCEL:
			return new ExcelFileExport(exporter, queryType, json);

		case CarIQFileUtils.ZIP:
		case CarIQFileUtils.ZIP_CSV:
		case CarIQFileUtils.ZIP_HTML:
			return new ZipFileExport(exporter, queryType, json, fType);

		default:
			break;
		}

		// TODO Auto-generated method stub
		Utils.handleException("Invalid file type: " + fileType);
		return null;
	}

	/**
	 * @param fileType
	 * @return
	 */
	private static String determineZipFileType(String fileType) {
		// based on the input parameter - determine in which format to return
		if (fileType.equalsIgnoreCase(CarIQFileUtils.CSV))
			return CarIQFileUtils.ZIP_CSV;

		if (fileType.toUpperCase().contains(CarIQFileUtils.HTML.toUpperCase()))
			return CarIQFileUtils.ZIP_HTML;

		return determineFileType(fileType);
	}

	/**
	 * @param fileType
	 * @return
	 */
	private static String determineFileType(String fileType) {
		// formalize the filetype within the given ones

		// first check for ZIP
		if (fileType.equalsIgnoreCase(CarIQFileUtils.ZIP))
			return CarIQFileUtils.ZIP_CSV;

		if (fileType.equalsIgnoreCase(CarIQFileUtils.ZIP_CSV))
			return CarIQFileUtils.ZIP_CSV;

		if (fileType.toUpperCase().contains(CarIQFileUtils.ZIP_HTML.toUpperCase()))
			return CarIQFileUtils.ZIP_HTML;

		// Check for basic types
		if (fileType.equalsIgnoreCase(CarIQFileUtils.CSV))
			return CarIQFileUtils.CSV;

		if (fileType.toUpperCase().contains(CarIQFileUtils.HTML.toUpperCase()))
			return CarIQFileUtils.HTML;

		if (fileType.equalsIgnoreCase(CarIQFileUtils.EXCEL))
			return CarIQFileUtils.EXCEL;

		Utils.handleException("Invalid file type: " + fileType);
		return fileType;
	}
}
