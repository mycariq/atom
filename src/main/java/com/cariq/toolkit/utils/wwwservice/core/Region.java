package com.cariq.toolkit.utils.wwwservice.core;

import com.cariq.toolkit.utils.wwwservice.core.GeoTile.Direction;
import com.cariq.toolkit.utils.wwwservice.core.GeoTile.GeoTileJson;

/**
 * Region identifies the (Non-Euclidian) Rectangle bounded by corner GeoTiles
 * 
 * @author HVN
 *
 */
public class Region implements RegionInterface {
	public static final double EPSILON_AREA = 0.01*0.01; // less than 10 meter x 10  meter 
	GeoTile lowerLeft, upperRight;

	/**
	 * Constructor taking 2 corners of the Region
	 * 
	 * @param corner1
	 * @param corner2
	 */
	public Region(GeoTile corner1, GeoTile corner2) {
		super();
		lowerLeft = getLowerLeft(corner1, corner2);
		upperRight = getUpperRight(corner1, corner2);
	}
	
	/**
	 *  Constructor to define region with center point
	 */
	public Region(GeoTile center, double size){
		super();
		upperRight = center.getTile(Direction.NORTH, size);
		lowerLeft = center.getTile(Direction.SOUTH, size);
	}

	/**
	 * @return the lowerLeft
	 */
	@Override
	public GeoTile getLowerLeft() {
		return lowerLeft;
	}

	/**
	 * @return the upperRight
	 */
	@Override
	public GeoTile getUpperRight() {
		return upperRight;
	}

	/**
	 * Check if Tile is inside the region
	 * 
	 * @param tile
	 * @return
	 */
	@Override
	public boolean isInside(GeoTile tile) {
		if ((lowerLeft.getLatitude() > tile.getLatitude())
				|| (lowerLeft.getLongitude() > tile.getLongitude())
				|| (upperRight.getLatitude() < tile.getLatitude())
				|| (upperRight.getLongitude() < tile.getLongitude()))
			return false;

		return true;
	}

	/**
	 * Find are of the region in Square Kilometers
	 * 
	 * @return
	 */
	@Override
	public double getArea() {
		GeoTile lowerRight = getLowerRight(lowerLeft, upperRight);
		double x = lowerLeft.getDistanceFrom(lowerRight);
		double y = lowerRight.getDistanceFrom(upperRight);

		return x * y;
	}

	/**
	 * Get Span of the region in horizontal direction on map i.e. EastWest
	 * 
	 * @return
	 */
	@Override
	public double getEastWestSpan() {
		GeoTile lowerRight = getLowerRight(lowerLeft, upperRight);
		return lowerLeft.getDistanceFrom(lowerRight);
	}

	/**
	 * Get Span of the region in vertical direction on map i.e. NorthSouth
	 * 
	 * @return
	 */
	@Override
	public double getNorthSouthSpan() {
		GeoTile lowerRight = getLowerRight(lowerLeft, upperRight);
		return lowerRight.getDistanceFrom(upperRight);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Region [lowerLeft=" + lowerLeft + ", upperRight=" + upperRight
				+ "]";
	}

	/**
	 * From two corners, find what is upper Right corner of Rectangle
	 * 
	 * @param corner1
	 * @param corner2
	 * @return
	 */
	public static GeoTile getUpperRight(GeoTile corner1, GeoTile corner2) {
		double corner1Lat = corner1.getLatitude();
		double corner2Lat = corner2.getLatitude();
		double corner1Long = corner1.getLongitude();
		double corner2Long = corner2.getLongitude();

		return new GeoTile(Math.max(corner1Lat, corner2Lat), Math.max(
				corner1Long, corner2Long));

	}

	/**
	 * From two corners, find what is Lower Left corner of Rectangle
	 * 
	 * @param corner1
	 * @param corner2
	 * @return
	 */
	public static GeoTile getLowerLeft(GeoTile corner1, GeoTile corner2) {
		double corner1Lat = corner1.getLatitude();
		double corner2Lat = corner2.getLatitude();
		double corner1Long = corner1.getLongitude();
		double corner2Long = corner2.getLongitude();

		return new GeoTile(Math.min(corner1Lat, corner2Lat), Math.min(
				corner1Long, corner2Long));

	}

	/**
	 * From two corners, find what is UpperLeft corner of Rectangle
	 * 
	 * @param corner1
	 * @param corner2
	 * @return
	 */
	public static GeoTile getUpperLeft(GeoTile corner1, GeoTile corner2) {
		double corner1Lat = corner1.getLatitude();
		double corner2Lat = corner2.getLatitude();
		double corner1Long = corner1.getLongitude();
		double corner2Long = corner2.getLongitude();

		return new GeoTile(Math.max(corner1Lat, corner2Lat), Math.min(
				corner1Long, corner2Long));

	}

	/**
	 * From two corners, find what is LowerRight corner of Rectangle
	 * 
	 * @param corner1
	 * @param corner2
	 * @return
	 */
	public static GeoTile getLowerRight(GeoTile corner1, GeoTile corner2) {
		double corner1Lat = corner1.getLatitude();
		double corner2Lat = corner2.getLatitude();
		double corner1Long = corner1.getLongitude();
		double corner2Long = corner2.getLongitude();

		return new GeoTile(Math.min(corner1Lat, corner2Lat), Math.max(
				corner1Long, corner2Long));

	}

	public static class RegionJson {
		public GeoTileJson getLowerLeft() {
			return lowerLeft;
		}
		public void setLowerLeft(GeoTileJson lowerLeft) {
			this.lowerLeft = lowerLeft;
		}
		public GeoTileJson getUpperRight() {
			return upperRight;
		}
		public void setUpperRight(GeoTileJson upperRight) {
			this.upperRight = upperRight;
		}
		GeoTileJson lowerLeft;
		GeoTileJson upperRight;
	}

	public static Region fromJson(RegionJson region) {
		// GeoTileJson lowerLeft;
		// GeoTileJson upperRight;
		return new Region(GeoTile.fromJson(region.lowerLeft),
				GeoTile.fromJson(region.upperRight));
	}

	public RegionJson toJson() {
		RegionJson regionJson = new RegionJson();
		regionJson.lowerLeft = this.lowerLeft.toJson();
		regionJson.upperRight = this.upperRight.toJson();
		return regionJson;
	}
}