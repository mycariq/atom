/**
 * 
 */
package com.cariq.toolkit.utils;

/**
 * @author hrishi
 *
 */
public class SimplePOJOs {
	public static class CountPOJO {
		Integer count;

		/**
		 * 
		 */
		public CountPOJO() {
			super();
			// TODO Auto-generated constructor stub
		}

		/**
		 * @param count
		 */
		public CountPOJO(Integer count) {
			this.count = count;
		}

		public Integer getCount() {
			return count;
		}

		public void setCount(Integer count) {
			this.count = count;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((count == null) ? 0 : count.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			CountPOJO other = (CountPOJO) obj;
			if (count == null) {
				if (other.count != null)
					return false;
			} else if (!count.equals(other.count))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "CountPOJO [count=" + count + "]";
		}

	}

}
