package com.cariq.toolkit.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.validator.UrlValidator;

import com.cariq.toolkit.utils.analytics.core.AnalyticsContext;
import com.cariq.toolkit.utils.analytics.core.Record;
import com.cariq.toolkit.utils.analytics.core.RecordProcessorTemplate;
import com.cariq.toolkit.utils.analytics.core.TableProcessor;
import com.cariq.toolkit.utils.analytics.core.TableProcessorImpl;
import com.cariq.toolkit.utils.profile.ProfilePoint;

public class CarIQFileUtils {
	public static final String CSV = "csv";
	public static final String HTML = "htm";
	public static final String EXCEL = "xls";
	public static final String ZIP = "zip"; // zip file (containing csv file(s))
	public static final String ZIP_CSV = "zip-csv"; // zip file (containing csv file(s))
	public static final String ZIP_HTML = "zip-htm"; // zip file (containing html file(s))


	public static final String SLASH = "/";
	
	public static String saveToFile(String reportFile, String text) throws IOException {
		File fout = new File(reportFile);
		FileOutputStream fos = new FileOutputStream(fout, true);
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new OutputStreamWriter(fos));
			bw.write(text);
		} finally {
			// close the handle
			if (null != bw) {
				bw.flush();
				bw.close();
			}

			if (null != fos)
				fos.close();
		}

		return reportFile;
	}
	
	public static String getTempFolder() {
		return System.getProperty("java.io.tmpdir") + File.separator;
	}

	public static String getTempFile(String fileName) {
		return System.getProperty("java.io.tmpdir") + File.separator + fileName;
	}
	
	public static String getTempFile(String rootFolder, Date date, String fileName) {
		String datecode = Utils.toTimeString(date, null, "yyyyMMdd");
		return getTempFile(rootFolder + File.separator + datecode, fileName);
	}
	
	public static String getTempFile(String folder, String fileName) {
		// create directory if not exists
		createOrGetFolder(folder);
		return folder + File.separator + fileName;
	}

	/**
	 * @param folder
	 */
	public static void createOrGetFolder(String folder) {
		try {
			File directory = new File(folder);
			if (! directory.exists()) {
			    directory.mkdirs();
			    setWritable(folder);
			}
		} catch (Exception e) {
			Utils.handleException(e);
		}
	}

	public static void copyContents(String outputFile, String inputFile) throws IOException {
		InputStream input = new FileInputStream(inputFile);
		OutputStream output = new FileOutputStream(outputFile);
		copyContents(output, input);

	}

	public static void copyContents(OutputStream output, InputStream input) throws IOException {
		byte[] buf = new byte[1024];
		int bytesRead;

		while ((bytesRead = input.read(buf)) > 0) {
			output.write(buf, 0, bytesRead);
		}
	}

	public static void copyContents(OutputStream output, String inputFile) throws IOException {
		InputStream input = new FileInputStream(inputFile);
		try {
			copyContents(output, input);
		} finally {
			if (input != null)
				input.close();
		}
	}

	public static void copyContents(String outputFile, InputStream input) throws IOException {
		OutputStream output = new FileOutputStream(outputFile);
		try {
			copyContents(output, input);
		} finally {
			output.close();
		}
	}

	public static long getFileSize(String filePath) {
		File f = new File(filePath);
		return f.length();
	}

	public static void attachCSVFileToResponse(HttpServletResponse response, String outputFilePath) throws Exception {
		// set content description
		attachFileToResponse(response, outputFilePath, "text/csv");
	}

	public static void attachHTMLFileToResponse(HttpServletResponse response, String outputFilePath) throws Exception {
		// set content description
		attachFileToResponse(response, outputFilePath, "text/html");
	}
	
	public static void attachZIPFileToResponse(HttpServletResponse response, String outputFilePath) throws Exception {
		// set content description
		attachFileToResponse(response, outputFilePath, "application/zip");
	}

	public static void attachFileToResponse(HttpServletResponse response, String outputFilePath, String mimeType)
			throws Exception {
		File toBeCopied = new File(outputFilePath);
		if (mimeType == null)
			mimeType = URLConnection.guessContentTypeFromName(toBeCopied.getName());

		String contentDisposition = String.format("attachment; filename=\"%s\"", toBeCopied.getName());
		int fileSize = Long.valueOf(toBeCopied.length()).intValue();

		response.setContentType(mimeType);
		response.setHeader("Content-Disposition", contentDisposition);
		response.setContentLength(fileSize);
		try (OutputStream out = response.getOutputStream()) {
			Path path = toBeCopied.toPath();
			Files.copy(path, out);
			out.flush();
		}
	}

	public static void deleteLocalFile(String fileName) {
		File f = new File(fileName);
		if (f.exists()) {
			//System.out.println("Deleting file: " + fileName);
			f.delete();
		}
	}

	public static class CarIQFileHolder implements AutoCloseable {
		String localFile;

		public CarIQFileHolder() {
			super();
			localFile = CarIQFileUtils.getTempFile("_tmp_" + UUID.randomUUID());
		}

		public CarIQFileHolder(InputStream inputStream) {
			super();
			localFile = CarIQFileUtils.getTempFile("_tmp_" + UUID.randomUUID());
			try {
				CarIQFileUtils.copyContents(localFile, inputStream);
			} catch (IOException e) {
				Utils.handleException(e);
			}
		}

		public String getFile() {
			return localFile;
		}

		@Override
		public void close() {
			CarIQFileUtils.deleteLocalFile(localFile);
		}
	}

	public static boolean exists(String csvFile) {
		File f = new File(csvFile);
		return f.exists();
	}

	/**
	 * @param rootPath
	 * @param fileName
	 * @return
	 */
	public static String getPath(String rootPath, String fileName) {
		if (!rootPath.endsWith(SLASH))
			return rootPath + SLASH + fileName;
		
		return rootPath + fileName;
	}
	
	/**
	 * The file created by application - make it readable
	 * @param filePath
	 */
	public static void setReadable(String filePath) {
		if (!exists(filePath))
			Utils.handleException("Chmod: File does not exist: " + filePath);
			
		File f = new File(filePath);
		f.setReadable(true, false);	
	}
	
	/**
	 * File Created by application, make it writable
	 * @param filePath
	 */
	public static void setWritable(String filePath) {
		if (!exists(filePath))
			Utils.handleException("Chmod: File does not exist: " + filePath);
			
		File f = new File(filePath);
		f.setWritable(true, false);	
	}
	
	/**
	 * Gets the URL file contents.
	 *
	 * @param urlPath the url path
	 * @return the URL file contents
	 */
	public static String getURLFileContents(String urlPath) {
		try (ProfilePoint _getURLFileContents = ProfilePoint.profileAction("ProfAction_getURLFileContents")) {

			String fileContents = null;
			try {
				UrlValidator urlValidator = new UrlValidator();
				if (!urlValidator.isValid(urlPath))
					Utils.handleException(urlPath + " not valid URL");

				URL url = new URL(urlPath);
				Utils.checkNotNull(url, urlPath + " is URL template not found!");

				// create a url connection object
				URLConnection urlConnection = url.openConnection();
				Utils.checkNotNull(urlConnection, "Could not open URL connection.");

				InputStream inputStream = urlConnection.getInputStream();
				
				// @TODO - mismatch between CarIQ POM and ATOM POM
				
//				fileContents = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
				fileContents = IOUtils.toString(inputStream);
			} catch (Exception e) {
				Utils.handleException(e.getMessage());
			}

			return fileContents;
		}
	}
	
	/**
	 * Copies the URL file contents to local file path
	 *
	 * @param urlPath the url path
	 */
	public static void copyURLFileContents(String urlPath, String localFilePath) {
		try (ProfilePoint _copyURLFileContents = ProfilePoint.profileAction("ProfAction_copyURLFileContents")) {
			try {
				UrlValidator urlValidator = new UrlValidator();
				if (!urlValidator.isValid(urlPath))
					Utils.handleException(urlPath + " not valid URL");

				URL url = new URL(urlPath);
				Utils.checkNotNull(url, urlPath + " is URL template not found!");

				InputStream inputStream = url.openStream();
				FileOutputStream fileOS = new FileOutputStream(localFilePath);
				if (IOUtils.copy(inputStream, fileOS) < 0) {
					Utils.handleException("Failed to copy the URL content: " + urlPath + " to local file: " + localFilePath);
				}

			} catch (Exception e) {
				Utils.handleException(e.getMessage());
			}
		}
	}
	
	public static List<GenericJSON> convertCsvToJsonList(String localFilePath) {
		try (ProfilePoint _convertCsvToJsonList = ProfilePoint.profileAction("CarIQFileUtils_convertCsvToJsonList")) {
			List<GenericJSON> retVal = new ArrayList<GenericJSON>();
			AnalyticsContext ctx = new AnalyticsContext();
			try {
				executeBatchProcessor(ctx, localFilePath, "CsvToJson", new RecordProcessorTemplate(ctx) {
					@Override
					protected Record doProcess(Record record) {
						GenericJSON json = new GenericJSON();
						json.putAll(record.jsonize());
						retVal.add(json);
						return record;
					}
				});
			} catch (Exception e) {
				Utils.handleException(e);
			}
			return retVal;
		}
	}
	
	/**
	 * Execute batch processor.
	 *
	 * @param ctx the ctx
	 * @param inputFile the input file
	 * @param processId the process id
	 * @param recordProcessorTemplate the record processor template
	 * @throws Exception the exception
	 */
	private static void executeBatchProcessor(AnalyticsContext ctx, String inputFile, String processId,
			RecordProcessorTemplate recordProcessorTemplate) throws Exception {
		try (ProfilePoint _executeBatchProcessor = ProfilePoint.profileAction("CarIQFileUtils_executeBatchProcessor")) {
			// Process the File 
			TableProcessor processor = new TableProcessorImpl(ctx, processId, inputFile, null);

			processor.setForgiveException(false);
			processor.add(recordProcessorTemplate);
			processor.init();
			processor.process();
			processor.close();
		}
	}
	
	public static String getLocalFilePathFromURL(String fileURL) {
		try (ProfilePoint _getLocalFilePathFromURL = ProfilePoint
				.profileAction("CarIQFileUtils_getLocalFilePathFromURL")) {

			try {
				String tatFileName = fileURL.substring(fileURL.lastIndexOf('/') + 1);
				String zipFilePath = CarIQFileUtils.getTempFile(tatFileName);

				// Download the content from url to local zip file
				copyURLFileContents(fileURL, zipFilePath);
				if (!(exists(zipFilePath) && (getFileSize(zipFilePath) > 0)))
					Utils.handleException("Failed to download content from url " + fileURL);

				// Extract the zip file to csv
				CarIQZipFile zip = new CarIQZipFile(zipFilePath, CarIQZipFile.Mode.EXTRACT);
				File csvFile = zip.extractFirst();
				if (null == csvFile)
					Utils.handleException("Could not open zip file : " + zipFilePath);
				return csvFile.getAbsolutePath();
			} catch (Exception e) {
				Utils.handleException(e);
			}

			return null;
		}
	}
}
