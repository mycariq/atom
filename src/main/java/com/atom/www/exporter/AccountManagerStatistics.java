package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.atom.www.helper.DomainHelper;
import com.atom.www.helper.TimeSlot;
import com.cariq.toolkit.coreiq.CarIQRawQuery;
import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.model.Appointment;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.Domain.Domain_Out;
import com.cariq.toolkit.model.StageStatistics;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.Day;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.Week;
import com.cariq.toolkit.utils.profile.ProfilePoint;


/**
 * Account manager weekly statistics. It gives day and stage wise details of 
 * account manager work. It presents count by each stage and date.
 * 
 * @author sagar
 *
 */
public class AccountManagerStatistics extends CarIQExporterTemplate {

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("AccountManagerWeeklyStatistics");
	
	public static final String TheExporter = "AccountManagerStatistics";
	private static final String TheDescription = "Account manager weekly statistics. "
			+ "It gives day and stage wise details of"
			+ " account manager work. It presents count by each stage and date.";
	
	public static final String QUERY_TYPE_BY_OWNER_AND_DAY = "ByOwnerAndDay";
	private static final String QUERY_TYPE_BY_OWNER_AND_DAY_DESCRIPTION = 
			"Get statistics for owner for given date";
	
	public static final String QUERY_TYPE_BY_OWNER = "ByOwner";
	private static final String QUERY_TYPE_BY_OWNER_DESCRIPTION = "Get statistics for owner";
	
	public static final String QUERY_TYPE_SPOTLIGHT = "SpotlightStats";
	public static final String QUERY_TYPE_SPOTLIGHT_DESCRIPTION = "Get statistics count for all spotlight.";
	
	public static final String ATTRIBUTE_OWNER = "owner";
	public static final String ATTRIBUTE_DATE = "date";
	public static final String ATTRIBUTE_DOMAIN = "domain";
	
	public static final String PreShipmentStages[] = { "Downloaded App", "Not Verified", "User Created"};
	public static final String PostShipmentStages[] = { "Device OnBoarded", "Device Delivered" };
	
	public static final String states[] = {"NOT_CONTACTABLE", "NOT_INTERESTED", "Invalid"};
	
	private static final String PreShipmentSpotlight = "PreShipmentSpotlight";
	private static final String PostShipmentSpotlight = "PostShipmentSpotlight";
	
	private static final LinkedHashMap<String, String> stages = new LinkedHashMap<>();
	
	private DomainHelper domainHelper;
	
	static {
		stages.put("entry_date", "Welcome Call");
		stages.put("verified_date", "App Not Downloaded");
		stages.put("device_requested_date", "Device Requested");
		stages.put("device_shipped_on_date", "Device Shipped");
		stages.put("device_delivered_on_date", "Device Delivered");
		stages.put("device_onboarded_date", "Device Onboarded");
		stages.put("car_connected_date", "Car Connected");
		stages.put("nc_date", "Not Contactable");
		stages.put("ni_date", "Not Interested");
	}
	
	/**
	 * Default constructor
	 */
	public AccountManagerStatistics() {
		super(TheExporter, TheDescription);
		
		domainHelper = new DomainHelper();
	
		//by owner and day query type details.
		addQueryType(QUERY_TYPE_BY_OWNER_AND_DAY, QUERY_TYPE_BY_OWNER_AND_DAY_DESCRIPTION,
				Arrays.asList(
						new QueryDefinitionAttribute(ATTRIBUTE_OWNER, "Owner or proxy", String.class),
						new QueryDefinitionAttribute(ATTRIBUTE_DATE, "Date of a week", Date.class),
						new QueryDefinitionAttribute(ATTRIBUTE_DOMAIN, "Name of domain", String.class)));
		
		//by owner
		addQueryType(QUERY_TYPE_BY_OWNER, QUERY_TYPE_BY_OWNER_DESCRIPTION,
				Arrays.asList(new QueryDefinitionAttribute(ATTRIBUTE_OWNER, "Owner username", String.class),
								new QueryDefinitionAttribute(ATTRIBUTE_DOMAIN, "Name of domain", String.class)));
		
		//by spotlight
		addQueryType(QUERY_TYPE_SPOTLIGHT, QUERY_TYPE_SPOTLIGHT_DESCRIPTION,
				Arrays.asList(new QueryDefinitionAttribute(ATTRIBUTE_OWNER, "Owner username", String.class),
						new QueryDefinitionAttribute(ATTRIBUTE_DOMAIN, "Name of domain", String.class)));
	}

	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		
		try (ProfilePoint _doExport = ProfilePoint
				.profileAction("ProfAction_doExport_" + this.getClass().getCanonicalName())) {

			List<GenericJSON> outputJSON = new ArrayList<GenericJSON>();
			
			// get input values
			String owner = Utils.getValue(json, ATTRIBUTE_OWNER);
			Date dateOfWeek = Utils.getValue(json, ATTRIBUTE_DATE) != null
					? Utils.getDate(Utils.getValue(json, ATTRIBUTE_DATE)) : null;

			String domainName = Utils.getValue(json, ATTRIBUTE_DOMAIN);
			User user = User.getByUsername(owner);
			Domain_Out domain = domainHelper.getDomainOfLoggedInUser(domainName);

			if (queryType.equalsIgnoreCase(QUERY_TYPE_BY_OWNER_AND_DAY)) {
				// if input values are invalid then throw exception.
				if (user == null || dateOfWeek == null)
					Utils.handleException("Owner or date cannot be empty!");
				// create a week which ends on input date. So week can find
				// statistics
				// for all week days before given input date.
				Week week = Week.getWeekEndingOn(new Day(dateOfWeek, null));

				/// get start time and end time of week
				String startTime = Utils.changeDateFormat(week.getStartTime(), Utils.YYYY_mm_dd_hh_mm_ss);
				String endTime = Utils.changeDateFormat(week.getEndTime(), Utils.YYYY_mm_dd_hh_mm_ss);

				// iterate on column values and execute query for each column
				for (String stage : stages.keySet()) {
					// TODO consider enhancing list by adding missing days(for
					// this week) in list
					List<GenericJSON> statistics = getStatisticsByDateAndOwner(user.getId(), stage, startTime, endTime);
					List<GenericJSON> stagefullStatistics = addStage(stages.get(stage), statistics);
					outputJSON.addAll(stagefullStatistics);
				}
			} else if(queryType.equalsIgnoreCase(QUERY_TYPE_BY_OWNER)) {
				// if input values are invalid then throw exception.
				if (user == null)
					Utils.handleException("Owner cannot be empty!");
				//get statistics by owner
				List<GenericJSON> byOwner = getByOwner(user);
				
				//check valid list.
				if(!Utils.isNullOrEmpty(byOwner))
					outputJSON.addAll(byOwner);
			} else if(queryType.equalsIgnoreCase(QUERY_TYPE_SPOTLIGHT)) {
				if (user == null)
					Utils.handleException("Owner or date cannot be empty!");
				outputJSON.addAll(getSpotlightStatistics(user, Domain.getByName(domain.getName())));
			} else {
				Utils.handleException("Invalid query type");
			}

			return outputJSON;
		}
	}


	/**
	 * Add stage to all db values
	 * 
	 * @param stage
	 * @param statistics
	 * @return
	 */
	private List<GenericJSON> addStage(String stage, List<GenericJSON> statistics) {
		List<GenericJSON> retValues = new ArrayList<>();
		
		if(statistics == null)
			return retValues;
		for(GenericJSON json : statistics) {
			json.put("stage", stage);
			retValues.add(json);
		}
		return retValues;
	}

	/**
	 * Get Statistics by date and owner
	 * 
	 * @param ownerId		Owner db id
	 * @param dateColumn	database column name
	 * @param startTime		start time 
	 * @param endTime		end time
	 * @return
	 */
	public static List<GenericJSON> getStatisticsByDateAndOwner(Long ownerId, String dateColumn, String startTime,
			String endTime) {
		logger.debug("getStatisticsByDateAndOwner: ownerId: " + ownerId + ", dateColumn: " + dateColumn
				+ ", startTime: " + startTime + ", endTime: " + endTime);

		try (ProfilePoint _getStatisticsByDateAndOwner = ProfilePoint
				.profileAction("ProfAction_getStatisticsByDateAndOwner_AccountManagerWeeklyStatistics")) {

			LinkedHashMap<String, String> attributeAliases = new LinkedHashMap<String, String>();
			List<String> tableAliasArray = new ArrayList<String>();
			
			//add attribute alias
			attributeAliases.put("date(" + dateColumn + ")", "date");
			attributeAliases.put("count(*)", "count");
			
			//add table alias
			tableAliasArray.add("stage_statistics ss");
			
			//Create a query and add conditions.
			CarIQRawQuery qry = new CarIQRawQuery("getAccountManagerStatistics_" + dateColumn,
					StageStatistics.entityManager(), attributeAliases, tableAliasArray);
			qry.addRawCondition("owner = " + ownerId);
			qry.addRawCondition(dateColumn + " >= '" + startTime + "'");
			qry.addRawCondition(dateColumn + " <= '" + endTime + "'");
			qry.addGroupBy("date(" + dateColumn + ")");
			
			qry.orderBy(dateColumn, "asc");

			return qry.getResult();
		}
	}
	
	/**
	 * Get Spotlight statistics.
	 * 
	 * @param user
	 * @return
	 */
	private Collection<? extends GenericJSON> getSpotlightStatistics(User user, Domain domain) {
		List<GenericJSON> statistics = new ArrayList<>();

		//Get stats for pre-shipment stages
		String commaSepPreShipmentStages= Utils.getCommaSeperatedString(Arrays.asList(PreShipmentStages));
		List<StageStatistics> preShipStats = executeMultiStageCountQuery(user, commaSepPreShipmentStages, PreShipmentSpotlight);

		//Get stats for post-shipment stages
		String commaSepPostShipmentStages = Utils.getCommaSeperatedString(Arrays.asList(PostShipmentStages));
		List<StageStatistics> postShipStats = executeMultiStageCountQuery(user, commaSepPostShipmentStages, PostShipmentSpotlight);
		
		//TODO When old data imported(Last year data) into stage statistics then 
		//TODO uncomment below code and modify some things to make it work.
		
//		//Get Renewal statistics.
//		List<StageStatistics> renewalStats = getRenewalStatistics(user, domain);
//		
//		//Get retrieval statistics
//		List<StageStatistics> retrievalStats = getRetrievalStatistics(user, domain);
		
		//Get count for appointment
		List<Appointment> appointmentsToExclude = Appointment.getForUser(user, domain, 1, Integer.MAX_VALUE);
		
		List<Appointment> appointments = Appointment.getForSlotAndAllDelayed(TimeSlot.getCurrentSlot()
			                     , user, domain, 1, Integer.MAX_VALUE);
				
		
		GenericJSON appointmentCount = new GenericJSON();
		appointmentCount.add("stage", "AppointmentSpotlight");
		appointmentCount.add("count", Utils.isNullOrEmpty(appointments) ? 0 : appointments.size());
		
		//Find number of pre-ship count excluding number of appointments.
		GenericJSON preShipmentCount = getCount(preShipStats, appointmentsToExclude, PreShipmentSpotlight);
		
		//Find number of post-ship count excluding number of appointments.
		GenericJSON postShipmentCount = getCount(postShipStats, appointmentsToExclude, PostShipmentSpotlight);
		
		//Overall spotlight count. Sum of all above.
		GenericJSON overallCount = new GenericJSON();
		int preCount = (Integer) preShipmentCount.get("count");
		int postCount = (Integer) postShipmentCount.get("count");
		int appCount = (Integer) appointmentCount.get("count");
		int ovCount = preCount + postCount + appCount;
		
		overallCount.add("stage", "OverallSpotlight");
		overallCount.add("count", ovCount);
		
		//Finally add all counts in list
		statistics.add(preShipmentCount);
		statistics.add(postShipmentCount);
		statistics.add(appointmentCount);
		statistics.add(overallCount);
		
		return statistics;
	}
	
	
//	/**
//	 * Get retrieval statistics.
//	 * 
//	 * @param user
//	 * @param domain
//	 * @return
//	 */
//	private List<StageStatistics> getRetrievalStatistics(User user, Domain domain) {
//		CarIQSimpleQuery<StageStatistics> query = 
//				new CarIQSimpleQuery<>("getRetrievalStatistics", StageStatistics.entityManager(), StageStatistics.class);
//		query.addCondition("owner", "=", user);
//		query.addCondition("domain", "=", domain);
//		query.addCondition("next_action", "=", "Due for retrieval");
//		query.addRawCondition("state not in ('READY_TO_RETRIEVE','GOT_DEVICE_BACK', 'LOST_DEVICE')");
//		
//		return query.getResultList();
//	}
//
//	/**
//	 * Get renewal statistics.
//	 * 
//	 * @param user
//	 * @param domain
//	 * @return
//	 */
//	private List<StageStatistics> getRenewalStatistics(User user, Domain domain) {
//		CarIQSimpleQuery<StageStatistics> query = 
//				new CarIQSimpleQuery<>("getRenewalStatistics", StageStatistics.entityManager(), StageStatistics.class);
//		query.addCondition("owner", "=", user);
//		query.addCondition("domain", "=", domain);
//		
//		return query.getResultList();
//	}

	/**
	 * Get count of given statistics excluding appointments
	 * 
	 * @param stats
	 * @param appointments
	 * @param spotlightName
	 * @return
	 */
	public GenericJSON getCount(List<StageStatistics> stats, List<Appointment> appointments, String spotlightName) {
		List<String> appointmentSignatures = Appointment.getSignatures(appointments);
		List<String> statsSignatures = StageStatistics.getSignatures(stats);
		
		statsSignatures.removeAll(appointmentSignatures);
		
		GenericJSON json = new GenericJSON();
		json.add("count", Utils.isNullOrEmpty(statsSignatures) ? 0 : statsSignatures.size());
		json.add("stage", spotlightName);
		
		return json;
	}
	
	
	/**
	 * Execute query for given spotlight with given list of stages.
	 *
	 * @param owner the owner
	 * @param commaSeperatedStages the comma seperated stages
	 * @param spotlightName the spotlight name
	 * @return the list
	 */
	private List<StageStatistics> executeMultiStageCountQuery(User owner, String commaSeperatedStages, String spotlightName) {
		
		CarIQSimpleQuery<StageStatistics> query = 
				new CarIQSimpleQuery<>(spotlightName, StageStatistics.entityManager(), StageStatistics.class);
		query.addCondition("owner", "=", owner);
		query.addRawCondition("stage IN " + Utils.getCommaSeperatedValuesForQuery(commaSeperatedStages));
		query.addRawCondition("(state = 'Active' or state = '' or state is null)");
		query.addRawCondition("(subscription_state = 'SubscriptionActive' or subscription_state is null)");
		
		return query.getResultList();
	}
	
	
	/**
	 * Find statistics on all items for given owner.
	 * 
	 * @param ownerId
	 * @return
	 */
	public static List<GenericJSON> getByOwner(User owner) {
		logger.debug("getByOwner: ownerId: " + owner.getId());

		try (ProfilePoint _getByOwner = ProfilePoint
				.profileAction("ProfAction_getByOwner_AccountManagerWeeklyStatistics")) {

			LinkedHashMap<String, String> attributeAliases = new LinkedHashMap<String, String>();
			List<String> tableAliasArray = new ArrayList<String>();
			
			//add attribute alias
			attributeAliases.put("stage", "stage");
			attributeAliases.put("count(*)", "count");
			tableAliasArray.add("stage_statistics ss");

			//Create a query and add conditions.
			CarIQRawQuery qry = new CarIQRawQuery("getByOwner",
					StageStatistics.entityManager(), attributeAliases, tableAliasArray);
			qry.addRawCondition("owner = " + owner.getId());
			qry.addGroupBy("stage");
			return qry.getResult();
		}
	}
}
