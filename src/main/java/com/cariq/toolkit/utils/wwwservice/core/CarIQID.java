package com.cariq.toolkit.utils.wwwservice.core;

import java.util.Date;

/**
 * CarIQID class represents primary key of all NOSQL DB It represents composite
 * of CarID (from RDBMS) and Date Epoch It is convenient to push and pull the
 * data in and out of the RDBMS tables
 * 
 * @author HVN
 *
 */
public class CarIQID {
	static int bitOffset = 36;
	static long EPOCH_MASK = Long.parseLong("FFFFFFFFF", 16);
	static Date MAX_DATE = fromEpoch(EPOCH_MASK);
	static long MAX_CARID = Long.parseLong("FFFFFFF", 16);

	long carid;
	long dateEpoch;

	/**
	 * Basic constructor from primary key
	 * 
	 * @param value
	 */
	public CarIQID(long value) {
		dateEpoch = value & EPOCH_MASK;
		carid = (value - dateEpoch) >> bitOffset;
	}

	/**
	 * Construct from carid and Date
	 * 
	 * @param carid
	 * @param date
	 */
	public CarIQID(long carid, Date date) {
		long dt = toEpoch(date);
		// long dt = date.getTime();
		if (carid > MAX_CARID)
			throw new RuntimeException("Invalid Carid: " + carid);

		if (dt > EPOCH_MASK)
			throw new RuntimeException("Invalid Date: " + date);

		this.carid = carid;
		this.dateEpoch = dt;
	}

	/**
	 * Date (time) of the record
	 * 
	 * @return
	 */
	public Date getDate() {
		return fromEpoch(dateEpoch);
	}

	/**
	 * Date in Epoch representation
	 * 
	 * @return
	 */
	public long getDateEpoch() {
		return dateEpoch;
	}

	/**
	 * ID of the car - from the RDBMS table - but not foreign key
	 * 
	 * @return
	 */
	public long getCarId() {
		return carid;
	}

	/**
	 * Is the other record for the same car
	 * 
	 * @param other
	 * @return
	 */
	public boolean isSameCar(CarIQID other) {
		return carid == other.getCarId();
	}

	/**
	 * Is other ID representing earlier time
	 * 
	 * @param other
	 * @return
	 */
	public boolean isBefore(CarIQID other) {
		return getDate().before(other.getDate());
	}

	/**
	 * Difference in seconds for 2 Ids (records)
	 * 
	 * @param other
	 * @return
	 */
	public long getTimeDifferenceInSeconds(CarIQID other) {
		return dateEpoch - other.getDateEpoch();
	}

	/**
	 * Add a few seconds and get a different ID
	 * 
	 * @param seconds
	 * @return
	 */
	public CarIQID plusSeconds(long seconds) {
		dateEpoch = Math.min(EPOCH_MASK, dateEpoch + seconds);
		return new CarIQID(getCarId(), fromEpoch(dateEpoch));
	}
	
	/**
	 * Add a few seconds and get a different ID
	 * 
	 * @param seconds
	 * @return
	 */
	public CarIQID minusSeconds(long seconds) {
		dateEpoch = Math.max(0, dateEpoch - seconds);
		return new CarIQID(getCarId(), fromEpoch(dateEpoch));
	}
	
	/**
	 * Add a few seconds and get a different ID
	 * 
	 * @param seconds
	 * @return
	 */
	public CarIQID plusMinutes(long minutes) {
		return plusSeconds(minutes*60);
	}
	
	/**
	 * Add a few seconds and get a different ID
	 * 
	 * @param seconds
	 * @return
	 */
	public CarIQID minusMinutes(long minutes) {
		return minusSeconds(minutes*60);
	}
	
	/**
	 * Static functions to Convert To and From Epoch And Date
	 * @param epochSeconds
	 * @return
	 */
	private static Date fromEpoch(long epochSeconds) {
		return new Date(epochSeconds * 1000);
	}
	
	private static long toEpoch(Date dt) {
		return dt.getTime()/1000;
	}
}
