package com.cariq.toolkit.service.report;

import java.util.List;

import com.cariq.toolkit.utils.PaginationEntityJson;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.serviceimpl.report.GetReportJson;

/**
 * The Interface ReportService.
 */
public interface ReportService {

	/**
	 * Gets the types.
	 *
	 * @return the types
	 */
	List<String> getTypes();

	ResponseJson createReport(String fromDate, String toDate, String type, String ownerType, String ownerId);

	PaginationEntityJson<List<GetReportJson>> getReports(String ownerType, String ownerId, String type, int pageNo, int pageSize);

	// only one having ownership can delete it.
	ResponseJson deleteReport(long reportId, String ownerType, String ownerId);

}
