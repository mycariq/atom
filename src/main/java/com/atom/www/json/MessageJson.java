package com.atom.www.json;


/**
 * The Class MessageJson.
 */
public class MessageJson {

	/** The message. */
	String message;

	/**
	 * Instantiates a new message json.
	 *
	 * @param message
	 *            the message
	 */
	public MessageJson(String message) {
		this.message = message;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
}