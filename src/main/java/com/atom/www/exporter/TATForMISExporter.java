package com.atom.www.exporter;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

public class TATForMISExporter extends StagingAndPolicyDetailsMiniReportExporter {
	
	public static final String TheExporter = "TATForMISExporter";
	private static final String TheExporterDescription = "TATReport to generate MIS Report";
	
	/**
	 * Default constructor
	 */
	public TATForMISExporter() {
		super(TheExporter, TheExporterDescription);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected GenericJSON getEnhancedObject(GenericJSON json, String queryType, GenericJSON inputJson) {
		GenericJSON exportedJSON = super.getEnhancedObject(json, queryType, inputJson);
		exportedJSON.remove(KEY_desposition);
		return exportedJSON;
	}
}
