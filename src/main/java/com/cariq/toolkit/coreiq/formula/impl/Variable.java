package com.cariq.toolkit.coreiq.formula.impl;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import com.cariq.toolkit.coreiq.formula.interfaces.VariableIfc;
import com.cariq.toolkit.coreiq.formula.interfaces.VariableResolverIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.CellValueAribiterIfc;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.GenericJsonable;
import com.cariq.toolkit.utils.Utils;

/**
 * A Basic Variable that can be resolved at runtime using resolver
 * @author hrishi
 *
 */
public class Variable implements VariableIfc, GenericJsonable {
	// Variable Name
	String name;
	GenericJSON valueDefinition;
	VariableResolverIfc variableResolver;
	
	// For performance optimization, cache the value
	Object resolvedValue = null;
	
	
	public Variable(String name, GenericJSON valueDefinition, VariableResolverIfc variableResolver) {
		super();
		this.name = name;
		this.valueDefinition = valueDefinition;
		this.variableResolver = variableResolver;
	}

	// required default constructor to load from input stream
	public Variable() {
	}

	public String getName() {
		return name;
	}

	public GenericJSON getValueDefinition() {
		return valueDefinition;
	}

	public VariableResolverIfc getVariableResolver() {
		return variableResolver;
	}

	@Override
	public Object resolve(Object context, boolean force) {
		if (!force && resolvedValue != null)
			return resolvedValue;
		
		resolvedValue = variableResolver.resolve(context, valueDefinition);
		// TODO Auto-generated method stub
		return resolvedValue;
	}

	@Override
	public GenericJSON toGenericJSON() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void fromGenricJSON(GenericJSON json) {
		name = Utils.getValue(String.class, json, "name");
		
		//TODO - not sure if it resolves - don't remember how it was done last time
		
		// load formula
		Object definitionJSONObj = json.get("definition");
		if (definitionJSONObj == null)
			Utils.handleException("Unable to read definition for Variable: " + name);
	
				
		valueDefinition = Utils.getJSonObject(Utils.getJSonString(definitionJSONObj), GenericJSON.class);
		
		String resolverClassName = Utils.getValue(String.class, json, "resolver");

		try {
			Class<?> indicatorClass = Class.forName(resolverClassName);
			variableResolver = Utils.downCast(VariableResolverIfc.class, indicatorClass.newInstance());

		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| SecurityException
				| ClassNotFoundException e) {
			Utils.handleException(e);
		}
		
	}
}
