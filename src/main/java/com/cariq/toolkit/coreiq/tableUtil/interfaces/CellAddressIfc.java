package com.cariq.toolkit.coreiq.tableUtil.interfaces;

public interface CellAddressIfc {

	NameIndexIdentifiable getReportIdentifier();

	NameIndexIdentifiable getTableIdentifier();

	NameIndexIdentifiable getRowIdentifier();

	NameIndexIdentifiable getColumnIdentifier();

}