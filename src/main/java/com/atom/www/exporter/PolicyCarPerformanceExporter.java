package com.atom.www.exporter;

import java.util.Arrays;
import java.util.List;

import com.cariq.toolkit.coreiq.exporter.CarIQExporterWrapper;
import com.cariq.toolkit.coreiq.exporter.CarIQWrappedExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * The Class PolicyCarPerformanceExporter.
 *
 * @author nitin
 */
public class PolicyCarPerformanceExporter extends CarIQWrappedExporterTemplate {

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("PolicyCarPerformanceExporter");

	/** The Constant EXPORTER. */
	public static final String EXPORTER = "PolicyCarPerformanceExporter";

	/** The Constant EXPORTER_DESCRIPTION. */
	private static final String EXPORTER_DESCRIPTION = "This is PolicyCarPerformance Exporter";

	/** The Constants. */
	public static final String BY_POLICY_NUMBERS = "ByPolicyNumbers", DOMAIN = "Domain", POLICY_NUMBER = "PolicyNumber";

	/**
	 * Instantiates a new policy car performance exporter.
	 */
	public PolicyCarPerformanceExporter() {
		super(EXPORTER, EXPORTER_DESCRIPTION);

		// Query definitions
		addQueryType(BY_POLICY_NUMBERS, "Exports car averager performance data by policy numbers",
				Arrays.asList(new QueryDefinitionAttribute(POLICY_NUMBER, "PolicyNumber"), new QueryDefinitionAttribute(
						DOMAIN, "Domain names - IL-Assist, Drivesmart, Driven-Dev, Driven, TAGIC")));

		// Support roles
		addSupportedRoles(Arrays.asList(Utils.ADMIN_ROLE, Utils.USER_ROLE));
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.exporter.CarIQWrappedExporterTemplate#getWrapper()
	 */
	@Override
	protected CarIQExporterWrapper getWrapper() {
		CarIQExporterWrapper wrapper = new CarIQExporterWrapper(EXPORTER, EXPORTER_DESCRIPTION);
		return wrapper;
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.exporter.CarIQWrappedExporterTemplate#doExport(java.lang.String, com.cariq.toolkit.utils.GenericJSON, int, int)
	 */
	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		logger.debug("doExport - queryType : " + queryType + " pageNo : " + pageNo + " pageSize : " + pageSize
				+ " json :" + json);

		if (pageNo > 1)
			return null;

		// validate query types
		if (!BY_POLICY_NUMBERS.equalsIgnoreCase(queryType))
			Utils.handleException("Invalid query type :" + queryType);

		try (ProfilePoint _doExport_ = ProfilePoint
				.profileAction("ProfAction_doExport_" + this.getClass().getCanonicalName())) {
			// for wrapper call.
			return super.doExport(queryType, json, pageNo, pageSize);
		}
	}
}