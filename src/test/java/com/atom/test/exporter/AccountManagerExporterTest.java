package com.atom.test.exporter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.atom.www.exporter.AccountManagerStatistics;
import com.cariq.toolkit.coreiq.exporter.CarIQExporter;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterRegistry;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Transactional
@Configurable
public class AccountManagerExporterTest {

	@Before
	public void init() {
		Utils.setupSecurityContext("preeti", "875d48192ccbbb18f32a13cec13cac14", "a1");
	}
	
	@Test
	public void getByOwnerAndStage() {
		
		CarIQExporter exporter = CarIQExporterRegistry.getExporter(AccountManagerStatistics.TheExporter);
		
		GenericJSON json = new GenericJSON();
		json.put(AccountManagerStatistics.ATTRIBUTE_OWNER, "sweta");
		json.put(AccountManagerStatistics.ATTRIBUTE_DATE, "2018-12-15 00:00:00");
		json.put(AccountManagerStatistics.ATTRIBUTE_DOMAIN, "IL-Assist");
		
		System.out.println(AccountManagerStatistics.QUERY_TYPE_BY_OWNER_AND_DAY + ":" +
				exporter.export(AccountManagerStatistics.QUERY_TYPE_BY_OWNER_AND_DAY,
				json, 1, 100));
	}
	
	@Test
	public void getByOwner() {
		CarIQExporter exporter = CarIQExporterRegistry.getExporter(AccountManagerStatistics.TheExporter);
		
		GenericJSON json = new GenericJSON();
		json.put(AccountManagerStatistics.ATTRIBUTE_OWNER, "sweta");
		json.put(AccountManagerStatistics.ATTRIBUTE_DOMAIN, "IL-Assist");
		
		System.out.println(AccountManagerStatistics.QUERY_TYPE_BY_OWNER + ":" +
				exporter.export(AccountManagerStatistics.QUERY_TYPE_BY_OWNER,
				json, 1, 100));
	}
	
	@Test
	public void getForSpotlight() {
		CarIQExporter exporter = CarIQExporterRegistry.getExporter(AccountManagerStatistics.TheExporter);
		
		GenericJSON json = new GenericJSON();
		json.put(AccountManagerStatistics.ATTRIBUTE_OWNER, "sweta");
		json.put(AccountManagerStatistics.ATTRIBUTE_DOMAIN, "IL-Assist");
		
		System.out.println(AccountManagerStatistics.QUERY_TYPE_SPOTLIGHT + ":" +
				exporter.export(AccountManagerStatistics.QUERY_TYPE_SPOTLIGHT,
				json, 1, 100));
	}
}
