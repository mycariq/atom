/**
 * 
 */
package com.cariq.toolkit.coreiq.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObjectSample.PersistentObjectSample_In;
import com.cariq.toolkit.coreiq.model.PersistentObjectSample.PersistentObjectSample_Out;
import com.cariq.toolkit.model.Tag;
import com.cariq.toolkit.model.Tag.Tag_Out;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;

/**
 * This class and its base class demonstrates creation of Entity Bean (Model) without using Roo
 * There is a boiler plate code at the top that needs to be copied in every model class
 * This can't be moved to Base class, because, Spring uses tremendous amount of 
 * reflection and dependency injection.
 * 
 *  At the end there are 2 more functions viz. hashCode() and toString(), which are generated 
 *  by using eclipse->Source->generate feature. These functions are very handy while working with logging and Collections
 *  
 *  Finally, the Model class implements JSONable<In, Out, Id> interface.
 *  This enforces the JSON for the model to be created and kept in sync with the model.
 * @author hrishi
 *
 */
@Entity
@Configurable
public class PersistentObjectSample extends PersistentObject<PersistentObjectSample> implements JSONable<PersistentObjectSample_In, PersistentObjectSample_Out, IdJSON>{
	/**
	 *  There are 5 sections in the class viz.
	 *  SECTION 1 - Bolier Plate code for Persistence
	 *  SECTION 2 - Model properties
	 *  SECTION 3 - POJO Utility Methods
	 *  SECTION 4 - JSONable interface
	 *  SECTION 5 - Static Functions for Query, finders and operations for this class
	 */
	
	// SECTION 1 - Bolier Plate code for Persistence
	/**
	 * @TODO Boilerplate code should be copied as it is...
	 * Name of class should be changed from PersistentObjectSample to your class
	 */
	// ----- Boilerplate code Ends ----------------
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    
    @Version
    Integer version;
	
    @Override
	public Long getId() {
		return id;
	}
    @Override
	public void setId(Long id) {
		this.id = id;
	}
    @Override
	public Integer getVersion() {
		return version;
	}
    @Override
	public void setVersion(Integer version) {
		this.version = version;
	}
	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new PersistentObjectSample().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null) this.entityManager = entityManager();
		return entityManager;
	}
	
    /**
	 * @param id
	 * @return
	 */
	protected static PersistentObjectSample find(Long id) {
		return entityManager().find(PersistentObjectSample.class, id);
	}
	
	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public PersistentObjectSample findById(Long id) {
		return find(id);
	}

	// ----- Boilerplate code Ends ----------------
	// --- Section 1 Ends -------

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------
	/**
	// ----------------------------------------------------------------
	// Implementation of Model Properties - the actual code.
	// ----------------------------------------------------------------
	// Use following Annotations wherever applicable
	// @Temporal(TemporalType.TIMESTAMP)
	// @DateTimeFormat(style = "M-")
	// @Column(length = 4000)
	// @Column(unique = true)
	// @Column(nullable = false)
	// @Column(unique = true)
	// @Column(unique = true)
	// @ManyToOne
	// @JoinColumn(name = "workshop")
	// @NotNull
	// ** AVOID ** @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "itsCar")
	// ** AVOID ** @OneToOne(cascade = CascadeType.ALL)

	*/



	/**
	 * Model properties
	 */
	String str;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date dt;
	Long lng;
	Double dbl;

	@ManyToOne
	Tag tag;
	
	// Default constructor is a must!
	public PersistentObjectSample() {
		super();
	}
	
	/**
	 * Parameterized constructor
	 * @param str
	 * @param dt
	 * @param lng
	 * @param dbl
	 */
	public PersistentObjectSample(Tag tag, String str, Date dt, Long lng, Double dbl) {
		this.tag = tag;
		this.str = str;
		this.dt = dt;
		this.lng = lng;
		this.dbl = dbl;
	}
	/**
	 * Getters Setters
	 */
	
	public String getStr() {
		return str;
	}
	public void setStr(String str) {
		this.str = str;
	}
	public Date getDt() {
		return dt;
	}
	public void setDt(Date dt) {
		this.dt = dt;
	}
	public Long getLng() {
		return lng;
	}
	public void setLng(Long lng) {
		this.lng = lng;
	}
	public Double getDbl() {
		return dbl;
	}
	public void setDbl(Double dbl) {
		this.dbl = dbl;
	}

	public Tag getCar() {
		return tag;
	}
	
	public void setCar(Tag tag) {
		this.tag = tag;
	}
	
	// --- Section 2 Ends -------

	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------

	// Implementation of toString and hashCode and equals are advisable for all Model classes
	@Override
	public String toString() {
		return "PersistentObjectSample [id=" + id + ", version=" + version + ", str=" + str + ", dt=" + dt + ", lng=" + lng
				+ ", dbl=" + dbl + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dbl == null) ? 0 : dbl.hashCode());
		result = prime * result + ((dt == null) ? 0 : dt.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lng == null) ? 0 : lng.hashCode());
		result = prime * result + ((str == null) ? 0 : str.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersistentObjectSample other = (PersistentObjectSample) obj;
		if (dbl == null) {
			if (other.dbl != null)
				return false;
		} else if (!dbl.equals(other.dbl))
			return false;
		if (dt == null) {
			if (other.dt != null)
				return false;
		} else if (!dt.equals(other.dt))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lng == null) {
			if (other.lng != null)
				return false;
		} else if (!lng.equals(other.lng))
			return false;
		if (str == null) {
			if (other.str != null)
				return false;
		} else if (!str.equals(other.str))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}
	
	// --- Section 3 Ends -------

	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API interaction
	// In Class (for construction)
	public static class PersistentObjectSample_In {
		String str;
		Date dt;
		Long lng;
		Double dbl;
		Long tagId;
		/**
		 * @param str
		 * @param dt
		 * @param lng
		 * @param dbl
		 */
		public PersistentObjectSample_In(Long tagId, String str, Date dt, Long lng, Double dbl) {
			this.tagId = tagId;
			this.str = str;
			this.dt = dt;
			this.lng = lng;
			this.dbl = dbl;
		}
		
		public String getStr() {
			return str;
		}
		public void setStr(String str) {
			this.str = str;
		}
		public Date getDt() {
			return dt;
		}
		public void setDt(Date dt) {
			this.dt = dt;
		}
		public Long getLng() {
			return lng;
		}
		public void setLng(Long lng) {
			this.lng = lng;
		}
		public Double getDbl() {
			return dbl;
		}
		public void setDbl(Double dbl) {
			this.dbl = dbl;
		}

		public Long getTagId() {
			return tagId;
		}

		public void setCarId(Long tagId) {
			this.tagId = tagId;
		}
	}
	
	// Out class for output
	public static class PersistentObjectSample_Out extends PersistentObjectSample_In {
		Long id;
		Tag_Out tagJSON;
		/**
		 * @param str
		 * @param dt
		 * @param lng
		 * @param dbl
		 * @param id
		 */
		public PersistentObjectSample_Out(Long carId, String str, Date dt, Long lng, Double dbl, Long id, Tag_Out tagJSON) {
			super(carId, str, dt, lng, dbl);
			this.id = id;
			this.tagJSON = tagJSON;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public Tag_Out getTagJSON() {
			return tagJSON;
		}

		public void setCarJSON(Tag_Out tagJSON) {
			this.tagJSON = tagJSON;
		}
	
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.utils.json.JSONable#toJSON()
	 */
	@Override
	public PersistentObjectSample_Out toJSON() {
		return new PersistentObjectSample_Out(tag.getId(), str, dt, lng, dbl, id, tag.toJSON());
	}
	/* (non-Javadoc)
	 * @see com.cariq.www.utils.json.JSONable#fromJSON(java.lang.Object)
	 */
	@Override
	public void fromJSON(PersistentObjectSample_In json) {
		this.tag = json.getTagId() == null ? null : Tag.findObjectById(json.getTagId());

		this.str = json.str;
		this.dt = json.dt;
		this.lng = json.lng;
		this.dbl = json.dbl;
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.utils.json.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}
	
	// --- Section 4 Ends -------
	
	// ---------------------------------------------
	// SECTION 5 - Query and Class specific finders and static utility methods
	// ---------------------------------------------
	public static PersistentObjectSample findObjectById(Long id) {
		return find(id);
	}
	
	public static List<PersistentObjectSample> getAllObjects(int pageNo, int pageSize) {
		CarIQSimpleQuery<PersistentObjectSample> qry = new CarIQSimpleQuery<PersistentObjectSample>("getAllObjects", entityManager(), PersistentObjectSample.class);
		qry.setPaging(pageNo, pageSize);
		
		return qry.getResultList();
	}
	/**
	 * @param car2
	 * @param i
	 * @param j
	 * @return
	 */
	public static List<PersistentObjectSample> getAllObjectsByCar(Tag tag, int pageNo, int pageSize) {
		CarIQSimpleQuery<PersistentObjectSample> qry = new CarIQSimpleQuery<PersistentObjectSample>("getAllObjects", entityManager(), PersistentObjectSample.class);
		qry.addCondition("car", "=", tag);
		qry.setPaging(pageNo, pageSize);
		
		return qry.getResultList();
	}
}
