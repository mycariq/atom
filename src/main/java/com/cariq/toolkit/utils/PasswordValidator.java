package com.fleetiq.www.validator;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Lists;

public class PasswordValidator {
	private Pattern pattern;
	  private Matcher matcher;

	  private static final String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";
	  
	  private final List<String> errMsgList = Lists.newArrayList();

		private void addException(String msg) {
			errMsgList.add(msg);
		}

		public PasswordValidator validateNotNull(String o, final String msg) {
			if (StringUtils.isEmpty(o)) {
				addException(msg);
			}
			return this;
		}
		
		public PasswordValidator checkLength(String str, int minLen, int maxLen, final String msg) {
			if (!StringUtils.isEmpty(str)) {
				if (str.length() < minLen || str.length() > maxLen) {
					addException(msg);
				}
			}
			return this;
		}
		
		/*
		 * Pattern matching code
		 */
		public PasswordValidator patternMatching(String validatingString, String patternString, String msg) {
			if (!StringUtils.isEmpty(validatingString)) {
				Pattern pattern = Pattern.compile(patternString);
				Matcher matcher = pattern.matcher(validatingString);
				if (!matcher.matches()) {
					addException(msg);
				}
			}
			return this;
		}


	 /* public PasswordValidator(){
		  pattern = Pattern.compile(PASSWORD_PATTERN);
	  }*/

	  /**
	   * Validate password with regular expression
	   * @param password password for validation
	   * @return true valid password, false invalid password
	   */
	  public boolean validate(final String password){

		  matcher = pattern.matcher(password);
		  return matcher.matches();

	  }

}
