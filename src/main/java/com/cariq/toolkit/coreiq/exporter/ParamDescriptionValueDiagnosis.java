/**
 * 
 */
package com.cariq.toolkit.coreiq.exporter;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

/**
 * @author hrishi
 *
 */
public class ParamDescriptionValueDiagnosis {
	String parameter, description, value, diagnosis;

	/**
	 * @param parameter
	 * @param description
	 * @param value
	 * @param diagnosis
	 */
	public ParamDescriptionValueDiagnosis(String parameter, String description, String value, String diagnosis) {
		this(parameter, value);
		this.description = description;
		this.diagnosis = diagnosis;
	}
	
	/**
	 * @param parameter
	 * @param description
	 * @param value
	 * @param diagnosis
	 */
	public ParamDescriptionValueDiagnosis(String parameter, String value) {
		this.parameter = parameter;
		this.description = parameter;
		this.value = value;
		this.diagnosis = Utils.DIAG_GOOD;
	}
	
	public ParamDescriptionValueDiagnosis(){}
	public String getParameter() {
		return parameter;
	}
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getDiagnosis() {
		return diagnosis;
	}
	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}
	
	/**
	 * Convert to GenericJSON (map)
	 * @return
	 */
	public GenericJSON toJSON() {
		GenericJSON retval = new GenericJSON();
		retval.put(Utils.PARAMETER, getParameter());
		retval.put(Utils.DESCRIPTION, getDescription());
		retval.put(Utils.VALUE, getValue());
		retval.put(Utils.DIAGNOSIS, getDiagnosis());
		
		return retval;
	}
	
	/**
	 * Convert to GenericJSON (map)
	 * @return
	 */
	public GenericJSON toLightJSON() {
		GenericJSON retval = new GenericJSON();
		retval.put(Utils.DESCRIPTION, getDescription());
		retval.put(Utils.VALUE, getValue());
		
		return retval;
	}
	
}
