package com.cariq.toolkit.coreiq.tableUtil.interfaces;

import java.util.List;

import com.cariq.toolkit.utils.GenericJsonable;

/**
 * 
 * @author hrishi
 *
 */
public interface TableIfc extends GenericJsonable, CellLocatorIfc  {
	String getHeading();
	
	List<RowIfc> getRows();

	List<ColumnIfc> getColumns();
}
