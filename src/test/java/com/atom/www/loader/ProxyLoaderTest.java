package com.atom.www.loader;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.coreiq.loader.CarIQLoader;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderContext;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderRegistry;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Transactional
@Configurable
public class ProxyLoaderTest {
	
	@Before
	public void init() {
		Utils.setupSecurityContext("preeti", "875d48192ccbbb18f32a13cec13cac14", "a1");
	}

	@Test 
	public void testProxyLoader() {
		CarIQLoader loader = CarIQLoaderRegistry.getLoader("ProxyUpdater");
		
		GenericJSON json = new GenericJSON();
		json.put("signature", "3001/158507506/00/B00");
		json.put("owner", "preeti");
		json.put("stage", "Not verified");
		json.put("type", "InsurancePolicy");
		json.put("state", "Invalid");
		json.put("domain", "IL-Assist");
		
		CarIQLoaderContext context = new CarIQLoaderContext();
		
		loader.load(json, context);
	}
}
