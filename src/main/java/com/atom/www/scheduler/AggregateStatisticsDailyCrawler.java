package com.atom.www.scheduler;

import java.util.List;

import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.atom.www.exporter.AggregateStatisticsExporter;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterService;
import com.cariq.toolkit.coreiq.loader.CarAggregateStatisticsLoader;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderContext;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderService;
import com.cariq.toolkit.serviceimpl.async.CarIqJobWorker;
import com.cariq.toolkit.serviceimpl.async.WIHelper;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * @author nitin
 *
 */
public class AggregateStatisticsDailyCrawler extends CarIqJobWorker {

	private static final CarIQLogger logger = WIHelper.getLogger("AggregateStatisticsDailyCrawler");

	@Autowired
	CarIQExporterService exporterService;

	/** The loader service. */
	@Autowired
	CarIQLoaderService loaderService;
	
	private static final int pageNumber = 1;
	//private static final int pageSize = 200000;

	private static final String KEY_DOMAIN = "domain", IL_ASSIST = "IL-Assist";
	
	//private static String commaSepDomains = null;
	
	@Override
	protected void executeInternal(JobExecutionContext context) throws Exception {
		logger.debug("executeInternal: ");

		try (ProfilePoint _executeInternal = ProfilePoint
				.profileAction("ProfAction_executeInternal_" + this.getClass().getCanonicalName())) {
			
			//commaSepDomains = Domain.getCommaSepDomains(pageNumber, pageSize);
			GenericJSON inputJson = new GenericJSON();
			inputJson.put(KEY_DOMAIN, IL_ASSIST);
			
			logger.debug("Call to AggregateStatisticsExporter");
			//Export data only from IL-Assist domain
			AggregateStatisticsExporter exporter = new AggregateStatisticsExporter();
			List<GenericJSON>  aggregateStatisticsJsons = exporter.export(AggregateStatisticsExporter.BY_STATE, inputJson, pageNumber, 1);
			
			logger.debug("Call to CarAggregateStatisticsLoader");
			CarIQLoaderContext LoaderContext = new CarIQLoaderContext();
			GenericJSON json = aggregateStatisticsJsons.get(0);
			CarAggregateStatisticsLoader loader = new CarAggregateStatisticsLoader();
			if (null != json)
				loader.load(json, LoaderContext);
		}
	}
}