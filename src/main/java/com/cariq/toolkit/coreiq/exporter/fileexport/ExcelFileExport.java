package com.cariq.toolkit.coreiq.exporter.fileexport;

import java.io.File;

import com.cariq.toolkit.coreiq.exporter.CarIQExporter;
import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.CsvToExcelConvertor;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

/**
 * The Class ExcelFileExport.
 */
public class ExcelFileExport extends FileExportTemplate {

	/**
	 * Instantiates a new excel file export.
	 *
	 * @param exporter
	 *            the exporter
	 * @param queryType
	 *            the query type
	 * @param inputJSON
	 *            the input JSON
	 */
	public ExcelFileExport(CarIQExporter exporter, String queryType, GenericJSON inputJSON) {
		super(exporter, queryType, inputJSON, CarIQFileUtils.EXCEL);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.exporter.fileexport.FileExport#getMimeType()
	 */
	@Override
	public String getMimeType() {
		return "application/vnd.ms-excel";
	}

	/**
	 * Export.
	 *
	 * @param exporter
	 *            the exporter
	 * @param queryType
	 *            the query type
	 * @param json
	 *            the json
	 * @param outputFile
	 *            the output file
	 * @throws Exception
	 *             the exception
	 */
	private void export(final CarIQExporter exporter, final String queryType, final GenericJSON json, String outputFile)
			throws Exception {

		for (CarIQExporter childExporter : exporter.getChildExporters()) {

			//execute child exporter and pass exported csv to excel converter
			CSVFileExport csvFileExport = new CSVFileExport(childExporter, queryType, inputJSON);
			File csvFile = csvFileExport.execute();

			new CsvToExcelConvertor(childExporter.getShortName()).convert(csvFile.getAbsolutePath(), outputFile);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.exporter.fileexport.FileExportTemplate#doExecute()
	 */
	@Override
	protected File doExecute() {
		// Prepare a temp file
		String excelExportFile = CarIQFileUtils.getTempFile(exporter.getName() + "-" + queryType + ".xls");

		try {
			export(exporter, queryType, inputJSON, excelExportFile);
			return new File(excelExportFile);
		} catch (Exception e) {

			e.printStackTrace();
			Utils.handleException(e);
		}

		return null;
	}
}
