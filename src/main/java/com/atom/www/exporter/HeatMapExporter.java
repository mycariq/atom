package com.atom.www.exporter;

import java.util.Arrays;
import java.util.List;

import com.cariq.toolkit.coreiq.exporter.CarIQExporterWrapper;
import com.cariq.toolkit.coreiq.exporter.CarIQWrappedExporterTemplate;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * The Class HeatMapExporter.
 *
 * @author nitin
 */
public class HeatMapExporter extends CarIQWrappedExporterTemplate {

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("HeatMapExporter");

	/** The Constant Exporter. */
	private static final String Exporter = "HeatMapExporter";

	/** The Constant ExporterDescription. */
	private static final String ExporterDescription = "This is HeatMap Exporter";

	/**
	 * Instantiates a new heat map exporter.
	 */
	public HeatMapExporter() {
		super(Exporter, ExporterDescription);

		// Support roles
		addSupportedRoles(
				Arrays.asList(Utils.ADMIN_ROLE, Utils.USER_ROLE, Utils.ROLE_FLEET_ADMIN, Utils.ROLE_GROUP_OPERATOR));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.toolkit.coreiq.exporter.CarIQWrappedExporterTemplate#getWrapper()
	 */
	@Override
	protected CarIQExporterWrapper getWrapper() {
		CarIQExporterWrapper wrapper = new CarIQExporterWrapper(Exporter, Exporter);
		return wrapper;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.toolkit.coreiq.exporter.CarIQWrappedExporterTemplate#doExport(java.
	 * lang.String, com.cariq.toolkit.utils.GenericJSON, int, int)
	 */
	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		logger.debug("doExport - queryType : " + queryType + " pageNo : " + pageNo + " pageSize : " + pageSize
				+ " json :" + json);
		try (ProfilePoint _doExport_ = ProfilePoint
				.profileAction("ProfAction_doExport_" + this.getClass().getCanonicalName())) {
			// for wrapper call.
			return super.doExport(queryType, json, pageNo, pageSize);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.toolkit.coreiq.exporter.CarIQWrappedExporterTemplate#postProcess(
	 * java.lang.String, com.cariq.toolkit.utils.GenericJSON, java.util.List)
	 */
	@Override
	protected List<GenericJSON> postProcess(String queryType, GenericJSON inputJson, List<GenericJSON> outputJson) {
		return outputJson;
	}
}