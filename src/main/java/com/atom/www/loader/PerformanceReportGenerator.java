/**
 * 
 */
package com.atom.www.loader;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.atom.www.exporter.AppointmentReport;
import com.atom.www.exporter.AppointmentReportExporter;
import com.atom.www.exporter.IndividualLoadMatrixReportExporter;
import com.atom.www.exporter.StagingAndPolicyDetailsMiniReportExporter;
import com.atom.www.exporter.StagingAndPolicyDetailsReportExporter;
import com.atom.www.exporter.SupportTATReport;
import com.atom.www.exporter.SystemPerformanceMatrixReportExporter;
import com.atom.www.exporter.TimeLineExporter;
import com.atom.www.exporter.TimeLineReportExporter;
import com.cariq.toolkit.coreiq.exporter.ExporterWorkDefinition;
import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * The Class PerformanceReportGenerator.
 *
 * @author santosh
 */
public class PerformanceReportGenerator extends MultipleExporterAsyncLoader {
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("PerformanceReportGenerator");

	public static final String TheLoader = "PerformanceReportGenerator";

	public PerformanceReportGenerator() {
		super(TheLoader, "Generating Support performance Report");
	}

	@Override
	protected List<ExporterWorkDefinition> getExporterWorkerDefinitions(GenericJSON json) {
		try (ProfilePoint _getExporterWorkerDefinitions = ProfilePoint
				.profileAction("PerformanceReportGenerator_getExporterWorkerDefinitions")) {
			List<ExporterWorkDefinition> retval = new ArrayList<ExporterWorkDefinition>();

			Date now = new Date();
			String startDate = Utils.convertToyyyymmdd_hhmmssFormat(Utils.getDaysBefore(now, 15));
			String endDate = Utils.convertToyyyymmdd_hhmmssFormat(now);

			// 1. support performance report
			// first is basic timeline report creation
			retval.add(new ExporterWorkDefinition(TimeLineExporter.EXPORTER, TimeLineExporter.DOMAIN_AND_DATE_RANGE,
					CarIQFileUtils.CSV, GenericJSON.build(TimeLineExporter.DOMAIN_NAME, TimeLineExporter.ALL,
							TimeLineExporter.START_DATE, startDate, TimeLineExporter.END_DATE, endDate)));

			// second is the timeline report exporter(support performance report)
			retval.add(new ExporterWorkDefinition(TimeLineReportExporter.EXPORTER,
					TimeLineReportExporter.FROM_LATEST_FILE_URL, "html", new GenericJSON()));
			
			String createdAfter = "2019-07-30 18:30:00";
			
			// 2. System Performance report
			retval.add(new ExporterWorkDefinition(SupportTATReport.TheExporter,
					StagingAndPolicyDetailsReportExporter.createdAfter, CarIQFileUtils.CSV,
					GenericJSON.build(StagingAndPolicyDetailsReportExporter.createdAfter, createdAfter)));

			retval.add(new ExporterWorkDefinition(SystemPerformanceMatrixReportExporter.EXPORTER,
					SystemPerformanceMatrixReportExporter.FROM_LATEST_TAT_URL, "html", new GenericJSON()));
		
			// 3. Individual Performance report
			retval.add(new ExporterWorkDefinition(StagingAndPolicyDetailsMiniReportExporter.TheExporter,
					StagingAndPolicyDetailsReportExporter.createdAfter, CarIQFileUtils.CSV,
					GenericJSON.build(StagingAndPolicyDetailsReportExporter.createdAfter, createdAfter)));

			retval.add(new ExporterWorkDefinition(IndividualLoadMatrixReportExporter.EXPORTER,
					IndividualLoadMatrixReportExporter.FROM_LATEST_TAT_URL, "html", new GenericJSON()));
			
			// 4. Generating Appointment report
			retval.add(new ExporterWorkDefinition(AppointmentReport.REPORT, AppointmentReport.ALL, CarIQFileUtils.CSV,
					GenericJSON.build(AppointmentReport.DOMAIN_NAME, AppointmentReport.ALL)));

			retval.add(new ExporterWorkDefinition(AppointmentReportExporter.EXPORTER,
					AppointmentReportExporter.FROM_LATEST_FILE_URL, "html", new GenericJSON()));
			
			return retval;
		}
	}
}
