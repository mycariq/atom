package com.cariq.toolkit.coreiq.tableUtil.application.mis.arbiter;

import java.util.List;

import com.cariq.toolkit.utils.GenericJSON;

public class CellValueArbiterDefaultOn extends MISCellValueArbiterBase {

	public CellValueArbiterDefaultOn(List list) {
		super(list);
	}

	@Override
	public boolean isApplicable(GenericJSON input) {
		return true;
	}
}
