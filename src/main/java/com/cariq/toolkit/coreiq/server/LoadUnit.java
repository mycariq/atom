package com.cariq.toolkit.coreiq.server;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.GenericJsonable;


public class LoadUnit implements GenericJsonable {
	String name;
	int count;
	List<ServerActivity> items = new ArrayList<ServerActivity>();
	
	public LoadUnit(String name, Map<ServerActivity, ServerActivity> load) {
		this.name = name;
		this.count = load.size();
		
		for (ServerActivity activity : load.keySet()) {
			items.add(activity);
		}
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	
	
	public List<ServerActivity> getItems() {
		return items;
	}
	public void setItems(List<ServerActivity> items) {
		this.items = items;
	}
	
	@Override
	public GenericJSON toGenericJSON() {
		GenericJSON json = new GenericJSON();
//		json.add("name", name);
		json.add("count", count);
		
		List<GenericJSON> itemsJSON = new ArrayList<GenericJSON>();
		for (ServerActivity unit : items) {
			itemsJSON.add(unit.toGenericJSON());
		}
		json.add("items", itemsJSON);
		
		return json;
	}
	
	@Override
	public void fromGenricJSON(GenericJSON json) {
		// TODO Auto-generated method stub
		
	}
}
