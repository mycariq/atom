package com.cariq.toolkit.utils.analytics.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cariq.toolkit.utils.Utils;

@SuppressWarnings("serial")
public class AnalyticsContext extends HashMap<String, Object> implements AnalyticsVisitable {
	AnalyticsContext output = null;
	boolean debug = false;
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder("===\nContext:\n");
		for (String key : keySet()) {
			if (key.equals("car"))
				continue;

			buf.append(key).append(":").append(get(key).toString()).append("\n");
		}
		
		return buf.toString();
	}
	
	/**
	 * @return the debug
	 */
	public boolean isDebug() {
		return debug;
	}

	/**
	 * @param debug the debug to set
	 */
	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	public void accept(AnalyticsVisitor visitor) {
		visitor.visit(this, this.getClass().getSimpleName());
		
//		// visit Informations
//		InfractionsMap infraMap = (InfractionsMap) get(AnalyticsHelper.INFRACTIONS);
//		infraMap.accept(visitor);
//		
//		// visit Informations
//		InfractionsMap infoMap = (InfractionsMap) get(AnalyticsHelper.INFORMATIONS);
//		infoMap.accept(visitor);
	}

	/**
	 * Get Parameter Name for a given class
	 * @param cls
	 * @param param
	 * @return
	 */
	public String getParamName(Class<?> cls, String param) {
		return cls.getName() + "." + param;
	}
	

	/**
	 * Get Param Value with Casting
	 * @param cls
	 * @param param
	 * @param castTo
	 * @return
	 */
	public <T> T getParamValue(Class<?> cls, String param, Class<T> castTo) {
		try {
			return Utils.downCast(castTo, get(getParamName(cls, param)));
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Get Param value for a given class
	 * @param cls
	 * @param param
	 * @return
	 */
	public Object getParamValue(Class<?> cls, String param) {
		return getParamValue(cls, param, Object.class);
	}
	
	/**
	 * Set parameter for given class
	 * @param cls
	 * @param param
	 * @param value
	 * @return
	 */
	public AnalyticsContext setParam(Class<?> cls, String param, Object value) {
		put(getParamName(cls, param), value);
		return this;
	}
	
	/**
	 * Get the List
	 * @param key
	 * @param clz
	 * @return
	 */
	public <T> List<T> getList(String key, Class<T> clz) {
		return (List<T>) get(key);
	}

	
	/**
	 * Put a value in the List
	 * @param key
	 * @param value
	 * @return
	 */
	public <T> AnalyticsContext putInList(String key, T value) {
		List<T> lst = (List<T>) get(key);
		if (lst == null) {
			lst = new ArrayList<T>();
			put(key, lst);
		}
		
		lst.add(value);
		return this;
	}
	
	/**
	 * Get the Map
	 * @param key
	 * @param clz
	 * @return
	 */
	public <K,V> Map<K,V> getMap(String key, Class<K> mapKeyClass, Class<V> mapValueClass) {
		return (Map<K,V>) get(key);
	}
	
	/**
	 * Put a value in the Map
	 * @param key
	 * @param value
	 * @return
	 */
	public <K,V> AnalyticsContext putInMap(String key, K mapKey, V mapValue) {
		Map<K,V> map = (Map<K,V>) get(key);
		if (map == null) {
			map = new HashMap<K,V>();
			put(key, map);
		}
		
		map.put(mapKey,  mapValue);
		return this;
	}
}
