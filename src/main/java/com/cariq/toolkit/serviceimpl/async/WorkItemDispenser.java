package com.cariq.toolkit.serviceimpl.async;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.coreiq.server.LoadSummary;
import com.cariq.toolkit.coreiq.server.Server;
import com.cariq.toolkit.model.async.WorkItem;
import com.cariq.toolkit.service.async.WorkItemBucket;
import com.cariq.toolkit.service.async.WorkItemFetcher;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.Utils;


@Transactional(propagation=Propagation.REQUIRES_NEW)
public class WorkItemDispenser implements Iterable<WorkItem> {
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("WorkItemDispenser");

	public static final int ItemsPerServer = 15;
//	private int rowNum = 0;
//	public static final int BATCH_SIZE = 20;
	private List<WorkItem> workItems = new ArrayList<WorkItem>();

//	private void loadWorkItems() {
//		// WIHelper.logger.debug("Loading Work Items from row = " +  rowNum);
//		List<WorkItem> items = WorkItem.findUnCompletedWorkItem(rowNum, BATCH_SIZE);
//		rowNum += items.size();
//		workItems.addAll(items);
//	}
	
	/**
	 * Load aware dispenser. Tell it how many servers present, accordingly it will dispense limited number of workitems to start
	 * Throttling is necessary to limit overload.
	 */
//	public WorkItemDispenser(int serverCount) {
//		// Max 5 workitems per server at any time.... there may be 100 ready, but take 5 per server.
//		int maxRunningItems = ItemsPerServer;
//		List<WorkItem> items = WorkItem.findReadyToWorkItems(maxRunningItems);
////		rowNum += items.size();
//		workItems.addAll(items);
//	}
	
	/**
	 * Load aware dispenser. Tell it how many servers present, accordingly it will dispense limited number of workitems to start
	 * Throttling is necessary to limit overload.
	 */
	public WorkItemDispenser(Object... typeCapacity) {
		// checks on the input
		if (typeCapacity == null || (typeCapacity.length % 2) != 0)
			Utils.handleException("WorkItemDispenser constructor: Type and capacity must be in pairs!");

		WorkItemFetcher fetcher = new WorkItemFetcher(createWorkItemBuckets(typeCapacity));
		
//		Utils.debug_log(logger, "Fetcher before Adjusting bucket capacity based on current load: " + fetcher.toString());
		List<LoadSummary> serverLoad = Server.getInstance().getLoadSummary(Server.WORKER_AUDIT);
		fetcher.adjustBucketCapcities(serverLoad);
	
		int maxReadyItems = fetcher.getTotalAvailableCapacity();
		// capacity is full
		if (maxReadyItems == 0)
			return;
		
//		Utils.debug_log(logger, "Fetcher before loading workitems: " + fetcher.toString());

		// get Ready Items from last 24 hours
		int MaxReadyItems = 500; // since the items may be hidden in there that are not workable
		List<WorkItem> items = WorkItem.findReadyToWorkItems(MaxReadyItems, 24);
		if (items.size() == 0)
			return;

		Utils.debug_log(logger, "Number of ready items found: " + items.size());
		
		// nothing to process
		fetcher.process(items);
		
		Utils.debug_log(logger, "Fetcher after processing workitems: " + fetcher.toString());

		workItems.addAll(fetcher.getDispensableItems());
	}
	
	private List<WorkItemBucket> createWorkItemBuckets(Object[] typeCapacity) {
		List<WorkItemBucket> retval = new ArrayList<WorkItemBucket>();
		
		for (int index = 0; index < typeCapacity.length; index += 2) {
			String type = Utils.downCast(String.class, typeCapacity[index]);
			int capacity = Utils.downCast(Integer.class, typeCapacity[index + 1]);
			
			retval.add(new WorkItemBucket(type, capacity));
		}
		
		return retval;
	}

//
//	public WorkItemDispenser() {
//		loadWorkItems();
//	}

	@Override
	public Iterator<WorkItem> iterator() {
		return new WorkItemIterator();
	}

	private class WorkItemIterator implements Iterator<WorkItem> {

		@Override
		public boolean hasNext() {
//			if (rowNum < BATCH_SIZE - 1)
//				return workItems.size() > 0;
				
//			loadWorkItems();
			return workItems.size() > 0;
		}

		@Override
		public WorkItem next() {
			WorkItem workItem = workItems.remove(0);
//			workItem.setStatus(WorkItemStatus.RUNNING);
//			workItem = workItem.merge();
//			APIHelper.logger.debug("================NEXT============="
//					+ workItem.getId());
			return workItem;
		}

//		@Override
//		public void remove() {
//			throw new UnsupportedOperationException("Unsupported method: WorkItemDispenser.remove");
//		}
	}	
}