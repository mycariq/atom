package com.cariq.toolkit.coreiq.tableUtil.application.mis;

import java.util.List;

import com.cariq.toolkit.coreiq.formula.impl.Formula;
import com.cariq.toolkit.coreiq.formula.interfaces.FormulaIfc;
import com.cariq.toolkit.coreiq.tableUtil.impl.ProcessingContext;
import com.cariq.toolkit.coreiq.tableUtil.impl.RowColumn;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.CellIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.CellValueAribiterIfc;
import com.cariq.toolkit.utils.APIHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

public class MISRowColumn extends RowColumn implements MISProcessable, CellValueAribiterIfc  {
	String label;
	CellValueAribiterIfc arbiter;
	FormulaIfc formula;

	public MISRowColumn(String style, String label, String evaluatorClass, String argumentJSON) {
		super(style);
		this.label = label;
		this.arbiter = MISHelper.constructArbiter(evaluatorClass, argumentJSON);
	}
	
	public MISRowColumn(String label, String evaluatorClass, String argumentJSON) {
		this(null, label, evaluatorClass, argumentJSON);
	}

	public MISRowColumn() {
		// TODO Auto-generated constructor stub
	}

	public String getLabel() {
		return label;
	}
	
	@Override
	public MISRowColumn process(MISProcessingContext context, GenericJSON json) {
		if (!arbiter.isApplicable(json))
			return this;

		for (CellIfc cellIfc: super.getCells()) {
			MISCell misCell = Utils.downCast(MISCell.class, cellIfc);
			misCell.process(context, json);
		}
		
		return this;
	}

	@Override
	public boolean isApplicable(GenericJSON input) {
		if (arbiter != null)
			return arbiter.isApplicable(input);
		
		return false;
	}

	@Override
	public void fromGenricJSON(GenericJSON json) {
		super.fromGenricJSON(json);
		label = Utils.getValue(String.class, json, "label");

		// load arbiter
		String evaluatorClass = Utils.getValue(String.class, json, "evaluatorClass");
		Object argument = json.get("argumentJSON");

		List argumentList = Utils.getJSonObject(Utils.getJSonString(argument), List.class);

		this.arbiter = MISHelper.constructArbiter(evaluatorClass, argumentList);

		// load formula
		Object formulaJSONObj = json.get("formula");
		if (formulaJSONObj != null) {
			try {
				GenericJSON formulaJSON = Utils.getJSonObject(Utils.getJSonString(formulaJSONObj), GenericJSON.class);
				formula = Utils.constructJSONable(Formula.class, formulaJSON);
			} catch (Exception e) {
				Utils.handleException("Unable construct Formula from " + Utils.getJSonString(formulaJSONObj));
			}
		}

	}

	public Integer evaluateCellValue(ProcessingContext context, GenericJSON json, MISCell misCell) {
		try {
			if (formula == null)
				return null;
			
			Object val = formula.evaluate(context);
			if (val instanceof Double)
				return ((Double) val).intValue();
			
			if (val instanceof Integer)
				return (Integer) val;
			
			return Integer.valueOf(val.toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Utils.logException(APIHelper.logger.getLogger("MISRowColumn"), e, "Evaluation of formula: " + formula.getExpression());
		}
		
		return null;
	}
	
}
