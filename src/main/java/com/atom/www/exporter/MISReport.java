package com.atom.www.exporter;

import java.util.Arrays;

import com.cariq.toolkit.coreiq.exporter.CarIQExporterRegistry;
import com.cariq.toolkit.coreiq.exporter.MonthlyAggregationExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.MultiExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;

/**
 * The Class MISAggregateExporter.
 */
public class MISReport extends MultiExporterTemplate {

	/** The Constant EXPORTER. */
	public static final String EXPORTER = "MIS Report";

	/**
	 * Instantiates a new MIS aggregate exporter.
	 */
	public MISReport() {
		super(EXPORTER, "The " + EXPORTER,
				Arrays.asList(CarIQExporterRegistry.getExporter(VerificationTATExporter.EXPORTER),
						CarIQExporterRegistry.getExporter(VerificationStatusExporter.EChannelExporter),
						CarIQExporterRegistry.getExporter(VerificationStatusExporter.RetailTelesaleExporter),
						CarIQExporterRegistry.getExporter(VerificationStatusExporter.AgencyExporter),
						CarIQExporterRegistry.getExporter(VerificationStatusExporter.GeoExporter),
						CarIQExporterRegistry.getExporter(VerificationStatusExporter.OthersExporter),
						CarIQExporterRegistry.getExporter(VerificationStatusExporter.TierAExporter),
						CarIQExporterRegistry.getExporter(VerificationStatusExporter.TierBExporter)));

		//query definition
		addQueryType(MonthlyAggregationExporterTemplate.TimeRange, "The time range",
				Arrays.asList(
						new QueryDefinitionAttribute(MonthlyAggregationExporterTemplate.StartTimestamp,
								"Start timestamp in yyyy-MM-dd HH:mm:ss"),
						new QueryDefinitionAttribute(MonthlyAggregationExporterTemplate.EndTimestamp,
								"End timestamp in yyyy-MM-dd HH:mm:ss")));
	}

}
