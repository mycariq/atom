/**
 * 
 */
package com.cariq.toolkit.utils.jsonProcessor;

/**
 * @author hrishi
 *
 */
public class JSONPath {
    String key;
	String url;
	int index;

	/**
	 * 
	 */
	public JSONPath() {
		this("", "", 0);
	}


	/**
	 * @param key
	 * @param url
	 * @param index
	 */
	public JSONPath(String key, String url, int index) {
		this.key = key;
		this.url = url;
		this.index = index;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + index;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JSONPath other = (JSONPath) obj;
		if (index != other.index)
			return false;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "JSONPath [key=" + key + ", url=" + url + ", index=" + index + "]";
	}


	/**
	 * @param i
	 * @return
	 */
	public JSONPath appendIndex(int i) {
		String index = "[" + i + "]";
		return new JSONPath(key, url + index, i);
	}
	

	/**
	 * @param i
	 * @return
	 */
	public JSONPath appendKey(String newKey) {
		return new JSONPath(newKey, url + "." + newKey, 0);
	}
}
