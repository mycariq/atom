package com.atom.www.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cariq.toolkit.model.Appointment.Appointment_In;
import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.service.AppointmentService;
import com.cariq.toolkit.utils.CarIQMutableAPI;
import com.cariq.toolkit.utils.ResponseWithIdJson;

@CarIQPublicAPI(name = "Appointment", description = "Everything related to Appointment")
@Controller
@RequestMapping("/appointment")
public class AppointmentController {
	
	/** The appointmentService. */
	@Autowired
	private AppointmentService appointmentService;
	
	
	/**
	 * Create new appointment
	 * 
	 * @param appointmentIn 	Input json to create appointment
	 * @return					response with id json
	 */
	@CarIQPublicAPIMethod(description = "Create new appointment", responseClass = ResponseWithIdJson.class, internal = true)
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public ResponseWithIdJson createAppointment(@RequestBody Appointment_In appointmentIn) {
		try (CarIQMutableAPI _createAppointment = new CarIQMutableAPI("CARIQ_createAppointment", appointmentIn)) {
			return appointmentService.create(appointmentIn);
		}		
	}

	/**
	 * Update exisiting appointment
	 * 
	 * @param appointmentId		Id of existing appointment to update
	 * @param appointmentIn		Input json of updated values
	 * @return					Response with id json
	 */
	@CarIQPublicAPIMethod(description = "Update existing appointment", responseClass = ResponseWithIdJson.class, internal = true)
	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	@ResponseBody	
	public ResponseWithIdJson updateAppointment(@PathVariable("id") Long appointmentId, @RequestBody Appointment_In appointmentIn) {
		try (CarIQMutableAPI _createAppointment = new CarIQMutableAPI("CARIQ_updateAppointment", appointmentIn)) {
			return appointmentService.update(appointmentId, appointmentIn);
		}
	}
}
