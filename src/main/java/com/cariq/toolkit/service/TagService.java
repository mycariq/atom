package com.cariq.toolkit.service;

import java.util.List;

import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.model.Tag.Tag_In;
import com.cariq.toolkit.model.Tag.Tag_Out;

/**
 * The Interface TagService.
 */
public interface TagService {

	/**
	 * Check if tag exists OR create a new.
	 * @param params
	 * @return
	 */
	public ResponseWithIdJson createOrGetTag(Tag_In params);

	/**
	 * Gets the tag by name.
	 * @param tagName
	 * @return
	 */
	public Tag_Out getTag(String tagName);

	/**
	 * Update tag.
	 * @param params
	 * @param tagname
	 * @return
	 */
	public ResponseJson updateTag(Tag_In params, String tagname);

	/**
	 * Delete tag.
	 * @param tagname
	 * @return
	 */
	public ResponseJson deleteTag(String tagname);

	/**
	 * Get all tags
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public List<Tag_Out> getAllTags(int pageNo, int pageSize);
}