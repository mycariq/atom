package com.cariq.toolkit.coreiq.formula.interfaces;

import com.cariq.toolkit.utils.GenericJSON;

public interface VariableResolverIfc {
	// Evaluate or resolve the value based on definition 
	// - which itself can be a formula
	Object resolve(Object context, GenericJSON valueDefinition);
}
