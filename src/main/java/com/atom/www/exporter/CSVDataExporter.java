/**
 * 
 */
package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Configurable;

import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.google.common.base.Strings;

/**
 * The Class CSVDataExporter.
 *
 * @author santosh
 */
@Configurable
public class CSVDataExporter extends CarIQExporterTemplate {

	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("CSVDataExporter");

	public static final String EXPORTER = "CSVDataExporter";
	public static final String EXPORTER_DESCRIPTION = "Converting data from file URL";

	public static final String FROM_FILE_URL = "FromFileURL", FILE_URL = "fileURL";

	public CSVDataExporter() {
		super(EXPORTER, EXPORTER_DESCRIPTION);

		addQueryType(FROM_FILE_URL, "Generates Weekly MIS Report from given TAT report URL",
				Arrays.asList(new QueryDefinitionAttribute(FILE_URL, "Exporter file URL")));

		// Support roles
		addSupportedRoles(Arrays.asList(Utils.ADMIN_ROLE, Utils.USER_ROLE));
	}

	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		try (ProfilePoint _doExport = ProfilePoint.profileAction(EXPORTER + "_doExport")) {
			if (pageNo > 1)
				return null;

			if (!FROM_FILE_URL.equals(queryType))
				Utils.handleException(String.format("QueryType '%s' is not supported!!", queryType));

			String fileURL = Utils.getValue(json, FILE_URL);
			if (Strings.isNullOrEmpty(fileURL))
				Utils.handleException("Please provide file path URL.");

			// converting csv zip file url into local csv file
			String localFile = CarIQFileUtils.getLocalFilePathFromURL(fileURL);

			// get json list from local csv file 
			// TODO need to enhance with pagination
			List<GenericJSON> outputList = CarIQFileUtils.convertCsvToJsonList(localFile);
			if (Utils.isNullOrEmpty(outputList))
				return new ArrayList<GenericJSON>();

			return outputList;
		}
	}

}
