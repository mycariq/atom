package com.cariq.toolkit.coreiq.server;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ContextConfiguration;

import com.cariq.toolkit.model.async.WorkItem.WorkItem_In;
import com.cariq.toolkit.service.async.WorkItemService;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.Utils;
import com.google.common.base.Strings;
 

/**
 * The Class TestServiceImpl.
 */
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Service
@Configurable
public class ServerAuditServiceImpl implements ServerAuditService {

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("ServerAuditServiceImpl");
	
	/** The work item service. */
	@Autowired
	private WorkItemService workItemService;

	public static final String HEAP_DUMP = "HEAP", THREAD_DUMP = "THREAD";

	/**
	 * A dummy api that takes set amount of time
	 */
	@Override
	public ServerSpecificResponseJSON dummyAPIRequest(long executionTimeSec) {
		try {
			Thread.sleep(executionTimeSec*1000);
			Server srvr = Server.getInstance();
			return new ServerSpecificResponseJSON("dummyAPI returning after " + executionTimeSec + " Seconds!", srvr.getHost(), srvr.getStartedOn());
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			Utils.handleException(e);
		}
		
		return null;
	}

	/**
	 * Create a dummy worker that takes given amount of time
	 */
	@Override
	public ResponseJson dummyWorkerRequest(DummyTaskRequest taskRequest) {
		// Create WorkItem which takes executionTimeSect time to perform - in Async
		GenericJSON timespanJSON = new GenericJSON();
		timespanJSON.put("ExecutionTimeSec", taskRequest.getExecutionTimeSec());

		ResponseJson response = workItemService.addWorkItem(DummyTimespanWorker.class, timespanJSON, taskRequest.getTaskType(),
				"Dummy Timespan Workitem", "Worker that will run for " + taskRequest.getExecutionTimeSec() + " Seconds!");
		
		return response;
	}

	/**
	 * Flush audit and statistics data into database
	 */
	@Override
	public ServerSpecificResponseJSON flushServerData(ServerFlushRequest serverFlushRequest) {
		// for now just flush everything
		// -----------------------------------------------------
		// <HVN - 31-Jan-2020> Flush is commented out for the time being 
		// till we have stable audit
		//-------------------------------------------------------

//		Server.getInstance().flush(Server.AUDIT);
//		Server.getInstance().flush(Server.STATISTICS);
		
		return new ServerSpecificResponseJSON("!!! Temporarily Inactive !!! Server Data Flushing into Database!", Server.getInstance().getHost(), Server.getInstance().getStartedOn());
	}

//	/**
//	 * Get the Server Data in question
//	 */
//	@Override
//	public GenericJSON getServerLoadData() {
//		Server server = Server.getInstance();
//		GenericJSON retval =  new GenericJSON();
//		retval.put("host", server.getHost());
//		retval.put("startedOn", server.getStartedOn());
//		retval.putAll(server.getServerLoadData().toGenericJSON());
//		retval.put("historicActivities", server.getServerHistoricActivities());
//
//		
//		return retval;
//	}

	// Dump for diagnostics
	private DumpResponse dumpThread(DumpRequest request) {
		GenericJSON json = new GenericJSON();
		
		final ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
		final ThreadInfo[] threadInfos = threadMXBean.getThreadInfo(threadMXBean.getAllThreadIds(), 100);
		List<ThreadInfo> carIQThreadInfos = new ArrayList<ThreadInfo>();
		for (ThreadInfo threadInfo : threadInfos) {
			final StackTraceElement[] stackTraceElements = threadInfo.getStackTrace();
			for (final StackTraceElement stackTraceElement : stackTraceElements) {
				if (stackTraceElement.getClassName().contains("cariq")) {
					carIQThreadInfos.add(threadInfo);
					break;
				}
			}
		}
		
		json.put("threaddump", carIQThreadInfos.toArray());
		return new DumpResponse(request, Server.getInstance().getHost(), Server.getInstance().getStartedOn(), json);
	}

	private DumpResponse dumpHeap(DumpRequest dumpRequest) {
		String fileLocation = HeapDumper.dump();
		
		return new DumpResponse(dumpRequest, Server.getInstance().getHost(), Server.getInstance().getStartedOn(), GenericJSON.build("dumpFileLocation", fileLocation));
	}
	
	@Override
	public DumpResponse getDump(DumpRequest dumpRequest) {
		Server server = Server.getInstance();
		String requestedServer = dumpRequest.getServerName();
		String respondingServer = server.getHost();
		// Check if this request should be served
		if (!Strings.isNullOrEmpty(requestedServer) && !Strings.isNullOrEmpty(respondingServer)) {
			if (!respondingServer.contains(requestedServer)) { // equal or part of it
				return new DumpResponse(dumpRequest, respondingServer, server.getStartedOn(), GenericJSON.build("message", "Dump request is not for this server! Please try again..."));
			}
		}
		
		String dumpType = dumpRequest.getDumpType() == null ? THREAD_DUMP : dumpRequest.getDumpType();
		// check if it is ThreadDump or Heapdump
		switch (dumpType.toUpperCase()) {
		case HEAP_DUMP:
			return dumpHeap(dumpRequest);
		case THREAD_DUMP:
			return dumpThread(dumpRequest);

		default:
			return dumpThread(dumpRequest);
		}
	}

	@Override
	public ServerSpecificResponseJSON auditOnOff(OnOffSwitch switchOnOffRequest) {
		boolean switchActionResult = Server.getInstance().switchOnOff(switchOnOffRequest.getSwitchName(), switchOnOffRequest.getIsOn());
		
		if (switchActionResult)
			return new ServerSpecificResponseJSON("Successfully Switched " + switchOnOffRequest.getSwitchName() + " to: " +  switchOnOffRequest.getIsOn(), Server.getInstance().getHost(), Server.getInstance().getStartedOn());
		
		return new ServerSpecificResponseJSON("Unable to Switch " + switchOnOffRequest.getSwitchName() + " to: " +  switchOnOffRequest.getIsOn(), Server.getInstance().getHost(), Server.getInstance().getStartedOn());
	}

	@Override
	public AuditOnOffSwitchBoard getAuditSwitchBoard(ServerSpecificRequest serverSpecificRequest) {
		Server srvr = Server.getInstance();
		
		return new AuditOnOffSwitchBoard(srvr.getAuditSwitchBoard(), srvr.getHost(), srvr.getStartedOn());
	}

	@Override
	public ServerLoadStatistics getServerLoadStatistics() {
		return Server.getInstance().getServerLoadData();
	}

	@Override
	public ResponseJson dummyWorkerChainRequest(DummyTaskRequest taskRequest) {
		String taskDetails = taskRequest.getTaskType();
		Integer chainLength = 5;
		try {
			chainLength = Integer.parseInt(taskDetails);
		} catch (NumberFormatException e) {
	        chainLength = 5;
	    }
		
		GenericJSON timespanJSON = new GenericJSON();
		timespanJSON.put("ExecutionTimeSec", taskRequest.getExecutionTimeSec());


		List<WorkItem_In> workItems = new ArrayList<WorkItem_In>();
		for (int i = 0; i < chainLength; i++) {
			// WorkItem_In wi = new WorkItem_In(timespanJSON.toString(),
			// taskRequest.getTaskType(), DummyTimespanWorker.class.getCanonicalName(),
			// UUID.randomUUID().toString(), null, "DummyTimeSpanChain - " + (i+1),
			// "Workitem Chain Item No. " + i, null, null,
			// WorkItemStatus.CREATED.toString());
			WorkItem_In wi = new WorkItem_In(timespanJSON, taskRequest.getTaskType(), DummyTimespanWorker.class,
					"DummyTimeSpanChain - " + (i + 1), "Workitem Chain Item No. " + i, null);
			workItems.add(wi);
		}
		
		return workItemService.addWorkItemChain(workItems);
	}
}
