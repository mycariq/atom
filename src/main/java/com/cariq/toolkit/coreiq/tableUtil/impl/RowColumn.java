package com.cariq.toolkit.coreiq.tableUtil.impl;

import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.coreiq.tableUtil.interfaces.CellAddressIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.CellIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.ColumnIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.RowIfc;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

/**
 * Common base class for Row and Column both
 * @author hrishi
 *
 */
public abstract class RowColumn implements RowIfc, ColumnIfc {
	String style = null;
	int index;
	
	List<CellIfc> cells = new ArrayList<CellIfc>();
	
	public RowColumn() {
		this(null);
	}
	
	public RowColumn(String style) {
		super();
		this.style = style;
	}

	@Override
	public int getIndex() {
		return index;
	}

	@Override
	public void setIndex(int indx) {
		this.index = indx;
	}
	
	@Override
	public List<CellIfc> getCells() {
		return cells;
	}

	@Override
	public String getStyle() {
		return style;
	}

	@Override
	public void add(CellIfc cell) {
		cells.add(cell);				
	}

	@Override
	public GenericJSON toGenericJSON() {
		List<Object> cellValues = new ArrayList<Object>();
		for (CellIfc cellIfc : cells) {
			cellValues.add(cellIfc.getValue());
		}
		
		GenericJSON retVal = new GenericJSON();
		if (style != null)
			retVal.put("style", style);
		
		retVal.put("cells", cellValues);
		
		return retVal;
	}


	@Override
	public void fromGenricJSON(GenericJSON json) {
		style = Utils.getValue(String.class, json, "style");		
	}

	// Column can locate based on Index only. But if Name is given MISColumn can!
	@Override
	public CellIfc locate(ProcessingContext processingContext, CellAddressIfc cellAddress) {
		if (!isMatching(processingContext, cellAddress))
			return null;

		// This is the right column - check with Columns of all cells
		for (CellIfc cellIfc : cells) {
			// Check with the column if it matches the requested one
			if (cellIfc.getItsColumn().isMatching(processingContext, cellAddress))
				return cellIfc;
		}

		// Otherwise return null
		return null;
	}

	@Override
	public boolean isMatching(ProcessingContext processingContext, CellAddressIfc cellAddress) {
		// If address doesn't have this report, return null
		Integer addressColumnIndex = cellAddress.getColumnIdentifier().getIndex();
		
		if ( addressColumnIndex == null || addressColumnIndex == index) {
			return true;
		}
		
		return false;
	}
}
