package com.cariq.toolkit.model.async.lrt;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.StopWatch;

/**
 * Static helper class with methods to handle LRT responses
 * @author Hrishi
 *
 */
public class LRTClientHelper {
	public static String getFetchLRTBaseUrl(String webService ) { 
		return webService + "/lrt/";
	}

	public static boolean isLRT(GenericJSON response) {
		return response.containsKey(LRTJSON.LRTToken);
	}

	public static String getLRTToken(GenericJSON responseJSON) {
		return Utils.getValue(String.class, responseJSON, LRTJSON.LRTToken);
	}

	/**
	 * Blocking function to wait after the response to arrive
	 * @param lrtToken
	 * @return
	 */
	public static LRTJSON waitForLRTResponse(String lrtToken, int timeoutSecond, LRTFetcher fetcher) {
		StopWatch st = new StopWatch();
		st.start();
		while (st.getElapsedTimeSecs() < timeoutSecond) {
			LRTJSON lrt = fetcher.fetch(lrtToken);
			
			if (lrt.getResponse() != null)
				return lrt;
			
			Utils.sleep(30 * 1000); // wait for 30 seconds 
		}
		
		// timed out
		return null;
	}
}
