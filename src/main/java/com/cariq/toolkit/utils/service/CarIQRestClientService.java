package com.cariq.toolkit.utils.service;

/**
 * The Interface W4IQRestClientService.
 */
public interface CarIQRestClientService {

	/**
	 * Post with generic input param and reponse.
	 *
	 * @param <R>
	 *            the generic type for response class
	 * @param <I>
	 *            the generic type for input class
	 * @param inputJson
	 *            the input json
	 * @param url
	 *            the url
	 * @param basicAuth
	 *            the basic auth
	 * @param responseClass
	 *            the response class
	 * @return the r of type R (response class)
	 */
	<R, I> R post(I inputJson, String url, String basicAuth, Class<R> responseClass);

	/**
	 * Get with generic response .
	 *
	 * @param <R>
	 *            the generic type for Response
	 * @param url
	 *            the url
	 * @param basicAuth
	 *            the basic auth
	 * @param responseClass
	 *            the response class
	 * @return the r of type response class
	 */
	<R> R get(String url, String basicAuth, Class<R> responseClass);

	/**
	 * Delete.
	 *
	 * @param <R>
	 *            the generic type
	 * @param url
	 *            the url
	 * @param basicAuth
	 *            the basic auth
	 * @param responseClass
	 *            the response class
	 * @return the r
	 */
	<R> R delete(String url, String basicAuth, Class<R> responseClass);

	/**
	 * Put.
	 *
	 * @param <R>
	 *            the generic type
	 * @param <I>
	 *            the generic type
	 * @param inputJson
	 *            the input json
	 * @param url
	 *            the url
	 * @param basicAuth
	 *            the basic auth
	 * @param responseClass
	 *            the response class
	 * @return the r
	 */
	<R, I> R put(I inputJson, String url, String basicAuth, Class<R> responseClass);
}
