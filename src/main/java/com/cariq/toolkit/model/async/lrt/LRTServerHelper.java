//package com.cariq.toolkit.model.async.lrt;
//
//import org.quartz.Job;
//
//import com.cariq.toolkit.model.async.WorkItem;
//import com.cariq.toolkit.utils.GenericJSON;
//import com.cariq.www.model.CarIqUser;
//
///**
// * Static helper class with methods to handle LRT responses
// * @author Hrishi
// *
// */
//public class LRTServerHelper {
//	public static LRTJSON fetch(String lrtToken) {
//		return convert(WorkItem.getWorkItemByUUID(lrtToken));
//	}
//	
//	public static LRTJSON convert(WorkItem in) {
//		if (in == null)
//			return null;
//		
//		CarIqUser user = in.getItsUser();
//		return new LRTJSON(in.getUuId(), in.getName(), in.getInputJson(), in.getOutputJson(), in.getStartTime(), in.getEndTime(), user == null ? "UNKNOWN" : user.getLoginUserName(), in.getStatus().toString());
//	}
//	
//	public static String create(GenericJSON inputJson, Class<?extends Job> workerClass, CarIqUser user) {
//		WorkItem item = WorkItem.create(inputJson, WorkItem.LRT, workerClass, user);
//		item.persist();
//		return item.getUuId();		
//	}
//
//}
