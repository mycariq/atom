package com.cariq.toolkit.coreiq.tableUtil.interfaces;

import com.cariq.toolkit.utils.GenericJSON;

public interface CellValueAribiterIfc {
	boolean isApplicable(GenericJSON input);
}
