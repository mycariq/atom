package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.model.Timeline.Timeline_Out;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;


/**
 * MISDetails report to fetch KMPL details
 * 
 * @author sagar
 *
 */
public class MISDetailsExporter extends CarIQExporterTemplate {
	
	private static final String TheExporter = "MISDetailsExporter";
	private static final String description = "MISDetails report to fetch KMPL details";
	
	//Columns names for output
	private static final String KEY_Customer_Name = "Customer Name";
	private static final String KEY_Shipping_Status = "Shipping Status";
	private static final String KEY_Customer_City = "Customer City";
	private static final String KEY_Customer_Loan_Number = "Customer Loan Number";
	private static final String KEY_Disbursement_Date = "Disbursement Date";
	private static final String KEY_Entry_Date = "Entry Date";
	private static final String KEY_Shipping_Date = "Shipping Date";
	private static final String KEY_Unit_delivered_date = "Unit delivered date";
	private static final String KEY_Shipped_AWB_Number = "Shipped AWB Number";
	private static final String KEY_Hyperlink_to_Aftership = "Hyperlink to Aftership";
	private static final String KEY_Notes = "Notes";
	
	//Keys to read data from json
	private static final String KEY_Staging_customerFirstName = "Staging_customerFirstName";
	private static final String KEY_Shipment_Status = "Shipment_Status";
	private static final String KEY_Staging_customerCity = "Staging_customerCity";
	private static final String KEY_policyNumber = "policyNumber";
	private static final String KEY_Staging_policyIssueDate = "Staging_policyIssueDate";
	private static final String KEY_StagingCreatedOn = "StagingCreatedOn";
	private static final String KEY_Shipped_On = "ShippedOn";
	private static final String KEY_Delivered_On = "DeliveredOn";
	private static final String KEY_Shipment_AWB = "Shipment_AWB";
	private static final String KEY_State = "State";
	private static final String KEY_TIMELINE = "timeline";
	
	private static final String createdAfter = "CreatedAfter";
	private static final String QueryType_createdAfter = createdAfter;
	private static final String createdAfterDescription = "Policy created after given date";
	private static final String KEY_Staging_customerState = "Staging_customerState";
	
	private OnboardingExporter onboardingExporter;
	
	/**
	 * Default constructor
	 */
	public MISDetailsExporter() {
		super(TheExporter, description);
	
		onboardingExporter = new OnboardingExporter();
		
		addQueryType(QueryType_createdAfter, createdAfterDescription,
				Arrays.asList(new QueryDefinitionAttribute(createdAfter, createdAfterDescription, Date.class)));
		
		// Support roles
		addSupportedRoles(
				Arrays.asList(Utils.ADMIN_ROLE, Utils.USER_ROLE,
						Utils.ROLE_FLEET_ADMIN, Utils.ROLE_GROUP_OPERATOR, Utils.CUSTOMER_ROLE));
	}
	
	
	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		
		//Get data from onboarding exporter
		List<GenericJSON> onboardingData = onboardingExporter.doExport(queryType, json, pageNo, pageSize);
		
		List<GenericJSON> finalArr = new ArrayList<GenericJSON>();
		
		//Iterate on onboarding data and process single policy to remove unwanted data
		//and add only specific data 
		for(GenericJSON policy : onboardingData) {			
			GenericJSON processedPolicy = new GenericJSON();
			
			List<Timeline_Out> timelines = (List<Timeline_Out>) json.get(KEY_TIMELINE);
			
			processedPolicy.put(KEY_Customer_Name , policy.get(KEY_Staging_customerFirstName));
			processedPolicy.put(KEY_Shipping_Status , policy.get(KEY_Shipment_Status));
			processedPolicy.put(KEY_Customer_City , policy.get(KEY_Staging_customerCity));
			processedPolicy.put(KEY_Customer_Loan_Number , policy.get(KEY_policyNumber));
			processedPolicy.put(KEY_Disbursement_Date , policy.get(KEY_Staging_policyIssueDate));
			processedPolicy.put(KEY_Entry_Date , policy.get(KEY_StagingCreatedOn));
			processedPolicy.put(KEY_Shipping_Date , policy.get(KEY_Shipped_On));
			processedPolicy.put(KEY_Unit_delivered_date , policy.get(KEY_Delivered_On));
			processedPolicy.put(KEY_Shipped_AWB_Number , policy.get(KEY_Shipment_AWB));
			processedPolicy.put(KEY_Hyperlink_to_Aftership, Utils.getAfterShipHyperLink((String) policy.get(KEY_Shipment_AWB)));
			processedPolicy.put(KEY_Notes , "none");			
			processedPolicy.put(KEY_State, StagingAndPolicyHelper.getValue(policy, KEY_Staging_customerState));
			
			finalArr.add(processedPolicy);
		}

		return finalArr;
	}
}
