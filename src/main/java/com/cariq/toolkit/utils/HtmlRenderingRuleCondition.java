package com.cariq.toolkit.utils;

/**
 * Condition to evaluate
 * At this time I am too bored to make it polymorphic.. maybe later
 * @author hrishi
 *
 */
public class HtmlRenderingRuleCondition {
	public enum ConditionType {
		// For numbers and Dates
		GreaterThan, DateGreaterThan,
		LessThan, DateLessThan,
		Between, BetweenDates,
		// for numbers and dates and Strings
		Equal, DateEquals,
		NotEqual, DateNotEquals,
		// For Strings
		Contains, 
		NotContains, 
		IsNullOrEmpty, 
		IsNotNullOrEmpty,
	}

	private static final Object NO_DATA = "no data";

	ConditionType conditionType; // check value for condition
	Object comparisonObject1, comparisonObject2; // may be null for unary check
	
	/**
	 * @param conditionType
	 * @param comparisonObject
	 */
	public HtmlRenderingRuleCondition(ConditionType conditionType) {
		this(conditionType, null);
	}	

	/**
	 * @param conditionType
	 * @param comparisonObject
	 */
	public HtmlRenderingRuleCondition(ConditionType conditionType, Object comparisonObject) {
		this(conditionType, comparisonObject, null);
	}

	/**
	 * @param conditionType
	 * @param comparisonObject
	 */
	public HtmlRenderingRuleCondition(ConditionType conditionType, Object comparisonObject1, Object comparisonObject2) {
		this.conditionType = conditionType;
		this.comparisonObject1 = comparisonObject1;
		this.comparisonObject2 = comparisonObject2;
	}

	/**
	 * Check if condition is satisfied
	 * @param value
	 * @return
	 */
	public boolean isSatisfied(String value) {
		try {
			// check isEmpty kind of conditions
			if (comparisonObject1 == null)
				return checkUnaryConditions(value);
			if (comparisonObject2 == null)
				return checkBinaryConditions(value);
			
			return checkTernaryConditions(value);
		} catch (Exception e) {
			Utils.handleException(e);
		}
		return false;
	}

	/**
	 * @param value
	 * @return
	 * @throws Exception 
	 */
	private boolean checkTernaryConditions(String value) throws Exception {
		Object val = Utils.downCast(comparisonObject1.getClass(), value);
		
		Comparable comparableValue = null, comparableObj1 = null, comparableObj2 = null;
		if (val instanceof Comparable) {
			comparableValue = (Comparable) val;
			comparableObj1 = (Comparable) comparisonObject1;
			comparableObj2 = (Comparable) comparisonObject2;
		}
		
		switch (conditionType) {
		case Between:
			return comparableValue.compareTo(comparableObj1) < 0 && comparableValue.compareTo(comparableObj2) > 0;
		default:
			return false;
		}
				
	}

	/**
	 * @param value
	 * @return
	 * @throws Exception 
	 */
	private boolean checkBinaryConditions(String value) throws Exception {
		// Let's try runtime comparison...
		Object val = Utils.downCast(comparisonObject1.getClass(), value);
		
		Comparable comparableValue = null, comparableObj = null;
		if (val instanceof Comparable) {
			comparableValue = (Comparable) val;
			comparableObj = (Comparable) comparisonObject1;
		}
		
		switch (conditionType) {
		case Contains: // case insensitive comparison
			return value.toUpperCase().contains(comparisonObject1.toString().toUpperCase());
			
		case NotContains:
			return !value.contains(comparisonObject1.toString());
			
		case LessThan:
			return comparableValue.compareTo(comparableObj) < 0;
			
		case GreaterThan:
			return comparableValue.compareTo(comparableObj) > 0;

		case Equal:
			return val.equals(comparisonObject1);

		default:
			return false;
		}	
	}

	/**
	 * @param value
	 * @return
	 */
	private boolean checkUnaryConditions(String value) {
		// Object val = Utils.downCast(comparisonObject1.getClass(), value);
		switch (conditionType) {
		case IsNullOrEmpty:
			return value==null || value.isEmpty() || value.equals(NO_DATA);
			
		case IsNotNullOrEmpty:
			return value!=null && !value.isEmpty() && !value.equals(NO_DATA);

		default:
			return false;
		}
	}

}
