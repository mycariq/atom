package com.cariq.toolkit.coreiq.server;

import com.cariq.toolkit.utils.ResponseJson;

/**
 * The Interface TestService.
 */
/**
 * @author hrishi
 *
 */
public interface ServerAuditService {

	/**
	 * API request that takes given amount of time
	 * @param executionTimeSec
	 * @return
	 */
	public ServerSpecificResponseJSON dummyAPIRequest(long executionTimeSec);

	/**
	 * Dummy work request of given type that takes given amount of time
	 * @param dummyTaskRequest
	 * @return
	 */
	public ResponseJson dummyWorkerRequest(DummyTaskRequest dummyTaskRequest);

	/**
	 * Flush server and audit data into database
	 * @param serverFlushRequest
	 * @return
	 */
	public ServerSpecificResponseJSON flushServerData(ServerFlushRequest serverFlushRequest);

//	/**
//	 * Get Server Data of the server in question
//	 * @return
//	 */
//	public GenericJSON getServerLoadData();

	/**
	 * Get Server Data of the server in question
	 * @return
	 */
	public ServerLoadStatistics getServerLoadStatistics();

	/**
	 * Dump Thread dump or Memory Dump based on the request from requested server
	 * @param dumpRequest
	 * @return
	 */
	public DumpResponse getDump(DumpRequest dumpRequest);

	/** 
	 * Switch on or off some Audit services.
	 * @param switchOnOffRequest
	 * @return
	 */
	public ServerSpecificResponseJSON auditOnOff(OnOffSwitch switchOnOffRequest);

	/** 
	 * List of Available Audit Services and their statuses.
	 * @param switchOnOffRequest
	 * @return
	 */
	public AuditOnOffSwitchBoard getAuditSwitchBoard(ServerSpecificRequest serverSpecificRequest);

	/**
	 * Create chain of workers running in succession
	 * @param dummyTaskRequest
	 * @return
	 */
	public ResponseJson dummyWorkerChainRequest(DummyTaskRequest dummyTaskRequest);
}

