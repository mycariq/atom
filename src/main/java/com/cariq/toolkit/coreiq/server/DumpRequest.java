package com.cariq.toolkit.coreiq.server;

import com.cariq.toolkit.coreiq.server.Server;

public class DumpRequest implements ServerSpecificRequest {

	private String dumpType = "Thread"; // one of API or WorkerType
	private String serverName; // If not provided - the one which picks the request

	public DumpRequest() {}

	public DumpRequest(String dumpType) {
		this(dumpType, Server.ANY);
	}
	
	public DumpRequest(String dumpType, String serverName) {
		super();
		this.dumpType = dumpType;
		this.serverName = serverName;
	}

	public String getDumpType() {
		return dumpType;
	}

	public void setDumpType(String dumpType) {
		this.dumpType = dumpType;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
}
