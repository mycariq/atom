package com.atom.www.loader;

import java.util.List;

import com.cariq.toolkit.coreiq.exporter.ExporterWorkDefinition;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;

public class SuccessTeamMISGenerator extends MultipleExporterAsyncLoader {
	
	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("SuccessTeamMISGenerator");

	public static final String TheLoader = "SuccessTeamMISGenerator";
	

	public SuccessTeamMISGenerator() {
		super(TheLoader, "Generate Success Team Performance MIS");
		
		// Add Parameters needed to generate the MIS
	}

	@Override
	protected List<ExporterWorkDefinition> getExporterWorkerDefinitions(GenericJSON json) {
		// TODO Auto-generated method stub
		return null;
	}

}
