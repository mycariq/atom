/**
 * 
 */
package com.cariq.toolkit.utils.analytics.application;

import java.io.IOException;

import com.cariq.toolkit.coreiq.tableUtil.application.mis.MISProcessingContext;
import com.cariq.toolkit.coreiq.tableUtil.application.mis.MISReport;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.analytics.core.AnalyticsContext;
import com.cariq.toolkit.utils.analytics.core.Column;
import com.cariq.toolkit.utils.analytics.core.Record;
import com.cariq.toolkit.utils.analytics.core.RecordProcessorTemplate;
import com.cariq.toolkit.utils.jsonProcessor.JSONObjectBuilder;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.google.common.base.Strings;

/**
 * The Class AppointmentReporter.
 *
 * @author santosh
 */
public class AppointmentReporter extends RecordProcessorTemplate {

	public static final String APPOINTMENT_JSON_PATH = "AppointmentJsonPath";
	public static final String APPOINTMENT_JSON = "AppointmentJSON";

	private static final String POLICY_NUMBER = "signature";
	private int dummyCount = 0;

	private MISReport report;

	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("TimeLineReporter");

	public AppointmentReporter(AnalyticsContext context) {
		super(context);
	}

	@Override
	public void init() throws IOException {
		super.init();

		//Init the MISReport table and add to the context
		String jsonPath = getProcessorContext().get(APPOINTMENT_JSON_PATH).toString();
		if (Strings.isNullOrEmpty(jsonPath))
			Utils.handleException("AppointmentReporter.init(): Failed to get " + APPOINTMENT_JSON_PATH);

		JSONObjectBuilder builder = new JSONObjectBuilder(jsonPath);

		report = new MISReport();
		logger.debug("===> registering builder json");
		builder.register(report.getJSONLoadable());
		logger.debug("===> processing builder json");
		builder.process();
	}

	@Override
	public void close() {
		GenericJSON outputJSON = report.toGenericJSON();
		getProcessorContext().put(APPOINTMENT_JSON, Utils.getJSonString(outputJSON));
		logger.debug("===> Skipped Dummy policies from TAT: " + dummyCount);
	}

	@Override
	protected Record doProcess(Record record) throws IOException {
		try (ProfilePoint _doProcess = ProfilePoint.profileAction("AppointmentReporter_doProcess")) {
			// Placeholder - need a better pattern
			MISProcessingContext context = new MISProcessingContext();

			GenericJSON recordJSON = new GenericJSON();

			// Skip dummy policies
			String policyNumber = record.getStringValue(new Column(POLICY_NUMBER));
			if (!Strings.isNullOrEmpty(policyNumber) && policyNumber.toLowerCase().contains("dummy")) {
				dummyCount++;
				return null;
			}

			// Update the report
			recordJSON.putAll(record.jsonize());
			report.process(context, recordJSON);

			return record;
		}
	}

}
