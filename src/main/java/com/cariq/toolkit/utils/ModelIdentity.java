package com.cariq.toolkit.utils;

import java.util.List;
import java.util.Map;

public class ModelIdentity {
	final static String[] CLASS_TOKENS = {"class", "cls", "clazz", "className", "class_name"};
	final static String[] ID_TOKENS = {"id", "identity"};
	final static String SIGNATURE = "signature";
	String className;
	Long id = null;
	String signature = null;
	
	public ModelIdentity(String className, String signature) {
		super();
		this.className = className;
		this.signature = signature;
	}
	
	public ModelIdentity(String className, Long id) {
		super();
		this.className = className;
		this.id = id;
	}	
	
	public ModelIdentity(String className, String id, String signature) {
		super();
		this.className = className;
		this.id = (id == null) ? null : Long.parseLong(id);
		this.signature = signature;
	}

	public ModelIdentity(Map identityMap) throws Exception {
		CaseInsensitiveJSON map = new CaseInsensitiveJSON(identityMap);
		this.className = (String) map.getAny(CLASS_TOKENS);
		this.id = Utils.downCast(Long.class, map.getAny(ID_TOKENS));
		this.signature = (String) map.get(SIGNATURE);
		
	}

	public ModelIdentity(List identityList) throws Exception {
		int indx = 0;
		Object cls = identityList.get(indx++);
		if (cls.getClass().equals(String.class))
			className = cls.toString();
		
		Object idObj = identityList.get(indx++);
		if (idObj.getClass().equals(String.class))
			signature = idObj.toString();
		else
			id = Utils.downCast(Long.class, idObj);	
	}

	public Class getClazz() throws ClassNotFoundException {
		return Class.forName(className);
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
}
