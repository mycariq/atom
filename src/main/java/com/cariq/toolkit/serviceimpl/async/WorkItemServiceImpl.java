package com.cariq.toolkit.serviceimpl.async;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.hibernate.StaleObjectStateException;
import org.json.JSONObject;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.model.User;
import com.cariq.toolkit.model.async.CarIqJob;
import com.cariq.toolkit.model.async.WorkItem;
import com.cariq.toolkit.model.async.WorkItem.WorkItem_In;
import com.cariq.toolkit.model.async.WorkItemStatus;
import com.cariq.toolkit.service.async.WorkItemDelegateTemplate;
import com.cariq.toolkit.service.async.WorkItemService;
import com.cariq.toolkit.service.async.WorkItemTypes;
import com.cariq.toolkit.service.async.WorkItemUpdater;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.ResponseHelper;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * The Class WorkItemServiceImpl.
 */
@Service
public class WorkItemServiceImpl implements WorkItemService {

	/**
	 * The Class WorkItemUpdaterImpl.
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public static class WorkItemUpdaterImpl implements WorkItemUpdater {

		/** The work item. */
		WorkItem workItem = null;

		/**
		 * Instantiates a new work item updater impl.
		 *
		 * @param workItem
		 *            the work item
		 */
		public WorkItemUpdaterImpl(WorkItem workItem) {
			this.workItem = workItem;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.cariq.www.workitem.model.WorkItemUpdater#start()
		 */
		@Override
		@Transactional(propagation = Propagation.REQUIRES_NEW)
		public void start() {
			if (workItem.getStatus().equals(WorkItemStatus.RUNNING))
				return;

			logger.debug(">>> WorkItem: " + workItem.getName() + " Version: " + workItem.getVersion() + " Starting");

			workItem = workItem.start();
			
			logger.debug("<<< WorkItem: " + workItem.getName() + " Version: " + workItem.getVersion() + " Started");
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.cariq.www.workitem.model.WorkItemUpdater#fail(java.lang.String)
		 */
		@Transactional(propagation = Propagation.REQUIRES_NEW)
		@Override
		public void fail(String exceptionMessage) {
			try {
				if (workItem.getStatus().equals(WorkItemStatus.FAILED))
					return;
				logger.debug(
						">>> WorkItem: " + workItem.getName() + " Version: " + workItem.getVersion() + " Failing!!");
				workItem = workItem.fail(exceptionMessage);
				logger.debug(
						">>> WorkItem: " + workItem.getName() + " Version: " + workItem.getVersion() + " Failed!!");
			} catch (Exception e) {
				logger.info("Caught stale state so continuing...");
				refresh();
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.cariq.www.workitem.model.WorkItemUpdater#complete()
		 */
		@Transactional(propagation = Propagation.REQUIRES_NEW)
		@Override
		public void complete() {
			try {
				if (workItem.getStatus().equals(WorkItemStatus.COMPLETED))
					return;

				logger.debug(
						">>> WorkItem: " + workItem.getName() + " Version: " + workItem.getVersion() + " Completing");

				workItem = workItem.complete();

				logger.debug(
						"<<< WorkItem: " + workItem.getName() + " Version: " + workItem.getVersion() + " Complete");
			} catch (Exception e) {
				logger.info("Caught stale state so continuing...");
				refresh();
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.cariq.www.workitem.model.WorkItemUpdater#setPercentComplete(int)
		 */
		@Transactional(propagation = Propagation.REQUIRES_NEW)
		@Override
		public void setPercentComplete(int percentComplete) {
			try {
				int currentProgress = workItem.getProgress();

				logger.debug("!!WorkItem!! " + workItem.getName() + ", currentPercent: " + currentProgress
						+ ", newPercent: " + percentComplete);

				if (currentProgress == percentComplete) // no action
					return;

				int completedOperations = currentProgress / 100;

				currentProgress = 100 * completedOperations + percentComplete;

				logger.debug(">>> WorkItem: " + workItem.getName() + " Version: " + workItem.getVersion()
						+ ", percentCompleting: " + currentProgress);

				workItem = workItem.setPercentComplete(currentProgress);
				logger.debug("<<< WorkItem: " + workItem.getName() + " Version: " + workItem.getVersion()
						+ ", percentComplete: " + currentProgress);

			} catch (Exception e) {
				logger.info("Caught stale state so continuing...");
				refresh();
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.cariq.www.workitem.model.WorkItemUpdater#getItem()
		 */
		@Override
		public WorkItem getItem() {
			return workItem;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.cariq.www.workitem.model.WorkItemUpdater#refresh()
		 */
		@Override
		public void refresh() {
			logger.info("Refreshing work item...");
			try {
				workItem = workItem.refresh();
			} catch (Exception e) {
				logger.info("Caught exception while refreshing workitem...");
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.cariq.www.workitem.model.WorkItemUpdater#setOutputJson(java.lang.
		 * String)
		 */
		@Transactional(propagation = Propagation.REQUIRES_NEW)
		@Override
		public void setOutputJson(String outputJson) {
			try {
				if (workItem.getOutputJson() != null && workItem.getOutputJson().equals(outputJson))
					return;

				workItem.setOutputJson(outputJson);
				workItem = workItem.merge();

			} catch (Exception e) {
				logger.info("Caught stale state so continuing...");
				refresh();
			}
		}
	}

	/** The Constant CARIQ_JOB. */
	private static final String CARIQ_JOB = "CarIQ Job";

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("WorkItemServiceImpl");

	/**
	 * Instantiates a new work item service impl.
	 */
	public WorkItemServiceImpl() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.workitem.service.WorkItemService#getListOfWorkItem(java.
	 * lang.String, java.lang.String, int, int)
	 */
	@Override
	public Object getListOfWorkItem(String type, String status, int pageNo, int pageSize) {
		List<GetListOfWorkItem> items = new ArrayList<GetListOfWorkItem>();
		int startPosition = Utils.validatePagination(pageSize, pageNo);
		List<WorkItem> workItems = WorkItem.getWorkItemByTypeAndStatus(type, status, startPosition, pageSize);

		for (WorkItem workItem : workItems) {
			long seconds = Utils.getDiffInSeconds(workItem.getEndTime(), workItem.getStartTime());
			double effectiveness = calculateEffectiveness(workItem, seconds);
			String time = getTime(workItem, seconds);

			items.add(new GetListOfWorkItem(workItem.getHost(), workItem.getType(), workItem.getProgress(), workItem.getStartTime(),
					workItem.getEndTime(), workItem.getUuId(), workItem.getName(), workItem.getDescription(),
					workItem.getUserId(), workItem.getStatus(), time, effectiveness));
		}
		Long noOfRecords = WorkItem.countWorkItemsIfStatusEqualsAndStatusEquals(type, status);
		int totalPages = Utils.totalPageCount(noOfRecords, pageSize);
		return new PaginationJson<List<GetListOfWorkItem>>(startPosition, noOfRecords, totalPages, items);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.workitem.service.WorkItemService#getWorkItemByUUID(java.
	 * lang.String)
	 */
	@Override
	public Object getWorkItemByUUID(String uuid) {
		WorkItem workItem = WorkItem.getWorkItemByUUID(uuid);
		if (workItem == null)
			return ResponseHelper.setResponse("No workItem found!");
		return new GetWorkItem(workItem.getType(), workItem.getProgress(), workItem.getStartTime(),
				workItem.getEndTime(), workItem.getUuId(), workItem.getWorkerClass(), workItem.getInputJson(),
				workItem.getOutputJson(), workItem.getStatus(), workItem.getName(), workItem.getDescription(),
				workItem.getUserId(), workItem.getFailureCause(), workItem.getHost());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.workitem.service.WorkItemService#getListOfWorkItemByType(
	 * java.lang.String, int, int)
	 */
	@Override
	public Object getListOfWorkItemByType(String type, int pageNo, int pageSize) {
		List<GetListOfWorkItem> items = new ArrayList<GetListOfWorkItem>();
		int startPosition = Utils.validatePagination(pageSize, pageNo);
		List<WorkItem> workItems = WorkItem.getWorkItemByType(type, startPosition, pageSize);

		for (WorkItem workItem : workItems) {
			long seconds = Utils.getDiffInSeconds(workItem.getEndTime(), workItem.getStartTime());
			double effectiveness = calculateEffectiveness(workItem, seconds);
			String time = getTime(workItem, seconds);

			items.add(new GetListOfWorkItem(workItem.getHost(), workItem.getType(), workItem.getProgress(), workItem.getStartTime(),
					workItem.getEndTime(), workItem.getUuId(), workItem.getName(), workItem.getDescription(),
					workItem.getUserId(), workItem.getStatus(), time, effectiveness));
		}
		Long noOfRecords = WorkItem.countWorkItemsIfTypeEquals(type);
		int totalPages = Utils.totalPageCount(noOfRecords, pageSize);
		return new PaginationJson<List<GetListOfWorkItem>>(startPosition, noOfRecords, totalPages, items);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.www.workitem.service.WorkItemService#getListOfWorkItemByStatus(
	 * java.lang.String, int, int)
	 */
	@Override
	public Object getListOfWorkItemByStatus(String status, int pageNo, int pageSize) {
		List<GetListOfWorkItem> items = new ArrayList<GetListOfWorkItem>();
		int startPosition = Utils.validatePagination(pageSize, pageNo);
		List<WorkItem> workItems = WorkItem.getWorkItemByStatus(status, startPosition, pageSize);

		for (WorkItem workItem : workItems) {
			long seconds = Utils.getDiffInSeconds(workItem.getEndTime(), workItem.getStartTime());
			double effectiveness = calculateEffectiveness(workItem, seconds);
			String time = getTime(workItem, seconds);

			items.add(new GetListOfWorkItem(workItem.getHost(), workItem.getType(), workItem.getProgress(), workItem.getStartTime(),
					workItem.getEndTime(), workItem.getUuId(), workItem.getName(), workItem.getDescription(),
					workItem.getUserId(), workItem.getStatus(), time, effectiveness));
		}
		Long noOfRecords = WorkItem.countWorkItemsIfStatusEquals(status);
		int totalPages = Utils.totalPageCount(noOfRecords, pageSize);
		return new PaginationJson<List<GetListOfWorkItem>>(startPosition, noOfRecords, totalPages, items);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.workitem.service.WorkItemService#getListOfWorkItem(int,
	 * int)
	 */
	@Override
	public Object getListOfWorkItem(int pageNo, int pageSize) {
		// CarIqUser user = CarIqValidator.validateUser();
		List<GetListOfWorkItem> items = new ArrayList<GetListOfWorkItem>();
		int startPosition = Utils.validatePagination(pageSize, pageNo);
		List<WorkItem> workItems = WorkItem.getWorkItems(pageNo, pageSize);

		for (WorkItem workItem : workItems) {
			long seconds = Utils.getDiffInSeconds(workItem.getEndTime(), workItem.getStartTime());
			double effectiveness = calculateEffectiveness(workItem, seconds);
			String time = getTime(workItem, seconds);

			items.add(new GetListOfWorkItem(workItem.getHost(), workItem.getType(), workItem.getProgress(), workItem.getStartTime(),
					workItem.getEndTime(), workItem.getUuId(), workItem.getName(), workItem.getDescription(),
					workItem.getUserId(), workItem.getStatus(), time, effectiveness));
		}
		Long noOfRecords = WorkItem.countWorkItems();
		int totalPages = Utils.totalPageCount(noOfRecords, pageSize);
		return new PaginationJson<List<GetListOfWorkItem>>(startPosition, noOfRecords, totalPages, items);
	}

	/**
	 * Gets the time.
	 *
	 * @param workItem
	 *            the work item
	 * @param seconds
	 *            the seconds
	 * @return the time
	 */
	private String getTime(WorkItem workItem, long seconds) {
		if (!WorkItemStatus.RUNNING.equals(workItem.getStatus()))
			return Utils.convertSecondsToTime(seconds);
		return null;
	}

	/**
	 * Calculate effectiveness.
	 *
	 * @param workItem
	 *            the work item
	 * @param seconds
	 *            the seconds
	 * @return the double
	 */
	private double calculateEffectiveness(WorkItem workItem, long seconds) {
		if (CARIQ_JOB.equalsIgnoreCase(workItem.getType()) && seconds > 0
				&& !WorkItemStatus.RUNNING.equals(workItem.getStatus())) {
			CarIqJob job = CarIqJob.findJobByJobName(workItem.getName());
			long frequency = Utils.convertFrequencyToSeconds(job.getDefaultFrequency(), job.getFrequencyUnit());

			return (frequency / seconds) * 100;
		}
		return 0;
	}

	// public WorkItemStatus getStatus() {
	// logger.debug("[CALLED],[getStatus],\"getStatus()\"");
	// return workItem.getStatus();
	// }
	//
	// public void setStatus(WorkItemStatus status) {
	// logger.debug("[CALLED],[setStatus],\"setStatus(status=" + status
	// + ")====" + workItem.getId() + "\"");
	//// if (status == WorkItemStatus.COMPLETED)
	//// setEndTime();
	//// if (status == WorkItemStatus.RUNNING)
	//// setStartTime();
	// workItem.setStatus(status);
	//// workItem = update();
	// }

	// private void setEndTime() {
	// logger.debug("[CALLED],[setEndTime],\"setEndTime()\"");
	// workItem.setEndTime(new Date());
	// }
	//
	// private void setStartTime() {
	// logger.debug("[CALLED],[setStartTime],\"setStartTime()\"");
	// workItem.setStartTime(new Date());
	// }

	// public int getProgress() {
	// logger.debug("[CALLED],[getProgress],\"getProgress()\"");
	// return workItem.getProgress();
	// }
	//
	// public void setProgress(int progress) {
	// logger.debug("[CALLED],[setProgress],\"setProgress(progress="
	// + progress + ")\"");
	// workItem.setProgress(progress);
	//// workItem = update();
	// }
	//
	// public WorkItem getWorkItem() {
	// logger.debug("[CALLED],[getWorkItem],\"getWorkItem()\"");
	// return workItem;
	// }
	//
	// public void setOutput(JSONObject output) {
	// logger.debug("[CALLED],[setOutput],\"setOutput(output=" + output
	// + ")\"");
	// workItem.setOutputJson(output.toString());
	//// workItem = update();
	// }
	//
	//// private WorkItem update() {
	//// return workItem.merge();
	//// }
	//
	// public Date getStartTime() {
	// logger.debug("[CALLED],[getStartTime],\"getStartTime()\"");
	// return workItem.getStartTime();
	// }
	//
	// public Date getEndTime() {
	// logger.debug("[CALLED],[getEndTime],\"getEndTime()\"");
	// return workItem.getEndTime();
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.workitem.service.WorkItemService#getListOfWorkItemType()
	 */
	@Override
	public Object getListOfWorkItemType() {
		List<GetListOfType> listOfTypes = new ArrayList<GetListOfType>();
		WorkItemTypes[] types = WorkItemTypes.values();
		for (WorkItemTypes workItemTypes : types) {
			listOfTypes.add(new GetListOfType(workItemTypes.name()));
		}

		for (String workItemType : WorkItem.getAllTypes()) {
			listOfTypes.add(new GetListOfType(workItemType));
		}
		return listOfTypes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.workitem.service.WorkItemService#getListOfWorkItemStatus()
	 */
	@Override
	public Object getListOfWorkItemStatus() {
		List<GetListOfWorkItemStatus> itemStatus = new ArrayList<GetListOfWorkItemStatus>();
		WorkItemStatus[] status = WorkItemStatus.values();
		for (WorkItemStatus workItemStatus : status) {
			itemStatus.add(new GetListOfWorkItemStatus(workItemStatus.name()));
		}
		return itemStatus;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.workitem.service.WorkItemService#addWorkItem(java.lang.
	 * String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, com.cariq.www.model.CarIqUser)
	 */
	@Override
	public ResponseJson addWorkItem(String workerClass, String inputJson, String type, String name, String descriptions,
			User user) {
		return addWorkItem(workerClass, inputJson, type, name, descriptions, user, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.workitem.service.WorkItemService#addWorkItem(java.lang.
	 * String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, com.cariq.www.model.CarIqUser)
	 */
	@Override
	public ResponseJson addWorkItem(String workerClass, String inputJson, String type, String name, String descriptions) {
		return addWorkItem(workerClass, inputJson, type, name, descriptions, null, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.workitem.service.WorkItemService#addWorkItem(java.lang.
	 * String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, com.cariq.www.model.CarIqUser)
	 */
	@Override
	public <T> ResponseJson addWorkItem(Class<? extends WorkItemDelegateTemplate> workerClass, T inputPOJO, String type,
			String name, String description) {
		return addWorkItem(workerClass.getCanonicalName(), Utils.getJSonString(inputPOJO), type, name, description,
				null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.fleetiq.www.service.toolkit.async.WorkItemService#addWorkItem(java.lang.
	 * String, com.cariq.toolkit.utils.GenericJSON, java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public ResponseJson addWorkItem(String workerClass, GenericJSON inputJson, String type, String name,
			String descriptions, User user) {
		return addWorkItem(workerClass, Utils.getJSonString(inputJson), type, name, descriptions, user);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.www.workitem.service.WorkItemService#addWorkItem(java.lang.Class,
	 * java.lang.Object, java.lang.String, java.lang.String, java.lang.String,
	 * com.cariq.www.workitem.model.WorkItem)
	 */
	@Override
	public <T> ResponseJson addWorkItem(Class<? extends WorkItemDelegateTemplate> workerClass, T inputPOJO, String type,
			String name, String descriptions, WorkItem parent) {
		return addWorkItem(workerClass.getCanonicalName(), Utils.getJSonString(inputPOJO), type, name, descriptions,
				null, parent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.www.workitem.service.WorkItemService#addWorkItem(java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * com.cariq.www.model.CarIqUser, com.cariq.www.workitem.model.WorkItem)
	 */
	@Override
	public ResponseJson addWorkItem(String workerClass, String inputJson, String type, String name, String descriptions,
			User user, WorkItem parent) {
		String uuId = UUID.randomUUID().toString();
		new WorkItem(inputJson, type, workerClass, uuId, user, name, descriptions, parent).persist();
		return new ResponseJson("Your task is accepted. We will notify you on completion of this task.");
	}

	@Override
	public ResponseJson addWorkItem(WorkItem_In workItem_In) {
		WorkItem item = Utils.constructJSONable(WorkItem.class, workItem_In);
		item.persist();
		
		return new ResponseJson("Your task is accepted. We will notify you on completion of this task.");
	}

	@Override
	public ResponseJson addWorkItemChain(List<WorkItem_In> workItemList) {
		WorkItem previous = null;
		List<WorkItem> chain = new ArrayList<WorkItem>();
		// Go over all the items - But don't save - save at last
		for (WorkItem_In workItem_In : workItemList) {
			WorkItem item = Utils.constructJSONable(WorkItem.class, workItem_In);
			if (previous != null) {
				item.setStatus(WorkItemStatus.CREATED);

				// set the successor for all except last item
				previous.setSuccessor(item);
			} else {
				// First Item should be READY
				item.setStatus(WorkItemStatus.READY);
			}

			// assign previous
			previous = item;
			chain.add(item);
		}
		
		// Now save everything in reverse order - to resolve the dependency
		Collections.reverse(chain);
		for (WorkItem workItem : chain) {
			workItem.persist();
		}
		
		return new ResponseJson("Your task is accepted. We will notify you on completion of this task.");
	}
	
	
	
	// =================================================
	// Scheduling
	// =================================================
	/**
	 * Every 47 seconds (prime no) look for any "Due" job and make it into a
	 * workitem
	 */
	@Scheduled(fixedDelay = 5000) // 5 seconds for test purpose
	// @Scheduled(fixedDelay = 30000)
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void scheduleJobs() {
		try (ProfilePoint _scheduleJob = ProfilePoint.profileActivity("ProfActivity_scheduleJob")) {
			List<CarIqJob> jobsData = CarIqJob.findAllCarIqJobs();
//			logger.info("Retriving Jobs from Database and Scheduling One by One | Total Number of Jobs: "
//					+ jobsData.size());
			for (CarIqJob carIqJob : jobsData) {
				if (!carIqJob.isDue())
					continue;

				logger.debug("Job ready to run: " + carIqJob.getJobName());

				try {
					carIqJob = carIqJob.start(); // May give stale state and it
													// is fine.
				} catch (StaleObjectStateException exc) {
					logger.warn("Job: " + carIqJob.getJobName() + " is already picked by different process!");
					continue; // go to next job
				}

				try {
					JSONObject json = new JSONObject();
					json.put(CarIQJobExecutor.JOBCLASS, carIqJob.getJobName());
					ResponseJson response = addWorkItem(CarIQJobExecutor.class.getName(),
							json.toString(), "CarIQ Job", carIqJob.getJobName(), carIqJob.getJobName());
					logger.info(response + " Created for job: " + carIqJob.getClass().getName());
				} catch (Exception e) {
					Utils.logException(logger, e, "Staging Job");
				}

			}
		}
	}

	/**
	 * @throws Exception
	 * @TODO Think of changing it to WorkItemExecutorImpl - using the Bean
	 *       mechanism in applicationcontext.xml Every 5 second pick the next
	 *       undone workitem and fire its execution through schedular
	 */
	@Scheduled(fixedDelay = 5000)
	// @Scheduled(fixedDelay = 20000)
	@Transactional(propagation = Propagation.NEVER)
	public void executeWorkItem() throws Exception {
		try (ProfilePoint _executeWorkItem = ProfilePoint.profileActivity("ProfActivity_executeWorkItem")) {
			// pick 1 work item at time and execute
			WorkItemDispenser dispenser = new WorkItemDispenser(
					WorkItem.CARIQ_JOB, 3, 
					WorkItem.COMPOUND, 2, 
					WorkItem.SIMPLE, 2,
					WorkItem.LONG, 1, 
					WorkItem.MICRO, 10, 
					WorkItem.NOTA, 4);
			
			for (WorkItem workItem : dispenser) {
				// if worker exists, process - else skip
				if (!Utils.checkClassExists(workItem.getWorkerClass()))
					continue;
				
				WorkItemUpdater updater = new WorkItemServiceImpl.WorkItemUpdaterImpl(workItem);
				try {
					// set this work item as ready - so that there is no
					// collision
					updater.start();
				} catch (StaleObjectStateException exc) { // is fine
					logger.warn("Job: " + updater.getItem().getName() + " is already picked by different process!");
					continue; // go to next job
				}

				try {
					executeWorkItem(updater);
				} catch (Exception e) {
					String exceptionMessage = Utils.getExceptionMessage(e);
					updater.fail(exceptionMessage);
					Utils.logException(logger, e, "Execute WorkItem: " + updater.getItem().getName());
					throw e;
				}
				workItem = updater.getItem(); // to avoid dirtying session with
												// stale object
			}
		}
	}

	private void executeWorkItem(WorkItemUpdater updater) throws Exception {
		// Schedule the job
		Scheduler scheduler = new StdSchedulerFactory().getScheduler();
		if (!scheduler.isStarted()) {
			scheduler.start();
		}

		JobDetail jobDetail = newJob(updater.getItem().getWorker()).withIdentity(updater.getItem().getName())
				.usingJobData(getJobDataMap(updater)).build();

		// clear earlier jobs and load a fresh as recommended
		// here:http://stackoverflow.com/questions/24747296/restarting-quartz-scheduler-without-getting-an-error
		scheduler.clear();
		scheduler.scheduleJob(jobDetail, getTrigger(updater.getItem(), new Date()));
	}

	private JobDataMap getJobDataMap(WorkItemUpdater updater) {
		JobDataMap jobDataMap = new JobDataMap();
//		// @TODO make these keys CONSTANTS in CarIqJob class
//		jobDataMap.put("notificationService", notificationService);
//		jobDataMap.put("carService", carService);
//		jobDataMap.put("velocityEngine", velocityEngine);
		jobDataMap.put(CarIQJobExecutor.INPUT_JSON, updater.getItem().getInputJson());
		jobDataMap.put(CarIQJobExecutor.UPDATER, updater);
		return jobDataMap;
	}

	// Create a trigger for current time - so that execution is taken
	// immediately
	private Trigger getTrigger(WorkItem item, Date expectedDate) {
		System.out.println("Created Trigger for " + item.getName() + " : " + expectedDate);
		SimpleTrigger simpleTrigger = newTrigger().withIdentity(item.getName() + "Trigger").startAt(expectedDate)
				.withSchedule(simpleSchedule()).build();
		return simpleTrigger;
	}
}
