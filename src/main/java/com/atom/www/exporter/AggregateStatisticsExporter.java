package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.cariq.toolkit.coreiq.exporter.CarIQExporterWrapper;
import com.cariq.toolkit.coreiq.exporter.CarIQWrappedExporterTemplate;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * The Class AggregateStatisticsExporter.
 *
 * @author nitin
 */
public class AggregateStatisticsExporter extends CarIQWrappedExporterTemplate {

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("AggregateStatisticsExporter");

	/** The Constant EXPORTER. */
	public static final String EXPORTER = "AggregateStatisticsExporter";

	/** The Constant EXPORTER_DESCRIPTION. */
	private static final String EXPORTER_DESCRIPTION = "This is AggregateStatistics Exporter";

	/** The Constants */
	public static final String BY_STATE = "ByState", DOMAIN_ID = "domainId", DOMAIN_NAME = "domainName",
			CONTEXT = "context", VALUE_JSON = "valueJson", DOMAIN = "domain";

	/**
	 * Instantiates a new aggregate statistics exporter.
	 */
	public AggregateStatisticsExporter() {
		super(EXPORTER, EXPORTER_DESCRIPTION);

		// Support roles
		addSupportedRoles(
				Arrays.asList(Utils.ADMIN_ROLE, Utils.USER_ROLE, Utils.ROLE_FLEET_ADMIN, Utils.ROLE_GROUP_OPERATOR));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.toolkit.coreiq.exporter.CarIQWrappedExporterTemplate#getWrapper()
	 */
	@Override
	protected CarIQExporterWrapper getWrapper() {
		CarIQExporterWrapper wrapper = new CarIQExporterWrapper(EXPORTER, EXPORTER);
		return wrapper;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.toolkit.coreiq.exporter.CarIQWrappedExporterTemplate#doExport(java.
	 * lang.String, com.cariq.toolkit.utils.GenericJSON, int, int)
	 */
	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		logger.debug("doExport - queryType : " + queryType + " pageNo : " + pageNo + " pageSize : " + pageSize
				+ " json :" + json);
		try (ProfilePoint _doExport_ = ProfilePoint
				.profileAction("ProfAction_doExport_" + this.getClass().getCanonicalName())) {
			// for wrapper call.
			return super.doExport(queryType, json, pageNo, pageSize);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.toolkit.coreiq.exporter.CarIQWrappedExporterTemplate#postProcess(
	 * java.lang.String, com.cariq.toolkit.utils.GenericJSON, java.util.List)
	 */
	@Override
	protected List<GenericJSON> postProcess(String queryType, GenericJSON inputJson, List<GenericJSON> outputJson) {
		logger.debug("postProcess - queryType : " + queryType + " inputJson : " + inputJson + " outputJson size :"
				+ outputJson.size());
		
		// TODO : For all domains we need to separate the json list based on domainId.

		// get domainId from first json
		Integer domainId = Utils.getValue(Integer.class, outputJson.get(0), DOMAIN_ID);
		// remove domainId and domainName from all json.
		for (GenericJSON json : outputJson) {
			json.remove(DOMAIN_ID);
			json.remove(DOMAIN_NAME);
		}

		List<GenericJSON> retVal = new ArrayList<GenericJSON>();
		GenericJSON json = new GenericJSON();
		json.add(CONTEXT, queryType);
		json.add(VALUE_JSON, outputJson.toString());
		json.add(DOMAIN, domainId);

		retVal.add(json);

		return retVal;
	}
}