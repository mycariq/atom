package com.cariq.toolkit.utils;

import java.util.ArrayList;
import java.util.List;

public class CompositeHtmlRenderingRule implements HtmlRenderingRule, ObjectBuilder<HtmlRenderingRule> {
	List<HtmlRenderingRule> renderingRules = new ArrayList<HtmlRenderingRule>();
	String key;
	
	CompositeHtmlRenderingRule(String key) {
		this.key = key;
	}
	
	@Override
	public String apply(String key, String value) {
		String retval = value;
		for (HtmlRenderingRule htmlRenderingRule : renderingRules) {
			retval = htmlRenderingRule.apply(key, retval);
		}
		return retval;
	}

	@Override
	public String getKey() {
		return key;
	}

	@Override
	public CompositeHtmlRenderingRule configure(String setting, Object value) {
		return this;
	}

	@Override
	public CompositeHtmlRenderingRule add(HtmlRenderingRule value) {
		if (value.getKey() != getKey())
			Utils.handleException("Missmatched keys in renderer - expected: " + getKey() + " Found: " + key);

		renderingRules.add(value);
		return this;
	}

	@Override
	public CompositeHtmlRenderingRule add(String parameter, HtmlRenderingRule value) {
		if (value.getKey() != getKey())
			Utils.handleException("Missmatched keys in renderer - expected: " + getKey() + " Found: " + key);
		renderingRules.add(value);
		return this;
	}

	@Override
	public CompositeHtmlRenderingRule build() {
		return this;
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.utils.HtmlRenderingRule#isSatisfied(java.lang.String)
	 */
	@Override
	public boolean isSatisfied(String value) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("HtmlRenderingRule.isSatisfied() is not implemented");
	}

}
