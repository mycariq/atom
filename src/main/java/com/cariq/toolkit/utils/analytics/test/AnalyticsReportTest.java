package com.cariq.toolkit.utils.analytics.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.utils.analytics.application.WeeklyMISReporter;
import com.cariq.toolkit.utils.analytics.core.AnalyticsContext;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsCookbook;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsProcess;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsRecipe;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
public class AnalyticsReportTest {
//	// /**
//	// * Timestamp class with notion of timezone
//	// * @author Abhijit
//	// *
//	// */
//	// static class CarIQDateTime {
//	// DateTime dateTime = new DateTime()
//	//
//	// }
//
//	//@Test
//	@Transactional
//	public void testGearDetection() {
//		ProfilePoint.setRenderer(new ProfilePointLogRenderer());
//		try (ProfilePoint _testMean = ProfilePoint.profileActivity("ProfActivity_testGearDetection")) {
//			// String csvfile =
//			// "D:\\projects\\Architecture\\analytics\\ola.csv";
//			String csvfile = "D:\\projects\\Architecture\\analytics\\cardekho\\data.csv";
//			// String report =
//			// "D:\\temp\\analytics\\scorpio_mumbai_report.html";
//
//			AnalyticsWorker worker = new DriverAnalysis("DriverAnalysis-" + Utils.getTimeEpoch(new Date()),
//					"Calculate avarage speed, Acceleration, Idling, Gear, etc.. ", csvfile, Car.getById(6), new Date(),
//					new Date());
//			worker.execute();
//		}
//	}
//
//	//@Test
//	@Transactional
//	public void testAnalyticsProcessingFromFile() {
//		ProfilePoint.setRenderer(new ProfilePointLogRenderer());
//		try (ProfilePoint _testMean = ProfilePoint.profileActivity("ProfActivity_testAnalyticsProcessingFromFile")) {
//			// String csvfile =
//			// "D:\\projects\\Architecture\\analytics\\ola.csv";
//			String csvfile = "D:\\temp\\ajay\\ajay\\data.csv";
//			// String csvfile = "D:\\temp\\omni\\omni\\data.csv";
//			// String report =
//			// "D:\\temp\\analytics\\scorpio_mumbai_report.html";
//
//			AnalyticsWorker worker = new DriverAnalysis("DriverAnalysis-" + Utils.getTimeEpoch(new Date()),
//					"Do Analytics Report from file ", csvfile, Car.getById(6), new Date(), new Date());
//			worker.execute();
//		}
//	}
//
//	//@Test
//	// @Transactional
//	public void testAnalyticsReportCreation() throws InterruptedException {
//		// Request for a report for Sagar's (id=7) Scorpio(id = 6)
//		ReportServiceImpl reportService = new ReportServiceImpl();
//		// {
//		// Object response = reportService.createReport(15, "2016-09-04
//		// 00:00:00", "2016-09-10 00:00:00", "ANALYTICS",
//		// CarIqUser.findCarIqUser((long) 19));
//		// System.out.println("Response =" + response);
//		// }
//
//		{
//			Object response = reportService.createReport(16, "2016-09-04 00:00:00", "2016-09-20 00:00:00", "ANALYTICS",
//					CarIqUser.findCarIqUser((long) 20));
//
//			System.out.println("Response =" + response);
//		}
//
//		// Sleep for 200 seconds
//		Thread.sleep(5000000);
//	}
//
//	//@Test
//	public void timeZoneTest() {
//		System.out.println(Utils.getDateTimeString(new Date(), TimeZone.getTimeZone(Utils.INDIA_TIMEZONE)));
//		DateTime now = new DateTime();
//		DateTime tenHoursBefore = now.minusHours(10);
//		// boolean sameDate = Utils.sameDate(now.toDate(),
//		// tenHoursBefore.toDate(),
//		// TimeZone.getTimeZone(Utils.INDIA_TIME_ZONE));
//		boolean sameDate = Utils.sameDate(now.toDate(), tenHoursBefore.toDate(), TimeZone.getTimeZone("UTC"));
//		System.out.println("Times: " + now + " and " + tenHoursBefore + " is sameDate ? : " + sameDate);
//		// DateTimeFormatter fmt = DateTimeFormat.fullDateTime();
//		// java.util.TimeZone zone =
//		// TimeZone.getTimeZone(Utils.INDIA_TIME_ZONE);
//		// DateTime india = new DateTime(DateTimeZone.forTimeZone(zone));
//		// System.out.println("India time");
//		// System.out.println(fmt.print(india));
//		// System.out.println(india.toDate());
//		// DateTime utc = new DateTime(DateTimeZone.UTC);
//		// System.out.println("UTC time");
//		//
//		// System.out.println(fmt.print(utc));
//		// System.out.println(utc.toDate());
//
//	}
//
//	@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
//	@Configurable
//	public static class TestDriveAnalysis extends AnalyticsWorker {
//		private static CarIQLogger logger = WIHelper.getLogger("Test DriveAnalysis");
//
//		// Car car;
//		// Date startTime, endTime;
//		@Autowired
//		private VelocityEngine velocityEngine;
//
//		List<CarIQFileDesc> reportFiles = new ArrayList<CarIQFileDesc>();
//
//		private static final String VelocityTemplate = "velocity/drive_report.vm";
//
//		// load config properties
//		@Resource(name = "configProperties")
//		private Properties configProperties;
//
//		public TestDriveAnalysis(String id, String description, String inputFile, Car car, Date startTime,
//				Date endTime) {
//			super(id, description, inputFile);
//			// this.car = car;
//			// this.startTime = startTime;
//			// this.endTime = endTime;
//			getContext().put(DriveReportHelper.CAR, car);
//			getContext().put(DriveReportHelper.TIME_ZONE, car.getOwner().getTimeZoneName());
//			getContext().put(DriveReportHelper.START_TIME, startTime);
//			getContext().put(DriveReportHelper.END_TIME, endTime);
//
//			// Get Milage and Fuel Cost from DB
//			Country itsCountry = car.getOwner().getItsCountry();
//			if (itsCountry == null)
//				itsCountry = Country.findCountry((long) 1);
//
//			double fuelPrice = Utils.FUEL_PRICE;
//			double milage = Utils.MILAGE;
//
//			if (car.getItsModel() != null && car.getItsModel().getFuelType() != null && itsCountry != null)
//				fuelPrice = Fuel.getFuelCostByFuelTypeAndItsCountry(car.getItsModel().getFuelType(), itsCountry);
//			else
//				logger.warn("!!Assuming default fuelPrice for car: " + car.getNickName());
//
//			if (car.getItsModel() != null && car.getItsModel().getMilage() != null)
//				milage = car.getItsModel().getMilage();
//			else
//				logger.warn("!!Assuming default Milage for car: " + car.getNickName());
//
//			getContext().put(DriveReportHelper.MILAGE, milage); // 15 kmpl -
//																// temp - take
//																// it from car
//			getContext().put(DriveReportHelper.FUELPRICE, fuelPrice); // Rs. 70
//																		// -
//																		// Take
//																		// it
//																		// from
//																		// global
//																		// properties
//		}
//
//		/**
//		 * Main function to do the multi-pass analytics.kha
//		 * 
//		 */
//		@Override
//		protected void doExecute() throws Exception {
//			AnalyticsRecipe recipe = AnalyticsCookbook.getInstance().getRecipe("DriveAnalysis");
//
//			try (AnalyticsProcess process = new AnalyticsProcess(recipe, getContext(), getInputFile())) {
//				process.execute();
//
//				String reportHtml = generateReport();
//				reportFiles.add(new CarIQFileDesc("DriveAnalysis.html", reportHtml));
//
//				// if DEBUG mode, save csv file and Context details in the zip
//				if (!getContext().isDebug())
//					return;
//
//				reportFiles.add(new CarIQFileDesc("data.csv", getInputFile()));
//
//				String contextFile = CarIQFileUtils.getTempFile(getId() + "-context.txt");
//				CarIQFileUtils.saveToFile(contextFile, getContext().toString());
//				reportFiles.add(new CarIQFileDesc("context.txt", contextFile));
//			}
//		}
//
//		@Override
//		protected void init() {
//			DriveReportModel model = DriveReportHelper.getDriveReportModel(getContext());
//			Car car = (Car) getContext().get(DriveReportHelper.CAR);
//			// Set Debug or not
//			getContext().setDebug(false); // do it based on car
////			if (Utils.isTestCar(configProperties, car))
////				getContext().setDebug(true); // do it based on car
//
//			Date startTime = (Date) getContext().get(DriveReportHelper.START_TIME);
//			Date endTime = (Date) getContext().get(DriveReportHelper.END_TIME);
//			// model.add(DriveReportModel.CAR_NICK_NAME, car.getNickName());
//			// model.add(DriveReportModel.CAR_REGISTRATION_NUMBER,
//			// car.getRegistrationNumberForDisplay());
//			// model.add(DriveReportModel.CAR_TOTAL_DIST,
//			// String.valueOf(car.getTotalKmCovered()));
//			model.add(DriveReportModel.CAR, car);
//			String imageUrl = Utils.getImageUrl(car.getImageName());
//			model.add(DriveReportModel.CAR_IMAGE_URL,
//					imageUrl.isEmpty() ? DriveReportHelper.DEFAULT_CAR_IMAGE_URL : imageUrl);
//			model.add(DriveReportModel.START_DATE, Utils.getDateFormateForEmail(startTime, null));
//			model.add(DriveReportModel.END_DATE, Utils.getDateFormateForEmail(endTime, null));
//			model.add(DriveReportModel.REPORT_DATE, Utils.getUserTimeStamp(new Date(), null));
//
//			// more to follow
//		}
//
//		/*
//		 * (non-Javadoc)
//		 * 
//		 * @see com.cariq.toolkit.utils.analytics.core.AnalyticsWorker#close()
//		 */
//		@Override
//		protected void close() {
//
//		}
//
//		/**
//		 * Return the map from the DriveReportModel
//		 */
//		private Map<String, Object> getReportModel() {
//			return DriveReportHelper.getDriveReportModel(getContext()).build().getMap();
//		}
//
//		/**
//		 * Add Report files so that those can be zipped.
//		 */
//		@Override
//		public List<CarIQFileDesc> getReportFiles() {
//			return reportFiles;
//		}
//
//		private String generateReport() throws IOException {
//			/**
//			 * Generate report based on velocity Engine Derived classes can
//			 * override this method.
//			 * 
//			 * @throws IOException
//			 */
//			String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, VelocityTemplate, "UTF-8",
//					getReportModel());
//
//			return CarIQFileUtils.saveToFile(CarIQFileUtils.getTempFile(getId() + "-report.html"), text);
//		}
//	}
//
//	@Transactional
//	//@Test
//	public void testAnalyticsThroughCode() throws Exception {
//		String inputFile = "D:\\temp\\ajay\\ajay\\data.csv";
//
//		// AnalyticsWorker worker = new TestDriveAnalysis("DriverAnalysis-" +
//		// Utils.getTimeEpoch(new Date()),
//		// "Do Analytics Report from file ", inputFile, Car.getById(6), new
//		// Date(), new Date());
//
//		AnalyticsWorker worker = new DriverAnalysisNew("DriverAnalysis-" + Utils.getTimeEpoch(new Date()),
//				"Do Analytics Report from file ", inputFile, Car.getById(6), new Date(), new Date());
//		worker.execute();
//
//	}
//
//	void populateContext(AnalyticsContext ctx, Car car, Date startTime, Date endTime) {
//		CarIQLogger logger = WIHelper.getLogger("Test DriveAnalysis");
//
//		ctx.put(DriveReportHelper.CAR, car);
//		ctx.put(DriveReportHelper.TIME_ZONE, car.getOwner().getTimeZoneName());
//		ctx.put(DriveReportHelper.START_TIME, startTime);
//		ctx.put(DriveReportHelper.END_TIME, endTime);
//
//		// Get Milage and Fuel Cost from DB
//		Country itsCountry = car.getOwner().getItsCountry();
//		if (itsCountry == null)
//			itsCountry = Country.findCountry((long) 1);
//
//		double fuelPrice = Utils.FUEL_PRICE;
//		double milage = Utils.MILAGE;
//
//		if (car.getItsModel() != null && car.getItsModel().getFuelType() != null && itsCountry != null)
//			fuelPrice = Fuel.getFuelCostByFuelTypeAndItsCountry(car.getItsModel().getFuelType(), itsCountry);
//		else
//			logger.warn("!!Assuming default fuelPrice for car: " + car.getNickName());
//
//		if (car.getItsModel() != null && car.getItsModel().getMilage() != null)
//			milage = car.getItsModel().getMilage();
//		else
//			logger.warn("!!Assuming default Milage for car: " + car.getNickName());
//
//		ctx.put(DriveReportHelper.MILAGE, milage); // 15 kmpl
//		ctx.put(DriveReportHelper.FUELPRICE, fuelPrice); // Rs. 70
//
//		// @TODO remove this - after fixing the FuelEff Mean and StdDev
//		// processors - following is a *BAD* idea
//		// TODOTODOTODO!!
//		ctx.setParam(MeanCalculator.class, MeanCalculator.COLUMN, new Column(DriveReportHelper.FUEL_EFF));
//		ctx.setParam(StdDeviationCalculator.class, StdDeviationCalculator.COLUMN,
//				new Column(DriveReportHelper.FUEL_EFF));
//		ctx.setParam(SkimData.class, SkimData.COLUMN_LIST,
//				Arrays.asList(new Column[] { new Column(AnalyticsHelper.CAR_ID), new Column(AnalyticsHelper.TIME_STAMP),
//						new Column(AnalyticsHelper.SERVER_TIME_STAMP), new Column(DriveReportHelper.SPEED),
//						new Column(DriveReportHelper.RPM), new Column(AnalyticsHelper.LATITUDE),
//						new Column(AnalyticsHelper.LONGITUDE), new Column(AnalyticsHelper.ACCELERATION_X),
//						new Column(AnalyticsHelper.ACCELERATION_Y), new Column(AnalyticsHelper.ACCELERATION_Z),
//						new Column(AnalyticsHelper.ACCELERATION_G), new Column(AnalyticsHelper.ERROR_CODES),
//						new Column(DriveReportHelper.MAFAIR_FLOW_RATE), new Column(DriveReportHelper.ODOMETER_READING),
//						new Column(DriveReportHelper.VOLTAGE) }));
//		ctx.setParam(SkimData.class, SkimData.VALIDATOR, new DriveRecordValidator());
//	}
//
//	@Transactional
//	//@Test
//	public void testAnalyticsCookbook() throws Exception {
//		String inputFile = "D:\\temp\\ajay\\ajay\\data.csv";
//		AnalyticsContext ctx = new AnalyticsContext();
//		populateContext(ctx, Car.getById(6), new Date(), new Date());
//		AnalyticsRecipe recipe = AnalyticsCookbook.getInstance().getRecipe("DriveAnalysis");
//
//		AnalyticsProcess process = new AnalyticsProcess(recipe, ctx, inputFile);
//		process.execute();
//
//		// get output files
//		String outputFile = process.getOutputFile();
//
//		// close will clean the intermediate files. do something with result
//		// copy files over to other location
//		// process.close();
//		//
//		// AnalyticsWorker worker = new TestDriveAnalysis("DriverAnalysis-" +
//		// Utils.getTimeEpoch(new Date()),
//		// "Do Analytics Report from file ", inputFile, Car.getById(6), new
//		// Date(), new Date());
//		// worker.execute();
//
//	}
//
	@Transactional
	@Test
	public void testAnalyticsCookbookMIS() throws Exception {
		String inputFile = "//home//amita//Documents//MIS//Feb20TAT.csv";
		AnalyticsContext ctx = new AnalyticsContext();
		ctx.put(WeeklyMISReporter.MIS_REPORT_JSON_PATH , "/home/amita/Documents/MIS/weeklymisreport.json");
		AnalyticsRecipe recipe = AnalyticsCookbook.getInstance().getRecipe("WeeklyMISReportRecipe");

		AnalyticsProcess process = new AnalyticsProcess(recipe, ctx, inputFile);
		process.execute();

		// get output files
		String outJSON = process.getContext().get(WeeklyMISReporter.WEEKLY_MIS_JSON).toString();
		System.out.println("===OutJSON from context: " + outJSON);
	}
//	
//	//@Test
//	public void testFileUploadToCDN() throws Exception {
//		String localFile = "/home/datta/Downloads/18301608_1477806908944757_1676908954489373211_n.jpg";
//		// String localFile =
//		// "/home/hrishi/Pictures/Prateek_car_july25_11_30.png";
//		String remoteFileName = "new_disk_usage333.png";
//		try (CDNConnection con = CDNConnection.open()) {
//			con.uploadFile("cdn", remoteFileName, localFile, "Image/PNG");
//		}
//	}
//
//	//@Test
//	public void testFileDeleteFromCDN() throws Exception {
//		String localFile = "/home/hrishi/projects/bagic/size/cariq/80-20/classes_contents.png";
//		String remoteFileName = "new_disk_usage333.png";
//		// String remoteFileName = "architecture.jpg";
//
//		try (CDNConnection con = CDNConnection.open()) {
//			con.deleteFile("cdn", remoteFileName);
//		}
//	}
//
//	//@Test
//	public void testReflectionOnController() throws Exception {
//		Class carController = com.cariq.toolkit.web.CarController.class;
//
//		Annotation[] annotations = carController.getAnnotations();
//
//		for (Annotation annotation : annotations) {
//			// System.out.println("Annotation: " + annotation.toString());
//			if (annotation instanceof CarIQPublicAPI) {
//				CarIQPublicAPI publicAPI = (CarIQPublicAPI) annotation;
//				System.out.println("API Description: " + publicAPI.description());
//			}
//			if (annotation instanceof RequestMapping) {
//				RequestMapping reqMapping = (RequestMapping) annotation;
//				System.out.println("API Base Url: " + reqMapping.value()[0]);
//			}
//		}
//
//		Method[] allMethods = carController.getDeclaredMethods();
//		for (Method method : allMethods) {
//			if (Modifier.isPublic(method.getModifiers())) {
//				// System.out.println(method);
//				Annotation[] methodAnnotations = method.getAnnotations();
//				for (Annotation annotation : methodAnnotations) {
//					// System.out.println("Annotation: " +
//					// annotation.toString());
//					if (annotation instanceof CarIQPublicAPIMethod) {
//						CarIQPublicAPIMethod publicAPIMethod = (CarIQPublicAPIMethod) annotation;
//						System.out.println("API Method Description: " + publicAPIMethod.description());
//						System.out.println("API Method Returns: " + publicAPIMethod.responseClass().getName());
//					}
//					if (annotation instanceof RequestMapping) {
//						RequestMapping reqMapping = (RequestMapping) annotation;
//						if (reqMapping.value() != null && reqMapping.value().length > 0) {
//							System.out.println("API Method Url: " + reqMapping.value()[0]);
//
//							RequestMethod meth = (reqMapping.method() != null && reqMapping.method().length > 0)
//									? reqMapping.method()[0] : RequestMethod.GET;
//							System.out.println("API Method HttpMethod: " + meth);
//
//						}
//					}
//				}
//
//				// Method Parameters
//				Annotation[][] annotationsMatrix = method.getParameterAnnotations();
//				Class<?>[] paramTypes = method.getParameterTypes();
//				int paramNo = 0;
//				for (Annotation[] annotationsArr : annotationsMatrix) {
//					for (Annotation anno : annotationsArr) {
//						if (anno instanceof CarIQPublicAPIParameter) {
//							System.out.println("Parameter No. " + paramNo);
//
//							CarIQPublicAPIParameter publicAPIParam = (CarIQPublicAPIParameter) anno;
//							System.out.println("Parameter Type: " + paramTypes[paramNo].getName());
//							System.out.println("Parameter Name: " + publicAPIParam.name());
//							System.out.println("Parameter Description: " + publicAPIParam.description());
//							System.out.println("-----");
//						}
//					}
//					paramNo++;
//					System.out.println("=== End of Param ====");
//
//				}
//			}
//			System.out.println("+++  End of Method +++");
//
//		}
//
//		System.out.println("---  End of Class ---");
//
//	}
//
//	/*
//	 * (non-Javadoc)
//	 * 
//	 * @see com.cariq.toolkit.w4iq.W4IQRestClientService#post(java.lang.Object,
//	 * java.lang.String, java.lang.String, java.lang.Class)
//	 */
//	public <I> void downloadFile(I inputJson, String url, String basicAuth, String path) throws Exception {
//		try (ProfilePoint _RestClientPost = ProfilePoint.profileAction("ProfAction_downloadFile")) {
//			// set basic auth headers
//			HttpHeaders requestHeaders = setBasicAuth(basicAuth);
//			requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));
//			RestTemplate restTemplate = new RestTemplate(Utils.getRequestFactory());
//			ResponseEntity<byte[]> response = restTemplate.exchange(url, HttpMethod.POST,
//					new HttpEntity<I>(inputJson, requestHeaders), byte[].class);
//
//			if (response.getStatusCode().equals(HttpStatus.OK)) {
//				FileOutputStream output = new FileOutputStream(new File(path));
//				IOUtils.write(response.getBody(), output);
//
//			}
//		}
//	}
//
//	/**
//	 * Test reflection on class to see if we can identify types meaningfully
//	 */
//	//@Test
//	public void testReflectionOnBeansForSchema() throws Exception {
//		Class<?> cls = TripListJson.class;
//		Map<String, Class<?>> attrs = BeanHelper.getAttributeProperties(cls);
//		for (String attr : attrs.keySet()) {
//			System.out.println(attr + " : " + attrs.get(attr));
//		}
//	}
//
//	/**
//	 * Sets the basic auth.
//	 *
//	 * @param basicAuth
//	 *            the basic auth
//	 * @return the http headers
//	 */
//	private HttpHeaders setBasicAuth(String basicAuth) {
//		HttpHeaders headers = new HttpHeaders();
//		headers.set("Content-Type", "application/json");
//		headers.set("Authorization", basicAuth);
//		return headers;
//	}
//
//	//	{
//	//	    "owner":"445032321825",
//	//	    "fromTimeStamp":1491039000000,
//	//	    "toTimeStamp":1491643800000
//	//	}
//
//	static class W4IQFileDownloadQuery {
//		String owner;
//		long fromTimeStamp, toTimeStamp;
//
//		/**
//		 * @param owner
//		 * @param fromTimeStamp
//		 * @param toTimeStamp
//		 */
//		public W4IQFileDownloadQuery(String owner, long fromTimeStamp, long toTimeStamp) {
//			this.owner = owner;
//			this.fromTimeStamp = fromTimeStamp;
//			this.toTimeStamp = toTimeStamp;
//		}
//
//		public String getOwner() {
//			return owner;
//		}
//
//		public long getFromTimeStamp() {
//			return fromTimeStamp;
//		}
//
//		public long getToTimeStamp() {
//			return toTimeStamp;
//		}
//	}
//
//	/**
//	 * Test program to download a file from other webservice to local dir Later
//	 * this will be added to the RestClient
//	 * 
//	 * @throws Exception
//	 */
//	//@Test
//	@Transactional
//	public void testDownloadFile() throws Exception {
//		Date fromTimeStamp = Utils.getDayStartTimeByItsTime(new Date(), null);
//		Date toTimeStamp = new Date();
//		String owner = "445032321825";
//
//		String url = "https://ecu-bg-dev.mycariq.com:443/W4IQ/workers/download/AnalyticsDataWorker/OwnerBased/1488794758000/1488967558000";
//		String basicAuth = "Basic YWRtaW46NTM2MTc0YmRhNmUwNDgyZTU1NDlmNDEwZmJiODJkZDU=";
//		String filePath = "/tmp/testDownloadFile.csv";
//
//		downloadFile(new W4IQFileDownloadQuery(owner, fromTimeStamp.getTime(), toTimeStamp.getTime()), url, basicAuth,
//				filePath);
//
//		AnalyticsWorker worker = new DriverAnalysisNew("DriverAnalysis-" + Utils.getTimeEpoch(new Date()),
//				"Do Analytics Report from file ", filePath, Car.getById(6), new Date(), new Date());
//		worker.execute();
//	}
//
//	//@Test
//	@Transactional
//	public void testCarHelperGetOwner() throws Exception {
//		Car car12 = Car.getById(22);
//		String owner = CarHelper.getOwner(car12);
//
//		System.out.println("Owner = " + owner);
//
//	}
//
}
