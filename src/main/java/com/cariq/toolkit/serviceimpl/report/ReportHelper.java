/**
 * 
 */
package com.cariq.toolkit.serviceimpl.report;

import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.service.report.ReportType;

/**
 * @author hrishi
 *
 */
public class ReportHelper {

	/**
	 * @param ownerType
	 * @param ownerId
	 * @return
	 */
	public static String getEmailAddress(String ownerType, String ownerId) {
		return "hrishi@mycariq.com"; // temp
		// Based on the owner type and id, get the information
	}
	
	public static ReportType validateReportType(String type) {
		try {
			return ReportType.valueOf(type);
		} catch (Exception e) {
			Utils.handleException("Invalid report type");
		}
		return null;
	}

}
