package com.cariq.toolkit.serviceimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.stereotype.Service;

import com.atom.www.json.PolicyRenewalPercentageJSON;
import com.cariq.toolkit.coreiq.CarIQRawQuery;
import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.model.CarAggregateStatistics;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.StageStatistics;
import com.cariq.toolkit.service.AggregateDataService;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.google.common.base.Strings;

/**
 * The Class AggregateDataServiceImpl.
 */
@Service
public class AggregateDataServiceImpl implements AggregateDataService {

	/** The Constant TOTAL_DISTANCE. */
	private static final String BY_STATE = "ByState", CARS_COUNT = "CarsCount", TOTAL_DISTANCE = "TotalDistance",
			RENEWAL_PERCENTAGE = "RenewalPercentage", CONTEXT = "context", CREATED_ON = "createdOn",
			SUBSCRIPTION_RENEWED = "SubscriptionRenewed", SUBSCRIPTION_STATE = "subscriptionState",
			AVG_DRIVING_SCORE = "AvgDrivingScore", SAFETY_DISTANCE = "SafetyDistance",
			SUBSCRIPTION_EXPIRED = "SubscriptionExpired", SUBSCRIPTION_DEACTIVATED = "SubscriptionDeactivated",
			TOTAL_COUNTS = "TotalCounts";

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.toolkit.service.AggregateDataService#getTotalCounts(java.lang.
	 * String)
	 */
	@Override
	public GenericJSON getTotalCounts(String domainName) {
		try (ProfilePoint _AggregateDataServiceImpl_getTotalCounts = ProfilePoint
				.profileActivity("ProfActivity_AggregateDataServiceImpl_getTotalCounts")) {

			Domain domain = Domain.getByName(domainName);
			if (null == domain)
				Utils.handleException("Domain does not exists.");

			CarIQSimpleQuery<CarAggregateStatistics> query = new CarIQSimpleQuery<CarAggregateStatistics>(
					"getAggregateTotalCounts", CarAggregateStatistics.entityManager(), CarAggregateStatistics.class);
			query.addCondition(CONTEXT, CarIQSimpleQuery.EQ, BY_STATE);
			query.orderBy(CREATED_ON, CarIQSimpleQuery.DESC);

			GenericJSON result = new GenericJSON();
			CarAggregateStatistics statistics = query.getSingleResult();
			if (null == statistics)
				return result;

			List<GenericJSON> jsonList = Utils.getJSonObjectList(statistics.getValueJson(), GenericJSON.class);

			Long totalCarsCount = 0L;
			Double totalDistance = 0.0;

			for (GenericJSON json : jsonList) {
				totalCarsCount += Utils.getValue(Long.class, json, CARS_COUNT);
				totalDistance += Utils.getValue(Double.class, json, TOTAL_DISTANCE);
			}

			LinkedHashMap<String, String> attributeAliasMap = new LinkedHashMap<String, String>();
			List<String> tableAliasArray = new ArrayList<String>();
			attributeAliasMap.put("st.subscription_state", SUBSCRIPTION_STATE);
			attributeAliasMap.put("count(1)", TOTAL_COUNTS);

			tableAliasArray.add("stage_statistics st");

			// get total counts of renewed policies,expired policies, deactivated policies
			CarIQRawQuery rowQuery = new CarIQRawQuery("getTotalCounts", StageStatistics.entityManager(),
					attributeAliasMap, tableAliasArray);
			rowQuery.addRawCondition("st.domain = " + domain.getId() + " group by st.subscription_state");

			int subscriptionRenewedCount = 0, subscriptionExpiredCount = 0, subscriptionDeactivatedCount = 0;
			List<GenericJSON> resultList = rowQuery.getResult();
			if(null == resultList)
				return result;
			
			for (GenericJSON json : resultList) {
				String subscriptionName = Utils.getValue(String.class, json, SUBSCRIPTION_STATE);
				int totalCount = Utils.getValue(Integer.class, json, TOTAL_COUNTS);
				if (SUBSCRIPTION_RENEWED.equalsIgnoreCase(subscriptionName))
					subscriptionRenewedCount = totalCount;
				else if (SUBSCRIPTION_EXPIRED.equalsIgnoreCase(subscriptionName))
					subscriptionExpiredCount = totalCount;
				else if (SUBSCRIPTION_DEACTIVATED.equalsIgnoreCase(subscriptionName))
					subscriptionDeactivatedCount = totalCount;
			}
			
			int totalSubscription = (subscriptionRenewedCount + subscriptionExpiredCount + subscriptionDeactivatedCount);
			double renewalPercentage = 0;
			
			// calculate renewal percentage.
			if(totalSubscription > 0)
				renewalPercentage = Utils.downCast(Double.class,((subscriptionRenewedCount * 100)/ totalSubscription));

			result.add(CARS_COUNT, totalCarsCount);
			result.add(TOTAL_DISTANCE, totalDistance);
			result.add(RENEWAL_PERCENTAGE, renewalPercentage);

			return result;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.toolkit.service.AggregateDataService#getStatisticsData(java.lang.
	 * String, java.lang.String)
	 */
	@Override
	public List<GenericJSON> getStatisticsData(String domainName, String context) {
		try (ProfilePoint _AggregateDataServiceImpl_getStatisticsData = ProfilePoint
				.profileActivity("ProfActivity_AggregateDataServiceImpl_getStatisticsData")) {
			CarIQSimpleQuery<CarAggregateStatistics> query = new CarIQSimpleQuery<CarAggregateStatistics>(
					"getStatisticsData", CarAggregateStatistics.entityManager(), CarAggregateStatistics.class);

			Domain domain = Domain.getByName(domainName);
			if (null == domain)
				Utils.handleException("Domain does not exists.");

			if (Strings.isNullOrEmpty(context))
				Utils.handleException("Invalid context.");

			query.addCondition(CONTEXT, CarIQSimpleQuery.EQ, context);
			query.orderBy(CREATED_ON, CarIQSimpleQuery.DESC);

			List<GenericJSON> jsonList = new ArrayList<GenericJSON>();
			CarAggregateStatistics statistics = query.getSingleResult();
			if (null == statistics)
				return jsonList;

			jsonList = Utils.getJSonObjectList(statistics.getValueJson(), GenericJSON.class);
			if (null == jsonList)
				return jsonList;
			
			for (GenericJSON json : jsonList) {
				double avgDrivingScore = Utils.getValue(Double.class, json, AVG_DRIVING_SCORE);
				double saftyIndex = (avgDrivingScore == 0)?  0.93 : Utils.getValue(Double.class, json, AVG_DRIVING_SCORE) / 10;
				double safetyDistance = (saftyIndex * Utils.getValue(Double.class, json, TOTAL_DISTANCE));
				json.add(SAFETY_DISTANCE, safetyDistance);
			}
			return jsonList;
		}
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.service.AggregateDataService#getPolicyRenewalPercentage(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public PolicyRenewalPercentageJSON getPolicyRenewalPercentage(String domainName, String fromDate, String toDate) {
		try (ProfilePoint _getPolicyRenewalPercentage = ProfilePoint.profileActivity("AggregateDataServiceImpl_getPolicyRenewalPercentage")) {
			// validate domain
			Domain domain = Domain.getByName(domainName);
			if (null == domain)
				Utils.handleException("Domain does not exists.");

			// validate dates
			Date startTimestamp = Utils.getDate(fromDate);
			Date endTimestamp = Utils.getDate(toDate);
					
			PolicyRenewalPercentageJSON renewalPercentageJSON = new PolicyRenewalPercentageJSON();
			// get total subscription_state counts by date range
			List<GenericJSON> jsonList = getTotalSubscriptionStateCounts(domain, fromDate, toDate);
			if(null == jsonList)
				return renewalPercentageJSON;
			
			int totalSubscription = 0, subscriptionRenewedCount = 0;
			for (GenericJSON json : jsonList) {
				String subscriptionName = Utils.getValue(String.class, json, SUBSCRIPTION_STATE);
				int totalCount = Utils.getValue(Integer.class, json, TOTAL_COUNTS);
				if (SUBSCRIPTION_EXPIRED.equalsIgnoreCase(subscriptionName)
						|| SUBSCRIPTION_DEACTIVATED.equalsIgnoreCase(subscriptionName))
					totalSubscription += totalCount;
				else if (SUBSCRIPTION_RENEWED.equalsIgnoreCase(subscriptionName))
					subscriptionRenewedCount = totalCount;
			}
			totalSubscription += subscriptionRenewedCount;
			// calculate renewal percentage.
			if (totalSubscription > 0)
				renewalPercentageJSON.setPolicyRenewalPercentage(
						Utils.downCast(Double.class, ((subscriptionRenewedCount * 100) / totalSubscription)));

			// return renewal percentage
			return renewalPercentageJSON;
		}
	}

	/**
	 * Gets the total subscription state counts.
	 *
	 * @param domain the domain
	 * @param fromDate the from date
	 * @param toDate the to date
	 * @return the total subscription state counts
	 */
	private List<GenericJSON> getTotalSubscriptionStateCounts(Domain domain, String fromDate, String toDate) {
		try (ProfilePoint _getTotalSubscriptionStateCounts = ProfilePoint
				.profileActivity("AggregateDataServiceImpl_getTotalSubscriptionStateCounts")) {
			LinkedHashMap<String, String> attributeAliasMap = new LinkedHashMap<String, String>();
			List<String> tableAliasArray = new ArrayList<String>();
			attributeAliasMap.put("st.subscription_state", SUBSCRIPTION_STATE);
			attributeAliasMap.put("count(1)", TOTAL_COUNTS);

			tableAliasArray.add("stage_statistics st");

			// get total counts of renewed policies,expired policies, deactivated policies by date range
			CarIQRawQuery rowQuery = new CarIQRawQuery("getTotalCountsByDateRange", StageStatistics.entityManager(),
					attributeAliasMap, tableAliasArray);
			rowQuery.addRawCondition("st.domain = " + domain.getId());
			rowQuery.addRawCondition("st.entry_date >= '" + fromDate + "'");
			rowQuery.addRawCondition("st.entry_date <= '" + toDate + "' group by st.subscription_state");

			return rowQuery.getResult();
		}
	}
}