package com.cariq.toolkit.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atom.www.helper.ConfigHelper;
import com.atom.www.helper.DomainHelper;
import com.cariq.toolkit.model.Domain.Domain_Out;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.service.CariqProxyService;
import com.cariq.toolkit.utils.APIHelper;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.utils.service.CarIQRestClientService;

/**
 * The Class CarIQProxyServiceImpl.
 */
@Service
public class CarIQProxyServiceImpl implements CariqProxyService {

	/** The logger. */
	CarIQLogger logger = APIHelper.logger.getLogger("CariqProxyService");

	/** The rest client service. */
	@Autowired
	CarIQRestClientService restClientService;

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.service.CariqProxyService#get(java.lang.String)
	 */
	@Override
	public Object get(String apiName) {
		try (ProfilePoint _get = ProfilePoint.profileAction("CarIQProxyServiceImpl_get" + apiName)) {
			User user = User.getLoggedInUser();
			DomainHelper domainHelper = new DomainHelper();
			Domain_Out domain = domainHelper.getDomainByUser(user);
			Utils.checkNotNull(domain, "Domain not found.");
			
			ConfigHelper helper = new ConfigHelper();
			String url = helper.makeUrl(apiName, domain.getUrl());
			return restClientService.get(url, helper.getSecurityToken(), Object.class);
		}
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.service.CariqProxyService#post(com.cariq.toolkit.utils.GenericJSON, java.lang.String)
	 */
	@Override
	public Object post(GenericJSON json, String apiName) {
		try (ProfilePoint _post = ProfilePoint.profileAction("CarIQProxyServiceImpl_post" + apiName)) {
			User user = User.getLoggedInUser();
			DomainHelper domainHelper = new DomainHelper();
			Domain_Out domain = domainHelper.getDomainByUser(user);
			Utils.checkNotNull(domain, "Domain not found.");
			
			ConfigHelper helper = new ConfigHelper();
			String url = helper.makeUrl(apiName, domain.getUrl());
			return restClientService.post(json, url,  helper.getSecurityToken(),Object.class);
		}
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.service.CariqProxyService#put(com.cariq.toolkit.utils.GenericJSON, java.lang.String)
	 */
	@Override
	public Object put(GenericJSON json, String apiName) {
		try (ProfilePoint _put = ProfilePoint.profileAction("CarIQProxyServiceImpl_put" + apiName)) {
			User user = User.getLoggedInUser();
			DomainHelper domainHelper = new DomainHelper();
			Domain_Out domain = domainHelper.getDomainByUser(user);
			Utils.checkNotNull(domain, "Domain not found.");
			
			ConfigHelper helper = new ConfigHelper();
			String url = helper.makeUrl(apiName, domain.getUrl());
			return restClientService.put(json, url, helper.getSecurityToken(),Object.class);
		}
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.service.CariqProxyService#delete(java.lang.String)
	 */
	@Override
	public Object delete(String apiName) {
		try (ProfilePoint _delete = ProfilePoint.profileAction("CarIQProxyServiceImpl_delete" + apiName)) {
			User user = User.getLoggedInUser();
			DomainHelper domainHelper = new DomainHelper();
			Domain_Out domain = domainHelper.getDomainByUser(user);
			Utils.checkNotNull(domain, "Domain not found.");
			
			ConfigHelper helper = new ConfigHelper();
			String url = helper.makeUrl(apiName, domain.getUrl());
			return restClientService.delete(url, helper.getSecurityToken(),
					Object.class);
		}
	}
}