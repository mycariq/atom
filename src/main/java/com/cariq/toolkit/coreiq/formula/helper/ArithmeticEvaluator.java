package com.cariq.toolkit.coreiq.formula.helper;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

/**
 * Engine to parse literal formulae
 * @author hrishi
 *
 */
public class ArithmeticEvaluator {
	ScriptEngine javascriptEngine = null;
	static ArithmeticEvaluator _instance = new ArithmeticEvaluator();

	ArithmeticEvaluator() {
		ScriptEngineManager mgr = new ScriptEngineManager();
		javascriptEngine = mgr.getEngineByName("JavaScript");
	}

	/**
	 * Create instance of CQScriptGenerator in singleton fashion This make sure that
	 * the initialization of JavaScript Engine happens only once
	 * 
	 * @return
	 */
	public static ArithmeticEvaluator getInstance() {
		return _instance;

	}

	public Object eval(String formula) throws ScriptException {
		Object retval = javascriptEngine.eval(formula);
//		System.out.println("Evaluating formula: " + formula + " = " + retval.toString());
		return retval;
		
	}

}
