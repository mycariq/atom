/**
 * 
 */
package com.cariq.toolkit.utils.jsonProcessor;

import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.cariq.toolkit.utils.Utils;

/**
 * @author hrishi
 *
 */
public class JSONObjectBuilder {
	String filePath;
	Map<String, JSONProcessor> jsonProcessorMap = new HashMap<String, JSONProcessor>();

	/**
	 * @param filePath
	 */
	public JSONObjectBuilder(String filePath) {
		this.filePath = filePath;
	}
	
	public JSONObjectBuilder(File file) {
		this.filePath = file.getAbsolutePath();
	}

	/**
	 * @param p
	 */
	public void register(JSONLoadableIfc jsonLoadable) {
		// double dispatch
		jsonLoadable.register(this);
	}

	/**
	 * @param key
	 * @param jsonProcessor
	 */
	public void subscribe(String key, JSONProcessor jsonProcessor) {
		jsonProcessorMap.put(key, jsonProcessor);
	}

	
	/**
	 * 
	 */
	public void process() {
		try {
			JSONParser parser = new JSONParser();

			FileReader reader = new FileReader(filePath);
			traverse(new JSONProcessorContext(), new JSONPath(), parser.parse(reader));
			reader.close();
		} catch (Exception e) {
			Utils.handleException(e);
		}
	}

	/**
	 * @param string
	 * @param parse
	 */
	private void traverse(JSONProcessorContext context, JSONPath jsonPath, Object obj) {
		// Current key is the key for which this round of traverse will happen
		// JSONPath indicates its path from root in jq format
		String currentKey = jsonPath.key;
		JSONProcessor processor = this.jsonProcessorMap.get(currentKey);
		// json contains nulls ?!
		if (obj == null)
			return;
		
		// if the object is array, traverse array
		if (obj.getClass().equals(JSONArray.class)) {
			JSONArray array = (JSONArray) obj;
			for (int i = 0; i < array.size(); i++) {
				// Add index to json path for complete path definition
				JSONPath path = jsonPath.appendIndex(i);
				
				// Registration does not work on array - unless one wants to register on particular element of array
				// Otherwise, either a full json, or bare value will be passed to registered Handleres
				// Within JSON however, Arrays are possible

				// recursively go further into the array elements
				traverse(context, path, array.get(i));
			}
		} else if (obj.getClass().equals(JSONObject.class)) {
			// If the object is json - traverse keys
			JSONObject json = (JSONObject) obj;
			// If registered, it's processor's responsibility to take necessary action 
			if (processor != null)
			{
				//System.out.println("object path --"+jsonPath+" --values--"+"object type"+obj.getClass().getName()+"--"+obj);
				processor.process(context, jsonPath, obj);
			}
			for (Object key : json.keySet()) {
				// add key to json path to make it complete
				JSONPath path = jsonPath.appendKey(key.toString());

				// recursively go further
				traverse(context, path, json.get(key));
			}

		} else {
			// if the object is Basic type, call handler if registered, and print
			// If registered, it's processor's responsibility to take necessary action 
			if (processor != null)
				processor.process(context, jsonPath, obj);

			// ** Note - there is no "traverse" call here because we have reached the leaf
			//System.out.println("[Traverse]" + jsonPath.url + " : " + obj.toString());
		}	
		
	}
	
}
