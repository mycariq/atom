/**
 * 
 */
package com.cariq.toolkit.coreiq.exporter;

import java.util.List;

import com.cariq.toolkit.utils.GenericJSON;

/**
 * The Class UnityExportDataEnhancer.
 *
 * @author santosh
 */
public class UnityExportDataEnhancer extends ExportEnhanceTemplate {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.toolkit.coreiq.exporter.ExportEnhanceTemplate#doEnhance(java.
	 * util.List)
	 */
	@Override
	protected List<GenericJSON> doEnhance(List<GenericJSON> list) {
		return list;
	}
}