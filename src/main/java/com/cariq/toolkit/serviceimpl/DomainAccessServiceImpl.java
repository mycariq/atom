package com.cariq.toolkit.serviceimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.coreiq.model.ActionLog;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.Domain.Domain_Out;
import com.cariq.toolkit.model.DomainAccess;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.model.User.User_Out;
import com.cariq.toolkit.service.DomainAccessService;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.service.CarIQRestClientService;


/**
 * The Class DomainServiceImpl.
 */
@Service
public class DomainAccessServiceImpl implements DomainAccessService {

	/** The rest service. */
	@Autowired
	CarIQRestClientService restService;

	@Override
	@Transactional
	public ResponseJson grantDomainAccessToUser(String userName, String domainName) {
		try {
			User user = User.getByUsername(userName);
			if (null == user) {
				Utils.handleException("User " + userName + " is invalid");
			}
			
			Domain domain = Domain.getByName(domainName);
			if (null == domain) {
				Utils.handleException("Domain " + domainName + " is invalid");
			}
			
			DomainAccess domainAccess = new DomainAccess(domain, user, new Date(), new Date());
			
			domainAccess.persist();
			
			ActionLog.log(Utils.createContext("DomainAccessServiceImpl.updateDomain"), "grantAccess", "domain",
					"Domain : " + domain.getName() + " User : " + user.getUsername(),
					"No access earlier");
			
		} catch (Exception e) {
			Utils.handleException("Failed to grant access to domain: " + domainName 
				+ " for user: " + userName + ", Exception: " + e.getMessage());
		}
		return new ResponseJson("User " + userName + " granted access to Domain " + domainName + " successfully.");
	}
	
	@Override
	@Transactional
	public ResponseJson removeDomainAccessForUser(String username, String domainName) {
		try {
			User user = User.getByUsername(username);
			if (user == null) {
				Utils.handleException("User " + username + " is invalid");
			}
			
			Domain domain = Domain.getByName(domainName);
			if (domain == null) {
				Utils.handleException("Domain " + domainName + " is invalid");
			}
			
			DomainAccess domainAccess = DomainAccess.getObjectByDomainUSer(domain, user);
			if (null != domainAccess) {
				domainAccess.remove();
				ActionLog.log(Utils.createContext("DomainAccessServiceImpl.removeDomainAccessForUser"), "removeAccess", "domain",
						"Domain : " + domain.getName() + " User : " + user.getUsername(),
						"Had access earlier");
				return new ResponseJson("User " + username + " is blocked to access the Domain " + domainName);
			}
		} catch (Exception e) {
			Utils.handleException("Failed to block access to domain: " + domainName 
				+ " for user: " + username + ", Exception: " + e.getMessage());
		}
		return new ResponseJson("User " + username + " already does not have to access the Domain " + domainName);
	}
	
	@Override
	public List<Domain_Out> getAccessibleDomainsForUser(String userName) {
		List<Domain_Out> resultList = new ArrayList<>();
		try {
			User user = User.getByUsername(userName);
			if (user == null) {
				Utils.handleException("User " + userName + " is invalid");
			}
			
			List<DomainAccess> domainAccesslist = DomainAccess.getAllObjectsByUser(user);
			for (DomainAccess domainAccess : domainAccesslist) {
				Domain domain = domainAccess.getDomain();
				if (null != domain) {
					Domain_Out domainOut = domain.toJSON();
					resultList.add(domainOut);
				}
			}
		} catch(Exception e) {
			Utils.handleException("Failed to get accessible domains for user: " 
				+ userName + ", Exception: " + e.getMessage());
		}
		return resultList;
		
	}
	
	@Override
	public List<User_Out> getPrivilegedUsersForDomain(String domainName) {
		List<User_Out> resultList = new ArrayList<>();
		try {
			Domain domain = Domain.getByName(domainName);
			if (domain == null) {
				Utils.handleException("domain " + domainName + " is invalid");
			}
			
			List<DomainAccess> domainAccesslist = DomainAccess.getAllObjectsByDomain(domain);
			for (DomainAccess domainAccess : domainAccesslist) {
				User user = domainAccess.getUser();
				if (null != user) {
					User_Out userOut = user.toJSON();
					resultList.add(userOut);
				}
			}
		} catch(Exception e) {
			Utils.handleException("Failed to get privileged users for domain: " 
					+ domainName + ", Exception: " + e.getMessage());
		}
		return resultList;
	}
}