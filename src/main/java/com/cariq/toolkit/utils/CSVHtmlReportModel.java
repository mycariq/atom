package com.cariq.toolkit.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.cariq.toolkit.utils.analytics.core.AnalyticsContext;
import com.cariq.toolkit.utils.analytics.core.Record;
import com.cariq.toolkit.utils.analytics.core.RecordProcessorTemplate;
import com.cariq.toolkit.utils.analytics.core.TableProcessor;
import com.cariq.toolkit.utils.analytics.core.TableProcessorImpl;

/**
 * Create html model from csv file for the report Move to Utils later
 * 
 * @param name
 * @param string
 * @param date
 * @param csvExportFile
 * @return
 */
public class CSVHtmlReportModel extends HashMap<String, Object> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	final static String NAME = "NAME";
	final static String DESCRIPTION = "DESCRIPTION";
	final static String CREATED_ON = "CREATED_ON";
	final static String TABLE_HEADER = "TABLE_HEADER";
	final static String TABLE_DATA = "TABLE_DATA";
	static final String COUNT = "COUNT";
	static final String HTML_VALUE_RENDERER = "HTML_VALUE_RENDERER";

	// Velocity Template for rendering CSV in html format using this model
	public static final String VelocityTemplate = "velocity/csv2html.vm";

	HtmlValueRenderer htmlValueRenderer;

	public CSVHtmlReportModel(String name, String description, Date createdOn, String csvFile) throws Exception {
		this(name, description, createdOn, csvFile, new HtmlValueIdentityRenderer());
	}

	public CSVHtmlReportModel(String name, String description, Date createdOn, String csvFile,
			HtmlValueRenderer renderer) throws Exception {
		super();
		put("NAME", name);
		put("DESCRIPTION", description);
		put("CREATED_ON", createdOn);

		processFile(csvFile, renderer);
	}

	/**
	 * Using Analytics Engine, process the input CSV file
	 * 
	 * @param ctx
	 * @param outputFile
	 * @param resourceType
	 * @param operation
	 * @param recordProcessorTemplate
	 * @return
	 * @throws Exception
	 */

	private int executeBatchProcessor(AnalyticsContext ctx, String inputFile, String processId,
			RecordProcessorTemplate recordProcessorTemplate) throws Exception {
		ctx.put(COUNT, 0);

		// Process the File using simple Analytics processor
		TableProcessor processor = new TableProcessorImpl(ctx, processId, inputFile, null);

		processor.setForgiveException(false);
		processor.add(recordProcessorTemplate);
		processor.init();
		processor.process();
		processor.close();
		return (int) processor.getProcessorContext().get(COUNT);
	}

	private void processFile(String csvFile, HtmlValueRenderer renderer) throws Exception {
		if (!CarIQFileUtils.exists(csvFile))
			return;

		put(TABLE_DATA, new ArrayList<List<String>>());

		AnalyticsContext ctx = new AnalyticsContext();
		// put the Renderer in the ctx
		ctx.put(HTML_VALUE_RENDERER, renderer);
		int count = executeBatchProcessor(ctx, csvFile, get(NAME).toString(), new RecordProcessorTemplate(ctx) {
			@Override
			protected Record doProcess(Record record) {
				List<String> columns = record.getColumnKeys();

				boolean headerExists = containsKey(TABLE_HEADER);
				if (!headerExists) {
					// add header row to 
					put(TABLE_HEADER, columns);
				}

				List<String> row = new ArrayList<String>(); //record.getValues();
				for (String col : columns) {
					if (col.isEmpty())
						continue;

					HtmlValueRenderer renderer = (HtmlValueRenderer) this.getProcessorContext()
							.get(HTML_VALUE_RENDERER);
					row.add(renderer.toHtmlString(col, record.getStringValue(col)));
				}

				List<List<String>> dataTable = (List<List<String>>) get(TABLE_DATA);
				dataTable.add(row);

				getProcessorContext().put(COUNT, (Integer) getProcessorContext().get(COUNT) + 1);
				return record;
			}
		});
	}
}