package com.atom.www.loader;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Configurable;

import com.atom.www.exporter.OnboardingExporter;
import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderContext;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.ProxyObject;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

/**
 * 
 * Template helps to transfer owners of given domain to new owners.
 * 
 * It gives abstract method which shall be used to decide exact list of policies for which 
 * we want to transfer owners. Specialized classes must implement same and decide 
 * list of policies for owner assignement.
 * 
 * @author sagar
 *
 */
@Configurable
public abstract class OwnerTransferLoaderTemplate extends OwnerAssignmentLoaderTemplate {

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("OwnerTransferLoaderTemplate");
	
	/** Name of domain **/
	public static final String DOMAIN = "domain";
	
	/** Comma separated list of usernames of current owners of proxies.**/
	public static final String CURRENT_OWNERS = "current_owners";
	
	/** Comma separated list of usernames of new owners for proxies.**/
	public static final String NEW_OWNERS = "new_owners";
	
	/**
	 * Default constructor
	 */
	public OwnerTransferLoaderTemplate(String loaderName, String loaderDescription) {
		super(loaderName, loaderDescription);
		
		//add attributes required for this loader
		addAttribute(DOMAIN, "Name of domain(IL-Assist, Driven, TAGIC, Drivesmart....", false);
		addAttribute(CURRENT_OWNERS, "Comma seperated usernames of current owners.", false);
		addAttribute(NEW_OWNERS, "Comma seperated usernames of new owners.", false);

	}

	@Override
	protected void doLoad(GenericJSON json, CarIQLoaderContext context) {
		//1. Get input values of loader.
		String domainName = Utils.getValue(json, DOMAIN);
		String currentOwners = Utils.getValue(json, CURRENT_OWNERS);
		String newOwners = Utils.getValue(json, NEW_OWNERS);
		
		//2. Validate: Don't consider None, wrong usernames 
		
		//2.2 Get valid domain
		Domain domain = Domain.getByName(domainName);
		
		//2.3 If domain does not exists then throw exception
		if(domain == null)
			Utils.handleException(domainName + " does not exists!");
		
		//2.4 Get valid input owners.
		List<String> cOwners = getValidOwners(currentOwners, domain);
		
		//2.5 Get valid new owners
		List<String> nOwners = getValidOwners(newOwners, domain);
		
		//Get users by usernames
		List<User> cUsers = User.getUsers(cOwners);

		//Created after date in string
		String createdAfterString = Utils.getValue(json, "CreatedAfter");
		
		//Created after date in date
		Date createdAfterDate = Utils.getDate(createdAfterString);
		
		//4. Find proxies of comma separated current usernames
		List<ProxyObject> proxies = ProxyObject.getProxiesByOwnerAndCreatedAfter(cUsers, createdAfterDate);
		
		//5. Get only signatures of those comma separated owners
		List<String> signatures = ProxyObject.getSignatures(proxies);
		
		//6. Give call to onboarding exporter with 'Not Connected' query type.
		//   Pass those signatures to onboarding exporter.
		//   It means cariq should return all not connected policies between given signatures.
		OnboardingExporter exporter = new OnboardingExporter();
		GenericJSON inputJson = new GenericJSON();
		inputJson.add(DOMAIN, domainName);
		inputJson.add("Keyword search", Utils.getCommaSeperatedString(signatures));
		List<GenericJSON> policies = exporter.export(getQueryType(), 
				inputJson, 1, Integer.MAX_VALUE);
		logger.debug("Total policies retrieved count: " + (Utils.isNullOrEmpty(policies) ? 0 : policies.size()));
		
		//7. Keep only those policies which match given comma separated stage. 
		List<GenericJSON> policiesByGivenStage = filterWithCondition(policies, json);
		logger.debug("Total policies for owner update count: " 
					+ (Utils.isNullOrEmpty(policiesByGivenStage) ? 0 : policies.size()));
		
		//Now we have all those policies whom owner need to be updated.
		
		//8. Use round robin to update owner of those policies.
		//   After updating owner, write message on timeline that ownership has been 
		//   transferred from 'current' owner to 'new' owner.
		assignOwners(policiesByGivenStage, nOwners, domain, true, context);
		
		//9. Done.
	}
	
	
	/**
	 * Filter policies, Choose only those which match condition. All filtered out will
	 * be applicable for owner transfer. And will be assignement new 
	 * owners from input list new owners.
	 * 
	 * @param policies
	 * @param inputJSON
	 * @return
	 */
	abstract List<GenericJSON> filterWithCondition(List<GenericJSON> policies, GenericJSON inputJSON);
	
	
	/**
	 * Get specific query type from child,, which will be used in cariq API call.
	 * 
	 * @return
	 */
	abstract String getQueryType();
}
