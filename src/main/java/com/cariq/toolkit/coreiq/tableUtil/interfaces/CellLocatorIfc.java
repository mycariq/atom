package com.cariq.toolkit.coreiq.tableUtil.interfaces;

import com.cariq.toolkit.coreiq.tableUtil.impl.ProcessingContext;

// TODO - MIS should not be included here... so ProcessingContext can be based on non MIS. It's just holder
public interface CellLocatorIfc {
	boolean isMatching(ProcessingContext processingContext, CellAddressIfc cellAddress);
	CellIfc locate(ProcessingContext processingContext, CellAddressIfc cellAddress);
}
