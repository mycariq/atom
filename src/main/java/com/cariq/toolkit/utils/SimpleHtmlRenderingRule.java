package com.cariq.toolkit.utils;

/**
 * A rule that defines how rendering should happen based on condition
 * @author hrishi
 *
 */
public class SimpleHtmlRenderingRule implements HtmlRenderingRule {
	String key;
	HtmlRenderingRuleCondition renderingCondition;
	HtmlRenderingValue renderingValue;
	

	public SimpleHtmlRenderingRule(String key, HtmlRenderingRuleCondition renderingCondition,
			HtmlRenderingValue renderingValue) {
		super();
		this.key = key;
		this.renderingCondition = renderingCondition;
		this.renderingValue = renderingValue;
	}

	@Override
	public String apply(String key, String value) {
		if (getKey().equals(key) == false)
			return value;
		
		if (renderingCondition.isSatisfied(value))
			return renderingValue.render(value);
		
		return value;
	}

	@Override
	public String getKey() {
		return key;
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.utils.HtmlRenderingRule#isSatisfied()
	 */
	@Override
	public boolean isSatisfied(String value) {
		return renderingCondition.isSatisfied(value);
	}
}
