package com.cariq.toolkit.coreiq.tableUtil.interfaces;

import com.cariq.toolkit.utils.GenericJSON;

public interface CellValueEvaluatorIfc extends CellValueAribiterIfc {
	Object evaluateValue(GenericJSON input, Object currentValue);
}
