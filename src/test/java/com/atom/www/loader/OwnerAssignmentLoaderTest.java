package com.atom.www.loader;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.coreiq.loader.CarIQLoader;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderContext;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderRegistry;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Transactional
@Configurable
public class OwnerAssignmentLoaderTest {
	
	@Before
	public void init() {
		Utils.setupSecurityContext("preeti", "875d48192ccbbb18f32a13cec13cac14", "a1");
	}

	@Test
	public void testLoader() {
		CarIQLoader loader = CarIQLoaderRegistry.getLoader("OwnerAssignmentLoader");
		
		GenericJSON json = new GenericJSON();
		json.put(OwnerAssignmentLoader.createdAfter, "2019-01-01 00:00");
		json.put(OwnerAssignmentLoader.domain, "IL-Assist");
		json.put(OwnerAssignmentLoader.owners, "sweta");
		
		CarIQLoaderContext context = new CarIQLoaderContext();
		
		loader.load(json, context);
		
		System.out.println("Context: " + context.toString());
	}
}
