package com.cariq.toolkit.serviceimpl;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Service;

import com.cariq.toolkit.model.ProxyObject;
import com.cariq.toolkit.service.TestService;
import com.cariq.toolkit.utils.GenericJSON;

@Service
@Configurable
public class TestServiceImpl implements TestService {

	@Override
	public GenericJSON mysqlPing() {
		long startTS = System.currentTimeMillis();
		ProxyObject.getAllObjects(1, 10);
		return GenericJSON.create("pong", System.currentTimeMillis() - startTS);
	}
}
