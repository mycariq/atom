/**
 * 
 */
package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.core.io.ClassPathResource;

import com.atom.www.helper.MISReportHelper;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.ExportedData;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.analytics.application.IndividualLoadMatrixReporter;
import com.cariq.toolkit.utils.analytics.core.AnalyticsContext;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsCookbook;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsProcess;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsRecipe;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.google.common.base.Strings;

/**
 * The Class IndividualLoadMatrixReportExporter.
 *
 * @author santosh
 */
public class IndividualLoadMatrixReportExporter extends CarIQExporterTemplate {

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("IndividualLoadMatrixReportExporter");

	/** The Constant EXPORTER. */
	public static final String EXPORTER = "IndividualLoadMatrixReportExporter";

	MISReportHelper reportHelper = new MISReportHelper();

	public static final String FROM_TAT_URL = "FROM_TAT_URL", FROM_TAT_FILE = "FROM_TAT_FILE",
			FROM_LATEST_TAT_URL = "FROM_LATEST_TAT_URL";

	public static final String TAT_FILE_URL = "TATFileURL", TAT_FILE_PATH = "TATFilePath";

	public static final String INDIVIDUAL_LOAD_MATRIX_REPORT_BUILDER_JSON = "json/IndividualLoadMatrixReportBulilder.json";
	public static final String INDIVIDUAL_LOAD_MATRIX_REPORT_RECIPE = "IndividualLoadMatrixReportRecipe";
	public static final String INDIVIDUAL_LOAD_MATRIX_REPORT = "IndividualLoadMatrixReport";

	public IndividualLoadMatrixReportExporter() {
		super(EXPORTER, "Individual Load Matrix Report Exporter");

		addQueryType(FROM_TAT_URL, "Generates Individual Load Matrix Report from given TAT report URL",
				Arrays.asList(new QueryDefinitionAttribute(TAT_FILE_URL, "TATReport URL")));

		addQueryType(FROM_LATEST_TAT_URL, "Generates Individual Load Matrix Report from latest TAT report URL",
				new String[] {});

		// Support roles
		addSupportedRoles(Arrays.asList(Utils.ADMIN_ROLE));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate#doExport(java.lang.
	 * String, com.cariq.toolkit.utils.GenericJSON, int, int)
	 */
	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		try (ProfilePoint _doExport = ProfilePoint.profileAction("IndividualLoadMatrixReportExporter_doExport")) {
			String tatFileURL = null;

			// Ignore pageNo, ignorepageSize, just return alerts of each type
			if (pageNo > 1)
				return null;

			logger.debug("doExport - queryType : " + queryType + " pageNo : " + pageNo + " pageSize : " + pageSize
					+ " json :" + json.toString());

			if (queryType.equalsIgnoreCase(FROM_TAT_URL)) {
				tatFileURL = Utils.getValue(String.class, json, TAT_FILE_URL);
			} else if (queryType.equalsIgnoreCase(FROM_LATEST_TAT_URL)) {
				ExportedData exportedData = ExportedData.getLatestExportedData(
						StagingAndPolicyDetailsMiniReportExporter.TheExporter, CarIQFileUtils.CSV);
				if (null != exportedData)
					tatFileURL = exportedData.getFileUrl();
			} else
				Utils.handleException("QueryType " + queryType + " is not supported!!");

			Utils.checkNotNullOrEmpty(tatFileURL, "file url should not be empty");

			return getIndividualLoadMatrixReport(queryType, json, tatFileURL);
		}
	}

	private List<GenericJSON> getIndividualLoadMatrixReport(String queryType, GenericJSON json, String fileURL) {
		try (ProfilePoint _getIndividualLoadMatrixReport = ProfilePoint
				.profileAction("IndividualLoadMatrixReportExporter_getIndividualLoadMatrixReport")) {
			List<GenericJSON> retList = new ArrayList<GenericJSON>();
			logger.debug("input json for report:>> " + json);
			reportHelper.configureDomainUser(json);

			String localFilePath = CarIQFileUtils.getLocalFilePathFromURL(fileURL);
			if (Strings.isNullOrEmpty(localFilePath) || !CarIQFileUtils.exists(localFilePath))
				Utils.handleException("Invalid report file: " + localFilePath);

			// Process the TimeLine CSV and get the report JSON 
			String reportJSONStr = analyzeReport(localFilePath);

			// Convert processed JSON to local HTML file
			String fileName = INDIVIDUAL_LOAD_MATRIX_REPORT + "-" + Utils.createDateId(true) + ".html";
			String htmlFilePath = reportHelper.convertJSONToHTMLFile(reportJSONStr, fileName, null);
			if (!CarIQFileUtils.exists(htmlFilePath))
				Utils.handleException("Failed to create HTML report from processed JSON");

			// upload and persist HTML file
			GenericJSON outJSON = reportHelper.exportHTMLReport(INDIVIDUAL_LOAD_MATRIX_REPORT, queryType, htmlFilePath,
					fileName, json.toString());
			// reportHelper.exportCompressedHTMLReport(INDIVIDUAL_LOAD_MATRIX_REPORT, queryType, htmlFilePath, fileName, json.toString());

			retList.add(outJSON);
			return retList;
		}
	}

	private String analyzeReport(String loadMatrixFile) {
		try (ProfilePoint _analyzeReport = ProfilePoint.profileAction("IndividualLoadMatrixReportExporter_analyzeReport")) {
			String outJSONString = null;

			try {
				String builderFilePath = new ClassPathResource(INDIVIDUAL_LOAD_MATRIX_REPORT_BUILDER_JSON).getFile()
						.getAbsolutePath();

				AnalyticsContext ctx = new AnalyticsContext();
				ctx.put(IndividualLoadMatrixReporter.INDIVIDUAL_LOAD_MATRIX_JSON_PATH, builderFilePath);
				AnalyticsRecipe recipe = AnalyticsCookbook.getInstance()
						.getRecipe(INDIVIDUAL_LOAD_MATRIX_REPORT_RECIPE);

				AnalyticsProcess process = new AnalyticsProcess(recipe, ctx, loadMatrixFile);
				process.execute();
				// get output JSON
				outJSONString = process.getContext().get(IndividualLoadMatrixReporter.INDIVIDUAL_LOAD_MATRIX_JSON)
						.toString();
				//close
				process.close();
			} catch (Exception e) {
				Utils.handleException(e);
			}

			return outJSONString;
		}
	}
}
