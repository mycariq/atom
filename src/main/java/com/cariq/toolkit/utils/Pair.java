package com.cariq.toolkit.utils;

public class Pair <FIRST, SECOND> {
	
	FIRST first;
	SECOND second;
	
	public FIRST getFirst() {
		return first;
	}

	public SECOND getSecond() {
		return second;
	}

	public Pair(FIRST first, SECOND second) {
		super();
		this.first = first;
		this.second = second;
	}
	

}
