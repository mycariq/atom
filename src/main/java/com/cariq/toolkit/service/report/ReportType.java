/**
 * 
 */
package com.cariq.toolkit.service.report;

/**
 * @author hrishi
 *
 */
public enum ReportType {
	/** The carlog. */
	DEMO,
	FLEET_OVERALL,
	FLEET_DRIVER,
	CARS_DAILY
}
