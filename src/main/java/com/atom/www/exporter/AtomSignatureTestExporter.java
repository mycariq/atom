package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Configurable;

import com.cariq.toolkit.coreiq.exporter.CarIQExporterWrapper;
import com.cariq.toolkit.coreiq.exporter.CarIQWrappedExporterTemplate;
import com.cariq.toolkit.model.ProxyObject;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.SimplePOJOs.CountPOJO;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

@Configurable
public class AtomSignatureTestExporter extends CarIQWrappedExporterTemplate {

	private static final String wrappedExporterName = "StagingAndInsurancePolicyDetailsExporter";

	private static final String wrappedExporterDescription = "This is Onboarding Report";

	private static final String TheExporter = "AtomSignatureTestExporter";
	private static final String TheDescription = "Test exporter for testing AtomSignatureTestExporter";

	private static final String KEY_DOMAIN_NAME = "domainName";

	public AtomSignatureTestExporter() {
		super(TheExporter, TheDescription);

		// Support roles
		addSupportedRoles(
				Arrays.asList(Utils.ADMIN_ROLE, Utils.USER_ROLE, Utils.ROLE_FLEET_ADMIN, Utils.ROLE_GROUP_OPERATOR));
	}

	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		return super.doExport(queryType, json, pageNo, pageSize);
	}

	@Override
	protected CarIQExporterWrapper getWrapper() {
		CarIQExporterWrapper wrapper = new CarIQExporterWrapper(wrappedExporterName, wrappedExporterDescription);
		return wrapper;
	}

	@Override
	protected List<GenericJSON> postProcess(String queryType, GenericJSON inputJson, List<GenericJSON> outputJson) {

		try (ProfilePoint _postProcessAtomSignatureTestExporter = ProfilePoint
				.profileAction("ProfAction_postProcessAtomSignatureTestExporter")) {
			if (outputJson == null)
				return new ArrayList<>();
			if (outputJson.isEmpty())
				return new ArrayList<>();
			GenericJSON firstObject = outputJson.get(0);
			String domainName = (String) firstObject.get(KEY_DOMAIN_NAME);
			AtomSignatureRecordsExporter exporter = new AtomSignatureRecordsExporter();
			GenericJSON json = new GenericJSON();
			String policyNumbers = getPolicyNumbers(outputJson);
			json.put("signatures", policyNumbers);
			json.put("domain", domainName);
			json.put("policy_type", "InsurancePolicy");
			List<GenericJSON> allJSONs = exporter.doExport(queryType, json, 1, 1000000);
			List<GenericJSON> output = new ArrayList<>();
			for (GenericJSON proxy : allJSONs) {
				GenericJSON newJson = new GenericJSON();
				ProxyObject proxyObject = (ProxyObject) proxy.get("proxy_object");
				newJson.put("createdOn", proxyObject.getCreatedOn());
				newJson.put("signature", proxyObject.getSignature());
				output.add(newJson);
			}
			return output;
		}
	}

	private String getPolicyNumbers(List<GenericJSON> outputJson) {
		StringBuilder builder = new StringBuilder();
		for (GenericJSON json : outputJson) {
			String policyNumber = (String) json.get("policyNumber");
			builder.append(policyNumber);
			builder.append(',');
		}
		builder.deleteCharAt(builder.lastIndexOf(","));
		return builder.toString();

	}

	@Override
	public CountPOJO getExportCount(String queryType, GenericJSON json) {
		throw new RuntimeException("Not yet implemented.");
	}

	@Override
	public List<GenericJSON> exportRecords(String queryType, GenericJSON json, int startIndex, int endIndex) {
		throw new RuntimeException("Not yet implemented.");
	}
}
