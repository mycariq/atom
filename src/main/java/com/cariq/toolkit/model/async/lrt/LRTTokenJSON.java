package com.cariq.toolkit.model.async.lrt;

public class LRTTokenJSON {
	String lrtToken;

	public LRTTokenJSON() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LRTTokenJSON(String lrtToken) {
		super();
		this.lrtToken = lrtToken;
	}

	public String getLrtToken() {
		return lrtToken;
	}

	public void setLrtToken(String lrtToken) {
		this.lrtToken = lrtToken;
	}
}
