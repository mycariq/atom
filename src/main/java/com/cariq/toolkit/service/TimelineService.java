package com.cariq.toolkit.service;

import java.util.List;

import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.ProxyObject;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_In;
import com.cariq.toolkit.model.Timeline.Timeline_In;
import com.cariq.toolkit.model.Timeline.Timeline_Out;
import com.cariq.toolkit.utils.ResponseWithIdJson;

public interface TimelineService {

	
	/**
	 * Create single timeline item
	 * 
	 * @param timelineIn
	 * @return
	 */
	ResponseWithIdJson addTimelineItem(Timeline_In timelineIn);
	
	
	/**
	 * Get timeline for the given proxy(policy or user)
	 * 
	 * @param proxyObjectId
	 * @return
	 */
	List<Timeline_Out> getTimeline(Long proxyObjectId);



	/**
	 * Get timeline by using policyNumber, DomainId, proxyType
	 * 
	 * @param policyNumber
	 * @param domainId
	 * @param proxyType
	 * @return
	 */
	List<Timeline_Out> getTimeline(String policyNumber, Long domainId, String proxyType);



	/**
	 * Delete timeline by timeline In(By using createdOn date and proxyObject
	 * 
	 * @param timelineIn
	 * @return
	 */
	ResponseWithIdJson deleteTimeline(Long timelineId);
	
	
	
	/**
	 * Get all timelines for given all proxy objects
	 * 
	 * @param proxyObjects		Proxy objects for which timelines need to fetch 
	 * @return					List of timelines
	 */
	List<Timeline_Out> getTimelines(List<ProxyObject> proxyObjects);
	
	
	/**
	 * Add simple timeline
	 * 
	 * @param message
	 * @param type
	 * @param username
	 * @return
	 */
	ResponseWithIdJson addSimpleTimeline(ProxyObject_In proxyObject, String message, String type, String stage, String tag, String username);
}
