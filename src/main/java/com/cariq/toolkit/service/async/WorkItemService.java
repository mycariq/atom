package com.cariq.toolkit.service.async;

import java.util.List;

import com.cariq.toolkit.model.User;
import com.cariq.toolkit.model.async.WorkItem;
import com.cariq.toolkit.model.async.WorkItem.WorkItem_In;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.ResponseJson;

/**
 * The Interface WorkItemService.
 */
public interface WorkItemService {

	/**
	 * Adds the work item.
	 *
	 * @param workerClass the worker class
	 * @param inputJson the input json
	 * @param type the type
	 * @param name the name
	 * @param descriptions the descriptions
	 * @param user the user
	 * @return the response json
	 */
	public ResponseJson addWorkItem(String workerClass, String inputJson, String type, String name, String descriptions,
			User user);

	/**
	 * Adds the work item.
	 *
	 * @param workerClass the worker class
	 * @param inputJson the input json
	 * @param type the type
	 * @param name the name
	 * @param descriptions the descriptions
	 * @param user the user
	 * @return the response json
	 */
	public ResponseJson addWorkItem(String workerClass, String inputJson, String type, String name, String descriptions);
	/**
	 * Adds the work item.
	 *
	 * @param workerClass the worker class
	 * @param inputJson the input json
	 * @param type the type
	 * @param name the name
	 * @param descriptions the descriptions
	 * @param user the user
	 * @param parent the parent
	 * @return the response json
	 */
	public ResponseJson addWorkItem(String workerClass, String inputJson, String type, String name, String descriptions,
			User user, WorkItem parent);

	/**
	 * Adds the work item.
	 *
	 * @param <T> the generic type
	 * @param workerClass the worker class
	 * @param inputPOJO the input POJO
	 * @param type the type
	 * @param name the name
	 * @param descriptions the descriptions
	 * @return the response json
	 */
	public <T> ResponseJson addWorkItem(Class<? extends WorkItemDelegateTemplate> workerClass, T inputPOJO, String type,
			String name, String descriptions);

	/**
	 * Adds the work item.
	 *
	 * @param <T> the generic type
	 * @param workerClass the worker class
	 * @param inputPOJO the input POJO
	 * @param type the type
	 * @param name the name
	 * @param descriptions the descriptions
	 * @param parent the parent
	 * @return the response json
	 */
	public <T> ResponseJson addWorkItem(Class<? extends WorkItemDelegateTemplate> workerClass, T inputPOJO, String type,
			String name, String descriptions, WorkItem parent);

	/**
	 * Adds the work item.
	 *
	 * @param workerClass the worker class
	 * @param inputJson the input json
	 * @param type the type
	 * @param name the name
	 * @param descriptions the descriptions
	 * @param user the user
	 * @return the response json
	 */
	public ResponseJson addWorkItem(String workerClass, GenericJSON inputJson, String type, String name,
			String descriptions, User user);


	/**
	 * Work item creation using the In pattern
	 * @param workItem
	 * @return
	 */
	public ResponseJson addWorkItem(WorkItem_In workItem);

	/**
	 * Create bunch of work items with successor predecessor links
	 * @param workItemList
	 * @return
	 */
	public ResponseJson addWorkItemChain(List<WorkItem_In> workItemList);
	
	
	
	/**
	 * Gets the list of work item.
	 *
	 * @param type the type
	 * @param status the status
	 * @param pageNo the page no
	 * @param size the size
	 * @return the list of work item
	 */
	public Object getListOfWorkItem(String type, String status, int pageNo, int size);

	/**
	 * Gets the work item by UUID.
	 *
	 * @param uuid the uuid
	 * @return the work item by UUID
	 */
	public Object getWorkItemByUUID(String uuid);

	/**
	 * Gets the list of work item by type.
	 *
	 * @param type the type
	 * @param pageNo the page no
	 * @param size the size
	 * @return the list of work item by type
	 */
	public Object getListOfWorkItemByType(String type, int pageNo, int size);

	/**
	 * Gets the list of work item by status.
	 *
	 * @param status the status
	 * @param pageNo the page no
	 * @param size the size
	 * @return the list of work item by status
	 */
	public Object getListOfWorkItemByStatus(String status, int pageNo, int size);

	/**
	 * Gets the list of work item.
	 *
	 * @param pageNo the page no
	 * @param size the size
	 * @return the list of work item
	 */
	public Object getListOfWorkItem(int pageNo, int size);

	/**
	 * Gets the list of work item type.
	 *
	 * @return the list of work item type
	 */
	public Object getListOfWorkItemType();

	/**
	 * Gets the list of work item status.
	 *
	 * @return the list of work item status
	 */
	public Object getListOfWorkItemStatus();

}

