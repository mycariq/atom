package com.cariq.toolkit.service;

import java.util.List;

import com.cariq.toolkit.model.BillingAccount.BillingAccount_In;
import com.cariq.toolkit.model.BillingAccount.BillingAccount_Out;
import com.cariq.toolkit.model.BillingContract.BillingContract_In;
import com.cariq.toolkit.model.BillingContract.BillingContract_Out;
import com.cariq.toolkit.model.BillingTerm.BillingTerm_In;
import com.cariq.toolkit.model.BillingTerm.BillingTerm_Out;
import com.cariq.toolkit.model.Payment.Payment_In;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;

/**
 * The Interface BillingService.
 */
public interface BillingService {

	/**
	 * Adds the BillingAccount.
	 * @param params
	 * @return
	 */
	public ResponseWithIdJson addBillingAccount(BillingAccount_In params);

	/**
	 * Gets the BillingAccount.
	 * @param name
	 * @return
	 */
	public BillingAccount_Out getBillingAccount(String name);
	
	/**
	 * Delete BillingAccount.
	 * @param id
	 * @return
	 */
	public ResponseJson deleteBillingAccount(Long id);

	/**
	 * Updates the BillingAccount.
	 * @param params
	 * @return
	 */
	public ResponseJson updateBillingAccount(BillingAccount_In params);
	
	/**
	 * Get All BillingAccounts.
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public List<BillingAccount_Out> getAllBillingAccounts(int pageNo, int pageSize);
	
	/*
	 * APIs for BillingContract
	 */
	
	/**
	 * Create new billing contract
	 * @param params
	 * @return
	 */
	public ResponseWithIdJson addBillingContract(BillingContract_In params);
	
	/**
	 * Update billing contract details
	 * @param id
	 * @param params
	 * @return
	 */
	public ResponseJson updateBillingContract(BillingContract_In params);
	
	/**
	 * Get billing contract by id
	 * @param id
	 * @return
	 */
	public BillingContract_Out getBillingContract(Long id);
	
	/**
	 * Delete billing contract by id
	 * @param id
	 * @return
	 */
	public ResponseJson deleteBillingContract(Long id);
	
	/*
	 * APIs for BillingTerm
	 */
	
	/**
	 * Add billing term
	 * @param params
	 * @return
	 */
	public ResponseWithIdJson addBillingTerm(BillingTerm_In params);
	
	/**
	 * Update billing term details
	 * @param id
	 * @param params
	 * @return
	 */
	public ResponseJson updateBillingTerm(BillingTerm_In params);
	
	/**
	 * Get billing term by id
	 * @param id
	 * @return
	 */
	public BillingTerm_Out getBillingTerm(Long id);
	
	/**
	 * Get billing term by name
	 * @param name
	 * @return
	 */
	public BillingTerm_Out getBillingTerm(String name);
	
	/**
	 * Delete billing term by id
	 * @param id
	 * @return
	 */
	public ResponseJson deleteBillingTerm(Long id);
	
	/**
	 * Add payment
	 * @param params
	 * @return
	 */
	public ResponseWithIdJson addPayment(Payment_In params);
	
	/**
	 * Delete payment by id
	 * @param id
	 * @return
	 */
	public ResponseJson deletePayment(Long id);
	
	/**
	 * Update invoice status
	 * @param id
	 * @param params
	 * @return
	 */
	public ResponseJson updateInvoiceStatus(Long id, String status);
	
	/**
	 * Destroy invoice
	 * @param id
	 * @return
	 */
	public ResponseJson destroyInvoice(Long id);
}