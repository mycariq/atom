package com.cariq.toolkit.utils;

public 	 class BeanAttribute {
	String name;
	Class<?> clazz;
	boolean optional;
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @return the optional
	 */
	public boolean isOptional() {
		return optional;
	}
	public BeanAttribute(String name, Class clazz, boolean optional) {
		super();
		this.name = name;
		this.clazz = clazz;
		this.optional = optional;
	}
	
	public BeanAttribute(String name, boolean optional) {
		super();
		this.name = name;
		this.optional = optional;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @param optional the optional to set
	 */
	public void setOptional(boolean optional) {
		this.optional = optional;
	}
	public Class getClazz() {
		return clazz;
	}
	public void setClazz(Class clazz) {
		this.clazz = clazz;
	}
	
	
}