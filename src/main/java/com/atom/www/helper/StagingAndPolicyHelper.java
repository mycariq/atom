package com.atom.www.helper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.ProxyObject;
import com.cariq.toolkit.model.Timeline.Timeline_Out;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.google.common.base.Strings;

public class StagingAndPolicyHelper {

	public static final String NOT_AVAILABLE = "Not Available";
	private static final String UNDER_VERIFICATION = "Under verification";
	private static final String STATE = "State";
	public static final String KEY_POLICY_NO = "policyNumber";
	private static final String KEY_TIER_CITY = "TierCity";
	private static final String KEY_EMAIL = "Staging_customerEmail";
	
	private static final String POLICY_STATUS_FIELD_ASSIGNED = "Field assigned";
	
	public static final String POLICY_STATE_INVALID = "Invalid";
	private static final String POLICY_STATE_ACTIVE = "Active";
	private static final String POLICY_STATE_SNOOZE = "Snooze";
	
	public static final String proxyType = "InsurancePolicy";
	
	private static final String TIER_A = "Tier-A";
	
	public static final String TAG_NOT_INTERESTED = "NOT_INTERESTED";
	public static final String TAG_NOT_CONTACTABLE = "NOT_CONTACTABLE";
	public static final String TAG_DUPLICATE = "DUPLICATE";
	public static final String TAG_TEST = "Test";
	public static final String TAG_CANCELLED = "Cancelled";
	public static final String TAG_INVALID_ORDER = "Invalid order";
	private static final String TAG_SNOOZE = "Snooze";
	public static final String TAG_READY_TO_RETRIEVE = "READY_TO_RETRIEVE";
	public static final String TAG_READY_TO_RENEW = "READY_TO_RENEW ";
	public static final String TAG_GOT_DEVICE_BACK = "GOT_DEVICE_BACK";
	public static final String TAG_LOST_DEVICE = "LOST_DEVICE";
	
	private static final String STATE_NOT_VERIFIED = "Not Verified";
	
	private static final String KEY_TIMELINE = "timeline";
	
	private static final String STAGE_CAR_CONNECTED = "Car Connected"; 
	
	private static final String QueryType_createdAfter = "CreatedAfter";
	private static final String QueryType_allCustomers = "All Users";
	private static final String QueryType_nonContactable = "Not contactable";
	private static final String QueryType_nonInterested = "Not interrested";
	private static final String QueryType_feedback = "Feedback";
	private static final String QueryType_notVerified = "Not Verified";
	private static final String QueryType_userCreated = "User Created";
	private static final String QueryType_deviceRequested = "Device Requested";
	private static final String QueryType_deviceShipped = "Device Shipped";
	private static final String QueryType_deviceOnBoarded = "Device OnBoarded";
	private static final String QueryType_carConnected = "Car Connected";
	private static final String QueryType_KeywordSearch = "KeywordSearch";
	private static final String QueryType_FieldAssistRequested = "FieldAssist Requested";
	public static final String QueryType_ByPolicyNumber = "ByPolicyNumber";
	private static final String QueryType_TestUser = "test-users";
	private static final String QueryType_Duplicate_User = "duplicate-users";		
	private static final String QueryType_Cancelled_Policy = "cancelled-policies";
	private static final String QueryType_Snoozed_Policy = "snoozed-users";
	private static final String QueryType_Invalid_Policy = "invalid-users";
	public static final String QueryType_PreShipment = "Pre-Shipment";
	public static final String QueryType_PostShipment = "Post-Shipment";
	public static final String QueryType_NotConnected = "Not-Connected";
	public static final String QueryType_Renewal = "Due for renewal";
	public static final String QueryType_Retrieval = "Due for retrieval";
	public static final String QueryType_PoliciesToRenew = "PoliciesToRenew";
	public static final String QueryType_PoliciesToRetrieve = "PoliciesToRetrieve";
	public static final String QueryType_createdAfterIncludeInvalid = "CreatedAfterIncludeInvalid";

	// Pre-Shipment stages
	public static final String StateUnVerified = "Not Verified";
	public static final String StateUserCreated = "User Created";
	public static final String StateDownloadedApp = "Downloaded App";
	public static final String StateDeviceRequested = "Device Requested";
	public static final String StateFieldAssistRequested = "FieldAssist Requested";

	// Post-Shipment stages
	public static final String StateDeviceShipped = "Device Shipped";
	public static final String StateDeviceDelivered = "Device Delivered";
	public static final String StateDeviceExceptionOrRTO = "Device RTO";
	public static final String StateDeviceOnBoarded = "Device OnBoarded";
	public static final String StateCarConnected = "Car Connected";
	public static final String StateRenewal = QueryType_Renewal;
	public static final String StateRetrieval = QueryType_Retrieval;
	
	public static final String SubscriptionStateActive = "SubscriptionActive";
	
	public static final String STAGING_TAG_CARNOT = "CARNOT_POLICY";
	public static final String STAGING_TAG_INVALID_TRANSACTION = "RENEWAL_POLICY_INVALID_TRANSACTION";
	public static final String STAGING_TAG_NEW_INVALID = "NEW_POLICY_INVALID";
	public static final String STAGING_TAG_DUP_CAR = "NEW_POLICY_DUPCAR";
	
	public static final String CATEGORY_PROCESSED = "PROCESSED";
	public static final String CATEGORY_NOT_VERIFIED = "NOT_VERIFIED";
	public static final String CATEGORY_NOT_VERIFIED_NI = "NOT_VERIFIED_NI";
	public static final String CATEGORY_NOT_VERIFIED_NC = "NOT_VERIFIED_NC";
	public static final String CATEGORY_INVALID_CARNOT = "INVALID_CARNOT";
	public static final String CATEGORY_INVALID_TRANSACTION = "INVALID_TRANSACTION";
	public static final String CATEGORY_INVALID_DUP_CAR = "INVALID_DUP_CAR";

	
	//List of all stages
	public static final List<String> stages = new ArrayList<String>();
	
	static {
		stages.add(StateUnVerified);
		stages.add(StateUserCreated);
		stages.add(StateDownloadedApp);
		stages.add(StateDeviceRequested);
		stages.add(StateFieldAssistRequested);
		
		stages.add(StateDeviceShipped);
		stages.add(StateDeviceDelivered);
		stages.add(StateDeviceExceptionOrRTO);
		stages.add(StateDeviceOnBoarded);
		stages.add(StateCarConnected);
		stages.add(StateRenewal);
		stages.add(StateRetrieval);
	}
	
	/**
	 * Get number of weeks between staging created and today
	 * @param json
	 * @return
	 */
	public static int getWeeksFromStagingCreatedDate(GenericJSON json) {
		String staginCreatedOnStr = (String) json.get("StagingCreatedOn");
		Date dateObj = Utils.convertToDate(staginCreatedOnStr, Utils.YYYY_MM_DD_hh_mm_ss);
		
		return Utils.getDiffInWeeks(Utils.getStartDateOfYear(), dateObj);
	}
	
	
	/**
	 * Find given user is test user or not
	 * @param json
	 * @return
	 */
	protected static boolean isTestUser(GenericJSON json) {
		String policyNumber = (String) json.get(KEY_POLICY_NO);
		String mobileNumber = (String) json.get(KEY_EMAIL);
		if(policyNumber == null) return true;
		if(Utils.containsIgnoreCase(policyNumber, "Dummy"))
			return true;
		
		@SuppressWarnings("unchecked")
		List<Timeline_Out> timelines = (List<Timeline_Out>) json.get(KEY_TIMELINE);		
		return doesTimelineHasTestTag(timelines);
	}
	

	/**
	 * Get device shipped on date
	 * 
	 * @param json
	 * @return
	 */
	public static String getDeviceShippedOnDate(GenericJSON json) {
		try (ProfilePoint _getDeviceShippedOnDate = ProfilePoint
				.profileActivity("StagingAndPolicyHelper_getDeviceShippedOnDate")) {

			String state = Utils.getValue(json, STATE);
			if (state == null)
				return null;

			String shippedOrDeliveredOn = Utils
					.convertToyyyymmdd_hhmmssFormat(Utils.getValue(Date.class, json, "Shipped_Or_Delivered_On"));

			if (state.equalsIgnoreCase("Device Shipped") || state.equalsIgnoreCase("Device OnBoarded")
					|| state.equalsIgnoreCase("Car Connected") || state.equalsIgnoreCase("Device Delivered")) {

				String shippedOn = Utils.convertToyyyymmdd_hhmmssFormat(Utils.getValue(Date.class, json, "ShippedOn"));
				if (Strings.isNullOrEmpty(shippedOrDeliveredOn) || Strings.isNullOrEmpty(shippedOn))
					return null;

				Date shippedOnDate = Utils.getDate(shippedOn);
				Date shippedOrDeliveredDate = Utils.getDate(shippedOrDeliveredOn);
				if (shippedOrDeliveredDate.after(shippedOnDate))
					return Utils
							.convertToyyyymmdd_hhmmssFormat(Utils.DateByHoursBack(shippedOnDate, Utils.dayHours * 3));

				return shippedOrDeliveredOn;
			}

			return null;
		}
	}

	/**
	 * Get device requested on date
	 * 
	 * @param json	
	 * @return
	 */
	public static String getDeviceRequestedOnDate(GenericJSON json) {
		try (ProfilePoint _getDeviceRequestedOnDate = ProfilePoint
				.profileActivity("StagingAndPolicyHelper_getDeviceRequestedOnDate")) {

			String state = Utils.getValue(json, STATE);
			if (null == state)
				return null;

			String shipmentModifiedOn = Utils
					.convertToyyyymmdd_hhmmssFormat(Utils.getValue(Date.class, json, "Shipped_Or_Delivered_On"));
			if (null == shipmentModifiedOn)
				return null;

			String policyCreatedOnDate = Utils
					.convertToyyyymmdd_hhmmssFormat(Utils.getValue(Date.class, json, "Policy_CreationTime"));

			if (state.equalsIgnoreCase("Device Requested") || state.equalsIgnoreCase("Device Shipped")
					|| state.equalsIgnoreCase("Device Delivered") || state.equalsIgnoreCase("Device OnBoarded")
					|| state.equalsIgnoreCase("Car Connected")) {

				String deviceRequestedOnDate = findDeviceRequestedOnDate(shipmentModifiedOn, policyCreatedOnDate);
				String shippedOn = Utils.convertToyyyymmdd_hhmmssFormat(Utils.getValue(Date.class, json, "ShippedOn"));
				if (Strings.isNullOrEmpty(shippedOn) || Strings.isNullOrEmpty(deviceRequestedOnDate))
					return null;

				Date shippedOnDate = Utils.getDate(shippedOn);
				Date deviceRequestedOn = Utils.getDate(deviceRequestedOnDate);
				if (shippedOnDate.after(deviceRequestedOn))
					return Utils
							.convertToyyyymmdd_hhmmssFormat(Utils.DateByHoursBack(shippedOnDate, Utils.dayHours * 3));

				return deviceRequestedOnDate;
			}

			return null;
		}
	}

	
	/**
	 * Find device requested on date based on shipped date and user created on date
	 * 
	 * @param shipmentModifiedOn
	 * @param policyCreatedOnDate
	 * @return
	 */
	protected static String findDeviceRequestedOnDate(String shipmentModifiedOn, String policyCreatedOnDate) {
		
		String finalDate = null;
		try {
			if(shipmentModifiedOn == null || policyCreatedOnDate == null)
				return null;
			
			Date shippedDate = Utils.convertToDate(shipmentModifiedOn, Utils.YYYY_MM_DD_hh_mm_ss);
			Date userCreatedDate = Utils.convertToDate(policyCreatedOnDate, Utils.YYYY_MM_DD_hh_mm_ss);
			
			if(shippedDate == null)
				return null;
			
			//Get date 3 days previous to shipeed date
			Date requestedOnDate = Utils.DateByHoursBack(shippedDate, Utils.dayHours * 3);
			
			//if new request date is before user created date then update request date with 6 hours after user created date
			if(requestedOnDate.before(userCreatedDate)){
				requestedOnDate = new Date(userCreatedDate.getTime() + Utils.hourMiliSeconds * 6);
			}
	
			//if new request date is after shipment date then update request date with 1 hour after user created date
			if(requestedOnDate.after(shippedDate)) {
				requestedOnDate = new Date(userCreatedDate.getTime() + Utils.hourMiliSeconds * 1);
			}
			
			finalDate = Utils.convertToyyyymmdd_hhmmssFormat(requestedOnDate);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return finalDate;
	}
	
	
	/**
	 * Calculate turn around time between given two dates.
	 * 
	 * @param startDate		first date to calculate time 
	 * @param endDate		second date to calculate time
	 * @return				Return turnaround time in days			
	 */
	public static String calculateTAT(String startDate, String endDate) {
		if(startDate == null || endDate == null)
			return "N/A";
		Date sDate = Utils.convertToDate(startDate, Utils.YYYY_MM_DD_hh_mm_ss);
		Date eDate = Utils.convertToDate(endDate, Utils.YYYY_MM_DD_hh_mm_ss);
		
		Integer diff = Utils.getDiffInDays(sDate, eDate);
		if(diff == null)
			return "N/A";
		
		return String.valueOf(diff);
	}	
	
	/**
	 * Get policy status
	 * 
	 * @param isNotContactable
	 * @param isNotInterested
	 * @param isDuplicate
	 * @param json
	 * @return
	 */
	public static String getPolicyStatus(GenericJSON json) {
		@SuppressWarnings("unchecked")
		List<Timeline_Out> timelines = (List<Timeline_Out>) json.get(KEY_TIMELINE);
		
//		if(isTimelineContainGivenTag(timelines, TAG_NOT_CONTACTABLE)) return TAG_NOT_CONTACTABLE;
//		if(isTimelineContainGivenTag(timelines, TAG_NOT_INTERESTED)) return TAG_NOT_INTERESTED;
		
		String latestDesposition = Utils.getValue(json, "latest_desposition");
		if(latestDesposition != null) {
			return latestDesposition;
		}
		//If state is device requested and tier city is A then change policy state to Field
		//engineer assigned.
		String state = (String) json.get(STATE);
//		String tierCity = (String) json.get(KEY_TIER_CITY);
//		if(tierCity == null || state == null)
//			return (String) json.get(STATE);
//		if(state.equalsIgnoreCase(QueryType_deviceRequested) && tierCity.equalsIgnoreCase(TIER_A))
//			return POLICY_STATUS_FIELD_ASSIGNED;
		
		if(STATE_NOT_VERIFIED.equalsIgnoreCase(state))
			state = UNDER_VERIFICATION;
		return state;
	}
//	
//	/**
//	 * Get latest desposition of proxyObject.
//	 * 
//	 * @param json
//	 * @return
//	 */
//	public static String getLatestDesposition(GenericJSON json) {
//		
//	}
	
	
	/**
	 * Get policy stage
	 * @param json
	 * @return
	 */
	public static String getPolicyStage(GenericJSON json) {
		
		//If state is device requested and tier city is A then change policy state to Field
		//engineer assigned.
		String stage = Utils.getValue(json, STATE);
		String tierCity = (String) json.get(KEY_TIER_CITY);
//		if(tierCity == null || stage == null)
//			return (String) json.get(STATE);
//		if(stage.equalsIgnoreCase(QueryType_deviceRequested) && tierCity.equalsIgnoreCase(TIER_A))
//			return POLICY_STATUS_FIELD_ASSIGNED;
		
		if(STATE_NOT_VERIFIED.equalsIgnoreCase(stage))
			stage = UNDER_VERIFICATION;
		return stage;
	}
	
	/**
	 * Get state of given policy
	 * @param json
	 * @return
	 */
	public static String getPolicyState(GenericJSON json) {
		//this state could be in original state or tag on timeline(nc, ni, test, duplicate)
		String state = getPolicyStatus(json);	
		
		//if duplicate or test then return state as invalid
		if(Utils.isValueIn(state, TAG_DUPLICATE, TAG_TEST, TAG_CANCELLED, TAG_INVALID_ORDER)
				|| isTestUser(json))
			return POLICY_STATE_INVALID;
		
		String stage = (String) json.get(STATE);
		String subscriptionState = (String) json.get("SubscriptionState");
		//if car is connected then policy state is active
		if(STAGE_CAR_CONNECTED.equalsIgnoreCase(stage) 
				&& SubscriptionStateActive.equalsIgnoreCase(subscriptionState))
			return POLICY_STATE_ACTIVE;
		
		//if nc or ni then return nc or ni
		if(Utils.isValueIn(state, TAG_NOT_CONTACTABLE, TAG_NOT_INTERESTED,
				POLICY_STATE_SNOOZE, POLICY_STATE_ACTIVE, 
				TAG_READY_TO_RENEW, TAG_READY_TO_RETRIEVE, TAG_GOT_DEVICE_BACK, TAG_LOST_DEVICE))
			return state;
		//it means state is original state. in this case return state as active.
		return POLICY_STATE_ACTIVE;
	}
	
	/**
	 * Is given user is not contactable
	 * 
	 * @param json
	 * @return
	 */
	public static boolean isNotContactable(List<Timeline_Out> timelines) {
		return isTimelineContainGivenTag(timelines, TAG_NOT_CONTACTABLE);
	}
	
	
	/**
	 * Find whether timeline contain test tag or not
	 * @param timelines
	 * @return
	 */
	public static boolean doesTimelineHasTestTag(List<Timeline_Out> timelines) {
		return isTimelineContainGivenTag(timelines, TAG_TEST);
	}
	

	/**
	 * Is duplicate user
	 * @param timelines
	 * @return
	 */
	public static boolean isDuplicate(List<Timeline_Out> timelines) {
		return isTimelineContainGivenTag(timelines, TAG_NOT_CONTACTABLE);
	}
	
	
	/**
	 * Get not contactable date
	 * 
	 * @param json
	 * @return
	 */
	public static String getNotContactableDate(List<Timeline_Out> timelines) {
		return getTagDate(timelines, TAG_NOT_CONTACTABLE);
	}

	
	/**
	 * Is given user is not interested
	 * 
	 * @param timelines
	 * @return
	 */
	public static boolean isNotInterested(List<Timeline_Out> timelines) {
		return isTimelineContainGivenTag(timelines, TAG_NOT_INTERESTED);
	}
	
	/**
	 * Find duplicate user from given timelines
	 * @param timelines		List of timelines
	 * @return				
	 */
	public static boolean isDuplicateUser(List<Timeline_Out> timelines) {
		return isTimelineContainGivenTag(timelines, TAG_DUPLICATE);
	}
	
	
	/**
	 * Is timelines contain given tag
	 * 
	 * @param timelines
	 * @param tag
	 * @return
	 */
	public static boolean isTimelineContainGivenTag(List<Timeline_Out> timelines, String tag) {
		if(timelines == null)
			return false;
		
		for(Timeline_Out t : timelines)
			if(getTimelineWithGivenTag(t, tag) != null)
				return true;
		
		return false;
	}
	
	
	/**
	 * Get latest timeline which has a tag.
	 * @param timelines
	 * @return
	 */
	public static Timeline_Out getLatestTimelineWithTag(List<Timeline_Out> timelines) {
		if(timelines == null)
			return null;
		for(Timeline_Out t : timelines) {
			if(t.getTag() != null && !Utils.EMPTY.equalsIgnoreCase(t.getTag()))
				return t;
		}
		return null;
	}
	
	
	/**
	 * Return latest timeline
	 *
	 * @param timelines
	 * @return
	 */
	public static Timeline_Out getLatestTimeline(List<Timeline_Out> timelines) {
		if(timelines == null )
			return null;
		if(timelines.isEmpty())
			return null;
		return timelines.get(0);
	}
	

	/**
	 * Get not contactable date
	 * 
	 * @param json
	 * @return
	 */
	public static String getNotInterestedDate(List<Timeline_Out> timelines) {
		return getTagDate(timelines, TAG_NOT_INTERESTED);
	}
	
	
	/**
	 * Get duplicate tag date
	 * 
	 * @param timelines
	 * @return
	 */
	public static String getDuplicateDate(List<Timeline_Out> timelines) {
		return getTagDate(timelines, TAG_DUPLICATE);
	}
	
	
	/**
	 * Get date for tag in timelines
	 * 
	 * @param timelines
	 * @param tag
	 * @return
	 */
	public static String getTagDate(List<Timeline_Out> timelines, String tag) {
		if(timelines == null)
			return null;
		for(Timeline_Out t : timelines) {
			Timeline_Out obj = getTimelineWithGivenTag(t, tag);
			if(obj != null) {
				return obj.getCreatedOn();
			}
		}
		return null;
	}
	
	
	/**
	 * Get timeline with give tag tag, if timeline not exists with not 
	 * given tag then return null;
	 * 
	 * @param timeline
	 * @return
	 */
	private static Timeline_Out getTimelineWithGivenTag(Timeline_Out timeline, String tag) {
		if(timeline == null)
			return null;
		if(timeline.getTag() != null && timeline.getTag().equalsIgnoreCase(tag))
			return timeline;
		return null;
	}
	
	
	/**
	 * Get value from json
	 * 
	 * @param json
	 * @param key
	 * @return
	 */
	public static String getValue(GenericJSON json, String key) {
		String value = Utils.getValue(json, key);
		if(value == null)
			value = NOT_AVAILABLE;
		return value;
	}


	/**
	 * Get desposition from timeline
	 * @param json
	 * @return
	 */
	public static String getDesposition(GenericJSON json) {
		@SuppressWarnings("unchecked")
		List<Timeline_Out> timelines = (List<Timeline_Out>) json.get(KEY_TIMELINE);
		 
		Timeline_Out timeline = getLatestTimelineWithTag(timelines);
		if (timeline == null)
			return NOT_AVAILABLE;
		String message = timeline.getMessage();
		if (message == null)
			return NOT_AVAILABLE;
		message = message.replaceAll("\"", "'");
		return "\"" + message + "\"";

	}
	
	/**
	 * Get desposition date
	 * 
	 * @param json
	 * @return
	 */
	public static String getDespositionDate(GenericJSON json) {
		@SuppressWarnings("unchecked")
		List<Timeline_Out> timelines = (List<Timeline_Out>) json.get(KEY_TIMELINE);
		Timeline_Out timeline = getLatestTimeline(timelines);
		return timeline == null ? NOT_AVAILABLE : timeline.getCreatedOn();
	}
	
	/**
	 * Get wrapped query type
	 * @param queryType
	 * @return
	 */
	public static String getWrappedQueryType(String queryType) {
		if(Utils.isValueIn(queryType, QueryType_allCustomers))
			return QueryType_createdAfter;
		
		if (Utils.isValueIn(queryType, QueryType_nonContactable, QueryType_nonInterested,
				QueryType_feedback, QueryType_TestUser, 
				QueryType_Duplicate_User, QueryType_Cancelled_Policy, QueryType_Snoozed_Policy,
				QueryType_Invalid_Policy))
			return QueryType_ByPolicyNumber;
		return queryType;
	}

	/**
	 * Get wrapped json
	 * @param queryType 
	 * @param json
	 * @return
	 */
	public static GenericJSON getWrappedJson(String queryType, GenericJSON json, int pageNumber, int pageSize) {
		if (Utils.isValueIn(queryType, QueryType_nonContactable, QueryType_nonInterested,
				QueryType_feedback, QueryType_TestUser, 
				QueryType_Duplicate_User, QueryType_Cancelled_Policy, QueryType_Snoozed_Policy,
				QueryType_Invalid_Policy)) {
			String tag = null;
			if (queryType.equalsIgnoreCase(QueryType_nonContactable)) {
				tag = TAG_NOT_CONTACTABLE;
			} else if (queryType.equalsIgnoreCase(QueryType_nonInterested)) {
				tag = TAG_NOT_INTERESTED;
			} else if (queryType.equalsIgnoreCase(QueryType_TestUser)) {
				tag = TAG_TEST;
			} else if (queryType.equalsIgnoreCase(QueryType_Duplicate_User)) {
				tag = TAG_DUPLICATE;
			} else if (queryType.equalsIgnoreCase(QueryType_Cancelled_Policy)) {
				tag = TAG_CANCELLED;
			} else if (queryType.equalsIgnoreCase(QueryType_Snoozed_Policy)) {
				tag = TAG_SNOOZE;
			} else if (queryType.equalsIgnoreCase(QueryType_Invalid_Policy)) {
				tag = TAG_INVALID_ORDER;
			}
			if(tag != null) {
				List<ProxyObject> proxyObjects = ProxyObject.getByTagAndDomain(tag, 
						Domain.getByName(new DomainHelper().getDomainByUser(User.getLoggedInUser()).getName()),
						pageNumber, pageSize);
				json.add("Keyword search", Utils.getCommaSeperatedString(ProxyObject.getSignatures(proxyObjects)));
			}
		}
		return json;
	}
	
	
	/**
	 * Is given state exists in known stages.
	 * 
	 * @param stage
	 * @return
	 */
	public static boolean stageExists(List<String> inputStages, String commaSeperatedStages) {
		String[] stages = Utils.getCommaSeperatedString(commaSeperatedStages);
		boolean exists = true; 
		
		for(String stage : stages) {
			if(inputStages.contains(stage) == false) {
				exists = false;
				break;
			}
		}
		return exists;
	}

	/**
	 * Get month from expiry date.
	 * 
	 * @param expiryDate
	 * @return
	 */
	public static String getMonth(String inputDate) {
		if(inputDate == null)
			return NOT_AVAILABLE;
		Date dateObj = Utils.fromTimeString(inputDate, Utils.getTimeZone(Utils.UTC),
				new String[] {Utils.YYYY_MM_DD_hh_mm_ss, Utils.YYYY_mm_dd, Utils.dd_mm_YYYY, Utils.d_mm_YYYY});
		if(dateObj == null)
			return null;
		return new SimpleDateFormat("MMMM").format(dateObj);
	}

	/**
	 * Get Year from expiry date.
	 * 
	 * @param value
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static String getYear(String inputDate) {
		if(inputDate == null)
			return NOT_AVAILABLE;
		Date dateObj = Utils.fromTimeString(inputDate, Utils.getTimeZone(Utils.UTC),
				new String[] {Utils.YYYY_MM_DD_hh_mm_ss, Utils.YYYY_mm_dd, Utils.dd_mm_YYYY, Utils.d_mm_YYYY});
		if(dateObj == null)
			return null;
		return String.valueOf(dateObj.getYear() + 1900);
	}
}
