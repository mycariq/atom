package com.cariq.toolkit.model.async.lrt;

public interface LRTFetcher {
	LRTJSON fetch(String lrtToken);
}
