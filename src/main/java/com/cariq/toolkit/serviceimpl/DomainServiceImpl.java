package com.cariq.toolkit.serviceimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.coreiq.model.ActionLog;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.Domain.Domain_In;
import com.cariq.toolkit.model.Domain.Domain_Out;
import com.cariq.toolkit.model.Domain.UpdateDomain_In;
import com.cariq.toolkit.service.DomainService;
import com.cariq.toolkit.utils.APIHelper;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.service.CarIQRestClientService;


/**
 * The Class DomainServiceImpl.
 */
@Service
public class DomainServiceImpl implements DomainService {

	/** The logger. */
	private static CarIQLogger logger = APIHelper.logger.getLogger("DomainServiceImpl");

	/** The rest service. */
	@Autowired
	CarIQRestClientService restService;

	@Override
	@Transactional
	public ResponseWithIdJson addDomain(Domain_In params) {
		Domain domain = null;
		try {
			Utils.logAction(logger, "DomainServiceImpl.addDomain", params.getName(), params.getUrl());
				domain = new Domain(params.getName(), params.getUrl(), 
						params.getAuthString(), params.getAuthType(),new Date(), new Date());
				
				domain.persist();
				
		} catch (Exception e) {
			Utils.handleException("Failed to add domain, Exception: " + e.getMessage());
		}
		return new ResponseWithIdJson(domain.getId().toString(), "Domain : " + params.getName() + " created successfully.");
	}

	@Override
	public List<Domain_Out> getAllDomains(int pageNo, int pageSize) {
		List<Domain_Out> resultList = new ArrayList<>();
		try {
			List<Domain> domains = Domain.getAllObjects(pageNo, pageSize);
			
			for (Domain domain : domains) {
				Domain_Out domainOut = domain.toJSON();
				resultList.add(domainOut);
			}
		} catch (Exception e) {
			Utils.handleException("Failed to get all domains, Exception: " + e.getMessage());
		}
		return resultList;
	}

	@Override
	public Domain_Out getDomain(String domainName) {
		Domain_Out domainOut = null;
		try {
			Domain domain = Domain.getByName(domainName);
			if (domain != null) {
				domainOut = domain.toJSON();
			}
		}catch (Exception e) {
			Utils.handleException("Failed to get domain: " + domainName + ", Exception: " + e.getMessage());
		}
		
		return domainOut;
	}

	@Override
	@Transactional
	public ResponseJson updateDomain(UpdateDomain_In params, String domainName) {
		try {
			Domain domain = Domain.getByName(domainName);
			if (domain == null) {
				Utils.handleException("Domain " + domainName + " is invalid");
			}
			domain.updateDomain(params.getName(), params.getUrl());
			domain.merge();
			ActionLog.log(Utils.createContext("DomainServiceImpl.updateDomain"), "Update", "domain",
					"Domain id : " + domain.getId() + " domainname : " + domain.getName(),
					"Old info :" + Utils.getJSonString(domain.toJSON()));
		} catch (Exception e) {
			Utils.handleException("Failed to update domain: " + domainName + ", exception: " + e.getMessage());
		}

		return new ResponseJson("Domain : " + domainName + " updated successfully.");
	}

	@Override
	@Transactional
	public ResponseJson deleteDomain(String domainName) {
		try {
			Domain domain = Domain.getByName(domainName);
			if (domain != null) {
				// domainaccess - FK constraint
				domain.remove();
				ActionLog.log(Utils.createContext("DomainServiceImpl.deleteeDomain"), "Delete", "domain",
					"Domain id : " + domain.getId() + " domainname : " + domain.getName(),
					"Old info :" + Utils.getJSonString(domain.toJSON()));
				return new ResponseJson("Domain : " + domainName + " deleted successfully.");
			}
		} catch (Exception e) {
			Utils.handleException("Failed to remove domain: " + domainName + ", Exception: " + e.getMessage());
		}
		return new ResponseJson("Domain : " + domainName + " does not exist!");
	}
}