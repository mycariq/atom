package com.cariq.toolkit.utils.analytics.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.profile.ProfilePoint;

public class CSVTableReaderStream extends TableReaderStream {
	static final int FETCH_SIZE = 5000;
	static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("CSVTableReaderStream");
	BufferedReader bufferedReader;
	RecordList recordList = null;

	static class RecordList {
		int currentIndex = 0;
		List<Record> recordList = new ArrayList<Record>();

		public RecordList(List<Record> recordList) {
			super();
			this.recordList = recordList;
			currentIndex = 0;
		}

		public boolean isProcessed() {
			return currentIndex >= recordList.size();
		}

		Record getNext() {
			return recordList.get(currentIndex++);
		}

		void close() {
			recordList.clear();
			recordList = null;
			currentIndex = 0;
		}
	}

	private List<Column> readHeader() {
		String[] params = readLine();
		if (params == null)
			return null;
		
		List<Column> list = new ArrayList<Column>();
		int i = 0;
		for (String param : params) {
			list.add(new Column(param.replaceAll("^\"|\"$", ""), i++));
		}

		return list;
	}

	private String[] readLine() {
		String line;
		try {
			try (ProfilePoint _readLine = ProfilePoint.profileAction("ProfAction_readLine")) {
				line = bufferedReader.readLine();
				// logger.debug(line);
			}
		} catch (IOException e) {
			return null;
		}
		if (null == line)
			return null;

		// @TODO use Apache-Commons csv
		try (ProfilePoint _splitLine = ProfilePoint.profileAction("ProfAction_splitLine")) {
			line = line.trim();
			if (line.length() < 1)
				return new String[] {};

			// Split on comma and trim whitespaces
			// String[] retval = line.split("\\s*,\\s*");
			//String[] retval = line.split(",");
			
			//Split on comma but ignore comma within quotes.
			String[] retval = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
			return retval;
		}
	}

	@Override
	public boolean hasNext() {
		if (recordList == null)
			return false;

		if (recordList.isProcessed()) {
			recordList.close();
			recordList = null;
			return fetch(FETCH_SIZE);
		}

		return true;
	}

	/**
	 * Return true if anything is fetched.
	 * 
	 * @param fetchSize
	 * @return
	 */
	private boolean fetch(int fetchSize) {
		try (ProfilePoint _TableReader_fetch = ProfilePoint.profileAction("ProfAction_TableReader_fetch")) {
			// From the file get next X rows in the form of Records (need java
			// stream)
			List<Record> lst = new ArrayList<Record>();
			for (int size = 0; size < fetchSize; size++) {
				String[] params = readLine();
				if (params == null) // unable to process line - at EOF
					break;

				if (params.length == 0) // no record found in the line
					continue;

				Record r = null;
				
				try {
					r = new Record(columns, params);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				// System.out.println(r);
				if (null != r)
					lst.add(r);
			}
			recordList = new RecordList(lst);
			return lst.size() > 0;
		}
	}

	@Override
	public Record getNext() {
		Record r = recordList.getNext();
		// System.out.println(r.toString());
		return r;
	}

	public CSVTableReaderStream(String fileName) throws FileNotFoundException {
		super(fileName);
		FileInputStream fis = new FileInputStream(new File(fileName));

		// Construct BufferedReader from InputStreamReader - keep it open
		bufferedReader = new BufferedReader(new InputStreamReader(fis));
		columns = readHeader();
		fetch(FETCH_SIZE);
	}

	@Override
	public void close() throws Exception {
		if (bufferedReader != null)
			bufferedReader.close();

		bufferedReader = null;
		if (recordList != null)
			recordList.close();
		recordList = null;
	}
}
