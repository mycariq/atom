package com.cariq.toolkit.service;

import java.util.List;

import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.model.Tag.Tag_Out;
import com.cariq.toolkit.model.TaggedObject.TaggedObject_In;
import com.cariq.toolkit.model.TaggedObject.TaggedObject_Out;

/**
 * The Interface TagService.
 */
public interface TaggedObjectService {

	/**
	 * Tag the specific object
	 * 
	 * @param params
	 * @return
	 */
	public ResponseWithIdJson tagObject(TaggedObject_In params);
	
	/**
	 * Remove tag from tagged object
	 * 
	 * @param taggedObjectId
	 * @return
	 */
	public ResponseJson untagObject(TaggedObject_In params);
	
	/**
	 * Get all tags assigned to specific object
	 * 
	 * @param fqdnClass
	 * @param objectId
	 * @return
	 */
	public List<Tag_Out> getTagsByFqdnObjectId(String fqdnClass, Long objectId);
	
	/**
	 * Get all objects of same class tagged with a specific tag
	 * 
	 * @param fqdnClass
	 * @param tagId
	 * @return
	 */
	public List<Long> getObjectIDsByFqdnTagname(String fqdnClass, String tagname);
	
	/**
	 * Get unique list of tags used for specific class
	 * 
	 * @param fqdnClass
	 * @return
	 */
	public List<Tag_Out> getTagsByFqdnClass(String fqdnClass);
	
	/**
	 * Get all objects tagged with a specific tag
	 * 
	 * @param tagId
	 * @return
	 */
	public List<TaggedObject_Out> getObjectsByTagname(String tagname);
}