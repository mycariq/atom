package com.atom.www.json;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * The Class LoginRequestJson.
 */
public class LoginRequestJson {

	/** The username. */
	@NotEmpty(message = "Username should not be empty")
	private String username;

	/** The password. */
	@NotEmpty(message = "Password should not be empty")
	private String password;

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}
}
