package com.atom.www.web;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cariq.toolkit.service.TestService;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.Utils;


@RequestMapping("/tests")
@Controller
public class TestController {
	
	/** The test service. */
	@Autowired
	private TestService testService;
	
	/**
	 * Test timeout by making a request that takes long time to return
	 * 
	 * @param responseTime
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/testTimeout/{responseTime}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseJson testTimeOut(@PathVariable("responseTime") Long responseTime, HttpServletResponse response) {
		try (CarIQAPI testTimeOut = new CarIQAPI("testTimeOut")) {
			Thread.sleep(responseTime);
			return new ResponseJson("Returning after " + responseTime + " miliSeconds!");
		} catch (InterruptedException e) {
			Utils.handleException(e);
		}

		return null;
	}


	@RequestMapping(value = "/ping/mysql", method = RequestMethod.GET)
	@ResponseBody
	public GenericJSON mysqlPing() {
		try (CarIQAPI testApi = new CarIQAPI("mysqlPing")) {
			return testService.mysqlPing();
		}
	}
}
