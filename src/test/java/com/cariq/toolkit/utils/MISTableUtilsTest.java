package com.cariq.toolkit.utils;

import java.util.Map;

import org.apache.velocity.app.VelocityEngine;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.atom.www.helper.MISReportHelper;
import com.cariq.toolkit.coreiq.tableUtil.application.mis.MISProcessingContext;
import com.cariq.toolkit.coreiq.tableUtil.application.mis.MISReport;
import com.cariq.toolkit.coreiq.tableUtil.impl.Column;
import com.cariq.toolkit.coreiq.tableUtil.impl.Row;
import com.cariq.toolkit.coreiq.tableUtil.impl.Table;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.CellIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.RowIfc;
import com.cariq.toolkit.utils.jsonProcessor.JSONObjectBuilder;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Configurable
public class MISTableUtilsTest {
	
	@Autowired
	private VelocityEngine velocityEngine;
	
	/**
	 * Test of Basic Table Creation - not MIS - if this works, we can move on to MIS
	 */
	//@Test
	public void BasicTableCreationTest() {
		// Create a table containing 3 rows and 4 columns in it
		Table tbl = new Table("My Table");
		tbl.add(Table.ROW, new Row("sub-header")).add(Table.ROW, new Row()).add(Table.ROW, new Row("special-row-orange")); 
		tbl.add(Table.COLUMN, new Column()).add(Table.COLUMN, new Column()).add(Table.COLUMN, new Column()); 
		tbl.build();
		
		int i = 0;
		for (RowIfc rowIfc : tbl.getRows()) {
			for (CellIfc cellIfc : rowIfc.getCells()) {
				cellIfc.setValue(i++);
			}
		}

		GenericJSON outputJSON = tbl.toGenericJSON();
		System.out.println(Utils.getJSonString(outputJSON));
	}

	/**
	 * Create MIS Table by hand - this will be obsolete once MISTable/Report is loaded from JSON file
	 * @throws Exception
	 */
//	@Test
//	public void BasicMISTableCreationTest() throws Exception {
//		// Create a table containing 3 rows and 4 columns in it
//		MISTable mistbl = new MISTable("My MIS Table", CellValueArbiterDefaultOn.class.getCanonicalName(), "{}");
//
//		// 4 rows, 3 columns - this should be done from json.
//		mistbl.add(Table.ROW, new MISRow("Row Labels", CellValueArbiterDefaultOff.class.getCanonicalName(), "{}"));
//		mistbl.add(Table.ROW, new MISRow("INVALID_CARNOT", KeyValueArbiter.class.getCanonicalName(), "{\"key\":\"status\",\"value\":\"INVALID_CARNOT\"}"));
//		mistbl.add(Table.ROW, new MISRow("Under verification", KeyValueArbiter.class.getCanonicalName(), "{\"key\":\"status\",\"value\":\"Under verification\"}"));
//		mistbl.add(Table.ROW, new MISRow("User Created", KeyValueArbiter.class.getCanonicalName(), "{\"key\":\"status\",\"value\":\"User Created\"}"));
//
//		mistbl.add(Table.COLUMN, new MISColumn("Row Labels", CellValueArbiterDefaultOff.class.getCanonicalName(), "{}"));
//		mistbl.add(Table.COLUMN, new MISColumn("Count of Policy Number", CellValueArbiterDefaultOn.class.getCanonicalName(), "{}"));
//
//		mistbl.build();
//		
//		String keys[] = {"status", "date"};
//		String values[] = {"INVALID_CARNOT", "Under verification", "User Created"};
//		
//		// process bunch of jsons
//		for (int i = 0; i < 100; i++) {
//			GenericJSON tatJSON = GenericJSON.build(keys[RandomHelper.InRange(0, 100) % 2], values[RandomHelper.InRange(0, 100) % 3]);
//			mistbl.process(tatJSON);
//			Thread.sleep(13);
//		}
//
//		GenericJSON outputJSON = mistbl.toGenericJSON();
//		System.out.println(Utils.getJSonString(outputJSON));
//	}
//	

	/**
	 * Create MIS Table by hand - this will be obsolete once MISTable/Report is loaded from JSON file
	 * @throws Exception
	 */
//	@Test
//	public void BasicMISReportCreationTest() throws Exception {
//		MISReport report = new MISReport();
//		
//		for (int tblNo = 0; tblNo < 10; tblNo++) {
//			// Create a table containing 3 rows and 4 columns in it
//			MISTable mistbl = new MISTable("My MIS Table", CellValueArbiterDefaultOn.class.getCanonicalName(), null);
//
//			// 4 rows, 3 columns - this should be done from json.
//			mistbl.add(Table.ROW, new MISRow("Row Labels", CellValueArbiterDefaultOff.class.getCanonicalName(), null));
//			mistbl.add(Table.ROW, new MISRow("INVALID_CARNOT", KeyValueArbiter.class.getCanonicalName(),
//					"{\"key\":\"status\",\"value\":\"INVALID_CARNOT\"}"));
//			mistbl.add(Table.ROW, new MISRow("Under verification", KeyValueArbiter.class.getCanonicalName(),
//					"{\"key\":\"status\",\"value\":\"Under verification\"}"));
//			mistbl.add(Table.ROW, new MISRow("User Created", KeyValueArbiter.class.getCanonicalName(),
//					"{\"key\":\"status\",\"value\":\"User Created\"}"));
//
//			mistbl.add(Table.COLUMN,
//					new MISColumn("Row Labels", CellValueArbiterDefaultOff.class.getCanonicalName(), null));
//			mistbl.add(Table.COLUMN,
//					new MISColumn("Count of Policy Number", CellValueArbiterDefaultOn.class.getCanonicalName(), null));
//
//			mistbl = mistbl.build();
//			report.addTable(mistbl);
//		}
//		
//		GenericJSON outputJSON = report.toGenericJSON();
//		System.out.println(Utils.getJSonString(outputJSON));
//	}
//	


	/**
	 * Test to pupulate MISReport from json
	 */
	//@Test
	public void MISReportPopulationTest() {
		
		JSONObjectBuilder builder = new JSONObjectBuilder("/tmp/TimeLineReportBulilder.json");

		MISReport report = new MISReport();
		System.out.println("UnLoaded Person: " + report);

		builder.register(report.getJSONLoadable());
		
		builder.process();
		
		System.out.println("Loaded Report:\n" + report.toGenericJSON());
	}
	
	/**
	 * Full and final test of loading Report from json file and doing operation on it and generating final table output
	 * @throws Exception
	 */
	//@Test
	public void MISTableRenderingTest() throws Exception {
//		JSONObjectBuilder builder = new JSONObjectBuilder("/home/hrishi/projects/jobs/architecture/mis/test/misreport1.json");
//		JSONObjectBuilder builder = new JSONObjectBuilder("/tmp/WeeklyMISBuilder-Proto.json");
//		JSONObjectBuilder builder = new JSONObjectBuilder("/tmp/WeeklyMISBuilder.json");
		JSONObjectBuilder builder = new JSONObjectBuilder("/tmp/TimeLineReportBulilder.json");

		MISReport report = new MISReport();
		builder.register(report.getJSONLoadable());
		builder.process();
		
		String keys[] = {"status", "date"};
		String values[] = {"INVALID_CARNOT", "Under verification", "User Created"};
		
		// process bunch of jsons - putting random values
		MISProcessingContext context = new MISProcessingContext();
		for (int i = 0; i < 100; i++) {
			GenericJSON tatJSON = GenericJSON.build(keys[RandomHelper.InRange(0, 100) % 2], values[RandomHelper.InRange(0, 100) % 3]);
			report.process(context, tatJSON);
			Thread.sleep(13);
		}

		GenericJSON outputJSON = report.toGenericJSON();
		String filePath = CarIQFileUtils.getTempFile("FinalReport.json");
		CarIQFileUtils.saveToFile(filePath, Utils.getJSonString(outputJSON));
	}
	
	//@Test
	public void testFormulaBasedTableReport() throws Exception {
		JSONObjectBuilder builder = new JSONObjectBuilder("/home/hrishi/projects/jobs/misc/mis/formula/mis_schema.json");

		MISReport report = new MISReport();
		builder.register(report.getJSONLoadable());
		builder.process();
		
		// prepare set of errors
//		String keys[] = {"TestCategory", "TestSubCategory", "TestParameter"};
		String testCategories[] = {"A", "B", "C"};
		String testSubCategories[] = {"X", "Y", "Z"};
		String testParameters[] = {"P", "Q", "R"};
		
		// process bunch of jsons - putting random values
		MISProcessingContext context = new MISProcessingContext();
		for (int i = 0; i < 500; i++) {
			GenericJSON tatJSON = GenericJSON.build("TestCategory", testCategories[RandomHelper.InRange(0, 10) % 3], 
					"TestSubCategory", testSubCategories[RandomHelper.InRange(0, 7) % 3], "TestParameter", testParameters[RandomHelper.InRange(0, 43) % 3]);

			report.process(context, tatJSON);
			Thread.sleep(3);
		}

		GenericJSON outputJSON = report.toGenericJSON();
		String filePath = CarIQFileUtils.getTempFile("FormulaReport.json");
		CarIQFileUtils.saveToFile(filePath, Utils.getJSonString(outputJSON));
	}
	
	@Test
	public void MISTableRenderingWithVMTest() throws Exception {
		JSONObjectBuilder builder = new JSONObjectBuilder("/tmp/SystemPerformanceMatrixReportBulilder.json");

		MISReport report = new MISReport();
		builder.register(report.getJSONLoadable());
		builder.process();
		
		String keys[] = {"status", "date"};
		String values[] = {"INVALID_CARNOT", "Under verification", "User Created"};
		
		// process bunch of jsons - putting random values
		MISProcessingContext context = new MISProcessingContext();
		for (int i = 0; i < 100; i++) {
			GenericJSON tatJSON = GenericJSON.build(keys[RandomHelper.InRange(0, 100) % 2], values[RandomHelper.InRange(0, 100) % 3]);
			report.process(context, tatJSON);
			Thread.sleep(13);
		}

		GenericJSON outputJSON = report.toGenericJSON();
		
		MISReportHelper helper = new MISReportHelper();
		helper.convertJSONToHTMLFile(Utils.getJSonString(outputJSON), "SystemPerformanceMatrixReport.html", null);

	}

}

