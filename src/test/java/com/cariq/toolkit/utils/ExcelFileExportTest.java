package com.cariq.toolkit.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.coreiq.exporter.fileexport.FileExport;
import com.cariq.toolkit.coreiq.exporter.fileexport.FileExportTemplate;
import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.GenericJSON;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Transactional
@Configurable
public class ExcelFileExportTest {

	@Test
	public void excelTest() {
		//FileExport fileExport = FileExportTemplate.getFileExport(ThroughputTrendExporter.EXPORTER, Utils.LAST_DAY,new GenericJSON(), CarIQFileUtils.EXCEL);
		FileExport fileExport = FileExportTemplate.getFileExport("DemoMultiCompositeExporter", "All", new GenericJSON(),
				CarIQFileUtils.EXCEL);
		File outputFile = fileExport.execute();
		System.out.println(outputFile.getAbsolutePath());
	}

	@Test
	public void testExcel() throws IOException {
		File file = new File("/tmp/NewExcelFile.xls");
		HSSFWorkbook workbook;
		if (file.exists())
			workbook = new HSSFWorkbook(new FileInputStream(file));
		else
			workbook = new HSSFWorkbook();

		HSSFSheet sheet = workbook.createSheet("Sheet" + System.currentTimeMillis());
		//style
		HSSFCellStyle style = workbook.createCellStyle();
		style.setFillBackgroundColor(IndexedColors.RED.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		HSSFCellStyle firstCellStyle = workbook.createCellStyle();
		firstCellStyle.setFillBackgroundColor(IndexedColors.BROWN.getIndex());
		firstCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		//set font
		HSSFFont defaultFont = workbook.createFont();
		defaultFont.setFontHeightInPoints((short) 12);
		defaultFont.setFontName("Arial");
		defaultFont.setColor(IndexedColors.WHITE.getIndex());
		defaultFont.setBold(true);
		defaultFont.setItalic(false);

		style.setFont(defaultFont);

		//create header
		HSSFRow rowheader = sheet.createRow((short) 0);
		rowheader.setHeight((short) 400);
		for (int i = 0; i < 10; i++) {
			HSSFCell cell = rowheader.createCell(i);
			cell.setCellValue("Head " + i);
			cell.setCellStyle(style);

		}

		for (int i = 1; i <= 65000; i++) {
			HSSFRow row = sheet.createRow(sheet.getLastRowNum() + 1);
			for (int j = 0; j < 10; j++) {
				HSSFCell cell = row.createCell(j);
				cell.setCellValue("Cell_" + i + "_" + j);
				//				if (j == 0)
				//					cell.setCellStyle(firstCellStyle);
			}
		}

		//		HSSFRow row = sheet.createRow((short) 1);
		//		row.createCell(0).setCellValue("1");
		//		row.createCell(1).setCellValue("Sankumarsingh");
		//		row.createCell(2).setCellValue("India");
		//		row.createCell(3).setCellValue("sankumarsingh@gmail.com");
		FileOutputStream fileOut = new FileOutputStream(file);
		workbook.write(fileOut);
		fileOut.close();
		workbook.close();
		System.out.println("Your excel file has been generated!");
	}

	@Test
	public void test() {
		System.out.println("Test done");
	}

}
