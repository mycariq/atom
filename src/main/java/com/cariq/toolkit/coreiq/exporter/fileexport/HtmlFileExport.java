/**
 * 
 */
package com.cariq.toolkit.coreiq.exporter.fileexport;

import java.io.File;
import java.util.Date;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.cariq.toolkit.coreiq.exporter.CarIQExporter;
import com.cariq.toolkit.utils.CSVHtmlReportModel;
import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

/**
 * @author hrishi
 *
 */
@Configurable
public class HtmlFileExport extends FileExportTemplate {
	@Autowired
	private VelocityEngine velocityEngine;

	/**
	 * @param exporter
	 * @param queryType
	 * @param inputJSON
	 * @param fileFormat
	 */
	public HtmlFileExport(CarIQExporter exporter, String queryType, GenericJSON inputJSON) {
		super(exporter, queryType, inputJSON, CarIQFileUtils.HTML);
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.exporter.fileexport.FileExportTemplate#doExecute()
	 */
	@Override
	protected File doExecute() {
		FileExport csvExport = new CSVFileExport(exporter, queryType, inputJSON);
		File csvfile = csvExport.execute();

		String htmlFile = CarIQFileUtils
				.getTempFile(exporter.getName() + "-" + queryType + "-" + Utils.createDateId(true) + ".html");

		// make generic json map from the csv
		try {
			String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine,
					CSVHtmlReportModel.VelocityTemplate, "UTF-8",
					new CSVHtmlReportModel(exporter.getName(),
							exporter.getDescription() + ", report Generated on the basis of QueryType: " + queryType,
							new Date(), csvfile.getAbsolutePath(), exporter.getHtmlRenderer()));

			// save to html file
			CarIQFileUtils.saveToFile(htmlFile, text);

			return new File(htmlFile);
		} catch (Exception e) {
			Utils.handleException(e);
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.exporter.fileexport.FileExport#getMimeType()
	 */
	@Override
	public String getMimeType() {
		return "text/html";
	}

}
