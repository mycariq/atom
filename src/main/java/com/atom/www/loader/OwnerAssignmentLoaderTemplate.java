package com.atom.www.loader;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.atom.www.helper.AppointmentStatus;
import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderContext;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderTemplate;
import com.cariq.toolkit.model.Appointment;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.DomainAccess;
import com.cariq.toolkit.model.ProxyObject;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.service.TimelineService;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * Owner assignment loader template
 * 
 * @author sagar
 *
 */
@Configurable
public abstract class OwnerAssignmentLoaderTemplate extends CarIQLoaderTemplate {
	
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("OwnerAssignmentLoaderTemplate");
	
	protected static final String KEY_POLICY_NO = "policyNumber";
	
	private static final String PROXY_TYPE = "InsurancePolicy";
	
	private static final String TIMELINE_TYPE = "system";
	
	@Autowired
	private TimelineService timelineService;

	public OwnerAssignmentLoaderTemplate(String name, String description) {
		super(name, description);
	}
	
	/**
	 * Iterate owner output jsons and assign owner.
	 * 
	 * @param outputJsons
	 * @param owners
	 * @param domain
	 */
	protected final void assignOwners(List<GenericJSON> outputJsons, List<String> owners, Domain domain, boolean forceUpdateOwner,
			CarIQLoaderContext context) {
		
		//treat below list as queue to hold current operating elements at first and last
		LinkedList<String> queue = new LinkedList<String>(owners);
		
		String signatures = Utils.getCommaSeperatedValues(outputJsons, KEY_POLICY_NO);
		List<ProxyObject> proxies = ProxyObject.getByDomainSignaturesObjectType(domain,
				Utils.getCommaSeperatedValuesForQuery(signatures), PROXY_TYPE);
		LinkedHashMap<String, ProxyObject> identifiedProxies = ProxyObject.getIdentifiedMapFromList(proxies);
		
		for(GenericJSON policyInformation : outputJsons) {
			
			try (ProfilePoint singleOwnerAssignment = ProfilePoint
					.profileAction("ProfAction_singleOwnerAssignment")) {
			
				String ownerAtHead = null;
			
				//Case 1: If proxy does not exist then create proxy and assign owner to proxy while creating proxy.
				//SubCase : If proxy exists and owner is null for proxy then handle same in Case 1.
				
				String signature = Utils.getValue(policyInformation, KEY_POLICY_NO);
				logger.debug("Trying to update owner of proxy with signature: " + signature);
				
				//get proxy object
				ProxyObject proxy = identifiedProxies.get(signature);
				
				if(proxy == null) {
					logger.debug("Proxy not found with signature " + signature + ", Creating new proxy ");
					//create new proxy
					ownerAtHead = queue.removeFirst();
					ProxyObject proxyObject = ProxyObject.createOrGet(domain, null, PROXY_TYPE, signature, User.getByUsername(ownerAtHead), null);
					String msg = "Owner '" + ownerAtHead + "' assigned to proxy with signature " + signature;
					context.addMessage(msg);
					logger.debug(msg);
					writeOnTimeline(policyInformation, proxyObject, ownerAtHead);
				} else if(proxy.getOwner() == null || forceUpdateOwner) {
					//update just owner(Internally createOrGet updates owner if valid owner provided)
					ownerAtHead = queue.removeFirst();
					ProxyObject proxyObject = ProxyObject.createOrGet(domain, null, PROXY_TYPE, signature, User.getByUsername(ownerAtHead), null);
					ProxyObject.update(domain, null, PROXY_TYPE, signature, User.getByUsername(ownerAtHead), null, null);
					String msg = "Owner '" + ownerAtHead + "' assigned to proxy with signature " + signature;
					context.addMessage(msg);
					logger.debug(msg);
					writeOnTimeline(policyInformation, proxyObject, ownerAtHead);
				} 
				//proxy exist and owner also exists, then, 
				//we are not doing anything with this proxy (Only if force update not enabled)
				
				//Mark appointment of this case as outdated if necessary.
				checkAndMarkAppointmetAsOutdated(policyInformation, domain);
				
				//add head at tail
				if(ownerAtHead != null) {
					logger.debug("Adding owner" + ownerAtHead + " at last");
					queue.addLast(ownerAtHead);
				}
			}
		}
	}
	
	/**
	 * Check and mark appointment as outdated. 
	 * 
	 * @param json
	 * @param domain
	 */
	private void checkAndMarkAppointmetAsOutdated(GenericJSON json, Domain domain) {
		logger.debug("checkAndMarkAppointmetAsOutdated:");
		String stageOfObject = Utils.getValue(json, "State");
		String signature = Utils.getValue(json, StagingAndPolicyHelper.KEY_POLICY_NO);
		
		//Get proxy of this case.
		ProxyObject proxy = ProxyObject.getByDomainSignatureObjectType(domain, signature,
				StagingAndPolicyHelper.proxyType);
		
		//Get all not completed appointments for this case.
		List<Appointment> appointments = Appointment.getByProxyAndNotCompleted(proxy);
		
		logger.debug("policyNumber " + signature + " has " + appointments.size() + " appointments.");
		
		//Mark outdated if it is outdated.
		for(Appointment appointment : appointments) {
			if(appointment.isOutdated(stageOfObject)) {
				
				String reasonOfOudated = appointment.getOudatedReason(stageOfObject);
				
				appointment.setStatus(AppointmentStatus.OUTDATED.name());
				appointment.setModifiedOn(new Date());
				appointment.persist();
				
				//Add message on timeline why this action is being taken.
				timelineService.addSimpleTimeline(proxy.toJSON(), "Appointment marked as OUTDATED due to: "
						+ reasonOfOudated,
						TIMELINE_TYPE,
						stageOfObject, null, Utils.getCurrentUserName());
			}
		}
	}
	
	/**
	 * Write a message on timeline
	 * 
	 * @param policyInformation
	 * @param domain 
	 * @param ownerAtHead
	 */
	private void writeOnTimeline(GenericJSON policyInformation, ProxyObject proxyObject, String ownerAtHead) {
		String message = "Task assigned to @" + ownerAtHead;
		String stage = Utils.getValue(policyInformation, "State"); 
		String username = Utils.getCurrentUserName();
		timelineService.addSimpleTimeline(proxyObject.toJSON(), message, TIMELINE_TYPE, stage, null, username);
	}

	/**
	 * Get valid domains from input owners
	 * 
	 * @param inputOwners
	 * @return
	 */
	protected final List<String> getValidOwners(String inputOwners, Domain domain) {
		
		//remove white spaces from string
		inputOwners = Utils.removeWhiteSpaces(inputOwners);
		//split owners on comma.
		String[] owners = inputOwners.split(",");

		//Find owners whom given domain is accecible and return only those as valid.
		List<String> ownersList = new ArrayList<>();
		for(String owner : owners) {
			if(DomainAccess.getObjectByDomainUSer(domain, User.getByUsername(owner)) != null)
				ownersList.add(owner);				
		}
		//return valid domains.
		return ownersList;
	}
}
