//package com.cariq.toolkit.model.async.lrt;
//
//import java.util.Date;
//import java.util.UUID;
//
//import com.cariq.www.utils.GenericJSON;
//import com.cariq.www.utils.GenericJsonable;
//import com.cariq.www.utils.Utils;
//import com.cariq.www.workitem.model.WorkItem;
//import com.cariq.www.workitem.model.WorkItemUpdater;
//import com.cariq.www.workitem.worker.WorkItemDelegateTemplate;
//
///**
// * LRTManager is the singleton responsible for managing Long Running Tasks
// * (/Transactions) (Typically API - but not only API) One can give Task to
// * LRTManager and then check back for completion. Surprizing how come we never
// * needed this!
// * 
// * @author Hrishi
// *
// */
//public abstract class LRT extends WorkItemDelegateTemplate  {	
//	public static final String LRTToken = "lrtToken";
//	
//	@Override
//	protected void doExecute(WorkItem workItem, WorkItemUpdater updater) throws Exception {
//		updater.setOutputJson(executeLRT(workItem.fetchInputJson()).toString());		
//	}
//
//	abstract protected GenericJSON executeLRT(GenericJSON fetchInputJson);
//}
