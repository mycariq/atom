package com.cariq.toolkit.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.StageStatistics.StageStats_In;
import com.cariq.toolkit.model.StageStatistics.StageStats_Out;
import com.cariq.toolkit.model.User.User_Out;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;
import com.cariq.toolkit.utils.Utils;

@Entity
@Configurable
public class StageStatistics extends PersistentObject<StageStatistics> implements JSONable<StageStats_In, StageStats_Out, IdJSON> { 

	// SECTION 1 -Code for Persistence
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@Version
	Integer version;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new ProxyObject().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null)
			this.entityManager = entityManager();
		return entityManager;
	}

	/**
	 * @param id
	 * @return
	 */
	protected static StageStatistics find(Long id) {
		return entityManager().find(StageStatistics.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public StageStatistics findById(Long id) {
		return find(id);
	}
	
	
	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------
	
	/* Signature of proxy **/
	@NotNull(message = "Signature cannot be null")
	@Column(unique = true)
	private String signature;
	
	/** Owner of proxy  **/
	@ManyToOne
	private User owner;
	
	/** Domain of proxy  **/
	@NotNull(message = "Domain cannot be null")
	@ManyToOne
	private Domain domain;
	
	/** stage of policy **/
	private String stage;
	
	/** state of policy **/
	private String state;
	
	/** Subscription state of policy **/
	private String subscriptionState;
	
	/** Next Action tobe done on policy **/
	private String nextAction;
	
	/** Ref policy number of this policy **/
	private String refPolicyNumber;
	
	/** Staging entry date **/
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date entryDate;
	
	/** Policy verified on date **/
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date verifiedDate;
	
	/** Date on user requested device **/
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date deviceRequestedDate;
	
	/** Date on device shipped to user **/
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date deviceShippedOnDate;
	
	/** Device delivered date **/
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date deviceDeliveredOnDate;
	
	/** User onboarded device in app **/
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date deviceOnboardedDate;
	
	/** User connected device to car **/
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date carConnectedDate;
	
	/** User became Not interested **/
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date niDate;
	
	/** User became Not contactable **/
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date ncDate;
	
	/** Policy deactivated on **/
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date deactivatedOn;
	
	/** Policy renewed on **/
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date renewedOn;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	/**
	 * Default constructor
	 */
	public StageStatistics() {
		super();
	}
	
	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getEntryDate() {
		return entryDate;
	}

	public void setEntryDate(Date entryDate) {
		this.entryDate = entryDate;
	}

	public Date getVerifiedDate() {
		return verifiedDate;
	}

	public void setVerifiedDate(Date verifiedDate) {
		this.verifiedDate = verifiedDate;
	}

	public Date getDeviceRequestedDate() {
		return deviceRequestedDate;
	}

	public void setDeviceRequestedDate(Date deviceRequestedDate) {
		this.deviceRequestedDate = deviceRequestedDate;
	}

	public Date getDeviceShippedOnDate() {
		return deviceShippedOnDate;
	}

	public void setDeviceShippedOnDate(Date deviceShippedOnDate) {
		this.deviceShippedOnDate = deviceShippedOnDate;
	}

	public Date getDeviceDeliveredOnDate() {
		return deviceDeliveredOnDate;
	}

	public void setDeviceDeliveredOnDate(Date deviceDeliveredOnDate) {
		this.deviceDeliveredOnDate = deviceDeliveredOnDate;
	}

	public Date getDeviceOnboardedDate() {
		return deviceOnboardedDate;
	}

	public void setDeviceOnboardedDate(Date deviceOnboardedDate) {
		this.deviceOnboardedDate = deviceOnboardedDate;
	}

	public Date getCarConnectedDate() {
		return carConnectedDate;
	}

	public void setCarConnectedDate(Date carConnectedDate) {
		this.carConnectedDate = carConnectedDate;
	}

	public Date getNiDate() {
		return niDate;
	}

	public void setNiDate(Date niDate) {
		this.niDate = niDate;
	}

	public Date getNcDate() {
		return ncDate;
	}

	public void setNcDate(Date ncDate) {
		this.ncDate = ncDate;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public String getSubscriptionState() {
		return subscriptionState;
	}

	public void setSubscriptionState(String subscriptionState) {
		this.subscriptionState = subscriptionState;
	}

	public String getNextAction() {
		return nextAction;
	}

	public void setNextAction(String nextAction) {
		this.nextAction = nextAction;
	}

	public String getRefPolicyNumber() {
		return refPolicyNumber;
	}

	public void setRefPolicyNumber(String refPolicyNumber) {
		this.refPolicyNumber = refPolicyNumber;
	}

	public Date getDeactivatedOn() {
		return deactivatedOn;
	}

	public void setDeactivatedOn(Date deactivatedOn) {
		this.deactivatedOn = deactivatedOn;
	}

	public Date getRenewedOn() {
		return renewedOn;
	}

	public void setRenewedOn(Date renewedOn) {
		this.renewedOn = renewedOn;
	}

	@Override
	public String toString() {
		String username = owner != null ? owner.getUsername() : null;
		String domainName = domain != null ? domain.getName() : null;
		return "StageStats [id=" + id + ", version=" + version + ", signature=" + signature + ", owner=" + username
				+ ", stage=" + stage + ", state=" + state + ", subscriptionState=" + subscriptionState 
				+ "nextAction=" + nextAction + ",refPolicyNumber " + refPolicyNumber + ", entryDate=" 
				+ entryDate + ", verifiedDate="	+ verifiedDate + ", deviceRequestedDate="
				+ deviceRequestedDate + ", deviceShippedOnDate=" + deviceShippedOnDate + ", deviceDeliveredOnDate="
				+ deviceDeliveredOnDate + ", deviceOnboardedDate=" + deviceOnboardedDate + ", carConnectedDate="
				+ carConnectedDate + ", niDate=" + niDate + ", ncDate=" + ncDate + ", createdOn=" 
				+ createdOn + ", deactivatedOn= " + deactivatedOn + ", renewedOn=" + renewedOn
				+ ", modifiedOn=" + modifiedOn + ", domain=" + domainName +"]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((carConnectedDate == null) ? 0 : carConnectedDate.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((deviceDeliveredOnDate == null) ? 0 : deviceDeliveredOnDate.hashCode());
		result = prime * result + ((deviceOnboardedDate == null) ? 0 : deviceOnboardedDate.hashCode());
		result = prime * result + ((deviceRequestedDate == null) ? 0 : deviceRequestedDate.hashCode());
		result = prime * result + ((deviceShippedOnDate == null) ? 0 : deviceShippedOnDate.hashCode());
		result = prime * result + ((entryDate == null) ? 0 : entryDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((ncDate == null) ? 0 : ncDate.hashCode());
		result = prime * result + ((niDate == null) ? 0 : niDate.hashCode());
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		result = prime * result + ((signature == null) ? 0 : signature.hashCode());
		result = prime * result + ((stage == null) ? 0 : stage.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((subscriptionState == null) ? 0 : subscriptionState.hashCode());
		result = prime * result + ((nextAction == null) ? 0 : nextAction.hashCode());
		result = prime * result + ((refPolicyNumber == null) ? 0 : refPolicyNumber.hashCode());
		result = prime * result + ((deactivatedOn == null) ? 0 : deactivatedOn.hashCode());
		result = prime * result + ((renewedOn == null) ? 0 : renewedOn.hashCode());
		result = prime * result + ((verifiedDate == null) ? 0 : verifiedDate.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StageStatistics other = (StageStatistics) obj;
		if (carConnectedDate == null) {
			if (other.carConnectedDate != null)
				return false;
		} else if (!carConnectedDate.equals(other.carConnectedDate))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (deviceDeliveredOnDate == null) {
			if (other.deviceDeliveredOnDate != null)
				return false;
		} else if (!deviceDeliveredOnDate.equals(other.deviceDeliveredOnDate))
			return false;
		if (deviceOnboardedDate == null) {
			if (other.deviceOnboardedDate != null)
				return false;
		} else if (!deviceOnboardedDate.equals(other.deviceOnboardedDate))
			return false;
		if (deviceRequestedDate == null) {
			if (other.deviceRequestedDate != null)
				return false;
		} else if (!deviceRequestedDate.equals(other.deviceRequestedDate))
			return false;
		if (deviceShippedOnDate == null) {
			if (other.deviceShippedOnDate != null)
				return false;
		} else if (!deviceShippedOnDate.equals(other.deviceShippedOnDate))
			return false;
		if (entryDate == null) {
			if (other.entryDate != null)
				return false;
		} else if (!entryDate.equals(other.entryDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		if (ncDate == null) {
			if (other.ncDate != null)
				return false;
		} else if (!ncDate.equals(other.ncDate))
			return false;
		if (niDate == null) {
			if (other.niDate != null)
				return false;
		} else if (!niDate.equals(other.niDate))
			return false;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		if (signature == null) {
			if (other.signature != null)
				return false;
		} else if (!signature.equals(other.signature))
			return false;
		if (stage == null) {
			if (other.stage != null)
				return false;
		} else if (!stage.equals(other.stage))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (subscriptionState == null) {
			if (other.subscriptionState != null)
				return false;
		} else if (!subscriptionState.equals(other.subscriptionState))
			return false;
		if (nextAction == null) {
			if (other.nextAction != null)
				return false;
		} else if (!nextAction.equals(other.nextAction))
			return false;
		if (refPolicyNumber == null) {
			if (other.refPolicyNumber != null)
				return false;
		} else if (!refPolicyNumber.equals(other.refPolicyNumber))
			return false;
		if (deactivatedOn == null) {
			if (other.deactivatedOn != null)
				return false;
		} else if (!deactivatedOn.equals(other.deactivatedOn))
			return false;
		if (renewedOn == null) {
			if (other.renewedOn != null)
				return false;
		} else if (!renewedOn.equals(other.renewedOn))
			return false;
		if (verifiedDate == null) {
			if (other.verifiedDate != null)
				return false;
		} else if (!verifiedDate.equals(other.verifiedDate))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}


	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API
	// interaction
	// In Class (for construction)
	public static class StageStats_In {
		private String signature;
		private String username;
		private String domainName;
		private String stage;
		private String state;
		private String subscriptionState;
		private String refPolicyNumber;
		private String nextAction;
		private String entryDate;
		private String verifiedDate;
		private String deviceRequestedDate;
		private String deviceShippedDate;
		private String deviceDeliveredDate;
		private String deviceOnboardedDate;
		private String carConnectedDate;
		private String niDate;
		private String ncDate;
		private String deactivatedOn; 
		private String renewedOn;
		
		/**
		 * Constructor
		 */
		public StageStats_In() {
			super();
		}
		
		public StageStats_In(String signature, String domainName, String username,String stage,
				String state, String subscriptionState, String nextAction, String refPolicyNumber, 
				String entryDate, String verifiedDate, String deviceRequestedDate,
				String deviceShippedOnDate, String deviceDeliveredDate, String deviceOnboardedOn,
				String carConnectedDate, String niDate, String ncDate, String deactivatedOn, String renewedOn) {
			this.signature = signature;
			this.domainName = domainName;
			this.username = username;
			this.stage = stage;
			this.state = state;
			this.subscriptionState = subscriptionState;
			this.nextAction = nextAction;
			this.refPolicyNumber = refPolicyNumber;
			this.entryDate = entryDate;
			this.verifiedDate = verifiedDate;
			this.deviceRequestedDate = deviceRequestedDate;
			this.deviceShippedDate = deviceShippedOnDate;
			this.deviceDeliveredDate = deviceDeliveredDate;
			this.deviceOnboardedDate = deviceOnboardedOn;
			this.carConnectedDate = carConnectedDate;
			this.niDate = niDate;
			this.ncDate = ncDate;
			this.deactivatedOn = deactivatedOn;
			this.renewedOn = renewedOn;
		}

		public String getSignature() {
			return signature;
		}

		public void setSignature(String signature) {
			this.signature = signature;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getStage() {
			return stage;
		}

		public void setStage(String stage) {
			this.stage = stage;
		}

		public String getState() {
			return state;
		}

		public void setState(String state) {
			this.state = state;
		}

		public String getEntryDate() {
			return entryDate;
		}

		public void setEntryDate(String entryDate) {
			this.entryDate = entryDate;
		}

		public String getVerifiedDate() {
			return verifiedDate;
		}

		public void setVerifiedDate(String verifiedDate) {
			this.verifiedDate = verifiedDate;
		}

		public String getDeviceRequestedDate() {
			return deviceRequestedDate;
		}

		public void setDeviceRequestedDate(String deviceRequestedDate) {
			this.deviceRequestedDate = deviceRequestedDate;
		}

		public String getDeviceShippedDate() {
			return deviceShippedDate;
		}

		public void setDeviceShippedDate(String deviceShippedDate) {
			this.deviceShippedDate = deviceShippedDate;
		}

		public String getDeviceDeliveredDate() {
			return deviceDeliveredDate;
		}

		public void setDeviceDeliveredDate(String deviceDeliveredDate) {
			this.deviceDeliveredDate = deviceDeliveredDate;
		}

		public String getDeviceOnboardedDate() {
			return deviceOnboardedDate;
		}

		public void setDeviceOnboardedDate(String deviceOnboardedDate) {
			this.deviceOnboardedDate = deviceOnboardedDate;
		}

		public String getCarConnectedDate() {
			return carConnectedDate;
		}

		public void setCarConnectedDate(String carConnectedDate) {
			this.carConnectedDate = carConnectedDate;
		}

		public String getNiDate() {
			return niDate;
		}

		public void setNiDate(String niDate) {
			this.niDate = niDate;
		}

		public String getNcDate() {
			return ncDate;
		}

		public void setNcDate(String ncDate) {
			this.ncDate = ncDate;
		}

		public String getDomainName() {
			return domainName;
		}

		public void setDomainName(String domainName) {
			this.domainName = domainName;
		}

		public String getSubscriptionState() {
			return subscriptionState;
		}

		public void setSubscriptionState(String subscriptionState) {
			this.subscriptionState = subscriptionState;
		}

		public String getRefPolicyNumber() {
			return refPolicyNumber;
		}

		public void setRefPolicyNumber(String refPolicyNumber) {
			this.refPolicyNumber = refPolicyNumber;
		}

		public String getNextAction() {
			return nextAction;
		}

		public void setNextAction(String nextAction) {
			this.nextAction = nextAction;
		}

		public String getDeactivatedOn() {
			return deactivatedOn;
		}

		public void setDeactivatedOn(String deactivatedOn) {
			this.deactivatedOn = deactivatedOn;
		}

		public String getRenewedOn() {
			return renewedOn;
		}

		public void setRenewedOn(String renewedOn) {
			this.renewedOn = renewedOn;
		}
	}
	
	// Out class for output
	public static class StageStats_Out extends StageStats_In {
		
		private Long id;
		private User_Out ownerOut;
		
		public StageStats_Out() {
			super();
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public User_Out getOwnerOut() {
			return ownerOut;
		}

		public void setOwnerOut(User_Out ownerOut) {
			this.ownerOut = ownerOut;
		}
	}

	@Override
	public StageStats_Out toJSON() {
		StageStats_Out out = new StageStats_Out();
		out.setSignature(signature);
		out.setOwnerOut(owner == null ? null : owner.toJSON());
		out.setStage(stage);
		out.setState(state);
		out.setSubscriptionState(subscriptionState);
		out.setNextAction(nextAction);
		out.setRefPolicyNumber(refPolicyNumber);
		out.setDomainName(domain.getName());
		
		out.setEntryDate(Utils.getDateTimeString(entryDate, null));
		out.setVerifiedDate(Utils.getDateTimeString(verifiedDate, null));
		out.setDeviceRequestedDate(Utils.getDateTimeString(deviceRequestedDate, null));
		out.setDeviceShippedDate(Utils.getDateTimeString(deviceShippedOnDate, null));
		out.setDeviceDeliveredDate(Utils.getDateTimeString(deviceDeliveredOnDate, null));
		out.setDeviceOnboardedDate(Utils.getDateTimeString(deviceOnboardedDate, null));
		out.setCarConnectedDate(Utils.getDateTimeString(carConnectedDate, null));
		
		out.setNiDate(Utils.getDateTimeString(niDate, null));
		out.setNcDate(Utils.getDateTimeString(ncDate, null));
		
		out.setDeactivatedOn(Utils.getDateTimeString(deactivatedOn, null));
		out.setRenewedOn(Utils.getDateTimeString(renewedOn, null));
		
		return out;
	}

	@Override
	public void fromJSON(StageStats_In json) {
		this.signature = json.getSignature();
		
		this.owner = User.getByUsername(json.getUsername());
		this.domain = Domain.getByName(json.getDomainName());
		this.stage = json.getStage();
		this.state = json.getState();
		this.subscriptionState = json.getSubscriptionState();
		this.nextAction = json.getNextAction();
		this.refPolicyNumber = json.getRefPolicyNumber();
		
		this.entryDate = json.getEntryDate() != null ? Utils.getDate(json.getEntryDate()) : null;
		this.verifiedDate = json.getVerifiedDate() != null ? Utils.getDate(json.getVerifiedDate()) : null;
		this.deviceRequestedDate = json.getDeviceRequestedDate() != null ? Utils.getDate(json.getDeviceRequestedDate()) : null;
		this.deviceShippedOnDate = json.getDeviceShippedDate() != null ? Utils.getDate(json.getDeviceShippedDate()) : null;
		this.deviceDeliveredOnDate = json.getDeviceDeliveredDate() != null ? Utils.getDate(json.getDeviceDeliveredDate()) : null;
		this.deviceOnboardedDate = json.getDeviceOnboardedDate() != null ? Utils.getDate(json.getDeviceOnboardedDate()) : null;
		this.carConnectedDate = json.getCarConnectedDate() != null ? Utils.getDate(json.getCarConnectedDate()) : null;
		
		this.niDate = json.getNiDate() != null ? Utils.getDate(json.getNiDate()) : null;
		this.ncDate = json.getNcDate() != null ? Utils.getDate(json.getNcDate()) : null;
		this.deactivatedOn = json.getDeactivatedOn() != null ? Utils.getDate(json.getDeactivatedOn()) : null;
		this.renewedOn = json.getRenewedOn() != null ? Utils.getDate(json.getRenewedOn()) : null;
		
		this.createdOn = new Date();
		this.modifiedOn = new Date();
	}
	
	/**
	 * Update by in json 
	 * 
	 * @param inJson
	 */
	public StageStatistics update(StageStats_In inJson) {
		if(inJson.getUsername() != null) {
			this.owner = User.getByUsername(inJson.getUsername());
		}		
		if(inJson.getStage() != null)
			this.stage = inJson.getStage();
		if(inJson.getState() != null)
			this.state = inJson.getState();
		if(inJson.getSubscriptionState() != null) 
			this.subscriptionState = inJson.getSubscriptionState();
		if(inJson.getNextAction() != null) 
			this.nextAction = inJson.getNextAction();
		if(inJson.getRefPolicyNumber() != null) 
			this.refPolicyNumber = inJson.getRefPolicyNumber();
		if(inJson.getEntryDate() != null)
			this.entryDate = Utils.getDate(inJson.getEntryDate());
		if(inJson.getVerifiedDate() != null) 
			this.verifiedDate = Utils.getDate(inJson.getVerifiedDate());
		if (inJson.getDeviceRequestedDate() != null)
			this.deviceRequestedDate = Utils.getDate(inJson.getDeviceRequestedDate());
		if (inJson.getDeviceShippedDate() != null)
			this.deviceShippedOnDate = Utils.getDate(inJson.getDeviceShippedDate());
		if (inJson.getDeviceDeliveredDate() != null)
			this.deviceDeliveredOnDate = Utils.getDate(inJson.getDeviceDeliveredDate());
		if (inJson.getDeviceOnboardedDate() != null)
			this.deviceOnboardedDate = Utils.getDate(inJson.getDeviceOnboardedDate());
		if (inJson.getCarConnectedDate() != null)
			this.carConnectedDate = Utils.getDate(inJson.getCarConnectedDate());

		if (inJson.getNiDate() != null)
			this.niDate = Utils.getDate(inJson.getNiDate());
		if (inJson.getNcDate() != null)
			this.ncDate = Utils.getDate(inJson.getNcDate());
		
		if(inJson.getDeactivatedOn() != null)
			this.deactivatedOn = Utils.getDate(inJson.getDeactivatedOn());
		if(inJson.getRenewedOn() != null)
			this.renewedOn = Utils.getDate(inJson.getRenewedOn());

		this.modifiedOn = new Date();

		return merge();
	}
	
	
	/**
	 * Get {@link StageStatistics} by signature
	 * 
	 * @param signature		Input signature
	 * @return				
	 */
	public static StageStatistics getBySignature(String signature) {
		CarIQSimpleQuery<StageStatistics> getBySignatureQuery = new CarIQSimpleQuery<StageStatistics>
									("getBySignatureQuery", StageStatistics.entityManager(), StageStatistics.class);
		getBySignatureQuery.addCondition("signature", "=", signature);
		return getBySignatureQuery.getSingleResult();
	}

	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}
	
	/***
	 * Create, Update or get StageStatistics
	 * 
	 * @param inJson	Input json.
	 * @return
	 */
	public static StageStatistics createOrUpdate(StageStats_In inJson) {
		//find whether stageStistics exists by signature
		StageStatistics stageStatistics = StageStatistics.getBySignature(inJson.getSignature());
		
		//if exists then update and return
		if(stageStatistics != null) {
			return stageStatistics.update(inJson);
		}
		
		//else create a new one
		StageStatistics statistics = new StageStatistics();
		statistics.fromJSON(inJson);
		statistics.persist();
		
		//and return
		return statistics; 
	}
	
	/**
	 * Get signatures from list of stats
	 * 
	 * @param stats
	 * @return
	 */
	public static List<String> getSignatures(List<StageStatistics> stats) {
		List<String> signatures = new ArrayList<>();
		if(Utils.isNullOrEmpty(stats))
			return signatures;
		
		for(StageStatistics stat : stats) {
			signatures.add(stat.getSignature());
		}
		return signatures;
	}
}
