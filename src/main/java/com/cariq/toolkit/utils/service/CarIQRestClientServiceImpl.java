package com.cariq.toolkit.utils.service;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * The Class W4IQRestClientServiceImpl.
 */
@Service
public class CarIQRestClientServiceImpl implements CarIQRestClientService {

	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("CarIQRestClientServiceImpl");
	
	/** The Constant MESSAGE_KEY. */
	private static final String MESSAGE_KEY = "messages";
	
	/**
	 * Instantiates a new w 4 IQ rest client service impl.
	 */
	public CarIQRestClientServiceImpl() {
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.utils.service.CarIQRestClientService#post(java.lang.Object, java.lang.String, java.lang.String, java.lang.Class)
	 */
	@Override
	public <R, I> R post(I inputJson, String url, String basicAuth, Class<R> responseClass) {
        try (ProfilePoint _post = ProfilePoint.profileAction("CarIQRestClientServiceImpl_RestClientPost")) {
            ResponseEntity<R> response = null;

            CarIQLogger innerLogger = logger.getLogger("post-" + System.currentTimeMillis());
            // set basic auth headers
            HttpHeaders requestHeaders = Utils.setHeaders(basicAuth);
            innerLogger.debug("Inside Post API call with url: " + url + " and inputJson = " + inputJson);
            RestTemplate restTemplate = new RestTemplate(Utils.getRequestFactory());
            try {
                response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<I>(inputJson, requestHeaders),
                        responseClass);
                innerLogger.debug("Response >>: " + response.getBody());
            } catch (HttpClientErrorException e) {
                innerLogger.debug("Getting HttpClientException >>: " + e);
                handleRestClientException(e);
            } catch (Exception e) {
                innerLogger.debug("Getting Exception >>: " + e);
                Utils.handleException(e.getMessage());
            }
            return response.getBody();
        }
    }

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.utils.service.CarIQRestClientService#get(java.lang.String, java.lang.String, java.lang.Class)
	 */
	@Override
	public <R> R get(String url, String basicAuth, Class<R> responseClass) {
        try (ProfilePoint _get = ProfilePoint.profileAction("CarIQRestClientServiceImpl_RestClientGet")) {
            ResponseEntity<R> response = null;

            CarIQLogger innerLogger = logger.getLogger("get-" + System.currentTimeMillis());
            // set basic auth headers
            HttpHeaders requestHeaders = Utils.setHeaders(basicAuth);
            innerLogger.debug("Inside Get API call with url: " + url);
            RestTemplate restTemplate = new RestTemplate(Utils.getRequestFactory());
            try {
                response = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<String>(requestHeaders),
                        responseClass);
                innerLogger.debug("Response >>: " + response.getBody());
            } catch (HttpClientErrorException e) {
                innerLogger.debug("Getting HttpClientException >>: " + e);
                handleRestClientException(e);
            } catch (Exception e) {
                innerLogger.debug("Getting Exception >>: " + e);
                Utils.handleException(e.getMessage());
            }
            return response.getBody();
        }
    }

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.utils.service.CarIQRestClientService#delete(java.lang.String, java.lang.String, java.lang.Class)
	 */
	@Override
	public <R> R delete(String url, String basicAuth, Class<R> responseClass) {
        try (ProfilePoint _delete = ProfilePoint.profileAction("CarIQRestClientServiceImpl_RestClientDelete")) {
            ResponseEntity<R> response = null;

            CarIQLogger innerLogger = logger.getLogger("delete-" + System.currentTimeMillis());
            // set basic auth headers
            HttpHeaders requestHeaders = Utils.setHeaders(basicAuth);
            innerLogger.debug("Inside Delete API call with url: " + url);
            RestTemplate restTemplate = new RestTemplate(Utils.getRequestFactory());
            try {
                response = restTemplate.exchange(url, HttpMethod.DELETE, new HttpEntity<String>(requestHeaders),
                        responseClass);
                innerLogger.debug("Response >>: " + response.getBody());
            } catch (HttpClientErrorException e) {
                innerLogger.debug("Getting HttpClientException >>: " + e);
                handleRestClientException(e);
            } catch (Exception e) {
                innerLogger.debug("Getting Exception >>: " + e);
                Utils.handleException(e.getMessage());
            }
            return response.getBody();
        }
    }

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.utils.service.CarIQRestClientService#put(java.lang.Object, java.lang.String, java.lang.String, java.lang.Class)
	 */
	@Override
	public <R, I> R put(I inputJson, String url, String basicAuth, Class<R> responseClass) {
        try (ProfilePoint _put = ProfilePoint.profileAction("CarIQRestClientServiceImpl_RestClientPut")) {
            ResponseEntity<R> response = null;

            CarIQLogger innerLogger = logger.getLogger("put-" + System.currentTimeMillis());
            // set basic auth headers
            HttpHeaders requestHeaders = Utils.setHeaders(basicAuth);
            innerLogger.debug("Inside Put API call with url: " + url + " and inputJson = " + inputJson);
            RestTemplate restTemplate = new RestTemplate(Utils.getRequestFactory());
            try {
                response = restTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<I>(inputJson, requestHeaders),
                        responseClass);
                innerLogger.debug("Response >>: " + response.getBody());
            } catch (HttpClientErrorException e) {
                innerLogger.debug("Getting HttpClientException >>: " + e);
                handleRestClientException(e);
            } catch (Exception e) {
                innerLogger.debug("Getting Exception >>: " + e);
                Utils.handleException(e.getMessage());
            }
            return response.getBody();
        }
    }
	
	/**
	 * Handle rest client exception.
	 *
	 * @param exception the exception
	 */
	public static void handleRestClientException(HttpClientErrorException exception) {
        // handle rest client exception from CarIQ exception
        GenericJSON json = Utils.getJSonObject(exception.getResponseBodyAsString(), GenericJSON.class);
        String errMsg = Utils.getValue(json, MESSAGE_KEY).replaceAll("^.|.$", "");
        Utils.handleException(errMsg);
    }
}