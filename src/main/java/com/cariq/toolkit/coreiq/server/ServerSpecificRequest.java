package com.cariq.toolkit.coreiq.server;

public interface ServerSpecificRequest {
	String getServerName();
}
