/**
 * 
 */
package com.cariq.toolkit.coreiq.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.ObjectBuilder;
import com.cariq.toolkit.utils.SortableParameter;
import com.cariq.toolkit.utils.SortableParameterComparator;
import com.cariq.toolkit.utils.Utils;

/**
 * Multi-threaded Exporter Template is useful to generate Export based on
 * different queries yielding different column values. Internally there are
 * multiple Exporters that get fired in parallel After they produce results,
 * results are accumulated as follows Exporter1 - A, B, C, 1, 2, 3, 4 Exporter2
 * - A, B, D, 4, 5, 6 Exporter3 - A, B, C, 5, 3, 9
 * 
 * Final outcome A, B, C, D, 1, 2, 3, 4, 5, 6, 9 To each exporter, fetch the
 * values a, b, c, d, - Those having data in a field will be compressed
 * 
 * @author hrishi
 *
 */
public abstract class MultiThreadedCompositeExporterTemplate extends CarIQExporterTemplate
		implements ObjectBuilder<Object> {
	public static final String EXPORTER = "EXPORTER";
	public static final String SORTABLE_PARAM = "SORTABLE_PARAM";

	List<CarIQExporter> childExporters = new ArrayList<CarIQExporter>();
	List<SortableParameter> sortableParameters = new ArrayList<SortableParameter>();

	/**
	 * @param name
	 * @param description
	 */
	public MultiThreadedCompositeExporterTemplate(String name, String description) {
		super(name, description);
	}

	@Override
	public List<CarIQExporter> getChildExporters() {
		return Arrays.<CarIQExporter>asList(this);
	}

	private List<SortableParameter> getSortableParameters() {
		return sortableParameters;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.exporter.CarIQExporterTemplate#doExport(java.lang.
	 * String, com.cariq.www.utils.GenericJSON, int, int)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		// Multiple threads - each running in different threads... each with set
		List<Future<List<GenericJSON>>> listOfFutures = new ArrayList<Future<List<GenericJSON>>>();
		for (CarIQExporter childExporter : getChildExporters()) {
			CarIQAsyncExporterIf asyncExp = new CarIQAsyncExporter(childExporter);
			listOfFutures.add(asyncExp.exportAsync(queryType, json, pageNo, pageSize));
		}

		// All the lists coming from all exporters
		List<List<GenericJSON>> outputLists = new ArrayList<List<GenericJSON>>();

		// Block on future for future to complete
		for (Future<List<GenericJSON>> future : listOfFutures) {
			List<GenericJSON> lst;
			try {
				lst = future.get();
				logger.debug("Future returned: " + lst);
				outputLists.add((List<GenericJSON>) Utils.sortMap(lst, getSortableParameters()));
			} catch (Exception e) {
				Utils.logException(logger, e, "MultiThreadedExporter export");
			}
		}

		// Now, we have multiple lists - start going over each and create a combined JSON

		// <HVN> Now is a very tricky algorithm... use pen and paper to figure out
		List<GenericJSON> retval = processLists(outputLists, sortableParameters, pageNo, pageSize);

		return retval;
	}

	public static class IndexedList<T> {
		List<T> list;
		int index;

		/**
		 * @param list
		 */
		public IndexedList(List<T> list) {
			this.list = list;
			index = 0;
		}

		public T get() {
			if (index >= list.size())
				return null;

			return list.get(index);
		}

		IndexedList<T> moveForward() {
			++index;
			return this;
		}

		IndexedList<T> moveBackward() {
			--index;
			return this;
		}

		IndexedList<T> reset() {
			index = 0;
			return this;
		}
	}

	public static class IndexedGenericJSON extends GenericJSON {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		int index;

		/**
		 * @param index
		 */
		public IndexedGenericJSON(int index, GenericJSON json) {
			this.index = index;
			super.putAll(json);
		}

	}

	public static class ProcessingBucket {
		List<IndexedList<GenericJSON>> listsToProcess = new ArrayList<IndexedList<GenericJSON>>();
		List<SortableParameter> sortableParameters;

		/**
		 * @param sortableParameters
		 */
		public ProcessingBucket(List<SortableParameter> sortableParameters) {
			this.sortableParameters = sortableParameters;
		}

		ProcessingBucket add(IndexedList<GenericJSON> l) {
			listsToProcess.add(l);
			return this;
		}

		/**
		 * Complicated Function - we want to get the Combined Generic JSOn based on
		 * sortable parameters match First get GenericJSON from each list Sort the list
		 * If the values match, combine jsons
		 * 
		 * @return
		 */
		@SuppressWarnings("unchecked")
		public GenericJSON getNext() {

			List<IndexedGenericJSON> combinedGenericJsons = new ArrayList<IndexedGenericJSON>();

			for (int i = 0; i < listsToProcess.size(); i++) {
				IndexedList<GenericJSON> currentList = listsToProcess.get(i);
				GenericJSON currentJSON = currentList.get();
				if (currentJSON != null && currentJSON.isEmpty() == false)
					combinedGenericJsons.add(new IndexedGenericJSON(i, currentJSON));
			}

			if (combinedGenericJsons.isEmpty())
				return null;

			combinedGenericJsons = (List<IndexedGenericJSON>) Utils.sortMap(combinedGenericJsons, sortableParameters);

			GenericJSON retval = new GenericJSON();

			IndexedGenericJSON first = combinedGenericJsons.get(0); // first has to go
			listsToProcess.get(first.index).moveForward(); // move to next json

			retval.putAll(first);

			Comparator<Map<String, Object>> c = new SortableParameterComparator(sortableParameters);
			for (int i = 1; i < combinedGenericJsons.size(); i++) {
				IndexedGenericJSON json = combinedGenericJsons.get(i);
				if (!(c.compare(first, json) == 0))
					break;

				listsToProcess.get(json.index).moveForward(); // move to next json
				retval.putAll(json);
			}

			return retval;
		}

	}

	/**
	 * @param outputLists
	 * @param sortableParameters2
	 * @return
	 */
	private static List<GenericJSON> processLists(List<List<GenericJSON>> outputLists,
			List<SortableParameter> sortableParameters, int pageNo, int pageSize) {
		int startIndex = (pageNo - 1) * pageSize;
		int endIndex = startIndex + pageSize;

		// iterate forever
		ProcessingBucket bucket = new ProcessingBucket(sortableParameters);
		for (List<GenericJSON> list : outputLists) {
			bucket.add(new IndexedList<GenericJSON>(list));
		}

		List<GenericJSON> retVal = new ArrayList<GenericJSON>();
		GenericJSON json = bucket.getNext();
		int indx = 0;
		while (json != null) {
			// Already crossed endIndex
			if (indx >= endIndex)
				break;

			// don't add till we reach startIndex
			if (indx >= startIndex)
				retVal.add(json);

			json = bucket.getNext();
			indx++;
		}

		return retVal;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.ObjectBuilder#configure(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public MultiThreadedCompositeExporterTemplate configure(String setting, Object value) {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.ObjectBuilder#add(java.lang.Object)
	 */
	@Override
	public MultiThreadedCompositeExporterTemplate add(Object value) {
		throw new UnsupportedOperationException("MultiThreadedCompositeExporter.add() is not implemented");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.ObjectBuilder#add(java.lang.String,
	 * java.lang.Object)
	 */
	@Override
	public MultiThreadedCompositeExporterTemplate add(String parameter, Object value) {
		if (parameter.equals(EXPORTER)) {
			CarIQExporter child = (CarIQExporter) value;
			childExporters.add(child);
			super.addExportAttributes(child.getExporterDefinition().getExportAttrDefinition());
		} else if (parameter.equals(SORTABLE_PARAM))
			sortableParameters.add((SortableParameter) value);
		else
			Utils.handleException("Unknown parameter: " + parameter);

		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.ObjectBuilder#build()
	 */
	@Override
	public MultiThreadedCompositeExporterTemplate build() {
		return this;
	}

}
