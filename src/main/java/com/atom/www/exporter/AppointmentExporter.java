package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Configurable;

import com.atom.www.helper.AppointmentStatus;
import com.atom.www.helper.DomainHelper;
import com.atom.www.helper.StagingAndPolicyHelper;
import com.atom.www.helper.TimeSlot;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.model.Appointment;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.Domain.Domain_Out;
import com.cariq.toolkit.model.ProxyObject;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * It returns all proxies which has been scheduled for current timeslot and
 * also all proxies whose appointments are delayed.
 * 
 * @author sagar
 *
 */
@Configurable
public class AppointmentExporter extends SpotlightTemplate {
	
	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("AppointmentExporter");
	
	public static final String TheExporter = "AppointmentExporter";
	
	private static final String TheDescription = TheExporter + " Gives list of policies which "
			+ "has appointment of current timeslot";
	
	protected static final String KEY_OWNER = "owner";
		
	public static final String QueryType_default = "Default";
	private static final String defaultQueryTypeDescription = "Get appointments by owner and domain.";

	private static final String DOMAIN = "domain";
	
	private static final String AppointmentDate = "AppointmentDate";
	private static final String AppointmentStatusKey = "AppoinmentStatus";
	private static final String AppointmentId = "AppointmentId";
	
	private DomainHelper domainHelper;

	/**
	 * 
	 * @param name
	 * @param description
	 */
	public AppointmentExporter() {
		super(TheExporter, TheDescription);
		domainHelper = new DomainHelper();
		
		addQueryType(QueryType_default, defaultQueryTypeDescription,
				Arrays.asList(new QueryDefinitionAttribute(KEY_OWNER, "Username of owner", String.class),
						new QueryDefinitionAttribute(DOMAIN, "Name of domain", String.class)));
	}

	@Override
	protected String getSignatures(String queryType, GenericJSON json, int pageNo, int pageSize) {
		logger.debug("getSignatures: queryType: " + queryType + ", inputJson: " 
				+ json + ", pageNo: " + pageNo + ", pageSize:" + pageSize);

		try (ProfilePoint _doExport_ = ProfilePoint
				.profileAction("ProfAction_getSignatures_" + this.getClass().getCanonicalName())) {

			//Get appointments for current and all delayed timeslots
			List<Appointment> appointmentsByTimeSlot = getAppoinmentsByTimeSlot(json, pageNo, pageSize);

			// get signatures from proxyobject
			List<String> signatures = Appointment.getSignatures(appointmentsByTimeSlot);

			// get comma seperated signature from list of sigantures
			String commaSeperatedSignatures = Utils.getCommaSeperatedString(signatures);
			
			logger.debug("Signatures: " + commaSeperatedSignatures);

			// remove whitespaces from comma seperated sigantures
			return Utils.removeWhiteSpaces(commaSeperatedSignatures);
		}
	}
	
	/**
	 * Get Appointments by timeslots
	 * 
	 * @param inputJson
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	private List<Appointment> getAppoinmentsByTimeSlot(GenericJSON inputJson, int pageNo, int pageSize) {
		String owner = Utils.getValue(inputJson, KEY_OWNER);

		// Find owner: if owner given in input json then use it. Else
		// consider logged in user as owner.
		User user = owner == null ? User.getLoggedInUser() : User.getByUsername(owner);

		Domain_Out domain = domainHelper.getDomainByUser(user);
		
		//Get current timeslot
		TimeSlot timeslot = TimeSlot.getCurrentSlot();
		
		logger.debug("Getting appointments for current and all delayed timeslots."
				+ " currentSlot: " + timeslot.toString() + ", user: " + user.getUsername());

		// Get appointments by user and current timeslot
		List<Appointment> appointmentsByTimeSlot = Appointment.getForSlotAndAllDelayed(timeslot,
				user, Domain.findObjectById(domain.getId()), pageNo, pageSize);
		
		logger.debug("Total appointments found: " + (appointmentsByTimeSlot == null ? 0 : appointmentsByTimeSlot.size()));
		
		return appointmentsByTimeSlot;
	}

	@Override
	protected String getQueryType() {
		return StagingAndPolicyHelper.QueryType_ByPolicyNumber;
	}

	@Override
	protected List<GenericJSON> postProcess(String queryType, GenericJSON inputJson, List<GenericJSON> outputJson,
			int pageNo, int pageSize) {
		logger.debug("AppointmentExporter: postProcess: outputJson count after enhancing: " + (outputJson != null ? outputJson.size() : 0));
		LinkedHashMap<String, GenericJSON> identifiedMap = Utils.getIdentifiedMapFromList(outputJson, "policyNumber");
		
		logger.debug("Identified map size: " + (identifiedMap == null ? 0 : identifiedMap.size()));
		
		List<GenericJSON> retValues = new ArrayList<>();
		
		List<Appointment> appointmentsByTimeSlot = getAppoinmentsByTimeSlot(inputJson, pageNo, pageSize);
		for(Appointment appointment : appointmentsByTimeSlot) {
			ProxyObject proxy = appointment.getProxy();
			GenericJSON json = identifiedMap.get(proxy.getSignature());
			
			/**
			 * Appointment may exists, but, details of proxy may not exists in CarIQ layer.
			 * This is because sometime dummy users get deleted from CarIQ and appointment of same
			 * remain staled in Atom Layer.(Need to handle same at Architecture level).
			 * 
			 * Why would anyone schedule appointment for dummy customer or customer which is
			 * no more required in CarIQ layer? Maybe, that is different story(We are playing safe here).
			 * 
			 * But, for now adding null check below.
			 */
			if(json == null) {
				logger.error("json not found for: " + proxy.getSignature());
				continue;
			}
			
			
			json.put(AppointmentDate, Utils.changeDateFormat(appointment.getAppointmentOn(),
					Utils.YYYY_mm_dd_hh_mm_ss));
			json.put(AppointmentId, appointment.getId());
			
			TimeSlot appointmentTimeslot = appointment.identifyTimeSlot();
			TimeSlot currentTimeSlot = TimeSlot.getCurrentSlot();
			String status = currentTimeSlot.isAfter(appointmentTimeslot) ? 
					AppointmentStatus.DELAYED.name() : appointment.getStatus();
			json.put(AppointmentStatusKey, status);
			
			logger.debug("Adding " + proxy.getSignature() + " into final list.");
			
			retValues.add(json);
		}
		return retValues;
	}
}
