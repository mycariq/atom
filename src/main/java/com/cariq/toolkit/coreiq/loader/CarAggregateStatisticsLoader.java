package com.cariq.toolkit.coreiq.loader;

import com.cariq.toolkit.model.CarAggregateStatistics;
import com.cariq.toolkit.model.CarAggregateStatistics.CarAggregateStatistics_In;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;

/**
 * The Class CarAggregateStatisticsLoader.
 *
 * @author nitin
 */
public class CarAggregateStatisticsLoader extends CarIQLoaderTemplate{

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("CarAggregateStatisticsLoader");

	
	/** The Constant LOADER. */
	public static final String LOADER = "CarAggregateStatisticsLoader";
	
	/** The Constant LOADER_DESCRIPTION. */
	private static final String LOADER_DESCRIPTION = "This is CarAggregateStatistics Loader";
	
	/**
	 * Instantiates a new car aggregate statistics loader.
	 */
	public CarAggregateStatisticsLoader() {
		super(LOADER, LOADER_DESCRIPTION);
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.loader.CarIQLoaderTemplate#doLoad(com.cariq.toolkit.utils.GenericJSON, com.cariq.toolkit.coreiq.loader.CarIQLoaderContext)
	 */
	@Override
	protected void doLoad(GenericJSON json, CarIQLoaderContext context) {
		logger.debug("doLoad - json : " + json);
		CarAggregateStatistics_In aggregateStatistics_In= new CarAggregateStatistics_In();
		aggregateStatistics_In.fromGenricJSON(json);
		CarAggregateStatistics carAggregateStatistics = new CarAggregateStatistics();
		carAggregateStatistics.fromJSON(aggregateStatistics_In);
		carAggregateStatistics.persist();
	}
}