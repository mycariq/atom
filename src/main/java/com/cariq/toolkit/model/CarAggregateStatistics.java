package com.cariq.toolkit.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.CarAggregateStatistics.CarAggregateStatistics_In;
import com.cariq.toolkit.model.CarAggregateStatistics.CarAggregateStatistics_Out;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.GenericJsonable;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;
import com.cariq.toolkit.utils.Utils;

/**
 * The Class CarAggregateStatistics.
 *
 * @author nitin
 */
@Entity
@Configurable
public class CarAggregateStatistics extends PersistentObject<StageStatistics>
		implements JSONable<CarAggregateStatistics_In, CarAggregateStatistics_Out, IdJSON> {

	// SECTION 1 -Code for Persistence
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	/** The version. */
	@Version
	Integer version;

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.model.PersistentObject#getId()
	 */
	@Override
	public Long getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.model.PersistentObject#setId(java.lang.Long)
	 */
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.model.PersistentObject#getVersion()
	 */
	@Override
	public Integer getVersion() {
		return version;
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.model.PersistentObject#setVersion(java.lang.Integer)
	 */
	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	/** The entity manager. */
	@PersistenceContext
	transient EntityManager entityManager;

	/**
	 * Entity manager.
	 *
	 * @return the entity manager
	 */
	public static final EntityManager entityManager() {
		EntityManager em = new ProxyObject().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.coreiq.model.PersistentObject#getEntityManager()
	 */
	public EntityManager getEntityManager() {
		if (this.entityManager == null)
			this.entityManager = entityManager();
		return entityManager;
	}

	/**
	 * Find.
	 *
	 * @param id the id
	 * @return the stage statistics
	 */
	protected static StageStatistics find(Long id) {
		return entityManager().find(StageStatistics.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public StageStatistics findById(Long id) {
		return find(id);
	}

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/** The created on. */
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	/** The modified on. */
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	/**  Domain of proxy *. */
	@NotNull(message = "Domain cannot be null")
	@ManyToOne
	private Domain domain;

	/** The context. */
	private String context;

	/** The value json. */
	@Column(length = 2000)
	private String valueJson;

	/**
	 * Instantiates a new car aggregate statistics.
	 */
	public CarAggregateStatistics() {
		super();
	}

	/**
	 * Gets the created on.
	 *
	 * @return the created on
	 */
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * Sets the created on.
	 *
	 * @param createdOn the new created on
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * Gets the modified on.
	 *
	 * @return the modified on
	 */
	public Date getModifiedOn() {
		return modifiedOn;
	}

	/**
	 * Sets the modified on.
	 *
	 * @param modifiedOn the new modified on
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	/**
	 * Gets the domain.
	 *
	 * @return the domain
	 */
	public Domain getDomain() {
		return domain;
	}

	/**
	 * Sets the domain.
	 *
	 * @param domain the new domain
	 */
	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	/**
	 * Gets the context.
	 *
	 * @return the context
	 */
	public String getContext() {
		return context;
	}

	/**
	 * Sets the context.
	 *
	 * @param context the new context
	 */
	public void setContext(String context) {
		this.context = context;
	}

	/**
	 * Gets the value json.
	 *
	 * @return the value json
	 */
	public String getValueJson() {
		return valueJson;
	}

	/**
	 * Sets the value json.
	 *
	 * @param valueJson the new value json
	 */
	public void setValueJson(String valueJson) {
		this.valueJson = valueJson;
	}

	/**
	 * Sets the entity manager.
	 *
	 * @param entityManager the new entity manager
	 */
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String domainName = domain != null ? domain.getName() : null;
		return "CarAggregateStatistics [id=" + id + ", version=" + version + ", createdOn=" + createdOn
				+ ", modifiedOn=" + modifiedOn + ", domain=" + domainName + ", context=" + context + ", valueJson="
				+ valueJson + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((context == null) ? 0 : context.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((domain == null) ? 0 : domain.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((valueJson == null) ? 0 : valueJson.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarAggregateStatistics other = (CarAggregateStatistics) obj;
		if (context == null) {
			if (other.context != null)
				return false;
		} else if (!context.equals(other.context))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (domain == null) {
			if (other.domain != null)
				return false;
		} else if (!domain.equals(other.domain))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		if (valueJson == null) {
			if (other.valueJson != null)
				return false;
		} else if (!valueJson.equals(other.valueJson))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	// ---------------------------------------------
	// SECTION - 3 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API
	// interaction

	/**
	 * The Class CarAggregateStatistics_In.
	 */
	public static class CarAggregateStatistics_In implements GenericJsonable  {

		/** The domain id. */
		private Long domainId;
		
		/** The context. */
		private String context;
		
		/** The value json. */
		private String valueJson;

		/**
		 * Instantiates a new car aggregate statistics in.
		 */
		public CarAggregateStatistics_In() {
			super();
		}

		/**
		 * Instantiates a new car aggregate statistics in.
		 *
		 * @param domainId the domain id
		 * @param context the context
		 * @param valueJson the value json
		 */
		public CarAggregateStatistics_In(Long domainId, String context, String valueJson) {
			super();
			this.domainId = domainId;
			this.context = context;
			this.valueJson = valueJson;
		}

		/**
		 * Gets the domain id.
		 *
		 * @return the domain id
		 */
		public Long getDomainId() {
			return domainId;
		}

		/**
		 * Sets the domain id.
		 *
		 * @param domainId the new domain id
		 */
		public void setDomainId(Long domainId) {
			this.domainId = domainId;
		}

		/**
		 * Gets the context.
		 *
		 * @return the context
		 */
		public String getContext() {
			return context;
		}

		/**
		 * Sets the context.
		 *
		 * @param context the new context
		 */
		public void setContext(String context) {
			this.context = context;
		}

		/**
		 * Gets the value json.
		 *
		 * @return the value json
		 */
		public String getValueJson() {
			return valueJson;
		}

		/**
		 * Sets the value json.
		 *
		 * @param valueJson the new value json
		 */
		public void setValueJson(String valueJson) {
			this.valueJson = valueJson;
		}

		/* (non-Javadoc)
		 * @see com.cariq.toolkit.utils.GenericJsonable#toGenericJSON()
		 */
		@Override
		public GenericJSON toGenericJSON() {
			// Do nothing
			return null;
		}

		/* (non-Javadoc)
		 * @see com.cariq.toolkit.utils.GenericJsonable#fromGenricJSON(com.cariq.toolkit.utils.GenericJSON)
		 */
		@Override
		public void fromGenricJSON(GenericJSON json) {
			this.context = Utils.getValue(String.class, json, "context");
			this.valueJson = Utils.getValue(String.class, json, "valueJson");
			this.domainId = Utils.getValue(Long.class, json, "domain");
		}
	}

	/**
	 * The Class CarAggregateStatistics_Out.
	 */
	public static class CarAggregateStatistics_Out extends CarAggregateStatistics_In {

		/** The id. */
		private Long id;

		/** The created on. */
		private String createdOn;

		/** The modified on. */
		private String modifiedOn;

		/**
		 * Instantiates a new car aggregate statistics out.
		 */
		public CarAggregateStatistics_Out() {
			super();
		}

		/**
		 * Instantiates a new car aggregate statistics out.
		 *
		 * @param id the id
		 * @param domainId the domain id
		 * @param context the context
		 * @param valueJson the value json
		 * @param createdOn the created on
		 * @param modifiedOn the modified on
		 */
		public CarAggregateStatistics_Out(Long id, Long domainId, String context, String valueJson,
				String createdOn, String modifiedOn) {
			super(domainId, context, valueJson);
			this.id = id;
			this.createdOn = createdOn;
			this.modifiedOn = modifiedOn;
		}

		/**
		 * Gets the id.
		 *
		 * @return the id
		 */
		public Long getId() {
			return id;
		}

		/**
		 * Sets the id.
		 *
		 * @param id the new id
		 */
		public void setId(Long id) {
			this.id = id;
		}

		/**
		 * Gets the created on.
		 *
		 * @return the created on
		 */
		public String getCreatedOn() {
			return createdOn;
		}

		/**
		 * Sets the created on.
		 *
		 * @param createdOn the new created on
		 */
		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		/**
		 * Gets the modified on.
		 *
		 * @return the modified on
		 */
		public String getModifiedOn() {
			return modifiedOn;
		}

		/**
		 * Sets the modified on.
		 *
		 * @param modifiedOn the new modified on
		 */
		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.utils.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.utils.JSONable#toJSON()
	 */
	@Override
	public CarAggregateStatistics_Out toJSON() {
		return new CarAggregateStatistics_Out(id, domain.getId(), context, valueJson,
				Utils.convertToyyyymmdd_hhmmssFormat(createdOn), Utils.convertToyyyymmdd_hhmmssFormat(modifiedOn));
	}

	/**
	 * From JSON.
	 *
	 * @param json the json
	 */
	@Override
	public void fromJSON(CarAggregateStatistics_In json) {
		this.domain = Domain.findObjectById(json.getDomainId());
		this.context = json.getContext();
		this.valueJson = json.getValueJson();
		this.createdOn = new Date();
		this.modifiedOn = new Date();
	}
}