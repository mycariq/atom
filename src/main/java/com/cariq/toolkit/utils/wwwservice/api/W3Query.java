package com.cariq.toolkit.utils.wwwservice.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.utils.wwwservice.core.Condition;
import com.cariq.toolkit.utils.wwwservice.core.DataPoint;
import com.cariq.toolkit.utils.wwwservice.core.EnormousResultException;
import com.cariq.toolkit.utils.wwwservice.core.Function;
import com.cariq.toolkit.utils.wwwservice.core.Function.FunctionInputJson;
import com.cariq.toolkit.utils.wwwservice.core.GeoTile;
import com.cariq.toolkit.utils.wwwservice.core.Identity;
import com.cariq.toolkit.utils.wwwservice.core.Identity.IdentityJson;
import com.cariq.toolkit.utils.wwwservice.core.ParameterDef;
import com.cariq.toolkit.utils.wwwservice.core.QueryInterface;
import com.cariq.toolkit.utils.wwwservice.core.QueryRangeRestriction;
import com.cariq.toolkit.utils.wwwservice.core.QueryResult;
import com.cariq.toolkit.utils.wwwservice.core.Region;
import com.cariq.toolkit.utils.wwwservice.core.Region.RegionJson;
import com.cariq.toolkit.utils.wwwservice.core.TimeRange;
import com.cariq.toolkit.utils.wwwservice.core.TimeRange.TimeRangeJson;
import com.cariq.toolkit.utils.wwwservice.core.TimeStamp;
import com.cariq.toolkit.utils.wwwservicetest.core.JDBCNativeQuery;


/**
 * Main Query Class for making complex multi parameter, multi-function query on
 * a query scope This facilitates adding values for the parameters and functions
 * on those
 * 
 * @author Abhijit
 *
 */
public class W3Query {
	public static class QueryScope {
		public List<Identity> getIds() {
			return ids;
		}

		public TimeRange getTimeRange() {
			return timeRange;
		}

		public Region getRegion() {
			return region;
		}

		List<Identity> ids = new ArrayList<Identity>();
		TimeRange timeRange = null;
		Region region = null;

		public void addId(Identity identity) {
			if (null == ids)
				ids = new ArrayList<Identity>();

			ids.add(identity);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "QueryScope [ids=" + ids + ", timeRange=" + timeRange
					+ ", region=" + region + "]";
		}

	}
	
	// Define the space in which query should be executed
	QueryScope scope = new QueryScope();
	
	// Define the range of query - by default null;
	QueryRangeRestriction range = null;
	
	// set of functions on different Parameters
	List<Function> functions = new ArrayList<Function>();
	// Query interface - HBASE or Test Interface
//	private QueryInterface queryInterface = new JDBCQuery();
	private QueryInterface queryInterface = new JDBCNativeQuery();
	private String dataset;

	public W3Query(String dataset) {
		if (dataset.equals("raw"))
			this.dataset = "pid";
		else if (dataset.equals("distance"))
			this.dataset = "odo_meter_log";
		else
			this.dataset = dataset;
	}

	/**
	 * @param queryInterface
	 *            the queryInterface to set
	 */
	public void setQueryInterface(QueryInterface queryInterface) {
		this.queryInterface = queryInterface;
	}

	public W3Query addFunction(Function f) {
		functions.add(f);
		return this;
	}

	public W3Query addId(long id) {
		scope.addId(new Identity(id));
		return this;
	}

	public W3Query addId(Identity id) {
		scope.addId(id);
		return this;
	}

	public List<Identity> getIds() {
		return scope.ids;
	}

	public W3Query setTimeRange(Date startDate, Date endDate) {
		scope.timeRange = new TimeRange(new TimeStamp(startDate),
				new TimeStamp(endDate));
		return this;
	}

	public W3Query setTimeRange(TimeRange range) {
		scope.timeRange = range;
		return this;
	}

	public W3Query setTimeRange(TimeStamp start, TimeStamp end) {
		scope.timeRange = new TimeRange(start, end);
		return this;
	}

	public TimeRange getTimeRange() {
		return scope.timeRange;
	}

	public W3Query setRegion(GeoTile lowerLeft, GeoTile upperRight) {
		scope.region = new Region(lowerLeft, upperRight);
		return this;
	}

	public W3Query setRegion(Region region) {
		scope.region = region;
		return this;
	}

	public W3Query setRegion(double lowerLeftLat, double lowerLeftLong,
			double upperRightLat, double upperRightLong) {
		return setRegion(new GeoTile(lowerLeftLat, lowerLeftLong), new GeoTile(
				upperRightLat, upperRightLong));
	}

	public Region getRegion() {
		return scope.region;
	}
	
	public W3Query setPaginationSize(long startRowNum, int pageSize) {
		if (null == range)
			range = new QueryRangeRestriction();
		
		range.setRowNumberRange(startRowNum, (startRowNum + pageSize));	
		return this;
	}
	
	public W3Query setPaginationRows(long startRowNum, long endRowNum) {
		if (null == range)
			range = new QueryRangeRestriction();
		
		range.setRowNumberRange(startRowNum, endRowNum);	
		return this;
	}

	public W3Query setRowIdRange(long startRowId, long endRowId) {
		if (null == range)
			range = new QueryRangeRestriction();
		
		range.setRowIdRange(startRowId, endRowId);
		return this;
	}
	
	public W3QueryResult execute() throws EnormousResultException {
		try (ProfilePoint _W3Query_execute = ProfilePoint
				.profileActivity("ProfActivity_W3Query_execute")) {
			// Make a query based on "scope" and "parameters"
			// execute Query and getResultSet
			// * Note for geotiles, we have to use "Special Comparator"
			// pass ResultSet down to each of the functions
			// once done, append the FunctionOutput to the QueryResult
			
			
			W3QueryResult retVal = new W3QueryResult();
			CarIQToolkitHelper.logger.info(scope.toString());
			// <TODO> Entity manager needs to be supplied to execute it
			QueryResult result = queryInterface.execute(null /*Pid.EntityManager*/, dataset, scope.ids,
					scope.region, scope.timeRange, range, getParams());
			
			if (null == result || result.isEmpty()) // fetched nothing
				return retVal;
			
			while (result.next()) {
				DataPoint dataPoint = result.getDataPoint();
				for (Function function : functions) {
					function.process(dataPoint);
				}
			}
			// Result can then be JSONized.
			for (Function function : functions) {
				retVal.addFunctionOutput(function);
			}
			return retVal;
		}
	}

	/**
	 * Get Parameters we are interested in
	 * 
	 * @return
	 */
	private Set<String> getParams() {
		Set<String> retval = new HashSet<String>();

		for (Function function : functions) {
			for (ParameterDef paramDef : function.getParams()) {
				String[] paramNames = paramDef.getParams();
				for (String p : paramNames) {
					retval.add(p);
				}
			}
			
			Condition condition = function.getCondition();
			if (null != condition)
				condition.collectParameters(retval);
		}
		return retval;
	}

	public static W3Query fromJson(W3QueryJson queryJson) {
		// String dataset;
		W3Query query = new W3Query(queryJson.dataset);

		if (queryJson.ids != null && !queryJson.ids.isEmpty()) {
			// List<IdentityJson> ids;
			for (IdentityJson id : queryJson.ids) {
				query.addId(Identity.fromJson(id));
			}
		}

		// TimeRangeJson timeRange;
		if (null != queryJson.timeRange)
			query.setTimeRange(TimeRange.fromJson(queryJson.timeRange));

		// RegionJson region;
		if (null != queryJson.region)
			query.setRegion(Region.fromJson(queryJson.region));

		// List<FunctionInputJson> functions;
		if (queryJson.functions != null && !queryJson.functions.isEmpty()) {
			for (FunctionInputJson function : queryJson.functions) {
				query.addFunction(Function.fromJson(function));
			}
		}

		return query;
	}

	public W3QueryJson toJson() {
		W3QueryJson w3QueryJson = new W3QueryJson();
		w3QueryJson.dataset = dataset;

		for (Identity identity : getIds()) {
			w3QueryJson.ids.add(identity.toJson());
		}

		TimeRange tr = getTimeRange();
		if (tr != null)
			w3QueryJson.timeRange = tr.toJson();

		Region r = getRegion();
		if (r != null)
			w3QueryJson.region = r.toJson();

		for (Function function : functions) {
			w3QueryJson.functions.add(function.toJson());
		}

		return w3QueryJson;
	}
	
	public static class W3QueryJson {
		public String getDataset() {
			return dataset;
		}
		public void setDataset(String dataset) {
			this.dataset = dataset;
		}
		public List<IdentityJson> getIds() {
			return ids;
		}
		public void setIds(List<IdentityJson> ids) {
			this.ids = ids;
		}
		public TimeRangeJson getTimeRange() {
			return timeRange;
		}
		public void setTimeRange(TimeRangeJson timeRange) {
			this.timeRange = timeRange;
		}
		public RegionJson getRegion() {
			return region;
		}
		public void setRegion(RegionJson region) {
			this.region = region;
		}
		public List<FunctionInputJson> getFunctions() {
			return functions;
		}
		public void setFunctions(List<FunctionInputJson> functions) {
			this.functions = functions;
		}
		String dataset;
		List<IdentityJson> ids = new ArrayList<Identity.IdentityJson>();
		TimeRangeJson timeRange;
		RegionJson region;
		List<FunctionInputJson> functions = new ArrayList<Function.FunctionInputJson>();
	}

	public W3Query() {
		super();
	}	
}
