/**
 * 
 */
package com.cariq.toolkit.coreiq.exporter;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.HtmlValueRenderer;
import com.cariq.toolkit.utils.SimplePOJOs.CountPOJO;

/**
 * @author hrishi
 *
 */
public class CarIQAsyncExporter implements CarIQAsyncExporterIf {
	CarIQExporter exporter;

	/**
	 * @param exporter
	 */
	public CarIQAsyncExporter(CarIQExporter exporter) {
		this.exporter = exporter;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.exporter.CarIQExporter#getName()
	 */
	@Override
	public String getName() {
		return exporter.getName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.exporter.CarIQExporter#getDescription()
	 */
	@Override
	public String getDescription() {
		return exporter.getDescription();
	}

	@Override
	public String getShortName() {
		return exporter.getShortName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.exporter.CarIQExporter#export(java.lang.String,
	 * com.cariq.www.utils.GenericJSON, int, int)
	 */
	@Override
	public List<GenericJSON> export(String queryType, GenericJSON json, int pageNo, int pageSize) {
		return exporter.export(queryType, json, pageNo, pageSize);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.exporter.CarIQExporter#getExporterDefinition()
	 */
	@Override
	public CarIQExporterDefinition getExporterDefinition() {
		return exporter.getExporterDefinition();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.exporter.CarIQExporter#getHtmlRenderer()
	 */
	@Override
	public HtmlValueRenderer getHtmlRenderer() {
		return exporter.getHtmlRenderer();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.exporter.CarIQExporter#isAuthorised()
	 */
	@Override
	public boolean isAuthorised() {
		return exporter.isAuthorised();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.www.coreiq.exporter.CarIQAsyncExporterIf#exportAsync(java.lang.
	 * String, com.cariq.www.utils.GenericJSON, int, int)
	 */
	@Override
	public Future<List<GenericJSON>> exportAsync(final String queryType, final GenericJSON json, final int pageNo,
			final int pageSize) {
		// TODO Auto-generated method stub
		Callable<List<GenericJSON>> callable = new Callable<List<GenericJSON>>() {
			public java.util.List<GenericJSON> call() throws Exception {
				return exporter.export(queryType, json, pageNo, pageSize);
			}
		};

		final int THREADCOUNT = 1; // play with this value
		ExecutorService executorService = Executors.newFixedThreadPool(THREADCOUNT);
		return executorService.submit(callable);
	}

	@Override
	public List<CarIQExporter> getChildExporters() {
		return Arrays.<CarIQExporter>asList(this);
	}

	@Override
	public CountPOJO getExportCount(String queryType, GenericJSON json) {
		throw new RuntimeException("Not yet implemented.");
	}

	@Override
	public List<GenericJSON> exportRecords(String queryType, GenericJSON json, int startIndex, int endIndex) {
		throw new RuntimeException("Not yet implemented.");
	}

}
