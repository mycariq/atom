package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.atom.www.helper.MISReportHelper;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.model.BillingAccount;
import com.cariq.toolkit.model.BillingContract;
import com.cariq.toolkit.model.BillingOperation;
import com.cariq.toolkit.model.BillingTerm;
import com.cariq.toolkit.model.Invoice;
import com.cariq.toolkit.model.BillingOperation.BillingStatusEnum;
import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Month;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.analytics.application.BillingRecordProcessor;
import com.cariq.toolkit.utils.analytics.core.AnalyticsContext;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsCookbook;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsProcess;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsRecipe;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.google.common.base.Strings;

/**
 * The Class BillingInvoiceExporter.
 *
 * @author amita
 */
public class BillingInvoiceExporter extends CarIQExporterTemplate {
	
	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("BillingInvoiceExporter");
	
	/** The Constant EXPORTER. */
	public static final String EXPORTER = "BillingInvoiceExporter";
	
	MISReportHelper reportHelper = new MISReportHelper();

	/** The Constant Query types. */
	public static final String FROM_TAT_URL = "FROM_TAT_URL", FROM_TAT_FILE = "FROM_TAT_FILE",
			FROM_TAT_URL_LATEST = "FROM_TAT_URL_LATEST";
			
	/** The Constant QueryDefinition attributes. */			
	public static final String	TAT_FILE_URL = "TATFileURL", TAT_FILE_PATH = "TATFilePath";
	public static final String CONTRACT = "BillingContract";
	public static final String RAISED_BY_ACCOUNT = "RaisedByAccount";
	public static final String RAISED_AGAINST_ACCOUNT = "RaisedAgainstAccount";
	public static final String FROM_MONTH = "FromMonth"; // format: MMMM-yyyy
	public static final String TO_MONTH = "ToMonth"; // format: MMMM-yyyy
	public static final String BILLINGTERMS = "BillingTerms";
	
	private static final String BILLING_RECIPE = "BillingRecipe";

	// Context to be used throughout billing process.
	BillingContract billingContract = null;
	BillingAccount raisedByAccount = null, raisedAgainstAccount = null;
	List<BillingTerm> billingTerms = new ArrayList<BillingTerm>();
	BillingOperation billingOperation = null;
	Month startMonth = null, endMonth = null;
	String tatFileURL = null, tatFilePath = null;
	
	/**
	 * 
	 * Instantiates a new BillingInvoiceExporter exporter.
	 */
	public BillingInvoiceExporter() {
		super(EXPORTER, "Exports billing invoice for given contract, months and billing terms");

		// Query definitions
		addQueryType(FROM_TAT_FILE,
				"Generates Billing invoice from given TAT report file",
				Arrays.asList(new QueryDefinitionAttribute(TAT_FILE_PATH, "Absolute file path to TATReport csv file"),
						new QueryDefinitionAttribute(CONTRACT, "Billing Contract"),
						new QueryDefinitionAttribute(RAISED_BY_ACCOUNT, "Raised by Account"),
						new QueryDefinitionAttribute(RAISED_AGAINST_ACCOUNT, "Raised against Account"),
						new QueryDefinitionAttribute(FROM_MONTH, "From Month (MMMM-yyyy) e.g: May-2020"),
						new QueryDefinitionAttribute(TO_MONTH, "To Month (MMMM-yyyy) ee.g: June-2020"),
						new QueryDefinitionAttribute(BILLINGTERMS, "Billing Terms")));

		addQueryType(FROM_TAT_URL,
				"Generates Billing invoice from given TAT report URL",
				Arrays.asList(new QueryDefinitionAttribute(TAT_FILE_URL, "TATReport URL"),
						new QueryDefinitionAttribute(CONTRACT, "Billing Contract"),
						new QueryDefinitionAttribute(RAISED_BY_ACCOUNT, "Raised by Account"),
						new QueryDefinitionAttribute(RAISED_AGAINST_ACCOUNT, "Raised against Account"),
						new QueryDefinitionAttribute(FROM_MONTH, "From Month (MMMM-yyyy) e.g: May-2020"),
						new QueryDefinitionAttribute(TO_MONTH, "To Month (MMMM-yyyy) ee.g: June-2020"),
						new QueryDefinitionAttribute(BILLINGTERMS, "Billing Terms")));

		addQueryType(FROM_TAT_URL_LATEST, "Generates Billing invoice from latest TAT report URL",
				Arrays.asList(new QueryDefinitionAttribute(CONTRACT, "Billing Contract"),
					new QueryDefinitionAttribute(RAISED_BY_ACCOUNT, "Raised by Account"),
					new QueryDefinitionAttribute(RAISED_AGAINST_ACCOUNT, "Raised against Account"),
					new QueryDefinitionAttribute(FROM_MONTH, "From Month (MMMM-yyyy) e.g: May-2020"),
					new QueryDefinitionAttribute(TO_MONTH, "To Month (MMMM-yyyy) ee.g: June-2020"),
					new QueryDefinitionAttribute(BILLINGTERMS, "Billing Terms")));
		
		// Support roles
		addSupportedRoles(Arrays.asList(Utils.ADMIN_ROLE));
	}

	// Process the date range from the given months in the format 'mmmm-yyyy'
	private void getDateRange(String fromMonth, String toMonth) {
		try {
			startMonth = new Month(fromMonth, Utils.INDIA_TIME_ZONE);
			endMonth = new Month(toMonth, Utils.INDIA_TIME_ZONE);
		} catch(Exception e) {
			Utils.handleException("Failed to find the date range fromMonth: " + fromMonth + ", toMonth: " + toMonth);
		}
	}
	
	// Initialize billing parameters
	private void initBillingParams(String queryType, GenericJSON json) {
		// Capture TAT file as per queryType
		if(queryType.equalsIgnoreCase(FROM_TAT_FILE)) {
			tatFilePath = Utils.getValue(String.class, json, TAT_FILE_PATH);
		} else if (queryType.equalsIgnoreCase(FROM_TAT_URL)) {
			tatFileURL = Utils.getValue(String.class, json, TAT_FILE_URL);
		} else if (queryType.equalsIgnoreCase(FROM_TAT_URL_LATEST)) {
			tatFileURL = reportHelper.getLatestTATURL();
		} else {
			Utils.handleException("QueryType " + queryType + " is not supported!!");
		}
		
		// Set the account raising this bill
		String byAccountName = Utils.getValue(json, RAISED_BY_ACCOUNT);
		raisedByAccount = BillingAccount.getByName(Utils.getValue(json, RAISED_BY_ACCOUNT));
		if (null == raisedByAccount)
			Utils.handleException("Invalid billing account: " + byAccountName);
		
		logger.debug("***raisedByAccount: " + raisedByAccount.getName());
		
		// Set the account against which billing should be done
		String againstAccountName = Utils.getValue(json, RAISED_AGAINST_ACCOUNT);
		raisedAgainstAccount = BillingAccount.getByName(againstAccountName);
		if (null == raisedAgainstAccount)
			Utils.handleException("Invalid billing account: " + againstAccountName);
		
		logger.debug("***raisedAgainstAccount: " + raisedAgainstAccount.getName());
		
		// Set the relevant contract
		String contractName = Utils.getValue(json, CONTRACT);
		billingContract = BillingContract.getByNameAndAccount(contractName, raisedAgainstAccount);
		if (null == billingContract)
			Utils.handleException("Invalid billing contract: " + contractName + " for account " + raisedAgainstAccount.getName());
		
		logger.debug("***billingContract: " + billingContract.getName());
		
		// Check and initialize the billing terms under given contract
		billingTerms.clear();
		String billingTermsList = Utils.getValue(json, BILLINGTERMS);
		if (billingTermsList.equalsIgnoreCase("all")) {
			billingTerms.addAll(BillingTerm.getByContract(billingContract));
		} else {
			for (String bt : billingTermsList.split(",")) {
				BillingTerm term = BillingTerm.getByName(bt.trim());
				if (null == term)
					Utils.handleException("Invalid billing term: " + bt.trim());
				if (billingContract != term.getItsContract())
					Utils.handleException("Invalid billing term " + bt.trim() + " for contract: " + contractName);
				billingTerms.add(term);
			}
		}		
		logger.debug("***billingTerms: " + billingTerms.size());
		
		// Process the date range.
		getDateRange(Utils.getValue(json, FROM_MONTH), Utils.getValue(json, TO_MONTH));		
		logger.debug("***fromDate: " + startMonth.getStartTime() + ", toDate: " + endMonth.getEndTime());
		
		// Make sure TAT file ready.
		if (! Strings.isNullOrEmpty(tatFileURL)) {
			tatFilePath = CarIQFileUtils.getLocalFilePathFromURL(tatFileURL);
		}
		
		if (Strings.isNullOrEmpty(tatFilePath) || !CarIQFileUtils.exists(tatFilePath)) {
			Utils.handleException("Invalid TAT report file: " + tatFilePath);
		}
		logger.debug("===> TAT File ready at: " + tatFilePath);
	}
	
	private void createBillingOperation() {
		String forBillingTerms = "";
		for (BillingTerm bt : billingTerms) {
			forBillingTerms = forBillingTerms.concat(bt.getName() + ",");
		}
		
		logger.debug("***forBillingTerms: " + forBillingTerms);
		
		billingOperation = new BillingOperation(new Date(), null, raisedByAccount, raisedAgainstAccount, billingContract, 
				startMonth.getName(), endMonth.getName(), forBillingTerms, reportHelper.getCurrentDomain(),
				reportHelper.getCurrentUser(), new Date(),	new Date());
		billingOperation.persist();
	}
	
	private void processTAT() {
		try {
			// Load analytics context
			AnalyticsContext ctx = new AnalyticsContext();		
			ctx.put(BillingRecordProcessor.BILLING_OPERATION_ID , billingOperation.getId().toString());
			logger.debug("***billingOperationId: " + billingOperation.getId().toString());
			
			// Get the billing recipe
			AnalyticsRecipe recipe = AnalyticsCookbook.getInstance().getRecipe(BILLING_RECIPE);
			AnalyticsProcess process = new AnalyticsProcess(recipe, ctx, tatFilePath);
			
			// Execute. Recipe will scan the TAT file cook the billing, ready to serve!
			logger.debug("**********Executing process***********");
			process.execute();
			process.close();
			logger.debug("**********Process Closed***********");
		} catch(Exception e) {
			Utils.handleException("Exception in processTAT(): " + e.getMessage());
		}
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate#doExport(java.lang.
	 * String, com.cariq.toolkit.utils.GenericJSON, int, int)
	 */
	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		try (ProfilePoint BillingInvoiceExporter_doExport = ProfilePoint
				.profileAction("ProfAction_BillingInvoiceExporter_doExport")) {
	        
			// Ignore pageNo, ignorepageSize, just return alerts of each type
	        if (pageNo > 1)
	            return null;
	        
			logger.debug("doExport - queryType : " + queryType + " pageNo : " + pageNo + " pageSize : " + pageSize
					+ " json :" + json);
	        
			List<GenericJSON> retList = new ArrayList<GenericJSON>();
			
			// Initialize the helper object
			reportHelper.configureDomainUser(json);			
				
			// Initialize the inputs for billing generation
			initBillingParams(queryType, json);

			createBillingOperation();
			
			processTAT();
			
			GenericJSON outJSON = new GenericJSON();
			outJSON.add("BillingOperationId", billingOperation.getId());
			outJSON.add("BillingOperationStatus", billingOperation.getBillingResult());
			outJSON.add("RaisedAgainstAccount", raisedAgainstAccount.getName());

			if (billingOperation.getBillingResult().equals(BillingStatusEnum.SUCCESSFUL.toString())) {
				Invoice invoice = Invoice.getByItsOperation(billingOperation);
				outJSON.add("InvoiceId", invoice.getId());
				outJSON.add("InvoiceDocument", invoice.getUrlToDocument());
				
				logger.info("Billing operation# " + billingOperation.getId() 
					+ " completed successfully for " + raisedAgainstAccount.getName() 
					+ ", please review the invoice# " + invoice.getId() + ": " + invoice.getUrlToDocument());
			} else				
				logger.info("Billing operation# " + billingOperation.getId() 
				+ " completed as INVALID for " + raisedAgainstAccount.getName() 
				+ ", hence NO valid invoice available.");
				
			retList.add(outJSON);				
			return retList;
		}
	}
}