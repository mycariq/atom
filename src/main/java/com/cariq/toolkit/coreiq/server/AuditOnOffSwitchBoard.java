package com.cariq.toolkit.coreiq.server;

import java.util.Date;

public class AuditOnOffSwitchBoard implements ServerSpecificResponse {
	OnOffSwitchBoard switchBoard;
	String serverName;
	Date serverStartTime;

	public AuditOnOffSwitchBoard() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AuditOnOffSwitchBoard(OnOffSwitchBoard switchBoard, String serverName, Date serverStartTime) {
		super();
		this.switchBoard = switchBoard;
		this.serverName = serverName;
		this.serverStartTime = serverStartTime;
	}

	public OnOffSwitchBoard getSwitchBoard() {
		return switchBoard;
	}

	public void setSwitchBoard(OnOffSwitchBoard switchBoard) {
		this.switchBoard = switchBoard;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public Date getServerStartTime() {
		return serverStartTime;
	}

	public void setServerStartTime(Date serverStartTime) {
		this.serverStartTime = serverStartTime;
	}
}
