/**
 * 
 */
package com.cariq.toolkit.publicapi.swagger;

/**
 * @author hrishi
 *
 */
public class Parameter {

	private String in;
	private String name;
	private String description;
	private boolean required;
	private Definition schema;
	private String type;

	public String getIn() {
		return in;
	}

	public void setIn(String in) {
		this.in = in;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public Definition getSchema() {
		return schema;
	}

	public void setSchema(Definition schema) {
		this.schema = schema;
	}

	/**
	 * @param in
	 * @param name
	 * @param description
	 * @param required
	 * @param paramType
	 */
	public Parameter(String in, String name, String description, boolean required, Definition schema) {
		this.in = in;
		this.name = name;
		this.description = description;
		this.required = required;
		this.schema = schema;
	}

	/**
	 * @param in
	 * @param name
	 * @param description
	 * @param required
	 * @param paramType
	 */
	public Parameter(String in, String name, String description, boolean required, String paramType) {
		this.in = in;
		this.name = name;
		this.description = description;
		this.required = required;
		this.type = paramType;
	}

}
