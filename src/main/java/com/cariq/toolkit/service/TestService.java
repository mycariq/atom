package com.cariq.toolkit.service;

import com.cariq.toolkit.utils.GenericJSON;

/**
 * The Interface TestService.
 */
public interface TestService {
	
	
	/**
	 * Mysql ping.
	 *
	 * @return the generic JSON
	 */
	public GenericJSON mysqlPing();
	
}