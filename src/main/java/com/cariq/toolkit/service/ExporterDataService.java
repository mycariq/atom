package com.cariq.toolkit.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.cariq.toolkit.coreiq.exporter.ExportedData.ExporterDataJSON_Out;
import com.cariq.toolkit.utils.ResponseJson;

public interface ExporterDataService {

	/**
	 * Get exported data list
	 * 
	 * @param string
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	List<ExporterDataJSON_Out> getExportedDataList(String string, int pageNo, int pageSize);

	/**
	 * Upload file.
	 *
	 * @param file
	 *            the file
	 * @param description
	 *            the description
	 * @param domain
	 *            the domain
	 * @return the response json
	 */
	ResponseJson uploadFile(MultipartFile file, String description, String domain);

	/**
	 * Gets the uploaded file exporter data list.
	 *
	 * @param domain the domain
	 * @param startDate the start date
	 * @param endDate the end date
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @return the uploaded file exporter data list
	 */
	List<ExporterDataJSON_Out> getUploadedFileExporterDataList(String domain, String startDate, String endDate, int pageNo,
			int pageSize);

	/**
	 * Gets the uploaded files by key word.
	 *
	 * @param domain the domain
	 * @param keyWord the key word
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @return the uploaded files by key word
	 */
	List<ExporterDataJSON_Out> getUploadedFilesByKeyWord(String domain, String keyWord, int pageNo, int pageSize);

	/**
	 * Gets the exporter data by domain and date.
	 *
	 * @param domain the domain
	 * @param startDate the start date
	 * @param endDate the end date
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @return the exporter data by domain and date
	 */
	List<ExporterDataJSON_Out> getExporterDataByDomainAndDate(String domain, String startDate, String endDate, int pageNo,
			int pageSize);
}