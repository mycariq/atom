package com.atom.www.exporter;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.coreiq.CarIQRawQuery;
import com.cariq.toolkit.coreiq.CarIQSimpleAggregationQuery;
import com.cariq.toolkit.coreiq.CarIQSimpleAggregationQuery.AggregationType;
import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.StageStatistics;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Transactional
@Configurable
public class StageStatisticsCountExporterTest {

	/** The Constant attributeAliasMap. */
	static final LinkedHashMap<String, String> attributeAliasMap = new LinkedHashMap<String, String>();

	/** The Constant tableAliasMap. */
	static final LinkedHashMap<String, String> tableAliasMap = new LinkedHashMap<String, String>();

	static {
		attributeAliasMap.put("MONTH(ss.entry_date)", "Month");
		attributeAliasMap.put("YEAR(ss.entry_date)", "Year");
		attributeAliasMap.put("count(*)", "Count");

		tableAliasMap.put("stage_statistics", "ss");
	}

	@Test
	public void test() {
		List<GenericJSON> result = getCountJson();
		for (GenericJSON json : result) {
			System.out.println(json);

		}
	}

	private List<GenericJSON> getCountJson() {
		try (ProfilePoint _getStatisticsByDateAndOwner = ProfilePoint.profileAction("ProfAction_getResult")) {
			Domain domain = Domain.getByName("IL-Assist");
			if (null == domain)
				Utils.handleException("Domain does not exists.");

			// get category name
			String category = "Car Connected";
			String type = "stage";
			// convert string to date
			Date sDate = Utils.convertToDate("2019-01-01 00:00:00", Utils.YYYY_MM_DD_hh_mm_ss);
			Date eDate = Utils.convertToDate("2019-09-01 00:00:00", Utils.YYYY_MM_DD_hh_mm_ss);

			if (null == sDate)
				Utils.handleException("From date should be in yyyy-MM-dd [HH:mm:ss] format.");

			if (null == eDate)
				Utils.handleException("To date should be in yyyy-MM-dd [HH:mm:ss] format.");

			CarIQSimpleAggregationQuery<Long> query = new CarIQSimpleAggregationQuery<Long>(
					"getCountJson", StageStatistics.entityManager(), StageStatistics.class, Long.class,
					AggregationType.Count, "carConnectedDate");
			query.addCondition("stage", CarIQSimpleQuery.EQ, "Car Connected");
			query.addCondition("domain", CarIQSimpleQuery.EQ, domain);
			query.addCondition("carConnectedDate", CarIQSimpleQuery.LT, sDate);
			
			Long totalCount = query.getSingleResult();
								
			CarIQRawQuery qry = new CarIQRawQuery("StageStatisticsCountExporter", StageStatistics.entityManager(),
					attributeAliasMap, tableAliasMap);
			qry.addGroupBy("MONTH(ss.car_connected_date),YEAR(ss.car_connected_date)");

			if (!"AllCustomers".equalsIgnoreCase(category))
				qry.addCondition(type, CarIQSimpleQuery.EQ, category);

			qry.addCondition("domain", CarIQSimpleQuery.EQ, domain);
			qry.addCondition("car_connected_date", CarIQSimpleQuery.GT_EQ, sDate);
			qry.addCondition("car_connected_date", CarIQSimpleQuery.LT_EQ, eDate);
			qry.orderBy("car_connected_date", CarIQSimpleQuery.ASC);

			List<GenericJSON> jsonList = qry.getResult();
			
			for (GenericJSON json : jsonList) {
				totalCount = totalCount + Utils.getValue(Long.class, json, "Count");
				json.put("Count", totalCount);
			}
			
			return jsonList;
		}
	}
}