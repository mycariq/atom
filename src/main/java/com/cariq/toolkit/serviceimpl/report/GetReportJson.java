package com.cariq.toolkit.serviceimpl.report;

import java.util.Date;

import com.cariq.toolkit.utils.Utils;

public class GetReportJson {
	private long id;
	private String generatedOn;
	private String from;
	private String to;
	private String logPath;
	private String size;
	private String type;

	public GetReportJson(long id, Date generatedOn, Date from, Date to, String logPath, String size, String type) {
		this.id = id;
		this.generatedOn = Utils.convertToyyyymmdd_hhmmssFormat(generatedOn);
		this.from = Utils.convertToyyyymmdd_hhmmssFormat(from);
		this.to = Utils.convertToyyyymmdd_hhmmssFormat(to);
		this.logPath = logPath;
		this.size = size;
		this.type = type;
	}

	public long getId() {
		return id;
	}

	public String getGeneratedOn() {
		return generatedOn;
	}

	public String getFrom() {
		return from;
	}

	public String getTo() {
		return to;
	}

	public String getLogPath() {
		return logPath;
	}

	public String getSize() {
		return size;
	}

	public String getType() {
		return type;
	}

}
