/**
 * 
 */
package com.cariq.toolkit.publicapi;

/**
 * @author hrishi
 *
 */
public @interface CarIQPublicAPIBodyParameter {

	String description();

	Class<?> cls();

}
