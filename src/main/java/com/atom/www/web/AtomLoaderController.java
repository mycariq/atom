package com.atom.www.web;

import java.io.File;
import java.util.List;
import java.util.Properties;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.cariq.toolkit.coreiq.loader.CarIQLoaderContext;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderService;
import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.publicapi.CarIQPublicAPIParameter;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.service.FileHandlerService;

@CarIQPublicAPI(name = "Loader", description = "[Admin Only] Everything related to Loader - For Loading Data into the Platform")
@RequestMapping("/admin/loader")
@Controller
public class AtomLoaderController {

	@Autowired
	CarIQLoaderService loaderService;

	@Autowired
	FileHandlerService fileHandlerService;

	@Resource(name = "configProperties")
	private Properties configProperties;

	/**
	 * Get All Loaders
	 *
	 * @param param
	 *            the raw json
	 * @param response
	 *            the response
	 * @return the object
	 */
	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "List the available Loaders", responseClass = List.class, internal = true)
	@RequestMapping(value = "/enlist", method = RequestMethod.GET)
	@ResponseBody
	public Object enlistLoaders(HttpServletResponse response) {
		try (CarIQAPI _getResource = new CarIQAPI("CARIQ_getLoaders")) {
			// CarIqUser carIqUser = CarIqValidator.validateUser();
			return loaderService.getLoaderDefinition();
		} catch (Exception e) {
			Utils.handleException(e);
		}
		return null;
	}

	/**
	 * Get Attributes required by a loader
	 * 
	 * @param loader
	 * @param response
	 * @return
	 */
	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Get Details of a Loader", responseClass = GenericJSON.class, internal = true)
	@RequestMapping(value = "/get/{loader}", method = RequestMethod.GET)
	@ResponseBody
	public Object getLoader(
			@CarIQPublicAPIParameter(name = "loader", description = "name of the loader") @PathVariable("loader") String loader,
			HttpServletResponse response) {
		try (CarIQAPI _getResource = new CarIQAPI("getLoaderConstructor")) {
			// CarIqUser carIqUser = CarIqValidator.validateUser();
			return loaderService.getLoaderDefinition(loader);
		} catch (Exception e) {
			Utils.handleException(e);
		}
		return null;
	}

	/**
	 * Register a resource.
	 *
	 * @param param
	 *            the raw json
	 * @param response
	 *            the response
	 * @return the object
	 */
	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Load Single Object using the loader", responseClass = GenericJSON.class, internal = true)
	@RequestMapping(value = "/loadSingle/{loader}", method = RequestMethod.POST)
	@ResponseBody
	@Transactional
	public Object loadSingle(
			@CarIQPublicAPIParameter(name = "loader", description = "name of the loader") @PathVariable("loader") String loader,
			@RequestBody GenericJSON json, HttpServletResponse response) {
		try (CarIQAPI _addResource = new CarIQAPI("CARIQ_loadSingle")) {
			try {
				CarIQLoaderContext ctx = new CarIQLoaderContext();
				loaderService.loadSingle(loader, json, ctx);
				ctx.addMessage("Loading done..!");
				return new ResponseJson(ctx.getMessages());
			} catch (Exception e) {
				Utils.handleException(e);
			}
			return null;
		}
	}

	/**
	 * Import resources with file
	 *
	 * @param param
	 *            the raw json
	 * @param response
	 *            the response
	 * @return the object
	 */
	// CarIQ Public API Documentation
@CarIQPublicAPIMethod(description = "Load csv file using the loader", responseClass = GenericJSON.class, internal = true)
	@RequestMapping(value = "/load/{loader}", method = RequestMethod.POST, consumes = { "multipart/form-data" })
	@ResponseBody
	@Transactional
	public Object importResources(@RequestParam("csvfile") MultipartFile csvFile, @CarIQPublicAPIParameter(name = "loader", description = "name of the loader") @PathVariable("loader") String loader,
			HttpServletResponse response) {
		try (CarIQAPI _addResources = new CarIQAPI("CARIQ_load")) {
			try {
				CarIQLoaderContext ctx = new CarIQLoaderContext();
				File file = fileHandlerService.getLocalFileHandle(csvFile, loader + ".csv");
				int count = loaderService.load(loader, file, ctx);
				ctx.addMessage("Loader - " + loader + ", Number of Loaded Objects: " + count);
				return new ResponseJson(ctx.getMessages());
			} catch (Exception e) {
				Utils.handleException(e);
			}
			return null;
		}
	}
}
