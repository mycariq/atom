/**
 * 
 */
package com.cariq.toolkit.coreiq.exporter;

import java.util.Arrays;
import java.util.List;

import com.cariq.toolkit.utils.GenericJSON;


/**
 * @author hrishi
 *
 */
public class UnityExportEnhancer extends ExportDataEnhancerTemplate {

	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.exporter.ExportDataEnhancerTemplate#doEnhance(com.cariq.www.utils.GenericJSON)
	 */
	@Override
	protected GenericJSON doEnhance(String queryType, GenericJSON inputJSON, GenericJSON exportJSON) {
		return exportJSON;
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.exporter.ExportDataEnhancerTemplate#doEnhance(com.cariq.www.utils.GenericJSON, com.cariq.www.utils.GenericJSON)
	 */
	@Override
	protected List<GenericJSON> doEnhance(String queryType, GenericJSON inputJSON, GenericJSON previous, GenericJSON current) {
		return Arrays.asList(current);
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.exporter.ExportDataEnhancerTemplate#doEnhance(java.util.List)
	 */
	@Override
	protected List<GenericJSON> doEnhance(String queryType, GenericJSON inputJSON, List<GenericJSON> exportJSON) {
		return exportJSON;
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.exporter.ExportDataEnhancerTemplate#doEnhance(com.cariq.www.utils.GenericJSON, java.util.List)
	 */
	@Override
	protected List<GenericJSON> doEnhance(String queryType, GenericJSON inputJSON, GenericJSON previous, List<GenericJSON> exportJSON) {
		return exportJSON;
	}
}
