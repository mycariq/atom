package com.cariq.toolkit.serviceimpl;

import org.springframework.stereotype.Service;

import com.atom.www.json.DrivingScoreJSON;
import com.cariq.toolkit.service.ScoreEngineService;
import com.cariq.toolkit.serviceimpl.async.WIHelper;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.utils.scoring.ScoreDefinition;
import com.cariq.toolkit.utils.scoring.Scorer;
import com.cariq.toolkit.utils.scoring.ScoringEngine;

/**
 * The Class ScoreEngineServiceImpl.
 *
 * @author nitin
 */
@Service
public class ScoreEngineServiceImpl implements ScoreEngineService {

	/** The logger. */
	private static CarIQLogger logger = WIHelper.getLogger("ScoreEngineServiceImpl");

	/** The Constant IDLING. */
	private static final String DRIVING_SCORE = "DrivingScore", TOTAL_DRIVE = "TotalDriving",
			RASH_BRAKING = "RashBraking", RASH_ACCELERATION = "RashAcceleration", NIGHTLY_DRIVE = "NightDriving",
			OVERSPEEDING = "OverSpeedingPercent", IDLING = "Idling";

	/* (non-Javadoc)
	 * @see com.cariq.www.interfaces.ScoreEngineService#getDrivingScore(com.cariq.www.json.DrivingScore_In)
	 */
	@Override
	public Double getDrivingScore(DrivingScoreJSON params) {
		try (ProfilePoint getDrivingScore = ProfilePoint.profileAction("getDrivingScore")) {
			ScoringEngine engine;
			try {
				logger.debug("getting instance of scoring engine");
				engine = ScoringEngine.getInstance();
			} catch (Exception e) {
				e.printStackTrace();
				logger.debug("Exception while getting scoring engine instance");
				return 0.0;
			}
			Scorer scorer = new Scorer(engine.getScoreDefinition(DRIVING_SCORE));
			Double totalDrive = params.getDistance();
			// less than 1 km travel -no score
			if (totalDrive == null || totalDrive <= 1)
				return 0.0;

			scorer.addParameter(TOTAL_DRIVE, totalDrive);

			Double nightlyDrive = params.getTravelTimeInNight();
			if (nightlyDrive != null && nightlyDrive > 1) {// less than 1 km nightly travel - don't add
				scorer.addParameter(NIGHTLY_DRIVE, nightlyDrive);
			}

			scorer.addParameter(RASH_ACCELERATION, params.getRashAccelerationCount());
			scorer.addParameter(RASH_BRAKING, params.getRashBrakingCount());
			scorer.addParameter(OVERSPEEDING, params.getOverSpeedingPercent());
			scorer.addParameter(IDLING, params.getIdlingPercent());

			return scorer.evaluate();
		}
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.interfaces.ScoreEngineService#getDrivingScoreDefinition()
	 */
	@Override
	public ScoreDefinition getDrivingScoreDefinition() {
		try (ProfilePoint getDrivingScoreDefinition = ProfilePoint.profileAction("getDrivingScoreDefinition")) {
			logger.debug("getting instance of scoring engine");
			
			// get instance of scoring engine.
			ScoringEngine engine = null;
			try {
				logger.debug("getting instance of scoring engine");
				engine = ScoringEngine.getInstance();
			} catch (Exception e) {
				e.printStackTrace();
				logger.debug("Exception while getting scoring engine instance");
			}
			Utils.checkNotNull(engine, " Scoring Engine not exist.");
			logger.debug("getting driving score definition");
			// get score definition
			ScoreDefinition scoreDefinition = engine.getScoreDefinition(DRIVING_SCORE);
			Utils.checkNotNull(scoreDefinition, "Driving Score definition does not exist.");
			
			return scoreDefinition;
		}
	}
}