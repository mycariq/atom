package com.cariq.toolkit.utils.analytics.application;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;

import com.cariq.toolkit.coreiq.tableUtil.application.mis.MISProcessingContext;
import com.cariq.toolkit.coreiq.tableUtil.application.mis.MISReport;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.analytics.core.AnalyticsContext;
import com.cariq.toolkit.utils.analytics.core.Column;
import com.cariq.toolkit.utils.analytics.core.Record;
import com.cariq.toolkit.utils.analytics.core.RecordProcessorTemplate;
import com.cariq.toolkit.utils.jsonProcessor.JSONObjectBuilder;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.google.common.base.Strings;

/**
 * The Class SystemPerformanceMatrixReporter.
 * 
 * @author santosh
 */
public class SystemPerformanceMatrixReporter extends RecordProcessorTemplate {

	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("SystemPerformanceMatrixReporter");

	public static final String SYSTEM_PERFORMANCE_MATRIX_JSON_PATH = "SystemPerformanceMatrixJsonPath";
	public static final String SYSTEM_PERFORMANCE_MATRIX_JSON = "SystemPerformanceMatrixJSON";

	private int dummyCount = 0;

	// Months constants
	private static final String[] MONTHS_MMM = new String[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
			"Sep", "Oct", "Nov", "Dec" };

	// Columns constants
	private static final String CARIQ_ENTRY_MONTH = "CarIQ Entry Month";
	private static final String COMPLETED_ONE_YEAR = "CompletedOneYear";
	private static final String CITY_TIER = "City Tier";
	private static final String POLICY_NUMBER = "Policy Number";
	private static final String POLICY_EXPIRY_DATE = "Policy Expiry Date";
	private static final String SHIPPED_BUT_NOT_CONNECTED = "ShippedButNotConnected";
    private static final String POLICY_STAGE = "Policy Stage";
    private static final String STAGE = "stage";
    private static final String CONNECTED = "Connected";

	private static final String NA = "NA";

	private MISReport report;

	public SystemPerformanceMatrixReporter(AnalyticsContext context) {
		super(context);
	}

	/**
	 * Keep only the relevant columns - don't get rest
	 */
	@Override
	public void init() throws IOException {
		super.init();

		//Init the MISReport table and add to the context
		String jsonPath = getProcessorContext().get(SYSTEM_PERFORMANCE_MATRIX_JSON_PATH).toString();
		if (Strings.isNullOrEmpty(jsonPath))
			Utils.handleException(
					"SystemPerformanceMatrixReporter.init(): Failed to get " + SYSTEM_PERFORMANCE_MATRIX_JSON_PATH);

		JSONObjectBuilder builder = new JSONObjectBuilder(jsonPath);

		report = new MISReport();
		logger.debug("===> registering builder json");
		builder.register(report.getJSONLoadable());
		logger.debug("===> processing builder json");
		builder.process();
	}

	@Override
	public void close() {
		GenericJSON outputJSON = report.toGenericJSON();
		getProcessorContext().put(SYSTEM_PERFORMANCE_MATRIX_JSON, Utils.getJSonString(outputJSON));
		logger.debug("===> Skipped Dummy policies from TAT: " + dummyCount);
		//logger.debug("===> OutputJSON: " + Utils.getJSonString(outputJSON));
	}

	/**
	 * Copy the contents of the record for relevant columns in the output Later,
	 * investigate specific values (e.g. speed etc.)
	 */
	@Override
	protected Record doProcess(Record record) {
		try (ProfilePoint _doProcess = ProfilePoint.profileAction("SystemPerformanceMatrixReporter_doProcess")) {
			// Placeholder - need a better pattern
			MISProcessingContext context = new MISProcessingContext();

			GenericJSON recordJSON = new GenericJSON();

			// Skip dummy policies
			String policyNumber = record.getStringValue(new Column(POLICY_NUMBER));
			if (!Strings.isNullOrEmpty(policyNumber) && policyNumber.toLowerCase().contains("dummy")) {
				dummyCount++;
				return null;
			}

			// Update the record
			record = updateRecord(record);

			// Update the report
			recordJSON.putAll(record.jsonize());
			report.process(context, recordJSON);

			return record;
		}
	}

	private Record updateRecord(Record record) {
		record = replaceMonthValue(record, new Column(CARIQ_ENTRY_MONTH));
		record = replaceNullValue(record, new Column(CITY_TIER));
		record.append(COMPLETED_ONE_YEAR, isCompletedOneYear(record));
		record.append(STAGE, getConnectedStatus(record, new Column(POLICY_STAGE)));

		return record;
	}
	
	private String getConnectedStatus(Record record, Column col) {
		String policyStage = record.getStringValue(col);
		if (Strings.isNullOrEmpty(policyStage))
			return NA;

		if (policyStage.equalsIgnoreCase("Car Connected"))
			return CONNECTED;

		if (policyStage.equalsIgnoreCase("Device OnBoarded") || policyStage.equalsIgnoreCase("Device Delivered")
				|| policyStage.equalsIgnoreCase("Device Shipped"))
			return SHIPPED_BUT_NOT_CONNECTED;

		return NA;
	}

	private Record replaceNullValue(Record record, Column col) {
		String value = record.getStringValue(col);
		if (Strings.isNullOrEmpty(value))
			record.set(col, NA);

		return record;
	}

	private String isCompletedOneYear(Record record) {
		String dateValue = record.getStringValue(new Column(POLICY_EXPIRY_DATE));
		if (Strings.isNullOrEmpty(dateValue))
			return "no";

		Date expiryDate = Utils.parseDate(dateValue);
		if (null == expiryDate)
			return "no";

		// Check iF the policy has completed 1 year in last month
		Date prevMonthDate = Utils.getDaysBefore(new Date(), 30);
		if (prevMonthDate.getMonth() == expiryDate.getMonth() && prevMonthDate.getYear() == expiryDate.getYear())
			return "yes";

		return "no";
	}

	private Record replaceMonthValue(Record record, Column col) {
		String value = record.getStringValue(col);

		if (Strings.isNullOrEmpty(value))
			return record;

		if (value.equalsIgnoreCase("Not Available-Not Available")) {
			record.set(col, NA);
		} else if (value.indexOf('/') > 0) { // Replace values like 02/20 to Feb-20
			String newValue = value.replace('/', '-');
			int index = Integer.parseInt(newValue.split("-")[0]);
			int year = Integer.parseInt(newValue.split("-")[1]);
			record.set(col, MONTHS_MMM[index - 1] + "-" + year);
		} else if (value.indexOf('-') > 0) { // Replace values like February-2020 to Feb-20
			if (Arrays.toString(MONTHS_MMM).contains(value.substring(0, 3))) { // Specifically check if value has month
				String[] parts = value.split("-");
				String monthVal = parts[0].substring(0, 3) + "-" + parts[1].substring(2, 4);
				record.set(col, monthVal);
			}
		}

		return record;
	}
}
