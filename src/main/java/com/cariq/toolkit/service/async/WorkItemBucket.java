package com.cariq.toolkit.service.async;

import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.model.async.WorkItem;
import com.cariq.toolkit.utils.Utils;

public class WorkItemBucket {
	String type;
	int size;
	int capacity;
	List<WorkItem> bucketItems = new ArrayList<WorkItem>();

	public WorkItemBucket(String type, int size) {
		super();
		this.type = type;
		this.size = size;
		this.capacity = size;
	}

	public String getType() {
		return type;
	}

	public int getCapacity() {
		return capacity;
	}

	public List<WorkItem> getBucketItems() {
		return bucketItems;
	}

	public WorkItemBucket putWorkItem(WorkItem workItem) {
		bucketItems.add(workItem);
		return this;
	}

	// Remaining capacity of the bucket
	public int getRemainingCapacity() {
		int remaining = capacity - bucketItems.size();
		Utils.checkConditional(remaining < 0, "WorkItemBucket filled beyond capacity of " + capacity);
		return remaining;
	}

	public boolean isFull() {
		return getRemainingCapacity() <= 0;
	}

	public boolean isEmpty() {
		return bucketItems.isEmpty();
	}

	// Try putting work item in the bucket
	public boolean tryPutWorkItem(WorkItem item) {
		// can't add if it's full
		if (isFull())
			return false;

		if (!isApplicableType(item.getType()))
			return false;
		
		// Add work item to the bucket
		putWorkItem(item);
		return true;
	}
	
	public boolean tryDecrementCapacity(String forType) {
		// can't add if it's full
		if (isFull())
			return false;

		if (!isApplicableType(forType))
			return false;

		capacity--;
		return true;
	}
	
	public int tryDecrementCapacity(String forType, int decrementCount) {
		int successfulDecementCount = 0;
		for (int i = 0; i < decrementCount; i++) {
			if (tryDecrementCapacity(forType))
				successfulDecementCount++;
		}
		
		return successfulDecementCount;
	}

	public boolean isApplicableType(String other) {
		// Anything can go to ANY or NOTA
		return (type.equalsIgnoreCase(WorkItem.NOTA) || type.equalsIgnoreCase(other));
	}

	@Override
	public String toString() {
		return "WorkItemBucket [type=" + type + ", size=" + size + ", capacity=" + capacity + "]";
	}
}
