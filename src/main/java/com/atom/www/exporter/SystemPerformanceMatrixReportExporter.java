/**
 * 
 */
package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.core.io.ClassPathResource;

import com.atom.www.helper.MISReportHelper;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.ExportedData;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.analytics.application.SystemPerformanceMatrixReporter;
import com.cariq.toolkit.utils.analytics.core.AnalyticsContext;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsCookbook;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsProcess;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsRecipe;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.google.common.base.Strings;

/**
 * The Class SystemPerformanceMatrixReportExporter.
 *
 * @author santosh
 */
public class SystemPerformanceMatrixReportExporter extends CarIQExporterTemplate {

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("SystemPerformanceMatrixReportExporter");

	/** The Constant EXPORTER. */
	public static final String EXPORTER = "SystemPerformanceMatrixReportExporter";

	MISReportHelper reportHelper = new MISReportHelper();

	public static final String FROM_TAT_URL = "FROM_TAT_URL", FROM_TAT_FILE = "FROM_TAT_FILE",
			FROM_LATEST_TAT_URL = "FROM_LATEST_TAT_URL";

	public static final String TAT_FILE_URL = "TATFileURL", TAT_FILE_PATH = "TATFilePath";

	public static final String SYSTEM_PERFORMANCE_MATRIX_REPORT_BUILDER_JSON = "json/SystemPerformanceMatrixReportBulilder.json";
	public static final String SYSTEM_PERFORMANCE_MATRIX_REPORT_RECIPE = "SystemPerformanceMatrixReportRecipe";
	public static final String SYSTEM_PERFORMANCE_MATRIX_REPORT = "SystemPerformanceMatrixReport";

	public SystemPerformanceMatrixReportExporter() {
		super(EXPORTER, "System Performance Matrix Report Exporter");

		addQueryType(FROM_TAT_URL, "System Performance Matrix Report from given TAT report URL",
				Arrays.asList(new QueryDefinitionAttribute(TAT_FILE_URL, "TATReport URL")));

		addQueryType(FROM_LATEST_TAT_URL, "Generates System Performance Matrix Report from latest TAT report URL",
				new String[] {});

		// Support roles
		addSupportedRoles(Arrays.asList(Utils.ADMIN_ROLE));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate#doExport(java.lang.
	 * String, com.cariq.toolkit.utils.GenericJSON, int, int)
	 */
	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		try (ProfilePoint _doExport = ProfilePoint.profileAction("SystemPerformanceMatrixReportExporter_doExport")) {
			String tatFileURL = null;

			// Ignore pageNo, ignorepageSize, just return alerts of each type
			if (pageNo > 1)
				return null;

			logger.debug("doExport - queryType : " + queryType + " pageNo : " + pageNo + " pageSize : " + pageSize
					+ " json :" + json.toString());

			if (queryType.equalsIgnoreCase(FROM_TAT_URL)) {
				tatFileURL = Utils.getValue(String.class, json, TAT_FILE_URL);
			} else if (queryType.equalsIgnoreCase(FROM_LATEST_TAT_URL)) {
				ExportedData exportedData = ExportedData.getLatestExportedData(SupportTATReport.TheExporter,
						CarIQFileUtils.CSV);
				if (null != exportedData)
					tatFileURL = exportedData.getFileUrl();
			} else
				Utils.handleException("QueryType " + queryType + " is not supported!!");

			Utils.checkNotNullOrEmpty(tatFileURL, "file url should not be empty");

			return getSystemPerformanceMatrixReport(queryType, json, tatFileURL);
		}
	}

	private List<GenericJSON> getSystemPerformanceMatrixReport(String queryType, GenericJSON json, String fileURL) {
		try (ProfilePoint _getSystemPerformanceMatrixReport = ProfilePoint
				.profileAction("SystemPerformanceMatrixReportExporter_getSystemPerformanceMatrixReport")) {
			List<GenericJSON> retList = new ArrayList<GenericJSON>();
			logger.debug("input json for report:>> " + json);
			reportHelper.configureDomainUser(json);

			String localFilePath = CarIQFileUtils.getLocalFilePathFromURL(fileURL);
			if (Strings.isNullOrEmpty(localFilePath) || !CarIQFileUtils.exists(localFilePath))
				Utils.handleException("Invalid report file: " + localFilePath);

			// Process the TimeLine CSV and get the report JSON 
			String reportJSONStr = analyzeReport(localFilePath);

			// Convert processed JSON to local HTML file
			String fileName = SYSTEM_PERFORMANCE_MATRIX_REPORT + "-" + Utils.createDateId(true) + ".html";
			String htmlFilePath = reportHelper.convertJSONToHTMLFile(reportJSONStr, fileName, null);
			if (!CarIQFileUtils.exists(htmlFilePath))
				Utils.handleException("Failed to create HTML report from processed JSON");

			// upload and persist HTML file
			GenericJSON outJSON = reportHelper.exportHTMLReport(SYSTEM_PERFORMANCE_MATRIX_REPORT, queryType,
					htmlFilePath, fileName, json.toString());
			// reportHelper.exportCompressedHTMLReport(SYSTEM_PERFORMANCE_MATRIX_REPORT, queryType, htmlFilePath, fileName, json.toString());

			retList.add(outJSON);
			return retList;
		}
	}

	private String analyzeReport(String loadMatrixFile) {
		try (ProfilePoint _analyzeReport = ProfilePoint.profileAction("SystemPerformanceMatrixReportExporter_analyzeReport")) {
			String outJSONString = null;

			try {
				String builderFilePath = new ClassPathResource(SYSTEM_PERFORMANCE_MATRIX_REPORT_BUILDER_JSON).getFile()
						.getAbsolutePath();

				AnalyticsContext ctx = new AnalyticsContext();
				ctx.put(SystemPerformanceMatrixReporter.SYSTEM_PERFORMANCE_MATRIX_JSON_PATH, builderFilePath);
				AnalyticsRecipe recipe = AnalyticsCookbook.getInstance()
						.getRecipe(SYSTEM_PERFORMANCE_MATRIX_REPORT_RECIPE);

				AnalyticsProcess process = new AnalyticsProcess(recipe, ctx, loadMatrixFile);
				process.execute();
				// get output JSON
				outJSONString = process.getContext().get(SystemPerformanceMatrixReporter.SYSTEM_PERFORMANCE_MATRIX_JSON)
						.toString();
				//close
				process.close();
			} catch (Exception e) {
				Utils.handleException(e);
			}

			return outJSONString;
		}
	}
}
