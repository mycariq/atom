package com.cariq.toolkit.utils.analytics.application;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cariq.toolkit.model.Billable;
import com.cariq.toolkit.model.Billable.Billable_In;
import com.cariq.toolkit.model.BillingTerm;
import com.cariq.toolkit.model.InvoiceUnit;
import com.cariq.toolkit.model.InvoiceUnit.InvoiceUnitStatusEnum;
import com.cariq.toolkit.model.InvoicedBillable;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Month;
import com.cariq.toolkit.utils.ObjectProcessorTemplate;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.google.common.base.Strings;

public class BillingTermProcessor extends ObjectProcessorTemplate<BillingTerm, GenericJSON> {
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("BillingTermProcessor");
	
	public static final String TAT_COL_CARIQ_ENTRY_MONTH = "CarIQ Entry Month";
	public static final String TAT_COL_CAR_CONNECTION_MONTH = "ConnectionMonth";
	public static final String TAT_COL_DEVICE_SHIPPING_MONTH = "ShippingMonth";
	public static final String TAT_COL_CAR_REGISTRATION_NUMBER = "Car Registration Number";
	public static final String TAT_COL_CITY_TIER = "City Tier";
	public static final String TAT_COL_POLICY_NUMBER = "Policy Number";
	public static final String TAT_COL_POLICY_ID = "PolicyId";
	public static final String TAT_COL_SHIPMENT_STATUS = "Shipment_Status";
	public static final String TAT_COL_POLICY_STAGE = "Policy Stage";
	public static final String TAT_COL_CAR_KMSDRIVEN = "KmsDriven";
	public static final String TAT_COL_DEVICE_ID  = "Device Id";
	public static final String TAT_COL_CAR_ID  = "CarId";
	
	
	public static final String TAT_COL_VALUE_DELIVERED = "Delivered";
	public static final String TAT_COL_VALUE_TIER_A = "Tier-A";
	public static final String TAT_COL_VALUE_NA = "NA";
	public static final String TAT_COL_VALUE_UNKNOWN = "UNKNOWN";
	public static final String TAT_COL_VALUE_FIELD_ASSISTANCE = "Field Assistance";
	public static final String TAT_COL_VALUE_CAR_CONNECTED = "Car Connected";
	
	public static final String BILLABLE_TYPE = "POLICY";
	public static final String BILLABLE_INFO_CARIQ_ENTRY_MONTH = "EntryMonth";
	public static final String BILLABLE_INFO_CAR_CONNECTION_MONTH = "ConnectionMonth";
	public static final String BILLABLE_INFO_DEVICE_SHIPPING_MONTH = "ShippingMonth";
	public static final String BILLABLE_INFO_CAR_REGISTRATION_NUMBER = "CarRegNo";
	public static final String BILLABLE_INFO_CITY_TIER = "CityTier";
	public static final String BILLABLE_INFO_POLICY_STAGE = "PolicyStage";
	public static final String BILLABLE_INFO_KMS_DRIVEN = "KmsDriven";
	public static final String BILLABLE_INFO_DEVICE_ID = "DeviceId";
	public static final String BILLABLE_INFO_CAR_ID = "CarId";

	public static final String BILLING_TERM_DEF_ANCHOR_MONTH_KEY = "AnchorMonth";
	public static final String BILLING_TERM_DEF_PROCESSORCLASS_KEY = "ProcessorClass";
	
	protected InvoiceUnit invoiceUnit = null;
	protected Integer numberOfInvoicedBillables = 0;
	
	protected String anchorMonthKey = null;
	protected GenericJSON defJSON = null;
	
	// Complex billing terms can implement this method further
	protected void validateFinishingCriteria() {};
	
	public BillingTermProcessor(BillingTerm bt, InvoiceUnit iu) {
		super(bt);
		invoiceUnit = iu;
		
		// Get the anchor month from definition json
		defJSON = GenericJSON.fromString(getModel().getDefinitionJson());
		Utils.checkNotNull(defJSON, "Invalid definition for billing term: " + bt.getName());
		
		anchorMonthKey = Utils.getValue(defJSON, BILLING_TERM_DEF_ANCHOR_MONTH_KEY);
		if (Strings.isNullOrEmpty(anchorMonthKey))
		   Utils.handleException("Invalid anchorMonth for billing term: " + bt.getName());
		logger.debug("===> anchorMonthKey: " + anchorMonthKey);
	}
	
	protected boolean isSpecialConditionKey(String btKey) {
		if (btKey.equalsIgnoreCase(BILLING_TERM_DEF_ANCHOR_MONTH_KEY)
				|| btKey.equalsIgnoreCase(BILLING_TERM_DEF_PROCESSORCLASS_KEY))
			return true;
		return false;
	}
	
	protected boolean matchKeyValue(String btKey, GenericJSON inputJSON) {
		// Just match the key-value
		List<String> btValues = new ArrayList<String>();
		
		if (null != Utils.getValue(List.class, defJSON, btKey)) {
			btValues = Utils.getValue(List.class, defJSON, btKey);
		} else if (null != Utils.getValue(String.class, defJSON, btKey)) {
			btValues.add(Utils.getValue(String.class, defJSON, btKey));
		} else
			Utils.handleException("Can not handle value in the JSON: " + defJSON.get(btKey));
		
		String recordValue = Utils.getValue(inputJSON, btKey);
		if (! btValues.contains(recordValue)) {
//			logger.debug("***Mismatch policy: " + Utils.getValue(inputJSON, TAT_COL_POLICY_NUMBER) 
//				+ ", btKey:" + btKey + ", btValues: " + btValues.toString() + ", recordValue: " + recordValue);
			return false;
		}
		
		//logger.debug("***BTKey: " + btKey + " matched for policy: " + Utils.getValue(inputJSON, TAT_COL_POLICY_NUMBER));
		return true;
	}
	
	// Complex billing terms can override this method
	protected boolean isRecordApplicableForBillingTerm(GenericJSON inputJSON) {		
		// Match all the required keys in definition with input record 
		for (String btKey : defJSON.keySet()) {
			if (isSpecialConditionKey(btKey))
				continue;
			
			if (!matchKeyValue(btKey, inputJSON))
				return false;
		}
		
		numberOfInvoicedBillables += 1;		
		//logger.debug("***Matched policy: " + Utils.getValue(inputJSON, TAT_COL_POLICY_NUMBER));
		return true;
	}
	
	private boolean isFieldAssistanceProvided(GenericJSON inputJSON) {
		String shipping_month = null, tier=null, shipment_status=null;
		shipping_month = Utils.getValue(inputJSON,TAT_COL_DEVICE_SHIPPING_MONTH);
		tier = Utils.getValue(inputJSON,TAT_COL_CITY_TIER);
		shipment_status = Utils.getValue(inputJSON, TAT_COL_SHIPMENT_STATUS);
		//logger.debug("***===> shipping_month: " + shipping_month + ", tier: " + tier + ", shipment_status: " + shipment_status) ;
		if ((!Strings.isNullOrEmpty(shipping_month)  && shipping_month.equalsIgnoreCase(TAT_COL_VALUE_NA))
			&& (!Strings.isNullOrEmpty(tier) && tier.equalsIgnoreCase(TAT_COL_VALUE_TIER_A))
			&& (!Strings.isNullOrEmpty(shipment_status) && shipment_status.equalsIgnoreCase(TAT_COL_VALUE_DELIVERED)))
			return true;

		return false;
	}
	
	private GenericJSON getInfoFromRecord(GenericJSON inputJSON) {
		DecimalFormat df = new DecimalFormat("0.00");
		GenericJSON json = new GenericJSON();
		
		json.add(BILLABLE_INFO_CARIQ_ENTRY_MONTH, Utils.getValue(inputJSON,TAT_COL_CARIQ_ENTRY_MONTH));
		json.add(BILLABLE_INFO_CAR_CONNECTION_MONTH, Utils.getValue(inputJSON,TAT_COL_CAR_CONNECTION_MONTH));
		/* TODO: Remove this check completely, if not required in future
		 * if (isFieldAssistanceProvided(inputJSON))
			json.add(BILLABLE_INFO_DEVICE_SHIPPING_MONTH, TAT_COL_VALUE_FIELD_ASSISTANCE);
		else*/
		json.add(BILLABLE_INFO_DEVICE_SHIPPING_MONTH, Utils.getValue(inputJSON,TAT_COL_DEVICE_SHIPPING_MONTH));
		json.add(BILLABLE_INFO_CITY_TIER, Utils.getValue(inputJSON,TAT_COL_CITY_TIER));
		
		String carRegNo = Utils.getValue(inputJSON, TAT_COL_CAR_REGISTRATION_NUMBER);
		if (Strings.isNullOrEmpty(carRegNo) || carRegNo.toUpperCase().contains(TAT_COL_VALUE_UNKNOWN))
			json.add(BILLABLE_INFO_CAR_REGISTRATION_NUMBER, TAT_COL_VALUE_UNKNOWN);
		else
			json.add(BILLABLE_INFO_CAR_REGISTRATION_NUMBER, carRegNo);
		
		json.add(BILLABLE_INFO_POLICY_STAGE, Utils.getValue(inputJSON, TAT_COL_POLICY_STAGE));	
		if(Utils.getValue(inputJSON, TAT_COL_POLICY_STAGE).equalsIgnoreCase(TAT_COL_VALUE_CAR_CONNECTED)) {
			if (null != Utils.getValue(Double.class, inputJSON, TAT_COL_CAR_KMSDRIVEN))
				json.add(BILLABLE_INFO_KMS_DRIVEN, df.format(Utils.getValue(Double.class, inputJSON, TAT_COL_CAR_KMSDRIVEN)));
			else
				json.add(BILLABLE_INFO_KMS_DRIVEN, "0.00");
		} else 
			json.add(BILLABLE_INFO_KMS_DRIVEN, TAT_COL_VALUE_NA);
		json.add(BILLABLE_INFO_CAR_ID, Utils.getValue(inputJSON, TAT_COL_CAR_ID));
		return json;
	}
	
	private Billable getBillable(GenericJSON inputJSON) {
		String policyNumber = Utils.getValue(inputJSON, TAT_COL_POLICY_NUMBER);
		Long policyId = Strings.isNullOrEmpty(Utils.getValue(inputJSON, TAT_COL_POLICY_ID)) ? null : Long.parseLong(Utils.getValue(inputJSON, TAT_COL_POLICY_ID));
		GenericJSON infoJSON = getInfoFromRecord(inputJSON);
		Billable_In billableIn = new Billable_In(BILLABLE_TYPE, policyId, policyNumber, infoJSON.toString(), null);
		return Billable.getOrCreateBillable(billableIn);
	}
	
	private boolean isBillableInvoicedAlreadyForBillingTerm(Billable billable) {
		List<InvoicedBillable> ibList = InvoicedBillable.getByBillable(billable);
		for (InvoicedBillable ib : ibList) {
			// Check its invoice unit is based on same billing term and its status is paid or ready.
			if(getModel() == ib.getItsInvoiceUnit().getItsBillingTerm()) {
				InvoiceUnit iu= ib.getItsInvoiceUnit();
				String status = iu.getStatus();
				if (InvoiceUnitStatusEnum.READY.toString().equalsIgnoreCase(status) ||
						InvoiceUnitStatusEnum.PAID.toString().equalsIgnoreCase(status))
					return true;
			}
		}
		return false;
	}
	
	private boolean isRecordEligibleForIinvoiceUnit(GenericJSON inputJSON) {
		String anchorMonthValue = Utils.getValue(inputJSON, anchorMonthKey); // eg: April-2020
		Month anchorMonth = new Month(anchorMonthValue, Utils.INDIA_TIME_ZONE);
		Month billingMonth = new Month(invoiceUnit.getBillingMonth(), Utils.INDIA_TIME_ZONE);
		if ((billingMonth.getMonth() == anchorMonth.getMonth()) &&  (billingMonth.getYear() == anchorMonth.getYear()))
			return true;
		
		return false;
	}

	@Override
	public void doStartProcess(GenericJSON inputJSON) {
		try (ProfilePoint _doStartProcess = ProfilePoint.profileAction("ProfAction__doStartProcess_" + this.getClass().getName())) {
			String policyNumber = Utils.getValue(inputJSON, TAT_COL_POLICY_NUMBER);
			if (null == policyNumber)
				Utils.handleException("!!! Invalid record to process: " + inputJSON.toString());
			
			// Check if the record eligible for invoice unit
			if (! isRecordEligibleForIinvoiceUnit(inputJSON)) {
				//logger.debug("*** Record " + policyNumber + " not eligible for parent IU");
				return;
			}
			
			// Get billable for record
			Billable billable = getBillable(inputJSON);
			
			// If billable is already invoiced/paid for same billing term (under different IU) then discard processing
			if (isBillableInvoicedAlreadyForBillingTerm(billable)) {
				//logger.debug("*** Billable " + policyNumber + " is already invoiced for billing term: " + getModel().getName());
			} else // Check eligibility
				if (isRecordApplicableForBillingTerm(inputJSON)) {
					// Create invoiced_billable
					InvoicedBillable ib = new InvoicedBillable(invoiceUnit, billable, getModel().getCost(), new Date(), new Date());
					ib.persist();
					//logger.debug("==> Created InvoicedBillable: " + ib.getId());
				} else {
					//logger.debug("==> Billable " + policyNumber + " not eligible for BT: " + getModel().getId());
				}
		}
	}
	
	@Override
	public void doStartFinish() {
		try (ProfilePoint _doStartFinish = ProfilePoint.profileAction("ProfAction__doStartFinish_" + this.getClass().getName())) {
			if (numberOfInvoicedBillables <= 0)
				throw new RuntimeException("NO_BILLABLES");
			
			validateFinishingCriteria();
		}
	}
}
