package com.cariq.toolkit.utils.wwwservice.core.condition;

import com.cariq.toolkit.utils.wwwservice.core.Condition.ConditionType;
import com.cariq.toolkit.utils.wwwservice.core.LiteralCondition;
import com.cariq.toolkit.utils.wwwservice.core.ParameterDef;

public class Contains extends LiteralCondition {
	public Contains(ParameterDef paramdef, Object value) {
		super(paramdef, value);
	}

	@Override
	public ConditionType getType() {
		return ConditionType.Contains;
	}

	@Override
	protected boolean isReallyTrue(Object conditionValue, Object parameterValue) {
		String paramStr = parameterValue.toString();
		return paramStr.contains(conditionValue.toString());
	}
	
}
