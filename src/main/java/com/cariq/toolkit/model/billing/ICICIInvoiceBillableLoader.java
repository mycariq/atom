package com.cariq.toolkit.model.billing;

import com.cariq.toolkit.coreiq.loader.CarIQLoaderContext;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderTemplate;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.OptionPicker;
import com.cariq.toolkit.utils.OptionPicker.Option;
import com.cariq.toolkit.utils.Utils;

/**
 * Load Simple invoiced Billable into system
 * @author hrishi
 *
 * username, notificationType (Email, SMS, Push), String subject, String shortMessage (for SMS), String longMessage; 
 */

public class ICICIInvoiceBillableLoader extends CarIQLoaderTemplate {

	public static final String TheLoader = "ICICIInvoiceBillableLoader";

	// Attributes
	public static final String PolicyNumber = "PolicyNumber";
	public static final String Category = "Category";
	public static final String InvoiceNumber = "InvoiceNumber";
	
	// Constants
	public static final String ILAssistBillable = "ILASSIST";

	public static final OptionPicker Categories = new OptionPicker(new Option("Hardware-1", "Hardware 1", "Hardware1"),
																new Option("Hardware-2", "Hardware 2", "Hardware2"), 
																new Option("Software", "Software 0", "Software0"),
																new Option("Hardware-Full", "Hardware Full", "HardwareFull", "Hardware"),
																new Option("Software-1", "Software 1", "Software1"), 
																new Option("Software-2", "Software 2", "Software2"),
																new Option("Software-3", "Software 3", "Software3"), 
																new Option("Software-4", "Software 4", "Software4"));

	public ICICIInvoiceBillableLoader() {
		super(TheLoader, "Load ICICI Billables information against invoice and category");

		addAttribute(PolicyNumber, "Policy Number for which billed", false);
		addAttribute(Category, "Category under which this Policy is invoiced viz. Hardware-1, Hardware-2, Hardware-Full, Software, Software-1, Software-2 etc.", false);
		addAttribute(InvoiceNumber, "Invoice number that includes this", false);
	}

	@Override
	protected void doLoad(GenericJSON json, CarIQLoaderContext context) {
		String policyNumber = Utils.getValue(String.class, json, PolicyNumber);
		String category = Categories.match(Utils.getValue(String.class, json, Category));
		String invoiceNumber = Utils.getValue(String.class, json, InvoiceNumber);
		
		new SimpleInvoicedBillable(ILAssistBillable, policyNumber, invoiceNumber, category, null).persist();
	}	
}
