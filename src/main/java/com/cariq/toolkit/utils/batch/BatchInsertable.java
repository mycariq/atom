package com.cariq.toolkit.utils.batch;

// TODO: Auto-generated Javadoc
/**
 * The Interface BatchInsertable.
 */
public interface BatchInsertable {

	/**
	 * Persist db.
	 */
	public void persistDB();

	/**
	 * Persist batch.
	 *
	 * @param batch
	 *            the batch
	 */
	public void persistBatch(Batch batch);
	
	/**
	 * Merge to db.
	 */
	public void mergeDB();

	/**
	 * Merge to batch.
	 *
	 * @param batch
	 *            the batch
	 */
	public void mergeBatch(Batch batch);
}
