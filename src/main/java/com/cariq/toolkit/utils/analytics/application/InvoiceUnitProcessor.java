package com.cariq.toolkit.utils.analytics.application;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import com.cariq.toolkit.model.BillingTerm;
import com.cariq.toolkit.model.InvoiceUnit;
import com.cariq.toolkit.model.InvoiceUnit.InvoiceUnitStatusEnum;
import com.cariq.toolkit.model.InvoicedBillable;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.ObjectProcessorTemplate;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

public class InvoiceUnitProcessor extends ObjectProcessorTemplate<InvoiceUnit, GenericJSON> {
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("InvoiceUnitProcessor");

	public InvoiceUnitProcessor(InvoiceUnit iu) {
		super(iu);
	}
	
	private BillingTermProcessor getProcessorInstance(BillingTerm bt, InvoiceUnit iu) {
		try {
			GenericJSON defJSON = GenericJSON.fromString(bt.getDefinitionJson());
			Utils.checkNotNull(defJSON, "Invalid definition for billing term: " + bt.getName());
			String className = Utils.getValue(defJSON, BillingTermProcessor.BILLING_TERM_DEF_PROCESSORCLASS_KEY);
			Utils.checkNotNull(className, "Invalid processor class in billing term: " + bt.getName());
			logger.debug("*********** getting instance of: " + className);
			
			Class<?> pclass = Class.forName(className);
			Constructor<?> cons = pclass.getConstructor(BillingTerm.class, InvoiceUnit.class);

			BillingTermProcessor bp = (BillingTermProcessor) cons.newInstance(bt, iu);
			return bp;
			
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| SecurityException	| NoSuchMethodException | InvocationTargetException | ClassNotFoundException e) {
			logger.debug("********** exception: " + e.getMessage());
			Utils.handleException(e);
		}
		
		return null;
	}
	
	@Override
	public void doStartPrepare() {
		try (ProfilePoint _doStartPrepare = ProfilePoint.profileAction("ProfAction__doStartPrepare_" + this.getClass().getName())) {
			
			// Confirm billing term exists	
			BillingTerm bt = getModel().getItsBillingTerm();
			if (null == bt)
				Utils.handleException("Invalid billing term for invoice unit: " + getModel().getId());
			
			// Create billing term Processor for IU
			BillingTermProcessor btProcessor = getProcessorInstance(bt, getModel());
			//BillingTermProcessor btProcessor = new BillingTermProcessor(bt, getModel());
			addChildProcessor(btProcessor);
			logger.debug("*** Created btProcessor for iu: " + getModel().getId());
		}
	}
	
	@Override
	public void doEndFinish() {
		try (ProfilePoint _doEndFinish = ProfilePoint.profileAction("ProfAction__doEndFinish_" + this.getClass().getName())) {
			InvoiceUnit iu = getModel();
			// Get count of invoiced_billables and total amount for this IU.
			List<InvoicedBillable> ibList = InvoicedBillable.getByInvoiceUnit(iu);
			iu.setNumberOfBillables(Long.valueOf(ibList.size()));
			Long totalAmount = Long.valueOf(0);
			for (InvoicedBillable ib : ibList) {
				totalAmount += ib.getAmount();
			}
			iu.setAmount(totalAmount);
			
			if (totalAmount > 0) 
				iu.setStatus(InvoiceUnitStatusEnum.READY.toString());
			else
				iu.setStatus(InvoiceUnitStatusEnum.INVALID.toString());
			iu.persist();
			logger.debug("***Updated invoiceUnit: " + iu.getId() + ", status: " + iu.getStatus());
		}
	}
	
	@Override
	public void destroyModel() {
		try (ProfilePoint _destroyModel = ProfilePoint.profileAction("ProfAction__destroyModel_" + this.getClass().getName())) {
			logger.debug("***!!! InvoiceUnitProcessor.destroyModel()...");
			InvoiceUnit iu = getModel();
			// Get invoiced_billables 
			List<InvoicedBillable> ibList = InvoicedBillable.getByInvoiceUnit(iu);
			logger.debug("*** Deleting invoiced_billables for iu: " + iu.getId());
			for (InvoicedBillable ib : ibList) {
				ib.remove();
			}
			logger.debug("*** Deleting iu: " + iu.getId());
			iu.remove();
		}
	}
}
