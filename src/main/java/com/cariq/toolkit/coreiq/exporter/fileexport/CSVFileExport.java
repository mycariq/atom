/**
 * 
 */
package com.cariq.toolkit.coreiq.exporter.fileexport;

import java.io.File;
import java.util.List;

import com.cariq.toolkit.coreiq.exporter.CarIQExporter;
import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.ObjectFeeder;
import com.cariq.toolkit.utils.PagingBatchFeeder;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.analytics.core.AnalyticsHelper;

/**
 * @author hrishi
 *
 */
public class CSVFileExport extends FileExportTemplate {

	/**
	 * @param exporter
	 * @param queryType
	 * @param inputJSON
	 * @param fileFormat
	 */
	public CSVFileExport(CarIQExporter exporter, String queryType, GenericJSON inputJSON) {
		super(exporter, queryType, inputJSON, CarIQFileUtils.CSV);
	}
	
	private void export(final CarIQExporter exporter, final String queryType, final GenericJSON json, String outputFile) throws Exception {
		int BATCH_SIZE = 500;
		AnalyticsHelper.writeToCSVFile(outputFile,
				new ObjectFeeder<GenericJSON>(new PagingBatchFeeder<GenericJSON>(BATCH_SIZE) {
					@Override
					protected List<GenericJSON> getList(int pageNo, int pageSize) {
						return exporter.export(queryType, json, pageNo, pageSize);
					}
				}));
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.exporter.fileexport.FileExportTemplate#doExecute()
	 */
	@Override
	protected File doExecute() {
		// Prepare a temp file
		String csvExportFile = CarIQFileUtils
				.getTempFile(exporter.getName() + "-" + queryType + "-" + Utils.createDateId(true) + ".csv");

		try {
			export(exporter, queryType, inputJSON, csvExportFile);
			
			return new File(csvExportFile);
		} catch (Exception e) {
			Utils.handleException(e);
		}
		
		return null;		
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.exporter.fileexport.FileExport#getMimeType()
	 */
	@Override
	public String getMimeType() {
		return "text/csv";
	}

}
