package com.atom.www.helper;

/**
 * Different slots
 * 
 * @author sagar
 *
 */
public enum Slot {
	
	/**
	 * First half of day
	 */
	MORNING,
	
	/**
	 * Second half of day
	 */
	AFTERNOON,
	
	/**
	 * Third half of day
	 */
	EVENING
}

