package com.cariq.toolkit.coreiq.loader;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cariq.toolkit.utils.CarIQAttrDefinition;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.analytics.core.AnalyticsContext;
import com.cariq.toolkit.utils.analytics.core.Record;
import com.cariq.toolkit.utils.analytics.core.RecordProcessorTemplate;
import com.cariq.toolkit.utils.analytics.core.TableProcessor;
import com.cariq.toolkit.utils.analytics.core.TableProcessorImpl;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderDefinition;

@Service
public class CarIQLoaderServiceImpl implements CarIQLoaderService {
	static final String LOADER = "LOADER";
	static final String COUNT = "COUNT";

	/**
	 * Using Analytics Engine, process the input CSV file
	 * 
	 * @param ctx
	 * @param outputFile
	 * @param resourceType
	 * @param operation
	 * @param recordProcessorTemplate
	 * @return
	 * @throws Exception
	 */

	private int executeBatchProcessor(AnalyticsContext ctx, String inputFile, String loader, String outputFile,
			RecordProcessorTemplate recordProcessorTemplate) throws Exception {
		ctx.put(COUNT, 0);

		// Process the File using simple Analytics processor
		TableProcessor processor = new TableProcessorImpl(ctx, loader, inputFile, outputFile);

		processor.setForgiveException(false);
		processor.add(recordProcessorTemplate);
		processor.init();
		processor.process();
		processor.close();
		return (int) processor.getProcessorContext().get(COUNT);
	}

	@Override
	public List<CarIQLoaderDefinition> getLoaderDefinition() {
		Collection<CarIQLoader> loaders = CarIQLoaderRegistry.getAllLoaders();
		List<CarIQLoaderDefinition> returnList = new ArrayList<CarIQLoaderDefinition>();
		for (CarIQLoader loader : loaders) {
			// Get definition - and check if its hidden
			CarIQLoaderDefinition def = loader.getLoaderDefinition();
			if (null == def || CarIQLoaderDefinition.HIDDEN_CATEGORY.equals(loader.getLoaderDefinition().getCategory()))
				continue;
			
			returnList.add(def);
		}
		Collections.sort(returnList);
		
		return returnList;
	}

	@Override
	public List<CarIQAttrDefinition> getLoaderAttributes(String loaderName) {
		CarIQLoader loader = CarIQLoaderRegistry.getLoader(loaderName);
		return loader.getAttributes();
	}

	@Override
	public void loadSingle(String loaderName, GenericJSON json, CarIQLoaderContext context) {
		CarIQLoader loader = CarIQLoaderRegistry.getLoader(loaderName);
		Utils.checkNotNull(loader, "Loader with name: " + loaderName + " not Found!!");
		loader.load(json,context);

	}

	@Override
	public int load(String loaderName, File file,  final CarIQLoaderContext context) throws Exception {
		Utils.checkNotNull(CarIQLoaderRegistry.getLoader(loaderName), "Loader with name: " + loaderName + " not Found!!");

		AnalyticsContext ctx = new AnalyticsContext();
		ctx.put(LOADER, loaderName);

		return executeBatchProcessor(ctx, file.getAbsolutePath(), loaderName, null, new RecordProcessorTemplate(ctx) {
			@Override
			protected Record doProcess(Record record) {
				GenericJSON json = new GenericJSON();
				json.putAll(record.jsonize());

				loadSingle((String) getProcessorContext().get(LOADER), json, context);
				getProcessorContext().put(COUNT, (Integer) getProcessorContext().get(COUNT) + 1);
				return record;
			}
		});
	}

	@Override
	public CarIQLoaderDefinition getLoaderDefinition(String loaderName) {
		CarIQLoader loader = CarIQLoaderRegistry.getLoader(loaderName);
		Utils.checkNotNull(loader, "Loader with name: " + loaderName + " not Found!!");
		return loader.getLoaderDefinition();
	}

}
