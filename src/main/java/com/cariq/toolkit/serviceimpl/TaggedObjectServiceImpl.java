package com.cariq.toolkit.serviceimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.model.Tag;
import com.cariq.toolkit.model.Tag.Tag_In;
import com.cariq.toolkit.model.Tag.Tag_Out;
import com.cariq.toolkit.model.TaggedObject;
import com.cariq.toolkit.model.TaggedObject.TaggedObject_In;
import com.cariq.toolkit.model.TaggedObject.TaggedObject_Out;
import com.cariq.toolkit.service.TagService;
import com.cariq.toolkit.service.TaggedObjectService;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.service.CarIQRestClientService;


/**
 * The Class TaggedObjectServiceImpl.
 */
@Service
public class TaggedObjectServiceImpl implements TaggedObjectService {

	/** The rest service. */
	@Autowired
	CarIQRestClientService restService;
	
	@Autowired
	private TagService tagService;

	@Override
	@Transactional
	public ResponseWithIdJson tagObject(TaggedObject_In params) {
		TaggedObject taggedObject = null;
		Tag tag = null;
		try {
			// Validate ObjectId
			if (null == params.getObjectId() || params.getObjectId() <= 0) { 
				Utils.handleException("ObjectId " + params.getObjectId() + " is invalid");
			}
					
			// Obtain the tag
			Tag_In tagIn = new Tag_In(params.getTagName(), null);
			ResponseWithIdJson idJson = tagService.createOrGetTag(tagIn);
			if (null == idJson.getId() || 
				(null == (tag = Tag.findObjectById(Long.parseLong(idJson.getId()))))) {
				Utils.handleException("Failed to create or get tag: " + params.getTagName() 
					+ ", exception: " + idJson.getMessages());
			}
						
			taggedObject = new TaggedObject(tag, params.getObjectId(), params.getFqdnClass(), new Date(), new Date());
			taggedObject.persist();
			
		} catch (Exception e) {
			Utils.handleException("Failed to tag the object, exception: " + e.getMessage());
		}
		return new ResponseWithIdJson(taggedObject.getId().toString(), "ObjectId " + params.getObjectId() + " tagged with " + params.getTagName() + " successfully.");
	}
	
	@Override
	@Transactional
	public ResponseJson untagObject(TaggedObject_In params) {
		try {
			Tag tag = Tag.getByName(params.getTagName());
			if (tag == null) {
				Utils.handleException("Tag " + params.getTagName() + " is invalid");
			}
			
			if (null == params.getObjectId() || params.getObjectId() <= 0) { 
				Utils.handleException("ObjectId " + params.getObjectId() + " is invalid");
			}

			// FqdnClass names should not contain dots.
			if (params.getFqdnClass().contains(".")) {
				Utils.handleException("fqdnClass " + params.getObjectId() + " contains invalid characters '.'");
			}
			
			TaggedObject taggedObject = TaggedObject.getObject(tag, params.getObjectId(), params.getFqdnClass());
			if (null != taggedObject) {
				taggedObject.remove();
				return new ResponseJson("Tag " + params.getTagName() + " is removed from object " + params.getObjectId());
			}
		} catch (Exception e) {
			Utils.handleException("Failed to untag the object, exception " + e.getMessage());
		}
		return new ResponseJson("Object " + params.getObjectId() + " is not tagged with " + params.getTagName() + " already!");
	}
	
	@Override
	public List<Tag_Out> getTagsByFqdnObjectId(String fqdnClass, Long objectId) {
		List<Tag_Out> resultList = new ArrayList<>();
		try {
			if (null == fqdnClass) {
				Utils.handleException("fqdnClass is invalid");
			}
			if (null == objectId || 0 >= objectId) {
				Utils.handleException("objectId is invalid");
			}

			List<TaggedObject> taggedObjectlist = TaggedObject.getTagsByFqdnObjectId(fqdnClass, objectId);
			for (TaggedObject taggedObject : taggedObjectlist) {
				Tag tag = taggedObject.getTag();
				if (null != tag) {
					resultList.add(tag.toJSON());
				}
			}
		} catch(Exception e) {
			Utils.handleException("Failed to get tags assigned to object, exception: " + e.getMessage());
		}
		return resultList;
		
	}
	
	@Override
	public List<Tag_Out> getTagsByFqdnClass(String fqdnClass) {
		List<Tag_Out> resultList = new ArrayList<>();
		try {
			if (null == fqdnClass) {
				Utils.handleException("fqdnClass is invalid");
			}
			
			List<Tag> tagList = TaggedObject.getTagsByFqdn(fqdnClass);
			for (Tag tag : tagList) {
				resultList.add(tag.toJSON());
			}
		} catch(Exception e) {
			Utils.handleException("Failed to get tags used by class, exception: " + e.getMessage());
		}
		return resultList;
	}
	
	@Override
	public List<Long> getObjectIDsByFqdnTagname(String fqdnClass, String tagname) {
		List<Long> resultList = new ArrayList<>();
		try {
			if (null == fqdnClass) {
				Utils.handleException("fqdnClass " + fqdnClass + " is invalid");
			}
			
			Tag tag = Tag.getByName(tagname);
			if (null == tag) {
				Utils.handleException("Tag " + tagname + " is invalid");
			}
			
			List<TaggedObject> taggedObjectlist = TaggedObject.getObjectsByFqdnTag(fqdnClass, tag);
			for (TaggedObject taggedObject : taggedObjectlist) {
				resultList.add(taggedObject.getObjectId());
				//TODO: We should be able to get object JSONs generically
			}
		} catch(Exception e) {
			Utils.handleException("Failed to get objects (of specific class) tagged with specific tag, exception: " + e.getMessage());
		}
		return resultList;
	}
	
	@Override
	public List<TaggedObject_Out> getObjectsByTagname(String tagname) {
		List<TaggedObject_Out> resultList = new ArrayList<>();
		try {	
			Tag tag = Tag.getByName(tagname);
			if (null == tag) {
				Utils.handleException("Tag " + tagname + " is invalid");
			}
			
			List<TaggedObject> taggedObjectlist = TaggedObject.getObjectsByTag(tag);
			for (TaggedObject taggedObject : taggedObjectlist) {
				resultList.add(taggedObject.toJSON());
				//TODO: We should be able to get object JSONs generically
			}
		} catch(Exception e) {
			Utils.handleException("Failed to get objects (of various class) tagged with specific tag, exception: " + e.getMessage());
		}
		return resultList;
	}
}