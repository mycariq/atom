package com.cariq.toolkit.coreiq.server;

import com.cariq.toolkit.coreiq.server.Server;
import com.google.common.base.Strings;

public class ServerFlushRequest implements ServerSpecificRequest {

	private String flushScope = "All"; // Audit or ServerStats or All or null  - null is considered as All
	private String serverName = Server.ANY;
	
	public ServerFlushRequest() {}
	
	public ServerFlushRequest(String flushScope) {
		this(Server.ANY, flushScope);
	}

	public ServerFlushRequest(String serverName, String flushScope) {
		super();
		this.serverName = Strings.isNullOrEmpty(serverName) ? Server.ANY : serverName;
		this.flushScope = Strings.isNullOrEmpty(flushScope) ? "All" : flushScope;
	}

	public String getFlushScope() {
		return flushScope;
	}

	public void setFlushScope(String flushScope) {
		this.flushScope = flushScope;
	}

	@Override
	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}	
}
