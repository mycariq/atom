package com.cariq.toolkit.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cariq.toolkit.model.Tag.Tag_In;
import com.cariq.toolkit.model.Tag.Tag_Out;
import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.publicapi.CarIQPublicAPIParameter;
import com.cariq.toolkit.service.TagService;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.utils.CarIQMutableAPI;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;


/**
 * The Class TagController.
 */
@CarIQPublicAPI(name = "Tag", description = "Everything related to Tag")
@Controller
@RequestMapping("/tag")
public class TagController {

	/** The tagservice. */
	@Autowired
	private TagService tagservice;

	/**
	 * Creates or gets the tag if existing
	 *
	 * @param params
	 *            the params
	 * @return the response with id json
	 */
	@CarIQPublicAPIMethod(description = "Check if tag exists OR create a new one", responseClass = ResponseWithIdJson.class, internal = true)
	@RequestMapping(value = "/createOrGetTag", method = RequestMethod.POST)
	@ResponseBody
	public ResponseWithIdJson createOrGetTag(@RequestBody Tag_In params) {
		try (CarIQMutableAPI _createOrGetTag = new CarIQMutableAPI("CARIQ_createOrGetTag", params)) {
			return tagservice.createOrGetTag(params);
		}
	}


	/** Gets the tags with pagination.
	 *
	 * @return the tags
	 */
	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Get all tags", responseClass = Tag_Out.class, internal = true)
	@RequestMapping(value = "/getAll/{pageNo}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public List<Tag_Out> getAllTags(
			@CarIQPublicAPIParameter(name = "PageNo", description = "Page Number") @PathVariable int pageNo, 
			@CarIQPublicAPIParameter(name = "PageSize", description = "Page Size") @PathVariable int pageSize) {
		try (CarIQAPI _getTags = new CarIQAPI("CARIQ_getAllTags_with_pagination")) {
			return tagservice.getAllTags(pageNo, pageSize);
		}
	}
	
	/**
	 * Gets the tag.
	 *
	 * @param tagname
	 *            the tagname
	 * @return the tag
	 */
	@CarIQPublicAPIMethod(description = "Get signle tag by Tag Name", responseClass = Tag_Out.class, internal = true)
	@RequestMapping(value = "/get/{tagName}", method = RequestMethod.GET)
	@ResponseBody
	public Tag_Out getTag(
			@CarIQPublicAPIParameter(name = "TagName", description = "Tag Name") @PathVariable String tagName) {
		try (CarIQAPI _getTag = new CarIQAPI("CARIQ_getTag")) {
			return tagservice.getTag(tagName);
		}
	}

	/**
	 * Update tag.
	 *
	 * @param params
	 *            the params
	 * @param tagname
	 *            the tagName
	 * @return the response json
	 */
	@CarIQPublicAPIMethod(description = "Update tag", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/update/{tagName}", method = RequestMethod.PUT)
	@ResponseBody
	public ResponseJson updateTag(@RequestBody Tag_In params,
			@CarIQPublicAPIParameter(name = "TagName", description = "Tag Name") @PathVariable String tagName) {
		try (CarIQMutableAPI _updateTag = new CarIQMutableAPI("CARIQ_updateTag", params)) {
			return tagservice.updateTag(params, tagName);
		}
	}

	/**
	 * Removes the tag.
	 *
	 * @param tagName
	 *            the tagName
	 * @return the response json
	 */
	@CarIQPublicAPIMethod(description = "Remove tag", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/remove/{tagName}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseJson removeTag(
			@CarIQPublicAPIParameter(name = "TagName", description = "Tag Name") @PathVariable String tagName) {
		try (CarIQMutableAPI _removeTag = new CarIQMutableAPI("CARIQ_removeTag", tagName)) {
			return tagservice.deleteTag(tagName);
		}
	}
}