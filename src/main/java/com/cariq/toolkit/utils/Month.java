/**
 * 
 */
package com.cariq.toolkit.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * @author hrishi
 *
 */
public class Month {
	int month, year;
	String timeZone;

	/**
	 * @param month
	 * @param year
	 */
	public Month(int month, int year, String timeZone) {
		this.month = month;
		this.year = year;
		this.timeZone = timeZone;
	}

	public int getMonth() {
		return month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	/**
	 * @param month
	 * @param year
	 */
	public Month(String MMMM_yyyy, String timeZone) {
		this(Utils.parseDate(MMMM_yyyy, "MMMM-yyyy"), timeZone);
	}

	/**
	 * @param createdOn
	 */
	public Month(Date date, String timeZone) {
		DateTime dateTime = new DateTime(date, DateTimeZone.forID(timeZone));
		month = dateTime.getMonthOfYear();
		year = dateTime.getYear();
	}

	public Day getFirstDay() {
		return new Day(getStartTime(), timeZone);
	}

	public Day getLastDay() {
		return new Day(getEndTime(), timeZone);
	}

	public Date getStartTime() {
		String firstDayStr = year + "-" + month + "-01 00:00:00";
		Date startDate = Utils.parseDateFormat(firstDayStr, timeZone);
		return startDate;
	}

	public Date getEndTime() {
		Date nextMonthStart = Utils.dateMonthAhead(getStartTime(), 1);
		return Utils.getDateSecondsBefore(nextMonthStart, 1);
	}

	public boolean isBefore(Date timeStamp) {
		return getStartTime().before(timeStamp);
	}

	public boolean isAfter(Date timeStamp) {
		return getEndTime().before(timeStamp);
	}

	public Month getNextsMonth() {
		return getMonthsAfter(1);
	}

	public Month getMonthsAfter(int months) {
		DateTime other = new DateTime(getStartTime()).plusMonths(months);
		return new Month(other.toDate(), this.timeZone);
	}

	/**
	 * @return
	 */
	public String getName() {
		SimpleDateFormat f = new SimpleDateFormat("MMMM-yyyy");
		return f.format(getStartTime());
	}

	public static List<Month> getMonths(Date minTimestamp, Date maxTimestamp) {
		if (minTimestamp.after(maxTimestamp))
			Utils.handleException("Min Time should be before Max Time");

		List<Month> returnVal = new ArrayList<Month>();
		Month month = new Month(minTimestamp, Utils.UTC);
		while (month.isBefore(maxTimestamp)) {// add till day frame crosses maxTime
			returnVal.add(month);
			month = month.getNextsMonth();
		}

		return returnVal;
	}

	/**
	 * Split's month by give milliseconds.
	 *
	 * @param milliseconds
	 *            the milliseconds
	 * @return list of date range which contains lower and upper timestamps
	 */
	public List<DateRange> split(int milliseconds) {
		List<DateRange> dateRanges = new ArrayList<DateRange>();
		Date startTS = getStartTime();

		while (!isAfter(startTS)) {
			Date endTs = Utils.addMilliseconds(startTS, milliseconds - 1);

			//if end timestamp is not within month range then assign month's endtime
			if (isAfter(endTs))
				endTs = getEndTime();
			dateRanges.add(new DateRange(startTS, endTs));
			startTS = Utils.addMilliseconds(startTS, milliseconds);
		}

		return dateRanges;
	}
}
