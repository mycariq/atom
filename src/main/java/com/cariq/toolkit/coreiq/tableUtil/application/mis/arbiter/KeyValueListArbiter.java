package com.cariq.toolkit.coreiq.tableUtil.application.mis.arbiter;

import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

public class KeyValueListArbiter extends MISCellValueArbiterBase {
	List<KeyValueArbiter> listArbiters;

	public KeyValueListArbiter(List list) {
		super(list);
		
		List<GenericJSON> jsonList = Utils.getJSonObjectList(list.toString(), GenericJSON.class);
		
		listArbiters = new ArrayList<KeyValueArbiter>();
		for (GenericJSON json : jsonList) {
			KeyValueArbiter arbiter = new KeyValueArbiter(json);
			listArbiters.add(arbiter);
		}
	}

	@Override
	public boolean isApplicable(GenericJSON input) {	
		for (KeyValueArbiter arbiter : listArbiters) {
			if (! arbiter.isApplicable(input))
				return false;
		}

		return true;
	}
}
