package com.cariq.toolkit.utils;

import java.util.Iterator;
import java.util.List;

/**
 * ObjectFeeder is iterator that uses batchIterator internally to fetch objects in bulk
 * @author hrishi
 *
 * @param <T>
 */
public class ObjectFeeder<T> implements Iterator<T> {
	BatchIterator<T> batchIterator;
	List<T> currentList = null;
	int currentIndex = 0;

	public ObjectFeeder(BatchIterator<T> batchIterator) {
		super();
		this.batchIterator = batchIterator;
	}

	@Override
	public boolean hasNext() {
		if (currentList == null || (currentIndex) >= currentList.size()) {
			currentList = fetchNextBatch();
			currentIndex = 0;
		}
		
		return currentList != null;
	}

	private List<T> fetchNextBatch() {
		// From the batch iterator get the next one
		if (batchIterator.hasNext()) {
			return batchIterator.next();
		}
		return null;
	}

	@Override
	public T next() {
		return currentList.get(currentIndex++);
	}

}
