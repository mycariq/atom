/**
 * 
 */
package com.cariq.toolkit.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cariq.toolkit.utils.HtmlRenderingRule;
import com.cariq.toolkit.utils.HtmlValueRenderer;
import com.cariq.toolkit.utils.ObjectBuilder;

/**
 * @author hrishi
 *
 */
public class HtmlRuleBasedRenderer implements HtmlValueRenderer, ObjectBuilder<HtmlRenderingRule>{
	Map<String, List<HtmlRenderingRule>> rules = new HashMap<String, List<HtmlRenderingRule>>();
	
	@Override
	public String toHtmlString(String key, String value) {
		if (!rules.containsKey(key))
			return value;
		
		// there is a rule
		List<HtmlRenderingRule> ruleList = rules.get(key);
		
		for (HtmlRenderingRule rule : ruleList) {
			if (rule.isSatisfied(value))
				return rule.apply(key, value);
			
		}
		
		return value;
	}

	@Override
	public HtmlRuleBasedRenderer configure(String setting, Object value) {
		return this;
	}

	@Override
	public HtmlRuleBasedRenderer add(HtmlRenderingRule value) {
		List<HtmlRenderingRule> ruleList = rules.get(value.getKey());
		// if no rulelist exists, create one
		if (ruleList == null) {
			ruleList = new ArrayList<HtmlRenderingRule>();
			rules.put(value.getKey(), ruleList);
		}
		
		ruleList.add(value);
		return this;
	}

	@Override
	public HtmlRuleBasedRenderer add(String key, HtmlRenderingRule value) {
		add(value);
		return this;
	}

	@Override
	public HtmlRuleBasedRenderer build() {
		return this;
	}

}
