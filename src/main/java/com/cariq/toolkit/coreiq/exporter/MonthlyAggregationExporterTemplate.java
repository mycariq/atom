package com.cariq.toolkit.coreiq.exporter;

import java.util.Arrays;
import java.util.List;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.SimplePOJOs.CountPOJO;

/**
 * The Class MonthlyAggregationExporterTemplate.
 */
public class MonthlyAggregationExporterTemplate extends CarIQWrappedExporterTemplate {

	/** The Constant EndTimestamp. */
	public static final String TimeRange = "TimeRange", StartTimestamp = "StartTimestamp",
			EndTimestamp = "EndTimestamp", Count = "Count", Percentage = "Per", GrandTotal = "GrandTotal";

	/**
	 * Instantiates a new monthly aggregation exporter template.
	 *
	 * @param name
	 *            the name
	 * @param description
	 *            the description
	 */
	public MonthlyAggregationExporterTemplate(String name, String shortName, String description) {
		super(name, shortName, description);

		//query definition
		addQueryType(TimeRange, "The time range",
				Arrays.asList(new QueryDefinitionAttribute(StartTimestamp, "Start timestamp in yyyy-MM-dd HH:mm:ss"),
						new QueryDefinitionAttribute(EndTimestamp, "End timestamp in yyyy-MM-dd HH:mm:ss")));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.www.coreiq.exporter.CarIQExporterTemplate#getExportCount(java.lang.
	 * String, com.cariq.www.utils.GenericJSON)
	 */
	@Override
	public CountPOJO getExportCount(String queryType, GenericJSON json) {
		return new CountPOJO(2);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.toolkit.coreiq.exporter.CarIQWrappedExporterTemplate#getWrapper()
	 */
	@Override
	protected CarIQExporterWrapper getWrapper() {
		return new CarIQExporterWrapper(getName());
	}

	@Override
	protected List<GenericJSON> postProcess(String queryType, GenericJSON inputJson, List<GenericJSON> outputJsons) {
		for (GenericJSON outJson : outputJsons) {
			outJson.remove("domainId");
			outJson.remove("domainName");
			for (String key : outJson.keySet()) {
				if (outJson.get(key) == null || "null".equals(outJson.get(key).toString())) {
					outJson.put(key, 0);
				}
			}
		}
		return outputJsons;
	}
}
