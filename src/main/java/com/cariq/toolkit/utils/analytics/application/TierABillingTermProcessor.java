package com.cariq.toolkit.utils.analytics.application;

import com.cariq.toolkit.model.BillingTerm;
import com.cariq.toolkit.model.InvoiceUnit;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.google.common.base.Strings;

public class TierABillingTermProcessor extends BillingTermProcessor {
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("BillingTermProcessor");
	
	private static final String BILLING_TERM_DEF_MINKMSDRIVEN = "MinKmsDriven";
	
	public static final String TIERA_IU_DETAILS_CAR_CONNECTED = "Car Connected";
	
	public TierABillingTermProcessor(BillingTerm bt, InvoiceUnit iu) {
		super(bt, iu);
	}
	
	@Override
	protected boolean isRecordApplicableForBillingTerm(GenericJSON inputJSON) {
		// Validate the required keys in definition with input record 
		for (String btKey : defJSON.keySet()) {
			if (isSpecialConditionKey(btKey))
				continue;
			
			// KMS_Driven should be greater than given value
			if (btKey.equalsIgnoreCase(BILLING_TERM_DEF_MINKMSDRIVEN)) {
				Double btValue = Utils.getValue(Double.class, defJSON, BILLING_TERM_DEF_MINKMSDRIVEN);
				Double recordValue = Utils.getValue(Double.class, inputJSON, TAT_COL_CAR_KMSDRIVEN);
				if (null == recordValue || recordValue < btValue) {
					logger.debug("***KMSDriven not sufficient for policy: " + Utils.getValue(inputJSON, TAT_COL_POLICY_NUMBER) 
					+ ", min btValue: " + btValue + ", recordValue: " + recordValue);
					return false;
				}
				continue;
			}
			
			if (!matchKeyValue(btKey, inputJSON))
				return false;
		}
		
		numberOfInvoicedBillables += 1;		
		//logger.debug("***Matched policy: " + Utils.getValue(inputJSON, TAT_COL_POLICY_NUMBER));
		return true;
	}
	
	@Override
	protected void validateFinishingCriteria() {
		// Just update the invoice unit details
		GenericJSON iu_details = GenericJSON.build(TIERA_IU_DETAILS_CAR_CONNECTED, numberOfInvoicedBillables);
		invoiceUnit.setinfoJson(iu_details.toString());
		invoiceUnit.merge();
	}
}
