package com.cariq.toolkit.utils.mistools;

import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.utils.CQObjectConvertor;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.KeyValuePair;
import com.cariq.toolkit.utils.Utils;

public class MISSchemaHelper {

	public static List<KeyValuePair> getArgumentKeyValuePairs(List<MISSectionDefinition> applicableSectionDefs) {
		return MISSectionDefinition.collectKeyValues(applicableSectionDefs);
	}
	
	public static List<GenericJSON> getArgumentList(List<MISSectionDefinition> applicableSectionDefs) {
		if (Utils.isNullOrEmpty(applicableSectionDefs))
				return new ArrayList<GenericJSON>();
				
		List<KeyValuePair> arguments = MISSectionDefinition.collectKeyValues(applicableSectionDefs);

		return Utils.convertList(arguments, new CQObjectConvertor<KeyValuePair, GenericJSON>() {

			@Override
			public GenericJSON convert(KeyValuePair in) {
				return in.toGenericJSON();
			}
		});
	}

}
