package com.cariq.toolkit.service;

import com.atom.www.json.DrivingScoreJSON;
import com.cariq.toolkit.utils.scoring.ScoreDefinition;

/**
 * @author nitin
 *
 */
public interface ScoreEngineService {

	/**
	 * Gets the driving score.
	 *
	 * @param params the params
	 * @return the driving score
	 */
	Double getDrivingScore(DrivingScoreJSON params);

	/**
	 * Gets the driving score definition.
	 *
	 * @return the driving score definition
	 */
	ScoreDefinition getDrivingScoreDefinition();
}
