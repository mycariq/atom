package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.List;

import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

/**
 * Exports all order of all stages after shipment.
 * 
 * @author sagar
 *
 */
public class PostShipmentExporter extends ShipmentBasedSpotlightTemplate {
	
	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("PostShipmentExporter");

	public static final String TheExporter = "PostShipmentExporter";

	public static final String TheDescription = "Exports all order of all stages after shipment.";
	
	//Pre-Shipment stages
	private static final String PostShipment_Stage_Device_Onboarded = "Device OnBoarded";
	private static final String PostShipment_Stage_Device_Delivered = "Device Delivered";
	
	/**
	 * Default constructor
	 */
	public PostShipmentExporter() {
		super(TheExporter, TheDescription);
	}

	@Override
	protected List<GenericJSON> enhanceByEffectiveDates(List<GenericJSON> input) {
		logger.debug("Enhancing response with effective dates...");
		List<GenericJSON> retValues = new ArrayList<>();
		
		for(GenericJSON item : input) {
			String effectiveDate = null;
			String stage = Utils.getValue(item, STATE);
			
			if(PostShipment_Stage_Device_Delivered.equalsIgnoreCase(stage)) {
				effectiveDate = Utils.getValue(item, "DeliveredOn");
			} else if(PostShipment_Stage_Device_Onboarded.equalsIgnoreCase(stage)) {
				effectiveDate = Utils.getValue(item, "Device_OnboardedOn");
			} else {
				//If unexpected stage come here. then it should be handled with 
				//oldest date. In this case we consider shipped On date as oldest date
				logger.debug("Unknown stage '" + stage + "' found!!!!!!!!!!!!!!!!!."
						+ " Using ShippedOn as effective date for same!!!!!!!!!!!!!!!!!!!!!");
				effectiveDate = Utils.getValue(item, "ShippedOn");
			}
			item.add(Effective_Date, effectiveDate);
			retValues.add(item);
		}
		return retValues;
	}

	@Override
	protected String getQueryType() {
		return StagingAndPolicyHelper.QueryType_PostShipment;
	}
}
