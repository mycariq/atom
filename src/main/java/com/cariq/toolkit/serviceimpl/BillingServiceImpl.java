package com.cariq.toolkit.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.coreiq.exporter.ExportedData;
import com.cariq.toolkit.coreiq.model.ActionLog;
import com.cariq.toolkit.model.Billable;
import com.cariq.toolkit.model.BillingAccount;
import com.cariq.toolkit.model.BillingAccount.BillingAccount_In;
import com.cariq.toolkit.model.BillingAccount.BillingAccount_Out;
import com.cariq.toolkit.model.BillingContract;
import com.cariq.toolkit.model.BillingContract.BillingContract_In;
import com.cariq.toolkit.model.BillingContract.BillingContract_Out;
import com.cariq.toolkit.model.BillingTerm;
import com.cariq.toolkit.model.BillingTerm.BillingTerm_In;
import com.cariq.toolkit.model.BillingTerm.BillingTerm_Out;
import com.cariq.toolkit.model.Invoice;
import com.cariq.toolkit.model.InvoiceUnit;
import com.cariq.toolkit.model.InvoicedBillable;
import com.cariq.toolkit.model.Payment;
import com.cariq.toolkit.model.Payment.Payment_In;
import com.cariq.toolkit.service.BillingService;
import com.cariq.toolkit.utils.APIHelper;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.service.CarIQRestClientService;
import com.cariq.toolkit.utils.service.FileHandlerService;
import com.google.common.base.Strings;


/**
 * The Class BillingServiceImpl.
 */
@Service
public class BillingServiceImpl implements BillingService {

	/** The logger. */
	private static CarIQLogger logger = APIHelper.logger.getLogger("BillingServiceImpl");

	/** The rest service. */
	@Autowired
	CarIQRestClientService restService;
	
	/** The file handler service. */
	@Autowired
	FileHandlerService fileHandlerService;

	/*
	 * APIs for BillingAccount
	 */
	
	@Override
	@Transactional
	public ResponseWithIdJson addBillingAccount(BillingAccount_In params) {
		BillingAccount billingAccount = new BillingAccount();
		try {
			Utils.logAction(logger, "BillingServiceImpl.addBillingAccount", null, params.toString());
			billingAccount.fromJSON(params); 				
			billingAccount.persist();
		} catch (Exception e) {
			Utils.handleException("Failed to add BillingAccount, Exception: " + e.getMessage());
		}
		return new ResponseWithIdJson(billingAccount.getId().toString(), "BillingAccountName : " + params.getName() + " created successfully.");
	}

	@Override
	public BillingAccount_Out getBillingAccount(String name) {
		BillingAccount_Out billingAccountOut = null;
		try {
			BillingAccount billingAccount = BillingAccount.getByName(name);
			if (billingAccount != null) {
				billingAccountOut = billingAccount.toJSON();
			}
		}catch (Exception e) {
			Utils.handleException("Failed to get BillingAccount: " + name + ", Exception: " + e.getMessage());
		}
		
		return billingAccountOut;
	}

	@Override
	public List<BillingAccount_Out> getAllBillingAccounts(int pageNo, int pageSize) {
		List<BillingAccount_Out> resultList = new ArrayList<>();
		try {
			List<BillingAccount> billingsAccounts = BillingAccount.getAllObjects(pageNo, pageSize);
			
			for (BillingAccount billingAccount : billingsAccounts) {
				BillingAccount_Out baOut = billingAccount.toJSON();
				resultList.add(baOut);
			}
		} catch (Exception e) {
			Utils.handleException("Failed to get all BillingAccounts, Exception: " + e.getMessage());
		}
		return resultList;
	}


	@Override
	@Transactional
	public ResponseJson deleteBillingAccount(Long id) {
		try {
			BillingAccount billingAcccount = BillingAccount.findObjectById(id);
			if (billingAcccount != null) {
				// BillingContract, BillingOperation, Invoice - FK constraint
				billingAcccount.remove();
				ActionLog.log(Utils.createContext("BillingServiceImpl.deleteBillingAccount"), "Delete", "BillingAccount",
					"ID : " + billingAcccount.getId() + " Name : " + billingAcccount.getName(),
					"Old info :" + Utils.getJSonString(billingAcccount.toJSON()));
				return new ResponseJson("Billing : " + id + " deleted successfully.");
			}
		} catch (Exception e) {
			Utils.handleException("Failed to remove BillingAccount: " + id + ", Exception: " + e.getMessage());
		}
		return new ResponseJson("BillingAccount : " + id + " does not exist!");
	}
	
	@Override
	@Transactional
	public ResponseJson updateBillingAccount(BillingAccount_In params) {
		try {
			if (Strings.isNullOrEmpty(params.getName()))
				Utils.handleException("Invalid account name to update!");
			BillingAccount billingAccount = BillingAccount.getByName(params.getName());
			if (billingAccount != null) {
				billingAccount.update(params);
				ActionLog.log(Utils.createContext("BillingServiceImpl.updateBillingAccount"), "Update", "BillingAccount",
						"ID : " + billingAccount.getId() + ", Name : " + billingAccount.getName(), "Updates: " + params.toString());
				return new ResponseJson("Billing : " + params.getName() + " updated successfully.");
			}
		} catch (Exception e) {
			Utils.handleException("Failed to update BillingAccount: " + params.getName() + ", Exception: " + e.getMessage());
		}
		return new ResponseJson("BillingAccount : " + params.getName() + " does not exist!");
	}
	
	/*
	 * APIs for BillingContract
	 */
	
	@Override
	@Transactional
	public ResponseWithIdJson addBillingContract(BillingContract_In params) {
		BillingContract billingContract = new BillingContract();
		try {
			Utils.logAction(logger, "BillingServiceImpl.addBillingContract", null, params.toString());
			billingContract.fromJSON(params); 				
			billingContract.persist();
		} catch (Exception e) {
			Utils.handleException("Failed to add BillingContract, Exception: " + e.getMessage());
		}
		return new ResponseWithIdJson(billingContract.getId().toString(), "BillingContract between " + billingContract.getAccount_1().getName()
				+ " and " + billingContract.getAccount_2().getName() + " created successfully.");
	}
	
	@Override
	public BillingContract_Out getBillingContract(Long id) {
		BillingContract_Out billingContractOut = null;
		try {
			BillingContract billingContract = BillingContract.findObjectById(id);
			if (billingContract != null) {
				billingContractOut = billingContract.toJSON();
			}
		}catch (Exception e) {
			Utils.handleException("Failed to get BillingContractId: " + id + ", Exception: " + e.getMessage());
		}
		
		return billingContractOut;
	}
	
	@Override
	@Transactional
	public ResponseJson deleteBillingContract(Long id) {
		try {
			BillingContract billingContract = BillingContract.findObjectById(id);
			if (billingContract != null) {
				// BillingOperation, Invoice - FK constraint
				billingContract.remove();
				ActionLog.log(Utils.createContext("BillingServiceImpl.deleteBillingContract"), "Delete", "BillingContract",
					"ID : " + billingContract.getId(), "Old info :" + Utils.getJSonString(billingContract.toJSON()));
				return new ResponseJson("BillingContract : " + id + " deleted successfully.");
			}
		} catch (Exception e) {
			Utils.handleException("Failed to remove BillingContract: " + id + ", Exception: " + e.getMessage());
		}
		return new ResponseJson("BillingContract : " + id + " does not exist!");
	}
	
	@Override
	@Transactional
	public ResponseJson updateBillingContract(BillingContract_In params) {
		try {
			if (Strings.isNullOrEmpty(params.getName()))
				Utils.handleException("Invalid billing contract to update!");
			BillingContract billingContract = BillingContract.getByName(params.getName());
			if (billingContract != null) {
				billingContract.update(params);
				ActionLog.log(Utils.createContext("BillingServiceImpl.updateBillingContract"), "Update", "BillingContract",
						"ID : " + billingContract.getId(), "Updates: " + params.toString());
				return new ResponseJson("Billing : " + params.getName() + " updated successfully.");
			}
		} catch (Exception e) {
			Utils.handleException("Failed to update BillingContract: " + params.getName() + ", Exception: " + e.getMessage());
		}
		return new ResponseJson("BillingContract : " + params.getName() + " does not exist!");
	}
	
	/*
	 * APIs for BillingTerm
	 */
	
	@Override
	@Transactional
	public ResponseWithIdJson addBillingTerm(BillingTerm_In params) {
		BillingTerm billingTerm = new BillingTerm();
		try {
			Utils.logAction(logger, "BillingServiceImpl.addBillingTerm", null, params.toString());
			billingTerm.fromJSON(params); 				
			billingTerm.persist();
		} catch (Exception e) {
			Utils.handleException("Failed to add BillingContract, Exception: " + e.getMessage());
		}
		return new ResponseWithIdJson(billingTerm.getId().toString(), "BillingTerm : " + billingTerm.getName() + " for contract " 
				+ billingTerm.getItsContract().getId() + " created successfully.");
	}
	
	@Override
	public BillingTerm_Out getBillingTerm(Long id) {
		BillingTerm_Out billingTermOut = null;
		try {
			BillingTerm billingTerm = BillingTerm.findObjectById(id);
			if (billingTerm != null) {
				billingTermOut = billingTerm.toJSON();
			}
		}catch (Exception e) {
			Utils.handleException("Failed to get BillingTermId: " + id + ", Exception: " + e.getMessage());
		}
		
		return billingTermOut;
	}
	
	@Override
	public BillingTerm_Out getBillingTerm(String name) {
		BillingTerm_Out billingTermOut = null;
		try {
			BillingTerm billingTerm = BillingTerm.getByName(name);
			if (billingTerm != null) {
				billingTermOut = billingTerm.toJSON();
			}
		}catch (Exception e) {
			Utils.handleException("Failed to get BillingTerm: " + name + ", Exception: " + e.getMessage());
		}
		
		return billingTermOut;
	}
	
	@Override
	@Transactional
	public ResponseJson deleteBillingTerm(Long id) {
		try {
			BillingTerm billingTerm = BillingTerm.findObjectById(id);
			if (billingTerm != null) {
				// BillingOperation, Invoice - FK constraint
				billingTerm.remove();
				ActionLog.log(Utils.createContext("BillingServiceImpl.deleteBillingTerm"), "Delete", "BillingTerm",
					"ID : " + billingTerm.getId() + " Name : " + billingTerm.getName(),
					"Old info :" + Utils.getJSonString(billingTerm.toJSON()));
				return new ResponseJson("BillingTerm : " + id + " deleted successfully.");
			}
		} catch (Exception e) {
			Utils.handleException("Failed to remove BillingTerm: " + id + ", Exception: " + e.getMessage());
		}
		return new ResponseJson("BillingTerm : " + id + " does not exist!");
	}
	
	@Override
	@Transactional
	public ResponseJson updateBillingTerm(BillingTerm_In params) {
		try {
			if (Strings.isNullOrEmpty(params.getName()))
				Utils.handleException("Invalid billing term to update!");
			BillingTerm billingTerm = BillingTerm.getByName(params.getName());
			if (billingTerm != null) {
				billingTerm.update(params);
				ActionLog.log(Utils.createContext("BillingServiceImpl.updateBillingTerm"), "Update", "BillingTerm",
						"ID : " + billingTerm.getId(), "Updates: " + params.toString());
				return new ResponseJson("BillingTerm : " + params.getName() + " updated successfully.");
			}
		} catch (Exception e) {
			Utils.handleException("Failed to update BillingTerm: " + params.getName() + ", Exception: " + e.getMessage());
		}
		return new ResponseJson("BillingTerm : " + params.getName() + " does not exist!");
	}
	
	/*
	 * APIs for Payment
	 */
	
	@Override
	@Transactional
	public ResponseWithIdJson addPayment(Payment_In params) {
		Payment payment = new Payment();
		try {
			Utils.logAction(logger, "BillingServiceImpl.addPayment", null, params.toString());
			payment.fromJSON(params); 				
			payment.persist();
			// Mark the related invoice as PAID
			payment.getItsInvoice().setStatus(Invoice.InvoiceStatusEnum.PAID.toString());
			payment.persist();
		} catch (Exception e) {
			Utils.handleException("Failed to add Payment, Exception: " + e.getMessage());
		}
		return new ResponseWithIdJson(payment.getId().toString(), "Payment created successfully.");
	}
	
	@Override
	@Transactional
	public ResponseJson deletePayment(Long id) {
		try {
			Payment payment = Payment.findObjectById(id);
			if (payment != null) {
				// Revert its invoice status to APPROVED, if PAID
				if (payment.getItsInvoice().getStatus().equalsIgnoreCase(Invoice.InvoiceStatusEnum.PAID.toString())) {
					logger.debug("*** Changing invoice# " + payment.getItsInvoice().getId() + " status as approved from paid.");
					payment.getItsInvoice().setStatus(Invoice.InvoiceStatusEnum.APPROVED.toString());
					payment.getItsInvoice().persist();
				}
				// Delete payment
				logger.debug("*** Deleting payment# " + id); 
				payment.remove();
				ActionLog.log(Utils.createContext("BillingServiceImpl.deletePayment"), "Delete", "Payment",
					"ID : " + payment.getId(), "Old info :" + Utils.getJSonString(payment.toJSON()));
				return new ResponseJson("Payment# : " + id + " deleted successfully.");
			}
		} catch (Exception e) {
			Utils.handleException("Failed to remove Payment# " + id + ", Exception: " + e.getMessage());
		}
		return new ResponseJson("Payment# : " + id + " does not exist!");
	}
	
	/*
	 * APIs for Invoice
	 */
	
	@Override
	@Transactional
	public ResponseJson updateInvoiceStatus(Long id, String status) {
		try {
			Invoice invoice = Invoice.findObjectById(id);
			if (invoice != null) {
				invoice.setStatus(status);
				ActionLog.log(Utils.createContext("BillingServiceImpl.updateInvoiceStatus"), "updateInvoiceStatus", "Invoice",
						"ID : " + invoice.getId(), "Updated Status: " + status);
				return new ResponseJson("Invoice# " + id + " updated successfully.");
			}
		} catch (Exception e) {
			Utils.handleException("Failed to update Invoice# " + id + ", Exception: " + e.getMessage());
		}
		return new ResponseJson("Invoice# " + id + " does not exist!");
	}
	
	@Override
	@Transactional
	public ResponseJson destroyInvoice(Long id) {
		try {
			Invoice invoice = Invoice.findObjectById(id);
			if (invoice != null) {
				// Validate invoice status
				if (invoice.getStatus().equalsIgnoreCase(Invoice.InvoiceStatusEnum.APPROVED.toString()) 
					|| invoice.getStatus().equalsIgnoreCase(Invoice.InvoiceStatusEnum.PAID.toString())
					|| invoice.getStatus().equalsIgnoreCase(Invoice.InvoiceStatusEnum.IN_REVIEW.toString()))
					Utils.handleException("Invoice is already " + invoice.getStatus() + ", can not destroy!!");
				
				// Get child objects and destroy them first
				List<InvoiceUnit> iuList = InvoiceUnit.getByItsInvoice(invoice);
				for (InvoiceUnit iu : iuList) {
					List<InvoicedBillable> ibList = InvoicedBillable.getByInvoiceUnit(iu);
					logger.debug("*** Deleting invoiced_billables & possible billables for iu: " + iu.getId());
					for (InvoicedBillable ib : ibList) {
						Billable itsBillable = ib.getItsBillable();
						List<InvoicedBillable> ibListForBillable = InvoicedBillable.getByBillable(itsBillable);
						//logger.debug("*** Deleting ib: " + ib.getId());
						ib.remove();
						// Its billable can be deleted only if deleted ib was alone referring to billable
						if (ibListForBillable.size() == 1) {
							//logger.debug("Deleting billable: " +  itsBillable.getSignature());
							itsBillable.remove();
						}

					}
					logger.debug("*** Deleting iu: " + iu.getId());
					iu.remove();
				}
				// Delete the document & its record
				try {
					logger.debug("Deleting document: " + invoice.getUrlToDocument());
					String fileName = invoice.getUrlToDocument().substring( invoice.getUrlToDocument().lastIndexOf('/')+1, invoice.getUrlToDocument().length());
					logger.debug("Deleting FileName: " + fileName);
					fileHandlerService.deleteFile(fileName, "exporter");		
					
					// Delete exported data entry
					ExportedData exportedDataRecord = ExportedData.findExportedDataLatest(invoice.getItsBillingOperation().getItsDomain(), 
							"BillingDetails", fileName, null, "html");
					if (null != exportedDataRecord)
						exportedDataRecord.remove();

				} catch (Exception e) {
					logger.debug("Failed to delete the document: " + invoice.getUrlToDocument() + ", exception: " + e.getMessage());
				}
				// Delete the invoice
				invoice.remove();
				ActionLog.log(Utils.createContext("BillingServiceImpl.destroyInvoice"), "Destroy", "Invoice",
					"ID : " + invoice.getId(), "Old info :" + Utils.getJSonString(invoice.toJSON()));
				return new ResponseJson("Invoice# : " + id + " destroyed successfully.");
			}
		} catch (Exception e) {
			Utils.handleException("Failed to destroy Invoice# " + id + ", Exception: " + e.getMessage());
		}
		return new ResponseJson("Invoice# : " + id + " does not exist!");
	}
}