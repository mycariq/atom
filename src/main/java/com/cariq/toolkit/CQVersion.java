package com.cariq.toolkit;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import com.cariq.toolkit.utils.APIHelper;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.Utils;

public class CQVersion {
	public static String Domain = "DOMAIN_UNKNOWN"; //@TMPL public static String Domain = "[% ROOT.servers.api.endPoint %]"; @TMPL
	
	public static final String PLATFORM_EXPIRE = "platformExpiry";
	public static final CarIQLogger logger = APIHelper.logger.getLogger("CQVersion");


	// DO NOT Autoformat following lines - or any part of this file!!!
	public static Map<String, String> getConfig() {
		Map<String, String> config = new LinkedHashMap<String, String>();
		
		config.put("fleetiq", "UNKNOWN"); //@TMPL config.put("fleetiq", "[% ROOT.servers.fleetiq.endPoint %]"); @TMPL
		config.put(PLATFORM_EXPIRE, "2025-01-01 00:00:00"); //@TMPL config.put("platformExpiry", "[% ROOT.servers.api.platformExpiry %]"); @TMPL
				
		return config;
	}
	
	public static String buildDate = "2019-04-15 00:00:00"; //@TMPL public static String buildDate = "__NOW__"; @TMPL
	public static String dateCode = "__DATECODE__"; //@TMPL public static String dateCode = "__DATECODE__"; @TMPL

	
	/**
	 * Checks if is platform expired.
	 *
	 * @return true, if is platform expired
	 */
	public static boolean isPlatformExpired() {
		
		Map<String, String> config = getConfig();
		if (!config.containsKey(CQVersion.PLATFORM_EXPIRE))
			return false;
		
		Date now = new Date();
		Date platformExpiryDate = Utils.downCast(Date.class, config.get(PLATFORM_EXPIRE));
		if(null == platformExpiryDate)
			return false;
		
		logger.debug("Platform Expiry Date: " + platformExpiryDate);
		return now.after(platformExpiryDate);
	}
	
	/**
	 * Main program to return the Build Date, Datecode and Configuration
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("=============================================");
		System.out.println("CarIQ Platform");
		System.out.println("=============================================");

		System.out.println("Domain: " + Domain);
		System.out.println("Build Date: " + buildDate);
		System.out.println("Date Code: " + dateCode);
		System.out.println("============");
		System.out.println("Configuration:");
		System.out.println("============");
		
		Map<String, String> config = getConfig();
		for (String server : config.keySet()) {
			System.out.println(server + " => " + config.get(server));	
		}
		
		System.out.println("=============================================");
	}
}