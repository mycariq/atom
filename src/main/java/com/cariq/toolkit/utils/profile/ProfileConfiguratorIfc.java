package com.cariq.toolkit.utils.profile;

import java.util.List;

import com.cariq.toolkit.utils.profile.listener.DiagnosticInstrumentFactory;

/**
 * Configurator for ProfilePoint - to configure different diagnostic instrument
 * 
 * @author HVN
 *
 */
public interface ProfileConfiguratorIfc {
	ProfileRendererIfc getRenderer();

	void setRenderer(ProfileRendererIfc renderer);

	boolean isProfilingEnabled();

	boolean isProfilingEnabled(String activityName);

	List<DiagnosticInstrumentFactory> getInstrumentFactories();
}
