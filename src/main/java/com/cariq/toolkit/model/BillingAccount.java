package com.cariq.toolkit.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.BillingAccount.BillingAccount_In;
import com.cariq.toolkit.model.BillingAccount.BillingAccount_Out;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.GenericJsonable;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;
import com.cariq.toolkit.utils.Utils;

/**
 * This class implements "billing account"
 * This is the business entity that has signed the billing contract with another account. 
 * Account can be the entity raising the invoice OR paying the invoice.
 * @author amita
 *
 */

@Entity
@Configurable
public class BillingAccount extends PersistentObject<BillingAccount> implements JSONable<BillingAccount_In, BillingAccount_Out, IdJSON>, GenericJsonable{

	// SECTION 1 -Code for Persistence
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@Version
	Integer version;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new BillingAccount().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null)
			this.entityManager = entityManager();
		return entityManager;
	}

	/**
	 * @param id
	 * @return
	 */
	protected static BillingAccount find(Long id) {
		return entityManager().find(BillingAccount.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public BillingAccount findById(Long id) {
		return find(id);
	}

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/**
	 * Model properties
	 */
	@NotNull(message = "@NotNull BillingAccount Name")
	@Column(unique = true)
	String name;

	@NotNull(message = "@NotNull BillingAccount PAN")
	@Column(unique = true)
	String pan;

	@Column(unique = true)
	String gst;
	
	String contactName;

	@Column(unique = true)
	String contactPhone;
	
	@Column(unique = true)
	String contactEmail;
	
	@Size(max=1024, message = "Address can be 1024 characters max")
	private String contactAddress;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	// Default constructor is a must!
	public BillingAccount() {
		super();
	}

	/**
	 * Parameterized constructor
	 * 
	 * @param name
	 * @param pan
	 * @param gst
	 * @param contactName
	 * @param contactPhone
	 * @param contactEmail
	 * @param contactAddress
	 * @param createdOn
	 * @param modifiedOn
	 */
	public BillingAccount(String name, String pan, String gst, String contactName, String contactPhone, 
			String contactEmail, String contactAddress, Date createdOn, Date modifiedOn) {
		super();
		this.name = name;
		this.pan = pan;
		this.gst = gst;
		this.contactName = contactName;
		this.contactPhone = contactPhone;
		this.contactEmail = contactEmail;
		this.contactAddress = contactAddress;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
	}

	/**
	 * Getters Setters
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getGst() {
		return gst;
	}

	public void setGst(String gst) {
		this.gst = gst;
	}
	
	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getContactAddress() {
		return contactAddress;
	}

	public void setContactAddress(String contactAddress) {
		this.contactAddress = contactAddress;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	
	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------

	// Implementation of toString and hashCode and equals are advisable for all
	// Model classes
	@Override
	public String toString() {
		return "BillingAccount [id=" + id + ", version=" + version + ", name=" + name 
				+ ", pan=" + pan + ", contactName=" + ", gst=" + gst
				+ contactName + ", contactPhone=" + contactPhone + ", contactEmail=" + contactEmail + ", contactAddress=" + contactAddress
				+ ", createdOn=" + createdOn + ", modifiedOn=" + modifiedOn + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((pan == null) ? 0 : pan.hashCode());
		result = prime * result + ((gst == null) ? 0 : gst.hashCode());
		result = prime * result + ((contactName == null) ? 0 : contactName.hashCode());
		result = prime * result + ((contactPhone == null) ? 0 : contactPhone.hashCode());
		result = prime * result + ((contactEmail == null) ? 0 : contactEmail.hashCode());
		result = prime * result + ((contactAddress == null) ? 0 : contactAddress.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BillingAccount other = (BillingAccount) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (pan == null) {
			if (other.pan != null)
				return false;
		} else if (!pan.equals(other.pan))
			return false;
		if (gst == null) {
			if (other.gst != null)
				return false;
		} else if (!gst.equals(other.gst))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (contactName == null) {
			if (other.contactName != null)
				return false;
		} else if (!contactName.equals(other.contactName))
			return false;
		if (contactPhone == null) {
			if (other.contactPhone != null)
				return false;
		} else if (!contactPhone.equals(other.contactPhone))
			return false;
		if (contactEmail == null) {
			if (other.contactEmail != null)
				return false;
		} else if (!contactEmail.equals(other.contactEmail))
			return false;
		if (contactAddress == null) {
			if (other.contactAddress != null)
				return false;
		} else if (!contactAddress.equals(other.contactAddress))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		return true;
	}

	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API
	// interaction
	
	// In Class (for construction)
	public static class BillingAccount_In {
		String name;
		String pan;
		String gst;
		String contactName;
		String contactPhone;
		String contactEmail;
		String contactAddress;

		public BillingAccount_In() {
			super();
		}

		public BillingAccount_In(String name, String pan, String gst, String contactName, String contactPhone, 
				String contactEmail, String contactAddress) {
			super();
			this.name = name;
			this.pan = pan;
			this.gst = gst;
			this.contactName = contactName;
			this.contactPhone = contactPhone;
			this.contactEmail = contactEmail;
			this.contactAddress = contactAddress;
		}

		/**
		 * Getters - Setters
		 */
		
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getPan() {
			return pan;
		}

		public void setPan(String pan) {
			this.pan = pan;
		}
		
		public String getGst() {
			return gst;
		}

		public void setGst(String gst) {
			this.gst = gst;
		}
		
		public String getContactName() {
			return contactName;
		}

		public void setContactName(String contactName) {
			this.contactName = contactName;
		}

		public String getContactPhone() {
			return contactPhone;
		}

		public void setContactPhone(String contactPhone) {
			this.contactPhone = contactPhone;
		}

		public String getContactEmail() {
			return contactEmail;
		}

		public void setContactEmail(String contactEmail) {
			this.contactEmail = contactEmail;
		}

		public String getContactAddress() {
			return contactAddress;
		}

		public void setContactAddress(String contactAddress) {
			this.contactAddress = contactAddress;
		}
		
		@Override
		public String toString() {
			return "BillingAccount [name=" + name + ", pan=" + pan + ", contactName=" + ", gst=" + gst
					+ contactName + ", contactPhone=" + contactPhone + ", contactEmail=" + contactEmail 
					+ ", contactAddress=" + contactAddress + "]";
		}	
	}

	// Out class for output
	public static class BillingAccount_Out extends BillingAccount_In {
		Long id;
		String createdOn;
		String modifiedOn;

		public BillingAccount_Out() {
			super();
		}
		
		/**
		 * @param name
		 * @param pan
		 * @param contactName
		 * @param contactPhone
		 * @param contactEmail
		 * @param contactAddress
		 * @param createdOn
		 * @param modifiedOn
		 * @param id
		 */
		public BillingAccount_Out(String name, String pan, String gst, String contactName, String contactPhone, String contactEmail, 
				String contactAddress, String createdOn, String modifiedOn, Long id) {
			super(name, pan, gst, contactName, contactPhone, contactEmail, contactAddress);
			this.id = id;
			this.createdOn = createdOn;
			this.modifiedOn = modifiedOn;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toJSON()
	 */
	@Override
	public BillingAccount_Out toJSON() {
		return new BillingAccount_Out(name, pan, gst, contactName, contactPhone, contactEmail, contactAddress, createdOn.toString(), modifiedOn.toString(), id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#fromJSON(java.lang.Object)
	 */
	@Override
	public void fromJSON(BillingAccount_In json) {
		this.name = json.name;
		this.pan = json.pan;
		this.gst = json.gst;
		this.contactName = json.contactName;
		this.contactPhone = json.contactPhone;
		this.contactEmail = json.contactEmail;
		this.contactAddress = json.contactAddress;
		this.createdOn = new Date();
		this.modifiedOn = new Date();
	}
	
	@Override
	public GenericJSON toGenericJSON() {		
		return GenericJSON.build("name", name, "pan", pan, "gst", gst,
				"contactName", contactName, "contactPhone", contactPhone, 
				"contactEmail", contactEmail, "contactAddress", contactAddress,
				"CreatedOn", createdOn.toString(), "ModifiedOn", modifiedOn.toString(), "Id", id);
	}
	
	@Override
	public void fromGenricJSON(GenericJSON json) {
		Utils.notImplementedException("BillingAccount.fromGenricJSON");
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}

	// ---------------------------------------------
	// SECTION 5 - Query and Class specific finders and static utility methods
	// ---------------------------------------------
	public static BillingAccount findObjectById(Long id) {
		return find(id);
	}
	
	public static List<BillingAccount> getAllObjects(int pageNo, int pageSize) {
		CarIQSimpleQuery<BillingAccount> qry = new CarIQSimpleQuery<BillingAccount>("getAllObjects", entityManager(), BillingAccount.class);
		qry.setPaging(pageNo, pageSize);

		return qry.getResultList();
	}

	public static BillingAccount getByName(String name) {
		CarIQSimpleQuery<BillingAccount> qry = new CarIQSimpleQuery<BillingAccount>("getByName", entityManager(), BillingAccount.class);
		qry.addCondition("name", "=", name);

		if (qry.getResultList().isEmpty())
			return null;
		return qry.getSingleResult();
	}
	
	public static BillingAccount getByPan(String pan) {
		CarIQSimpleQuery<BillingAccount> qry = new CarIQSimpleQuery<BillingAccount>("getByPan", entityManager(), BillingAccount.class);
		qry.addCondition("pan", "=", pan);

		if (qry.getResultList().isEmpty())
			return null;
		return qry.getSingleResult();
	}

	public static BillingAccount getByEmail(String email) {
		CarIQSimpleQuery<BillingAccount> qry = new CarIQSimpleQuery<BillingAccount>("getByEmail", entityManager(), BillingAccount.class);
		qry.addCondition("contactEmail", "=", email);

		if (qry.getResultList().isEmpty())
			return null;
		return qry.getSingleResult();
	}
	
	public void update(BillingAccount_In json) {
		if (json.pan != null)
			this.pan = json.pan;
		if(json.gst != null)
			this.gst = json.gst;
		if (json.contactName != null)
			this.contactName = json.contactName;
		if (json.contactPhone != null)
			this.contactPhone = json.contactPhone;
		if (json.contactEmail != null)
			this.contactEmail = json.contactEmail;
		if (json.contactAddress != null)
			this.contactAddress = json.contactAddress;

		this.modifiedOn = new Date();
		this.persist();
	}
}