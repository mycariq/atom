package com.cariq.toolkit.utils;

import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.utils.ObjectProcessorHelper.ObjectProcessorStage;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * This is abstract template class implementing ObjectProcessor<MODEL, INPUT>
 * This template defines the flow of processing at every step (Prepare, Process, Finish): StartingSteps, Child-processing, EndingSteps
 * @author amita
 */

public abstract class ObjectProcessorTemplate<MODEL, INPUT> implements ObjectProcessor<MODEL, INPUT> {
	CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("ObjectProcessor");
	
	private MODEL model;
	private String stage;
	private List<ObjectProcessor> childProcessors = new ArrayList<ObjectProcessor>(); 
	
	protected void doStartPrepare() {} // Preparation before child
	protected void doEndPrepare() {} // Preparation after child
	
	protected void doStartProcess(INPUT input) {} // Processing before child
	protected void doEndProcess(INPUT input) {} // Processing after child
	
	protected void doStartFinish() {} // Finishing steps before child
	protected void doEndFinish() {} // Finishing steps after child
	
	protected void doProcessChildPrepareException(ObjectProcessor o, RuntimeException e) {throw e;} // Handle exception in child preparation
	protected void doProcessChildProcessException(ObjectProcessor o, RuntimeException e) {throw e;} // Handle exception in child processing
	protected void doProcessChildFinishException(ObjectProcessor o, RuntimeException e) {throw e;} // Handle exception in child finishing
	
	@Override
	public void destroyModel() {} // Method to destroy self OR child models 
	
	public ObjectProcessorTemplate(MODEL object) {
		super();
		this.model = object;
	}
	
	@Override
	public void prepare() {	
		try (ProfilePoint _ObjectProcessor_Prepare = ProfilePoint.profileAction("ProfAction__ObjectProcessor_Prepare_" + this.getClass().getName())) {
			this.stage = ObjectProcessorStage.IN_PREPARE.toString();		
			
			// Prepare things before child preparation starts
			doStartPrepare();
			
			// Prepare Child Processors
			for (ObjectProcessor childProcessor : childProcessors) {
				childProcessor.prepare();
			}
			
			// Prepare things after child preparation done
			doEndPrepare();
		
			this.stage = ObjectProcessorStage.PREPARED.toString();
		}
	}

	@Override
	public void process(INPUT input) {
		try (ProfilePoint _ObjectProcessor_Process = ProfilePoint.profileAction("ProfAction__ObjectProcessor_Process_" + this.getClass().getName())) {
			this.stage = ObjectProcessorStage.IN_PROCESS.toString();

			// Process things before child processing starts
			doStartProcess(input);
			
			// Processing is bottom-up. First get things done with child processors and then do your work.
			for (ObjectProcessor childProcessor : childProcessors) {
				childProcessor.process(input);
			}
			
			// Process things after child processing done
			doEndProcess(input);
		
			this.stage = ObjectProcessorStage.PROCESSED.toString();
		}
	}
	
	@Override
	public void finish() {
		try (ProfilePoint _ObjectProcessor_Process = ProfilePoint.profileAction("ProfAction__ObjectProcessor_Process_" + this.getClass().getName())) {
			this.stage = ObjectProcessorStage.IN_FINISH.toString();
			
			// Finish things before child finishing starts
			doStartFinish();
			
			// Finishing is bottom-up. First finish things with child processors and then finish your work.
			for (ObjectProcessor childProcessor : childProcessors) {
				try {
					childProcessor.finish();
				} catch(RuntimeException e) {
					doProcessChildFinishException(childProcessor, e);
				}
			}
			
			// Finish things after child finishing done
			doEndFinish();
			
			this.stage = ObjectProcessorStage.FINISHED.toString();
		}
	}
	
	@Override
	public MODEL getModel() {
		return model;
	}

	@Override
	public String getStage() {
		return stage;
	}

	protected final void addChildProcessor(ObjectProcessor childProcessor) {
		childProcessors.add(childProcessor);
	}

	@Override
	public List<ObjectProcessor> getChildProcessors() {		
		return childProcessors;
	} 
}
