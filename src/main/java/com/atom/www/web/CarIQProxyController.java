package com.atom.www.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.publicapi.CarIQPublicAPIParameter;
import com.cariq.toolkit.service.CariqProxyService;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.utils.CarIQMutableAPI;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

@CarIQPublicAPI(name = "CarIQProxy", description = "Everything related to CarIQ API calling from Atom")
@Controller
@RequestMapping("/cariqProxy")
public class CarIQProxyController {

	@Autowired
	CariqProxyService proxyService;

	@CarIQPublicAPIMethod(description = "Call cariq get API", responseClass = GenericJSON.class,internal = true)
	@RequestMapping(value = "/get/{apiName}", method = RequestMethod.GET)
	@ResponseBody
	public Object get(
			@CarIQPublicAPIParameter(name = "apiName", description = "the apiName") @PathVariable String apiName) {
		try (CarIQAPI _get = new CarIQAPI("CarIQProxyController_get")) {
			return proxyService.get(Utils.makeApi(apiName));
		}
	}

	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Call cariq post API", responseClass = GenericJSON.class, internal = true)
	@RequestMapping(value = "/post/{apiName}", method = RequestMethod.POST)
	@ResponseBody
	public Object post(@RequestBody GenericJSON json,
			@CarIQPublicAPIParameter(name = "apiName", description = "the apiName") @PathVariable String apiName) {
		try (CarIQMutableAPI _post = new CarIQMutableAPI("CarIQProxyController_post", json)) {
			return proxyService.post(json, Utils.makeApi(apiName));
		}
	}

	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Call cariq put API", responseClass = GenericJSON.class, internal = true)
	@RequestMapping(value = "/put/{apiName}", method = RequestMethod.PUT)
	@ResponseBody
	public Object put(@RequestBody GenericJSON json,
			@CarIQPublicAPIParameter(name = "apiName", description = "the apiName") @PathVariable String apiName) {
		try (CarIQMutableAPI _put = new CarIQMutableAPI("CarIQProxyController_put", json)) {
			return proxyService.put(json, Utils.makeApi(apiName));
		}
	}

	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Call cariq delete API", responseClass = GenericJSON.class, internal = true)
	@RequestMapping(value = "/delete/{apiName}", method = RequestMethod.DELETE)
	@ResponseBody
	public Object delete(
			@CarIQPublicAPIParameter(name = "apiName", description = "the apiName") @PathVariable String apiName) {
		try (CarIQMutableAPI _delete = new CarIQMutableAPI("CarIQProxyController_delete", apiName)) {
			return proxyService.delete(Utils.makeApi(apiName));
		}
	}
}