package com.cariq.toolkit.utils.wwwservice.core;

/**
 * GeoTile represents approximately 11x11 meters of square on earth surface.
 * 
 * @author HVN
 *
 */
public class GeoTile implements Comparable<GeoTile> {
	private static final double PI = 180.0;
	private static final int LEFT_SHIFT_BITS = 32;
	private static final int MAX_BITS = 63;

	// average size of tile = 10.8 meters
	private static final double tileSize = 0.0108;
	private static final double EARTH_RADIUS = 6371.0;

	// The Geotile Id - unique number representing 10 x 10 area on earth surface
	long tile;

	/**
	 * Enum for direction
	 */
	enum Direction {
		EAST, WEST, NORTH, SOUTH;
	};

	/**
	 * Construct from Latitude, Longitude
	 * 
	 * @param latitude
	 * @param longitude
	 */
	public GeoTile(double latitude, double longitude) {
		tile = store(convertToStorableValue(latitude),
				convertToStorableValue(longitude));

	}

	/**
	 * Simplified Constructor
	 * 
	 * @param latitude
	 * @param longitude
	 */
	public GeoTile(String latitude, String longitude) {
		this(Double.parseDouble(latitude), Double.parseDouble(longitude));
	}

	/**
	 * Construction of GeoTile from a long value
	 * 
	 * @param tile
	 */
	public GeoTile(long tile) {
		super();
		this.tile = tile;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (tile ^ (tile >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GeoTile other = (GeoTile) obj;
		if (tile != other.tile)
			return false;
		return true;
	}

	/**
	 * Convert Lat/Long value in the range of +/- 180
	 * 
	 * @param value
	 * @return
	 */
	private double convertToUserValue(long value) {
		double retval = value / 10000.0;
		while (retval > PI / 2)
			retval -= PI;

		while (retval < -(PI / 2))
			retval += PI;

		return retval;
	}

	/**
	 * Get Latitude within +/- 180 degrees
	 * 
	 * @return
	 */
	public double getLatitude() {
		return convertToUserValue(BitOperations.valueOf(tile, LEFT_SHIFT_BITS,
				MAX_BITS));

	}

	/**
	 * Get Longitude within +/- 180 degrees
	 * 
	 * @return
	 */
	public double getLongitude() {
		return convertToUserValue(BitOperations.valueOf(tile, 0,
				LEFT_SHIFT_BITS));
	}

	/**
	 * get Distance of this geotile from other
	 * 
	 * @param other
	 * @return
	 */
	public double getDistanceFrom(GeoTile other) {
		// Approx distance is assuming constant size of tile, which is not the
		// case
		// System.out.println("Approx Distance = " +
		// getApproxDistanceFrom(other));
		double exactDistance = getExactDistanceFrom(other);

		return exactDistance;
	}

	/**
	 * CalculateDistance with the help of longitude and latitude.
	 *
	 * @param lat1
	 *            the lat1
	 * @param lon1
	 *            the lon1
	 * @param lat2
	 *            the lat2
	 * @param lon2
	 *            the lon2
	 * @return double
	 */
	public static double calculateDistance(double lat1, double lon1,
			double lat2, double lon2) {
		lat1 = Math.toRadians(lat1);
		lat2 = Math.toRadians(lat2);
		lon1 = Math.toRadians(lon1);
		lon2 = Math.toRadians(lon2);
		double finalLat = Math.toRadians(lat2 - lat1);
		double finalLon = Math.toRadians(lon2 - lon1);
		double cal = Math.sin(finalLat / 2) * Math.sin(finalLat / 2)
				+ Math.cos(lat1) * Math.cos(lat1) * Math.sin(finalLon / 2)
				* Math.sin(finalLon / 2);
		double cal1 = 2 * Math.atan2(Math.sqrt(cal), Math.sqrt(1 - cal));
		double d = EARTH_RADIUS * cal1;
		return Math.toDegrees(d);
	}

	/**
	 * Utility function to give Latitude*10000
	 * 
	 * @param tile
	 * @return
	 */
	public static long getLatComponent(long tile) {
		return BitOperations.valueOf(tile, 0, LEFT_SHIFT_BITS);
	}

	/**
	 * Utility function to give Longitude*10000
	 * 
	 * @param tile
	 * @return
	 */
	public static long getLongComponent(long tile) {
		return BitOperations.valueOf(tile, LEFT_SHIFT_BITS, MAX_BITS);
	}

	/**
	 * Simple function to calulate approximate distance between two tiles
	 * 
	 * @param other
	 * @return
	 */
	public double getApproxDistanceFrom(GeoTile other) {
		return calculateApproxDistance(tile, other.tile);
	}

	/**
	 * Internal stored value of tile
	 * 
	 * @return
	 */
	public long getTileValue() {
		return tile;
	}

	private static double calculateApproxDistance(long tile1, long tile2) {
		long latOffset = Math.abs(getLatComponent(tile1)
				- getLatComponent(tile2));
		long longOffset = Math.abs(getLongComponent(tile1)
				- getLongComponent(tile2));

		double approxDistance = Math.sqrt(latOffset * latOffset + longOffset
				* longOffset);
		return approxDistance * tileSize;
	}

	private double getExactDistanceFrom(GeoTile other) {
		return calculateDistance(getLatitude(), getLongitude(),
				other.getLatitude(), other.getLongitude());
	}

	private long store(long latitude, long longitude) {
		return BitOperations.shiftLeft(latitude, LEFT_SHIFT_BITS) + longitude;
	}

	private long convertToStorableValue(double angleInDegrees) {
		// adjust within positive 0-360 range
		while (angleInDegrees < 0)
			angleInDegrees += PI;

		while (angleInDegrees > PI)
			angleInDegrees -= PI;

		angleInDegrees *= 10000; // round off to 4 digits

		return Math.round(angleInDegrees);
	}

	@Override
	public int compareTo(GeoTile other) {
		if (other.getLatitude() < getLatitude()
				&& other.getLongitude() < getLongitude())
			return +1;

		if (other.getLatitude() > getLatitude()
				&& other.getLongitude() > getLongitude())
			return -1;

		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GeoTile [latitude = " + getLatitude() + ", longitude = "
				+ getLongitude() + "]";
	}

	public static class GeoTileJson {
		public double getLatitude() {
			return latitude;
		}

		public void setLatitude(double latitude) {
			this.latitude = latitude;
		}

		public double getLongitude() {
			return longitude;
		}

		public void setLongitude(double longitude) {
			this.longitude = longitude;
		}

		double latitude;
		double longitude;
	}

	public static GeoTile fromJson(GeoTileJson lowerLeft) {
		return new GeoTile(lowerLeft.latitude, lowerLeft.longitude);
	}

	public GeoTileJson toJson() {
		GeoTileJson geoTileJson = new GeoTileJson();
		geoTileJson.latitude = getLatitude();
		geoTileJson.longitude = getLongitude();

		return geoTileJson;
	}

	/**
	 * Get geo tile based on direction
	 * 
	 * @param direction
	 * @param size
	 * @return
	 */
	public GeoTile getTile(Direction direction, double size) {
		double angleOffset = 0.0;
		if (direction.name().equals("NORTH"))
			angleOffset = Math.toDegrees(Math.atan(size / size));
		else
			angleOffset = MapUtil.BaseAngle
					+ Math.toDegrees(Math.atan(size / size));

		return getTile(angleOffset, size);
	}

	/**
	 * Get geo tile based on angle
	 * 
	 * @param angle
	 * @param offset
	 * @return
	 */
	public GeoTile getTile(double angle, double offset) {

		// calculate diagonal
		double diagonal = MapUtil.calculateDiagonal(offset / 2, offset / 2);

		// calculate latitude;
		double latitude = MapUtil.calculateLatitude(this.getLatitude(), angle,
				diagonal);

		// calculate lon1
		double longitude = MapUtil.calculateLongitude(this.getLatitude(),
				this.getLongitude(), latitude, angle, diagonal);

		return new GeoTile(latitude, longitude);
	}
}
