package com.cariq.toolkit.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.model.Spotlight.Spotlight_In;
import com.cariq.toolkit.model.Spotlight.Spotlight_Out;
import com.cariq.toolkit.utils.JSONable;

/**
 * A beam projected on narrow window which shows important elements in domain(Proxies or accounts).
 * 
 * @author sagar
 *
 */

@Entity
@Configurable
public class Spotlight extends PersistentObject<Timeline> implements JSONable<Spotlight_In, Spotlight_Out, IdJSON> {

	// SECTION 1 -Code for Persistence
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@Version
	Integer version;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new Timeline().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null)
			this.entityManager = entityManager();
		return entityManager;
	}

	/**
	 * @param id
	 * @return
	 */
	protected static Timeline find(Long id) {
		return entityManager().find(Timeline.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public Timeline findById(Long id) {
		return find(id);
	}
	

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	//name of spotlight
	@Column(unique=true)
	String name;
	
	//description of spotlight
	String description;
	
	//number of elements visible in spotlight
	int size;
	
	//name of exporter which exports elements for this spotlight
	String dataStream;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;
	
	/**
	 * Default constructor
	 */
	public Spotlight() {
		super();
	}
	
	/**
	 * Parameterized constructor 
	 * @param name
	 * @param description
	 * @param size
	 * @param dataStream
	 */
	public Spotlight(String name, String description, int size, String dataStream) {
		super();
		this.name = name;
		this.description = description;
		this.size = size;
		this.dataStream = dataStream;
	}
	
	/**
	 * Get name of spotlight
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Get description of spotlight
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Get size of spotlight
	 * @return
	 */
	public int getSize() {
		return size;
	}

	/**
	 * Get datastream of spotlight
	 * @return
	 */
	public String getDataStream() {
		return dataStream;
	}	
	
	/**
	 * Set name of spotlight
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Set description of spotlight
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Set size of spotlight
	 * @param size
	 */
	public void setSize(int size) {
		this.size = size;
	}

	/**
	 * Set datastream for spotlight
	 * @param dataStream
	 */
	public void setDataStream(String dataStream) {
		this.dataStream = dataStream;
	}

	/**
	 * Set modified on spotlight
	 * @param modifiedOn
	 */
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------
	// Implementation of toString and hashCode and equals are advisable for all
	// Model classes
	@Override
	public String toString() {
		return "Spotlight [id=" + id + ", version=" + version + ", name =" + name + 
				", description =" + description + ", size =" + size + ", dataStream=" + dataStream + 
				", createdOn=" + createdOn + ", modifiedOn=" + modifiedOn + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + size;
		result = prime * result + ((dataStream == null) ? 0 : dataStream.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Spotlight other = (Spotlight) obj;
		if (name == null) {
			if (other.getName() != null)
				return false;
		} else if (!description.equals(other.getDescription()))
			return false;
		if (size != other.getSize()) {
			return false;
		} else if (!dataStream.equals(other.getDataStream()))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		return true;
	}

	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API
	// interaction
	// In Class (for construction)
	public static class Spotlight_In {
		private String name;
		private String description;
		private int size;
		private String dataStream;
		
		/**
		 * Spotlight In Constructor
		 * 
		 * @param name
		 * @param description
		 * @param size
		 * @param dataStream
		 */
		public Spotlight_In(String name, String description, int size, String dataStream) {
			this.name = name;
			this.description = description;
			this.size = size;
			this.dataStream = dataStream;
		}

		/**
		 * Get name
		 * @return
		 */
		public String getName() {
			return name;
		}

		/**
		 * Get description
		 * @return
		 */
		public String getDescription() {
			return description;
		}

		/**
		 * Get size
		 * @return
		 */
		public int getSize() {
			return size;
		}

		/**
		 * Get datastream
		 * @return
		 */
		public String getDataStream() {
			return dataStream;
		}

		/**
		 * Set name
		 * @param name
		 */
		public void setName(String name) {
			this.name = name;
		}

		/**
		 * Set description
		 * @param description
		 */
		public void setDescription(String description) {
			this.description = description;
		}

		/**
		 * Set size
		 * @param size
		 */
		public void setSize(int size) {
			this.size = size;
		}

		/**
		 * Set data stream
		 * @param dataStream
		 */
		public void setDataStream(String dataStream) {
			this.dataStream = dataStream;
		}
	}
	
	//Out json for spotlight
	public static class Spotlight_Out extends Spotlight_In {
		private Long id;
		
		public Spotlight_Out(Long id, String name, String description, int size, String dataStream) {
			super(name, description, size, dataStream);
			this.id = id;
		}
		public Long getId() {
			return id;
		}
	}

	@Override
	public Spotlight_Out toJSON() {
		return new Spotlight_Out(id, name, description, size, dataStream);
	}

	@Override
	public void fromJSON(Spotlight_In json) {
		this.name = json.getName();
		this.description = json.getDescription();
		this.size = json.getSize();
		this.dataStream = json.getDataStream();
	}

	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(id);
	}
	
	/**
	 * Enlist all spotlights
	 * 
	 * @param pageNo	page number
	 * @param pageSize	page size
	 * @return			list of spotlight
	 */
	public static List<Spotlight> enlist(int pageNo, int pageSize) {
		CarIQSimpleQuery<Spotlight> enlistQuery = 
				new CarIQSimpleQuery<>("Spotlight.enlistQuery", Spotlight.entityManager(), Spotlight.class);
		enlistQuery.setPaging(pageNo, pageSize);
		return enlistQuery.getResultList();
	}
	
	/**
	 * Get spotlight by name
	 * @param spotlightName
	 * @return
	 */
	public static Spotlight getByName(String spotlightName) {
		CarIQSimpleQuery<Spotlight> getByNameQuery = 
				new CarIQSimpleQuery<>("Spotlight.getByNameQuery", Spotlight.entityManager(), Spotlight.class);
		getByNameQuery.addCondition("name", "=", spotlightName);
		return getByNameQuery.getSingleResult();
	}
	
	
	/**
	 * Create or get spotlight using spotlight in json
	 * 
	 * @param spotlightIn
	 * @return
	 */
	public static Spotlight createOrGetSpotLight(Spotlight_In spotlightIn) {
		Spotlight spotLight = Spotlight.getByName(spotlightIn.getName());
		if(spotLight != null) {
			//spotlight is present, then update, merge and return.
			String name = spotLight.getName();
			String description = spotLight.getDescription();
			int size = spotLight.getSize();
			String dataStream = spotLight.getDataStream();
			
			spotLight.setName(spotlightIn.getName() != null 
					&& !spotlightIn.getName().equals(name) ? spotlightIn.getName() : name);
			spotLight.setDescription(spotlightIn.getDescription() != null && 
					!spotlightIn.getDescription().equals(description) ? spotlightIn.getDescription() : description);
			spotLight.setSize(spotlightIn.getSize() != size	? spotlightIn.getSize() : size);
			spotLight.setDataStream(spotlightIn.getDataStream() != null &&
					!spotlightIn.getDataStream().equals(dataStream) ? spotlightIn.getDataStream() : dataStream);
			spotLight.setModifiedOn(new Date());
			
			spotLight.merge();
			return spotLight;
		}
		//SpotLight is not present, then create and return
		spotLight = new Spotlight(spotlightIn.getName(), spotlightIn.getDescription()
						, spotlightIn.getSize(), spotlightIn.getDataStream());
		spotLight.persist();
		return spotLight;
	}
}
