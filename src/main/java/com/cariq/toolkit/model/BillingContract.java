package com.cariq.toolkit.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.BillingContract.BillingContract_In;
import com.cariq.toolkit.model.BillingContract.BillingContract_Out;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.GenericJsonable;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;
import com.cariq.toolkit.utils.Utils;

/**
 * This class implements "billing contract"
 * Its the deal between two parties/accounts specifying billing terms and contract period that both agreed upon. 
 * @author amita
 *
 */

@Entity
@Configurable
public class BillingContract extends PersistentObject<BillingContract> implements JSONable<BillingContract_In, BillingContract_Out, IdJSON>, GenericJsonable {

	// SECTION 1 -Code for Persistence
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@Version
	Integer version;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new BillingContract().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null)
			this.entityManager = entityManager();
		return entityManager;
	}

	/**
	 * @param id
	 * @return
	 */
	protected static BillingContract find(Long id) {
		return entityManager().find(BillingContract.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public BillingContract findById(Long id) {
		return find(id);
	}

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/**
	 * Model properties
	 */
	@NotNull(message = "@NotNull BillingContract Name")
	@Column(unique = true)
	String name;
	
	@NotNull
	@ManyToOne
	BillingAccount account_1;

	@NotNull
	@ManyToOne
	BillingAccount account_2;

	@Size(max = 4000)
	String additionalInfo;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date startDate;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date endDate;


	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	// Default constructor is a must!
	public BillingContract() {
		super();
	}

	/**
	 * Parameterized constructor
	 * 
	 * @param account_1
	 * @param account_2
	 * @param additionalInfo
	 * @param startDate
	 * @param endDate
	 * @param createdOn
	 * @param modifiedOn
	 */

	public BillingContract(String name, BillingAccount account_1, BillingAccount account_2, String additionalInfo, Date startDate,
			Date endDate, Date createdOn, Date modifiedOn) {
		super();
		this.name = name;
		this.account_1 = account_1;
		this.account_2 = account_2;
		this.additionalInfo = additionalInfo;
		this.startDate = startDate;
		this.endDate = endDate;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
	}

	/**
	 * Getters Setters
	 */	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public BillingAccount getAccount_1() {
		return account_1;
	}

	public void setAccount_1(BillingAccount account_1) {
		this.account_1 = account_1;
	}

	public BillingAccount getAccount_2() {
		return account_2;
	}

	public void setAccount_2(BillingAccount account_2) {
		this.account_2 = account_2;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	
	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------

	// Implementation of toString and hashCode and equals are advisable for all
	// Model classes
	@Override
	public String toString() {
		return "BillingContract [id=" + id + ", version=" + version + ", name" + name + ", account_1=" + account_1.getId() + ", account_2=" + account_2.getId()
				+ ", additional_Info=" + additionalInfo + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", createdOn=" + createdOn + ", modifiedOn=" + modifiedOn + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((account_1 == null) ? 0 : account_1.getName().hashCode());
		result = prime * result + ((account_2 == null) ? 0 : account_2.getName().hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BillingContract other = (BillingContract) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (account_1 == null) {
			if (other.account_1 != null)
				return false;
		} else if (!account_1.equals(other.account_1))
			return false;
		if (account_2 == null) {
			if (other.account_2 != null)
				return false;
		} else if (!account_2.equals(other.account_2))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (additionalInfo == null) {
			if (other.additionalInfo != null)
				return false;
		} else if (!additionalInfo.equals(other.additionalInfo))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		return true;
	}

	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API
	// interaction
	
	// In Class (for construction)
	public static class BillingContract_In {
		String name;
		String account_1_name;
		String account_2_name;
		String startDate;
		String endDate;
		String additionalInfo;

		public BillingContract_In() {
			super();
		}

		public BillingContract_In(String name, String account_1_name, String account_2_name, String startDate, String endDate,	String additionalInfo) {
			super();
			this.name = name;
			this.account_1_name = account_1_name;
			this.account_2_name = account_2_name;
			this.startDate = startDate;
			this.endDate = endDate;
			this.additionalInfo = additionalInfo;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getAccount_1() {
			return account_1_name;
		}

		public void setAccount_1(String account_1_name) {
			this.account_1_name = account_1_name;
		}

		public String getAccount_2() {
			return account_2_name;
		}

		public void setAccount_2(String account_2_name) {
			this.account_2_name = account_2_name;
		}

		public String getStartDate() {
			return startDate;
		}

		public void setStartDate(String startDate) {
			this.startDate = startDate;
		}

		public String getEndDate() {
			return endDate;
		}

		public void setEndDate(String endDate) {
			this.endDate = endDate;
		}

		public String getAdditionalInfo() {
			return additionalInfo;
		}

		public void setAdditionalInfo(String additionalInfo) {
			this.additionalInfo = additionalInfo;
		}

		@Override
		public String toString() {
			return "BillingContract_In [name=" + name + ", account_1=" + account_1_name + ", account_2=" + account_2_name
					+ ", additional_Info=" + additionalInfo + ", startDate=" + startDate + ", endDate=" + endDate + "]";
		}
	}

	// Out class for output
	public static class BillingContract_Out extends BillingContract_In {
		Long id;
		String createdOn;
		String modifiedOn;

		public BillingContract_Out() {
			super();
		}

		public BillingContract_Out(String name, String account_1_name, String account_2_name, String startDate, String endDate, 
				String additionalInfo, String createdOn, String modifiedOn, Long id) {
			super(name, account_1_name, account_2_name, startDate, endDate,	additionalInfo);
			this.id = id;
			this.createdOn = createdOn;
			this.modifiedOn = modifiedOn;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toJSON()
	 */
	@Override
	public BillingContract_Out toJSON() {
		return new BillingContract_Out(name, account_1.getName(), account_2.getName(), startDate.toString(), 
				endDate.toString(), additionalInfo, createdOn.toString(), modifiedOn.toString(), id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#fromJSON(java.lang.Object)
	 */
	@Override
	public void fromJSON(BillingContract_In json) {
		this.name = json.getName();
		this.account_1 =  json.getAccount_1() != null ? BillingAccount.getByName(json.getAccount_1()) : null;
		this.account_2 =  json.getAccount_2() != null ? BillingAccount.getByName(json.getAccount_2()) : null;
		this.additionalInfo = json.getAdditionalInfo();
		this.startDate = json.getStartDate() != null ? Utils.getDate(json.getStartDate()) : null;
		this.endDate = json.getEndDate() != null ? Utils.getDate(json.getEndDate()) : null;
		this.createdOn = new Date();
		this.modifiedOn = new Date();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}
	
	@Override
	public GenericJSON toGenericJSON() {		
		return GenericJSON.build("name", name, "account_1", account_1.getName(), "account_2", account_2.getName(), 
				"StartDate", startDate.toString(), "EndDate", endDate.toString(), "AdditionalInfo", additionalInfo, 
				"CreatedOn", createdOn.toString(), "ModifiedOn", modifiedOn.toString(), "Id", id);
	}

	
	@Override
	public void fromGenricJSON(GenericJSON json) {
		Utils.notImplementedException("BillingContract.fromGenricJSON");
	}

	// ---------------------------------------------
	// SECTION 5 - Query and Class specific finders and static utility methods
	// ---------------------------------------------
	public static BillingContract findObjectById(Long id) {
		return find(id);
	}
	
	public static List<BillingContract> getAllObjects(int pageNo, int pageSize) {
		CarIQSimpleQuery<BillingContract> qry = new CarIQSimpleQuery<BillingContract>("getAllObjects", entityManager(), BillingContract.class);
		qry.setPaging(pageNo, pageSize);

		return qry.getResultList();
	}

	public static BillingContract getByAccount(BillingAccount account) {
		TypedQuery<BillingContract> query = BillingContract.entityManager()
				.createQuery("SELECT bc FROM BillingContract bc WHERE bc.account_1 = :account OR bc.account_2 = :account", BillingContract.class);
		query.setParameter("account", account);

		if (query.getResultList().isEmpty())
			return null;
		return query.getSingleResult();
	}
	
	public static BillingContract getByNameAndAccount(String name, BillingAccount account) {
		TypedQuery<BillingContract> query = BillingContract.entityManager()
				.createQuery("SELECT bc FROM BillingContract bc WHERE bc.name = :name and (bc.account_1 = :account OR bc.account_2 = :account)", BillingContract.class);
		query.setParameter("account", account);
		query.setParameter("name", name);

		if (query.getResultList().isEmpty())
			return null;
		return query.getSingleResult();
	}
	
	public static BillingContract getByName(String name) {
		CarIQSimpleQuery<BillingContract> qry = new CarIQSimpleQuery<BillingContract>("getByName", entityManager(), BillingContract.class);
		qry.addCondition("name", "=", name);

		if (qry.getResultList().isEmpty())
			return null;
		return qry.getSingleResult();
	}

	// Update can be done only fields other than accounts involved.
	public void update(BillingContract_In params) {
		if (params.additionalInfo != null)
			this.additionalInfo = params.additionalInfo;
		if (params.startDate != null)
			this.startDate = Utils.getDate(params.startDate);
		if (params.endDate != null)
			this.endDate = Utils.getDate(params.endDate);

		this.modifiedOn = new Date();
		this.persist();
	}
}