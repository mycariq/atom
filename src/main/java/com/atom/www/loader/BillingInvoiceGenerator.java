package com.atom.www.loader;

import java.util.ArrayList;
import java.util.List;

import com.atom.www.exporter.BillingInvoiceExporter;
import com.atom.www.exporter.InvoiceDetailsExporter;
import com.atom.www.exporter.StagingAndPolicyDetailsReportExporter;
import com.atom.www.exporter.TATForMISExporter;
import com.cariq.toolkit.coreiq.exporter.ExporterWorkDefinition;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

public class BillingInvoiceGenerator extends MultipleExporterAsyncLoader {
	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("BillingInvoiceGenerator");

	public static final String TheLoader = "BillingInvoiceGenerator";
	public static final String CreatedAfter = "CreatedAfter";

	public BillingInvoiceGenerator() {
		super(TheLoader, "Do Billing Operation and Generate Invoice");
		
		addAttribute(BillingInvoiceExporter.CONTRACT, "Billing contract name", false);
		addAttribute(BillingInvoiceExporter.RAISED_BY_ACCOUNT, "Name of the account raising the bill", false);
		addAttribute(BillingInvoiceExporter.RAISED_AGAINST_ACCOUNT, "Name of the account to raise the billing against", false);
		addAttribute(BillingInvoiceExporter.FROM_MONTH, "From month (MMMM-yyyy)", false);
		addAttribute(BillingInvoiceExporter.TO_MONTH, "To month (MMMM-yyyy)", false);
		addAttribute(BillingInvoiceExporter.BILLINGTERMS, "Comma separated billing term names under the contract OR All", false);
		addAttribute(CreatedAfter, "TAT based on data created After", true);
	}

	@Override
	protected List<ExporterWorkDefinition> getExporterWorkerDefinitions(GenericJSON json) {
		List<ExporterWorkDefinition> retval = new ArrayList<ExporterWorkDefinition>();
		// This requires 2 Exporters, first is TAT exporter, and second is MIS
		String createdAfter = Utils.getValue(String.class, json, CreatedAfter);
		if (createdAfter == null)
			createdAfter = "2001-01-01";

		retval.add(new ExporterWorkDefinition(TATForMISExporter.TheExporter,
				StagingAndPolicyDetailsReportExporter.QueryType_createdAfterIncludeInvalid, "csv",
				GenericJSON.build(StagingAndPolicyDetailsReportExporter.createdAfter, createdAfter)));

		// second is the BillingInvoice exporter
		retval.add(new ExporterWorkDefinition(BillingInvoiceExporter.EXPORTER,
				BillingInvoiceExporter.FROM_TAT_URL_LATEST, "html",
				GenericJSON.build(BillingInvoiceExporter.CONTRACT,
						Utils.getValue(String.class, json, BillingInvoiceExporter.CONTRACT),
						BillingInvoiceExporter.RAISED_BY_ACCOUNT,
						Utils.getValue(String.class, json, BillingInvoiceExporter.RAISED_BY_ACCOUNT),
						BillingInvoiceExporter.RAISED_AGAINST_ACCOUNT,
						Utils.getValue(String.class, json, BillingInvoiceExporter.RAISED_AGAINST_ACCOUNT),
						BillingInvoiceExporter.FROM_MONTH,
						Utils.getValue(String.class, json, BillingInvoiceExporter.FROM_MONTH),
						BillingInvoiceExporter.TO_MONTH,
						Utils.getValue(String.class, json, BillingInvoiceExporter.TO_MONTH),
						BillingInvoiceExporter.BILLINGTERMS,
						Utils.getValue(String.class, json, BillingInvoiceExporter.BILLINGTERMS))));

		// generate invoice report
		retval.add(new ExporterWorkDefinition(InvoiceDetailsExporter.EXPORTER, InvoiceDetailsExporter.BY_LATEST_REPORT,
				"html", new GenericJSON()));

		return retval;
	}

}
