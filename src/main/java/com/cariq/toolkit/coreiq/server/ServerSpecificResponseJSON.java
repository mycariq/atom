package com.cariq.toolkit.coreiq.server;

import java.util.Date;
import java.util.List;

import com.cariq.toolkit.utils.ResponseJson;

public class ServerSpecificResponseJSON extends ResponseJson implements ServerSpecificResponse{
	String serverName;
	Date serverStartTime;

	public ServerSpecificResponseJSON() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ServerSpecificResponseJSON(List<String> messages, String serverName, Date serverStartTime) {
		super(messages);
		this.serverName = serverName;
		this.serverStartTime = serverStartTime;
	}
	
	public ServerSpecificResponseJSON(String message, String serverName, Date serverStartTime) {
		super(message);
		this.serverName = serverName;
		this.serverStartTime = serverStartTime;
	}

	@Override
	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	@Override
	public Date getServerStartTime() {
		return serverStartTime;
	}

	public void setServerStartTime(Date serverStartTime) {
		this.serverStartTime = serverStartTime;
	}
}
