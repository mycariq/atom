package com.cariq.toolkit.utils.wwwservice.core.function;

import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.utils.wwwservice.api.W3FunctionOutput;
import com.cariq.toolkit.utils.wwwservice.core.ArithmeticFunction;
import com.cariq.toolkit.utils.wwwservice.core.ArithmeticFunction.FunctionProcessingInfo;
import com.cariq.toolkit.utils.wwwservice.core.Function.FunctionType;
import com.cariq.toolkit.utils.wwwservice.core.Parameter;
import com.cariq.toolkit.utils.wwwservice.core.ParameterDef;
import com.cariq.toolkit.utils.wwwservice.core.Tuple;

public class Max extends ArithmeticFunction {
	static class MaxProcessingInfo extends FunctionProcessingInfo {
		Tuple tuple;
		
		public MaxProcessingInfo(ParameterDef def) {
			super(def);
			tuple = null;
			value = Double.MIN_VALUE;
		}

		@Override
		public Parameter addValue(Tuple tuple, double paramValue) {
			if (paramValue > value) {
				value = paramValue;
				this.tuple = tuple;
			}
			return null;
		}

		/**
		 * @return the tuple
		 */
		public Tuple getTuple() {
			return tuple;
		}
	}
	
	@Override
	public FunctionType getType() {
		return FunctionType.MAX;
	}

	@Override
	protected FunctionProcessingInfo getProcessingInfo(ParameterDef paramDef) {
		return new MaxProcessingInfo(paramDef);
	}

	/**
	 * Max and Min have special implementation. They have each parameter with a tuple attached.
	 */
	@Override
	protected List<W3FunctionOutput> doGetOutput() {
		// Single record from the AverageInfo
		ArrayList<W3FunctionOutput> output = new ArrayList<W3FunctionOutput>();

		for (FunctionProcessingInfo info : processingInfoMap.values()) {
			MaxProcessingInfo maxInfo = (MaxProcessingInfo) info;
			W3FunctionOutput out = new W3FunctionOutput(maxInfo.getTuple());
			out.addParameter(new Parameter(info.getDef(), info.getValue()));
			
			output.add(out);
		}
		
		return output;
	}
}
