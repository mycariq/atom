package com.atom.www.loader;

import java.util.ArrayList;
import java.util.List;

import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderContext;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;


/**
 * This loader assigns owner to proxies on different conditions.
 * Client of this loader has to give name of condition. Based on name of conditions,
 * different parameters of case(policy) will be compared. And only those which match 
 * condition will be used for owner resettlement.
 * 
 * @author sagar
 *
 */
public class ConditionBasedOwnerAssignmentLoader extends OwnerTransferLoaderTemplate {

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("ConditionBasedOwnerAssignmentLoader");
	
	public static final String TheLoader = "ConditionBasedOwnerAssignmentLoader";
	public static final String TheDescription = "This loader assigns owner to proxies on different conditions.";
	
	public static final String ATTR_CONDITION = "Condition";
	
	public static final String CONDITION_RENEWAL = "RENEWAL";
	public static final String CONDITION_RETRIEVAL = "RETRIEVAL";
	
	public static final String[] CONDITIONS = { CONDITION_RENEWAL, CONDITION_RETRIEVAL};
	
	/**
	 * Default constructor.
	 */
	public ConditionBasedOwnerAssignmentLoader() {
		super(TheLoader, TheDescription);
		
		addAttribute(ATTR_CONDITION, "Name of Condition(RENEWAL, RETRIEVAL). Note: Provide only one condition.", false);
		addAttribute("CreatedAfter", "Date of proxies created after in YYYY-MM-DD HH:MM format.", false);
	}


	@Override
	protected void doLoad(GenericJSON json, CarIQLoaderContext context) {
		
		String condition = Utils.getValue(json, ATTR_CONDITION);
		
		//Validate attribute before loading.
		if(Utils.isValueIn(null, condition))
			Utils.handleException("Please enter valid condition.");
		
		if(!Utils.isValueIn(condition, CONDITIONS))
			Utils.handleException("Given condition not exists!");
		
		super.doLoad(json, context);
	}
	

	@Override
	List<GenericJSON> filterWithCondition(List<GenericJSON> policies, GenericJSON inputJSON) {
		
		List<GenericJSON> filteredPolicies = new ArrayList<GenericJSON>();
		
		String condition = Utils.getValue(inputJSON, ATTR_CONDITION);
		
		for(GenericJSON policy: policies) {
			String nextAction = Utils.getValue(policy, "NextAction");
			
			//If nextAction is null then this object is active(Not renewal or retrieval)
			if(nextAction == null)
				continue;
			
			//If condition is renewal & next action is also renewal then add object in final list
			if(condition.equalsIgnoreCase(CONDITION_RENEWAL) 
					&& nextAction.equalsIgnoreCase("Due for renewal")) {
				filteredPolicies.add(policy);
				logger.debug("Found renewal object, adding in final list.");
				continue;
			}
			
			//If condition is retrieval and next action is also renewal then add object in final list.
			if(condition.equalsIgnoreCase(CONDITION_RETRIEVAL) 
					&& nextAction.equalsIgnoreCase("Due for retrieval")) {
				filteredPolicies.add(policy);
				logger.debug("Found retrieval object, adding in final list.");
			}
		}
		return filteredPolicies;
	}


	@Override
	String getQueryType() {
		return StagingAndPolicyHelper.QueryType_ByPolicyNumber;
	}

}
