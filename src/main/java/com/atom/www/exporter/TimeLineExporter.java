/**
 * 
 */
package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import com.cariq.toolkit.coreiq.CarIQRawQuery;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.model.Timeline;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * The Class TimeLineExporter.
 *
 * @author santosh
 */
public class TimeLineExporter extends CarIQExporterTemplate {

	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("TimeLineExporter");

	public static final String EXPORTER = "TimeLineExporter";

	private static final LinkedHashMap<String, String> attributeAliasMap = new LinkedHashMap<String, String>();
	private static final List<String> tableAliasArray = new ArrayList<>();

	public static final String ALL = "All", DOMAIN_NAME = "domainName", START_DATE = "startDate", END_DATE = "endDate",
			DOMAIN_AND_DATE_RANGE = "DomainAndDateRange";

	private static final String firstName = "firstName", lastName = "lastName", userName = "userName", role = "role",
			domain = "domain", type = "type", tag = "tag", stage = "stage", message = "message",
			timelineCreatedOn = "timelineCreatedOn", status = "status", appointmentCreatedOn = "appointmentCreatedOn",
			appointmentOn = "appointmentOn", objectId = "objectId", objectType = "objectType", signature = "signature";

	static {
		attributeAliasMap.put("user.first_name", firstName);
		attributeAliasMap.put("user.last_name", lastName);
		attributeAliasMap.put("user.username", userName);
		attributeAliasMap.put("user.role", role);

		attributeAliasMap.put("domain.name", domain);

		attributeAliasMap.put("timeline.type", type);
		attributeAliasMap.put("timeline.tag", tag);
		attributeAliasMap.put("timeline.stage", stage);
		//attributeAliasMap.put("timeline.message", message);
		attributeAliasMap.put("DATE_FORMAT(timeline.created_on, '%Y-%m-%d')", timelineCreatedOn);

		//attributeAliasMap.put("appointment.status", status);
		//attributeAliasMap.put("DATE_FORMAT(appointment.appointment_on, '%Y-%m-%d')", appointmentOn);
		//attributeAliasMap.put("DATE_FORMAT(appointment.created_on, '%Y-%m-%d')", appointmentCreatedOn);

		attributeAliasMap.put("proxy.object_id", objectId);
		attributeAliasMap.put("proxy.object_type", objectType);
		attributeAliasMap.put("proxy.signature", signature);
		
		tableAliasArray.add("timeline timeline,");
		tableAliasArray.add("user user,");
		tableAliasArray.add("domain domain,");
		tableAliasArray.add("domain_access domainAccess,");
		tableAliasArray.add("proxy_object proxy");
		
		//tableAliasArray.add("LEFT JOIN proxy_object proxy ON proxy.id = timeline.proxy");
		//tableAliasArray.add("LEFT JOIN appointment appointment ON appointment.proxy = proxy.id");
		//tableAliasArray.add("LEFT JOIN user user ON user.id = proxy.owner");
		//tableAliasArray.add("LEFT JOIN domain domain ON domain.id = proxy.domain");
	}

	public TimeLineExporter() {
		super(EXPORTER, "Timeline Exporter");

		// Declare Export attributes
		addExportAttributes(new ArrayList(attributeAliasMap.values()));

		addQueryType(DOMAIN_AND_DATE_RANGE, "timeline exporter to given date range",
				Arrays.asList(
						new QueryDefinitionAttribute(DOMAIN_NAME, "domain Name(ALL/perticular domain name)",
								String.class),
						new QueryDefinitionAttribute(START_DATE, "Start Date", Date.class),
						new QueryDefinitionAttribute(END_DATE, "End Date", Date.class)));

		// Support roles
		addSupportedRoles(Arrays.asList(Utils.ADMIN_ROLE));
	}

	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		try (ProfilePoint _doExport = ProfilePoint.profileAction("TimeLineExporter_doExport")) {

			CarIQRawQuery qry = getExportQuery(queryType, json, pageNo, pageSize);
			return qry.getResult();
		}
	}

	private CarIQRawQuery getExportQuery(String queryType, GenericJSON json, int pageNo, int pageSize) {
		try (ProfilePoint _getExportQuery = ProfilePoint.profileAction("TimeLineExporter_getExportQuery")) {
			CarIQRawQuery qry = new CarIQRawQuery("getTimeLineExportQuery", Timeline.entityManager(), attributeAliasMap,
					tableAliasArray);
			qry.addRawCondition(
					"user.id = timeline.user and user.id = domainAccess.user and domain.id = domainAccess.domain and proxy.id = timeline.proxy");

			if (!DOMAIN_AND_DATE_RANGE.equalsIgnoreCase(queryType))
				Utils.handleException("Invalid Query Type: " + queryType);

			Date startDate = Utils.getValue(Date.class, json, START_DATE);
			Date endDate = Utils.getNextDate(Utils.getValue(Date.class, json, END_DATE));
			Utils.checkNotNull(startDate, "Invalid start date", endDate, "Invalid end date");

			String domainName = Utils.getValue(json, DOMAIN_NAME);
			if (!ALL.equalsIgnoreCase(domainName))
				qry.addRawCondition("domain.name in (" + Utils.getStringifyList(Utils.getList(domainName)) + ")");

			qry.addRawCondition("(timeline.created_on >= " + Utils.getSQLDate(startDate) + " AND timeline.created_on <= "
					+ Utils.getSQLDate(endDate) + ")");

			qry.setPaging(pageNo, pageSize);
			return qry;
		}
	}
}