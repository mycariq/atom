/**
 * 
 */
package com.cariq.toolkit.coreiq.exporter.fileexport;

import java.io.File;

import com.cariq.toolkit.coreiq.exporter.CarIQExporter;
import com.cariq.toolkit.utils.GenericJSON;

/**
 * @author hrishi
 *
 */
public interface FileExport {
	/**
	 * Exporter for generating the output
	 * 
	 * @return
	 */
	CarIQExporter getExporter();

	String getQueryType();

	GenericJSON getInputJSON();

	/**
	 * In which format to export
	 * 
	 * @return
	 */
	String getFileFormat();

	/**
	 * Execute Export to file and return the File object
	 * 
	 * @return
	 */
	File execute();

	/**
	 * @return
	 */
	String getMimeType();
}
