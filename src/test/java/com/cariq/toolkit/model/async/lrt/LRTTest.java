/**
 * 
 */
package com.cariq.toolkit.model.async.lrt;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.atom.www.exporter.CSVDataExporter;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.service.CarIQRestClientService;

/**
 * @author santosh
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Configurable
public class LRTTest {

	@Autowired
	CarIQRestClientService restService;

	//@Test
	public void lrtExporterTest() {

		// REST API call - for given Exporter input file url
		String domain = "ilgecu.mycariq.com";
		String url = "Cariq/admin/loader/loadSingle/StagingAndInsurancePolicyDetailsExporterLoader";
		GenericJSON json = GenericJSON.build("QueryType", "CreatedAfterIncludeInvalid", "CreatedAfter", "2001-01-01", "FileType", "csv");

		System.out.println("Making call to CarIQ to Load");

		String loaderUrl = "https://" + domain + ":443/" + url;
		String basicAuth = "Basic YXRvbWFkbWluOjU4YzAzMzBkZjM1NmE0Y2ZlMWRhM2FkNDFiYTM3OWVj";
		ResponseJson response = restService.post(json, loaderUrl, basicAuth, ResponseJson.class);

		System.out.println("Received response from CarIQ Loader: " + response);
		System.out.println("Collecting response " + response + ", object from " + domain + " domain");
		List<String> responseList = Utils.downCast(List.class, response.getMessages());
		if (Utils.isNullOrEmpty(responseList))
			return;

		System.out.println("Got response list: " + responseList.size());
		// ResponseJSON consists of messagelist - first being the FileUrl - or, LRT
		GenericJSON responseJSON = Utils.getJSonObject(responseList.get(0), GenericJSON.class);
		if (null == responseJSON)
			return;

		// In normal cases, fileJSON would be responseJSON
		GenericJSON fileJSON = responseJSON;

		System.out.println("Got file JSON: " + fileJSON);

		// check if ResponseJSON is LRT - if so, wait for 30 minutes, checking every 10
		// seconds for the update
		if (LRTClientHelper.isLRT(responseJSON)) {
			String lrtUrl = "https://" + domain + ":443/Cariq/lrt/";

			LRTJSON completedLRT = LRTClientHelper.waitForLRTResponse(LRTClientHelper.getLRTToken(responseJSON),
					30 * Utils.minuteSeconds, new LRTRestCallFetcher(restService, lrtUrl, basicAuth));
			if (null == completedLRT)
				return;

			// LRT Response is a Response JSON
			fileJSON = Utils.getJSonObject(completedLRT.getResponse().toString(), GenericJSON.class);
			if (Utils.isNullOrEmpty(fileJSON))
				return;
		}

		System.out.println(
				"Final Output file url: " + Utils.downCast(String.class, fileJSON.get(CSVDataExporter.FILE_URL)));
	}
	
	@Test
	public void replaceTest() {
		
		String address = "PLOT NO. 11, \"KARMA-YOG\", NEW S.B.H. COLONY;, \nNEAR JYOTI-NAGAR, SHAHNOORWADI AREA, \nalt no.9823363821";
		address = address.replaceAll("\"", "");
		address = address.replaceAll("\\n", "");
		address = address.replaceAll(";", "");
		System.out.println("Final Address: " + address);
	}

}
