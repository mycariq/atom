package com.cariq.toolkit.model.billing;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.billing.SimpleInvoicedBillable.SimpleInvoicedBillable_In;
import com.cariq.toolkit.model.billing.SimpleInvoicedBillable.SimpleInvoicedBillable_Out;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.GenericJsonable;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;
import com.cariq.toolkit.utils.Utils;

/**
 * This class implements "SimpleInvoicedBillable"
 * Billable is the specific entity that is charged (accounted) in the billing operation. eg: car, policy, subscription, device etc.
 * @author Hrishi
 *
 */

@Entity
@Configurable
public class SimpleInvoicedBillable extends PersistentObject<SimpleInvoicedBillable> implements JSONable<SimpleInvoicedBillable_In, SimpleInvoicedBillable_Out, IdJSON> {

	// SECTION 1 -Code for Persistence
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@Version
	Integer version;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new SimpleInvoicedBillable().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null)
			this.entityManager = entityManager();
		return entityManager;
	}

	/**
	 * @param id
	 * @return
	 */
	protected static SimpleInvoicedBillable find(Long id) {
		return entityManager().find(SimpleInvoicedBillable.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public SimpleInvoicedBillable findById(Long id) {
		return find(id);
	}

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/**
	 * Model properties
	 */
	// type = ICICI Insurance Policy of BAGIC Policy or something like that - for Driven too similar one
	@NotNull(message = "@NotNull type")
	String type;
	
	// This is the policy number - We are not using covernote here - Covernote is specific to ICICI
	@NotNull(message = "@NotNull signature")
	String signature;
	
	@NotNull(message = "@NotNull invoiceNumber")
	String invoiceNumber;
	
	// Which category, the billable is billed - Hardware, Software, Software-1 etc.
	@NotNull(message = "@NotNull category")
	String category;

	// Note is optional - it can be covernote or it can be something else.
	@Size(max = 1024)
	String note;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	// Default constructor is a must!
	public SimpleInvoicedBillable() {
		super();
	}



	public SimpleInvoicedBillable(String type, String signature, String invoiceNumber, String category, String note) {
		super();
		this.type = type;
		this.signature = signature;
		this.invoiceNumber = invoiceNumber;
		this.category = category;
		this.note = note;
		createdOn = new Date();
		modifiedOn = new Date();
		version = 1;
	}

	/**
	 * Getters - Setters
	 */
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	
	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------


	// Implementation of toString and hashCode and equals are advisable for all
	// Model classes

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((invoiceNumber == null) ? 0 : invoiceNumber.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((note == null) ? 0 : note.hashCode());
		result = prime * result + ((signature == null) ? 0 : signature.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleInvoicedBillable other = (SimpleInvoicedBillable) obj;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (invoiceNumber == null) {
			if (other.invoiceNumber != null)
				return false;
		} else if (!invoiceNumber.equals(other.invoiceNumber))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		if (note == null) {
			if (other.note != null)
				return false;
		} else if (!note.equals(other.note))
			return false;
		if (signature == null) {
			if (other.signature != null)
				return false;
		} else if (!signature.equals(other.signature))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SimpleInvoicedBillable [id=" + id + ", version=" + version + ", type=" + type + ", signature="
				+ signature + ", invoiceNumber=" + invoiceNumber + ", category=" + category + ", note=" + note
				+ ", createdOn=" + createdOn + ", modifiedOn=" + modifiedOn + "]";
	}
	
	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API
	// interaction
	
	// In Class (for construction)
	public static class SimpleInvoicedBillable_In {
		String type;
		String signature;
		String note;
		String category;
		String invoiceNumber;
		
		public SimpleInvoicedBillable_In() {
			super();
		}


		public SimpleInvoicedBillable_In(String type, String signature, String note, String category,
				String invoiceNumber) {
			super();
			this.type = type;
			this.signature = signature;
			this.note = note;
			this.category = category;
			this.invoiceNumber = invoiceNumber;
		}



		public String getCategory() {
			return category;
		}


		public void setCategory(String category) {
			this.category = category;
		}


		public String getInvoiceNumber() {
			return invoiceNumber;
		}


		public void setInvoiceNumber(String invoiceNumber) {
			this.invoiceNumber = invoiceNumber;
		}


		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getSignature() {
			return signature;
		}

		public void setSignature(String signature) {
			this.signature = signature;
		}

		public String getNote() {
			return note;
		}

		public void setNote(String note) {
			this.note = note;
		}
	}

	// Out class for output
	public static class SimpleInvoicedBillable_Out extends SimpleInvoicedBillable_In {
		Long id;
		String createdOn;
		String modifiedOn;

		public SimpleInvoicedBillable_Out() {
			super();
		}

		public SimpleInvoicedBillable_Out(String type, String signature, String note, String category,
				String invoiceNumber, String createdOn, String modifiedOn, Long id) {
			super(type, signature, note, category, invoiceNumber);
			this.id = id;
			this.createdOn = createdOn;
			this.modifiedOn = modifiedOn;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toJSON()
	 */
	@Override
	public SimpleInvoicedBillable_Out toJSON() {
		return new SimpleInvoicedBillable_Out(type, signature, note, category, invoiceNumber, createdOn.toString(), modifiedOn.toString(), id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#fromJSON(java.lang.Object)
	 */
	@Override
	public void fromJSON(SimpleInvoicedBillable_In json) {
		this.type =  json.type;
		this.signature =  json.signature;
		this.category = json.category;
		this.invoiceNumber = json.invoiceNumber;
		this.note = json.note;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}
//	
//	@Override
//	public GenericJSON toGenericJSON() {
//		return GenericJSON.build("Type", type, "Signature", signature, "Category", category, "InvoiceNumber", invoiceNumber, "Note", note, "CreatedOn", createdOn, "ModifiedOn", modifiedOn, "Id", id);
//	}
//	
//	@Override
//	public void fromGenricJSON(GenericJSON json) {
//		Utils.notImplementedException("SimpleInvoicedBillable.fromGenricJSON");
//	}
//	
	// ---------------------------------------------
	// SECTION 5 - Query and Class specific finders and static utility methods
	// ---------------------------------------------
	public static SimpleInvoicedBillable findObjectById(Long id) {
		return find(id);
	}
}