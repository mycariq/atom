package com.atom.www.loader;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.atom.www.exporter.OnboardingExporter;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderContext;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.service.ProxyObjectService;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * OwnerAssignment Loader assigns input owner's in round robin fashin to proxies
 * which does not have owner.
 * 
 * @author sagar
 *
 */
@Configurable
public class OwnerAssignmentLoader extends OwnerAssignmentLoaderTemplate {
	
	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("OwnerAssignmentLoader");
	
	private static final String TheLoader = "OwnerAssignmentLoader";
	private static final String TheDescription = "OwnerAssignment Loader assigns input owner's in round robin fashin to proxies"
			+ " which does not have owner.";
	
	public static final String createdAfter = "CreatedAfter";
	
	//comma seperated owners(usernames)
	public static final String owners = "Owners";
	public static final String domain = "domain";
	
	protected static final String KEY_POLICY_NO = "policyNumber";
	
	private static final String PROXY_TYPE = "InsurancePolicy";
	
	/** Proxy Object service to update proxy object **/
	@Autowired
	private ProxyObjectService proxyObjectService;

	public OwnerAssignmentLoader() {
		super(TheLoader, TheDescription);
		
		//add attributes required for this loader
		addAttribute(createdAfter, "Date of policies created after in YYYY-MM-DD HH:MM format.", false);
		addAttribute(owners, "Comma seperated usernames of owners.", false);
		addAttribute(domain, "Name of domain(IL-Assist, Driven, TAGIC, Drivesmart....", false);
		
	}

	@Override
	protected void doLoad(GenericJSON json, CarIQLoaderContext context) {
		logger.debug("doLoad: json : " + json.toString());
		
		String domainName = Utils.getValue(json, domain);
		String inputOwners = Utils.getValue(json, owners);
		
		//Check given domain exists or not
		Domain domain = Domain.getByName(domainName);
		if(domain == null) {
			context.addMessage("Domain not found for domain name : " + domainName);
			return;
		}
		
		List<String> ownersList = getValidOwners(inputOwners, domain); 
		
		//get all policies created after input date by using onboarding exporter.
		OnboardingExporter onboardingExporter = new OnboardingExporter();
		
		//get all policies created after given date
		List<GenericJSON> output = onboardingExporter.export(createdAfter, json, 1, 2000000);
		
		logger.debug("Found " + (output == null ? 0 : output.size()) + " policies");
		
		try (ProfilePoint assignOwnersOwnerAssignmentLoader = ProfilePoint
				.profileAction("ProfAction_assignOwnersOwnerAssignmentLoader")) {
			//finally create proxies/assign owners.
			assignOwners(output, ownersList, domain, false, context);
		}
	}
}
