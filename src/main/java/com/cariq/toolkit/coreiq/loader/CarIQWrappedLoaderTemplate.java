package com.cariq.toolkit.coreiq.loader;

import java.util.List;

import com.cariq.toolkit.coreiq.loader.CarIQLoaderWrapper.STATUS;
import com.cariq.toolkit.utils.CarIQAttrDefinition;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;

public abstract class CarIQWrappedLoaderTemplate implements CarIQLoader{
	CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("CarIQLoaderWrappedTemplate");
	
	protected abstract CarIQLoaderWrapper getWrapper();
	
	protected abstract void doLoad(GenericJSON json, CarIQLoaderContext context, STATUS status);
	
	CarIQLoaderDefinition loaderDefinition;
	
	public CarIQWrappedLoaderTemplate(String name, String description) {
		super();
		loaderDefinition = new CarIQLoaderDefinition(name, description);
	}
	
	@Override
	public String getName() {
		return loaderDefinition.getName();
	}

	@Override
	public String getDescription() {
		return loaderDefinition.getDescription();
	}

	@Override
	public List<CarIQAttrDefinition> getAttributes() {
		return loaderDefinition.getAttrDefinition();
	}

	@Override
	public void load(GenericJSON json, CarIQLoaderContext context) {
		CarIQLoaderContext wrappedContext = getWrapper().load(json);
		context.put("MESSAGE_QUEUE", wrappedContext.getMessages());
		
		CarIQLoaderWrapper.STATUS status = (CarIQLoaderWrapper.STATUS) 
				wrappedContext.get(CarIQLoaderWrapper.LOAD_STATUS);
		
		doLoad(json, context, status);
	}
	
	
	public void addAttribute(List<CarIQAttrDefinition> attributes) {
		for(CarIQAttrDefinition attr : attributes) {
			loaderDefinition.addAttrDefinition(attr);
		}
	}

	@Override
	public CarIQLoaderDefinition getLoaderDefinition() {
		CarIQLoaderDefinition wrappedDefinition = getWrapper().getWrappedDefinition();
		// Can't find wrapped definition for this user
		if (wrappedDefinition == null)
			return null;
		
		addAttribute(wrappedDefinition.attrDefinitions);
		return loaderDefinition;
	}

}
