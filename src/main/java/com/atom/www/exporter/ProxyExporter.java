package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Configurable;

import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;


@Configurable
public class ProxyExporter extends CarIQExporterTemplate {

	/** The Constant attributeAliasMap. */
	private static final List<String> attributeAliasMap = new ArrayList<>();
	
	private static final String TheExporter = "ProxyExporter";
	private static final String TheExporterDescription = "Exports proxy details with proxy stage and state.";
	
	private static final String QueryType_createdAfter = "CreatedAfter";
	private static final String createdAfterQueryTypeDescription = "All policies created after given date.";
	
	private static final String QueryType_Domain = 	"Domain";
	private static final String QueryTypeDomainDescription = "Domain(Ex. IL-Assist, Driven, TAGIC, Drivesmart, etc...)";
	
	private static final String QueryType_Owner = "Owner";
	private static final String QueryTypeOwnerDescription = "Username of agent. Owner 'None' is allowed(None, preeti, sweta, etc...)";
	
	private static final String QueryType_Stage = "stage";
	private static final String QueryTypeStageDescription = "Stage of proxy";
	
	private static final String QueryType_State = "state";
	private static final String QueryTypeStateDescription = "State of proxy";
	
	private static final String createdAfter = "CreatedAfter";

	private static final String KEY_Signature = "signature";

	private static final String KEY_Policy_Stage = "stage";
	private static final String KEY_Policy_State = "state";

	private static final String KEY_Owner = "owner";
	private static final String DOMAIN = "domain";
	private static final String CREATION_DATE = "CreationDate";
	private static final String TYPE = "type";

	/**
	 * Default constrcutor
	 */
	public ProxyExporter() {
		super(TheExporter, TheExporterDescription);

		//query type created after
		addQueryType(QueryType_createdAfter, createdAfterQueryTypeDescription,
				Arrays.asList(new QueryDefinitionAttribute(createdAfter, createdAfterQueryTypeDescription, Date.class),
								new QueryDefinitionAttribute(QueryType_Domain, QueryTypeDomainDescription, String.class)));
		
		//query type owner
		addQueryType(QueryType_Owner, QueryTypeOwnerDescription,
				Arrays.asList(new QueryDefinitionAttribute(createdAfter, createdAfterQueryTypeDescription, Date.class),
						new QueryDefinitionAttribute(QueryType_Domain, QueryTypeDomainDescription, String.class),
						new QueryDefinitionAttribute(QueryType_Owner, QueryTypeOwnerDescription, String.class)));

		// query type stage
		addQueryType(QueryType_Stage, QueryTypeStageDescription,
				Arrays.asList(new QueryDefinitionAttribute(createdAfter, createdAfterQueryTypeDescription, Date.class),
						new QueryDefinitionAttribute(QueryType_Domain, QueryTypeDomainDescription, String.class),
						new QueryDefinitionAttribute(QueryType_Stage, QueryTypeStageDescription, String.class)));
		
		// query type state
		addQueryType(QueryType_State, QueryTypeStateDescription,
				Arrays.asList(new QueryDefinitionAttribute(createdAfter, createdAfterQueryTypeDescription, Date.class),
						new QueryDefinitionAttribute(QueryType_Domain, QueryTypeDomainDescription, String.class),
						new QueryDefinitionAttribute(QueryType_State, QueryTypeStateDescription, String.class)));

		// Support roles
		addSupportedRoles(Arrays.asList(Utils.ADMIN_ROLE, Utils.USER_ROLE));
	}
	
	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		
		try (ProfilePoint _doExportStagingAndPolicyDetailsReportExporter =
				ProfilePoint.profileAction("ProfAction_doExportProxyExporter")) {
			
			//retrive all information by using onboarding exporter
			OnboardingExporter exporter = new OnboardingExporter();
			//post process output values
			return postProcess(queryType, json, exporter.doExport(createdAfter, json, pageNo, pageSize));
		}
	}
	
	/**
	 * Post process data
	 * 
	 * @param queryType
	 * @param inputJson
	 * @param outputJson
	 * @return
	 */
	protected List<GenericJSON> postProcess(String queryType, GenericJSON inputJson, List<GenericJSON> outputJsons) {
		
		try (ProfilePoint _postProcess = ProfilePoint
				.profileAction("ProfAction_postProcess_" + this.getClass().getCanonicalName())) {
		
			List<GenericJSON> finalOutputJSON = new ArrayList<GenericJSON>();
			for(GenericJSON outputJson : outputJsons) {
				GenericJSON processedJson = getProcessedJson(outputJson);
				String stage = Utils.getValue(processedJson, KEY_Policy_Stage);
				String state = Utils.getValue(processedJson, KEY_Policy_State);
				String owner = Utils.getValue(processedJson, KEY_Owner);
				
				//based on query types add or ignore values
				if(queryType.equalsIgnoreCase(createdAfter)){
					finalOutputJSON.add(processedJson);
				} else if(queryType.equalsIgnoreCase(QueryType_Owner)
						&& Utils.getValue(inputJson, "Owner").equalsIgnoreCase(owner)) {
					finalOutputJSON.add(processedJson);
				} else if(queryType.equalsIgnoreCase(QueryType_Stage)
						&& Utils.getValue(inputJson, KEY_Policy_Stage).equalsIgnoreCase(stage)) {
					finalOutputJSON.add(processedJson);
				} else if(queryType.equalsIgnoreCase(QueryType_State)
						&& Utils.getValue(inputJson, KEY_Policy_State).equalsIgnoreCase(state)) {
					finalOutputJSON.add(processedJson);
				}
			}
			return finalOutputJSON;
		}
	}
	
	/**
	 * Get processed json
	 * 
	 * @param outputJson
	 * @return
	 */
	private GenericJSON getProcessedJson(GenericJSON outputJson) {
		GenericJSON processedJson = new GenericJSON();
		processedJson.put(KEY_Signature, outputJson.get("policyNumber"));
		processedJson.put(KEY_Policy_Stage, StagingAndPolicyHelper.getPolicyStage(outputJson));
		processedJson.put(KEY_Policy_State, StagingAndPolicyHelper.getPolicyState(outputJson));		
		processedJson.put(KEY_Owner, Utils.getValue(outputJson, "owner"));
		processedJson.put(DOMAIN, Utils.getValue(outputJson, "domainName"));
		processedJson.put(TYPE, "InsurancePolicy");
		processedJson.put(CREATION_DATE, Utils.getValue(outputJson, "StagingCreatedOn"));
		return processedJson;
	}
}
