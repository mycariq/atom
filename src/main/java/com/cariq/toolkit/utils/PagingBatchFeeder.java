package com.cariq.toolkit.utils;

import java.util.List;

/**
 * Class that fetches batches based on paging
 * The abstract base class that serves objects of type <T> 
 * @author hrishi
 *
 * @param <T>
 * Should be used as follows:
 * while (iter.hasNext()) { // should check count and compare
 * 	Object o = iter.next(); // should fetch
 *  // process
 *  }
 */
public abstract class PagingBatchFeeder<T> implements BatchIterator<T> {
	int batchSize;
	int pageNumber = 1;
	List<T> batch = null;
	GenericJSON keyValueMap = null;

	public PagingBatchFeeder(int batchSize) {
		super();
		this.batchSize = batchSize;
	}
	
	public PagingBatchFeeder(int batchSize, GenericJSON keyValueMap) {
		super();
		this.batchSize = batchSize;
		this.keyValueMap = keyValueMap;
	}

	@Override
	public boolean hasNext() {
		if (batch == null)
			batch = getList(pageNumber, batchSize);
		
		if (batch == null || batch.isEmpty())
			return false;

		return true;
	}

	@Override
	public List<T> next() {
		if (!hasNext())
			return null;
		
		List<T> retval = batch;
		pageNumber++;
		batch = null;
		return retval;
	}

	@Override
	public int getBatchSize() {
		return batchSize;
	}
	
	public GenericJSON getKeyValueMap() {
		return keyValueMap;
	}

	// to be implemented using the API typically in anonyous class
	protected abstract List<T> getList(int pageNo, int pageSize);

}
