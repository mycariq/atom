package com.cariq.toolkit.coreiq.tableUtil.impl;

import com.cariq.toolkit.coreiq.tableUtil.interfaces.CellIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.ColumnIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.RowIfc;

public class Cell implements CellIfc {
	RowIfc itsRow;
	ColumnIfc itsColumn;
	Object value;

	public Cell(RowIfc row, ColumnIfc column) {
		this(row, column, 0);
	}
	
	public Cell(RowIfc row, ColumnIfc column, Object value) {
		this.itsRow = row;
		this.itsColumn = column;
		this.value = value;
	}

	@Override
	public RowIfc getItsRow() {
		return itsRow;
	}

	@Override
	public ColumnIfc getItsColumn() {
		return itsColumn;
	}

	@Override
	public Object getValue() {
		return value;
	}

	@Override
	public void setValue(Object o) {
		this.value = o;
	}
}
