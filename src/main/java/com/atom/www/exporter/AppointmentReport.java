/**
 * 
 */
package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import com.cariq.toolkit.coreiq.CarIQRawQuery;
import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.model.Appointment;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * The Class AppointmentReport.
 *
 * @author santosh
 */
public class AppointmentReport extends CarIQExporterTemplate {

	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("AppointmentReport");

	public static final String REPORT = "AppointmentReport";

	private static final LinkedHashMap<String, String> attributeAliasMap = new LinkedHashMap<String, String>();
	private static final List<String> tableAliasArray = new ArrayList<>();

	public static final String ALL = "All", DOMAIN_NAME = "domainName";

	private static final String firstName = "firstName", lastName = "lastName", userName = "userName", role = "role",
			domain = "domain", type = "type", status = "status", appointmentCreatedOn = "appointmentCreatedOn",
			appointmentOn = "appointmentOn", objectId = "objectId", objectType = "objectType", signature = "signature",
			appointmentState = "appointmentState", COMPLETE = "COMPLETE", appointment = "appointment";

	static {
		attributeAliasMap.put("user.first_name", firstName);
		attributeAliasMap.put("user.last_name", lastName);
		attributeAliasMap.put("user.username", userName);
		attributeAliasMap.put("user.role", role);

		attributeAliasMap.put("domain.name", domain);
		attributeAliasMap.put("timeline.type", type);

		attributeAliasMap.put("appointment.status", status);
		attributeAliasMap.put("DATE_FORMAT(appointment.appointment_on, '%Y-%m-%d')", appointmentOn);
		attributeAliasMap.put("DATE_FORMAT(appointment.created_on, '%Y-%m-%d')", appointmentCreatedOn);
		attributeAliasMap.put(
				"CASE WHEN DATE(appointment.appointment_on) > DATE(CURDATE()) THEN 'Future' WHEN DATE(appointment.appointment_on) = DATE(CURDATE()) THEN 'Today' ELSE 'Past' END",
				appointmentState);

		attributeAliasMap.put("proxy.object_id", objectId);
		attributeAliasMap.put("proxy.object_type", objectType);
		attributeAliasMap.put("proxy.signature", signature);
		
		tableAliasArray.add("appointment appointment");
		tableAliasArray.add("LEFT JOIN proxy_object proxy ON proxy.id = appointment.proxy");
		tableAliasArray.add("LEFT JOIN timeline timeline ON timeline.proxy = proxy.id");
		tableAliasArray.add("LEFT JOIN user user ON user.id = proxy.owner");
		tableAliasArray.add("LEFT JOIN domain domain ON domain.id = proxy.domain");
	}

	public AppointmentReport() {
		super(REPORT, "Appointment Report");

		// Declare Export attributes
		addExportAttributes(new ArrayList(attributeAliasMap.values()));

		addQueryType(ALL, "All open appointments by domain", Arrays.asList(
				new QueryDefinitionAttribute(DOMAIN_NAME, "domain Name(ALL/perticular domain name)", String.class)));

		// Support roles
		addSupportedRoles(Arrays.asList(Utils.ADMIN_ROLE));
	}

	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		try (ProfilePoint _doExport = ProfilePoint.profileAction("AppointmentReport_doExport")) {

			CarIQRawQuery qry = getExportQuery(queryType, json, pageNo, pageSize);
			return qry.getResult();
		}
	}

	private CarIQRawQuery getExportQuery(String queryType, GenericJSON json, int pageNo, int pageSize) {
		try (ProfilePoint _getExportQuery = ProfilePoint.profileAction("AppointmentReport_getExportQuery")) {
			CarIQRawQuery qry = new CarIQRawQuery("getAppointmentExportQuery", Appointment.entityManager(),
					attributeAliasMap, tableAliasArray);

			if (!ALL.equalsIgnoreCase(queryType))
				Utils.handleException("Invalid Query Type: " + queryType);

			String domainName = Utils.getValue(json, DOMAIN_NAME);
			if (!ALL.equalsIgnoreCase(domainName))
				qry.addRawCondition("domain.name in (" + Utils.getStringifyList(Utils.getList(domainName)) + ")");
			
			//TODO: need to enhance currently getting all appointments
			//qry.addCondition("appointment.status", CarIQSimpleQuery.NOT_EQ, COMPLETE);
			qry.addCondition("timeline.type", CarIQSimpleQuery.EQ, appointment);

			qry.setPaging(pageNo, pageSize);
			return qry;
		}
	}
}