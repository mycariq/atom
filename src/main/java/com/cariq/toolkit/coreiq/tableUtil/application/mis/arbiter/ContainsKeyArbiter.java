package com.cariq.toolkit.coreiq.tableUtil.application.mis.arbiter;

import java.util.List;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

public class ContainsKeyArbiter extends MISCellValueArbiterBase {
	String key;

	public ContainsKeyArbiter(List list) {
		super(list);
		
		// take the first item of the list and map it to key value
		List<GenericJSON> jsonList = Utils.getJSonObjectList(list.toString(), GenericJSON.class);
		if (Utils.isNullOrEmpty(jsonList))
			Utils.handleException("Argument not passed to ContainsKeyArbiter in correct format!");
		
		GenericJSON first = jsonList.get(0);

		key = Utils.getValue(String.class, first, "key");
	}

	@Override
	public boolean isApplicable(GenericJSON input) {
		return input.containsKey(key);
	}
}
