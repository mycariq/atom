package com.cariq.toolkit.model.async;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;
import org.quartz.Job;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.coreiq.CarIQSimpleAggregationQuery;
import com.cariq.toolkit.coreiq.CarIQSimpleAggregationQuery.AggregationType;
import com.cariq.toolkit.coreiq.server.Server;
import com.cariq.toolkit.coreiq.server.ServerLoadStatistics;
import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.model.async.WorkItem.WorkItem_In;
import com.cariq.toolkit.model.async.WorkItem.WorkItem_Out;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;
import com.cariq.toolkit.utils.Utils;
import com.google.common.base.Strings;

	
@RooToString
@RooJpaActiveRecord
public class WorkItem implements JSONable<WorkItem_In, WorkItem_Out, IdJSON> {
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("WorkItem");

	public static final String HIDDEN_ITEM = "MICRO";
	public static final String MICRO = "MICRO";
	public static final String COMPOUND = "COMPOUND";
	public static final String CARIQ_JOB = "CarIQ Job";
	public static final String SIMPLE = "SIMPLE";
	public static final String LONG = "LONG";
	public static final String NOTA = "NOTA"; // None of the Above
	
	
	
	public static final String EXPORTER = "ExporterWorker";
	/**
	 */
	@NotNull(message = "Worker class should not be null")
	private String workerClass;

	/**
	 */
	private String inputJson;

	/**
	 */
	private String type;

	/**
	 */
	private int progress;

	private String host;

	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date startTime;

	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date endTime;

	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date createdOn;

	/**
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date modifiedOn;

	/**
	 */
	private String outputJson;

	/**
	 */
	@ManyToOne
	// @NotNull(message = "Its user should not be null")
	private User user;

	/**
	 */
	@ManyToOne
	private WorkItem parent;

	@ManyToOne
	private WorkItem successor;

	/**
	 */
	@NotNull(message = "Status should not be null")
	private String status = WorkItemStatus.READY.toString();

	/**
	 *
	 */
	@NotEmpty(message = "UUID should not be empty")
	@Column(unique = true)
	private String uuId;

	/**
	 *
	 */
	@NotNull(message = "name should not be null")
	private String name;

	/**
	 *
	 */
	private String descriptions;

	public WorkItem() {
	}

	public WorkItem(String inputJson, String type, String workerClass, String uuId, User itsUser, String name,
			String descriptions) {
		this(inputJson, type, workerClass, uuId, itsUser, name, descriptions, null);
	}

	public WorkItem(String inputJson, String type, String workerClass, String uuId, User itsUser, String name,
			String descriptions, WorkItem parent) {
		this(inputJson, type, workerClass, uuId, itsUser, name, descriptions, parent, null);
	}
	
	public WorkItem(String inputJson, String type, String workerClass, String uuId, User itsUser, String name,
			String descriptions, WorkItem parent, WorkItem successor) {
		this.inputJson = inputJson;
		this.type = type;
		this.workerClass = workerClass;
		this.uuId = uuId;
		this.user = itsUser;
		this.name = name;
		this.descriptions = descriptions;
		this.parent = parent;
		this.successor = successor;
		this.createdOn = new Date();
		this.modifiedOn = new Date();
	}

	public WorkItem(GenericJSON inputJson, String type, String workerClass, String uuId, User itsUser, String name,
			String descriptions) {
		this(Utils.getJSonString(inputJson), type, workerClass, uuId, itsUser, name, descriptions);
	}

	public static List<WorkItem> findUnCompletedWorkItem(int rowNumber, int size) {
		TypedQuery<WorkItem> query = WorkItem.entityManager()
				.createQuery("SELECT w from WorkItem AS w WHERE w.status=:status order by id desc", WorkItem.class);
		query.setParameter("status", WorkItemStatus.READY.toString());
		query.setFirstResult(rowNumber);
		query.setMaxResults(size);
		return query.getResultList();
	}

	public static long countUnCompletedWorkItems() {
		TypedQuery<Long> query = WorkItem.entityManager()
				.createQuery("SELECT count(w) from WorkItem AS w WHERE w.status!=:status", Long.class);
		query.setParameter("status", WorkItemStatus.READY.toString());
		return query.getSingleResult();
	}

	public String getWorkerClass() {
		return workerClass;
	}

	public void setWorkerClass(String workerClass) {
		this.workerClass = workerClass;
	}

	public String getInputJson() {
		return inputJson;
	}

	public GenericJSON fetchInputJson() {
		return Utils.getJSonObject(getInputJson(), GenericJSON.class);
	}

	public void setInputJson(String inputJson) {
		this.inputJson = inputJson;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getOutputJson() {
		return outputJson;
	}

	public void setOutputJson(String outputJson) {
		this.outputJson = outputJson;
	}

	public User getItsUser() {
		return user;
	}

	public void setItsUser(User itsUser) {
		this.user = itsUser;
	}

	public WorkItem getParent() {
		return parent;
	}

	public void setParent(WorkItem parent) {
		this.parent = parent;
	}

	public WorkItem getSuccessor() {
		return successor;
	}

	public void setSuccessor(WorkItem successor) {
		this.successor = successor;
	}

	public WorkItemStatus getStatus() {
		return WorkItemStatus.valueOf(status);
	}

	public void setStatus(WorkItemStatus status) {
		this.status = status.toString();
	}

	public String getUuId() {
		return uuId;
	}

	public void setUuId() {
		this.uuId = UUID.randomUUID().toString();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return descriptions;
	}

	public void setDescription(String descriptions) {
		this.descriptions = descriptions;
	}

	public String getHost() {
		return host;
	}
	
	public void setHost(String host) {
		this.host = host;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	/**
	 * getWorkItemByTypeAndStatus
	 *
	 * @param type
	 * @param status
	 * @param startPosition
	 * @param maxResult
	 * @return List<WorkItem>
	 */
	public static List<WorkItem> getWorkItemByTypeAndStatus(String type, String status, int startPosition,
			int maxResult) {
		TypedQuery<WorkItem> query = WorkItem.entityManager()
				.createQuery(" SELECT o FROM WorkItem o WHERE O.type = :type AND o.status = :status ", WorkItem.class);
		query.setFirstResult(startPosition);
		query.setMaxResults(maxResult);
		query.setParameter("type", type);
		query.setParameter("status", status);
		return query.getResultList();
	}

	/**
	 * getWorkItemByUUID
	 *
	 * @param uuid
	 * @return WorkItem
	 */
	public static WorkItem getWorkItemByUUID(String uuid) {
		TypedQuery<WorkItem> query = WorkItem.entityManager()
				.createQuery(" SELECT o FROM WorkItem o WHERE o.uuId = :uuid ", WorkItem.class);
		query.setParameter("uuid", uuid);
		if (!query.getResultList().isEmpty())
			return query.getSingleResult();
		return null;
	}

	/**
	 * getWorkItemByItsUser
	 *
	 * @param startPosition
	 * @param maxResult
	 * @return List<WorkItem>
	 */
	public static List<WorkItem> getWorkItems(int pageNo, int pageSize) {
		CarIQSimpleQuery<WorkItem> query = new CarIQSimpleQuery<WorkItem>("getWorkItemsList", entityManager(),
				WorkItem.class);

		query.addCondition("type", CarIQSimpleQuery.NOT_EQ, HIDDEN_ITEM);
		query.orderBy("startTime", CarIQSimpleQuery.DESC);
		query.setPaging(pageNo, pageSize);

		return query.getResultList();
	}

	/**
	 * getWorkItemByStatus
	 *
	 * @param status
	 * @param startPosition
	 * @param maxResult
	 * @return List<WorkItem>
	 */
	public static List<WorkItem> getWorkItemByStatus(String status, int startPosition, int maxResult) {
		TypedQuery<WorkItem> query = WorkItem.entityManager().createQuery(
				" SELECT o FROM WorkItem o WHERE o.status = :status  order by startTime desc ", WorkItem.class);
		query.setFirstResult(startPosition);
		query.setMaxResults(maxResult);
		query.setParameter("status", status);
		return query.getResultList();
	}

	/**
	 * getWorkItemByType
	 *
	 * @param type
	 * @param startPosition
	 * @param maxResult
	 * @return List<WorkItem>
	 */
	public static List<WorkItem> getWorkItemByType(String type, int startPosition, int maxResult) {
		TypedQuery<WorkItem> query = WorkItem.entityManager()
				.createQuery("SELECT o FROM WorkItem o WHERE o.type = :type order by startTime desc ", WorkItem.class);
		query.setFirstResult(startPosition);
		query.setMaxResults(maxResult);
		query.setParameter("type", type);
		return query.getResultList();
	}

	/**
	 * countWorkItemsIfStatusEquals
	 *
	 * @param status
	 * @return
	 */
	public static Long countWorkItemsIfStatusEquals(String status) {
		TypedQuery<Long> query = WorkItem.entityManager().createQuery(
				"SELECT COUNT(o) FROM WorkItem o WHERE o.status = :status  order by startTime desc ", Long.class);
		query.setParameter("status", status);
		if (query.getSingleResult() != null)
			return query.getSingleResult();
		return (long) 0;
	}

	/**
	 * countWorkItemsIfTypeEquals
	 *
	 * @param type
	 * @return
	 */
	public static Long countWorkItemsIfTypeEquals(String type) {
		TypedQuery<Long> query = WorkItem.entityManager().createQuery(
				"SELECT COUNT(o) FROM WorkItem o WHERE o.type = :type order by startTime desc ", Long.class);
		query.setParameter("type", type);
		if (query.getSingleResult() != null)
			return query.getSingleResult();
		return (long) 0;
	}

	/**
	 * countWorkItemsIfStatusEqualsAndStatusEquals
	 *
	 * @param type
	 * @param status
	 * @return
	 */
	public static Long countWorkItemsIfStatusEqualsAndStatusEquals(String type, String status) {
		TypedQuery<Long> query = WorkItem.entityManager().createQuery(
				"SELECT COUNT(o) FROM WorkItem o WHERE o.type = :type AND o.status = :status  order by startTime desc ",
				Long.class);
		query.setParameter("type", type);
		query.setParameter("status", status);
		if (query.getSingleResult() != null)
			return query.getSingleResult();
		return (long) 0;
	}

	// Autocommit function - not transactional - requires some annotation?
	public WorkItem start() {
		if (getStatus().equals(WorkItemStatus.READY)) {
			setStatus(WorkItemStatus.RUNNING);
			setStartTime(new Date());
			setHost(Utils.getHostName());
			setModifiedOn(new Date());
			
			WorkItem retval = merge();

			return retval;
		}
		throw new RuntimeException("WorkItem.start(): Invalid status to start the WorkItem: " + getStatus());
	}

	public WorkItem fail(String cause) {
		if (getStatus().equals(WorkItemStatus.RUNNING)) {
			setStatus(WorkItemStatus.FAILED);
			setEndTime(new Date());
			this.failureCause = cause;
			setModifiedOn(new Date());

			return merge();
		}
		throw new RuntimeException("WorkItem.fail(): Invalid status to start the WorkItem: " + getStatus());
	}

	// Autocommit function - not transactional - requires some annotation?
	public WorkItem complete() {
		if (getStatus().equals(WorkItemStatus.RUNNING)) {
			setStatus(WorkItemStatus.COMPLETED);
			setEndTime(new Date());
			setModifiedOn(new Date());

			// If there is a successor, make it ready to run
			WorkItem successor = this.getSuccessor();
			if (successor != null) {
				successor.setStatus(WorkItemStatus.READY);
				successor = successor.merge();
			}
			
			return merge();
		}
		throw new RuntimeException("WorkItem.complete(): Invalid status to start the WorkItem: " + getStatus());
	}

	// Autocommit function - not transactional - requires some annotation?
	public WorkItem setPercentComplete(int percentComplete) {
		setProgress(percentComplete);
		setModifiedOn(new Date());

		return merge();
	}

	@SuppressWarnings("unchecked")
	public Class<? extends Job> getWorker()
			throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		return (Class<? extends Job>) Class.forName(workerClass);
	}

	/***********************************************
	 * WorkItem In Out pattern
	 * and WorkOut Chaining
	 ***********************************************/
	public static class WorkItem_In {
		String inputJson;
		String type;
		String workerClass;
		String uuId;
		Long itsUser;
		String name;
		String descriptions;
		String status;
		WorkItem_In parent;
		WorkItem_In successor;
		
		public WorkItem_In(String inputJson, String type, Class workerClass, String name, String descriptions, User user) {
			this(inputJson, type, workerClass.getCanonicalName(), UUID.randomUUID().toString(), user == null ? null : user.getId(),
					name, descriptions, null, null, WorkItemStatus.CREATED.toString());
		}

		public WorkItem_In(GenericJSON inputJson, String type, Class workerClass, String name, String descriptions, User user) {
			this(inputJson.toString(), type, workerClass.getCanonicalName(), UUID.randomUUID().toString(), user == null ? null : user.getId(),
					name, descriptions, null, null, WorkItemStatus.CREATED.toString());
		}
		
		public WorkItem_In(String inputJson, String type, String workerClass, String uuId, Long itsUser,
				String name, String descriptions, WorkItem_In parent, WorkItem_In successor, String status) {
			super();
			this.inputJson = inputJson;
			this.type = type;
			this.workerClass = workerClass;
			this.uuId = uuId;
			this.itsUser = itsUser;
			this.name = name;
			this.descriptions = descriptions;
			this.parent = parent;
			this.successor = successor;
			this.status = Strings.isNullOrEmpty(status) ? WorkItemStatus.READY.toString() : status;
		}

		public String getInputJson() {
			return inputJson;
		}

		public void setInputJson(String inputJson) {
			this.inputJson = inputJson;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public String getWorkerClass() {
			return workerClass;
		}

		public void setWorkerClass(String workerClass) {
			this.workerClass = workerClass;
		}

		public String getUuId() {
			return uuId;
		}

		public void setUuId(String uuId) {
			this.uuId = uuId;
		}

		public Long getItsUser() {
			return itsUser;
		}

		public void setItsUser(Long itsUser) {
			this.itsUser = itsUser;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDescriptions() {
			return descriptions;
		}

		public void setDescriptions(String descriptions) {
			this.descriptions = descriptions;
		}

		public WorkItem_In getParent() {
			return parent;
		}

		public void setParent(WorkItem_In parent) {
			this.parent = parent;
		}

		public WorkItem_In getSuccessor() {
			return successor;
		}

		public void setSuccessor(WorkItem_In successor) {
			this.successor = successor;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}
	}

	public static class WorkItem_Out extends WorkItem_In {
		Long id;
		Date createdOn;
		Date modifiedOn;
		
		public WorkItem_Out(String inputJson, String type, String workerClass, String uuId, Long itsUser,
				String name, String descriptions, WorkItem_In parent, WorkItem_In successor, String status, long id, Date createdOn, Date modifiedOn) {
			super(inputJson, type, workerClass, uuId, itsUser, name, descriptions, parent, successor, status);

			this.id = id;
			this.createdOn = createdOn;
			this.modifiedOn = modifiedOn;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public Date getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(Date createdOn) {
			this.createdOn = createdOn;
		}

		public Date getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(Date modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
	}
	
	@Override
	public WorkItem_Out toJSON() {
		return new WorkItem_Out(inputJson, type, workerClass, uuId, 
				user == null ? null : user.getId(),
				name, descriptions, 
				parent == null ? null : parent.toJSON(),
				successor == null ? null : successor.toJSON(), status,
				getId(), createdOn, modifiedOn);
	}

	@Override
	public void fromJSON(WorkItem_In json) {
		this.inputJson = json.inputJson;
		this.type = json.type;
		this.workerClass = json.workerClass;
		this.uuId = json.uuId;
		this.user = json.itsUser == null ? null : User.find(json.itsUser);
		this.name = json.name;
		this.descriptions = json.descriptions;
		this.parent = json.parent == null ? null : Utils.constructJSONable(WorkItem.class, json.parent);
		this.successor = json.successor == null ? null : Utils.constructJSONable(WorkItem.class, json.successor);

		this.createdOn = new Date();
		this.modifiedOn = new Date();
	}

	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(getId());
	}

	
	/**
	 * Get Latest Work Item with given JobName - it may be running, or, in Progress
	 * 
	 * @param jobName
	 * @return
	 */
	public static WorkItem getByDescription(String jobName) {
		String qry = "SELECT o FROM WorkItem o WHERE o.descriptions = :description order by id desc ";
		TypedQuery<WorkItem> query = WorkItem.entityManager().createQuery(qry, WorkItem.class);
		query.setParameter("description", jobName);
		query.setMaxResults(1);
		if (!query.getResultList().isEmpty())
			return query.getSingleResult();
		return null;
	}

	public Long getUserId() {
		return user == null ? null : user.getId();
	}

	public static List<String> getAllTypes() {
		String qry = "SELECT DISTINCT w.type from WorkItem w ORDER by w.type";
		TypedQuery<String> query = WorkItem.entityManager().createQuery(qry, String.class);
		return query.getResultList();
	}

	/**
	 */
	@Size(max = 5000)
	private String failureCause;

	public String getFailureCause() {
		return failureCause;
	}

	public WorkItem refresh() {
		if (WorkItem.entityManager().contains(this)) {
			WorkItem.entityManager().refresh(this);
		}
		return this;
	}

	public static List<WorkItem> findReadyToWorkItemsOld(int maxRunningItems, String type, boolean include) {
		CarIQSimpleAggregationQuery<Long> countQuery = new CarIQSimpleAggregationQuery<Long>("countRunningWorkItems",
				entityManager(), WorkItem.class, Long.class, AggregationType.Count, null);

		// Take WorkItem from last 3 hours only.
		Date threeHoursAgo = Utils.DateByHoursBack(new Date(), 3);
		// Last 1 day
		Date oneDayAgo = Utils.DateByHoursBack(new Date(), 24);

		// pick only worky items in last day
		countQuery.addCondition("createdOn", CarIQSimpleQuery.GT, oneDayAgo);

		// identify Running workItems
		countQuery.addCondition("status", CarIQSimpleQuery.EQ, WorkItemStatus.RUNNING.toString());

		// Add a condition based on type
		if (include)
			countQuery.addCondition("type", CarIQSimpleQuery.EQ, type);
		else
			countQuery.addCondition("type", CarIQSimpleQuery.NOT_EQ, type);

		Long numberOfRunningItems = countQuery.getSingleResult();

		logger.debug("findReadyToWorkItems - Number of Running Items: " + numberOfRunningItems
				+ " and maxRunningItems: " + maxRunningItems);

		if (numberOfRunningItems != null && numberOfRunningItems >= maxRunningItems)
			return new ArrayList<WorkItem>(); // return empty list

		// identify how many items to return. Return only those.
		int readyItemsCount = (int) ((numberOfRunningItems == null) ? maxRunningItems
				: (maxRunningItems - numberOfRunningItems));

//		logger.debug("findReadyToWorkItems - Number of Ready Items to return: " + readyItemsCount);

		// Create a query to fetch READY Items - oldest first - from last day
		CarIQSimpleQuery<WorkItem> readyItemsQry = new CarIQSimpleQuery<WorkItem>("findReadyToWorkItems",
				entityManager(), WorkItem.class);
		readyItemsQry.addCondition("createdOn", CarIQSimpleQuery.GT, oneDayAgo);

		// identify Running workItems
		readyItemsQry.addCondition("status", CarIQSimpleQuery.EQ, WorkItemStatus.READY.toString());
		// Add a condition based on type
		if (include)
			readyItemsQry.addCondition("type", CarIQSimpleQuery.EQ, type);
		else
			readyItemsQry.addCondition("type", CarIQSimpleQuery.NOT_EQ, type);

		// set the id ascending
		readyItemsQry.orderBy("id", CarIQSimpleQuery.ASC);

		readyItemsQry.setPaging(1, readyItemsCount);

		return readyItemsQry.getResultList();

	}

	/**
	 * Find the work items including or excluding the given type.
	 * TODO - this need to be revisited soon - It is done this way so that things need not change elsewhere.
	 * Let's change it elsewhere and obsolete/deprecate this function
	 * @param maxRunningItems
	 * @param type
	 * @param include
	 * @return
	 */
	public static List<WorkItem> findReadyToWorkItems(int maxRunningItems, String type, boolean include) {
		return include? findReadyMicroWorkItems(maxRunningItems, maxRunningItems) : findReadyMacroWorkItems(maxRunningItems, 1);
	}
	
	/**
	 * Find Workitems to take based on max number of MICRO and MACRO
	 * @param maxRunningItems
	 * @param type
	 * @param include
	 * @return
	 */
	public static List<WorkItem> findReadyMicroWorkItems(int maxRunningItems, int batchSize) {
		ServerLoadStatistics load = Server.getInstance().getServerLoadData();
		
		int microWorkItems = load.getMicroWorkerCount();

		// If micro work items are a lot, don't spawn new.
		if (microWorkItems >= maxRunningItems)
			return new ArrayList<WorkItem>(); // return empty list

		// identify how many items to return. Return only those.
		int readyItemsCount = maxRunningItems - microWorkItems;
		
		// Don't give more than batchSize
		if (readyItemsCount > batchSize)
			readyItemsCount = batchSize;


		return findReadyWorkItems(WorkItem.MICRO, true, readyItemsCount);
	}


	/**
	 * Find Workitems to take based on max number of MICRO and MACRO
	 * @param maxRunningItems
	 * @param type
	 * @param include
	 * @return
	 */
	public static List<WorkItem> findReadyMacroWorkItems(int maxRunningItems, int batchSize) {
		ServerLoadStatistics load = Server.getInstance().getServerLoadData();
		
		int workItemCount = load.getWorkerCount();
		int microWorkItems = load.getMicroWorkerCount();
		int macroWorkItems = workItemCount - microWorkItems;

		if (macroWorkItems >= maxRunningItems)
			return new ArrayList<WorkItem>(); // return empty list

		// identify how many items to return. Return only those.
		int readyItemsCount = maxRunningItems - macroWorkItems;
		
		// Don't give more than batchSize
		if (readyItemsCount > batchSize)
			readyItemsCount = batchSize;

//		logger.debug("findReadyToWorkItems - Number of Ready Items to return: " + readyItemsCount);
		return findReadyWorkItems(WorkItem.MICRO, false, readyItemsCount);
	}

	private  static List<WorkItem>findReadyWorkItems(String type, boolean include, int maxReadyItemsCount) {
//		logger.debug("findReadyToWorkItems - Number of Ready Items to return: " + maxReadyItemsCount);

		// Get items from last 24 hours only
		Date oneDayAgo = Utils.DateByHoursBack(new Date(), 24);

		// Create a query to fetch READY Items - oldest first - from last hour
		CarIQSimpleQuery<WorkItem> readyItemsQry = new CarIQSimpleQuery<WorkItem>("findReadyToWorkItems",
				entityManager(), WorkItem.class);
		readyItemsQry.addCondition("createdOn", CarIQSimpleQuery.GT, oneDayAgo);

		// identify Running workItems
		readyItemsQry.addCondition("status", CarIQSimpleQuery.EQ, WorkItemStatus.READY.toString());
		
		// Add a condition based on type
		if (include) {
			readyItemsQry.addCondition("type", CarIQSimpleQuery.EQ, type);
		} else {
			readyItemsQry.addCondition("type", CarIQSimpleQuery.NOT_EQ, type);
		}

		// set the id ascending
		readyItemsQry.orderBy("id", CarIQSimpleQuery.ASC);

		readyItemsQry.setPaging(1, maxReadyItemsCount);

		return readyItemsQry.getResultList();
	}

	/**
	 * Get the WorkItems to run... but if some are already running, get only ones
	 * that are ready If some are running from last 24 hours, then leave those.
	 * 
	 * @param rowNum
	 * @param maxRunningItems
	 * @return
	 */
	public static List<WorkItem> findReadyToWorkItems(int maxReadyItems, int durationInHours) {
//		logger.debug("findReadyToWorkItems - Number of Ready Items to return: " + maxReadyItems);

		// Get items from last 24 hours only
		Date oneDayAgo = Utils.DateByHoursBack(new Date(), durationInHours);

		// Create a query to fetch READY Items - oldest first - from last hour
		CarIQSimpleQuery<WorkItem> readyItemsQry = new CarIQSimpleQuery<WorkItem>("findReadyToWorkItems",
				entityManager(), WorkItem.class);
		readyItemsQry.addCondition("createdOn", CarIQSimpleQuery.GT, oneDayAgo);

		// identify Running workItems
		readyItemsQry.addCondition("status", CarIQSimpleQuery.EQ, WorkItemStatus.READY.toString());

		// set the id ascending
		readyItemsQry.orderBy("id", CarIQSimpleQuery.ASC);

		readyItemsQry.setPaging(1, maxReadyItems);

		return readyItemsQry.getResultList();
	}
	
	/**
	 * Get the WorkItems to run... but if some are already running, get only ones
	 * that are ready If some are running from last 24 hours, then leave those.
	 * 
	 * @param rowNum
	 * @param maxRunningItems
	 * @return
	 */
	public static List<WorkItem> findReadyToWorkItems(int maxRunningItems) {
		// Max running items are number of servers* items per server
		List<WorkItem> retval = new ArrayList<WorkItem>();
		// Micro items to include in return list
		List<WorkItem> microItems = findReadyMicroWorkItems(maxRunningItems, maxRunningItems);
		if (!Utils.isNullOrEmpty(microItems))
			retval.addAll(microItems);

		// Macro items to be included in return list
		List<WorkItem> macroItems = findReadyMacroWorkItems(maxRunningItems, 1);
		if (!Utils.isNullOrEmpty(macroItems))
			retval.addAll(macroItems);

		
		
		return retval;
	}


	/**
	 * To debug string.
	 *
	 * @return the string
	 */
	public String toDebugString() {
		return "WorkItem [workerClass=" + workerClass + ", inputJson=" + inputJson + ", type=" + type + ", progress="
				+ progress + ", host=" + host + ", startTime=" + startTime + ", endTime=" + endTime + ", createdOn="
				+ createdOn + ", modifiedOn=" + modifiedOn + ", outputJson=" + outputJson + ", itsUser="
				+ ((null != user) ? user.getId() : "null") + ", parent="
				+ ((null != parent) ? parent.getId() : "null") + ", status=" + status + ", uuId=" + uuId + ", name="
				+ name + ", descriptions=" + descriptions + ", failureCause=" + failureCause + "]";
	}

	/**
	 * Gets the child count.
	 *
	 * @param parent
	 *            the parent
	 * @return the child count
	 */
	public static Long getChildCount(WorkItem parent) {
		CarIQSimpleAggregationQuery<Long> query = new CarIQSimpleAggregationQuery<Long>("WorkItem.getChildCount",
				entityManager(), WorkItem.class, Long.class, AggregationType.Count, null);
		query.addCondition("parent", CarIQSimpleQuery.EQ, parent);
		return query.getSingleResult();
	}

	/**
	 * Gets the completed child count.
	 *
	 * @param parent
	 *            the parent
	 * @return the completed child count
	 */
	public static Long getCompletedChildCount(WorkItem parent) {
		CarIQSimpleAggregationQuery<Long> query = new CarIQSimpleAggregationQuery<Long>(
				"WorkItem.getCompletedChildCount", entityManager(), WorkItem.class, Long.class, AggregationType.Count,
				null);
		query.addCondition("parent", CarIQSimpleQuery.EQ, parent);
		query.addCondition("status", CarIQSimpleQuery.IN,
				Arrays.asList(WorkItemStatus.COMPLETED.toString(), WorkItemStatus.FAILED.toString()));
		return query.getSingleResult();
	}

	/**
	 * Checks if is complete.
	 *
	 * @return true, if is complete
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public boolean isComplete() {
		if (!COMPOUND.equalsIgnoreCase(this.type) && !CARIQ_JOB.equalsIgnoreCase(this.type))
			return (WorkItemStatus.COMPLETED.toString().equals(this.status)
					|| WorkItemStatus.FAILED.toString().equals(this.status));
		//TODO check childer count
		Long totalCount = getChildCount(this);
		Long completedCount = getCompletedChildCount(this);
		return completedCount == totalCount;
	}
}