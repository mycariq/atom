package com.atom.www.loader;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Configurable;

import com.atom.www.helper.MISReportHelper;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderContext;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderDefinition;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderTemplate;
import com.cariq.toolkit.model.BillingAccount;
import com.cariq.toolkit.model.BillingContract;
import com.cariq.toolkit.model.BillingOperation;
import com.cariq.toolkit.model.BillingOperation.BillingStatusEnum;
import com.cariq.toolkit.model.BillingTerm;
import com.cariq.toolkit.model.Invoice;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Month;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.analytics.application.BillingRecordProcessor;
import com.cariq.toolkit.utils.analytics.core.AnalyticsContext;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsCookbook;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsProcess;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsRecipe;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.google.common.base.Strings;

/**
 * Performs billing operation
 * 
 * @author amita
 *
 */
@Configurable
public class BillingGenerator extends CarIQLoaderTemplate {
	
	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("BillingGenerator");
	
	MISReportHelper reportHelper = new MISReportHelper();
	
	private static final String TheLoader = "BillingGenerator";
	private static final String TheDescription = "Generates billing for given contract, months and billing terms";
	
	private static final String CONTRACT = "BillingContract";
	private static final String RAISED_BY_ACCOUNT = "RaisedByAccount";
	private static final String RAISED_AGAINST_ACCOUNT = "RaisedAgainstAccount";
	private static final String FROM_MONTH = "FromMonth"; // format: MMMM-yyyy
	private static final String TO_MONTH = "ToMonth"; // format: MMMM-yyyy
	private static final String BILLINGTERMS = "BillingTerms";
	private static final String TAT_URL = "TAT_URL", TAT_FILE = "TAT_FILE";
	
	private static final String BILLING_RECIPE = "BillingRecipe";
	
	// Context to be used throughout billing process.
	BillingContract billingContract = null;
	BillingAccount raisedByAccount = null, raisedAgainstAccount = null;
	List<BillingTerm> billingTerms = new ArrayList<BillingTerm>();
	BillingOperation billingOperation = null;
	Month startMonth = null, endMonth = null;
	String tatUrl = null, tatFile = null;
	
	public BillingGenerator() {
		super(TheLoader, TheDescription, CarIQLoaderDefinition.HIDDEN_CATEGORY);
		
		//add attributes required for this loader
		addAttribute(CONTRACT, "Billing contract name", false);
		addAttribute(RAISED_BY_ACCOUNT, "Name of the account raising the bill", false);
		addAttribute(RAISED_AGAINST_ACCOUNT, "Name of the account to raise the billing against", false);
		addAttribute(FROM_MONTH, "From month (MMMM-yyyy)", false);
		addAttribute(TO_MONTH, "To month (MMMM-yyyy)", false);
		addAttribute(BILLINGTERMS, "Comma separated billing term names under the contract OR All", false);
		addAttribute(TAT_URL, "Base TAT URL", true);
		addAttribute(TAT_FILE, "Base TAT File", true);
	}

	// Process the date range from the given months in the format 'mmmm-yyyy'
	private void getDateRange(String fromMonth, String toMonth) {
		try {
			startMonth = new Month(fromMonth, Utils.INDIA_TIME_ZONE);
			endMonth = new Month(toMonth, Utils.INDIA_TIME_ZONE);
		} catch(Exception e) {
			Utils.handleException("Failed to find the date range fromMonth: " + fromMonth + ", toMonth: " + toMonth);
		}
	}
	
	// Initialize billing parameters
	private void initBillingParams(GenericJSON json) {
		
		String contractName = Utils.getValue(json, CONTRACT);
		String byAccountName = Utils.getValue(json, RAISED_BY_ACCOUNT);
		String againstAccountName = Utils.getValue(json, RAISED_AGAINST_ACCOUNT);
		String fromMonth = Utils.getValue(json, FROM_MONTH);
		String toMonth = Utils.getValue(json, TO_MONTH);
		String billingTermsList = Utils.getValue(json, BILLINGTERMS);
		
		tatFile =  Utils.getValue(json, TAT_FILE);
		tatUrl =  Utils.getValue(json, TAT_URL);
		
		// Confirm the input TAT
		if (Strings.isNullOrEmpty(tatUrl) && Strings.isNullOrEmpty(tatFile)) 
			Utils.handleException("Invalid TAT provided");
		
		// Set the account raising this bill
		raisedByAccount = BillingAccount.getByName(byAccountName);
		if (null == raisedByAccount)
			Utils.handleException("Invalid billing account: " + byAccountName);
		
		logger.debug("***raisedByAccount: " + raisedByAccount.getName());
		
		// Set the account against which billing should be done
		raisedAgainstAccount = BillingAccount.getByName(againstAccountName);
		if (null == raisedAgainstAccount)
			Utils.handleException("Invalid billing account: " + againstAccountName);
		
		logger.debug("***raisedAgainstAccount: " + raisedAgainstAccount.getName());
		
		// Set the relevant contract
		billingContract = BillingContract.getByNameAndAccount(contractName, raisedAgainstAccount);
		if (null == billingContract)
			Utils.handleException("Invalid billing contract: " + contractName + " for account " + contractName);
		
		logger.debug("***billingContract: " + billingContract.getName());
		
		// Check and initialize the billing terms under given contract
		if (billingTermsList.equalsIgnoreCase("all")) {
			billingTerms.addAll(BillingTerm.getByContract(billingContract));
		} else {
			for (String bt : billingTermsList.split(",")) {
				BillingTerm term = BillingTerm.getByName(bt);
				if (null == term)
					Utils.handleException("Invalid billing term: " + bt);
				if (billingContract != term.getItsContract())
					Utils.handleException("Invalid billing term " + bt + " for contract: " + contractName);
				billingTerms.add(term);
			}
		}
		
		logger.debug("***billingTerms: " + billingTerms.size());
		
		// Process the date range.
		getDateRange(fromMonth, toMonth);
		
		logger.debug("***fromDate: " + startMonth.getStartTime() + ", toDate: " + endMonth.getEndTime());
	}
	
	private void createBillingOperation() {
		String forBillingTerms = "";
		for (BillingTerm bt : billingTerms) {
			forBillingTerms = forBillingTerms.concat(bt.getName() + ",");
		}
		
		logger.debug("***forBillingTerms: " + forBillingTerms);
		
		billingOperation = new BillingOperation(new Date(), null, raisedByAccount, raisedAgainstAccount, billingContract, 
				startMonth.getName(), endMonth.getName(), forBillingTerms, 
				reportHelper.getCurrentDomain(), reportHelper.getCurrentUser(), new Date(),	new Date());
		billingOperation.persist();
	}
	
	private void processTAT() {
		try {
			// Load analytics context
			AnalyticsContext ctx = new AnalyticsContext();		
			ctx.put(BillingRecordProcessor.BILLING_OPERATION_ID , billingOperation.getId().toString());
			logger.debug("***billingOperationId: " + billingOperation.getId().toString());
			
			// Get the billing recipe
			AnalyticsRecipe recipe = AnalyticsCookbook.getInstance().getRecipe(BILLING_RECIPE);
			AnalyticsProcess process = new AnalyticsProcess(recipe, ctx, tatFile);
			
			// Execute. Recipe will scan the TAT file cook the billing, ready to serve!
			logger.debug("**********Executing process***********");
			process.execute();
			process.close();
			logger.debug("**********Process Closed***********");
		} catch(Exception e) {
			Utils.handleException("Exception in processTAT(): " + e.getMessage());
		}
	}
	
	@Override
	protected void doLoad(GenericJSON json, CarIQLoaderContext context) {
		logger.debug("BillingGenerator.doLoad(): json : " + json.toString());
		
		initBillingParams(json);
		
		try (ProfilePoint billingGenerator = ProfilePoint.profileAction("ProfAction_BillingGenerator")) {

			// Initialize the helper object
			reportHelper.configureDomainUser(json);		
			
			createBillingOperation();
			
			processTAT();
			
			if (billingOperation.getBillingResult().equals(BillingStatusEnum.SUCCESSFUL.toString())) {
				Invoice invoice = Invoice.getByItsOperation(billingOperation);
				context.addMessage("Billing operation# " + billingOperation.getId() 
					+ " completed successfully for " + raisedAgainstAccount.getName() 
					+ ", please review the invoice# " + invoice.getId() + ": " + invoice.getUrlToDocument());
			} else
				context.addMessage("Billing operation# " + billingOperation.getId() 
				+ " completed as INVALID for " + raisedAgainstAccount.getName() 
				+ ", hence NO valid invoice available.");
			
		} catch (Exception e) {			
			context.addMessage("Exception in Billing operation# " + billingOperation.getId() 
			+ ", error: " + e.getMessage());
		}
	}
}
