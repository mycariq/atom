/**
 * 
 */
package com.cariq.toolkit.coreiq.exporter;

import java.util.List;

import com.cariq.toolkit.utils.GenericJSON;


/**
 * @author hrishi
 *
 */
public interface ExporterDataEnhancer {
	GenericJSON enhance(String queryType, GenericJSON inputJSON, GenericJSON exportJSON);
	List<GenericJSON> enhance(String queryType, GenericJSON inputJSON, GenericJSON previous, GenericJSON current);
	List<GenericJSON> enhance(String queryType, GenericJSON inputJSON, List<GenericJSON> exportJSON);
	List<GenericJSON> enhance(String queryType, GenericJSON inputJSON, GenericJSON previous, List<GenericJSON> exportJSON);
}
