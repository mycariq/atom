/**
 * 
 */
package com.cariq.toolkit.coreiq.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.SimplePOJOs.CountPOJO;
import com.cariq.toolkit.utils.Utils;

/**
 * @author hrishi DemoMulti1 - 20 records - columns A, B, C, values a1, b1, c1;
 *         ... a20, b20, c20 DemoMulti2 - 30 records - columns D, E, F, G values
 *         d1, e1, f1, g1; ... d20, e20, f20, g20 DemoMulti3 - 40 records -
 *         columns H, I values h1, i1; ... h20, i20
 */
public class DemoMultiCompositeExporter extends MultiExporterTemplate {

	public static class DemoMulti1 extends CarIQExporterTemplate {
		static final int MAX_RECORDS = 20;

		/**
		 * @param name
		 * @param description
		 */
		public DemoMulti1() {
			super("DemoMulti1", "20 records - columns A, B, C, values a1, b1, c1; ... a20, b20, c20");
			super.addExportAttributes(Arrays.asList("A", "B", "C"));
			super.addQueryType("All", "Everything", new String[] { "" });
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.cariq.www.coreiq.exporter.CarIQExporterTemplate#doExport(java.lang.
		 * String, com.cariq.www.utils.GenericJSON, int, int)
		 */
		@Override
		protected List<GenericJSON> doExportRecords(String queryType, GenericJSON json, int startIndex, int endIndex) {
			if (startIndex > MAX_RECORDS)
				return null;

			List<GenericJSON> retval = new ArrayList<GenericJSON>();
			int countOfRecords = Math.min((endIndex + 1), MAX_RECORDS);
			for (int i = startIndex; i < countOfRecords; i++) {
				GenericJSON outJSON = new GenericJSON();
				outJSON.put("A", "a" + i);
				outJSON.put("B", "b" + i);
				outJSON.put("C", "c" + i);

				retval.add(outJSON);
			}

			return retval;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.cariq.www.coreiq.exporter.CarIQExporterTemplate#doExport(java.lang.
		 * String, com.cariq.www.utils.GenericJSON, int, int)
		 */
		@Override
		protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
			int startIndex = Utils.validatePagination(pageSize, pageNo);

			return doExportRecords(queryType, json, startIndex, (startIndex + pageSize - 1));

		}

		@Override
		public CountPOJO getExportCount(String queryType, GenericJSON json) {
			return new CountPOJO(MAX_RECORDS);
		}

	}

	public static class DemoMulti2 extends CarIQExporterTemplate {
		static final int MAX_RECORDS = 30;

		/**
		 * @param name
		 * @param description
		 */
		public DemoMulti2() {
			super("DemoMulti2", "30 records - columns D, E, F, G values d1, e1, f1, g1; ... d20, e20, f20, g20");
			super.addExportAttributes(Arrays.asList("D", "E", "F", "G"));
			super.addQueryType("All", "Everything", new String[] { "" });
		}

		@Override
		protected List<GenericJSON> doExportRecords(String queryType, GenericJSON json, int startIndex, int endIndex) {
			if (startIndex > MAX_RECORDS)
				return null;

			List<GenericJSON> retval = new ArrayList<GenericJSON>();
			int countOfRecords = Math.min((endIndex + 1), MAX_RECORDS);
			for (int i = startIndex; i < countOfRecords; i++) {
				GenericJSON outJSON = new GenericJSON();
				outJSON.put("D", "d" + i);
				outJSON.put("E", "e" + i);
				outJSON.put("F", "f" + i);
				outJSON.put("G", "g" + i);

				retval.add(outJSON);
			}

			return retval;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.cariq.www.coreiq.exporter.CarIQExporterTemplate#doExport(java.lang.
		 * String, com.cariq.www.utils.GenericJSON, int, int)
		 */
		@Override
		protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
			int startIndex = Utils.validatePagination(pageSize, pageNo);

			return doExportRecords(queryType, json, startIndex, (startIndex + pageSize - 1));

		}

		@Override
		public CountPOJO getExportCount(String queryType, GenericJSON json) {
			return new CountPOJO(MAX_RECORDS);
		}
	}

	public static class DemoMulti3 extends CarIQExporterTemplate {
		static final int MAX_RECORDS = 40;

		/**
		 * @param name
		 * @param description
		 */
		public DemoMulti3() {
			super("DemoMulti3", "40 records - columns H, I values h1, i1; ... h20, i20");
			super.addExportAttributes(Arrays.asList("H", "I"));
			super.addQueryType("All", "Everything", new String[] { "" });
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.cariq.www.coreiq.exporter.CarIQExporterTemplate#doExport(java.lang.
		 * String, com.cariq.www.utils.GenericJSON, int, int)
		 */
		@Override
		protected List<GenericJSON> doExportRecords(String queryType, GenericJSON json, int startIndex, int endIndex) {
			if (startIndex > MAX_RECORDS)
				return null;

			List<GenericJSON> retval = new ArrayList<GenericJSON>();
			int countOfRecords = Math.min((endIndex + 1), MAX_RECORDS);
			for (int i = startIndex; i < countOfRecords; i++) {
				GenericJSON outJSON = new GenericJSON();
				outJSON.put("H", "h" + i);
				outJSON.put("I", "i" + i);

				retval.add(outJSON);
			}

			return retval;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.cariq.www.coreiq.exporter.CarIQExporterTemplate#doExport(java.lang.
		 * String, com.cariq.www.utils.GenericJSON, int, int)
		 */
		@Override
		protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
			int startIndex = Utils.validatePagination(pageSize, pageNo);

			return doExportRecords(queryType, json, startIndex, (startIndex + pageSize - 1));

		}

		@Override
		public CountPOJO getExportCount(String queryType, GenericJSON json) {
			return new CountPOJO(MAX_RECORDS);
		}
	}

	/**
	 * @param name
	 * @param description
	 * @param exporters
	 */
	public DemoMultiCompositeExporter() {
		super("DemoMultiCompositeExporter", "Exporters data from multiple Exporters",
				Arrays.<CarIQExporter>asList(new DemoMulti1(), new DemoMulti2(), new DemoMulti3()));
		super.addSupportedRoles(Arrays.asList("ADMIN", "DEMO_EXPORTER"));
	}
}
