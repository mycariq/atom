package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.core.io.ClassPathResource;

import com.atom.www.helper.MISReportHelper;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.analytics.application.MonthlyMISReporter;
import com.cariq.toolkit.utils.analytics.core.AnalyticsContext;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsCookbook;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsProcess;
import com.cariq.toolkit.utils.analytics.engine.AnalyticsRecipe;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.google.common.base.Strings;

/**
 * The Class MonthlyMISReportExporter.
 *
 * @author amita
 */
public class MonthlyMISReportExporter extends CarIQExporterTemplate {
	
	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("MonthlyMISReportExporter");
	
	/** The Constant EXPORTER. */
	public static final String EXPORTER = "MonthlyMISReportExporter";

	MISReportHelper reportHelper = new MISReportHelper();
	
	/** The Constant Query types. */
	public static final String FROM_TAT_URL = "FROM_TAT_URL", FROM_TAT_FILE = "FROM_TAT_FILE",
			FROM_TAT_URL_LATEST = "FROM_TAT_URL_LATEST", FROM_TAT_URL_LATEST_IN_DATERANGE = "FROM_TAT_URL_LATEST_IN_DATERANGE";
			
	/** The Constant QueryDefinition attributes. */			
	public static final String	TAT_FILE_URL = "TATFileURL", TAT_FILE_PATH = "TATFilePath", 
			FROM_DATE = "FROM_DATE", TO_DATE = "TO_DATE", TAT_INPUT_DATE = "TAT_INPUT_DATE";

	public static final String MONTHLY_MISREPORT_FILE_PATH = "MonthlyMISReportFilePath";
	public static final String MONTHLY_MISREPORT_BUILDER_JSON = "json/MonthlyMisReportBuilder.json";
	public static final String MONTHLY_MISREPORT_RECIPE = "MonthlyMISReportRecipe";
	
	/**
	 * 
	 * Instantiates a new MonthlyMISReportExporter exporter.
	 */
	public MonthlyMISReportExporter() {
		super(EXPORTER, "Monthly MIS Report Exporter");

		// Query definitions
		addQueryType(FROM_TAT_FILE,
				"Generates Monthly MIS Report from given TAT report file",
				Arrays.asList(new QueryDefinitionAttribute(TAT_FILE_PATH, "Absolute file path to TATReport csv file")));
						//new QueryDefinitionAttribute(TAT_INPUT_DATE, "Input date used for given TAT file ")));

		addQueryType(FROM_TAT_URL,
				"Generates Monthly MIS Report from given TAT report URL",
				Arrays.asList(new QueryDefinitionAttribute(TAT_FILE_URL, "TATReport URL")));
						//new QueryDefinitionAttribute(TAT_INPUT_DATE, "Input used date for given TAT URL")));

		addQueryType(FROM_TAT_URL_LATEST, "Generates Monthly MIS Report from latest TAT report URL", new String[] {});
			
		// Support roles
		addSupportedRoles(Arrays.asList(Utils.ADMIN_ROLE));
	}

	private String analyzeTATReport(String tatFile) {
		String outJSONString = null;
		
		try {
			// For testing: "//home//amita//Documents//MIS//JSON//MonthlyMisReportBuilder.json";
			String builderJSONFilePath = new ClassPathResource(MONTHLY_MISREPORT_BUILDER_JSON).getFile().getAbsolutePath();
			logger.debug("===> builderJSONFilePath: " + builderJSONFilePath);
			AnalyticsContext ctx = new AnalyticsContext();
			ctx.put(MonthlyMISReporter.MIS_REPORT_JSON_PATH , builderJSONFilePath);
			AnalyticsRecipe recipe = AnalyticsCookbook.getInstance().getRecipe(MONTHLY_MISREPORT_RECIPE);
	
			AnalyticsProcess process = new AnalyticsProcess(recipe, ctx, tatFile);
			process.execute();
			// get output JSON
			outJSONString = process.getContext().get(MonthlyMISReporter.MONTHLY_MIS_JSON).toString();
			process.close();
			
			//logger.debug("************outJSON: " + outJSONString);
			
		} catch (Exception e) {
			Utils.handleException(e);
		}

		return outJSONString;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate#doExport(java.lang.
	 * String, com.cariq.toolkit.utils.GenericJSON, int, int)
	 */
	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		try (ProfilePoint MonthlyMISReportExporter_doExport = ProfilePoint
				.profileAction("ProfAction_MonthlyMISReportExporter_doExport")) {
	        
			// Ignore pageNo, ignorepageSize, just return alerts of each type
	        if (pageNo > 1)
	            return null;
	        
			logger.debug("doExport - queryType : " + queryType + " pageNo : " + pageNo + " pageSize : " + pageSize
					+ " json :" + json.toString());
	        
			List<GenericJSON> retList = new ArrayList<GenericJSON>();
			
			String tatFileURL = null;
			String tatFilePath = null;

			// Initialize the helper object
			reportHelper.configureDomainUser(json);
			
			if(queryType.equalsIgnoreCase(FROM_TAT_FILE)) {
				tatFilePath = Utils.getValue(String.class, json, TAT_FILE_PATH);
			} else if (queryType.equalsIgnoreCase(FROM_TAT_URL)) {
				tatFileURL = Utils.getValue(String.class, json, TAT_FILE_URL);
			} else if (queryType.equalsIgnoreCase(FROM_TAT_URL_LATEST)) {
				tatFileURL = reportHelper.getLatestTATURL();
			} else {
				Utils.handleException("QueryType " + queryType + " is not supported!!");
			}
					

			if (! Strings.isNullOrEmpty(tatFileURL)) {
				tatFilePath = CarIQFileUtils.getLocalFilePathFromURL(tatFileURL);
			}
			
			if (Strings.isNullOrEmpty(tatFilePath) || !CarIQFileUtils.exists(tatFilePath)) {
				Utils.handleException("Invalid TAT report file: " + tatFilePath);
			}
			logger.debug("===> TAT File ready at: " + tatFilePath);
			
			// Process the TAT CSV and get the report JSON 
			String reportJSONString = analyzeTATReport(tatFilePath);	
			
			String reportFileName = EXPORTER + "-" + Utils.createDateId(true) + ".html";
			
			// Convert processed JSON to local HTML file
			String localHTMLFilePath = reportHelper.convertJSONToHTMLFile(reportJSONString, reportFileName, null);
			
			if (! CarIQFileUtils.exists(localHTMLFilePath)) {
				Utils.handleException("Failed to create HTML report from processed JSON");
			}
						
			// compress, upload and persist HTML file
			String reportDetails = "InputJSON: " + json.toString();
//			GenericJSON outJSON = reportHelper.exportCompressedHTMLReport(EXPORTER, queryType, localHTMLFilePath, reportFileName, reportDetails);
			
			// upload and persist HTML file
			GenericJSON outJSON = reportHelper.exportHTMLReport(EXPORTER, queryType, localHTMLFilePath, reportFileName, reportDetails);
			
//			GenericJSON outJSON = new GenericJSON();
//			outJSON.add("LocalHTML", localHTMLFilePath);
					
			retList.add(outJSON);
								
			return retList;
		}
	}
}