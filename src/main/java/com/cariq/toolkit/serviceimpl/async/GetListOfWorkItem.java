package com.cariq.toolkit.serviceimpl.async;

import java.util.Date;

import com.cariq.toolkit.model.async.WorkItemStatus;
import com.google.common.base.Strings;


public class GetListOfWorkItem {
	private String host;
	private String type;
	private Integer progress;
	private String startTime;
	private String endTime;
	private String time;
	private Double effectiveness;
	private String uuid;
	private String name;
	private String descriptions;
	private Long itsUser;
	private WorkItemStatus status;

	public GetListOfWorkItem(String host, String type, Integer progress, Date startTime, Date endTime, String uuid, String name,
			String descriptions, Long itsUser, WorkItemStatus status) {

		this(host, type, progress, startTime, endTime, uuid, name, descriptions, itsUser, status, null, 0.0);
	}

	public GetListOfWorkItem(String host, String type, Integer progress, Date startTime, Date endTime, String uuid, String name,
			String descriptions, Long itsUser, WorkItemStatus status, String time, Double effectiveness) {
		super();
		this.host = host;
		this.type = type != null ? type : null;
		this.progress = progress != null ? progress : null;
		this.startTime = startTime != null ? startTime.toString() : null;
		this.endTime = endTime != null ? endTime.toString() : null;
		this.uuid = uuid != null ? uuid : null;
		this.name = name;
		this.descriptions = descriptions != null ? descriptions : null;
		this.itsUser = itsUser;
		this.status = status;
		this.time = (Strings.isNullOrEmpty(time)) ? null : time;
		this.effectiveness = effectiveness != null ? effectiveness : null;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getType() {
		return type;
	}

	public Integer getProgress() {
		return progress;
	}

	public String getStartTime() {
		return startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public String getUuid() {
		return uuid;
	}

	public String getName() {
		return name;
	}

	public String getDescriptions() {
		return descriptions;
	}

	public Long getItsUser() {
		return itsUser;
	}

	public WorkItemStatus getStatus() {
		return status;
	}

	public String getTime() {
		return time;
	}

	public Double getEffectiveness() {
		return effectiveness;
	}
}