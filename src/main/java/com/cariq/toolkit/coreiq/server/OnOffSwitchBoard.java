package com.cariq.toolkit.coreiq.server;

import java.util.ArrayList;
import java.util.List;

/**
 * Audit enabled entities - pattern - Settings class - where any parameters can be switched.
 * @author hrishi
 *
 */
public class OnOffSwitchBoard {
	List<OnOffSwitch> switches = new ArrayList<OnOffSwitch>();

	public OnOffSwitchBoard() {}

	public OnOffSwitchBoard(List<String> switchNames) {
		for (String switchName : switchNames) {
			add(new OnOffSwitch(switchName));
		}
	}

	public List<OnOffSwitch> getSwitches() {
		return switches;
	}

	public void setSwitches(List<OnOffSwitch> switches) {
		this.switches = switches;
	}

	//=================== Convenience function for construction ===
	public void add(OnOffSwitch theSwitch) {
		if (!switches.contains(theSwitch))
			switches.add(theSwitch);
	}
	
	public OnOffSwitch locateSwitch(String switchName) {
		for (OnOffSwitch onOffSwitch : switches) {
			if (onOffSwitch.getSwitchName().equalsIgnoreCase(switchName))
				return onOffSwitch;
		}
		// not known
		return null;
	}
	
	public Boolean isOn(String switchName) {
		OnOffSwitch theSwitch = locateSwitch(switchName);
		if (theSwitch != null)
			return theSwitch.getIsOn();
		
		// not known
		return null;
	}

	public boolean switchOn(String switchName) {
		return switchOnOrOff(switchName, true);
	}
	
	public boolean switchOff(String switchName) {
		return switchOnOrOff(switchName, false);
	}
	
	public boolean switchOnOrOff(String switchName, boolean onOrOff) {
		OnOffSwitch theSwitch = locateSwitch(switchName);
		if (theSwitch != null && theSwitch.getIsOn() != onOrOff) {
			theSwitch.setIsOn(onOrOff);
			return true;
		}
		
		// no Action was taken
		return false;		
	}	
}
