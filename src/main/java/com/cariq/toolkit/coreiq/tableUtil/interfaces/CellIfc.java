package com.cariq.toolkit.coreiq.tableUtil.interfaces;

/**
 * 
 * @author hrishi
 *
 */
public interface CellIfc {
	RowIfc getItsRow();

	ColumnIfc getItsColumn();

	Object getValue();

	void setValue(Object o);
}
