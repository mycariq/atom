package com.cariq.toolkit.utils.mistools;

import java.util.ArrayList;
import java.util.List;

public class ApplicableSection  {
	String sectionGroup;
	List<String> stringDefList = new ArrayList<String>();
	
	public ApplicableSection(String sectionGroup, String stringDef) {
		this.sectionGroup = sectionGroup;
		stringDefList.add(stringDef);
	}

	public ApplicableSection(String sectionGroup, List<String> stringDefList) {
		super();
		this.sectionGroup = sectionGroup;
		this.stringDefList = stringDefList;
	}

	public String getSectionGroup() {
		return sectionGroup;
	}

	public void setSectionGroup(String sectionGroup) {
		this.sectionGroup = sectionGroup;
	}

	public List<String> getStringDefList() {
		return stringDefList;
	}

	public void setStringDefList(List<String> stringDefList) {
		this.stringDefList = stringDefList;
	}
}
