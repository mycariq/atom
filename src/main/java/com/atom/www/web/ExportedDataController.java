package com.atom.www.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.cariq.toolkit.coreiq.exporter.ExportedData.ExporterDataJSON_Out;
import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.publicapi.CarIQPublicAPIParameter;
import com.cariq.toolkit.service.ExporterDataService;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.utils.ResponseJson;

@CarIQPublicAPI(name = "ExportedData", description = "Cache of async downloaded Exported Data")
@RequestMapping("/exportedData")
@Controller
public class ExportedDataController {

	@Autowired
	ExporterDataService exporterDataService;
	
	/**
	 * Get All ExporterData in decending order with paging
	 *
	 * @param param
	 *            the raw json
	 * @param response
	 *            the response
	 * @return the object
	 */
	// CarIQ Public API Documentation
	@CarIQPublicAPIMethod(description = "Enlist ExporterData", responseClass = ExporterDataJSON_Out.class, internal = true)
	@RequestMapping(value = "/enlist/{pageNo}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public List<ExporterDataJSON_Out> enlistExporterData(
			@CarIQPublicAPIParameter(name = "pageNo", description = "the pageNo") @PathVariable int pageNo,
			@CarIQPublicAPIParameter(name = "pageSize", description = "the pageSize") @PathVariable int pageSize) {
		try (CarIQAPI _getResource = new CarIQAPI("FleetIQ_getExporterData")) {
			return exporterDataService.getExportedDataList("user", pageNo, pageSize);
		}
	}
	
	/**
	 * Gets the exporter data by domain and date.
	 *
	 * @param domain the domain
	 * @param startDate the start date
	 * @param endDate the end date
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @return the exporter data by domain and date
	 */
	@CarIQPublicAPIMethod(description = "Get ExporterData by domain and date wise", responseClass = ExporterDataJSON_Out.class, internal = true)
	@RequestMapping(value = "/enlist/{domain}/{startDate}/{endDate}/{pageNo}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public List<ExporterDataJSON_Out> getExporterDataByDomainAndDate(
			@CarIQPublicAPIParameter(name = "domain", description = "the domain") @PathVariable String domain,
			@CarIQPublicAPIParameter(name = "startDate", description = "startDate") @PathVariable String startDate,
			@CarIQPublicAPIParameter(name = "endDate", description = "endDate") @PathVariable String endDate,
			@CarIQPublicAPIParameter(name = "pageNo", description = "the pageNo") @PathVariable int pageNo,
			@CarIQPublicAPIParameter(name = "pageSize", description = "the pageSize") @PathVariable int pageSize) {
		try (CarIQAPI _getExporterDataByDomainAndDate = new CarIQAPI("ExportedData_getExporterDataByDomainAndDate")) {
			return exporterDataService.getExporterDataByDomainAndDate(domain,startDate,endDate, pageNo, pageSize);
		}
	}
		
	/**
	 * Gets the uploaded file exporter data.
	 *
	 * @param params
	 *            the params
	 * @param pageNo
	 *            the page no
	 * @param pageSize
	 *            the page size
	 * @return the uploaded file exporter data
	 */
	@CarIQPublicAPIMethod(description = "Enlist Uploaded File ExporterData", responseClass = ExporterDataJSON_Out.class, internal = true)
	@RequestMapping(value = "/enlist/uploadedFiles/{domain}/{startDate}/{endDate}/{pageNo}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public List<ExporterDataJSON_Out> getUploadedFileExporterData(@CarIQPublicAPIParameter(name = "domain", description = "the domain") @PathVariable String domain,
			@CarIQPublicAPIParameter(name = "startDate", description = "startDate") @PathVariable String startDate,
			@CarIQPublicAPIParameter(name = "endDate", description = "endDate") @PathVariable String endDate,
			@CarIQPublicAPIParameter(name = "pageNo", description = "the pageNo") @PathVariable int pageNo,
			@CarIQPublicAPIParameter(name = "pageSize", description = "the pageSize") @PathVariable int pageSize) {
		try (CarIQAPI _getResource = new CarIQAPI("ExportedData_getUploadedFileExporterData")) {
			return exporterDataService.getUploadedFileExporterDataList(domain,startDate,endDate, pageNo, pageSize);
		}
	}

	/**
	 * Gets the uploaded file by key word.
	 *
	 * @param domain the domain
	 * @param keyWord the key word
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @return the uploaded file by key word
	 */
	@CarIQPublicAPIMethod(description = "Enlist uploaded file by keyword search", responseClass = ExporterDataJSON_Out.class, internal = true)
	@RequestMapping(value = "/enlist/uploadedFilesByKeyWord/{domain}/{keyWord}/{pageNo}/{pageSize}", method = RequestMethod.GET)
	@ResponseBody
	public List<ExporterDataJSON_Out> getUploadedFilesByKeyWord(@CarIQPublicAPIParameter(name = "domain", description = "the domain") @PathVariable String domain,
			@CarIQPublicAPIParameter(name = "keyWord", description = "keyWord") @PathVariable String keyWord,
			@CarIQPublicAPIParameter(name = "pageNo", description = "the pageNo") @PathVariable int pageNo,
			@CarIQPublicAPIParameter(name = "pageSize", description = "the pageSize") @PathVariable int pageSize) {
		try (CarIQAPI _getResource = new CarIQAPI("ExportedData_getUploadedFilesByKeyWord")) {
			return exporterDataService.getUploadedFilesByKeyWord(domain,keyWord, pageNo, pageSize);
		}
	}
	/**
	 * Upload file.
	 *
	 * @param file
	 *            the file
	 * @param description
	 *            the description
	 * @param domain
	 *            the domain
	 * @return the response json
	 */
	@CarIQPublicAPIMethod(description = "upload file", responseClass = ResponseJson.class, internal = true)
	@RequestMapping(value = "/load/{domain}", method = RequestMethod.POST, consumes = { "multipart/form-data" })
	@ResponseBody
	@Transactional
	public ResponseJson uploadFile(@RequestParam("file") MultipartFile file, @RequestParam("text") String description,
			@CarIQPublicAPIParameter(name = "domain", description = "domain name") @PathVariable String domain) {
		try (CarIQAPI _getResource = new CarIQAPI("ExportedData_uploadFile")) {
			return exporterDataService.uploadFile(file, description, domain);
		}
	}
}