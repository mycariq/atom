package com.cariq.toolkit.coreiq.tableUtil.application.mis;

import com.cariq.toolkit.coreiq.tableUtil.impl.ProcessingContext;
import com.cariq.toolkit.coreiq.tableUtil.impl.RowColumn;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.CellAddressIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.CellIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.NameIndexIdentifiable;

/**
 * MISColumn is responsible for managing "Column" in MIS Table.
 * @author hrishi
 *
 * @author hrishi
 *
 */
public class MISColumn extends MISRowColumn {
	public MISColumn(String style, String label, String evaluatorClass, String argumentJSON) {
		super(style, label, evaluatorClass, argumentJSON);
	}
	
	public MISColumn(String label, String evaluatorClass, String argumentJSON) {
		this(null, label, evaluatorClass, argumentJSON);
	}

	public MISColumn() {
		super();
	}
	
	@Override
	public CellIfc locate(ProcessingContext processingContext, CellAddressIfc cellAddress) {
		// TODO Auto-generated method stub
		return super.locate(processingContext, cellAddress);
	}

	/**
	 * This is a special and interesting function.
	 * It matches with the column of the current cell - first by index then by name
	 */
	@Override
	public boolean isMatching(ProcessingContext processingContext, CellAddressIfc cellAddress) {
		// If cellAddress is not there, based on processing Context, check the current cells index
		NameIndexIdentifiable columnIdentifier = cellAddress.getColumnIdentifier();
		
		// see if index matches
		if (columnIdentifier == null) {
			// from processingContext, get the column under process.
			RowColumn currentColumn = processingContext.getCurrentColumn();
			return currentColumn.getIndex() == this.getIndex();
		}
		
		// If identifier specified, use it
		// check index
		Integer colIndex = columnIdentifier.getIndex();
		if (colIndex != null)
			return colIndex == this.getIndex();
		
		// see if name matches
		String name = columnIdentifier.getName();
		if (name != null)
			return name.equalsIgnoreCase(this.getLabel());
		
		// all bets are off
		return false;
	}
}
