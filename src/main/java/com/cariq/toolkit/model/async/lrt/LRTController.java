//package com.cariq.toolkit.model.async.lrt;
//
//import java.util.List;
//
//import javax.servlet.http.HttpServletResponse;
//
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import com.cariq.toolkit.model.async.WorkItem;
//import com.cariq.toolkit.publicapi.CarIQPublicAPI;
//import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
//import com.cariq.toolkit.publicapi.CarIQPublicAPIParameter;
//import com.cariq.toolkit.utils.CQObjectConvertor;
//import com.cariq.toolkit.utils.CarIQAPI;
//import com.cariq.toolkit.utils.Utils;
//
//// TODO: Auto-generated Javadoc
///**
// * The Class SecuredActionController.
// */
//@CarIQPublicAPI(name = "LRT", description = "Long Running Task API")
//@RequestMapping("/lrt")
//@Controller
//public class LRTController {
//	/**
//	 * Create dummy API Item which takes given amount of time
//	 * 
//	 * @param litmusRequest
//	 * @param response
//	 * @return
//	 */
//	@CarIQPublicAPIMethod(description = "Fetch the response of Long Running Task", responseClass = LRT.class)
//	@RequestMapping(value = "/{token}", method = RequestMethod.GET)
//	@ResponseBody
//	public LRTJSON fetchResult(@CarIQPublicAPIParameter(name = "token", description = "UUID of Long Running Task (LRT)") @PathVariable("token") final String token,
//			HttpServletResponse response) {
//		try (CarIQAPI _fetchAPIRequest = new CarIQAPI("LRTController.fetchResult")) {
//			return LRTServerHelper.fetch(token);
//		}
//	}
//
//	/**
//	 * Create dummy API Item which takes given amount of time
//	 * 
//	 * @param litmusRequest
//	 * @param response
//	 * @return
//	 */
//	@CarIQPublicAPIMethod(description = "Enlist Long Running Tasks", responseClass = LRT.class)
//	@RequestMapping(value = "/enlist", method = RequestMethod.GET)
//	@ResponseBody
//	public List<LRTJSON> enlist(HttpServletResponse response) {
//		try (CarIQAPI _getStatusRequest = new CarIQAPI("LRTController.enlist")) {
//			return Utils.convertList(WorkItem.getWorkItemByType(WorkItem.LRT, 1, 100), new CQObjectConvertor<WorkItem, LRTJSON>(){
//				@Override
//				public LRTJSON convert(WorkItem in) {
//					return LRTServerHelper.convert(in);
//				}});
//		}
//	}
//}
