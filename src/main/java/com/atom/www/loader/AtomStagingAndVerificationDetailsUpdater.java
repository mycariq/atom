package com.atom.www.loader;

import com.cariq.toolkit.coreiq.loader.CarIQLoaderContext;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderWrapper;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderWrapper.STATUS;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderWrapperImpl;
import com.cariq.toolkit.coreiq.loader.CarIQWrappedLoaderTemplate;
import com.cariq.toolkit.utils.GenericJSON;

public class AtomStagingAndVerificationDetailsUpdater extends CarIQWrappedLoaderTemplate {

	private static final String TheAtomLoader = "AtomStagingAndVerificationDetailsUpdater"; 
	
	private static final String TheLoader = "StagingAndVerificationDetailsUpdater";
	
	private static final String TheLoaderDescription = "Load staging data";
	
	public AtomStagingAndVerificationDetailsUpdater() {
		super(TheAtomLoader, TheLoaderDescription);
	}

	@Override
	protected CarIQLoaderWrapper getWrapper() {
		return new CarIQLoaderWrapperImpl(TheLoader, TheLoaderDescription);
	}

	@Override
	protected void doLoad(GenericJSON json, CarIQLoaderContext context, STATUS status) {
		
		
	}
}
