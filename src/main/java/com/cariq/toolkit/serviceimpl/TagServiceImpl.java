package com.cariq.toolkit.serviceimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.model.Tag;
import com.cariq.toolkit.model.Tag.Tag_In;
import com.cariq.toolkit.model.Tag.Tag_Out;
import com.cariq.toolkit.service.TagService;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.ResponseWithIdJson;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.service.CarIQRestClientService;


/**
 * The Class TagServiceImpl.
 */
@Service
public class TagServiceImpl implements TagService {

	/** The rest service. */
	@Autowired
	CarIQRestClientService restService;

	@Override
	@Transactional
	public ResponseWithIdJson createOrGetTag(Tag_In params) {
		Tag tag = null;
		try {
				// Validate tagname
				if (null == params.getName()) {
					Utils.handleException("Tagname invalid");
				}
				tag = Tag.createOrGet(params.getName(), params.getDescription());
				
		} catch (Exception e) {
			Utils.handleException("Failed to create OR get tag, Exception: " + e.getMessage());
		}
		return new ResponseWithIdJson(tag.getId().toString(), "Tag : " + params.getName() + " created/obtained successfully.");
	}

	@Override
	public List<Tag_Out> getAllTags(int pageNo, int pageSize) {
		List<Tag_Out> resultList = new ArrayList<>();
		try {
			List<Tag> tags = Tag.getAllObjects(pageNo, pageSize);
			
			for (Tag tag : tags) {
				resultList.add(tag.toJSON());
			}
		} catch (Exception e) {
			Utils.handleException("Failed to get the tags, Exception: " + e.getMessage());
		}
		return resultList;
	}

	@Override
	public Tag_Out getTag(String tagName) {
		Tag_Out tagOut = null;
		try {
			Tag tag = Tag.getByName(tagName);
			if (null != tag) {
				tagOut = tag.toJSON();
			}
		}catch (Exception e) {
			Utils.handleException("Failed to get tag: " + tagName + ", Exception: " + e.getMessage());
		}
		
		return tagOut;
	}

	@Override
	@Transactional
	public ResponseJson updateTag(Tag_In params, String tagName) {
		try {
			Tag tag = Tag.getByName(tagName);
			if (tag == null) {
				Utils.handleException("Tag " + tagName + " is invalid");
			}
			tag.updateTag(params.getName(), params.getDescription());
			tag.merge();
		} catch (Exception e) {
			Utils.handleException("Failed to update tag: " + tagName + ", exception: " + e.getMessage());
		}

		return new ResponseJson("Tag : " + tagName + " updated successfully.");
	}

	@Override
	@Transactional
	public ResponseJson deleteTag(String tagName) {
		try {
			Tag tag = Tag.getByName(tagName);
			if (tag != null) {
				// taggedobject - FK constraint
				tag.remove();
				return new ResponseJson("Tag : " + tagName + " deleted successfully.");
			}
		} catch (Exception e) {
			Utils.handleException("Failed to remove tag: " + tagName + ", exception: " + e.getMessage());
		}
		return new ResponseJson("Tag : " + tagName + " does not exist!");
	}
}