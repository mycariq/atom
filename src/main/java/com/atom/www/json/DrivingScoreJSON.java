package com.atom.www.json;

/**
 * @author nitin
 *
 */
public class DrivingScoreJSON {

	private Double distance;
	private Double travelTimeInNight;
	private Integer rashAccelerationCount;
	private Integer rashBrakingCount;
	private Double overSpeedingPercent;
	private Double idlingPercent;
	
	public DrivingScoreJSON() {
		super();
	}
	
	public DrivingScoreJSON(Double distance, Double travelTimeInNight, Integer rashAccelerationCount,
			Integer rashBrakingCount, Double overSpeedingPercent, Double idlingPercent) {
		super();
		this.distance = distance;
		this.travelTimeInNight = travelTimeInNight;
		this.rashAccelerationCount = rashAccelerationCount;
		this.rashBrakingCount = rashBrakingCount;
		this.overSpeedingPercent = overSpeedingPercent;
		this.idlingPercent = idlingPercent;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public Double getTravelTimeInNight() {
		return travelTimeInNight;
	}

	public void setTravelTimeInNight(Double travelTimeInNight) {
		this.travelTimeInNight = travelTimeInNight;
	}

	public Integer getRashAccelerationCount() {
		return rashAccelerationCount;
	}

	public void setRashAccelerationCount(Integer rashAccelerationCount) {
		this.rashAccelerationCount = rashAccelerationCount;
	}

	public Integer getRashBrakingCount() {
		return rashBrakingCount;
	}

	public void setRashBrakingCount(Integer rashBrakingCount) {
		this.rashBrakingCount = rashBrakingCount;
	}

	public Double getOverSpeedingPercent() {
		return overSpeedingPercent;
	}

	public void setOverSpeedingPercent(Double overSpeedingPercent) {
		this.overSpeedingPercent = overSpeedingPercent;
	}

	public Double getIdlingPercent() {
		return idlingPercent;
	}

	public void setIdlingPercent(Double idlingPercent) {
		this.idlingPercent = idlingPercent;
	}
}