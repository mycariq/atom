/**
 * 
 */
package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.atom.www.helper.ConfigHelper;
import com.atom.www.helper.MISReportHelper;
import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.coreiq.exporter.UnityExportEnhancer;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.Domain.Domain_Out;
import com.cariq.toolkit.model.ProxyObject;
import com.cariq.toolkit.model.Timeline;
import com.cariq.toolkit.model.Timeline.Timeline_Out;
import com.cariq.toolkit.model.User.User_In;
import com.cariq.toolkit.model.async.lrt.LRTClientHelper;
import com.cariq.toolkit.model.async.lrt.LRTJSON;
import com.cariq.toolkit.model.async.lrt.LRTRestCallFetcher;
import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.utils.service.CarIQRestClientService;
import com.google.common.base.Strings;

/**
 * @author santosh
 *
 */
@Configurable
public class MISTATEnhancerExporter extends CarIQExporterTemplate {

	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("MISTATEnhancerExporter");

	public static final String EXPORTER = "MISTATEnhancerExporter";

	MISReportHelper reportHelper = new MISReportHelper();

	public static final String TIER_A = "Tier-A", TIER_B = "Tier-B", NA = "N/A", NONE = "None";

	public static final String POLICY_NUMBER = "Policy Number", POLICY_STAGE = "Policy Stage",
			POLICY_STATE = "Policy State", SUBSCRIPTION_STATE = "SubscriptionState",
			POLICY_STATE_DATE = "Policy state date", POLICY_CATEGORY = "PolicyCategory",
			CARIQ_ENTRY_DATE = "CarIQ Entry Date", POLICY_ISSUE_DATE = "Policy Issue Date",
			POLICY_EXPIRY_DATE = "Policy Expiry Date", POLICY_DEACTIVATED_ON = "PolicyDeactivatedOn",
			POLICY_RENEWED_ON = "PolicyRenewedOn", NC_DATE = "NC Date", NI_DATE = "NI Date",
			CUSTOMER_VERIFIED_DATE = "Customer Verified Date", DEVICE_REQUESTED_DATE = "Device Requested Date",
			SHIPPED_OR_FIELD_ASSIGNED_DATE = "Shipped or Field Assigned Date", DEVICE_ADDED_DATE = "Device Added Date",
			CAR_CONNECTED_DATE = "Car Connected Date", EXPIRY_MONTH = "Expiry Month", ISSUE_MONTH = "Issue Month",
			POLICY_ORDER_DATE = "PolicyOrderDate", POLICY_ORDER_MONTH = "PolicyOrderMonth",
			SHIPPING_MONTH = "ShippingMonth", CONNECTION_MONTH = "ConnectionMonth", VERIFIED_STATUS = "Verified Status",
			DEVICE_REQUESTED_STATUS = "Device Requested Status",
			SHIPPED_OR_FIELD_ASSIGNED_STATUS = "Shipped or Field Assigned Status",
			DEVICE_ADDED_STATUS = "Device Added Status", CAR_CONNECTED_STATUS = "Car Connected Status",
			SHIPMENT_STATUS = "Shipment_Status", AGENCY = "Agency", CUSTOMER_FIRST_NAME = "Customer First Name",
			CUSTOMER_LAST_NAME = "Customer Last Name", CUSTOMER_CITY = "Customer City",
			CUSTOMER_STATE = "Customer State", CUSTOMER_PINCODE = "Customer PinCode",
			CUSTOMER_MOBILE_NUMBER = "Customer Mobile Number", CUSOMTER_EMAIL = "Customer Email", CAR_MAKE = "Car Make",
			CAR_MODEL = "Car Model", CAR_ID = "CarId", CAR_REGISTRATION_NUMBER = "Car Registration Number",
			DEVICE_ID = "Device Id", SIM_IMSI_NUMBER = "Sim IMSI Number", KMS_DRIVEN = "KmsDriven",
			CITY_TIER = "City Tier", CARIQ_ENTRY_MONTH = "CarIQ Entry Month", WEEKS = "weeks",
			VERIFIED_TAT = "Verified TAT", DEVICE_REQUESTED_TAT = "Device Requested TAT",
			DEVICE_SHIPPED_TAT = "Device Shipped TAT", CAR_CONNECTED_TAT = "Car Connected TAT",
			POLICY_RENEWAL_COUNT = "PolicyRenewalCount", VERTICAL = "Vertical", SUB_VERTICAL = "SubVertical",
			NEXT_ACTION = "NextAction", STAGING_TAGS = "StagingTags", FIRST_NAME = "First Name",
			LAST_NAME = "Last Name";

	public static final String CreatedAfter = "CreatedAfter", CreatedAfterIncludeInvalid = "CreatedAfterIncludeInvalid";
	private static final String CARIQ_LOADER = "StagingAndInsurancePolicyDetailsExporterLoader";

	public static final String FileType = "FileType", MESSAGES = "messages", QueryType = "QueryType",
			DOMAIN_NAME = "domain";
	
	public static final String LOADER_URL = "Cariq/admin/loader/loadSingle/" + CARIQ_LOADER;

	@Autowired
	CarIQRestClientService restService;
	
	private ConfigHelper configHelper;

	public MISTATEnhancerExporter() {
		this(EXPORTER, "MIS TAT Enhancer Exporter");
	}

	public MISTATEnhancerExporter(String name, String description) {
		super(name, description);

		this.configHelper = new ConfigHelper();

		addQueryType(CreatedAfter, "Valid Policies Created after given date",
				Arrays.asList(new QueryDefinitionAttribute(CreatedAfter, "Start timestamp", Date.class)));

		addQueryType(CreatedAfterIncludeInvalid, "All Policies - including invalid - Created after given date",
				Arrays.asList(new QueryDefinitionAttribute(CreatedAfter, "Start timestamp", Date.class)));

		// Support roles
		addSupportedRoles(Arrays.asList(Utils.ADMIN_ROLE));
	}

	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		try (ProfilePoint _doExport = ProfilePoint.profileAction(EXPORTER + "_doExport")) {
			if (pageNo > 1) {
				logger.debug(String.format("Pagination not supported for exporter: %s", EXPORTER));
				return null;
			}

			// Validate query types
			if (!CreatedAfter.equalsIgnoreCase(queryType) && !CreatedAfterIncludeInvalid.equalsIgnoreCase(queryType))
				Utils.handleException(String.format("Invalid query type: %s", queryType));

			// Get domain by domain name
			Domain domainObj = Domain.getByName(Utils.getValue(json, DOMAIN_NAME));
			Utils.checkNotNull(domainObj, "Domain not found ");

			// This must be timing out - while calling loader on CarIQ
			Domain_Out domain = domainObj.toJSON();
			// String fileURL = "https://cinilgprodstorageaccount.blob.core.windows.net/cariqcarlogstore/exporter/StagingAndInsurancePolicyDetailsNewExporter-230307095743343.zip";
			String fileURL = getPolicyDetailsFileURL(queryType, json, domain);
			if (Strings.isNullOrEmpty(fileURL)) {
				logger.debug(String.format("File url not found for exporter: %s", EXPORTER));
				return new ArrayList<GenericJSON>();
			}

			// Get staging and policy data from csv data exporter.
			// Read from CSV and create the Exported data using old ATOM logic
			json.add(CSVDataExporter.FILE_URL, fileURL);
			List<GenericJSON> responseList = getStagingAndPolicyData(json, pageNo, pageSize);
			if (Utils.isNullOrEmpty(responseList)) {
				logger.debug(String.format("Got empty for exporter: %s", EXPORTER));
				return new ArrayList<GenericJSON>();
			}

			return responseList;
		}
	}

	private List<GenericJSON> getStagingAndPolicyData(GenericJSON json, int pageNo, int pageSize) {
		try (ProfilePoint _getStagingAndPolicyData = ProfilePoint
				.profileAction(EXPORTER + "_getStagingAndPolicyData")) {

			CSVDataExporter exporter = new CSVDataExporter();
			return exporter.doExport(CSVDataExporter.FROM_FILE_URL, json, pageNo, pageSize);
		}
	}

	@SuppressWarnings("unchecked")
	private String getPolicyDetailsFileURL(String queryType, GenericJSON json, Domain_Out domain) {
		try (ProfilePoint _getPolicyDetailsFileURL = ProfilePoint
				.profileAction(EXPORTER + "_getPolicyDetailsFileURL")) {

			// Adding file type into input json
			json.add(QueryType, queryType);
			json.add(FileType, CarIQFileUtils.CSV);

			// REST API call - for given Exporter input file url
			String url = configHelper.makeUrl(LOADER_URL, domain.getUrl());
			logger.debug("CarIQ Rest Loader Url: " + url + ", input Json : " + json);

			ResponseJson response = restService.post(json, url, configHelper.getSecurityToken(), ResponseJson.class);
			logger.debug(
					"CarIQ Rest Collecting response: " + response + ", object from " + domain.getUrl() + " domain");
			if (null == response)
				return null;

			// <HVN> Response to Loader.loadSingle is a ResponseJson - made from LoaderContext - which contains Messages.
			// We are using the first message to get the LRT Token or file url.
			// THis is a hacky way of doing it. The message used for 2 possible ways is not elegant
			List<String> responseList = Utils.downCast(List.class, response.getMessages());
			if (Utils.isNullOrEmpty(responseList))
				return null;

			// ResponseJSON consists of messagelist - first being the FileUrl - or, LRT
			GenericJSON lrtTokenJSON = Utils.getJSonObject(responseList.get(0), GenericJSON.class);
			if (null == lrtTokenJSON)
				return null;

			// In normal cases, fileJSON would be responseJSON
			logger.debug("Received LRT token from CarIQ Loader: " + lrtTokenJSON);
			GenericJSON fileJSON = lrtTokenJSON;

			// Check if lrtTokenJSON is LRT - if so, wait for 60 minutes, Checking every 30 seconds for the update
			if (LRTClientHelper.isLRT(lrtTokenJSON)) {
				LRTJSON completedLRT = LRTClientHelper.waitForLRTResponse(LRTClientHelper.getLRTToken(lrtTokenJSON),
						60 * Utils.minuteSeconds,
						new LRTRestCallFetcher(restService,
								configHelper.makeUrl(LRTClientHelper.getFetchLRTBaseUrl("Cariq"), domain.getUrl()),
								configHelper.getSecurityToken()));
				if (null == completedLRT)
					return null;

				// LRT Response is a Response JSON
				fileJSON = Utils.getJSonObject(completedLRT.getResponse().toString(), GenericJSON.class);
				if (Utils.isNullOrEmpty(fileJSON))
					return null;
			}

			return Utils.downCast(String.class, fileJSON.get(CSVDataExporter.FILE_URL));
		} catch (Exception e) {
			logger.error("Got Exception: " + e.getMessage());
			Utils.handleException(e);
		}

		// unreachable code
		return null;
	}

	// Enhancer to process the attributes
	@Override
	protected MISTATEnhancerExporterEnhancer getEnhancer() {
		return new MISTATEnhancerExporterEnhancer();
	}

	public static class MISTATEnhancerExporterEnhancer extends UnityExportEnhancer {

		public static final String EXPORTER_ENHANCER = "MISTATEnhancerExporterEnhancer";
		
		private static CarIQLogger enhancerLogger = CarIQToolkitHelper.logger.getLogger("MISTATEnhancerExporterEnhancer");

		public static final String policy_Number = "policyNumber", subscription_State = "SubscriptionState",
				staging_Tags = "StagingTags", staging_CreatedOn = "StagingCreatedOn",
				staging_PolicyIssueDate = "Staging_policyIssueDate",
				staging_policyExpiryDate = "Staging_policyExpiryDate", policy_DeactivatedOn = "PolicyDeactivatedOn",
				policy_RenewedOn = "PolicyRenewedOn", policy_CreationTime = "Policy_CreationTime",
				tier_City = "TierCity", device_OnboardedOn = "Device_OnboardedOn",
				vehicle_FirstSeenOn = "Car_FirstSeenOn", policy_OrderDate = "PolicyOrderDate", shipped_On = "ShippedOn",
				verified = "Verified", shipment_Status = "Shipment_Status", agency = "Agency",
				customer_FirstName = "customerFirstName", customer_LastName = "customerLastName",
				staging_CustomerCity = "Staging_customerCity", staging_CustomerState = "Staging_customerState",
				staging_CustomerPinCode = "Staging_customerPinCode",
				staging_CustomerMobileNumber = "Staging_customerMobileNumber",
				staging_CustomerEmail = "Staging_customerEmail", vehicle_Make = "Staging_carMake",
				vehicle_Model = "Staging_carModel", vehicle_Id = "CarId",
				vehicle_RegistrationNumber = "carRegistrationNumber", device_Id = "DeviceId",
				sim_IMSI_Numver = "Sim_IMSI_Number", kms_Driven = "Car_Distance_Since_Connected",
				staging_Month = "StagingMonth", policy_RenewalCount = "PolicyRenewalCount",
				staging_Channel = "StagingChannel", staging_SubChannel = "StagingSubChannel",
				next_Action = "NextAction";

		@Override
		protected List<GenericJSON> doEnhance(String queryType, GenericJSON inputJSON, List<GenericJSON> exportJSON) {
			try (ProfilePoint _doEnhance = ProfilePoint.profileActivity(EXPORTER_ENHANCER + "_doEnhance")) {
				List<GenericJSON> retValList = new ArrayList<GenericJSON>();
				enhancerLogger.debug("Working on doEnahcer");

				String objectType = "InsurancePolicy";
				Domain domain = Domain.getByName(Utils.getValue(inputJSON, DOMAIN_NAME));

				enhancerLogger.debug("Got domain, now creating final output list");
				for (GenericJSON input : exportJSON) {
					//enhancerLogger.debug("input json: " + input);

					String signature = Utils.getValue(input, policy_Number);
					// TODO: No need for MIS TAT report
//					ProxyObject proxy = ProxyObject.getByDomainSignatureObjectType(domain, signature, objectType);
//
//					List<Timeline> timelineObjects = Timeline.getUserTimeline(proxy);
//					List<Timeline_Out> timelines = Utils.convertList(timelineObjects, Timeline_Out.class);
//					Timeline_Out timeline = StagingAndPolicyHelper.getLatestTimeline(timelines);

					GenericJSON output = new GenericJSON();
					output.add(POLICY_NUMBER, signature);
					output.add(POLICY_STAGE, StagingAndPolicyHelper.getPolicyStage(input));
					output.add(POLICY_STATE, StagingAndPolicyHelper.getPolicyState(input));
					output.add(SUBSCRIPTION_STATE, Utils.getValue(input, subscription_State));
					output.add(POLICY_STATE_DATE, StagingAndPolicyHelper.NOT_AVAILABLE);
//					output.add(POLICY_STATE_DATE,
//							(null == timeline) ? StagingAndPolicyHelper.NOT_AVAILABLE : timeline.getCreatedOn());
					output.add(POLICY_CATEGORY, getPolicyCategory(input));

					String policyEntryDate = Utils
							.convertToyyyymmdd_hhmmssFormat(Utils.getValue(Date.class, input, staging_CreatedOn));
					output.add(CARIQ_ENTRY_DATE, policyEntryDate);
					output.add(POLICY_ISSUE_DATE, Utils.getValue(input, staging_PolicyIssueDate));
					output.add(POLICY_EXPIRY_DATE, Utils.getValue(input, staging_policyExpiryDate));
					output.add(POLICY_DEACTIVATED_ON, Utils.getValue(input, policy_RenewedOn));
					output.add(POLICY_RENEWED_ON, Utils.getValue(input, policy_RenewedOn));

//					output.add(NC_DATE, StagingAndPolicyHelper.getNotContactableDate(timelines));
//					output.add(NI_DATE, StagingAndPolicyHelper.getNotInterestedDate(timelines));
					output.add(NC_DATE, null); // TODO: revrert back to orignal data
					output.add(NI_DATE, null); // TODO: revrert back to orignal data
					
					output.add(CUSTOMER_VERIFIED_DATE, Utils.getValue(input, policy_CreationTime));
					output.add(DEVICE_REQUESTED_DATE, StagingAndPolicyHelper.getDeviceRequestedOnDate(input));

					String tierCity = Utils.getValue(input, tier_City);
					output.add(SHIPPED_OR_FIELD_ASSIGNED_DATE,
							(TIER_A.equalsIgnoreCase(tierCity)) ? StagingAndPolicyHelper.getDeviceRequestedOnDate(input)
									: StagingAndPolicyHelper.getDeviceShippedOnDate(input));

					output.add(DEVICE_ADDED_DATE, Utils.getValue(input, device_OnboardedOn));
					output.add(CAR_CONNECTED_DATE, Utils.getValue(input, vehicle_FirstSeenOn));
					output.add(EXPIRY_MONTH, getMonthYearString(input, staging_policyExpiryDate));
					output.add(ISSUE_MONTH, getMonthYearString(input, staging_PolicyIssueDate));
					output.add(POLICY_ORDER_DATE, Utils.getValue(input, policy_OrderDate));
					output.add(POLICY_ORDER_MONTH, getMonthYearString(input, policy_OrderDate));
					output.add(SHIPPING_MONTH, getMonthYearString(input, shipped_On));
					output.add(CONNECTION_MONTH, getMonthYearString(input, vehicle_FirstSeenOn));
					output.add(VERIFIED_STATUS, Utils.getValue(input, verified));

					output.add(DEVICE_REQUESTED_STATUS,
							null != StagingAndPolicyHelper.getDeviceRequestedOnDate(input) ? true : false);
					output.add(SHIPPED_OR_FIELD_ASSIGNED_STATUS,
							(TIER_A.equalsIgnoreCase(tierCity))
									? (null != StagingAndPolicyHelper.getDeviceRequestedOnDate(input) ? true : false)
									: (null != StagingAndPolicyHelper.getDeviceShippedOnDate(input) ? true : false));
					output.add(DEVICE_ADDED_STATUS, (null != Utils.getValue(input, device_OnboardedOn)) ? true : false);
					output.add(CAR_CONNECTED_STATUS,
							(null != Utils.getValue(input, vehicle_FirstSeenOn)) ? true : false);

					output.add(SHIPMENT_STATUS, Utils.getValue(input, shipment_Status));
					output.add(AGENCY, Utils.getValue(input, agency));
					output.add(CUSTOMER_FIRST_NAME, Utils.getValue(input, customer_FirstName));
					output.add(CUSTOMER_LAST_NAME, Utils.getValue(input, customer_LastName));
					output.add(CUSTOMER_CITY, Utils.getValue(input, staging_CustomerCity));
					output.add(CUSTOMER_STATE, Utils.getValue(input, staging_CustomerState));
					output.add(CUSTOMER_PINCODE, Utils.getValue(input, staging_CustomerPinCode));
					output.add(CUSTOMER_MOBILE_NUMBER, Utils.getValue(input, staging_CustomerMobileNumber));
					output.add(CUSOMTER_EMAIL, Utils.getValue(input, staging_CustomerEmail));
					output.add(CAR_MAKE, Utils.getValue(input, vehicle_Make));
					output.add(CAR_MODEL, Utils.getValue(input, vehicle_Model));
					output.add(CAR_ID, Utils.getValue(input, vehicle_Id));
					output.add(CAR_REGISTRATION_NUMBER, Utils.getValue(input, vehicle_RegistrationNumber));
					output.add(DEVICE_ID, Utils.getValue(input, device_Id));
					output.add(SIM_IMSI_NUMBER, Utils.getValue(input, sim_IMSI_Numver));
					output.add(KMS_DRIVEN, Utils.getValue(input, kms_Driven));
					output.add(CITY_TIER, Utils.getValue(input, tierCity));
					output.add(CARIQ_ENTRY_MONTH, Utils.getValue(input, staging_Month));
					output.add(WEEKS, StagingAndPolicyHelper.getWeeksFromStagingCreatedDate(input));

					String policyCreationDate = Utils
							.convertToyyyymmdd_hhmmssFormat(Utils.getValue(Date.class, input, policy_CreationTime));
					output.add(VERIFIED_TAT, StagingAndPolicyHelper.calculateTAT(policyEntryDate, policyCreationDate));
					output.add(DEVICE_REQUESTED_TAT, StagingAndPolicyHelper.calculateTAT(policyEntryDate,
							StagingAndPolicyHelper.getDeviceRequestedOnDate(input)));
					output.add(DEVICE_SHIPPED_TAT, StagingAndPolicyHelper.calculateTAT(policyEntryDate,
							StagingAndPolicyHelper.getDeviceShippedOnDate(input)));
					
					String vehicleFirstSeen = Utils
							.convertToyyyymmdd_hhmmssFormat(Utils.getValue(Date.class, input, vehicle_FirstSeenOn));
					output.add(CAR_CONNECTED_TAT,
							StagingAndPolicyHelper.calculateTAT(policyEntryDate, vehicleFirstSeen));

					output.add(POLICY_RENEWAL_COUNT, Utils.getValue(input, policy_RenewalCount));
					output.add(VERTICAL, null == Utils.getValue(input, staging_Channel) ? NA
							: Utils.getValue(input, staging_Channel));
					output.add(SUB_VERTICAL, null == Utils.getValue(input, staging_SubChannel) ? NA
							: Utils.getValue(input, staging_SubChannel));
					output.add(NEXT_ACTION, Utils.getValue(input, next_Action));
					output.add(STAGING_TAGS, Utils.getValue(input, staging_Tags));

					// TODO: revrert back to orignal data after testing
//					User_In user = getUser(timelines);
//					output.put(FIRST_NAME, null != user ? user.getFirstName() : NONE);
//					output.put(LAST_NAME, null != user ? user.getLastName() : NONE);

					//enhancerLogger.debug("final output json: " + output);
					retValList.add(output);
				}

				enhancerLogger.debug("final previous records:: " + exportJSON.size());
				enhancerLogger.debug("final output records:: " + retValList.size());
				return retValList;
			}
		}

		private User_In getUser(List<Timeline_Out> timelines) {
			try (ProfilePoint _getUser = ProfilePoint.profileActivity(EXPORTER_ENHANCER + "_getUser")) {
				if (Utils.isNullOrEmpty(timelines))
					return null;

				for (Timeline_Out timeline_Out : timelines) {
					if (null != timeline_Out.getUser())
						return timeline_Out.getUser();
				}

				return null;
			}
		}

		public static String getMonthYearString(GenericJSON input, String key) {
			try (ProfilePoint _getMonthYearString = ProfilePoint
					.profileActivity(EXPORTER_ENHANCER + "_getMonthYearString")) {

				String month = StagingAndPolicyHelper.getMonth(Utils.getValue(input, key));
				if (null == month)
					return StagingAndPolicyHelper.NOT_AVAILABLE;

				String year = StagingAndPolicyHelper.getYear(Utils.getValue(input, key));

				return month.concat("-").concat(year);
			}
		}

		public static String getPolicyCategory(GenericJSON input) {
			try (ProfilePoint _getPolicyCategory = ProfilePoint
					.profileActivity(EXPORTER_ENHANCER + "_getPolicyCategory")) {

				String subscriptionState = Utils.getValue(input, subscription_State);
				Boolean isValid = Utils.getValue(Boolean.class, input, "Valid");
				String state = StagingAndPolicyHelper.getPolicyState(input);
				String stagingTags = Utils.getValue(input, staging_Tags);

				// If subscription state is not null then processed
				if (subscriptionState != null)
					return StagingAndPolicyHelper.CATEGORY_PROCESSED;

				// If NC and valid then not-verified-nc
				if (StagingAndPolicyHelper.TAG_NOT_CONTACTABLE.equals(state) && isValid)
					return StagingAndPolicyHelper.CATEGORY_NOT_VERIFIED_NC;

				// If NI and valid then not-verified-ni
				if (StagingAndPolicyHelper.TAG_NOT_INTERESTED.equals(state) && isValid)
					return StagingAndPolicyHelper.CATEGORY_NOT_VERIFIED_NI;

				// If subscription state is null and policy is valid then not verified
				if (subscriptionState == null && isValid)
					return StagingAndPolicyHelper.CATEGORY_NOT_VERIFIED;

				// If carnot and invalid then invalid-carnot
				if (StagingAndPolicyHelper.STAGING_TAG_CARNOT.equals(stagingTags) && !isValid)
					return StagingAndPolicyHelper.CATEGORY_INVALID_CARNOT;

				// If invalid transaction or new invalid and not valid then invalid-transaction.
				if ((StagingAndPolicyHelper.STAGING_TAG_INVALID_TRANSACTION.equals(stagingTags)
						|| StagingAndPolicyHelper.STAGING_TAG_NEW_INVALID.equals(stagingTags)) && !isValid)
					return StagingAndPolicyHelper.CATEGORY_INVALID_TRANSACTION;

				// If staging tag is duplicate car and not valid then invalid-dup-car.
				if (StagingAndPolicyHelper.STAGING_TAG_DUP_CAR.equals(stagingTags) && !isValid)
					return StagingAndPolicyHelper.CATEGORY_INVALID_DUP_CAR;

				return null;
			}
		}
	}

}