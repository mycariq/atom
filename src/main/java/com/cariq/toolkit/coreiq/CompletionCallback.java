package com.cariq.toolkit.coreiq;

public interface CompletionCallback<T> {
	void onComplete(T data);
}
