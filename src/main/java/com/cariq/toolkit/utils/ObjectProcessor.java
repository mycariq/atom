package com.cariq.toolkit.utils;

import java.util.List;

/**
 * Base Interface of ObjectProcessor. It has a model to process using input.
 * 
 */
public interface ObjectProcessor<MODEL, INPUT>{
	
	void prepare();
	
	void process(INPUT input);
	
	void finish();
	
	String getStage();
	
	MODEL getModel();
	
	List<ObjectProcessor> getChildProcessors();
	
	void destroyModel();
}
