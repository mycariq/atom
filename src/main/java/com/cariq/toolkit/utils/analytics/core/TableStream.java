package com.cariq.toolkit.utils.analytics.core;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public abstract class TableStream {
	String fileName; // Filesystem presence of this
	List<Column> columns = new ArrayList<Column>(); // ordered list of columns

	public TableStream(String fileName) {
		super();
		this.fileName = fileName;
	}

	public TableStream(String fileName, List<Column> columns) {
		super();
		this.fileName = fileName;
		this.columns = columns;
	}

	public void addColumn(String column) {
		addColumn(new Column(column));
	}

	public void addColumn(Column c) {
		if (!columns.contains(c))
			columns.add(c);
	}

	public void addColumns(List<Column> newColumns) {
		for (Column newColumn : newColumns) {
			addColumn(newColumn);

		}
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Gets the file.
	 *
	 * @return the file
	 */
	public File getFile() {
		return new File(fileName);
	}

	/**
	 * @return the columns
	 */
	public List<Column> getColumns() {
		return columns;
	}

	public void removeAllColumns() {
		columns.clear();
	}
}
