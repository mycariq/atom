/**
 * 
 */
package com.cariq.toolkit.coreiq.exporter.fileexport;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import com.cariq.toolkit.coreiq.exporter.CarIQExporter;
import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.CarIQZipFile;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

/**
 * @author hrishi
 *
 */
public class ZipFileExport extends FileExportTemplate {

	/**
	 * @param exporter
	 * @param queryType
	 * @param inputJSON
	 * @param fileFormat
	 */
	public ZipFileExport(CarIQExporter exporter, String queryType, GenericJSON inputJSON, String fileFormat) {
		super(exporter, queryType, inputJSON, fileFormat);
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.exporter.fileexport.FileExportTemplate#doExecute()
	 */
	@Override
	protected File doExecute() {
		try {
			String internalFileType = determineInternalFileType();
			
			String zipExportFile = CarIQFileUtils
					.getTempFile(exporter.getName() + "-" + queryType + "-" + Utils.createDateId(true) + ".zip");

			// Create a ZIP file
			CarIQZipFile zipFile = new CarIQZipFile(zipExportFile);
			
			// Loop through all exporters to create multiple files in a zip
			List<CarIQExporter> childExporters = exporter.getChildExporters();
			for (CarIQExporter carIQExporter : childExporters) {
				FileExport fileExport = FileExportTemplate.getFileExport(carIQExporter.getName(), queryType, inputJSON, internalFileType);
				File outputFile = fileExport.execute();
				zipFile.add(outputFile.getName(), outputFile.getAbsolutePath());
			}
			
			zipFile = zipFile.build();
			
			return zipFile.getFile();
		} catch (FileNotFoundException e) {
			Utils.handleException(e);
		}
		
		return null;
	}

	/**
	 * @return
	 */
	private String determineInternalFileType() {
		// based on fileformat, determine the fileformat of internal files.
		switch (fileFormat) {
		case CarIQFileUtils.ZIP:
		case CarIQFileUtils.ZIP_CSV:
			return CarIQFileUtils.CSV;
			
		case CarIQFileUtils.ZIP_HTML:
			return CarIQFileUtils.HTML;

		default:
			break;
		};
		
		Utils.handleException("Invalid fileType for ZipFileExport: " + fileFormat);
		return fileFormat;
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.exporter.fileexport.FileExport#getMimeType()
	 */
	@Override
	public String getMimeType() {
		return "application/zip";
	}
}
