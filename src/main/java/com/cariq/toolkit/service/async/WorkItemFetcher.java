package com.cariq.toolkit.service.async;

import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.coreiq.server.LoadSummary;
import com.cariq.toolkit.model.async.WorkItem;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.Utils;



public class WorkItemFetcher {
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("WorkItemFetcher");

	List<WorkItemBucket> workItemBuckets = new ArrayList<WorkItemBucket>();

	public WorkItemFetcher(List<WorkItemBucket> workItemBuckets) {
		super();
		this.workItemBuckets = workItemBuckets;
	}
	
	public int getTotalAvailableCapacity() {
		int capacity = 0;
		for (WorkItemBucket workItemBucket : workItemBuckets) {
			capacity += workItemBucket.getCapacity();
		}
		
		return capacity;
	}
	
	public WorkItemFetcher adjustBucketCapcities(List<LoadSummary> serverWorkItemLoadSummary) {
		for (LoadSummary loadSummary : serverWorkItemLoadSummary) {
			for (WorkItemBucket workItemBucket : workItemBuckets) {
				if (!workItemBucket.isApplicableType(loadSummary.getSubCategory()))
					continue;

				// if bucket is full, can't process this item. Break out to next item
				if (workItemBucket.isFull())
					break;

				// what if it becomes negative? - then don't decrement
//				Utils.debug_log(logger, "Decrementing capacity by " + loadSummary.getCount() + " for: " + workItemBucket.getType());
				int successfullyDecremented = workItemBucket.tryDecrementCapacity(loadSummary.getSubCategory(), loadSummary.getCount());
				if (successfullyDecremented > 0)
					Utils.debug_log(logger, "Successfully decremented bucket capacity by " + successfullyDecremented + " for bucket type: " + loadSummary.getSubCategory());
				
				break; // don't process more buckets - NOTA should be reached if none succeeded
			}
		}
		
		return this;
	}
	
	public WorkItemFetcher process(List<WorkItem> readyItems) {
		for (WorkItem item : readyItems) {
			for (WorkItemBucket workItemBucket : workItemBuckets) {
				// if item is not applicable to this bucket, go to next bucket
				if (!workItemBucket.isApplicableType(item.getType()))
					continue;
				
				// if bucket is full, can't process this item. Break out to next item
				if (workItemBucket.isFull())
					break;

				
				// what if it becomes negative? - then don't decrement
//				Utils.debug_log(logger, "Putting Workitem to bucket for: " + item.getType());
				boolean success = workItemBucket.tryPutWorkItem(item);		
				if (success)
					Utils.debug_log(logger, "Successfully Put Workitem to bucket for: " + item.getType());
				
				// appropriate bucket has processed the item. Don't go to next bucket. Break to next item
				break; // don't process more buckets - NOTA should be reached if none succeeded
			}
		}
		
		// return this for chaining
		return this;
	}
	
	public List<WorkItem> getDispensableItems() {
		List<WorkItem> retval = new ArrayList<WorkItem>();
		
		for (WorkItemBucket workItemBucket : workItemBuckets) {
			retval.addAll(workItemBucket.getBucketItems());
		}
		
		Utils.debug_log(logger, "Finally found dispensableItems count: " + retval.size());
		return retval;
	}

	@Override
	public String toString() {
		return "WorkItemFetcher [workItemBuckets=" + workItemBuckets + "]";
	}
}
