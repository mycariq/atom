package com.cariq.toolkit.coreiq.tableUtil.impl;

/**
 * Basic Column from Table.. It can have style and it has 0 based index in the table
 * And it has cells - 1 per row

 * @author hrishi
 *
 */
public class Column extends RowColumn {
	public Column() {
		this(null);
	}
	
	public Column(String style) {
		super(style);
	}
}

