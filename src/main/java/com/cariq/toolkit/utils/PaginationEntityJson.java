package com.cariq.toolkit.utils;

import org.springframework.roo.addon.javabean.RooJavaBean;

public class PaginationEntityJson<T> {

	private int pageNum;
	private Long noOfRecords;
	private int totalPages;
	private T rows;

	public PaginationEntityJson() {
		// TODO Auto-generated constructor stub
	}
	
	public PaginationEntityJson(long noOfRecords, int pageNo, T detailJson, int totalPages) {
		this.pageNum = pageNo;
		this.noOfRecords = noOfRecords;
		this.rows = detailJson;
		this.totalPages = totalPages;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public Long getNoOfRecords() {
		return noOfRecords;
	}

	public void setNoOfRecords(Long noOfRecords) {
		this.noOfRecords = noOfRecords;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public T getRows() {
		return rows;
	}

	public void setRows(T rows) {
		this.rows = rows;
	}
	
}
