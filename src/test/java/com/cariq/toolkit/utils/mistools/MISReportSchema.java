package com.cariq.toolkit.utils.mistools;

import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.GenericJsonSchemable;

public class MISReportSchema implements GenericJsonSchemable{
	List<MISTableSchema> tableSchemas = new ArrayList<MISTableSchema>();
	
	public MISReportSchema(String string) {
		// TODO Auto-generated constructor stub
	}

	@Override
	public GenericJSON getSchema() {
		List<GenericJSON> tablesJSONList = new ArrayList<GenericJSON>();
		
		for (MISTableSchema misTableSchema : tableSchemas) {
			tablesJSONList.add(misTableSchema.getSchema());
		}

		GenericJSON tablesJSON = GenericJSON.build("tables", tablesJSONList);
		return GenericJSON.build("report", tablesJSON);
	}

	public void add(MISTableSchema misTableSchema) {
		tableSchemas.add(misTableSchema);
	}
}
