package com.cariq.toolkit.coreiq.exporter;

import java.util.Date;

import com.cariq.toolkit.model.User;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

public class ExporterRequest {

	String exporterName, queryType, outputFileName, fileFormat;
	GenericJSON inputJSON;
	Date createdOn;
	Long userId;
	String reportId; // Client generated Identifier of the report

	/**
	 * 
	 */
	public ExporterRequest() {
		super();
	}

	/**
	 * @param exporterName
	 * @param queryType
	 * @param outputFileName
	 * @param inputJSON
	 * @param createdOn
	 * @param itsUser
	 * @param reportId
	 */
	public ExporterRequest(String exporterName, String queryType, String outputFileName, String fileFormat,
			GenericJSON inputJSON, User user, String reportId) {
		this.exporterName = exporterName;
		this.queryType = queryType;
		this.outputFileName = outputFileName;
		this.fileFormat = fileFormat;
		this.inputJSON = inputJSON;
		this.createdOn = new Date();
		this.userId = (user == null) ? null : user.getId();
		this.reportId = reportId;

		String username = (user == null) ? null : user.getUsername();
		this.inputJSON.put(CarIQExporterWrapper.KEY_DOMAIN_NAME, (user == null) ? "Unknown" : user.getCurrentDomainName(Utils.getValue(inputJSON, "domain")));
		this.inputJSON.put(CarIQExporterWrapper.USERNAME, (user == null) ? "Unknown" : username);
	}

	public String getExporterName() {
		return exporterName;
	}

	public String getQueryType() {
		return queryType;
	}

	public String getOutputFileName() {
		return outputFileName;
	}

	public GenericJSON getInputJSON() {
		return inputJSON;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public Long getUserId() {
		return userId;
	}

	public String getReportId() {
		return reportId;
	}

	public void setExporterName(String exporterName) {
		this.exporterName = exporterName;
	}

	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}

	public void setOutputFileName(String outputFileName) {
		this.outputFileName = outputFileName;
	}

	public void setInputJSON(GenericJSON inputJSON) {
		this.inputJSON = inputJSON;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public String getFileFormat() {
		return fileFormat;
	}

	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}

	@Override
	public String toString() {
		return "ExporterRequest [exporterName=" + exporterName + ", queryType=" + queryType + ", outputFileName="
				+ outputFileName + ", fileFormat=" + fileFormat + ", userId=" + userId + ", reportId=" + reportId + "]";
	}
}
