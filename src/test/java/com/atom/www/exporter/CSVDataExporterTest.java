/**
 * 
 */
package com.atom.www.exporter;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.cariq.toolkit.utils.GenericJSON;

/**
 * The Class CSVDataExporterTest.
 *
 * @author santosh
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Configurable
public class CSVDataExporterTest {

	@Test
	public void getDataFromFileUrl() {

		String queryType = CSVDataExporter.FROM_FILE_URL;
		String urlPath = "https://cindrivenstorageaccount.blob.core.windows.net/cariqcarlogstore/exporter/CarDailyDataExporter-210527032517517.csv.zip";

		GenericJSON input = GenericJSON.build(CSVDataExporter.FILE_URL, urlPath);

		CSVDataExporter exporter = new CSVDataExporter();
		List<GenericJSON> outputList = exporter.doExport(queryType, input, 1, 500);
		System.out.println("=========================================");
		for (GenericJSON output : outputList)
			System.out.println(output);

	}

}
