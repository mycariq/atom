package com.atom.www.exporter;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.coreiq.exporter.CarIQExporter;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterRegistry;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Transactional
@Configurable
public class AppointmentExporterTest {
	
	@Before
	public void init() {
		Utils.setupSecurityContext("preeti", "875d48192ccbbb18f32a13cec13cac14", "a1");
	}
	
	@Test
	public void testGetSignatureMethod() {
		AppointmentExporter exporter = Utils.downCast(AppointmentExporter.class,
				CarIQExporterRegistry.getExporter(AppointmentExporter.TheExporter));

		GenericJSON json = new GenericJSON();
		json.put("owner", "preeti");
		json.put("domain", "IL-Assist");
		
		String signatures = exporter.getSignatures(StagingAndPolicyHelper.QueryType_ByPolicyNumber, json, 1, 20);
		System.out.println("signatures" + signatures);
		Assert.assertNotNull(signatures);
	}
	
	@Test
	public void testExporter() {
		CarIQExporter exporter = CarIQExporterRegistry.getExporter(AppointmentExporter.TheExporter);
		
		GenericJSON json = new GenericJSON();
		json.put("owner", "preeti");
		json.put("domain", "IL-Assist");
		
		List<GenericJSON> outputJson = exporter.export(StagingAndPolicyHelper.QueryType_ByPolicyNumber, json, 1, 20);
		System.out.println(outputJson);
		Assert.assertTrue(!outputJson.isEmpty());
	}
}
