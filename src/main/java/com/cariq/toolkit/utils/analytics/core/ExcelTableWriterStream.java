package com.cariq.toolkit.utils.analytics.core;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;

import com.cariq.toolkit.utils.Utils;

/**
 * The Class ExcelTableWriterStream.
 */
public class ExcelTableWriterStream extends TableWriterStream {

	/** The records. */
	private List<Record> records = new ArrayList<Record>();

	/** The Constant BATCH_SIZE. */
	static final int BATCH_SIZE = 1000;

	/** The sheet name. */
	private String sheetName;

	/**
	 * Instantiates a new excel table writer stream.
	 *
	 * @param excelFile
	 *            the excel file
	 * @param sheetName
	 *            the sheet name
	 */
	public ExcelTableWriterStream(String excelFile, String sheetName) {
		super(excelFile);
		createSheetName(sheetName);
	}

	/**
	 * Creates the sheet name.
	 *
	 * @param sheetName
	 *            the sheet name
	 */
	private void createSheetName(String sheetName) {
		sheetName = sheetName.replaceAll("Exporter", "");
		sheetName = sheetName.replaceAll("exporter", "");
		if (sheetName.length() > 30)
			sheetName = sheetName.substring(0, 29);
		this.sheetName = sheetName;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.AutoCloseable#close()
	 */
	@Override
	public void close() throws Exception {
		flush();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cariq.www.utils.analytics.core.TableWriterStream#write(com.cariq.www.
	 * utils.analytics.core.Record)
	 */
	@Override
	public void write(Record record) throws IOException {
		records.add(record);
		if (records.size() >= BATCH_SIZE)
			flush();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.analytics.core.TableWriterStream#flush()
	 */
	@Override
	public void flush() throws IOException {

		//create workbook, sheet, style of first cell
		try (HSSFWorkbook workbook = createWorkbook(); FileOutputStream fileOut = new FileOutputStream(getFile())) {

			HSSFSheet sheet = workbook.getSheet(sheetName);
			HSSFCellStyle firstCellStyle = getFirstCellStyle(workbook);
			HSSFCellStyle baseCellStyle = getBaseCellStyle(workbook);

			//Iterate over the all records and insert into sheet
			for (Record record : records) {
				insertRow(sheet, record, firstCellStyle, baseCellStyle);
			}

			//write workbook into File System
			workbook.write(fileOut);
			records.clear();
		}
	}

	/**
	 * Insert row.
	 *
	 * @param sheet
	 *            the sheet
	 * @param record
	 *            the record
	 * @param firstCellStyle
	 *            the first cell style
	 */
	private void insertRow(HSSFSheet sheet, Record record, HSSFCellStyle firstCellStyle, HSSFCellStyle baseCellStyle) {
		//create row after last row
		HSSFRow row = sheet.createRow(sheet.getLastRowNum() + 1);

		//create cells from record values
		List<String> values = record.getValues();
		for (int index = 0; index < record.getValues().size(); index++) {
			HSSFCell cell = row.createCell(index);
			setCellValue(cell, values.get(index));
			//set style to only first cell
			if (index == 0)
				cell.setCellStyle(firstCellStyle);
			else
				cell.setCellStyle(baseCellStyle);
		}
	}

	private void setCellValue(HSSFCell cell, String value) {
		Date dateValue = Utils.parse(Date.class, value);
		if (dateValue != null) {
			cell.setCellValue(dateValue);
			return;
		}

		Double doubleValue = Utils.parse(Double.class, value);
		if (doubleValue != null) {
			cell.setCellValue(Utils.formatDouble(doubleValue, 2));
			return;
		}

		cell.setCellValue(value);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.analytics.core.TableWriterStream#writeHeader()
	 */
	@Override
	public void writeHeader() throws IOException {

		//create workbook and sheet
		try (HSSFWorkbook workbook = createWorkbook(); FileOutputStream fileOut = new FileOutputStream(getFile())) {
			HSSFSheet sheet = workbook.createSheet(sheetName);

			//insert header row into sheet
			insertHeader(sheet, getHeaderStyle(workbook));

			//write workbook into File System
			workbook.write(fileOut);
		}
	}

	/**
	 * Insert header.
	 *
	 * @param sheet
	 *            the sheet
	 * @param style
	 *            the style
	 */
	private void insertHeader(HSSFSheet sheet, HSSFCellStyle style) {
		HSSFRow headerRow = sheet.createRow((short) 0);
		headerRow.setHeight((short) 400);
		for (Column column : getColumns()) {
			HSSFCell cell = headerRow.createCell(
					(headerRow.getLastCellNum() < 0) ? headerRow.getLastCellNum() + 1 : headerRow.getLastCellNum());
			cell.setCellValue(column.getName());
			cell.setCellStyle(style);
		}
	}

	/**
	 * Creates the workbook.
	 *
	 * @return the HSSF workbook
	 * @throws FileNotFoundException
	 *             the file not found exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	private HSSFWorkbook createWorkbook() throws FileNotFoundException, IOException {
		if (getFile().exists())
			return new HSSFWorkbook(new FileInputStream(getFile()));
		return new HSSFWorkbook();
	}

	/**
	 * Gets the header style.
	 *
	 * @param workbook
	 *            the workbook
	 * @return the header style
	 */
	private HSSFCellStyle getHeaderStyle(HSSFWorkbook workbook) {
		HSSFCellStyle style = getFirstCellStyle(workbook);
		style.getFont(workbook).setFontHeightInPoints((short) 13);
		return style;
	}

	/**
	 * Gets the first cell style.
	 *
	 * @param workbook
	 *            the workbook
	 * @return the first cell style
	 */
	private HSSFCellStyle getFirstCellStyle(HSSFWorkbook workbook) {
		HSSFCellStyle style = getBaseCellStyle(workbook);
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		HSSFPalette pallete = workbook.getCustomPalette();
		pallete.setColorAtIndex(HSSFColor.BLUE.index, (byte) 11, (byte) 83, (byte) 148);
		style.setFillForegroundColor(pallete.getColor(HSSFColor.BLUE.index).getIndex());

		//set font
		HSSFFont font = style.getFont(workbook);
		font.setColor(IndexedColors.WHITE.getIndex());
		font.setBold(true);
		return style;
	}

	private HSSFCellStyle getBaseCellStyle(HSSFWorkbook workbook) {
		HSSFCellStyle style = workbook.createCellStyle();
		style.setAlignment(HorizontalAlignment.CENTER);
		//set font
		HSSFFont font = workbook.createFont();
		font.setFontName("Arial");
		font.setFontHeightInPoints((short) 11);
		style.setFont(font);
		return style;
	}
}
