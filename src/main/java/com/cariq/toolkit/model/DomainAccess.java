package com.cariq.toolkit.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.Domain.Domain_Out;
import com.cariq.toolkit.model.DomainAccess.DomainAccess_In;
import com.cariq.toolkit.model.DomainAccess.DomainAccess_Out;
import com.cariq.toolkit.model.User.User_Out;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;

/**
 * This class implements "domain-user association" in the producer context. Users are provided access to specific domains.
 * @author amita
 *
 */

@Entity
@Configurable
public class DomainAccess extends PersistentObject<DomainAccess> implements JSONable<DomainAccess_In, DomainAccess_Out, IdJSON>{

	// SECTION 1 -Code for Persistence
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    
    @Version
    Integer version;
    
    @Override
	public Long getId() {
		return id;
	}
    @Override
	public void setId(Long id) {
		this.id = id;
	}
    @Override
	public Integer getVersion() {
		return version;
	}
    @Override
	public void setVersion(Integer version) {
		this.version = version;
	}
	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new DomainAccess().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null) this.entityManager = entityManager();
		return entityManager;
	}
	
    /**
	 * @param id
	 * @return
	 */
	protected static DomainAccess find(Long id) {
		return entityManager().find(DomainAccess.class, id);
	}
	
	/* (non-Javadoc)
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public DomainAccess findById(Long id) {
		return find(id);
	}

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/**
	 * Model properties
	 */
	@NotNull
	@ManyToOne
	Domain domain;
	
	@NotNull
	@ManyToOne
	User user;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	// Default constructor is a must!
	public DomainAccess() {
		super();
	}
	
	/**
	 * Parameterized constructor
	 * @param domain
	 * @param user
	 * @param createdOn
	 * @param modifiedOn
	 */
	public DomainAccess(Domain domain, User user, Date createdOn, Date modifiedOn) {
		super();
		this.domain = domain;
		this.user = user;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
	}

	/**
	 * Getters Setters
	 */
	public Domain getDomain() {
		return domain;
	}
	public void setDomain(Domain domain) {
		this.domain = domain;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Date getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}


	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------

	// Implementation of toString and hashCode and equals are advisable for all Model classes
	@Override
	public String toString() {
		return "DomainAccess [id=" + id + ", version=" + version + ", domainId=" + domain.getId() + ", userId=" + user.getId()
				+ ", createdOn=" + createdOn + ", modifiedOn=" + modifiedOn + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DomainAccess other = (DomainAccess) obj;
		if (domain == null) {
			if (other.domain != null)
				return false;
		} else if (!domain.getId().equals(other.domain.getId()))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.getId().equals(other.user.getId()))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		return true;
	}
	

	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API interaction
	// In Class (for construction)
	public static class DomainAccess_In {
		 Long domainId;
		 Long userId;
		 

		/**
		 * @param domain
		 * @param user
		 * @param createdOn
		 * @param modifiedOn
		 */
		public DomainAccess_In(Long domainId, Long userId) {
			this.domainId = domainId;
			this.userId = userId;
		}
		
		public Long getDomainId() {
			return domainId;
		}

		public void setDomain(Long domainId) {
			this.domainId = domainId;
		}

		public Long getUserId() {
			return userId;
		}

		public void setUser(Long userId) {
			this.userId = userId;
		}	
	}
	
	// Out class for output
	public static class DomainAccess_Out extends DomainAccess_In {
		Long id;
		Domain_Out domainJSON;
		User_Out userJSON;
		String createdOn;
		String modifiedOn;
		

		/**
		/**
		 * @param domainId
		 * @param userId
		 * @param createdOn
		 * @param modifiedOn
		 */
		public DomainAccess_Out(Long domainId, Long userId, String createdOn, String modifiedOn, Long id, 
					Domain_Out domainJSON, User_Out userJSON) {
			super(domainId, userId);
			this.id = id;
			this.domainJSON = domainJSON;
			this.userJSON = userJSON;
			this.modifiedOn = modifiedOn;
			this.createdOn = createdOn;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}
		
		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.utils.json.JSONable#toJSON()
	 */
	@Override
	public DomainAccess_Out toJSON() {
		return new DomainAccess_Out(domain.getId(), user.getId(), createdOn.toString(), modifiedOn.toString(), id, domain.toJSON(), user.toJSON());
	}
	
	/* (non-Javadoc)
	 * @see com.cariq.www.utils.json.JSONable#fromJSON(java.lang.Object)
	 */
	@Override
	public void fromJSON(DomainAccess_In json) {
		this.domain = json.getDomainId() == null ? null : Domain.find(json.getDomainId());
		this.user = json.getUserId() == null ? null : User.find(json.getUserId());         	
	}

	/* (non-Javadoc)
	 * @see com.cariq.www.utils.json.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}
		
	// ---------------------------------------------
	// SECTION 5 - Query and Class specific finders and static utility methods
	// ---------------------------------------------
	public static DomainAccess findObjectById(Long id) {
		return find(id);
	}
	
	public static List<DomainAccess> getAllObjects(int pageNo, int pageSize) {
		CarIQSimpleQuery<DomainAccess> qry = new CarIQSimpleQuery<DomainAccess>("getAllObjects", entityManager(), DomainAccess.class);
		qry.setPaging(pageNo, pageSize);
		
		return qry.getResultList();
	}
	
	/**
	 * @param domain
	 * @return
	 */
	public static List<DomainAccess> getAllObjectsByDomain(Domain domain) {
		CarIQSimpleQuery<DomainAccess> qry = new CarIQSimpleQuery<DomainAccess>("getAllObjectsByDomain", entityManager(), DomainAccess.class);
		qry.addCondition("domain", "=", domain);
		return qry.getResultList();
	}
	
	/**
	 * @param user
	 * @return
	 */
	public static List<DomainAccess> getAllObjectsByUser(User user) {
		CarIQSimpleQuery<DomainAccess> qry = new CarIQSimpleQuery<DomainAccess>("getAllObjectsByUser", entityManager(), DomainAccess.class);
		qry.addCondition("user", "=", user);
		
		return qry.getResultList();
	}
	
	public static DomainAccess getObjectByDomainUSer(Domain domain, User user) {
		CarIQSimpleQuery<DomainAccess> qry = new CarIQSimpleQuery<DomainAccess>("getObjectByDomainUSer", entityManager(), DomainAccess.class);
		qry.addCondition("domain", "=", domain);
		qry.addCondition("user", "=", user);
		
		return qry.getSingleResult();
	}
	
}

