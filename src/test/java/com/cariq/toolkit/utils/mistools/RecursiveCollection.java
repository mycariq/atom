package com.cariq.toolkit.utils.mistools;

import java.util.List;

/**
 * Class for easy recursion over groups of items arranged hierarchically
 * Useful for Pivot Table functionality
 * @author hrishi
 *
 * @param <GROUP>
 * @param <SECTION>
 */
public class RecursiveCollection<GROUP, SECTION> {
	GROUP thisGroup;
	List<SECTION> sectionList;
	
	RecursiveCollection<GROUP, SECTION> next;
	
	public RecursiveCollection(GROUP thisGroup, List<SECTION> sectionList) {
		this(thisGroup, sectionList, null);
	}
	
	public RecursiveCollection(GROUP thisGroup, List<SECTION> sectionList, RecursiveCollection<GROUP, SECTION> next) {
		super();
		this.thisGroup = thisGroup;
		this.sectionList = sectionList;
		this.next = next;
	}

	GROUP getCurrent() {
		return thisGroup;
	}
	
	RecursiveCollection<GROUP, SECTION> getNext() {
		return next;
	}
	
	List<SECTION> getItems() {
		return sectionList;
	}
	
	RecursiveCollection<GROUP, SECTION> setNext(RecursiveCollection<GROUP, SECTION> next) {
		this.next = next;
		
		return this;
	}
	
	RecursiveCollection<GROUP, SECTION> setPrevious(RecursiveCollection<GROUP, SECTION> prev) {
		if (prev != null)
			prev.setNext(this);
		
		return this;
	}
}
