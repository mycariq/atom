package com.atom.www.helper;

public enum AppointmentStatus {
	
	/**
	 * New appointment,
	 */
	CREATED,
	
	/**
	 * Appointment was created but it was again scheduled
	 */
	RESCHEDULED,
	
	/**
	 * Appointment has been completed
	 */
	COMPLETE,
	
	/**
	 * Appointment hass been delayed
	 */
	DELAYED,
	
	
	/**
	 * Appointment has been outdated.
	 */
	OUTDATED
}
