package com.cariq.toolkit.coreiq.tableUtil.interfaces;

public interface NameIndexIdentifiable {
	String getName();
	Integer	getIndex();
}
