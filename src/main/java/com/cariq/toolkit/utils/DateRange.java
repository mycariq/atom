package com.cariq.toolkit.utils;

import java.util.Date;

/**
 * The Class DateRange.
 */
public class DateRange {

	/**
	 * The Enum RangeScope.
	 */
	public enum RangeScope {

		/** The before. */
		BEFORE,
		/** The within. */
		WITHIN,
		/** The after. */
		AFTER
	}

	/** The lower. */
	private Date lower;

	/** The upper. */
	private Date upper;

	/**
	 * Instantiates a new date range.
	 *
	 * @param lower
	 *            the lower
	 * @param upper
	 *            the upper
	 */
	public DateRange(Date lower, Date upper) {
		super();
		this.lower = lower;
		this.upper = upper;
	}

	/**
	 * Gets the lower.
	 *
	 * @return the lower
	 */
	public Date getLower() {
		return lower;
	}

	/**
	 * Gets the upper.
	 *
	 * @return the upper
	 */
	public Date getUpper() {
		return upper;
	}

	/**
	 * Checks if is in range.
	 *
	 * @param date
	 *            the date
	 * @return the range scope
	 */
	public RangeScope isInRange(Date date) {
		if (date.before(lower))
			return RangeScope.BEFORE;

		if (date.after(upper))
			return RangeScope.AFTER;

		return RangeScope.WITHIN;
	}

	/**
	 * Shift by days.
	 *
	 * @param days
	 *            the days
	 */
	public void shiftByDays(int days) {
		if (days < 0) {
			lower = Utils.minusDays(this.lower, days);
			upper = Utils.minusDays(this.upper, days);
		} else {
			lower = Utils.addDays(this.lower, days);
			upper = Utils.addDays(this.upper, days);
		}
	}

	/**
	 * Shift by weeks.
	 *
	 * @param weeks
	 *            the weeks
	 */
	public void shiftByWeeks(int weeks) {
		if (weeks < 0) {
			lower = Utils.minusWeeks(this.lower, weeks);
			upper = Utils.minusWeeks(this.upper, weeks);
		} else {
			lower = Utils.addWeeks(this.lower, weeks);
			upper = Utils.addWeeks(this.upper, weeks);
		}
	}

}
