package com.cariq.toolkit.utils.analytics.application;

import java.io.IOException;

import com.cariq.toolkit.model.BillingOperation;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.analytics.core.AnalyticsContext;
import com.cariq.toolkit.utils.analytics.core.Column;
import com.cariq.toolkit.utils.analytics.core.Record;
import com.cariq.toolkit.utils.analytics.core.RecordProcessorTemplate;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.google.common.base.Strings;

public class BillingRecordProcessor extends RecordProcessorTemplate {
	public static final String BILLING_OPERATION_ID = "BILLING_OPERATION_ID";
	
	private int dummyCount = 0;
	
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("BillingRecordProcessor");
	private BillingOperationProcessor billingOperationProcessor = null;
	

	public BillingRecordProcessor(AnalyticsContext context) {
		super(context);
	}
	
	/**
	 * Keep only the relevant columns - don't get rest
	 */
	@Override
	public void init() throws IOException {
		super.init();
	
		// Initialize billing operation to process
		String billingOperationId = getProcessorContext().get(BILLING_OPERATION_ID).toString();
		if (Strings.isNullOrEmpty(billingOperationId)) {
			Utils.handleException("BillingRecordProcessor.init(): Failed to get " + BILLING_OPERATION_ID);
		}

		BillingOperation billingOperation = BillingOperation.findObjectById(Long.parseLong(billingOperationId));
		if (null == billingOperation)
			Utils.handleException("Invalid billingOperation ID to process: " + billingOperationId);
		
		// Create billing operation processor
		billingOperationProcessor = new BillingOperationProcessor(billingOperation);
		billingOperationProcessor.prepare();
		logger.debug("===> billingOperationProcessor.stage: " + billingOperationProcessor.getStage());
	}
	
	@Override
	public void close() {
		logger.debug("===> Skipped Dummy policies from TAT: " + dummyCount);
		logger.debug("===> billingOperationProcessor.stage: " + billingOperationProcessor.getStage());
		
		// Call BillingOperation finish
		billingOperationProcessor.finish();
	}

	private Record replaceNullValue(Record record, Column col) {
		String value = record.getStringValue(col);
		if (Strings.isNullOrEmpty(value) || value.toLowerCase().contains("not available")) {
			record.set(col, "NA");
		}
		return(record);	
	}

	private Record updateRecord(Record record) {
		record = replaceNullValue(record, new Column(BillingTermProcessor.TAT_COL_CITY_TIER));
		record = replaceNullValue(record, new Column(BillingTermProcessor.TAT_COL_CAR_CONNECTION_MONTH));
		record = replaceNullValue(record, new Column(BillingTermProcessor.TAT_COL_DEVICE_SHIPPING_MONTH));
		record = replaceNullValue(record, new Column(BillingTermProcessor.TAT_COL_CARIQ_ENTRY_MONTH));
		record = replaceNullValue(record, new Column(BillingTermProcessor.TAT_COL_CAR_ID));
		return record;
	}
	
	/**
	 * Copy the contents of the record for relevant columns in the output. Later,
	 * investigate specific values to match the required conditions.
	 */
	@Override
	protected Record doProcess(Record record) {
		try (ProfilePoint _BillingRecordProcessor_doProcess = ProfilePoint.profileAction("ProfAction_BillingRecordProcessor_doProcess")) {

			GenericJSON recordJSON = new GenericJSON();
			
			// Skip dummy policies
			String policyNumber = record.getStringValue(new Column(BillingTermProcessor.TAT_COL_POLICY_NUMBER));
			if (!Strings.isNullOrEmpty(policyNumber) && policyNumber.toLowerCase().contains("dummy")) {
				dummyCount++;
				return null;
			}
			
			// Update the record
			record = updateRecord(record);
			recordJSON.putAll(record.jsonize());
			
			// Process BillingOperation
			billingOperationProcessor.process(recordJSON);
			
			return record;
		}
	}
}
