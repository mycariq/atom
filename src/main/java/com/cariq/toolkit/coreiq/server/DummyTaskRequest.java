package com.cariq.toolkit.coreiq.server;

import com.cariq.toolkit.coreiq.server.Server;

public class DummyTaskRequest implements ServerSpecificRequest {

	private String taskType = "API"; // one of API or WorkerType
	private long executionTimeSec = 5; // Execution Time in seconds
	private String serverName = Server.ANY;

	public DummyTaskRequest() {}

	public DummyTaskRequest(String taskType, long executionTimeSec) {
		this(taskType, executionTimeSec, Server.ANY);
	}
	
	public DummyTaskRequest(String taskType, long executionTimeSec, String serverName) {
		super();
		this.taskType = taskType;
		this.executionTimeSec = executionTimeSec;
		this.serverName = serverName;
	}

	public String getTaskType() {
		return taskType;
	}

	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	public long getExecutionTimeSec() {
		return executionTimeSec;
	}

	public void setExecutionTimeSec(long executionTimeSec) {
		this.executionTimeSec = executionTimeSec;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	@Override
	public String getServerName() {
		// TODO Auto-generated method stub
		return serverName;
	}
}
