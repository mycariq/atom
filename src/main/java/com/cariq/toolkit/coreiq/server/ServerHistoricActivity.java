package com.cariq.toolkit.coreiq.server;

public class ServerHistoricActivity {
	String category;
	long totalTimeMS;
	int totalCount;	
	
	public ServerHistoricActivity() {}
	
	public ServerHistoricActivity(String category) {
		super();
		this.category = category;
	}
	
	public ServerHistoricActivity addActivity(ServerActivity activity) {
		if (!category.equalsIgnoreCase(activity.getCategory()))
			return this;
		
		totalCount++;
		totalTimeMS += activity.calculateElapsedTime();
		return this;
	}


	public String getCategory() {
		return category;
	}

	public long getTotalTimeMS() {
		return totalTimeMS;
	}

	public int getTotalCount() {
		return totalCount;
	}
}

