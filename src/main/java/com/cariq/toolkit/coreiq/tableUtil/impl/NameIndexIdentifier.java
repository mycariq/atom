package com.cariq.toolkit.coreiq.tableUtil.impl;

import com.cariq.toolkit.coreiq.tableUtil.interfaces.NameIndexIdentifiable;

/**
 * Simple name and index pair
 * Used for locating the artifact (Report/Table/...)
 * @author hrishi
 *
 */
public class NameIndexIdentifier implements NameIndexIdentifiable {
	String name;
	Integer index;
	public NameIndexIdentifier(String name, Integer index) {
		super();
		this.name = name;
		this.index = index;
	}
	public String getName() {
		return name;
	}
	public Integer getIndex() {
		return index;
	}
	
	
}
