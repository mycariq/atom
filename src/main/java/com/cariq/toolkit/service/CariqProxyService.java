package com.cariq.toolkit.service;

import com.cariq.toolkit.utils.GenericJSON;

/**
 * The Interface CariqProxyService.
 */
public interface CariqProxyService {

	/**
	 * Gets the.
	 *
	 * @param apiName the api name
	 * @return the object
	 */
	public Object get(String apiName);

	/**
	 * Post.
	 *
	 * @param json the json
	 * @param apiName the api name
	 * @return the object
	 */
	public Object post(GenericJSON json, String apiName);

	/**
	 * Put.
	 *
	 * @param json the json
	 * @param apiName the api name
	 * @return the object
	 */
	public Object put(GenericJSON json, String apiName);

	/**
	 * Delete.
	 *
	 * @param apiName the api name
	 * @return the object
	 */
	public Object delete(String apiName);
}