package com.cariq.toolkit.utils.mistools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Section Group is like 
 * @author hrishi
 *
 */
public class MISSectionGroup {
	String name;
	String label;
	String key;
	List<MISSectionDefinition> itsSections = new ArrayList<MISSectionDefinition>();

	static Map<String, MISSectionGroup> sectionGroups = new HashMap<String, MISSectionGroup>();
	
	public static MISSectionGroup create(String name) {
		return create(name, name, name);
	}

	public static MISSectionGroup create(String name, String label, String key) {
		MISSectionGroup grp = new MISSectionGroup(name, label, key);
		sectionGroups.put(label, grp);
		
		return grp;
	}
	
	public MISSectionGroup(String name, String label, String key) {
		super();
		this.name = name;
		this.label = label;
		this.key = key;
	}
	
	public MISSectionGroup addSectionDefinition(MISSectionDefinition sectionDef) {
		itsSections.add(sectionDef);
		return this;
	}
	
	public String getName() {
		return name;
	}

	public String getLabel() {
		return label;
	}

	public String getKey() {
		return key;
	}

	public List<MISSectionDefinition> getItsSections() {
		return itsSections;
	}

	public static Map<String, MISSectionGroup> getSectionGroups() {
		return sectionGroups;
	}

	public static MISSectionGroup lookupGroup(String sectionGroupName) {
		return sectionGroups.get(sectionGroupName);
	}
	
	public static MISSectionDefinition lookupDefinition(String definitionName) {
		String[] groupDef = definitionName.split("\\.");
		if (groupDef.length == 2) {
			MISSectionGroup group = lookupGroup(groupDef[0]);
			if (group != null) {
				for (MISSectionDefinition sectionDef : group.itsSections) {
					if (sectionDef.label.equals(groupDef[1]))
						return sectionDef;
				}
			}
		}
		
		return null;
	}

	public static MISSectionGroup create(String name, String[] sections) {
		MISSectionGroup grp = create(name);
		for (String section : sections) {
			grp.addSectionDefinition(new MISSectionDefinition(grp, section));
		}

		return grp;
	}
}
