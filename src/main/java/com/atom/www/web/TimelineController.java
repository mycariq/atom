package com.atom.www.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cariq.toolkit.model.Timeline.Timeline_In;
import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.service.TimelineService;
import com.cariq.toolkit.utils.CarIQMutableAPI;
import com.cariq.toolkit.utils.ResponseWithIdJson;

/**
 * The Class TimelineController.
 */
@CarIQPublicAPI(name = "Timeline", description = "Everything related to Timline")
@Controller
@RequestMapping("/timeline")
public class TimelineController {

	/** The timelineService. */
	@Autowired
	private TimelineService timelineService;
	
	/**
	 * create new timeline item
	 *
	 * @param params
	 *            the params
	 * @return the response with id json
	 */
	@CarIQPublicAPIMethod(description = "Create new timeline", responseClass = ResponseWithIdJson.class, internal = true)
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public ResponseWithIdJson createTimeline(@RequestBody Timeline_In timelineIn) {
		try (CarIQMutableAPI _createTimeline = new CarIQMutableAPI("CARIQ_addTimeline", timelineIn)) {
			return timelineService.addTimelineItem(timelineIn);
		}
	}
	
	
	/**
	 * Delete timeline 
	 * 
	 * @param timelineIn
	 * @return
	 */
	@CarIQPublicAPIMethod(description = "Remove timeline", responseClass = ResponseWithIdJson.class, internal = true)
	@RequestMapping(value = "/remove/{id}", method = RequestMethod.DELETE)
	@ResponseBody	
	public ResponseWithIdJson deleteTimeline(@PathVariable("id") Long id) {
		try (CarIQMutableAPI _deleteTimeline = new CarIQMutableAPI("CARIQ_deleteTimeline", id)) {
			return timelineService.deleteTimeline(id);
		}
	}
}
