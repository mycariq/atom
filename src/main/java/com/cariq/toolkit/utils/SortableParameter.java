/**
 * 
 */
package com.cariq.toolkit.utils;

/**
 * @author hrishi
 *
 */
public class SortableParameter {
	String name;
	Class<? extends Comparable> clazz;
	
	/**
	 * @param name
	 * @param clazz
	 */
	public SortableParameter(String name, Class<? extends Comparable> clazz) {
		this.name = name;
		this.clazz = clazz;
	}

	public String getName() {
		return name;
	}

	public Class<? extends Comparable> getClazz() {
		return clazz;
	}
}
