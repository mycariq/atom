package com.cariq.toolkit.utils.wwwservice.core;

import com.cariq.toolkit.utils.wwwservice.core.GeoTile.GeoTileJson;
import com.cariq.toolkit.utils.wwwservice.core.Identity.IdentityJson;
import com.cariq.toolkit.utils.wwwservice.core.TimeStamp.TimeStampJson;

/**
 * Tuple represents the moment in Time and Space for an entity.
 * Consists of Identity of the entity,
 * TimeStamp of the moment
 * Location (GeoTile) of the moment
 * @author HVN
 *
 */
public class Tuple {
	Identity id;
	TimeStamp time;
	GeoTile location;
	
	/**
	 * Basic constructor
	 * @param id
	 * @param time
	 * @param location
	 */
	public Tuple(Identity id, TimeStamp time, GeoTile location) {
		super();
		this.id = id;
		this.time = time;
		this.location = location;
	}
	
	/**
	 * Simplified Constructor
	 * @param id
	 * @param time
	 * @param tile
	 */
	public Tuple(long id, long time, long tile) {
		this(new Identity(id), new TimeStamp(time), new GeoTile(tile));
	}

	
	/**
	 * @return the id
	 */
	public Identity getId() {
		return id;
	}

	/**
	 * @return the time
	 */
	public TimeStamp getTime() {
		return time;
	}

	/**
	 * @return the location
	 */
	public GeoTile getLocation() {
		return location;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Tuple))
			return false;
		Tuple other = (Tuple) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Tuple = {id=" + id.toString() + ", time=" + time.toString() + ", location=" + location.toString()
				+ "}";
	}
	
	public static class TupleJson {
		public IdentityJson getId() {
			return id;
		}
		public void setId(IdentityJson id) {
			this.id = id;
		}
		public TimeStampJson getTime() {
			return time;
		}
		public void setTime(TimeStampJson time) {
			this.time = time;
		}
		public GeoTileJson getLocation() {
			return location;
		}
		public void setLocation(GeoTileJson location) {
			this.location = location;
		}
		IdentityJson id;
		TimeStampJson time;
		GeoTileJson location;
	}

	public TupleJson toJson() {
		TupleJson tupleJson = new TupleJson();
		tupleJson.id = id.toJson();
		tupleJson.time = time.toJson();
		tupleJson.location = location.toJson();
		return tupleJson;
	}

	
}
