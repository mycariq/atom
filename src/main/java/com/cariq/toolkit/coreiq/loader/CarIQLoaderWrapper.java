package com.cariq.toolkit.coreiq.loader;

import com.cariq.toolkit.utils.GenericJSON;


/**
 * Wraps call to loaders of CarIQ
 * 
 * @author sagar
 *
 */
public interface CarIQLoaderWrapper {
	
	//Key to retrive status object from hashmap
	public static final String LOAD_STATUS = "status";
	
	/**
	 * Status of loading task in CarIQ
	 */
	public enum STATUS {
		
		/**
		 * Loading is successfull
		 */
		OK,
		/**
		 * Loading is failed
		 */
		FAILED
	}
	

	/**
	 * Get Definition of loader from CarIQ layer, This definition will be used 
	 * by loader of Atom.
	 * 
	 * @return {@link CarIQLoaderDefinition}
	 */
	public CarIQLoaderDefinition getWrappedDefinition();
	
	
	/**
	 * Load data into CarIQ layer loader. After loading 
	 * 
	 * @param json input json to load into CarIQ
	 * @return {@link CarIQLoaderContext}
	 */
	public CarIQLoaderContext load(GenericJSON json);
}
