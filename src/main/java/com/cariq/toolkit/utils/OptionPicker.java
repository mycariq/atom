package com.cariq.toolkit.utils;

import java.util.Arrays;
import java.util.List;

/**
 * OptionPicker is a simple 2 level soundex kind matcher, where 1 concept/word can be represented with different spellings.
 * e.g. Bangalore/Banglore/Bengaluru - All mean the same.
 * @author Hrishi
 *
 */
public class OptionPicker {
	// Pick from multiple possible options - if none found, return the input
	public static class Option {
		List<String> stringPatterns;

		public Option(String... stringPatterns) {
			super();
			this.stringPatterns = Arrays.asList(stringPatterns);
		}
		
		public String match(String input) {
			for (String string : stringPatterns) {
				if (string.equalsIgnoreCase(input))
					return stringPatterns.get(0);
			}
				
				return null;
		}
	}
	
	List<Option> optionList;
	
	public OptionPicker(Option... options) {
		super();
		this.optionList = Arrays.asList(options);
	}
	
	public String match(String input) {
		for (Option option : optionList) {
			String matchingOption = option.match(input);
				
			if (null != matchingOption)
				return matchingOption;
		}
			
		return input;
	}
	
}
