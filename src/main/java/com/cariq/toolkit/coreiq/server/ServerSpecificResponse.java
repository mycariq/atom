package com.cariq.toolkit.coreiq.server;

import java.util.Date;

public interface ServerSpecificResponse {
	String getServerName();
	Date getServerStartTime();
}
