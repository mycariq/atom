package com.cariq.toolkit.utils.cdn;

import java.io.Closeable;
import java.io.InputStream;

/**
 * Similar to JDBCDriver - abstract interface to do CDN Operations
 * @author hrishi
 *
 */
public interface CDNDriver extends Closeable {
	void initialize(String url, String userName, String password) throws Exception;
	String getUrl();
	String getUserName();
	String getPassword();
	
	void uploadFile(String containerName, String fileName, InputStream inputStream, String contentType, long length) throws Exception;
	void uploadFile(String containerName, String fileName, String localFilePath, String contentType) throws Exception;
	void deleteFile(String containerName, String fileName) throws Exception;
	
	// more to follow
}
