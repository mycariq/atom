/**
 * 
 */
package com.cariq.toolkit.utils.analytics.application;

import java.io.IOException;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

import com.cariq.toolkit.coreiq.tableUtil.application.mis.MISProcessingContext;
import com.cariq.toolkit.coreiq.tableUtil.application.mis.MISReport;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.Week;
import com.cariq.toolkit.utils.analytics.core.AnalyticsContext;
import com.cariq.toolkit.utils.analytics.core.Column;
import com.cariq.toolkit.utils.analytics.core.Record;
import com.cariq.toolkit.utils.analytics.core.RecordProcessorTemplate;
import com.cariq.toolkit.utils.jsonProcessor.JSONObjectBuilder;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.google.common.base.Strings;

/**
 * The Class TimeLineReporter.
 *
 * @author santosh
 */
public class TimeLineReporter extends RecordProcessorTemplate {
	public static final String TIMELINE_REPORT_JSON_PATH = "TimeLineReportJsonPath";
	public static final String TIMELINE_JSON = "TimeLineJSON";

	private static final String POLICY_NUMBER = "signature";
	private static final String timelineCreatedOn = "timelineCreatedOn", reportType = "reportType", Daily = "Daily",
			Weekly = "Weekly", DailyReport = "DailyReport", WeeklyReport = "WeeklyReport", NA = "NA", Stage = "stage",
			Tag = "tag";
	private int dummyCount = 0;

	private MISReport report;

	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("TimeLineReporter");

	public TimeLineReporter(AnalyticsContext context) {
		super(context);
	}

	@Override
	public void init() throws IOException {
		super.init();

		//Init the MISReport table and add to the context
		String jsonPath = getProcessorContext().get(TIMELINE_REPORT_JSON_PATH).toString();
		if (Strings.isNullOrEmpty(jsonPath))
			Utils.handleException("MonthlyMISReporter.init(): Failed to get " + TIMELINE_REPORT_JSON_PATH);

		JSONObjectBuilder builder = new JSONObjectBuilder(jsonPath);

		report = new MISReport();
		logger.debug("===> registering builder json");
		builder.register(report.getJSONLoadable());
		logger.debug("===> processing builder json");
		builder.process();
	}

	@Override
	public void close() {
		GenericJSON outputJSON = report.toGenericJSON();
		getProcessorContext().put(TIMELINE_JSON, Utils.getJSonString(outputJSON));
		logger.debug("===> Skipped Dummy policies from TAT: " + dummyCount);
	}

	@Override
	protected Record doProcess(Record record) throws IOException {
		try (ProfilePoint _doProcess = ProfilePoint.profileAction("TimeLineReporter_doProcess")) {
			// Placeholder - need a better pattern
			MISProcessingContext context = new MISProcessingContext();

			GenericJSON recordJSON = new GenericJSON();

			// Skip dummy policies
			String policyNumber = record.getStringValue(new Column(POLICY_NUMBER));
			if (!Strings.isNullOrEmpty(policyNumber) && policyNumber.toLowerCase().contains("dummy")) {
				dummyCount++;
				return null;
			}

			// Update the record
			record = updateRecord(record);

			// Update the report
			recordJSON.putAll(record.jsonize());
			report.process(context, recordJSON);

			return record;
		}
	}

	private Record updateRecord(Record record) {
		try (ProfilePoint _updateRecord = ProfilePoint.profileAction("TimeLineReporter_updateRecord")) {
			Date now = new Date();

			record.append(DailyReport, isDailyReportInfo(now, record));
			record.append(WeeklyReport, isWeeklyReportInfo(now, record));
			record.append(Stage, getData(record, Stage));
			record.append(Tag, getData(record, Tag));
			return record;
		}
	}

	private String getData(Record record, String columnName) {
		String val = record.getStringValue(new Column(columnName));
		if (Strings.isNullOrEmpty(val))
			return NA;

		return val;
	}

	private String isWeeklyReportInfo(Date now, Record record) {
		try (ProfilePoint _isWeeklyReportInfo = ProfilePoint.profileAction("TimeLineReporter_isWeeklyReportInfo")) {
			String dateValue = record.getStringValue(new Column(timelineCreatedOn));
			if (Strings.isNullOrEmpty(dateValue))
				return NA;

			Date timelineDate = Utils.parseDate(dateValue);
			if (null == timelineDate)
				return NA;

			// get start time and end time of week
			Week week = Week.getLastWeek(now, null);
			if (week.isInTheWeek(timelineDate))
				return Weekly;

			return NA;
		}
	}

	private String isDailyReportInfo(Date now, Record record) {
		try (ProfilePoint _isDailyReportInfo = ProfilePoint.profileAction("TimeLineReporter_isDailyReportInfo")) {
			String dateValue = record.getStringValue(new Column(timelineCreatedOn));
			if (Strings.isNullOrEmpty(dateValue))
				return NA;

			Date timelineDate = Utils.parseDate(dateValue);
			if (null == timelineDate)
				return NA;

			Date yesterdayDate = Utils.getDaysBefore(now, 1);
			if (DateUtils.isSameDay(yesterdayDate, timelineDate))
				return Daily;

			return NA;
		}
	}

}
