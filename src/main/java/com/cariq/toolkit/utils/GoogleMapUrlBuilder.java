package com.cariq.toolkit.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cariq.toolkit.utils.wwwservice.core.GeoTile;

public class GoogleMapUrlBuilder implements ObjectBuilder<GeoTile> {
	public static final String URL_LENGTH = "URL_LENGTH";
	public static final String SIZE = "SIZE";
	public static final String POINT = "POINT";
	public static final String COLOR = "COLOR";
	
	private static String url = "https://maps.googleapis.com/maps/api/staticmap?";
	List<GeoTile> geoTiles = new ArrayList<GeoTile>();
	Map<String, String> settings = new HashMap<String, String>();
	
	String mapUrl = null;
	
	@Override
	public GoogleMapUrlBuilder configure(String setting, Object value) {
		settings.put(setting, value.toString());
		return this;
	}
	@Override
	public GoogleMapUrlBuilder add(String parameter, GeoTile value) {
		if (parameter != POINT)
			return this;
		
		geoTiles.add(value);
		return this;
	}
	
	@Override
	public GoogleMapUrlBuilder build() {		
		int urlLength = getSetting(URL_LENGTH, 2000);
		String color = getSetting(COLOR, "red");
		String size = getSetting(SIZE, "400x800");
		int count = 0;
		
		StringBuilder urlBuilder = new StringBuilder(url).append("size=" + size + "&");
		for (GeoTile geoTile : geoTiles) {
			urlBuilder.append("markers=color:").append(color).append("%7Clabel:" + (++count) + "%7C");	
			urlBuilder.append(geoTile.getLatitude()).append(",").append(geoTile.getLongitude()).append("&");
			
			if (urlBuilder.length() > (urlLength * .95))
				break;
		}
		
		mapUrl = urlBuilder.toString();
		
		return this;
	}
	
	private int getSetting(String setting, int defaultVal) {
		String val = getSetting(setting, String.valueOf(defaultVal));
		
		return Integer.parseInt(val);
	}
	
	private String getSetting(String setting, String defaultValue) {
		String val = settings.get(setting);
		if (null == val)
			return defaultValue;

		return val;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return mapUrl;
	}
	
	@Override
	public GoogleMapUrlBuilder add(GeoTile value) {
		return add(POINT, value);
	}
}
