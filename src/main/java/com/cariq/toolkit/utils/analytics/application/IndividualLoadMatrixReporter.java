/**
 * 
 */
package com.cariq.toolkit.utils.analytics.application;

import java.io.IOException;

import com.cariq.toolkit.coreiq.tableUtil.application.mis.MISProcessingContext;
import com.cariq.toolkit.coreiq.tableUtil.application.mis.MISReport;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.analytics.core.AnalyticsContext;
import com.cariq.toolkit.utils.analytics.core.Column;
import com.cariq.toolkit.utils.analytics.core.Record;
import com.cariq.toolkit.utils.analytics.core.RecordProcessorTemplate;
import com.cariq.toolkit.utils.jsonProcessor.JSONObjectBuilder;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.google.common.base.Strings;

/**
 * The Class IndividualLoadMatrixReporter.
 *
 * @author santosh
 */
public class IndividualLoadMatrixReporter extends RecordProcessorTemplate {

	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("IndividualLoadMatrixReporter");

	public static final String INDIVIDUAL_LOAD_MATRIX_JSON_PATH = "IndividualLoadMatrixJsonPath";
	public static final String INDIVIDUAL_LOAD_MATRIX_JSON = "IndividualLoadMatrixJSON";

	private int dummyCount = 0;

	// Columns constants
	private static final String NA = "NA";
	private static final String CITY_TIER = "City Tier";
	private static final String POLICY_STAGE = "Policy Stage";
	private static final String POLICY_NUMBER = "Policy Number";

	private MISReport report;

	public IndividualLoadMatrixReporter(AnalyticsContext context) {
		super(context);
	}

	/**
	 * Keep only the relevant columns - don't get rest
	 */
	@Override
	public void init() throws IOException {
		super.init();

		//Init the MISReport table and add to the context
		String jsonPath = getProcessorContext().get(INDIVIDUAL_LOAD_MATRIX_JSON_PATH).toString();
		if (Strings.isNullOrEmpty(jsonPath))
			Utils.handleException(
					"IndividualLoadMatrixReporter.init(): Failed to get " + INDIVIDUAL_LOAD_MATRIX_JSON_PATH);

		JSONObjectBuilder builder = new JSONObjectBuilder(jsonPath);

		report = new MISReport();
		logger.debug("===> registering builder json");
		builder.register(report.getJSONLoadable());
		logger.debug("===> processing builder json");
		builder.process();
	}

	@Override
	public void close() {
		GenericJSON outputJSON = report.toGenericJSON();
		getProcessorContext().put(INDIVIDUAL_LOAD_MATRIX_JSON, Utils.getJSonString(outputJSON));
		logger.debug("===> Skipped Dummy policies from TAT: " + dummyCount);
		//logger.debug("===> OutputJSON: " + Utils.getJSonString(outputJSON));
	}

	/**
	 * Copy the contents of the record for relevant columns in the output Later,
	 * investigate specific values (e.g. speed etc.)
	 */
	@Override
	protected Record doProcess(Record record) {
		try (ProfilePoint _doProcess = ProfilePoint.profileAction("IndividualLoadMatrixReporter_doProcess")) {
			// Placeholder - need a better pattern
			MISProcessingContext context = new MISProcessingContext();

			GenericJSON recordJSON = new GenericJSON();

			// Skip dummy policies
			String policyNumber = record.getStringValue(new Column(POLICY_NUMBER));
			if (!Strings.isNullOrEmpty(policyNumber) && policyNumber.toLowerCase().contains("dummy")) {
				dummyCount++;
				return null;
			}

			// Update the record
			record = updateRecord(record);

			// Update the report
			recordJSON.putAll(record.jsonize());
			report.process(context, recordJSON);

			return record;
		}
	}

	private Record updateRecord(Record record) {
		record = replaceNullValue(record, new Column(CITY_TIER));
		record = replaceNullValue(record, new Column(POLICY_STAGE));

		return record;
	}

	private Record replaceNullValue(Record record, Column col) {
		String value = record.getStringValue(col);
		if (Strings.isNullOrEmpty(value))
			record.set(col, NA);

		return record;
	}
}
