package com.cariq.toolkit.serviceimpl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.cariq.toolkit.coreiq.exporter.ExportedData;
import com.cariq.toolkit.coreiq.exporter.ExportedData.ExporterDataJSON_In;
import com.cariq.toolkit.coreiq.exporter.ExportedData.ExporterDataJSON_Out;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.service.ExporterDataService;
import com.cariq.toolkit.serviceimpl.async.WIHelper;
import com.cariq.toolkit.utils.CarIQFileServer;
import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.CarIQZipFile;
import com.cariq.toolkit.utils.ResponseJson;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

@Service
public class ExporterDataServiceImpl implements ExporterDataService {

	/** The Constant ZIP. */
	private static final String ZIP_EXTENSION = ".zip", ZIP = "zip";

	/** The Constant DEFAULT. */
	private static final String QUERY_TYPE_UPLOAD_FILE= "UploadFile";

	/** The file server. */
	@Autowired
	private CarIQFileServer fileServer;

	@Override
	public List<ExporterDataJSON_Out> getExportedDataList(String option, int pageNo, int pageSize) {
		try (ProfilePoint _getExportedDataList = ProfilePoint.profileAction("ProfAction_getExportedDataList")) {
			List<ExportedData> exportedDataList;
			if (option.equalsIgnoreCase(ExportedData.ALL)) {
				exportedDataList = ExportedData.findExportedData(pageNo, pageSize);
			} else {
				User user = User.getLoggedInUser();
				exportedDataList = ExportedData.findExportedData(user, pageNo, pageSize);
			}
			return Utils.convertList(exportedDataList, ExporterDataJSON_Out.class);
		}
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.service.ExporterDataService#uploadFile(org.springframework.web.multipart.MultipartFile, java.lang.String, java.lang.String)
	 */
	@Override
	public ResponseJson uploadFile(MultipartFile file, String description, String domainName) {
		try {
			// transfer multipart file to local storage
			String localFilePath = CarIQFileUtils.getTempFile(file.getOriginalFilename());
			file.transferTo(new File(localFilePath));
			String localZipFilePath;

			// check and create local zip file path
			if (ZIP.equalsIgnoreCase(FilenameUtils.getExtension(file.getOriginalFilename())))
				localZipFilePath = CarIQFileUtils.getTempFile(file.getOriginalFilename());
			else
				localZipFilePath = CarIQFileUtils.getTempFile(file.getOriginalFilename() + ZIP_EXTENSION);

			// Create blobstorage path
			String zipServerFilePath = "exporter/" + localZipFilePath.substring(localZipFilePath.lastIndexOf("-") + 1);

			// create CarIQZipFile
			CarIQZipFile zipFile = new CarIQZipFile(localZipFilePath, zipServerFilePath);
			zipFile.add(file.getOriginalFilename(), localFilePath);
			zipFile = zipFile.build();

			// Upload file on blob storage
			fileServer.upload(zipFile, WIHelper.exporterCDN);

			// file details persist on DB
			persistFileDetails(localFilePath, zipServerFilePath, domainName, description);

		} catch (IOException e) {
			Utils.handleException(e);
		}
		return new ResponseJson(file.getOriginalFilename() + " file uploaded successfully");
	}

	/**
	 * Persist file details.
	 *
	 * @param localFilePath
	 *            the local file path
	 * @param zipServerFilePath
	 *            the zip server file path
	 * @param domainName
	 *            the domain name
	 * @param description
	 *            the description
	 */
	private void persistFileDetails(String localFilePath, String zipServerFilePath, String domainName,
			String description) {
		// file URL
		String fileUrl = WIHelper.getCarLogPath(zipServerFilePath);
		String fileName = FilenameUtils.getBaseName(localFilePath);
		User user = User.getLoggedInUser();

		// get domain by name.
		Domain domain = Domain.getByName(domainName);
		if (null == domain)
			Utils.handleException("Domain does not exists.");

		String inputJson = (description.isEmpty()) ? "{}" : "{description:" + description + "}";
		// persist data in DB.
		ExportedData exportedData = new ExportedData();
		ExporterDataJSON_In json = new ExporterDataJSON_In(fileName, QUERY_TYPE_UPLOAD_FILE, fileName,
				FilenameUtils.getExtension(fileName), inputJson, new Date(), user.getId(), fileName, fileUrl, ExportedData.COMPLETE,
				domain.getId());
		exportedData.fromJSON(json);
		exportedData.persist();
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.service.ExporterDataService#getUploadedFileExporterDataList(java.lang.String, java.lang.String, java.lang.String, int, int)
	 */
	@Override
	public List<ExporterDataJSON_Out> getUploadedFileExporterDataList(String domainName, String startDate, String endDate,
			int pageNo, int pageSize) {
		try (ProfilePoint _getExportedDataList = ProfilePoint.profileAction("ProfAction_getUploadedFileExporterData")) {

			// convert string to date
			Date sDate = Utils.convertToDate(startDate, Utils.YYYY_MM_DD_hh_mm_ss);
			Date eDate = Utils.convertToDate(endDate, Utils.YYYY_MM_DD_hh_mm_ss);
			
			// get domain by name.
			Domain domain = Domain.getByName(domainName);
			if (null == domain)
				Utils.handleException("Domain does not exists.");
			
			List<ExportedData> exportedDataList = ExportedData.findUploadedFileExportedData(domain, QUERY_TYPE_UPLOAD_FILE,
					sDate, eDate, pageNo, pageSize);

			return Utils.convertList(exportedDataList, ExporterDataJSON_Out.class);
		}
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.service.ExporterDataService#getUploadedFilesByKeyWord(java.lang.String, java.lang.String, int, int)
	 */
	@Override
	public List<ExporterDataJSON_Out> getUploadedFilesByKeyWord(String domainName, String keyWord, int pageNo,
			int pageSize) {
		try (ProfilePoint _getExportedDataList = ProfilePoint.profileAction("ProfAction_getUploadedFilesByKeyWord")) {
			// get domain by name.
			Domain domain = Domain.getByName(domainName);
			if (null == domain)
				Utils.handleException("Domain does not exists.");
			
			List<ExportedData> exportedDataList = ExportedData.findUploadedFilesByKeyWord(domain, QUERY_TYPE_UPLOAD_FILE,
					keyWord, pageNo, pageSize);

			return Utils.convertList(exportedDataList, ExporterDataJSON_Out.class);
		}
	}

	/* (non-Javadoc)
	 * @see com.cariq.toolkit.service.ExporterDataService#getExporterDataByDomainAndDate(java.lang.String, java.lang.String, java.lang.String, int, int)
	 */
	@Override
	public List<ExporterDataJSON_Out> getExporterDataByDomainAndDate(String domainName, String startDate, String endDate,
			int pageNo, int pageSize) {
		try (ProfilePoint _getExporterDataByDomainAndDate = ProfilePoint
				.profileAction("ProfAction_ExporterDataServiceImpl_getExporterDataByDomainAndDate")) {
			// get loggedIn user
			User user = User.getLoggedInUser();
			// get domain
			Domain domain = Domain.getByName(domainName);
			Utils.checkNotNull(domain, "Domain not found");
			
			// convert string to date
			Date sDate = Utils.convertToDate(startDate, Utils.YYYY_MM_DD_hh_mm_ss);
			Date eDate = Utils.convertToDate(endDate, Utils.YYYY_MM_DD_hh_mm_ss);

			// get exported data by user,domain and date wise
			List<ExportedData> exportedDataList = ExportedData.findExportedDataByUserAndDomain(user, domain, sDate, eDate,pageNo,
					pageSize);
			
			// if exportedDataList is null/empty then return empty list
			if(Utils.isNullOrEmpty(exportedDataList))
				return new ArrayList<ExporterDataJSON_Out>();
				
			// return exported data list	
			return Utils.convertList(exportedDataList, ExporterDataJSON_Out.class);
		}
	}
}