package com.cariq.toolkit.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.ActionLog;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.Domain.Domain_Out;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_In;
import com.cariq.toolkit.model.ProxyObject.ProxyObject_Out;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * This class implements "ProxyObject" concept. We use proxy calls to obtain objects from "remote" services (e.g calls to CarIQ/W4 API layers). 
 * These objects can be retained in the current service/layer using their lighter version(just identification). 
 * These proxyobjects can be used to enhance functionality or to decorate in the current service.
 * 
 * @author amita
 *
 */

@Entity
@Configurable
public class ProxyObject extends PersistentObject<ProxyObject> implements JSONable<ProxyObject_In, ProxyObject_Out, IdJSON> {

	// SECTION 1 -Code for Persistence
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@Version
	Integer version;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new ProxyObject().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null)
			this.entityManager = entityManager();
		return entityManager;
	}

	/**
	 * @param id
	 * @return
	 */
	protected static ProxyObject find(Long id) {
		return entityManager().find(ProxyObject.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public ProxyObject findById(Long id) {
		return find(id);
	}

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/**
	 * Model properties
	 */

	@NotNull
	@ManyToOne
	Domain domain;
	
	
	Long objectId;
	
	@NotNull
	String objectType;
	
	String signature;
	
	/***** Owner of this proxy ****/
	@ManyToOne
	User owner;
	
	String latestDesposition;
	
	String renewalDisposition;
	
	String retrievalDisposition;
	
	String feedbackDisposition;
	

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	// Default constructor is a must!
	public ProxyObject() {
		super();
	}

	/**
	 * Parameterized constructor
	 * 
	 * @param domain
	 * @param objectId
	 * @param signature
	 * @param objectType
	 * @param createdOn
	 * @param modifiedOn
	 */
	public ProxyObject(Domain domain, Long objectId, String objectType, String signature,
			User owner, String latestDesposition, Date createdOn, Date modifiedOn) {
		super();
		this.domain = domain;
		this.objectId = objectId;
		this.objectType = objectType;
		this.signature = signature;
		this.owner = owner;
		this.latestDesposition = latestDesposition;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
	}

	/**
	 * Getters Setters
	 */
	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public Long getObjectId() {
		return objectId;
	}

	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public String getLatestDesposition() {
		return latestDesposition;
	}

	public void setLatestDesposition(String latestDesposition) {
		this.latestDesposition = latestDesposition;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
	

	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------

	public String getRenewalDisposition() {
		return renewalDisposition;
	}

	public void setRenewalDisposition(String renewalDisposition) {
		this.renewalDisposition = renewalDisposition;
	}

	public String getRetrievalDisposition() {
		return retrievalDisposition;
	}

	public void setRetrievalDisposition(String retrievalDisposition) {
		this.retrievalDisposition = retrievalDisposition;
	}

	public String getFeedbackDisposition() {
		return feedbackDisposition;
	}

	public void setFeedbackDisposition(String feedbackDisposition) {
		this.feedbackDisposition = feedbackDisposition;
	}

	// Implementation of toString and hashCode and equals are advisable for all
	// Model classes
	@Override
	public String toString() {
		return "ProxyObject [id=" + id + ", version=" + version + ", domain=" + domain.getId() + 
				", objectId=" + objectId + ", signature=" + signature + ", objectType=" + objectType +
				", owner=" + owner + ", latestDesposition=" + latestDesposition +  
				", renewalDisposotion= " + renewalDisposition + ", retrievalDisposition= " + retrievalDisposition +
				", feedbackDisposition= " + feedbackDisposition + 
				", createdOn=" + createdOn.toString() + ", modifiedOn=" + modifiedOn.toString() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((domain == null) ? 0 : domain.getId().hashCode());
		result = prime * result + ((objectId == null) ? 0 : objectId.hashCode());
		result = prime * result + ((signature == null) ? 0 : signature.hashCode());
		result = prime * result + ((objectType == null) ? 0 : objectType.hashCode());
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		result = prime * result + ((latestDesposition == null) ? 0 : latestDesposition.hashCode());
		result = prime * result + ((renewalDisposition == null) ? 0 : renewalDisposition.hashCode());
		result = prime * result + ((retrievalDisposition == null) ? 0 : retrievalDisposition.hashCode());
		result = prime * result + ((feedbackDisposition == null) ? 0 : feedbackDisposition.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProxyObject other = (ProxyObject) obj;
		if (domain == null) {
			if (other.domain != null)
				return false;
		} else if (!domain.getId().equals(other.domain.getId()))
			return false;
		if (objectId == null) {
			if (other.objectId != null)
				return false;
		} else if (!objectId.equals(other.objectId))
			return false;
		if (signature == null) {
			if (other.signature != null)
				return false;
		} else if (!signature.equals(other.signature))
			return false;
		if (objectType == null) {
			if (other.objectType != null)
				return false;
		} else if (!objectType.equals(other.objectType))
			return false;
		if(owner == null) {
			if(other.owner != null)
				return false;
		} else if(!owner.equals(other.owner))
			return false;
		if(latestDesposition == null) {
			if(other.latestDesposition != null)
				return false;
		} else if(!latestDesposition.equals(other.latestDesposition))
			return false;
		if(renewalDisposition == null) {
			if(other.renewalDisposition != null)
				return false;
		} else if(!renewalDisposition.equals(other.renewalDisposition))
			return false;
		if(retrievalDisposition == null) {
			if(other.retrievalDisposition != null)
				return false;
		} else if(!retrievalDisposition.equals(other.retrievalDisposition))
			return false;
		if(feedbackDisposition == null) {
			if(other.feedbackDisposition != null)
				return false;
		} else if(!feedbackDisposition.equals(other.feedbackDisposition))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		return true;
	}

	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API
	// interaction
	// In Class (for construction)
	public static class ProxyObject_In {
		String domainName;
		Long objectId;
		String signature;
		String objectType;
		String username;
		Long domainId;
		String latestDesposition;
		
		String renewalDisposition;
		String retrievalDisposition;
		String feedbackDisposition;

		public ProxyObject_In() {
			super();
		}
		
		/***
		 * Constructor
		 * 
		 * @param domainName
		 * @param objectId
		 * @param objectType
		 * @param signature
		 * @param username
		 * @param domainId
		 */
		public ProxyObject_In(String domainName, Long objectId, String objectType, 
				String signature, String username, Long domainId) {
			this(domainName, objectId, objectType, signature, username, domainId, null, null, null, null);
		}

		/**
		 * @param domainId
		 * @param objectId
		 * @param objectType
		 * @param signature
		 */
		public ProxyObject_In(String domainName, Long objectId, String objectType, 
				String signature, String username, Long domainId, String latestDesposition, String renewalDisposition,
				String retrievalDisposition, String feedbackDisposition) {
			this.domainName = domainName;
			this.objectId = objectId;
			this.objectType = objectType;
			this.signature = signature;
			this.username = username;
			this.domainId = domainId;
			this.latestDesposition = latestDesposition;
			this.renewalDisposition = renewalDisposition;
			this.retrievalDisposition = retrievalDisposition;
			this.feedbackDisposition = feedbackDisposition;
		}

		public String getDomainName() {
			return domainName;
		}

		public void setDomainName(String domainName) {
			this.domainName = domainName;
		}

		public Long getObjectId() {
			return objectId;
		}

		public void setObjectId(Long objectId) {
			this.objectId = objectId;
		}

		public String getObjectType() {
			return objectType;
		}

		public void setObjectType(String objectType) {
			this.objectType = objectType;
		}
		
		public String getSignature() {
			return signature;
		}

		public void setSignature(String signature) {
			this.signature = signature;
		}

		public Long getDomainId() {
			return domainId;
		}

		public void setDomainId(Long domainId) {
			this.domainId = domainId;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getLatestDesposition() {
			return latestDesposition;
		}

		public void setLatestDesposition(String latestDesposition) {
			this.latestDesposition = latestDesposition;
		}
	}

	// Out class for output
	public static class ProxyObject_Out extends ProxyObject_In {
		Long id;
		Domain_Out domainJSON;
		String createdOn;
		String modifiedOn;

		public ProxyObject_Out() {
			super();
		}
		
		/**
		 * 
		 * @param domainName
		 * @param objectId
		 * @param signature
		 * @param objectType
		 * @param createdOn
		 * @param modifiedOn
		 * @param id
		 * @param domainJSON
		 */
		public ProxyObject_Out(String domainName, Long objectId, String signature, String username, String objectType, 
				String createdOn, String modifiedOn, Long id, Domain_Out domainJSON, String latestDesposition,
				String renewalDisposition, String retrievalDisposition, String feedbackDisposition) {
			super(domainName, objectId, objectType, signature, username, domainJSON.getId(), latestDesposition,
					renewalDisposition, retrievalDisposition, feedbackDisposition);
			this.domainJSON = domainJSON;
			this.id = id;
			this.createdOn = createdOn;
			this.modifiedOn = modifiedOn;
		}

		public Domain_Out getDomainJSON() {
			return domainJSON;
		}

		public void setDomainJSON(Domain_Out domainJSON) {
			this.domainJSON = domainJSON;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toJSON()
	 */
	@Override
	public ProxyObject_Out toJSON() {
		String ownerName = owner != null ? owner.getUsername() : null;
		return new ProxyObject_Out(domain.getName(), objectId, signature, ownerName, objectType, createdOn.toString(), 
				modifiedOn.toString(), id, domain.toJSON(), latestDesposition, renewalDisposition, retrievalDisposition,
				feedbackDisposition);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#fromJSON(java.lang.Object)
	 */
	@Override
	public void fromJSON(ProxyObject_In json) {
		this.domain = json.domainName == null ? null : Domain.getByName(json.domainName);
		//if get domain by name failed then try to find by domain id
		if(this.domain == null) {
			this.domain = json.domainId == null ? null : Domain.find(json.domainId);
		}
		this.objectId = json.getObjectId();
		this.signature = json.getSignature();
		this.objectType = json.getObjectType();
		this.owner = User.getByUsername(json.getUsername());
		this.latestDesposition = json.getLatestDesposition();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}

	// ---------------------------------------------
	// SECTION 5 - Query and Class specific finders and static utility methods
	// ---------------------------------------------
	public static ProxyObject findObjectById(Long id) {
		return find(id);
	}
	
	public static List<ProxyObject> getAllObjects(int pageNo, int pageSize) {
		CarIQSimpleQuery<ProxyObject> qry = new CarIQSimpleQuery<ProxyObject>("getAllObjects", entityManager(), ProxyObject.class);
		qry.setPaging(pageNo, pageSize);

		return qry.getResultList();
	}

	public static ProxyObject getByDomainObjectIdType(Domain domain, Long objectId, String objectType) {
		CarIQSimpleQuery<ProxyObject> qry = new CarIQSimpleQuery<ProxyObject>("getByDomainObjectIdType", entityManager(), ProxyObject.class);
		qry.addCondition("domain", "=", domain);
		qry.addCondition("objectId", "=", objectId);
		qry.addCondition("objectType", "=", objectType);
		
		return qry.getSingleResult();
	}
	
	public static ProxyObject getByDomainSignatureObjectType(Domain domain, String signature, String objectType) {
		CarIQSimpleQuery<ProxyObject> qry = new CarIQSimpleQuery<ProxyObject>("getByDomainSignatureObjectType", entityManager(), ProxyObject.class);
		qry.addCondition("domain", "=", domain);
		qry.addCondition("signature", "=", signature);
		qry.addCondition("objectType", "=", objectType);
		return qry.getSingleResult();
	}
	
	/**
	 * Get by domain and list of signatures and object type.
	 * 
	 * @param domain
	 * @param signatures
	 * @param objectType
	 * @return
	 */
	public static List<ProxyObject> getByDomainSignaturesObjectType(Domain domain, String signatures, String objectType) {
		CarIQSimpleQuery<ProxyObject> qry = new CarIQSimpleQuery<ProxyObject>("getByDomainSignatureObjectType", entityManager(), ProxyObject.class);
		qry.addCondition("domain", "=", domain);
		qry.addRawCondition("signature in " + signatures);
		qry.addCondition("objectType", "=", objectType);
		return qry.getResultList();
	}
	
	
	/**
	 * Get all proxy objects that match given condition
	 * 
	 * @param signatures		comma seperated signatures. 
	 * @param domain			domain of proxy object
	 * @param objectType		type of proxy object
	 * @param pageNo			page number
	 * @param pageSize			page size
	 * @return					list or proxy objects that matches condition
	 */
	public static List<ProxyObject> getAllObjectsForSignatures(String signatures, Domain domain, String objectType,
			int pageNo, int pageSize) {
		
		List<String> arr = Arrays.asList(signatures.split(","));
		
		CarIQSimpleQuery<ProxyObject> qry = new CarIQSimpleQuery<ProxyObject>("getAllObjectsForSignatures", entityManager(), ProxyObject.class);
		qry.addCondition("domain", "=", domain);
		qry.addCondition("objectType", "=", objectType);
		qry.addCondition("signature", "IN", arr);
		qry.setPaging(pageNo, pageSize);

		return qry.getResultList();
	}
	
	
	/**
	 * Update proxy object.
	 * 
	 * @param domain
	 * @param objectId
	 * @param objectType
	 * @param signature
	 * @param owner
	 * @param latestDesposition
	 * @return
	 */
	public static ProxyObject update(Domain domain, Long objectId, String objectType, String signature,
			User owner, String latestDesposition, String nextAction) {
		ProxyObject proxy = ProxyObject.getByDomainSignatureObjectType(domain, signature, objectType);
		
		if (null != proxy) {
			// Update owner
			if(owner != null) {
				proxy.setOwner(owner);
				proxy.setModifiedOn(new Date());
				proxy.merge();
				ActionLog.log(Utils.createContext("ProxyObject.createOrGet"), "updateOwner",
						"ProxyObject", signature, owner);
			}
			
			//Update latest desposition
			if(Utils.notNullAndEmpty(latestDesposition)) {
				
				//If next action is due for renewal then update renewal desposition
				if(StagingAndPolicyHelper.StateRenewal.equalsIgnoreCase(nextAction)) {
					proxy.setRenewalDisposition(latestDesposition);
				} else if(StagingAndPolicyHelper.StateRetrieval.equalsIgnoreCase(nextAction)) {
					//If next action is due for retrieval then update retrieval desposition 
					proxy.setRetrievalDisposition(latestDesposition);
				} else {
					//If nextAction is not available then just update latestDesposition
					proxy.setLatestDesposition(latestDesposition);	
				}
				proxy.setModifiedOn(new Date());
				proxy.merge();
				ActionLog.log(Utils.createContext("ProxyObject.createOrGet"), "updateLatestDesposition",
						"ProxyObject", signature, latestDesposition);
			}
			
			// Update signature if required
			if(proxy.getObjectId() == null && objectId != null) {
				proxy.setObjectId(objectId);
				proxy.setModifiedOn(new Date());
				proxy.merge();
			}
			return proxy;
		}
		return null;
	}

	/**
	 * Create or get proxy.
	 * 
	 * @param domain
	 * @param objectId
	 * @param objectType
	 * @param signature
	 * @param owner
	 * @param latestDesposition
	 * @return
	 */
	public static ProxyObject createOrGet(Domain domain, Long objectId, String objectType, String signature, User owner, String latestDesposition) {
				
		// Check if proxy-object exists
		ProxyObject proxy = ProxyObject.getByDomainSignatureObjectType(domain, signature, objectType);
		if (null != proxy) {
			return proxy;
		}

		// Need to create new proxy-object
		proxy = new ProxyObject(domain, objectId, objectType, signature, owner, latestDesposition, new Date(), new Date());
		proxy.persist();

		return proxy;
	}
	
	
	/**
	 * Get proxy by tag and domain
	 * 
	 * @param tag
	 * @param domain
	 * @return
	 */
	public static List<ProxyObject> getByTagAndDomain(String tag, Domain domain, int pageNumber, int pageSize) {
		try (ProfilePoint _getByTagAndDomain = ProfilePoint
				.profileAction("ProfAction_getByTagAndDomain")) {
			String queryStr = "select o from ProxyObject o, Timeline t " + "where o.id=t.proxy and t.tag='" + tag
					+ "' and o.domain = " + domain.getId()
					+ " and t.id in (select max(id) from Timeline where tag is not null group by proxy)";
			TypedQuery<ProxyObject> query = ProxyObject.entityManager().createQuery(queryStr, ProxyObject.class);
			query.setFirstResult(Utils.validatePagination(pageSize, pageNumber));
			query.setMaxResults(pageSize);
			return query.getResultList();
		}
	}

	/**
	 * Get proxy by tag and domain and owner.
	 * 
	 * @param tag
	 * @param domain
	 * @param owner
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public static List<ProxyObject> getByLatestTagsAndDomainAndOwner(String commaSepTagsForQuery, Domain domain, User owner, String column,
			int pageNumber, int pageSize) {
		try (ProfilePoint _getByTagAndDomain = ProfilePoint
				.profileAction("ProfAction_getByLatestTagAndDomainAndOwner")) {
			String queryStr = "SELECT p FROM ProxyObject p WHERE p. " + column + "  IN " + commaSepTagsForQuery
					+ " AND p.domain = " + domain.getId() + " AND p.owner = " + owner.getId();
			System.out.println(queryStr);
			TypedQuery<ProxyObject> query = ProxyObject.entityManager().createQuery(queryStr, ProxyObject.class);
			query.setFirstResult(Utils.validatePagination(pageSize, pageNumber));
			query.setMaxResults(pageSize);
			return query.getResultList();
		}
	}
	
	/**
	 * Get proxy by tag and domain.
	 * 
	 * @param tag
	 * @param domain
	 * @param owner
	 * @param pageNumber
	 * @param pageSize
	 * @return
	 */
	public static List<ProxyObject> getByLatestTagsAndDomain(String commaSepTagsForQuery, Domain domain, int pageNumber, int pageSize) {
		try (ProfilePoint _getByTagAndDomain = ProfilePoint
				.profileAction("ProfAction_getByLatestTagAndDomainAndOwner")) {
			String queryStr = "SELECT p FROM ProxyObject p WHERE p.latestDesposition IN " + commaSepTagsForQuery + " AND p.domain = " + domain.getId();
			System.out.println(queryStr);
			TypedQuery<ProxyObject> query = ProxyObject.entityManager().createQuery(queryStr, ProxyObject.class);
			query.setFirstResult(Utils.validatePagination(pageSize, pageNumber));
			query.setMaxResults(pageSize);
			return query.getResultList();
		}
	}
	
	
	/**
	 * Get list of signatures from proxy objects
	 * 
	 * @param proxyObjects
	 * @return
	 */
	public static List<String> getSignatures(List<ProxyObject> proxyObjects) {
		List<String> signatures = new ArrayList<>();
		for(ProxyObject object: proxyObjects) {
			signatures.add(object.getSignature());
		}
		return signatures;
	}
	
	
	/**
	 * Get complement for given object against all objects
	 * 
	 * @param proxyObjects
	 * @param domain2 
	 * @return
	 */
	public static List<ProxyObject> getByOwnerWithComplement(List<ProxyObject> proxyObjects, User user, Domain domain) {
		try (ProfilePoint _complementQuery = ProfilePoint
				.profileAction("ProfAction_complementQuery_ProxyObject")) {
	
			CarIQSimpleQuery<ProxyObject> query = new CarIQSimpleQuery<ProxyObject>("query", ProxyObject.entityManager(), ProxyObject.class);
			if(!Utils.isNullOrEmpty(proxyObjects)) {
				List<Long> ids = ProxyObject.getProxyIds(proxyObjects);
				query.addRawCondition("id NOT IN (" + Utils.getCommaSeperatedString(ids) + ")");
			}
			query.addCondition("owner", "=", user);
			query.addCondition("domain", "=", domain);
			
			List<ProxyObject> resultList = query.getResultList();
			return resultList;			
		}
	}
	
	/**
	 * Get list of proxy ids from proxy objects
	 * 
	 * @param proxies
	 * @return
	 */
	public static List<Long> getProxyIds(List<ProxyObject> proxies ) {
		List<Long> ids = new ArrayList<>();
		for(ProxyObject proxy: proxies) {
			ids.add(proxy.getId());
		}
		return ids;
	}
	
	
	/**
	 * Get proxies by owners.
	 * 
	 * @param owners
	 * @return
	 */
	public static List<ProxyObject> getProxiesByOwner(List<User> owners) {
		CarIQSimpleQuery<ProxyObject> qry = new CarIQSimpleQuery<ProxyObject>("getUsers", entityManager(), ProxyObject.class);
		qry.addCondition("owner", "IN", owners);
		
		return qry.getResultList();
	}
	

	/**
	 * Get proxies by owners.
	 * 
	 * @param owners
	 * @return
	 */
	public static List<ProxyObject> getProxiesByOwnerAndCreatedAfter(List<User> owners, Date createdAfter) {
		CarIQSimpleQuery<ProxyObject> qry = new CarIQSimpleQuery<ProxyObject>("getUsers", entityManager(), ProxyObject.class);
		qry.addCondition("owner", "IN", owners);
		qry.addCondition("createdOn", CarIQSimpleQuery.GT, createdAfter);
		
		return qry.getResultList();
	}
	
	/**
	 * Get identified list of object against given identity
	 * 
	 * @param list
	 * @param identity
	 * @return
	 */
	public static LinkedHashMap<String, ProxyObject> getIdentifiedMapFromList(List<ProxyObject> list) {
		LinkedHashMap<String, ProxyObject> retValues = new LinkedHashMap<>();
		for (ProxyObject t : list) {
			retValues.put(t.getSignature(), t);
		}
		return retValues;
	}
}