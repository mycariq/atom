package com.atom.www.exporter;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

public class SupportTATReport extends StagingAndPolicyDetailsMiniReportExporter {
	
	public static final String TheExporter = "SupportTATReport";
	private static final String TheExporterDescription = "Support Exporter";
	
	/**
	 * Default constructor
	 */
	public SupportTATReport() {
		super(TheExporter, TheExporterDescription);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected GenericJSON getEnhancedObject(GenericJSON json, String queryType, GenericJSON inputJson) {
		GenericJSON exportedJSON = super.getEnhancedObject(json, queryType, inputJson);
		exportedJSON.put("owner", Utils.getValue(json, "owner"));
		exportedJSON.put("PolicyPlan", Utils.getValue(json, "Staging_policyPlan"));
		exportedJSON.put("Address", Utils.getValue(json, "Staging_customerAddress"));
		
		return exportedJSON;
	}
}
