package com.cariq.toolkit.model.async.lrt;

import org.springframework.beans.factory.annotation.Autowired;

import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.service.CarIQRestClientService;

/**
 * Fetch LRT from other system
 * @author Hrishi
 *
 */
public class LRTRestCallFetcher implements LRTFetcher {
	@Autowired
	CarIQRestClientService restService;
	String url;
	String securityToken;
	
	
	public LRTRestCallFetcher(CarIQRestClientService service, String url, String securityToken) {
		super();
		this.restService = service;
		this.url = url;
		this.securityToken = securityToken;
	}


	@Override
	public LRTJSON fetch(String lrtToken) {
		try {
			return restService.get(url + lrtToken, securityToken, LRTJSON.class);
		} catch (Exception e) {
			Utils.handleException(e);
		}
		
		return null;
	}

}
