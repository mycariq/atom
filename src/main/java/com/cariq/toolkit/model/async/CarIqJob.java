package com.cariq.toolkit.model.async;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import org.apache.commons.lang.Validate;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.PeriodicEvent;
import com.cariq.toolkit.utils.Utils;

/**
 * Job is periodically running activity. Job can be run at a set timing every
 * day/week or every few minutes For a set timing everyday, ScheduledTime
 * dictates the time. For ever few minutes, scheduled time is null - so based on
 * previous run time, it is triggered Weekly and Monthly jobs also exist. They
 * are run on specific day of week/month
 *
 * @TODO This needs to be modelled.
 * @author HVN
 *
 */
@RooJavaBean
@RooToString
@Table(name = "job")
@RooJpaActiveRecord(finders = { "findCarIqJobsByJobNameEquals" })
public final class CarIqJob {

    // @Transient
    private static final CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("CarIqJob");

    public static final int JOB_FREQUENCY_NONE = -1;

    public static final int JOB_FREQUENCY_DAILY = Calendar.DATE;

    public static final int JOB_FREQUENCY_WEEKLY = Calendar.DAY_OF_WEEK_IN_MONTH;

    public static final int JOB_FREQUENCY_MONTHLY = Calendar.MONTH;

    @Column(unique = true)
    private String jobName;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "M-")
    private Date lastRunDate;

    // Static information - identified for every job
    // Frequency of job (unit and count)
    // Scheduled Day and Scheduled Time of execution
    // Unit of frequency - one of MINUTE, HOUR, DAILY, WEEKLY, MONTHLY
    @Column(nullable = false)
    private int frequencyUnit;

    // Number of frequencyUnits
    @Column(nullable = false)
    private int frequency;

    // Number of frequencyUnits
    @Column(nullable = false)
    private int defaultFrequency;

    // scheduled day (1-8 for weekly and 1-30 for monthly and 1-365 for yearly)
    private int scheduledDay;

    // scheduled time (between 00:00:00 to 23:59:59)
    @Temporal(TemporalType.TIME)
    @Column(nullable = false)
    private Date scheduledTime;

    public int getDefaultFrequency() {
        return defaultFrequency;
    }

    public void setDefaultFrequency(int defaultFrequency) {
        this.defaultFrequency = defaultFrequency;
    }

    public Date getNextRunTime() {
        PeriodicEvent periodicEvent = new PeriodicEvent(frequencyUnit, frequency, scheduledDay, scheduledTime);
        return periodicEvent.getNextOccuranceTime(lastRunDate);
    }

    private Date getNextDefaultRunTime() {
        PeriodicEvent periodicEvent = new PeriodicEvent(frequencyUnit, defaultFrequency, scheduledDay, scheduledTime);
        return periodicEvent.getNextOccuranceTime(lastRunDate);
    }

    public boolean shouldRunJob(DateTime now) {
        Validate.notNull(now);
        // check if job is not scheduled
        if (!isJobScheduled()) {
            return false;
        }
        // last run date is null, frequency is not "none" ,so can run
        // This is the way to explicitly trigger the job
        if (lastRunDate == null && frequency > 0) {
            return true;
        }
        // If according to frequency, job is due, return true
        if (frequency > 0) {
            Date nextTime = getNextRunTime();
            if (now.isAfter(new DateTime(nextTime))) return true;
            if (Utils.randomMatch(50)) logger.debug("Expected run date for Job: " + jobName + " is: " + nextTime);
        }
        // Check based on default Frequency - and reset frequency if needed
        if (defaultFrequency > 0 && frequency < 0) {
            Date nextDefaultTime = getNextDefaultRunTime();
            // If it's been 10 minutes since the expected run time, reset the frequency
            if (now.isAfter(new DateTime(nextDefaultTime)) && Utils.getDiffInMinutes(now.toDate(), nextDefaultTime) > 10) {
                frequency = defaultFrequency;
                this.persist();
            }
        }
        return false;
    }

    private boolean isJobScheduled() {
        // If both frequency and Default frequency is negative, job is off
        if (frequency <= 0 && defaultFrequency <= 0) {
            return false;
        }
        return true;
    }

    //	//
    //	// @SuppressWarnings("deprecation")
    //	// @Transient
    //	public Date getExpectedDateToRunJob(DateTime dt) {
    //		PeriodicEvent periodicEvent = new PeriodicEvent(frequencyUnit, frequency, scheduledDay, scheduledTime);
    //		return periodicEvent.getNextOccuranceTime(lastRunDate);
    //	}
    public static CarIqJob findJobByJobName(String jobName) {
        TypedQuery<CarIqJob> query = CarIqJob.findCarIqJobsByJobNameEquals(jobName);
        return query.getSingleResult();
    }

    public boolean isDue() {
        return shouldRunJob(new DateTime());
    }

    // Autocommit function - not transactional - requires some annotation?
    // @Transactional(propagation=Propagation.REQUIRES_NEW)
    public CarIqJob start() {
        this.setLastRunDate(new Date());
        return merge();
    }

    /**
     */
    private String jobType;
}
