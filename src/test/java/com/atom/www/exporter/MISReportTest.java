package com.atom.www.exporter;

import java.io.File;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.cariq.toolkit.coreiq.exporter.CarIQExporter;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterRegistry;
import com.cariq.toolkit.coreiq.exporter.MonthlyAggregationExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.fileexport.FileExport;
import com.cariq.toolkit.coreiq.exporter.fileexport.FileExportTemplate;
import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.GenericJSON;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:/META-INF/spring/applicationContext*.xml")
@Transactional
@Configurable
public class MISReportTest {

	@Test
	public void testVerificationTAT() {
		GenericJSON inputJson = GenericJSON.create(MonthlyAggregationExporterTemplate.StartTimestamp, "2018-01-04")
				.add(MonthlyAggregationExporterTemplate.EndTimestamp, "2018-12-06").add("username", "sagar");

		CarIQExporter exporter = CarIQExporterRegistry.getExporter(VerificationTATExporter.EXPORTER);

		List<GenericJSON> result = exporter.export(MonthlyAggregationExporterTemplate.TimeRange, inputJson, 1, 100);
		for (GenericJSON json : result) {
			System.out.println(json);
		}

	}

	@Test
	public void testBusinessVertical() {
		GenericJSON inputJson = GenericJSON.create(MonthlyAggregationExporterTemplate.StartTimestamp, "2018-01-04")
				.add(MonthlyAggregationExporterTemplate.EndTimestamp, "2018-12-06").add("username", "sagar");

		CarIQExporter exporter = CarIQExporterRegistry.getExporter(VerificationStatusExporter.EChannelExporter);

		List<GenericJSON> result = exporter.export(MonthlyAggregationExporterTemplate.TimeRange, inputJson, 1, 100);
		for (GenericJSON json : result) {
			System.out.println(json);
		}

	}

	@Test
	public void testTier() {
		GenericJSON inputJson = GenericJSON.create(MonthlyAggregationExporterTemplate.StartTimestamp, "2018-01-04")
				.add(MonthlyAggregationExporterTemplate.EndTimestamp, "2018-12-06").add("username", "sagar");

		CarIQExporter exporter = CarIQExporterRegistry.getExporter(VerificationStatusExporter.TierAExporter);

		List<GenericJSON> result = exporter.export(MonthlyAggregationExporterTemplate.TimeRange, inputJson, 1, 100);
		for (GenericJSON json : result) {
			System.out.println(json);
		}

	}

	@Test
	public void testAggregateExporter() {
		GenericJSON inputJson = GenericJSON.create(MonthlyAggregationExporterTemplate.StartTimestamp, "2018-01-04")
				.add(MonthlyAggregationExporterTemplate.EndTimestamp, "2018-12-06").add("username", "sagar");

		CarIQExporter exporter = new MISReport();

		List<GenericJSON> result = exporter.export(MonthlyAggregationExporterTemplate.TimeRange, inputJson, 1, 100);
		for (GenericJSON json : result) {
			System.out.println(json);
		}
	}

	@Test
	public void testExcel() {
		CarIQExporterRegistry.getExporter(MISReport.EXPORTER);
		GenericJSON inputJson = GenericJSON.create(MonthlyAggregationExporterTemplate.StartTimestamp, "2018-01-04")
				.add(MonthlyAggregationExporterTemplate.EndTimestamp, "2018-12-06").add("username", "sagar");

		FileExport fileExport = FileExportTemplate.getFileExport(MISReport.EXPORTER,
				MonthlyAggregationExporterTemplate.TimeRange, inputJson, CarIQFileUtils.EXCEL);
		File outputFile = fileExport.execute();
		System.out.println(outputFile.getAbsolutePath());
	}

}
