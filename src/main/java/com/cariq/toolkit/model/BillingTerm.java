package com.cariq.toolkit.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.BillingTerm.BillingTerm_In;
import com.cariq.toolkit.model.BillingTerm.BillingTerm_Out;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;

/**
 * This class implements "billing term"
 * Billing term is the billing rule and rate agreed in the billing contract by two parties (accounts).
 * @author amita
 *
 */

@Entity
@Configurable
public class BillingTerm extends PersistentObject<BillingTerm> implements JSONable<BillingTerm_In, BillingTerm_Out, IdJSON> {

	// SECTION 1 -Code for Persistence
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@Version
	Integer version;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new BillingTerm().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null)
			this.entityManager = entityManager();
		return entityManager;
	}

	/**
	 * @param id
	 * @return
	 */
	protected static BillingTerm find(Long id) {
		return entityManager().find(BillingTerm.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public BillingTerm findById(Long id) {
		return find(id);
	}

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/**
	 * Model properties
	 */
	@NotNull(message = "@NotNull BillingTerm Name")
	@Column(unique = true)
	String name;

	@NotNull
	@ManyToOne
	BillingContract itsContract;

	@Size(max = 4000)
	String definitionJson;
	
	@NotNull
	Long cost;
	
	@Size(max = 1024)
	String note;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	// Default constructor is a must!
	public BillingTerm() {
		super();
	}

	/**
	 * 
	 * @param name
	 * @param itsContract
	 * @param definitionJson
	 * @param note
	 * @param createdOn
	 * @param modifiedOn
	 */
	public BillingTerm(String name, BillingContract itsContract, String definitionJson, Long cost, 
			String note, Date createdOn, Date modifiedOn) {
		super();
		this.name = name;
		this.itsContract = itsContract;
		this.definitionJson = definitionJson;
		this.cost = cost;		
		this.note = note;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BillingContract getItsContract() {
		return itsContract;
	}

	public void setItsContract(BillingContract itsContract) {
		this.itsContract = itsContract;
	}

	public String getDefinitionJson() {
		return definitionJson;
	}

	public void setDefinitionJson(String definitionJson) {
		this.definitionJson = definitionJson;
	}

	public Long getCost() {
		return cost;
	}

	public void setCost(Long cost) {
		this.cost = cost;
	}
	
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	
	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------


	// Implementation of toString and hashCode and equals are advisable for all
	// Model classes
	@Override
	public String toString() {
		return "BillingTerm [id=" + id + ", version=" + version + ", name=" + name + ", itsContract=" + itsContract.getId()
				+ ", definitionJson=" + definitionJson + ", cost=" + cost + ", note=" + note + ", createdOn=" + createdOn + ", modifiedOn=" + modifiedOn + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((itsContract == null) ? 0 : itsContract.getId().hashCode());
		result = prime * result + ((definitionJson == null) ? 0 : definitionJson.hashCode());
		result = prime * result + ((cost == null) ? 0 : cost.hashCode());
		result = prime * result + ((note == null) ? 0 : note.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BillingTerm other = (BillingTerm) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (itsContract == null) {
			if (other.itsContract != null)
				return false;
		} else if (!itsContract.equals(other.itsContract))
			return false;
		if (definitionJson == null) {
			if (other.definitionJson != null)
				return false;
		} else if (!definitionJson.equals(other.definitionJson))
			return false;
		if (cost == null) {
			if (other.cost != null)
				return false;
		} else if (!cost.equals(other.cost))
			return false;
		if (note == null) {
			if (other.note != null)
				return false;
		} else if (!note.equals(other.note))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		return true;
	}

	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API
	// interaction
	
	// In Class (for construction)
	public static class BillingTerm_In {
		String name;
		Long contractId;
		String definitionJson;
		Long cost;
		String note;

		public BillingTerm_In() {
			super();
		}

		/**
		 * 
		 * @param name
		 * @param contractId
		 * @param definitionJson
		 * @param note
		 */
		public BillingTerm_In(String name, Long contractId, String definitionJson, Long cost, String note) {
			super();
			this.name = name;
			this.contractId = contractId;
			this.definitionJson = definitionJson;
			this.cost = cost;
			this.note = note;
		}

		/**
		 * Getter Setters
		 */
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Long getContractId() {
			return contractId;
		}

		public void setContractId(Long contractId) {
			this.contractId = contractId;
		}

		public String getDefinitionJSON() {
			return definitionJson;
		}

		public void setDefinitionJSON(String definitionJson) {
			this.definitionJson = definitionJson;
		}
		
		public Long getCost() {
			return cost;
		}

		public void setCost(Long cost) {
			this.cost = cost;
		}
		
		public String getNote() {
			return note;
		}

		public void setNote(String note) {
			this.note = note;
		}
		
		@Override
		public String toString() {
			return "BillingTerm [name=" + name + ", contractId=" + contractId + ", definitionJson=" + definitionJson + ", cost=" + cost + "]";
		}

	}

	// Out class for output
	public static class BillingTerm_Out extends BillingTerm_In {
		Long id;
		String createdOn;
		String modifiedOn;

		public BillingTerm_Out() {
			super();
		}

		public BillingTerm_Out(String name, Long contractId, String definitionJson, Long cost, String note, String createdOn, String modifiedOn, Long id) {
			super(name, contractId, definitionJson, cost, note);
			this.id = id;
			this.createdOn = createdOn;
			this.modifiedOn = modifiedOn;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toJSON()
	 */
	@Override
	public BillingTerm_Out toJSON() {
		return new BillingTerm_Out(name, itsContract.getId(), definitionJson, cost, note, createdOn.toString(), modifiedOn.toString(), id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#fromJSON(java.lang.Object)
	 */
	@Override
	public void fromJSON(BillingTerm_In json) {
		this.name =  json.name;
		this.itsContract =  json.getContractId() != null ? BillingContract.findObjectById(json.getContractId()) : null;
		this.definitionJson = json.definitionJson;
		this.cost = json.cost;
		this.note = json.note;
		this.createdOn = new Date();
		this.modifiedOn = new Date();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}

	// ---------------------------------------------
	// SECTION 5 - Query and Class specific finders and static utility methods
	// ---------------------------------------------
	public static BillingTerm findObjectById(Long id) {
		return find(id);
	}
	
	public static List<BillingTerm> getAllObjects(int pageNo, int pageSize) {
		CarIQSimpleQuery<BillingTerm> qry = new CarIQSimpleQuery<BillingTerm>("getAllObjects", entityManager(), BillingTerm.class);
		qry.setPaging(pageNo, pageSize);

		return qry.getResultList();
	}

	public static BillingTerm getByName(String name) {
		CarIQSimpleQuery<BillingTerm> qry = new CarIQSimpleQuery<BillingTerm>("getByName", entityManager(), BillingTerm.class);
		qry.addCondition("name", "=", name);

		if (qry.getResultList().isEmpty())
			return null;
		return qry.getSingleResult();
	}
	
	public static List<BillingTerm> getByContract(BillingContract contract) {
		TypedQuery<BillingTerm> query = BillingTerm.entityManager()
				.createQuery("SELECT bt FROM BillingTerm bt WHERE bt.itsContract = :contract", BillingTerm.class);
		query.setParameter("contract", contract);

		return query.getResultList();
	}

	// Only definitionJson and note can be updated
	public void update(BillingTerm_In params) {
		if (params.definitionJson != null)
			this.definitionJson = params.definitionJson;
		if (params.note != null)
			this.note = params.note;
		if(params.cost != null)
			this.cost = params.cost;
		
		this.modifiedOn = new Date();
		this.persist();
	}
}