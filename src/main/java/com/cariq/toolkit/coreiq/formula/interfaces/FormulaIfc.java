package com.cariq.toolkit.coreiq.formula.interfaces;

import java.util.List;

public interface FormulaIfc {
	String getExpression();
	List<VariableIfc> getVariables();
	
	// Will be Numeric - but one can use StrConcat etc. so...
	Object evaluate(Object context) throws Exception;
}
