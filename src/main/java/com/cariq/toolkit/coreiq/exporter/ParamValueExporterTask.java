/**
 * 
 */
package com.cariq.toolkit.coreiq.exporter;

import java.util.concurrent.Callable;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

/**
 * @author hrishi
 * @param <T>
 *
 */
public abstract class ParamValueExporterTask implements Callable<ParamDescriptionValueDiagnosis> {
	String queryType;
	GenericJSON inputJson;
	
	protected abstract ParamDescriptionValueDiagnosis doExecute();
	/* (non-Javadoc)
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public ParamDescriptionValueDiagnosis call() throws Exception {
		try {
			return doExecute();
		} catch (Exception e) {
			Utils.handleException(e);
			throw e;
		}
	}
	/**
	 * @param queryType
	 * @param json
	 * @return
	 */
	public ParamValueExporterTask initialize(String queryType, GenericJSON json) {
		this.queryType = queryType;
		this.inputJson = json;
		
		return this;
	}
	
	public String getQueryType() {
		return queryType;
	}
	
	public GenericJSON getInputJson() {
		return inputJson;
	}
	
	
	
}
