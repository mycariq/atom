package com.cariq.toolkit.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.BillingOperation.BillingOperation_In;
import com.cariq.toolkit.model.BillingOperation.BillingOperation_Out;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;
import com.cariq.toolkit.utils.Utils;

/**
 * This class implements "billing operation"
 * This is the process of actual billing by applying rules (billing terms) and generating invoice against specific account. 
 * This model captures the result of billing operation. Billing operation generates billables and invoices.
 * @author amita
 *
 */

@Entity
@Configurable
public class BillingOperation extends PersistentObject<BillingOperation> implements JSONable<BillingOperation_In, BillingOperation_Out, IdJSON> {
	
	/**
	 * The Enum BillingStatusEnum.
	 */
	public static enum BillingStatusEnum {
		SUCCESSFUL, FAILED, INVALID 
	};
	
	// SECTION 1 -Code for Persistence
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@Version
	Integer version;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new BillingOperation().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null)
			this.entityManager = entityManager();
		return entityManager;
	}

	/**
	 * @param id
	 * @return
	 */
	protected static BillingOperation find(Long id) {
		return entityManager().find(BillingOperation.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public BillingOperation findById(Long id) {
		return find(id);
	}

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/**
	 * Model properties
	 */
	@NotNull
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date billingDate;
	
	String billingResult; // Can be Success/Failed

	@NotNull
	@ManyToOne
	BillingAccount raisedbyAccount;

	@NotNull
	@ManyToOne
	BillingAccount againstAccount;
	
	@NotNull
	@ManyToOne
	BillingContract itsContract;
	
	@Size(max = 4000)
	String forBillingTerms; 
	
	// MMMM-yyyy (February-2020)
	String billingStartMonth;
	
	// MMMM-yyyy (October-2021)
	String billingEndMonth;
	
	@NotNull
	@ManyToOne
	Domain itsDomain;
	
	@NotNull
	@ManyToOne
	User itsUser;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	// Default constructor is a must!
	public BillingOperation() {
		super();
	}

	/**
	 * 
	 * @param billingDate
	 * @param billingResult
	 * @param raisedbyAccount
	 * @param againstAccount
	 * @param itsContract
	 * @param billingStartMonth
	 * @param billingEndMonth
	 * @param forBillingTerms
	 * @param createdOn
	 * @param modifiedOn
	 */
	public BillingOperation(Date billingDate, String billingResult, BillingAccount raisedbyAccount,
			BillingAccount againstAccount, BillingContract itsContract, String billingStartMonth, String billingEndMonth, 
			String forBillingTerms, Domain itsDomain, User itsUser, Date createdOn,	Date modifiedOn) {
		super();
		this.billingDate = billingDate;
		this.billingResult = billingResult;
		this.raisedbyAccount = raisedbyAccount;
		this.againstAccount = againstAccount;
		this.itsContract = itsContract;
		this.forBillingTerms = forBillingTerms;
		this.billingStartMonth = billingStartMonth;
		this.billingEndMonth = billingEndMonth;
		this.itsDomain = itsDomain;
		this.itsUser = itsUser;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
	}

	/**
	 * Getter-Setters
	 */
	
	public Date getBillingDate() {
		return billingDate;
	}

	public void setBillingDate(Date billingDate) {
		this.billingDate = billingDate;
	}

	public String getBillingResult() {
		return billingResult;
	}

	public void setBillingResult(String billingResult) {
		this.billingResult = billingResult;
	}

	public BillingAccount getRaisedbyAccount() {
		return raisedbyAccount;
	}

	public void setRaisedbyAccount(BillingAccount raisedbyAccount) {
		this.raisedbyAccount = raisedbyAccount;
	}

	public BillingAccount getAgainstAccount() {
		return againstAccount;
	}

	public void setAgainstAccount(BillingAccount againstAccount) {
		this.againstAccount = againstAccount;
	}

	public BillingContract getItsContract() {
		return itsContract;
	}

	public void setItsContract(BillingContract itsContract) {
		this.itsContract = itsContract;
	}

	public String getForBillingTerms() {
		return forBillingTerms;
	}

	public void setForBillingTerms(String forBillingTerms) {
		this.forBillingTerms = forBillingTerms;
	}
	
	public String getBillingStartMonth() {
		return billingStartMonth;
	}

	public void setBillingStartMonth(String billingStartMonth) {
		this.billingStartMonth = billingStartMonth;
	}

	public String getBillingEndMonth() {
		return billingEndMonth;
	}

	public void setBillingEndMonth(String billingEndMonth) {
		this.billingEndMonth = billingEndMonth;
	}	
	
	public User getItsUser() {
		return itsUser;
	}

	public void setItsUser(User itsUser) {
		this.itsUser = itsUser;
	}

	public Domain getItsDomain() {
		return itsDomain;
	}

	public void setItsDomain(Domain itsDomain) {
		this.itsDomain = itsDomain;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------

	// Implementation of toString and hashCode and equals are advisable for all
	// Model classes
	@Override
	public String toString() {
		return "BillingOperation [id=" + id + ", version=" + version + ", billingDate=" + billingDate + ", billingResult=" + billingResult  
				+ ", raisedbyAccount=" + raisedbyAccount.getName() + ", againstAccount=" + againstAccount.getName() + ", itsContract=" + itsContract.getId() 
				+ ", forBillingTerms=" + forBillingTerms + ", billingStartMonth=" + billingStartMonth + ", billingEndMonth" + billingEndMonth
				+ ", itsDomain=" + itsDomain.getName() + ", itsUser=" + itsUser.getId() 
				+ ", createdOn=" + createdOn + ", modifiedOn=" + modifiedOn + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((billingDate == null) ? 0 : billingDate.hashCode());
		result = prime * result + ((billingResult == null) ? 0 : billingResult.hashCode());
		result = prime * result + ((raisedbyAccount == null) ? 0 : raisedbyAccount.getName().hashCode());
		result = prime * result + ((againstAccount == null) ? 0 : againstAccount.getName().hashCode());
		result = prime * result + ((itsContract == null) ? 0 : itsContract.getId().hashCode());
		result = prime * result + ((forBillingTerms == null) ? 0 : forBillingTerms.hashCode());
		result = prime * result + ((billingStartMonth == null) ? 0 : billingStartMonth.hashCode());
		result = prime * result + ((billingEndMonth == null) ? 0 : billingEndMonth.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((itsDomain == null) ? 0 : itsDomain.hashCode());
		result = prime * result + ((itsUser == null) ? 0 : itsUser.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BillingOperation other = (BillingOperation) obj;
		if (billingDate == null) {
			if (other.billingDate != null)
				return false;
		} else if (!billingDate.equals(other.billingDate))
			return false;
		if (billingResult == null) {
			if (other.billingResult != null)
				return false;
		} else if (!billingResult.equals(other.billingResult))
			return false;
		if (raisedbyAccount == null) {
			if (other.raisedbyAccount != null)
				return false;
		} else if (!raisedbyAccount.equals(other.raisedbyAccount))
			return false;
		if (againstAccount == null) {
			if (other.againstAccount != null)
				return false;
		} else if (!againstAccount.equals(other.againstAccount))
			return false;
		if (itsContract == null) {
			if (other.itsContract != null)
				return false;
		} else if (!itsContract.equals(other.itsContract))
			return false;
		if (forBillingTerms == null) {
			if (other.forBillingTerms != null)
				return false;
		} else if (!forBillingTerms.equals(other.forBillingTerms))
			return false;
		if (billingStartMonth == null) {
			if (other.billingStartMonth != null)
				return false;
		} else if (!billingStartMonth.equals(other.billingStartMonth))
			return false;
		if (billingEndMonth == null) {
			if (other.billingEndMonth != null)
				return false;
		} else if (!billingEndMonth.equals(other.billingEndMonth))
			return false;
		if (itsDomain == null) {
			if (other.itsDomain != null)
				return false;
		} else if (!itsDomain.equals(other.itsDomain))
			return false;
		if (itsUser == null) {
			if (other.itsUser != null)
				return false;
		} else if (!itsUser.equals(other.itsUser))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		return true;
	}

	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API
	// interaction
	
	// In Class (for construction)
	public static class BillingOperation_In {
		String billingDate;
		String billingResult;
		String raisedbyAccountName;
		String againstAccountName; 
		Long itsContractId; 
		String forBillingTerms;
		String billingStartMonth;
		String billingEndMonth;
		String domainName;
		String userName;
		
		public BillingOperation_In() {
			super();
		}

		/**
		 * 
		 * @param billingDate
		 * @param billingResult
		 * @param raisedbyAccountName
		 * @param againstAccountName
		 * @param itsContractId
		 * @param forBillingTerms
		 * @param billingStartMonth
		 * @param billingEndMonth
		 */
		public BillingOperation_In(String billingDate, String billingResult, String raisedbyAccountName,
				String againstAccountName, Long itsContractId, String forBillingTerms,  
				String billingStartMonth, String billingEndMonth, String domainName, String userName) {
			super();
			this.billingDate = billingDate;
			this.billingResult = billingResult;
			this.raisedbyAccountName = raisedbyAccountName;
			this.againstAccountName = againstAccountName;
			this.itsContractId = itsContractId;
			this.forBillingTerms = forBillingTerms;
			this.billingStartMonth = billingStartMonth;
			this.billingEndMonth = billingEndMonth;
			this.domainName = domainName;
			this.userName = userName;
		}

		public String getRaisedbyAccountName() {
			return raisedbyAccountName;
		}

		public void setRaisedbyAccount(String raisedbyAccountName) {
			this.raisedbyAccountName = raisedbyAccountName;
		}

		public String getAgainstAccountName() {
			return againstAccountName;
		}

		public void setAgainstAccount(String againstAccountName) {
			this.againstAccountName = againstAccountName;
		}

		public Long getItsContractId() {
			return itsContractId;
		}

		public void setItsContract(Long itsContractId) {
			this.itsContractId = itsContractId;
		}

		public String getForBillingTerms() {
			return forBillingTerms;
		}

		public void setForBillingTerms(String forBillingTerms) {
			this.forBillingTerms = forBillingTerms;
		}

		public String getBillingStartMonth() {
			return billingStartMonth;
		}

		public void setBillingStartMonth(String billingStartMonth) {
			this.billingStartMonth = billingStartMonth;
		}

		public String getBillingEndMonth() {
			return billingEndMonth;
		}

		public void setBillingEndMonth(String billingEndMonth) {
			this.billingEndMonth = billingEndMonth;
		}

		public String getBillingDate() {
			return billingDate;
		}

		public void setBillingDate(String billingDate) {
			this.billingDate = billingDate;
		}

		public String getBillingResult() {
			return billingResult;
		}

		public void setBillingResult(String billingResult) {
			this.billingResult = billingResult;
		}
		
		public String getDomainName() {
			return domainName;
		}

		public void setDomainName(String domainName) {
			this.domainName = domainName;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}
	}

	// Out class for output
	public static class BillingOperation_Out extends BillingOperation_In {
		Long id;
		String createdOn;
		String modifiedOn;

		public BillingOperation_Out() {
			super();
		}

		public BillingOperation_Out(String billingDate, String billingResult, String raisedbyAccountName,
				String againstAccountName, Long itsContractId, String forBillingTerms, String billingStartMonth,
				 String billingEndMonth, String domainName, String userName, String createdOn, String modifiedOn, Long id) {
			super(billingDate, billingResult, raisedbyAccountName, againstAccountName, itsContractId, forBillingTerms, 
					billingStartMonth, billingEndMonth, domainName, userName);
			this.id = id;
			this.createdOn = createdOn;
			this.modifiedOn = modifiedOn;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toJSON()
	 */
	@Override
	public BillingOperation_Out toJSON() {
		return new BillingOperation_Out(billingDate.toString(), billingResult, raisedbyAccount.getName(), againstAccount.getName(), 
				itsContract.getId(), forBillingTerms, billingStartMonth, billingEndMonth, itsDomain.getName(), itsUser.getUsername(), 
				createdOn.toString(), modifiedOn.toString(), id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#fromJSON(java.lang.Object)
	 */
	@Override
	public void fromJSON(BillingOperation_In json) {
		this.billingDate =  json.getBillingDate() != null ? Utils.getDate(json.getBillingDate()) : null;
		this.billingResult =  json.billingResult;
		this.raisedbyAccount = json.getRaisedbyAccountName() != null ? BillingAccount.getByName(json.getRaisedbyAccountName()) : null;
		this.againstAccount = json.getAgainstAccountName() != null ? BillingAccount.getByName(json.getAgainstAccountName()) : null;
		this.itsContract = json.getItsContractId() != null ? BillingContract.find(json.getItsContractId()) : null;
		this.forBillingTerms =  json.getForBillingTerms();
		this.billingStartMonth = json.getBillingStartMonth();
		this.billingEndMonth = json.getBillingEndMonth();
		this.itsDomain = json.getDomainName() != null ? Domain.getByName(json.getDomainName()) : null;
		this.itsUser = json.getUserName() != null ? User.getByUsername(json.getUserName()) : null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}

	// ---------------------------------------------
	// SECTION 5 - Query and Class specific finders and static utility methods
	// ---------------------------------------------
	public static BillingOperation findObjectById(Long id) {
		return find(id);
	}
	
	public static List<BillingOperation> getAllObjects(int pageNo, int pageSize) {
		CarIQSimpleQuery<BillingOperation> qry = new CarIQSimpleQuery<BillingOperation>("getAllObjects", entityManager(), BillingOperation.class);
		qry.setPaging(pageNo, pageSize);

		return qry.getResultList();
	}

	public static List<BillingOperation> getByBillingMonths(String months) {
		CarIQSimpleQuery<BillingOperation> qry = new CarIQSimpleQuery<BillingOperation>("getByBillingDate", entityManager(), BillingOperation.class);
		qry.addRawCondition("forBillingMonths like '%" + months + "%'");

		return qry.getResultList();
	}
	
	public static List<BillingOperation> getByBillingMonthsTerms(String startMonth, String endMonth, String terms) {
		CarIQSimpleQuery<BillingOperation> qry = new CarIQSimpleQuery<BillingOperation>("getByBillingDate", entityManager(), BillingOperation.class);
		qry.addRawCondition("billingStartMonth like '%" + startMonth + "%'");
		qry.addRawCondition("billingEndMonth like '%" + endMonth + "%'");
		qry.addRawCondition("forBillingTerms like '%" + terms + "%'");

		return qry.getResultList();
	}
	
	public static List<BillingOperation> getByBillingContract(BillingContract contract) {
		CarIQSimpleQuery<BillingOperation> qry = new CarIQSimpleQuery<BillingOperation>("getByBillingContract", entityManager(), BillingOperation.class);
		qry.addCondition("itsContract", CarIQSimpleQuery.EQ, contract);

		return qry.getResultList();
	}
	
	public static List<BillingOperation> getByRaisedAgainstContract(BillingAccount account) {
		CarIQSimpleQuery<BillingOperation> qry = new CarIQSimpleQuery<BillingOperation>("getByRaisedAgainstContract", entityManager(), BillingOperation.class);
		qry.addCondition("againstContract", CarIQSimpleQuery.EQ, account);

		return qry.getResultList();
	}

	// Only definitionJSON can be updated
	public void updateResult(String billingResult) {
		if (billingResult != null) {
			this.billingResult = billingResult;
			this.modifiedOn = new Date();
		}
	}
}