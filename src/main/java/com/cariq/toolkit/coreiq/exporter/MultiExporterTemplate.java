/**
 * 
 */
package com.cariq.toolkit.coreiq.exporter;

import java.util.ArrayList;
import java.util.List;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.SimplePOJOs.CountPOJO;
import com.cariq.toolkit.utils.Utils;

/**
 * @author hrishi
 *
 */
public class MultiExporterTemplate extends CarIQExporterTemplate {
	List<CarIQExporter> childExporters;

	/**
	 * @param name
	 * @param description
	 */
	public MultiExporterTemplate(String name, String description, List<CarIQExporter> exporters) {
		super(name, description);
		childExporters = exporters;

		// Accumulate QueryTypes and Attributes
		List<CarIQQueryDefinition> queryDefs = null;
		List<String> attributes = new ArrayList<String>();
		// loop through exporters and prepare a fresh definition
		for (CarIQExporter carIQExporter : exporters) {
			attributes.addAll(carIQExporter.getExporterDefinition().getExportAttrDefinition());

			// Take only the first querydef - assuming it is same for all
			if (queryDefs == null)
				queryDefs = carIQExporter.getExporterDefinition().getQueryDefinition();
		}

		addExportAttributes(attributes);
		setQueryDefinitions(queryDefs);
	}

	@Override
	public List<CarIQExporter> getChildExporters() {
		return childExporters;
	}

	static class ExporterSize {
		CarIQExporter exporter;
		int size;

		/**
		 * @param exporter
		 * @param size
		 */
		public ExporterSize(CarIQExporter exporter, int size) {
			this.exporter = exporter;
			this.size = size;
		}

		public CarIQExporter getExporter() {
			return exporter;
		}

		public int getSize() {
			return size;
		}
	}

	/**
	 * ExporterSizeStack will contain exporters and size of each
	 * 
	 * @author hrishi
	 *
	 */
	static class ExporterSizeStack extends ArrayList<ExporterSize> {
		private static int toExporterFrame(int overallIndex, int marker, int minIndex, int maxIndex) {
			int indx = overallIndex - marker;
			indx = Math.min(indx, maxIndex);
			indx = Math.max(indx, minIndex);

			return indx;
		}

		private static int toOverallFrame(int exporterIndex, int marker) {
			int indx = exporterIndex + marker;
			//			indx = Math.min(indx, maxIndex);
			//			indx = Math.max(indx, minIndex);

			return indx;
		}

		/**
		 * Traverse the stack
		 * 
		 * @param pageNo
		 * @param pageSize
		 * @return
		 */
		public List<ExporterRange> getExporterRanges(int pageNo, int pageSize) {
			List<ExporterRange> retval = new ArrayList<ExporterRange>();

			// Stack contains all the exporters with the size of each.
			// {Exp1, 20}, {Exp2, 25}, {Exp3, 28}

			// from pageNo and pageSize, we get the start and end index.
			int overallStartIndex = Utils.validatePagination(pageSize, pageNo);
			int overallEndIndex = overallStartIndex + pageSize - 1;

			// Now we should overlay the startIndex to endIndex on the ExporterRange
			int markerIndex = 0;
			for (int i = 0; i < size(); i++) {
				ExporterSize expSize = get(i);
				int size = expSize.getSize();

				// evaluate start and End indexes of current exporter in overall 
				int expStartIndex = 0, expEndIndex = (size - 1);

				if (overallStartIndex > toOverallFrame(expStartIndex, markerIndex)) {
					// update marker
					markerIndex += size;
					continue;
				}

				if (overallEndIndex < toOverallFrame(expEndIndex, markerIndex)) {
					// update marker
					markerIndex += size;
					continue;
				}

				retval.add(new ExporterRange(expSize.getExporter(),
						toExporterFrame(overallStartIndex, markerIndex, expStartIndex, expEndIndex),
						toExporterFrame(overallEndIndex, markerIndex, expEndIndex, expEndIndex)));

				// update marker
				markerIndex += size;
			}

			return retval;
		}
	}

	static class ExporterRange {
		CarIQExporter exporter;
		int startIndex, endIndex;

		/**
		 * @param exporter
		 * @param startIndex
		 * @param endIndex
		 */
		public ExporterRange(CarIQExporter exporter, int startIndex, int endIndex) {
			this.exporter = exporter;
			this.startIndex = startIndex;
			this.endIndex = endIndex;
		}

		public CarIQExporter getExporter() {
			return exporter;
		}

		public int getStartIndex() {
			return startIndex;
		}

		public int getEndIndex() {
			return endIndex;
		}
	}

	/**
	 * From the list of exporters and given pageNo and pageSize of final result, put
	 * in exporterRange in.
	 * 
	 * @param exporters
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	private static List<ExporterRange> evaluateExporterRanges(List<CarIQExporter> exporters, String queryType,
			GenericJSON json, int pageNo, int pageSize) {
		List<ExporterRange> retval = new ArrayList<ExporterRange>();

		// Get the stack ready
		ExporterSizeStack stack = evaluateExporterSizeStack(exporters, queryType, json);

		return stack.getExporterRanges(pageNo, pageSize);
	}

	/**
	 * @param exporters
	 * @param queryType
	 * @param json
	 * @return
	 */
	private static ExporterSizeStack evaluateExporterSizeStack(List<CarIQExporter> exporters, String queryType,
			GenericJSON json) {
		ExporterSizeStack stack = new ExporterSizeStack();
		for (CarIQExporter exporter : exporters) {
			int size = 50;
			CountPOJO countPOJO = exporter.getExportCount(queryType, json);
			if (countPOJO.getCount() > 0)
				size = countPOJO.getCount();

			stack.add(new ExporterSize(exporter, size));
		}

		return stack;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.exporter.CarIQExporterTemplate#doExport(java.lang.
	 * String, com.cariq.www.utils.GenericJSON, int, int)
	 */
	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		// Loop through all the Attributes (union of all)
		// Run exporter one by one - based on maxCount of each - if no maxCount, just take maxCount as 100
		List<String> attributes = super.getExporterDefinition().getExportAttrDefinition();
		List<ExporterRange> exporters = evaluateExporterRanges(childExporters, queryType, json, pageNo, pageSize);

		// make return json
		List<GenericJSON> retval = new ArrayList<GenericJSON>();
		// iterate over the Exporters
		for (ExporterRange exporterRange : exporters) {
			// iterate over the ranges for each exporter
			List<GenericJSON> exportedRecords = exporterRange.getExporter().exportRecords(queryType, json,
					exporterRange.getStartIndex(), exporterRange.getEndIndex());
			List<GenericJSON> returnRecords = marshallAttributes(exportedRecords, attributes);
			retval.addAll(returnRecords);
		}

		return retval;
	}

	/**
	 * @param exportedRecords
	 * @param attributes
	 * @return
	 */
	private static List<GenericJSON> marshallAttributes(List<GenericJSON> records, List<String> attributes) {
		List<GenericJSON> retArr = new ArrayList<GenericJSON>();

		// iterate over all records
		for (GenericJSON json : records) {
			GenericJSON retval = new GenericJSON();

			// Fill in all attributes - if present in records, great, else empty
			for (String attr : attributes) {
				Object val = json.get(attr);
				retval.put(attr, val == null ? "" : val);
			}

			retArr.add(retval);
		}

		return retArr;
	}

}
