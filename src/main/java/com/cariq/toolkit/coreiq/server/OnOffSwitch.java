package com.cariq.toolkit.coreiq.server;

public class OnOffSwitch {

	private String switchName; // one of WORKER, SQL, REST, API, ALL
	private Boolean isOn; // If not provided - the one which picks the request

	public OnOffSwitch() {}

	
	public OnOffSwitch(String auditType, Boolean isOn) {
		super();
		this.switchName = auditType;
		this.isOn = isOn;
	}

	public OnOffSwitch(String switchName) {
		this(switchName, false); // by default off
	}


	public String getSwitchName() {
		return switchName;
	}

	public void setSwitchName(String auditType) {
		this.switchName = auditType;
	}

	public Boolean getIsOn() {
		return isOn;
	}

	public synchronized void setIsOn(Boolean isOn) {
		this.isOn = isOn;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((switchName == null) ? 0 : switchName.hashCode());
		result = prime * result + ((isOn == null) ? 0 : isOn.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OnOffSwitch other = (OnOffSwitch) obj;
		if (switchName == null) {
			if (other.switchName != null)
				return false;
		} else if (!switchName.equals(other.switchName))
			return false;
		if (isOn == null) {
			if (other.isOn != null)
				return false;
		} else if (!isOn.equals(other.isOn))
			return false;
		return true;
	}	
}
