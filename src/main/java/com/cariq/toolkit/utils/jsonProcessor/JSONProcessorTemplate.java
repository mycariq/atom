/**
 * 
 */
package com.cariq.toolkit.utils.jsonProcessor;

import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * @author hrishi
 *
 */
public abstract class JSONProcessorTemplate implements JSONProcessor {

	protected abstract void doProcess(JSONProcessorContext context, JSONPath node, Object obj);

	@Override
	public void process(JSONProcessorContext context, JSONPath node, Object obj) {
		try (ProfilePoint _JSONProcessor_Process = ProfilePoint.profileAction("ProfAction_JSONProcessor_Process")) {
			doProcess(context, node, obj);
		}
	}

}
