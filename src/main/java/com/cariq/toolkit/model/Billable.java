package com.cariq.toolkit.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.format.annotation.DateTimeFormat;

import com.cariq.toolkit.coreiq.CarIQSimpleQuery;
import com.cariq.toolkit.coreiq.model.PersistentObject;
import com.cariq.toolkit.model.Billable.Billable_In;
import com.cariq.toolkit.model.Billable.Billable_Out;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.GenericJsonable;
import com.cariq.toolkit.utils.IdJSON;
import com.cariq.toolkit.utils.JSONable;
import com.cariq.toolkit.utils.Utils;
import com.google.common.base.Strings;

/**
 * This class implements "billable"
 * Billable is the specific entity that is charged (accounted) in the billing operation. eg: car, policy, subscription, device etc.
 * @author amita
 *
 */

@Entity
@Configurable
public class Billable extends PersistentObject<Billable> implements JSONable<Billable_In, Billable_Out, IdJSON>, GenericJsonable {

	// SECTION 1 -Code for Persistence
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;

	@Version
	Integer version;

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public Integer getVersion() {
		return version;
	}

	@Override
	public void setVersion(Integer version) {
		this.version = version;
	}

	// Entity Manager
	@PersistenceContext
	transient EntityManager entityManager;

	public static final EntityManager entityManager() {
		EntityManager em = new Billable().entityManager;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public EntityManager getEntityManager() {
		if (this.entityManager == null)
			this.entityManager = entityManager();
		return entityManager;
	}

	/**
	 * @param id
	 * @return
	 */
	protected static Billable find(Long id) {
		return entityManager().find(Billable.class, id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.coreiq.model.PersistentObject#findById(java.lang.Long)
	 */
	@Override
	public Billable findById(Long id) {
		return find(id);
	}

	// ---------------------------------------------
	// SECTION 2 - Model properties
	// ---------------------------------------------

	/**
	 * Model properties
	 */
	@NotNull(message = "@NotNull type")
	String type;

	Long identity;
	
	@NotNull(message = "@NotNull signature")
	@Column(unique = true)
	String signature;

	@Size(max = 4000)
	String infoJson;
	
	@Size(max = 1024)
	String note;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date createdOn;

	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	Date modifiedOn;

	// Default constructor is a must!
	public Billable() {
		super();
	}

	/**
	 * 
	 * @param type
	 * @param identity
	 * @param signature
	 * @param note
	 * @param createdOn
	 * @param modifiedOn
	 */
	public Billable(String type, Long identity, String signature, String infoJson, String note, Date createdOn, Date modifiedOn) {
		super();
		this.type = type;
		this.identity = identity;
		this.signature = signature;
		this.infoJson = infoJson;
		this.note = note;
		this.createdOn = createdOn;
		this.modifiedOn = modifiedOn;
	}

	/**
	 * Getters - Setters
	 */
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getIdentity() {
		return identity;
	}

	public void setIdentity(Long identity) {
		this.identity = identity;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getInfoJson() {
		return infoJson;
	}

	public void setInfoJson(String infoJson) {
		this.note = infoJson;
	}
	
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	
	// ---------------------------------------------
	// SECION 3 - POJO Utility methods
	// ---------------------------------------------


	// Implementation of toString and hashCode and equals are advisable for all
	// Model classes
	@Override
	public String toString() {
		return "Billable [id=" + id + ", version=" + version + ", type=" + type + ", identity=" + identity + ", signature=" + signature
				+ ", note=" + note + ", infoJson=" + infoJson + ", createdOn=" + createdOn + ", modifiedOn=" + modifiedOn + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((identity == null) ? 0 : identity.hashCode());
		result = prime * result + ((signature == null) ? 0 : signature.hashCode());
		result = prime * result + ((infoJson == null) ? 0 : infoJson.hashCode());
		result = prime * result + ((note == null) ? 0 : note.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((modifiedOn == null) ? 0 : modifiedOn.hashCode());
		result = prime * result + ((version == null) ? 0 : version.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Billable other = (Billable) obj;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (identity == null) {
			if (other.identity != null)
				return false;
		} else if (!identity.equals(other.identity))
			return false;
		if (signature == null) {
			if (other.signature != null)
				return false;
		} else if (!signature.equals(other.signature))
			return false;
		if (infoJson == null) {
			if (other.infoJson != null)
				return false;
		} else if (!infoJson.equals(other.infoJson))
			return false;
		if (note == null) {
			if (other.note != null)
				return false;
		} else if (!note.equals(other.note))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (version == null) {
			if (other.version != null)
				return false;
		} else if (!version.equals(other.version))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (modifiedOn == null) {
			if (other.modifiedOn != null)
				return false;
		} else if (!modifiedOn.equals(other.modifiedOn))
			return false;
		return true;
	}

	// ---------------------------------------------
	// SECTION - 4 - JSONable Interface implementation
	// ---------------------------------------------
	// Finally Implementation of JSONable interface - to support REST API
	// interaction
	
	// In Class (for construction)
	public static class Billable_In {
		String type;
		Long identity;
		String signature;
		String infoJson;
		String note;
		
		public Billable_In() {
			super();
		}

		/**
		 * 
		 * @param type
		 * @param identity
		 * @param signature
		 * @param infoJson
		 * @param note
		 */
		public Billable_In(String type, Long identity, String signature, String infoJson, String note) {
			super();
			this.type = type;
			this.identity = identity;
			this.signature = signature;
			this.infoJson = infoJson;
			this.note = note;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public Long getIdentity() {
			return identity;
		}

		public void setIdentity(Long identity) {
			this.identity = identity;
		}

		public String getSignature() {
			return signature;
		}

		public void setSignature(String signature) {
			this.signature = signature;
		}

		public String getInfoJson() {
			return infoJson;
		}

		public void setInfoJson(String infoJson) {
			this.infoJson = infoJson;
		}

		public String getNote() {
			return note;
		}

		public void setNote(String note) {
			this.note = note;
		}
	}

	// Out class for output
	public static class Billable_Out extends Billable_In {
		Long id;
		String createdOn;
		String modifiedOn;

		public Billable_Out() {
			super();
		}

		public Billable_Out(String type, Long identity, String signature, String infoJson, 
				String note, String createdOn, String modifiedOn, Long id) {
			super(type, identity, signature, infoJson, note);
			this.id = id;
			this.createdOn = createdOn;
			this.modifiedOn = modifiedOn;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getCreatedOn() {
			return createdOn;
		}

		public void setCreatedOn(String createdOn) {
			this.createdOn = createdOn;
		}

		public String getModifiedOn() {
			return modifiedOn;
		}

		public void setModifiedOn(String modifiedOn) {
			this.modifiedOn = modifiedOn;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toJSON()
	 */
	@Override
	public Billable_Out toJSON() {
		return new Billable_Out(type, identity, signature, infoJson, note, createdOn.toString(), modifiedOn.toString(), id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#fromJSON(java.lang.Object)
	 */
	@Override
	public void fromJSON(Billable_In json) {
		this.type =  json.type;
		this.identity =  json.identity;
		this.signature =  json.signature;
		this.infoJson = json.infoJson;
		this.note = json.note;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cariq.www.utils.json.JSONable#toLightJSON()
	 */
	@Override
	public IdJSON toLightJSON() {
		return new IdJSON(this.getId());
	}
	
	@Override
	public GenericJSON toGenericJSON() {
		return GenericJSON.build("Type", type, "Identity", identity, "Signature", signature, "Info", Utils.getJSonObject(infoJson, GenericJSON.class), 
				"Note", note, "CreatedOn", createdOn, "ModifiedOn", modifiedOn, "Id", id);
	}
	
	@Override
	public void fromGenricJSON(GenericJSON json) {
		Utils.notImplementedException("Billable.fromGenricJSON");
	}
	
	// ---------------------------------------------
	// SECTION 5 - Query and Class specific finders and static utility methods
	// ---------------------------------------------
	public static Billable findObjectById(Long id) {
		return find(id);
	}
	
	public static List<Billable> getAllObjects(int pageNo, int pageSize) {
		CarIQSimpleQuery<Billable> qry = new CarIQSimpleQuery<Billable>("getAllObjects", entityManager(), Billable.class);
		qry.setPaging(pageNo, pageSize);

		return qry.getResultList();
	}
	
	public static Billable getByTypeId(String type, Long id) {
		CarIQSimpleQuery<Billable> qry = new CarIQSimpleQuery<Billable>("getByTypeId", entityManager(), Billable.class);
		qry.addCondition("type", "=", type);
		qry.addCondition("identity", "=", id);

		if (qry.getResultList().isEmpty())
			return null;
		return qry.getSingleResult();
	}

	public static Billable getByTypeSignature(String type, String signature) {
		CarIQSimpleQuery<Billable> qry = new CarIQSimpleQuery<Billable>("getByTypeSignature", entityManager(), Billable.class);
		qry.addCondition("type", "=", type);
		qry.addCondition("signature", "=", signature);

		if (qry.getResultList().isEmpty())
			return null;
		return qry.getSingleResult();
	}

	// Only info and note can be updated
	public void update(String infoJson, String note) {
		if (infoJson != null)
			this.infoJson = infoJson;
		
		if (note != null)
			this.note = note;
		
		this.modifiedOn = new Date();
	}
	
	public static Billable getOrCreateBillable(Billable_In billableIn) {
		Billable billable = Billable.getByTypeSignature(billableIn.getType(), billableIn.getSignature());
		
		if (null == billable) {
			billable = new Billable();
			billable.fromJSON(billableIn);
			billable.setCreatedOn(new Date());
			billable.setModifiedOn(new Date());
			billable.persist();
		}
		
		return billable;
	}
}