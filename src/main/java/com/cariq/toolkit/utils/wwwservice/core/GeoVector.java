/**
 * 
 */
package com.cariq.toolkit.utils.wwwservice.core;

import com.cariq.toolkit.utils.Pair;

/**
 * @author hrishi
 *
 */
public class GeoVector {
	static final double EPSILON_DISTANCE = 0.05; // 50 meters
	GeoTile first, second;

	/**
	 * @param first
	 * @param second
	 */
	public GeoVector(GeoTile first, GeoTile second) {
		this.first = first;
		this.second = second;
	}
	
	/**
	 * @param first
	 * @param second
	 */
	public GeoVector(double latCoeff, double longCoeff) {
		first = new GeoTile(0,0);
		second = new GeoTile(latCoeff, longCoeff);
	}

	public GeoTile getFirst() {
		return first;
	}

	public GeoTile getSecond() {
		return second;
	}
	
	/**
	 * Vector cross product (matrix multiply) of this vector with other
	 * This is actually a vector, but in 2D, cross product vector is always along Z
	 * We don't have GeoVector along Z, so, this is only the value - with sign
	 * @param second
	 * @return
	 */
	public double cross(GeoVector other) {
		// first = a1*i + b1*j
		// second = a2*i + b2*j
		// first x second = a1*b1-a2*b1

		Pair<Double, Double> firstCoeffs = getVectorCoefficients();
		Pair<Double, Double> otherCoeffs = other.getVectorCoefficients();
		
		return firstCoeffs.getFirst()*otherCoeffs.getSecond() - firstCoeffs.getSecond()*otherCoeffs.getFirst();
	}
	
	/**
	 * @return
	 */
	private Pair<Double, Double> getVectorCoefficients() {
		double latCoeff = second.getLatitude() - first.getLatitude();
		double longCoeff = second.getLongitude() - first.getLongitude();
		
		return new Pair<Double, Double>(latCoeff, longCoeff);
	}

	/**
	 * Vector dot product of this vector with other return scalar value in kilometers
	 * @param other
	 * @return
	 */
	public double dot(GeoVector other) {
		Pair<Double, Double> firstCoeffs = getVectorCoefficients();
		Pair<Double, Double> otherCoeffs = other.getVectorCoefficients();

		// dot product is a1b1 + a2b2 (+ a3b3)
		return firstCoeffs.getFirst() * otherCoeffs.getFirst() + firstCoeffs.getSecond()*otherCoeffs.getSecond();
	}
	
	/**
	 * Get the magnitude of the vector in kilometers
	 * @return
	 */
	public double getMagnitude() {
		return first.getDistanceFrom(second);
	}
	
	/**
	 * Get Unit Vector
	 * @return
	 */
	public GeoVector getUnitVector() {
		Pair<Double, Double> coeffs = this.getVectorCoefficients();
		
		double magnitude = getMagnitude();
		
		if (magnitude < EPSILON_DISTANCE)
			return null; // callers beware!
		
		return new GeoVector(new GeoTile(0,0), new GeoTile(coeffs.getFirst()/magnitude, coeffs.getSecond()/magnitude)); 
	}
	
	/**
	 * Check if two Geovectors are orthogonal
	 * @param other
	 * @return
	 */
	boolean isOrthogonal(GeoVector other) {
		return dot(other) < EPSILON_DISTANCE;
	}

	@Override
	public String toString() {
		Pair<Double, Double> dcs = getVectorCoefficients();
		return "GeoVector [" + dcs.getFirst() + "i + " + dcs.getSecond() + "j]";
	}

	/**
	 * @param second2
	 * @return
	 */
	public GeoVector plus(GeoVector other) {
		return new GeoVector(getVectorCoefficients().getFirst() + other.getVectorCoefficients().getFirst(), getVectorCoefficients().getSecond() + other.getVectorCoefficients().getSecond());
	}

	/**
	 * @param second2
	 * @return
	 */
	public GeoVector minus(GeoVector other) {
		return new GeoVector(getVectorCoefficients().getFirst() - other.getVectorCoefficients().getFirst(), getVectorCoefficients().getSecond() - other.getVectorCoefficients().getSecond());
	}
}
