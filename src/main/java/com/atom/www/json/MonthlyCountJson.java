package com.atom.www.json;

/**
 * The Class MonthlyCountJson.
 */
public class MonthlyCountJson {

	/** The Constant COUNT. */
	public static final String MONTH = "month", YEAR = "year", COUNT = "count";

	/** The month. */
	private Integer month;

	/** The year. */
	private Integer year;

	/** The count. */
	private Long count;

	
	/**
	 * Instantiates a new monthly count json.
	 *
	 * @param month the month
	 * @param year the year
	 * @param count the count
	 */
	public MonthlyCountJson(Integer month, Integer year, Long count) {
		super();
		this.month = month;
		this.year = year;
		this.count = count;
	}

	/**
	 * Gets the month.
	 *
	 * @return the month
	 */
	public Integer getMonth() {
		return month;
	}

	/**
	 * Gets the year.
	 *
	 * @return the year
	 */
	public Integer getYear() {
		return year;
	}

	/**
	 * Gets the count.
	 *
	 * @return the count
	 */
	public Long getCount() {
		return count;
	}
}