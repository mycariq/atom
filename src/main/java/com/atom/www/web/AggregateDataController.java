package com.atom.www.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.atom.www.json.PolicyRenewalPercentageJSON;
import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.publicapi.CarIQPublicAPIParameter;
import com.cariq.toolkit.service.AggregateDataService;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.utils.GenericJSON;

/**
 * @author nitin
 *
 */
@CarIQPublicAPI(name = "AggregateData", description = "Aggregate data")
@RequestMapping("/aggregateData")
@Controller
public class AggregateDataController {

	@Autowired
	AggregateDataService aggregateDataService;

	@CarIQPublicAPIMethod(description = "Get car related counts", responseClass = GenericJSON.class, internal = true)
	@RequestMapping(value = "counts/{domain}", method = RequestMethod.GET)
	@ResponseBody
	public GenericJSON getTotalCounts(
			@CarIQPublicAPIParameter(name = "domain", description = "the domain") @PathVariable String domain) {
		try (CarIQAPI _getResource = new CarIQAPI("AggregateData_getTotalCounts")) {
			return aggregateDataService.getTotalCounts(domain);
		}
	}

	@CarIQPublicAPIMethod(description = "Get statistics data", responseClass = GenericJSON.class, internal = true)
	@RequestMapping(value = "counts/{domain}/{context}", method = RequestMethod.GET)
	@ResponseBody
	public List<GenericJSON> getStatisticsData(
			@CarIQPublicAPIParameter(name = "domain", description = "the domain") @PathVariable String domain,
			@CarIQPublicAPIParameter(name = "context", description = "the context") @PathVariable String context) {
		try (CarIQAPI _getResource = new CarIQAPI("AggregateData_getTotalCounts")) {
			return aggregateDataService.getStatisticsData(domain,context);
		}
	}
	
	/**
	 * Gets the policy renewal percentage.
	 *
	 * @param domain the domain
	 * @param fromDate the from date
	 * @param toDate the to date
	 * @return the policy renewal percentage
	 */
	@CarIQPublicAPIMethod(description = "Get policy renewal percentage", responseClass = PolicyRenewalPercentageJSON.class, internal = true)
	@RequestMapping(value = "/policyRenewalPercentage/{domain}/{fromDate}/{toDate}", method = RequestMethod.GET)
	@ResponseBody
	public PolicyRenewalPercentageJSON getPolicyRenewalPercentage(
			@CarIQPublicAPIParameter(name = "domain", description = "the domain") @PathVariable String domain,
			@CarIQPublicAPIParameter(name = "fromDate", description = "the fromDate") @PathVariable("fromDate") String fromDate,
			@CarIQPublicAPIParameter(name = "toDate", description = "the toDate") @PathVariable("toDate") String toDate) {
		try (CarIQAPI _getPolicyRenewalPercentage = new CarIQAPI("AggregateData_getPolicyRenewalPercentage")) {
			return aggregateDataService.getPolicyRenewalPercentage(domain, fromDate, toDate);
		}
	}
}
