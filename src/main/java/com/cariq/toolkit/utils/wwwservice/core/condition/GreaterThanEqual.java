package com.cariq.toolkit.utils.wwwservice.core.condition;

import com.cariq.toolkit.utils.wwwservice.core.ArithmeticCondition;
import com.cariq.toolkit.utils.wwwservice.core.Condition.ConditionType;
import com.cariq.toolkit.utils.wwwservice.core.ParameterDef;

public class GreaterThanEqual extends ArithmeticCondition {
	public GreaterThanEqual(ParameterDef paramdef, Comparable<?> value) {
		super(paramdef, value);
	}

	@Override
	public ConditionType getType() {
		return ConditionType.GreaterThanEqual;
	}

	/**
	 * Return if parameterValue is greater than conditionValue
	 */
	@Override
	protected boolean isReallyTrue(Comparable conditionValue,
			Comparable<?> parameterValue) {
		return (conditionValue.compareTo(parameterValue) < 0);
	}

}
