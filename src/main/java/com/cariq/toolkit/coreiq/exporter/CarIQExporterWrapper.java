package com.cariq.toolkit.coreiq.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.atom.www.helper.ConfigHelper;
import com.atom.www.helper.DomainHelper;
import com.cariq.toolkit.coreiq.exporter.CarIQExporter;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterDefinition;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterRegistry;
import com.cariq.toolkit.coreiq.exporter.CarIQQueryDefinition;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.service.DomainAccessService;
import com.cariq.toolkit.service.DomainService;
import com.cariq.toolkit.model.Domain.Domain_Out;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.HtmlValueIdentityRenderer;
import com.cariq.toolkit.utils.HtmlValueRenderer;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.utils.service.CarIQRestClientService;

// TODO: Auto-generated Javadoc
/**
 * The Class CariqReporterWrapper. This wrapper connects with CarIQ, exports data and definition in such 
 * way that all other Exporter communicates with it as normal native exporter.
 * 
 * Client of this need to tell which CarIQ domain connection has to made using inputJSON and 
 * with key KEY_DOMAIN_NAME. If data has to be retrived from multiple domain then comma seperated 
 * domain names can be provided.
 * 
 * If CarIQ layer needs additional information in inputJSON it will throw an exception with BadRequest,then
 * same should be provided in input json.
 * 
 */
@Configurable
public class CarIQExporterWrapper {

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("CarIQExporterWrapper");
	
	public static final String KEY_DOMAIN_NAME = "domain";
	
	public static final String USERNAME = "username";
	
	/** The name. */
	private String name;
	
	/** The description. */
	private String description;
	
	/** The is init. */
	private boolean isInit = false;
	
	/** The def. */
	private CarIQExporterDefinition def;
	
	/** The helper. */
	private ConfigHelper configHelper;
	
	/** The rest service. */
	@Autowired
	CarIQRestClientService restService;
	
	private DomainHelper domainHelper;
	
	/** Current logged in user ***/
	private User user;

	/**
	 * Instantiates a new cariq reporter wrapper.
	 *
	 * @param name the name
	 * @param description the description
	 */
	public CarIQExporterWrapper(String name, String description) {
		this.name = name;
		this.description = description;
		this.configHelper = new ConfigHelper();
		this.domainHelper = new DomainHelper();
	}

	/**
	 * Instantiates a new cariq reporter wrapper.
	 *
	 * @param name the name
	 */
	public CarIQExporterWrapper(String name){
		this(name, "This is a " + name);
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		if(def == null)
			return description;
		return def.getDescription();
	}

	/**
	 * Gets the exporter definition.
	 *
	 * @return the exporter definition
	 */
	public CarIQExporterDefinition getExporterDefinition() {
		CarIQExporterDefinition definition = getWrappedExporterDefintion();
		definition.add(CarIQExporterDefinition.EXPORT_PARAM, USERNAME);
		return definition;
	}

	/**
	 * Checks if is authorised.
	 *
	 * @return true, if is authorised
	 */
	public boolean isAuthorised() {
		return def.isAuthorized();
	}

	/**
	 * Gets the html renderer.
	 *
	 * @return the html renderer
	 */
	public HtmlValueRenderer getHtmlRenderer() {
		return new HtmlValueIdentityRenderer();
	}

	/**
	 * Do export.
	 *
	 * @param queryType the query type
	 * @param json the json
	 * @param pageNo the page no
	 * @param pageSize the page size
	 * @return the list
	 */
	public List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		logger.debug("queryType : " + queryType + ", json : " + json);
		
		//List to hold data from every domain which is accessible to this user;
		List<GenericJSON> outputJSON = new ArrayList<GenericJSON>();
		
		//get comma seperated domains
		String commaSeperatedDomains = Utils.getValue(json, KEY_DOMAIN_NAME);
		
		//If comma seperated domain is null then find domain of logged in user.
		String[] domains = commaSeperatedDomains != null ? commaSeperatedDomains.split(",") : 
			domainHelper.getDomainOfLoggedInUser(commaSeperatedDomains).getName().split(",");
		
		//Iterate over all domains and call API for each domain
		for(String domainName : domains) {
			domainName = Utils.removeWhiteSpaces(domainName);
			Domain_Out domain = null;
			if ((domain = domainHelper.getDomainOfLoggedInUser(domainName)) == null) {
				Utils.handleException("Domain not found for user");
			}
			//execute API for given domain and retrive data
			List<GenericJSON> response = executeAPI(queryType, json, pageNo, pageSize, domain);
			outputJSON.addAll(response);
		}		
		return outputJSON;
	}
	
	/**
	 * Execute API and retrive data from API for given domain
	 * 
	 * @param queryType
	 * @param json
	 * @param pageNo
	 * @param pageSize
	 * @param domainName
	 * @return
	 */
	private List<GenericJSON> executeAPI(String queryType, GenericJSON json, int pageNo, int pageSize, Domain_Out domain) {
		
		List<GenericJSON> outputJSON = new ArrayList<GenericJSON>();

		try (ProfilePoint _wrapper_doExport = ProfilePoint
				.profileActivity("ProfActivity_wrapper_doExport" + this.getClass().getName())) {

			try (ProfilePoint _doExport_call_rest_api = ProfilePoint
					.profileAction("ProfAction_doExport_call_rest_api")) {
				// REST API call - for given Exporter with JSON as input
				String url = "Cariq/admin/exporter/exportMultiple/" + getName() + "/" + queryType + "/" + pageNo + "/"
						+ pageSize;
				logger.debug("Reporter Url : " + configHelper.makeUrl(url, domain.getUrl()));
				logger.debug("Reporter Input Json : " + json);
				GenericJSON[] response = restService.post(json, configHelper.makeUrl(url, domain.getUrl()),
						configHelper.getSecurityToken(), GenericJSON[].class);
				if (response != null) {
					logger.debug("Collecting " + response.length + " object from " + domain.getUrl() + " domain");
					// Before adding response to final output list we need to
					// attach domain id to every object
					List<GenericJSON> domainAttachedList = addDomainToObject(response, domain);
					outputJSON.addAll(domainAttachedList);
				}
			}
		}
		return outputJSON;
	}

	/**
	 * We need to associate to which domain this object associate for further
	 * operations of this object regarding domain of this object
	 * 
	 * @param response
	 * @param domain
	 */
	private List<GenericJSON> addDomainToObject(GenericJSON[] response, Domain_Out domain) {
		List<GenericJSON> domainAttachedList = new ArrayList<GenericJSON>();
		for(int i = 0; i < response.length; i++) {
			GenericJSON json = response[i];
			json.put("domainId", domain.getId());
			json.put("domainName", domain.getName());
			domainAttachedList.add(json);
		}
		return domainAttachedList;
	}

	/**
	 * Get exporter definition from CarIQ.
	 *
	 * @return the wrapped exporter defintion
	 */
	private CarIQExporterDefinition getWrappedExporterDefintion() {
		try(ProfilePoint _originalExporterDefinitionCall = ProfilePoint.profileAction("ProfAction__originalExporterDefinitionCall")) {
			String url = "Cariq/admin/exporter/get/" + getName();
			
			logger.debug("Exporter definition url " + configHelper.makeUrl(url));
			
			GenericJSON response = restService
					.get(configHelper.makeUrl(url), configHelper.getSecurityToken(), GenericJSON.class);
			
			GenericJSON genericJSON = response;
			logger.debug(genericJSON.toString()); 
		
			return getCarIQExporterDefintionFromGenericJSON(response);
			
		}
	}

	/**
	 * Gets the car IQ exporter defintion from generic JSON.
	 *
	 * @param response the response
	 * @return the car IQ exporter defintion from generic JSON
	 */
	private CarIQExporterDefinition getCarIQExporterDefintionFromGenericJSON(GenericJSON response) {
		CarIQExporterDefinition def = new CarIQExporterDefinition(name, description);
		
		try {
			System.out.println(response.get("queryDefinition"));
			//Get array of query definition from JSON
			ArrayList<Object> definitionArray = (ArrayList<Object>) response.get("queryDefinition");
			
			//Iterate over json array and add each query definition into <code>def</code>
			for (int i = 0; i < definitionArray.size(); i++) {
				
				LinkedHashMap<String, Object> object = (LinkedHashMap<String, Object>) definitionArray.get(i);
				String queryType = (String) object.get("queryType");
				String queryDesc = (String) object.get("description");
				ArrayList<Object> queryAttrDefinition = (ArrayList<Object>) object.get("queryAttrDefinition");
				
				List<QueryDefinitionAttribute> attrs = new ArrayList<>(); 
				for(int j = 0; j < queryAttrDefinition.size(); j++) {
					LinkedHashMap<String, Object> queryAttr = (LinkedHashMap<String, Object>) queryAttrDefinition.get(j);
					String name = (String) queryAttr.get("name");
					String description = (String) queryAttr.get("description");
					String clazz = (String) queryAttr.get("clazz");
					
					attrs.add(new QueryDefinitionAttribute(name, description, Utils.getClazzName(clazz)));
				}
				def.add(CarIQExporterDefinition.QUERY_DEF, new CarIQQueryDefinition(queryType, queryDesc, attrs));

			}
			
			//Get array of exportAttr Definition
			ArrayList<Object> exportAttrDefinition = (ArrayList<Object>) response.get("exportAttrDefinition");
			for(int exportAttrIndex = 0; exportAttrIndex < exportAttrDefinition.size(); exportAttrIndex++) {
				String singleAttr = (String) exportAttrDefinition.get(exportAttrIndex);
				def.add(CarIQExporterDefinition.EXPORT_PARAM, singleAttr);
			}
			
		} catch(Exception e) {
			e.printStackTrace();
			Utils.logException(logger, e, "getCarIQExporterDefinitionFromGenericJSON");
		}
		return def;
	}
}
