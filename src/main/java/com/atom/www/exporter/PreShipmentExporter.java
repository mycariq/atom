package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.atom.www.helper.DomainHelper;
import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.model.Appointment;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.Domain.Domain_Out;
import com.cariq.toolkit.model.ProxyObject;
import com.cariq.toolkit.model.User;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * Exports all order of all stages before shipment
 * 
 * @author sagar
 *
 */
public class PreShipmentExporter extends ShipmentBasedSpotlightTemplate {

	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("PreShipmentExporter");

	public static final String TheExporter = "PreShipmentExporter";

	public static final String TheDescription = "Exports all order of all stages before shipment.";
	
	//Pre-Shipment stages
	private static final String PreShipment_Stage_Not_Verified = "Not Verified";
	private static final String PreShipment_Stage_User_Created = "User Created";
	private static final String PreShipment_Stage_Downloaded_App = "Downloaded App";
	
	/**
	 * Default constructor
	 */
	public PreShipmentExporter() {
		super(TheExporter, TheDescription);
	}

	/**
	 * Get objects with effective date
	 * 
	 * @param response
	 * @return
	 */
	protected List<GenericJSON> enhanceByEffectiveDates(List<GenericJSON> response) {
		logger.debug("Enhancing response with effective dates...");
		List<GenericJSON> retValues = new ArrayList<>();
		
		for(GenericJSON item : response) {
			String effectiveDate = null;
			String stage = Utils.getValue(item, STATE); 
			if(PreShipment_Stage_Not_Verified.equalsIgnoreCase(stage)) {
				effectiveDate = Utils.getValue(item, "StagingCreatedOn");
			} else if(PreShipment_Stage_User_Created.equalsIgnoreCase(stage)) {
				effectiveDate = Utils.getValue(item, "Policy_CreationTime");
			} else {
				//for stage: App downloaded and all other
				logger.debug("Unknown stage '" + stage + "' found!!!!!!!!!!!!!!!!!."
						+ " Using StagingModifiedOn as effective date for same!!!!!!!!!!!!!!!!!!!!!");
				effectiveDate = Utils.getValue(item, "StagingModifiedOn");
			}
			item.add(Effective_Date, effectiveDate);
			retValues.add(item);
		}
		return retValues;
	}

	@Override
	protected String getQueryType() {
		return StagingAndPolicyHelper.QueryType_PreShipment;
	}
}
