package com.atom.www.exporter;

import java.util.List;

import com.atom.www.helper.StagingAndPolicyHelper;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * Validates input json, and retrieves policies with the help of on-boarding exporter.
 * 
 * @author sagar
 *
 */
public abstract class SpotlightTemplate extends CarIQExporterTemplate {

	protected static final String KEY_DOMAIN_NAME = "domain";

	protected static final String KEY_OWNER = "owner";
	
	/**
	 * Parameterized constructor
	 * 
	 * @param name			name of exporter
	 * @param description	description of exporter
	 */
	public SpotlightTemplate(String name, String description) {
		super(name, description);
	}

	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		
		try (ProfilePoint _doExport_ = ProfilePoint
				.profileAction("ProfAction_doExport_" + this.getClass().getCanonicalName())) {
			OnboardingExporter exporter = new OnboardingExporter();

			// add comma seperated policy numbers in json.
			json.add("Keyword search", getSignatures(queryType, json, pageNo, pageSize));
			List<GenericJSON> outputJson = exporter.doExport(getQueryType(), json,
					pageNo, pageSize);
			
			logger.debug("SpotlightTemplate: doExport: outputJson count after enhancing: " + (outputJson != null ? outputJson.size() : 0));

			try (ProfilePoint _postProcess_ = ProfilePoint.profileAction(
					"ProfAction_postProcess_" + PreShipmentExporter.class.getCanonicalName())) {
				// post process json	
				return postProcess(queryType, json, outputJson, pageNo, pageSize);
			}
		}
	}

	/**
	 * Get comma separated signatures from child of this. Which will be used as input json for onboarding exporter.
	 * 
	 * CarIQ layer may not return details of every user of each signature. 
	 * This is because sometime dummy users get deleted from CarIQ and appointment of same
	 * remain staled in Atom Layer.(Need to handle same at Architecture level).
	 * 
	 * @param queryType
	 * @param json
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	protected abstract String getSignatures(String queryType, GenericJSON json, int pageNo, int pageSize);
	
	
	/**
	 * Get query type for this exporter
	 * 
	 * @return
	 */
	protected abstract String getQueryType();
	
	
	/**
	 * Post process output
	 * 
	 * @param queryType
	 * @param inputJson
	 * @param outputJson
	 * @return
	 */
	protected List<GenericJSON> postProcess(String queryType, GenericJSON inputJson, List<GenericJSON> outputJson, int pageNo, int pageSize) {
		return outputJson;
	}
}
