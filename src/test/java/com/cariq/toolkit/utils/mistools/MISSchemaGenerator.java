package com.cariq.toolkit.utils.mistools;

import java.io.IOException;
import java.util.Arrays;
import org.junit.Test;

import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.mistools.GroupStyle;
import com.cariq.toolkit.utils.mistools.MISReportSchema;
import com.cariq.toolkit.utils.mistools.MISSectionGroup;
import com.cariq.toolkit.utils.mistools.MISTableSchema;

public class MISSchemaGenerator {
	public static final String DefaultOnEvaluator = "com.cariq.toolkit.coreiq.tableUtil.application.mis.arbiter.CellValueArbiterDefaultOn";
	public static final String DefaultOffEvaluator = "com.cariq.toolkit.coreiq.tableUtil.application.mis.arbiter.CellValueArbiterDefaultOff";
	public static final String KeyValueListEvaluator = "com.cariq.toolkit.coreiq.tableUtil.application.mis.arbiter.KeyValueListArbiter";

	/**
	 * This is almost full fledged program to generate the Schema of JSONBuilder
	 * There are 5-6 dimensions along which the information is to be rendered in
	 * MIS. Earlier I thought it is random, but in fact it's quite systematic.
	 * Following are the heads, their Keys and their values. All the data is
	 * arranged in these cross sections. I'll define a few classes here and use the
	 * JSON conversion etc. utility for getting CarIQ Entry Month:
	 * Feb-18,Mar-18,....,Mar-18,Apr-18 City Tier: null, Tier-A, Tier-B SubVertical:
	 * E Channel, Others, Agency Policy Policy Stage: Car Connected,Device
	 * OnBoarded,Device RTO,Device Delivered,Device Shipped,Device
	 * Requested,Downloaded App,User Created,Under Verification Subscription Status:
	 * SubscriptionActive,SubscriptionCancelled,SubscriptionDeactivated,SubscriptionExpired,SubscriptionRenewed
	 * Policy State:
	 * Active,GOT_DEVICE_BACK,READY_TO_RETRIEVE,NOT_CONTACTABLE,NOT_INTERESTED,Invalid
	 * TAT: 0-7 DAYS,8-14 DAYS,15-21 DAYS,22-30 DAYS,> 30 DAYS PolicyCategory:
	 * INVALID_CARNOT,INVALID_TRANSACTION,NOT_VERIFIED,NOT_VERIFIED_NC,NOT_VERIFIED_NI,PROCESSED,null
	 * @throws IOException 
	 */

	// This is almost full fledged program to generate the Schema of JSONBuilder

	// -------------------------------------------------

	//@Test
	public void prepareMISReportJSONProto() throws IOException {
		// initialize SectionGroups and Sections - in code for now
		initializeSectionData();

		MISReportSchema schema = new MISReportSchema("WEEKLY MIS");
		// Table with all policies - without Monthly Seggragation
//		schema.add(new MISTableSchema("Table 1 - All Policy Status", /* style, */ null,
//				Arrays.asList(new GroupStyle("PolicyCategory", "BLUE"), "Policy Stage"), null));

		// Table with all policies - with Monthly Seggragation along City Tier and
		// SubVertical
		schema.add(new MISTableSchema("Table 2  - All Policy Status vertical wise", /* style, */ null,
				Arrays.asList(new GroupStyle("City Tier", "cityStyle"), "Combo Vertical"), "CarIQ Entry Month"));

//		 Test Table to create table of Grand totals (Table 1) with only NI and Active
//		 applicable
		 schema.add(new MISTableSchema("Table 1 - All Policy Status", 
		 Arrays.asList(new ApplicableSection("PolicyCategory", "PROCESSED"), new ApplicableSection("Policy State", Arrays.asList("READY_TO_RETRIEVE, GOT_DEVICE_BACK"))),
		 Arrays.asList(new GroupStyle("PolicyCategory", "BLUE"), "Policy Stage"), null));

		GenericJSON outSchema = schema.getSchema();
		String jsonString = Utils.getJSonString(outSchema);
		
		String filePath = CarIQFileUtils.getTempFile("WeeklyMISBuilder-Proto.json");
		CarIQFileUtils.saveToFile(filePath, jsonString);
		
		
	}
	
	@Test
	public void prepareMonthlyMISReportJSON() throws IOException {
		// initialize SectionGroups and Sections - in code for now
		initializeSectionData();

		MISReportSchema schema = new MISReportSchema("MONTHLY MIS");

		// ------------------------
		// Tables for Monthly MIS
		// ------------------------
		
		// FreshBooking Tables
		
//		schema.add(new MISTableSchema("FreshBooking - Device Dispatch Status", null,
//				Arrays.asList(new GroupStyle("Dispatch Status", "special-row-green"), new GroupStyle("Policy State", "special-row-blue"), "Policy Stage"), "Issue Month"));
		
		// Retention Tables
		
		schema.add(new MISTableSchema("Retention Table A - All Policies SubscriptionState vs Policy State", null,
				Arrays.asList("SubscriptionState"), "Policy State"));

		schema.add(new MISTableSchema("Retention Table B - All Policies Completing 1 Year: SubscriptionState vs Policy State",
				Arrays.asList(new ApplicableSection("CompletedOneYear", Arrays.asList("true"))),				
				Arrays.asList("SubscriptionState"), "Policy State"));
		
		schema.add(new MISTableSchema("Retention Table C - All Policies Completing 1 Year: SubscriptionState vs Dispatch Status",
				Arrays.asList(new ApplicableSection("CompletedOneYear", Arrays.asList("true"))),
				Arrays.asList("SubscriptionState"), "Dispatch Status"));

		schema.add(new MISTableSchema("Retention Table D - Policies Completing 1 Year & Active only: SubscriptionState vs Dispatch Status",
				Arrays.asList(new ApplicableSection("CompletedOneYear", Arrays.asList("true")),
							  new ApplicableSection("Policy State", Arrays.asList("Active", "READY_TO_RETRIEVE", "GOT_DEVICE_BACK", "LOST_DEVICE"))),
				Arrays.asList("SubscriptionState"), "Dispatch Status"));
		
		
		// MTD Tables
//		schema.add(new MISTableSchema("MTD Table A - All Policies", null,
//				Arrays.asList("PolicyCategory"), null));
//
//		schema.add(new MISTableSchema("MTD Table B - All Policies State Vertical wise", null,
//				Arrays.asList("Policy State"), "Combo Vertical"));
		
//		schema.add(new MISTableSchema("MTD Table C - All Verticals (except invalid policies) Month wise", null,
////				Arrays.asList("Policy State", Arrays.asList("Active", "GOT_DEVICE_BACK", "READY_TO_RETRIEVE", 
////						"LOST_DEVICE", "NOT_CONTACTABLE", "NOT_INTERESTED")),
//				Arrays.asList("Combo Vertical"), "Issue Month"));
//		
//		schema.add(new MISTableSchema("MTD Table C.1 - All Renewals (except invalid policies) Month wise", null,
//				Arrays.asList("SubscriptionState"), "Issue Month"));
//				//Arrays.asList("SubscriptionState.SubscriptionRenewed"), "Issue Month"));
//		
//		schema.add(new MISTableSchema("MTD Table C.2 - All New cases (except invalid policies) Month wise", null,
//				Arrays.asList("SubscriptionState"), "Issue Month"));
		
//				Arrays.asList("SubscriptionState.NA", "SubscriptionState.SubscriptionActive", "SubscriptionState.SubscriptionCancelled",
//							  "SubscriptionState.SubscriptionDeactivated", "SubscriptionState.SubscriptionExpired"), "Issue Month"));

//		schema.add(new MISTableSchema("MTD Table D - Active and Only Device-Dispatched Policies Month wise", 
//				Arrays.asList(new ApplicableSection("Policy State", Arrays.asList("Active", "READY_TO_RETRIEVE, GOT_DEVICE_BACK", "LOST_DEVICE")),
//						      new ApplicableSection("SubscriptionState", Arrays.asList("SubscriptionActive")),
//						      new ApplicableSection("Dispatch Status", Arrays.asList("Device Dispatched"))),
//				Arrays.asList("Combo Vertical"), "Issue Month"));
		
//		schema.add(new MISTableSchema("MTD Table E -  Active Policies Month wise (Including both, Device Dispatched & not Dispatched)", 
//				Arrays.asList(new ApplicableSection("Policy State", Arrays.asList("Active", "READY_TO_RETRIEVE, GOT_DEVICE_BACK", "LOST_DEVICE"))),
//				Arrays.asList("Combo Vertical"), "Issue Month"));
		
//		schema.add(new MISTableSchema("MTD Table F -  Renewed Policies (Except invalid) by Verticals, Month wise", 
//				Arrays.asList(new ApplicableSection("Policy State", Arrays.asList("Active", "NOT_CONTACTABLE", "NOT_INTERESTED", "READY_TO_RETRIEVE", "GOT_DEVICE_BACK", "LOST_DEVICE")),
//							  new ApplicableSection("SubscriptionState", Arrays.asList("SubscriptionRenewed"))),
//				Arrays.asList("Combo Vertical"), "Issue Month"));
		
//		schema.add(new MISTableSchema("MTD Table G.1 -  Active Policies Device verification TAT Bucket, Month wise", 
//				Arrays.asList(new ApplicableSection("Policy State", Arrays.asList("Active", "READY_TO_RETRIEVE", "GOT_DEVICE_BACK", "LOST_DEVICE"))),
//				Arrays.asList("Device verification TAT Bucket"), "Issue Month"));
//		
//		schema.add(new MISTableSchema("MTD Table G.2 -  Active Policies Device req TAT Bucket, Month wise", 
//				Arrays.asList(new ApplicableSection("Policy State", Arrays.asList("Active", "READY_TO_RETRIEVE", "GOT_DEVICE_BACK", "LOST_DEVICE"))),
//				Arrays.asList("Device req TAT Bucket"), null));	
//		
//		schema.add(new MISTableSchema("MTD Table G.3 -  Active Policies Device shipped TAT Bucket, Month wise", 
//				Arrays.asList(new ApplicableSection("Policy State", Arrays.asList("Active", "READY_TO_RETRIEVE", "GOT_DEVICE_BACK", "LOST_DEVICE"))),
//				Arrays.asList("Device shipped TAT Bucket"), null));	
//		
//		schema.add(new MISTableSchema("MTD Table G.4 -  Active Policies Car connected TAT Bucket, Month wise", 
//				Arrays.asList(new ApplicableSection("Policy State", Arrays.asList("Active", "READY_TO_RETRIEVE", "GOT_DEVICE_BACK", "LOST_DEVICE"))),
//				Arrays.asList("Car connected TAT Bucket"), null));	
		
//		schema.add(new MISTableSchema("MTD Table H -  Active Policies by Tiers, Month wise", 
//				Arrays.asList(new ApplicableSection("Policy State", Arrays.asList("Active", "READY_TO_RETRIEVE", "GOT_DEVICE_BACK", "LOST_DEVICE"))),
//				Arrays.asList(new GroupStyle("City Tier", "special-row-green"), "Policy Stage"), "Issue Month"));	
		
//		schema.add(new MISTableSchema("MTD Table I.1 -  Car Connected and Active only by Tiers, Month wise", 
//				Arrays.asList(new ApplicableSection("Policy State", Arrays.asList("Active", "READY_TO_RETRIEVE", "GOT_DEVICE_BACK", "LOST_DEVICE")),
//							  new ApplicableSection("Policy Stage", Arrays.asList("Car Connected", "Device OnBoarded", "Device Delivered", "Device Shipped"))),
//				Arrays.asList("City Tier"), "Issue Month"));
//		
//		schema.add(new MISTableSchema("MTD Table I.2 -  NI & NC only by Tiers, Month wise", 
//				Arrays.asList(new ApplicableSection("Policy State", Arrays.asList("NOT_CONTACTABLE", "NOT_INTERESTED"))),
//				Arrays.asList("City Tier"), "Issue Month"));
		
//		schema.add(new MISTableSchema("MTD Table J -  Not Contactable Policies (NC) by Verticals, Month wise", 
//				Arrays.asList(new ApplicableSection("Policy State", Arrays.asList("NOT_CONTACTABLE"))),
//				Arrays.asList("Combo Vertical"), "Issue Month"));
//		
//		schema.add(new MISTableSchema("MTD Table K -  Not Interested Policies (NI) by Verticals, Month wise", 
//				Arrays.asList(new ApplicableSection("Policy State", Arrays.asList("NOT_INTERESTED"))),
//				Arrays.asList("Combo Vertical"), "Issue Month"));
		
//		GenericJSON outSchema = schema.getSchema();
//		System.out.println(Utils.getJSonString(outSchema));
		
		GenericJSON outSchema = schema.getSchema();
		String jsonString = Utils.getJSonString(outSchema);
		
		String filePath = CarIQFileUtils.getTempFile("MonthlyMisReportBuilder-Ret.json");
		CarIQFileUtils.saveToFile(filePath, jsonString);
	}

	
	//@Test
	public void prepareWeeklyMISReportJSON() throws IOException {
		// initialize SectionGroups and Sections - in code for now
		initializeSectionData();

		MISReportSchema schema = new MISReportSchema("WEEKLY MIS");

		// ------------------------
		// Tables for Weekly MIS
		// ------------------------
		schema.add(new MISTableSchema("Table 1 - All Policy Status", null,
				Arrays.asList("PolicyCategory", "Policy Stage"), null));
		schema.add(new MISTableSchema("Table 2  - All Policy Status vertical wise", null,
				Arrays.asList(new GroupStyle("City Tier", "cityStyle"), new GroupStyle("Combo Vertical")), "CarIQ Entry Month"));
		schema.add(new MISTableSchema(
				"Table 3 - Tier & Vertical wise active processed policy CarIQ entry month (except NI, NC & Invalid)",
				Arrays.asList("Policy State.Active", "Policy State.GOT_DEVICE_BACK", "Policy State.READY_TO_RETRIEVE"),
				Arrays.asList("City Tier", "Combo Vertical", "Policy Stage"), "CarIQ Entry Month"));
		schema.add(new MISTableSchema(
				"Table 4 - Tier & Vertical wise non-contactable (NC) policy status by CarIQ entry month",
				Arrays.asList("Policy State.NOT_CONTACTABLE"),
				Arrays.asList("City Tier", "Combo Vertical", "Policy Stage"), "CarIQ Entry Month"));
		schema.add(new MISTableSchema(
				"Table 5 - Tier & Vertical wise not-interested (NI) policy status by CarIQ entry month",
				Arrays.asList("Policy State.NOT_INTERESTED"), Arrays.asList("City Tier", "Combo Vertical", "Policy Stage"),
				"CarIQ Entry Month"));
		schema.add(new MISTableSchema("Table 6  - Tier & Vertical wise invalid policy status by CarIQ entry month",
				Arrays.asList("Policy State.Invalid"), Arrays.asList("City Tier", "Combo Vertical", "Policy Stage"),
				"CarIQ Entry Month"));
		schema.add(
				new MISTableSchema("Table 7 - Policy subscription status CarIQ entry month (except NC, NI & Invalid)",
						Arrays.asList("Policy State.Active", "Policy State.GOT_DEVICE_BACK",
								"Policy State.READY_TO_RETRIEVE"),
						Arrays.asList("SubscriptionState"), "CarIQ Entry Month"));

		schema.add(new MISTableSchema(
				"Table 8 - Policy subscription status vertical wise CarIQ entry month (except NC, NI & Invalid)",
				Arrays.asList("Policy State.Active", "Policy State.GOT_DEVICE_BACK", "Policy State.READY_TO_RETRIEVE"),
				Arrays.asList("City Tier", "Combo Vertical", "SubscriptionState"), "CarIQ Entry Month"));

		schema.add(new MISTableSchema(
				"Table 9  - Tier & Vertical wise policy state of car connected only by CarIQ entry month", null,
				Arrays.asList("City Tier", "Combo Vertical", "SubscriptionState", "Policy State"), "CarIQ Entry Month"));

		schema.add(new MISTableSchema("Table 10 - Turn around time (TAT) for car connected stage by CarIQ entry month",
				Arrays.asList("Policy Stage.Car Connected"), Arrays.asList("City Tier", "TAT"), "CarIQ Entry Month"));
		schema.add(
				new MISTableSchema("Table 11 - Turn aroud time (TAT) for device dispatched stage by CarIQ entry month",
						Arrays.asList("Policy Stage.Device OnBoarded", "Policy Stage.Device RTO",
								"Policy Stage.Device Delivered", "Policy Stage.Device Shipped"),
						Arrays.asList("City Tier", "TAT"), "CarIQ Entry Month"));
		schema.add(new MISTableSchema(
				"Table 12 Turn around time (TAT)  for app download & device requested stage by CarIQ entry month",
				Arrays.asList("Policy Stage.Device Requested", "Policy Stage.Downloaded App"),
				Arrays.asList("City Tier", "TAT"), "CarIQ Entry Month"));

		// Unsure whether User Created should be included in this
		schema.add(new MISTableSchema(
				"Table 13 - Turn around time (TAT) for device verification stage by CarIQ entry month",
				Arrays.asList("Policy Stage.User Created", "Policy Stage.Under Verification"),
				Arrays.asList("City Tier", "TAT"), "CarIQ Entry Month"));
//		GenericJSON outSchema = schema.getSchema();
//		System.out.println(Utils.getJSonString(outSchema));

		GenericJSON outSchema = schema.getSchema();
		String jsonString = Utils.getJSonString(outSchema);
		
		String filePath = CarIQFileUtils.getTempFile("WeeklyMISBuilder.json");
		CarIQFileUtils.saveToFile(filePath, jsonString);
	}

	private void initializeSectionData() {
		MISSectionGroup.create("CarIQ Entry Month",
				new String[] { "Feb-18", "Mar-18", "Apr-18", "May-18", "Jun-18", "Jul-18", "Aug-18", "Sep-18", "Oct-18",
						"Nov-18", "Dec-18", "Jan-19", "Feb-19", "Mar-19", "Apr-19", "May-19", "Jun-19", "Jul-19",
						"Aug-19", "Sep-19", "Oct-19", "Nov-19", "Dec-19", "Jan-20", "Feb-20", "Mar-20", "Apr-20",
						"May-20", "Jun-20", "Jul-20", "Aug-20", "Sep-20", "Oct-20", "Nov-20", "Dec-20" });
		
		MISSectionGroup.create("Issue Month",
				new String[] { "NA", "Jan-18", "Feb-18", "Mar-18", "Apr-18", "May-18", "Jun-18", "Jul-18", "Aug-18", "Sep-18", "Oct-18",
						"Nov-18", "Dec-18", "Jan-19", "Feb-19", "Mar-19", "Apr-19", "May-19", "Jun-19", "Jul-19",
						"Aug-19", "Sep-19", "Oct-19", "Nov-19", "Dec-19", "Jan-20", "Feb-20", "Mar-20", "Apr-20",
						"May-20", "Jun-20", "Jul-20", "Aug-20", "Sep-20", "Oct-20", "Nov-20", "Dec-20" });
		
		MISSectionGroup.create("City Tier", new String[] { "NA", "Tier-A", "Tier-B" });
		
		MISSectionGroup.create("Combo Vertical", new String[] { "E Channel", "Others", "Agency", "TELESALES" });
		
		MISSectionGroup.create("Policy Stage",
				new String[] { "Car Connected", "Device OnBoarded", "Device RTO", "Device Delivered", "Device Shipped",
						"Device Requested", "Downloaded App", "User Created", "Under Verification" });
		
		MISSectionGroup.create("SubscriptionState", new String[] { "NA", "SubscriptionActive", "SubscriptionCancelled",
				"SubscriptionDeactivated", "SubscriptionExpired", "SubscriptionRenewed" });
		
		MISSectionGroup.create("Policy State", new String[] { "Active", "GOT_DEVICE_BACK", "READY_TO_RETRIEVE",
				"NOT_CONTACTABLE", "NOT_INTERESTED", "Invalid", "LOST_DEVICE" });
		
		
		MISSectionGroup.create("Device verification TAT Bucket",
				new String[] { "NA", "0-7 DAYS", "8-14 DAYS", "15-21 DAYS", "22-30 DAYS", "Above 30 DAYS" });
		MISSectionGroup.create("Device req TAT Bucket",
				new String[] { "NA", "0-7 DAYS", "8-14 DAYS", "15-21 DAYS", "22-30 DAYS", "Above 30 DAYS" });
		MISSectionGroup.create("Device shipped TAT Bucket",
				new String[] { "NA", "0-7 DAYS", "8-14 DAYS", "15-21 DAYS", "22-30 DAYS", "Above 30 DAYS" });
		MISSectionGroup.create("Car connected TAT Bucket",
				new String[] { "NA", "0-7 DAYS", "8-14 DAYS", "15-21 DAYS", "22-30 DAYS", "Above 30 DAYS" });

		
		MISSectionGroup.create("PolicyCategory", new String[] {"INVALID_CARNOT", "INVALID_TRANSACTION", "NOT_VERIFIED",
				"NOT_VERIFIED_NC", "NOT_VERIFIED_NI", "PROCESSED", "NA" });
		
		MISSectionGroup.create("Dispatch Status", new String[] { "Device Dispatched", "Device Not Dispatched"});
		
		MISSectionGroup.create("CompletedOneYear", new String[] { "yes", "no"});
		
		// For testing purpose - can be used for Rows and Columns
		MISSectionGroup.create("TestCategory", new String[] { "A", "B", "C" });
		MISSectionGroup.create("TestSubCategory", new String[] { "X", "Y", "Z" });
		MISSectionGroup.create("TestParameter", new String[] { "P", "Q", "R" });

		// For FormulaTest
		MISSectionGroup.create("FormulaTest", new String[] { "Hundred Times A", "Percentage AB", "Local B" });
	}
	
	/**
	 * - Report with 2 tables First is Pivot MISTable... Generated from the Schema
	 * generator {A, B, C}, {X,Y, Z} against TestParameters in Column... Second Add
	 * 1 more table below the above table by hand (first generated by Schema
	 * generator then by hand). Table has 3 rows. First row has name "Hundred A" has
	 * formula - every row has 100*A (A is the row above from Table1) Second row has
	 * name "Percentage" and has formula - 100*A/B (A and B from table1) Third row
	 * has name "B from Local" and has formula - Hundred A/percentage
	 * 
	 * @throws IOException
	 */
	@Test
	public void prepareFormulaTableSchema() throws IOException {
		// initialize SectionGroups and Sections - in code for now
		initializeSectionData();

		MISReportSchema schema = new MISReportSchema("Formula Test");
		
		// Data table
		schema.add(new MISTableSchema("Table 1  - ABC-XYZ Data", null,
				Arrays.asList(new GroupStyle("TestCategory", "special-row-blue"), "TestSubCategory"),
				"TestParameter"));
		
		// Formula table
		schema.add(new MISTableSchema("Table 2  - Formula", null,
				Arrays.asList("FormulaTest"),
				"TestParameter"));

		GenericJSON outSchema = schema.getSchema();
		String jsonString = Utils.getJSonString(outSchema);

		System.out.println(jsonString);

		String filePath = CarIQFileUtils.getTempFile("MIS_Formula_Schema.json");
		CarIQFileUtils.saveToFile(filePath, jsonString);
	}
	
	@Test
	public void prepareTimeLineSReportJSON() throws IOException {
		// initialize SectionGroups and Sections - in code for now
		initializeTimeLineSectionData();

		MISReportSchema schema = new MISReportSchema("TimeLine Report");

		// ------------------------
		// Tables for Time line report
		// ------------------------
		
		schema.add(new MISTableSchema("Table 1: Daily Report(type wise)",
				Arrays.asList(new ApplicableSection("DailyReport", "Daily")),
				Arrays.asList(new GroupStyle("type", null)), "firstName"));
		
		schema.add(new MISTableSchema("Table 2: Daily Report(tag wise)",
				Arrays.asList(new ApplicableSection("DailyReport", "Daily")),
				Arrays.asList(new GroupStyle("tag", null)), "firstName"));

		schema.add(new MISTableSchema("Table 3: Weekly Report(type wise)",
				Arrays.asList(new ApplicableSection("WeeklyReport", "Weekly")),
				Arrays.asList(new GroupStyle("type", null)), "firstName"));
		
		schema.add(new MISTableSchema("Table 4: Weekly Report(tag wise)",
				Arrays.asList(new ApplicableSection("WeeklyReport", "Weekly")),
				Arrays.asList(new GroupStyle("tag", null)), "firstName"));
		// ==============================================================================================================
		schema.add(new MISTableSchema("Table 5: Daily Details(Support Performance Report)",
				Arrays.asList(new ApplicableSection("DailyReport", "Daily")),
				Arrays.asList(new GroupStyle("domain", "special-row-orange"), new GroupStyle("tag", "special-row-green"),
						new GroupStyle("type", "special-row-blue"), new GroupStyle("stage")),
				"firstName"));

		schema.add(new MISTableSchema("Table 6: Weekly Details(Support Performance Report)",
				Arrays.asList(new ApplicableSection("WeeklyReport", "Weekly")),
				Arrays.asList(new GroupStyle("domain", "special-row-orange"), new GroupStyle("tag", "special-row-green"),
						new GroupStyle("type", "special-row-blue"), new GroupStyle("stage")),
				"firstName"));

		GenericJSON outSchema = schema.getSchema();
		String jsonString = Utils.getJSonString(outSchema);

		String filePath = CarIQFileUtils.getTempFile("TimeLineReportBulilder.json");
		CarIQFileUtils.saveToFile(filePath, jsonString);
	}

	private void initializeTimeLineSectionData() {
		MISSectionGroup.create("domain", new String[] { "Driven", "Drivesmart", "IL-Assist", "TAGIC" });
		
		MISSectionGroup.create("firstName", new String[] { "Amit", "Devyani", "Prajyot", "Preeti", "Rutuja",
				"Shikha", "Shubham", "Swati", "Sweta", "Shital" });

		MISSectionGroup.create("type", new String[] { "Phone call", "Email", "SMS", "Comment", "appointment" });
		
		/*MISSectionGroup.create("stage",
				new String[] { "NA", "Not Verified", "Device Requested", "Device Shipped", "Device Delivered",
						"Device RTO", "Downloaded App", "User Created", "Under Verification", "Field assigned",
						"Device OnBoarded", "Car Connected", "NOT_INTERESTED", "NOT_CONTACTABLE", "Due for retrieval",
						"Due for renewal", "SubscriptionRenewed" });*/
		
		MISSectionGroup.create("stage",
				new String[] { "NA", "Not Verified", "Device Requested", "Device Shipped", "Device Delivered", "Device RTO",
						"Downloaded App", "User Created", "Device OnBoarded", "Car Connected" });

		/*MISSectionGroup.create("tag",
				new String[] { "NA", "Active", "GOT_DEVICE_BACK", "READY_TO_RETRIEVE", "Out of Service", "Engaged",
						"Ringing", "Call Back", "Verified", "Not Reachable", "Switched Off", "Call Drop",
						"Network Issue", "Wrong Number", "Test", "Cancelled", "Snooze", "Invalid order", "RANDRSHEET",
						"READY_TO_RENEW", "INTERESTED", "DUPLICATE", "Invalid", "LOST_DEVICE", "NOT_CONTACTABLE", "NC",
						"WAS_NOT_CONTACTABLE", "NOT_INTERESTED", "Not Interested", "WAS_NOT_INTERESTED" });*/
		
		MISSectionGroup.create("tag", new String[] { "NA", "Active", "Invalid", "LOST_DEVICE", "READY_TO_RENEW",
				"READY_TO_RETRIEVE", "NOT_CONTACTABLE", "NOT_INTERESTED" });

		MISSectionGroup.create("DailyReport", new String[] { "Daily" });
		MISSectionGroup.create("WeeklyReport", new String[] { "Weekly" });
	}
	
	@Test
	public void prepareAppointmentReportJSON() throws IOException {
		// initialize SectionGroups and Sections - in code for now
		initializeAppointmentSectionData();

		MISReportSchema schema = new MISReportSchema("Individual Load Matrix");

		// ------------------------
		// Tables for Time line report
		// ------------------------
		
		schema.add(new MISTableSchema("Table 1: Appointment Report", null, Arrays.asList("appointmentState"),
				"firstName"));

		GenericJSON outSchema = schema.getSchema();
		String jsonString = Utils.getJSonString(outSchema);

		String filePath = CarIQFileUtils.getTempFile("AppointmentReportBulilder.json");
		CarIQFileUtils.saveToFile(filePath, jsonString);
	}

	private void initializeAppointmentSectionData() {

		MISSectionGroup.create("firstName", new String[] { "Amit", "Devyani", "Prajyot", "Preeti", "Rutuja", "Shikha",
				"Shubham", "Swati", "Sweta", "Shital" });
		
		MISSectionGroup.create("appointmentState", new String[] { "Past", "Today", "Future" });
	}
	
	
	@Test
	public void prepareSystemPerformanceReportJSON() throws IOException {
		// initialize SectionGroups and Sections - in code for now
		initializeSystemPerformanceSectionData();

		MISReportSchema schema = new MISReportSchema("System Performance Matrix");

		// ------------------------
		// Tables for Time line report
		// ------------------------
		schema.add(new MISTableSchema("Table 1 - System Performance Matrix", null,
				Arrays.asList(new GroupStyle("City Tier", "special-row-green"), new GroupStyle("Policy Stage")),
				"CarIQ Entry Month"));

//		schema.add(new MISTableSchema("Table 2: System Performance Matrix", null,
//				Arrays.asList(new GroupStyle("City Tier", "special-row-green"), new GroupStyle("stage")),
//				"CarIQ Entry Month"));
//
//		schema.add(new MISTableSchema("Table 3: System Performance Matrix", null,
//				Arrays.asList(new GroupStyle("tierPercentage")), "CarIQ Entry Month"));

		GenericJSON outSchema = schema.getSchema();
		String jsonString = Utils.getJSonString(outSchema);

		String filePath = CarIQFileUtils.getTempFile("SystemPerformanceMatrixReportBulilder.json");
		CarIQFileUtils.saveToFile(filePath, jsonString);
	}

	private void initializeSystemPerformanceSectionData() {

		MISSectionGroup.create("CarIQ Entry Month",
				new String[] { "Aug-19", "Sep-19", "Oct-19", "Nov-19", "Dec-19", "Jan-20", "Feb-20", "Mar-20", "Apr-20",
						"May-20", "Jun-20", "Jul-20", "Aug-20", "Sep-20", "Oct-20", "Nov-20", "Dec-20", "Jan-21" });

		MISSectionGroup.create("City Tier", new String[] { "Tier-A", "Tier-B" });

		MISSectionGroup.create("Policy Stage",
				new String[] { "NA", "Not Verified", "Under verification", "Device Requested", "Device Shipped", "Device Delivered",
						"Device RTO", "Downloaded App", "User Created", "Device OnBoarded", "Car Connected" });

		MISSectionGroup.create("stage", new String[] { "ShippedButNotConnected", "Connected" });

		MISSectionGroup.create("tierPercentage",
				new String[] { "ConnectionPercentageTierA", "ConnectionPercentageTierB" });
	}
	
	@Test
	public void prepareIndividualLoadMatrixReportJSON() throws IOException {
		// initialize SectionGroups and Sections - in code for now
		initializeIndividualLoadMatrixSectionData();

		MISReportSchema schema = new MISReportSchema("Individual Load Matrix");

		// ------------------------
		// Tables for Time line report
		// ------------------------

		schema.add(new MISTableSchema("Individual Load Matrix", null,
				Arrays.asList(new GroupStyle("City Tier", "special-row-green"), new GroupStyle("Policy Stage")),
				"firstName"));

		GenericJSON outSchema = schema.getSchema();
		String jsonString = Utils.getJSonString(outSchema);

		String filePath = CarIQFileUtils.getTempFile("IndividualLoadMatrixReportBulilder.json");
		CarIQFileUtils.saveToFile(filePath, jsonString);
	}

	private void initializeIndividualLoadMatrixSectionData() {

		MISSectionGroup.create("firstName", new String[] { "Amit", "Devyani", "Prajyot", "Preeti", "Rutuja", "Shikha",
				"Shubham", "Swati", "Sweta", "Shital", "Anil" });
		
		MISSectionGroup.create("City Tier", new String[] { "Tier-A", "Tier-B" });
		
		MISSectionGroup.create("Policy Stage",
				new String[] { "NA", "Not Verified", "Device Requested", "Device Shipped", "Device Delivered",
						"Device RTO", "Downloaded App", "User Created", "Device OnBoarded", "Car Connected" });
	}
}
