package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

/**
 * 
 * Policy details mini exporter. It exports data required for finance team. 
 * 
 * @author sagar
 *
 */
public class MiniPolicyDetailsExporter extends CarIQExporterTemplate {
	
	private static final String TheExporter = "MiniPolicyDetailsExporter";
	private static final String description = "Policy details mini exporter. It exports data required for finance team";
	
	
	//Keys for exported data
	private static final String KEY_Loan_Number = "Loan Number";
	private static final String KEY_PolicyId = "PolicyId";
	private static final String KEY_Customer_First_Name = "Customer First Name";
	private static final String KEY_Customer_Last_Name = "Customer Last Name";
	private static final String KEY_Customer_Address = "Customer Address";
	private static final String KEY_Customer_City = "Customer City";
	private static final String KEY_Customer_State = "Customer State";
	private static final String KEY_Customer_Pincode = "Customer Pincode";
	private static final String KEY_Customer_Email = "Customer Email";
	private static final String KEY_Customer_Mobile_Number = "Customer Mobile Number";
	private static final String KEY_Car_Make = "Car Make";
	private static final String KEY_Car_Model = "Car Model";
	private static final String KEY_Verified = "Verified";
	private static final String KEY_Entry_date = "Entry date";
	private static final String KEY_Order_Id = "Order Id";
	private static final String KEY_Shipment_AWB = "Shipment AWB";
	private static final String KEY_Shipment_CreatedOn = "Shipment CreatedOn";
	private static final String KEY_Shipment_ModifiedOn = "Shipment DeliveredOn";
	private static final String KEY_Shipment_Status = "Shipment Status";
	private static final String KEY_Device_Id = "Device Id";
	private static final String KEY_Device_OnboardedOn = "Device OnboardedOn";

	
	//Specific attribute keys which we want to appear in final exported data.
	private static final String KEY_policyNumber = "policyNumber";
	private static final String KEY_Staging_customerFirstName = "Staging_customerFirstName";
	private static final String KEY_Staging_customerLastName = "Staging_customerLastName";
	private static final String KEY_Staging_customerAddress = "Staging_customerAddress";
	private static final String KEY_Staging_customerCity = "Staging_customerCity";
	private static final String KEY_Staging_customerState = "Staging_customerState";
	private static final String KEY_Staging_customerPinCode = "Staging_customerPinCode";
	private static final String KEY_Staging_customerEmail = "Staging_customerEmail";
	private static final String KEY_Staging_customerMobileNumber = "Staging_customerMobileNumber";
	private static final String KEY_Staging_carMake = "Staging_carMake";
	private static final String KEY_Staging_carModel = "Staging_carModel";
	private static final String KEY_StagingCreatedOn = "StagingCreatedOn";
	private static final String KEY_Order_Id1 = "Order_Id";
	private static final String KEY_Shipment_AWB1 = "Shipment_AWB";
	private static final String KEY_Shipment_CreatedOn1 = "Shipment_CreatedOn";
	private static final String KEY_Shipped_Or_Delivered_On = "Shipped_Or_Delivered_On";
	private static final String KEY_Shipment_Status1 = "Shipment_Status";
	private static final String KEY_DeviceId = "DeviceId";
	private static final String KEY_Device_OnboardedOn1 = "Device_OnboardedOn";
	
	private static final String createdAfter = "CreatedAfter";
	private static final String QueryType_createdAfter = createdAfter;
	private static final String createdAfterDescription = "Policy created after given date";

	
	private OnboardingExporter onboardingExporter;

	/**
	 * Default constructor
	 */
	public MiniPolicyDetailsExporter() {
		super(TheExporter, description);
	
		onboardingExporter = new OnboardingExporter();
		
		addQueryType(QueryType_createdAfter, createdAfterDescription,
				Arrays.asList(new QueryDefinitionAttribute(createdAfter, createdAfterDescription, Date.class)));
		
		// Support roles
		addSupportedRoles(
				Arrays.asList(Utils.ADMIN_ROLE, Utils.USER_ROLE,
						Utils.ROLE_FLEET_ADMIN, Utils.ROLE_GROUP_OPERATOR, Utils.CUSTOMER_ROLE));
	}

	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		
		//Get data from onboarding exporter
		List<GenericJSON> onboardingData = onboardingExporter.doExport(queryType, json, pageNo, pageSize);
		
		List<GenericJSON> finalArr = new ArrayList<GenericJSON>();
		
		//Iterate on onboarding data and process single policy to remove unwanted data
		//and add only specific data 
		for(GenericJSON policy : onboardingData) {
			
			GenericJSON processedPolicy = new GenericJSON();
			
			processedPolicy.put(KEY_Loan_Number, policy.get(KEY_policyNumber));
			processedPolicy.put(KEY_PolicyId, policy.get(KEY_PolicyId));
			processedPolicy.put(KEY_Customer_First_Name, policy.get(KEY_Staging_customerFirstName));
			processedPolicy.put(KEY_Customer_Last_Name, policy.get(KEY_Staging_customerLastName));
			processedPolicy.put(KEY_Customer_Address, policy.get(KEY_Staging_customerAddress));
			processedPolicy.put(KEY_Customer_City, policy.get(KEY_Staging_customerCity));
			processedPolicy.put(KEY_Customer_State, policy.get(KEY_Staging_customerState));
			processedPolicy.put(KEY_Customer_Pincode, policy.get(KEY_Staging_customerPinCode));
			processedPolicy.put(KEY_Customer_Email, policy.get(KEY_Staging_customerEmail));
			processedPolicy.put(KEY_Customer_Mobile_Number, policy.get(KEY_Staging_customerMobileNumber));
			processedPolicy.put(KEY_Car_Make, policy.get(KEY_Staging_carMake));
			processedPolicy.put(KEY_Car_Model, policy.get(KEY_Staging_carModel));
			processedPolicy.put(KEY_Verified, policy.get(KEY_Verified));
			processedPolicy.put(KEY_Entry_date, policy.get(KEY_StagingCreatedOn));
			processedPolicy.put(KEY_Order_Id, policy.get(KEY_Order_Id1));
			processedPolicy.put(KEY_Shipment_AWB, policy.get(KEY_Shipment_AWB1));
			processedPolicy.put(KEY_Shipment_CreatedOn, policy.get(KEY_Shipment_CreatedOn1));
			processedPolicy.put(KEY_Shipment_ModifiedOn, policy.get(KEY_Shipped_Or_Delivered_On));
			processedPolicy.put(KEY_Shipment_Status, policy.get(KEY_Shipment_Status1));
			processedPolicy.put(KEY_Device_Id, policy.get(KEY_DeviceId));
			processedPolicy.put(KEY_Device_OnboardedOn, policy.get(KEY_Device_OnboardedOn1));

			finalArr.add(processedPolicy);
		}

		return finalArr;
	}

}
