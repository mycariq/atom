package com.cariq.toolkit.coreiq.loader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.atom.www.helper.ConfigHelper;
import com.atom.www.helper.DomainHelper;
import com.cariq.toolkit.model.Domain;
import com.cariq.toolkit.model.Domain.Domain_Out;
import com.cariq.toolkit.service.DomainAccessService;
import com.cariq.toolkit.service.DomainService;
import com.cariq.toolkit.utils.CarIQAttrDefinition;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;
import com.cariq.toolkit.utils.service.CarIQRestClientService;

/**
 * Responsible for retriving loader definition from CarIQ and load data into
 * CarIQ loader layer. After loading completed, then status of loading will be
 * returned to Loader of Atom layer.
 * 
 * @author sagar
 *
 */
@Configurable
public class CarIQLoaderWrapperImpl implements CarIQLoaderWrapper {

	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("CarIQLoaderWrapperImpl");

	private static final String LOAD_SINGLE_API = "Cariq/admin/loader/loadSingle/"/**
																					 * {Loader
																					 * name
																					 **/
	;

	private static final String GET_LOADER_DETAIL_API = "Cariq/admin/loader/get/"/**
																					 * {Loader
																					 * name
																					 **/
	;

	private static final String MESSAGES = "messages";
	private static final String NAME = "name";
	private static final String DESCRIPTION = "description";
	private static final String OPTIONAL = "optional";
	private static final String DOMAIN_NAME = "domainName";

	/** Loader name in CarIQ **/
	private String name;

	/** Loader description in CarIQ **/
	private String description;

	/** The rest service. */
	@Autowired
	private CarIQRestClientService restService;
	
	/** Domain helper **/
	private DomainHelper domainHelper;

	/**
	 * Parameterized constructor
	 * 
	 * @param name
	 *            name of loader in cariq
	 * @param description
	 *            description of loader
	 */
	public CarIQLoaderWrapperImpl(String name, String description) {
		this.name = name;
		this.description = description;
		this.domainHelper = new DomainHelper();
	}

	/**
	 * Get Definition of loader from CarIQ layer, This definition will be used
	 * by loader of Atom.
	 * 
	 * @return {@link CarIQLoaderDefinition}
	 */
	@Override
	public CarIQLoaderDefinition getWrappedDefinition() {

		/*********************
		 * MULTI DOMAIN CASE IS NOT HANDLED HERE
		 **********************/

		Domain_Out domain = domainHelper.getDomainOfLoggedInUser(null);
		if (domain == null)
			return null;

		CarIQLoaderDefinition loaderDefinition = getWrappedDefinitionFromDomain(domain);
		
		return loaderDefinition;
	}



	/**
	 * Call loader API and return CarIQLoaderDefinition
	 * 
	 * @param domain
	 *            domain on which this API will be called
	 * @return {@link CarIQLoaderDefinition}
	 */
	private CarIQLoaderDefinition getWrappedDefinitionFromDomain(Domain_Out domain) {

		ConfigHelper configHelper = new ConfigHelper();

		/** Make url **/
		String url = configHelper.makeUrl(GET_LOADER_DETAIL_API + name, domain.getUrl());
		try (ProfilePoint profilePoint = ProfilePoint
				.profileActivity("CarIQLoaderWrapperImpl_getWrappedDefinitionFromDomain")) {
			logger.debug("Getting wrapped definition from : " + url);

			// Call API and retrive json
			GenericJSON response = restService.get(url, configHelper.getSecurityToken(), GenericJSON.class);
			logger.debug("RESPONES: " + response.toString());
			return getDefinitionFromGenericJSON(response);
		}
	}

	/**
	 * Get Definition from Generic JSON which is retrived from cariq
	 * 
	 * @param response
	 *            {@link GenericJSON } array
	 * @return {@link CarIQLoaderDefinition}
	 */
	private CarIQLoaderDefinition getDefinitionFromGenericJSON(GenericJSON response) {
		CarIQLoaderDefinition definition = new CarIQLoaderDefinition(name, description);
		System.out.println("SERVER RESPONSE : " + response.get("attrDefinition").getClass());
		List<GenericJSON> attrArray = Utils.downCast(ArrayList.class, response.get("attrDefinition"));
		for (LinkedHashMap<String, Object> json : attrArray) {
			String name = (String) json.get(NAME);
			String description = (String) json.get(DESCRIPTION);
			boolean isOptional = (boolean) json.get(OPTIONAL);

			// create new attr and add into list
			definition.addAttrDefinition(new CarIQAttrDefinition(name, description, isOptional));
		}
		return definition;
	}

	/**
	 * Load data into CarIQ layer loader. After loading
	 * 
	 * @param json
	 *            input json to load into CarIQ
	 * @return {@link CarIQLoaderContext}
	 */
	@Override
	public CarIQLoaderContext load(GenericJSON json) {

		/*********************
		 * MULTI DOMAIN CASE IS NOT HANDLED HERE
		 **********************/

		ConfigHelper configHelper = new ConfigHelper();

		Domain_Out domain = domainHelper.getDomainOfLoggedInUser(Utils.getValue(json, DOMAIN_NAME));
		if (domain == null)
			return null;

		/** Make url **/
		String url = configHelper.makeUrl(LOAD_SINGLE_API + name, domain.getUrl());
		try (ProfilePoint profilePoint = ProfilePoint
				.profileActivity("CarIQLoaderWrapperImpl_getWrappedDefinitionFromDomain")) {
			
			// load json in cariq using post API call
			GenericJSON response = restService.post(json, url, configHelper.getSecurityToken(), GenericJSON.class);
			return getLoaderContextFromResponse(json, response);
		}
	}

	/**
	 * Create and return context from response which got from Cariq
	 * 
	 * @param response
	 *            {@link GenericJSON } response from cariq
	 * @return {@link CarIQLoaderContext }
	 */
	private CarIQLoaderContext getLoaderContextFromResponse(GenericJSON input, GenericJSON response) {
		CarIQLoaderContext context = new CarIQLoaderContext();

		// If response is null the loading task has failed
		if (response == null) {
			context.put(LOAD_STATUS, CarIQLoaderWrapper.STATUS.FAILED);
			context.addMessage("Failed to load : " + input);
			return context;
		}

		ArrayList<String> messages = Utils.downCast(ArrayList.class, response.get(MESSAGES));

		context.put(LOAD_STATUS, CarIQLoaderWrapper.STATUS.OK);

		for (String message : messages) {
			context.addMessage(message.toString());
		}
		return context;
	}

}
