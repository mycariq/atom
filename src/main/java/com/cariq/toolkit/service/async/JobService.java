package com.cariq.toolkit.service.async;

import java.util.List;

public interface JobService {
	List<GetJobJson> getJobs(int start, int pageSize);
}
