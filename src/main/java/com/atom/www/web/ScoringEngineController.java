package com.atom.www.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.atom.www.json.DrivingScoreJSON;
import com.cariq.toolkit.publicapi.CarIQPublicAPI;
import com.cariq.toolkit.publicapi.CarIQPublicAPIMethod;
import com.cariq.toolkit.service.ScoreEngineService;
import com.cariq.toolkit.utils.CarIQAPI;
import com.cariq.toolkit.utils.scoring.ScoreDefinition;

/**
 * The Class ScoringEngineController.
 *
 * @author nitin
 */
@RequestMapping("/scoringEngine")
@CarIQPublicAPI(name = "ScoringEngine", description = "Everything related to Scoring Engine")
@Controller
public class ScoringEngineController {

	/** The score engine service. */
	@Autowired
	ScoreEngineService scoreEngineService;
	
	/**
	 * Gets the driving score.
	 *
	 * @param params the params
	 * @return the driving score
	 */
	@CarIQPublicAPIMethod(description = "Get Driving Score)", responseClass = Double.class)
	@RequestMapping(value = "/drivingScore", method = RequestMethod.POST)
	@ResponseBody
	public Double getDrivingScore(@RequestBody DrivingScoreJSON params) { 
		try (CarIQAPI _getDrivingScore = new CarIQAPI("ScoringEngineController_getDrivingScore")) {
			return scoreEngineService.getDrivingScore(params);
		}
	}
	
	/**
	 * Gets the driving score definition.
	 *
	 * @return the driving score definition
	 */
	@CarIQPublicAPIMethod(description = "Get Driving Score definition)", responseClass = ScoreDefinition.class)
	@RequestMapping(value = "/drivingScoreDef", method = RequestMethod.GET)
	@ResponseBody
	public ScoreDefinition getDrivingScoreDefinition() {
		try (CarIQAPI _getDrivingScoreDefinition = new CarIQAPI("ScoringEngineController_getDrivingScoreDefinition")) {
			return scoreEngineService.getDrivingScoreDefinition();
		}
	}
}