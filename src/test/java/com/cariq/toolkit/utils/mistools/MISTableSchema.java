package com.cariq.toolkit.utils.mistools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.GenericJsonSchemable;
import com.cariq.toolkit.utils.KeyValuePair;
import com.cariq.toolkit.utils.Pair;
import com.cariq.toolkit.utils.RecursiveCollection;
import com.cariq.toolkit.utils.Utils;
import com.google.common.base.Strings;

/**
 * 
 * @author hrishi
 *
 */
public class MISTableSchema implements GenericJsonSchemable {
	String heading, style;

	// List of Sections or definitions applicable to this table.
//	List<MISSectionDefinition> applicableSectionDefs = new ArrayList<MISSectionDefinition>();

	List<ApplicableSection> applicableSections = new ArrayList<ApplicableSection>();
	// Collection of SectionGroups and Definitions under it in hierarchical fashion
	RecursiveCollection<Pair<MISSectionGroup,String>, MISSectionDefinition> rowSection;

	MISSectionGroup columnsGroup; // Top Column if null, only GrandTotal column

	public MISTableSchema(String heading, List applicableSectionsInput, List sectionGroupsWithStyle,
			String columnsGroupDef) {
		super();
		this.heading = heading;
		this.style = "header-black"; // TODO Hardcoded for now

		// Applicable Section Definitions - e.g. NI + NC + ...
//		this.applicableSections = applicableSections;
		if (!Utils.isNullOrEmpty(applicableSectionsInput)) {
			// applicableSectionNames can be a String or a list.
			for (Object applicableSectionName : applicableSectionsInput) {
				ApplicableSection appSection = Utils.downCast(ApplicableSection.class, applicableSectionName);
				if (appSection == null) {
					MISSectionDefinition def = MISSectionGroup.lookupDefinition(applicableSectionName.toString());
					if (def != null) {
						appSection = new ApplicableSection(def.getItsGroup().getKey(), Arrays.asList(def.getValue()));
					}
				}
				
				// add it to sections list
				if (appSection != null)
					applicableSections.add(appSection);
			}
		}

		// Row sections
		if (!Utils.isNullOrEmpty(sectionGroupsWithStyle)) {

			RecursiveCollection<Pair<MISSectionGroup,String>, MISSectionDefinition> prev = null;

			// Process the rows Section by going through the groups - looks like recursion
			// would be necessary.
			for (Object sectionGroupAndStyle : sectionGroupsWithStyle) {
				GroupStyle grpStyle = Utils.downCast(GroupStyle.class, sectionGroupAndStyle);
				if (grpStyle == null)
					grpStyle = new GroupStyle(sectionGroupAndStyle.toString());
					
				RecursiveCollection<Pair<MISSectionGroup,String>, MISSectionDefinition> grp = new RecursiveCollection<Pair<MISSectionGroup,String>, MISSectionDefinition>(
						new Pair<MISSectionGroup,String>(MISSectionGroup.lookupGroup(grpStyle.getGroupName()),  grpStyle.getStyle()),
						MISSectionGroup.lookupGroup(grpStyle.getGroupName()).getItsSections());

				if (prev != null)
					grp.setPrevious(prev);

				if (rowSection == null)
					rowSection = grp;

				prev = grp;
			}
		}

		// Columns Definition
		if (!Strings.isNullOrEmpty(columnsGroupDef)) {
			this.columnsGroup = MISSectionGroup.lookupGroup(columnsGroupDef);
		}
	}

	@Override
	public GenericJSON getSchema() {
		String evaluatorClass = Utils.isNullOrEmpty(applicableSections) ? MISSchemaGenerator.DefaultOnEvaluator
				: MISSchemaGenerator.KeyValueListEvaluator;
		
		return GenericJSON.build("heading", heading, "style", style, "evaluatorClass", evaluatorClass, "argumentJSON",
				getArgumentList(applicableSections), "rows", generateRowsSchemas(), "columns",
				generateColumnsSchemas());
	}

	private static List<GenericJSON> getArgumentList(List<ApplicableSection> applicableSections) {
		List<GenericJSON> retval = new ArrayList<GenericJSON>();

		if (!Utils.isNullOrEmpty(applicableSections)) {
			for (ApplicableSection applicableSection : applicableSections) {
				MISSectionGroup grp = MISSectionGroup.lookupGroup(applicableSection.getSectionGroup());

				// setting without looking up - may get into random values but that can be seen
				retval.add(GenericJSON.build(KeyValuePair.KEY, grp.getKey(), KeyValuePair.VALUE,
						applicableSection.getStringDefList()));
			}
		}

		return retval;
	}

	private List<GenericJSON> generateColumnsSchemas() {
		List<GenericJSON> retval = new ArrayList<GenericJSON>();
		// first Row
		retval.add(new MISColumnSchema("sub-header", "Row Labels", MISSchemaGenerator.DefaultOffEvaluator, null)
				.getSchema());

		if (columnsGroup != null) {
			List<MISSectionDefinition> sectionDefinitions = columnsGroup.getItsSections();
			for (MISSectionDefinition misSectionDefinition : sectionDefinitions) {
				retval.add(new MISColumnSchema(null, misSectionDefinition.getLabel(), 
						MISSchemaGenerator.KeyValueListEvaluator,
						MISSchemaHelper.getArgumentKeyValuePairs(Arrays.asList(misSectionDefinition))).getSchema());

			}
		}
		// last Row
		retval.add(new MISColumnSchema("grand-total", "Grand Total", MISSchemaGenerator.DefaultOnEvaluator, null)
				.getSchema());

		return retval;
	}

	private List<GenericJSON> generateRowsSchemas() {
		List<GenericJSON> retval = new ArrayList<GenericJSON>();
		// first Row
		retval.add(
				new MISRowSchema("sub-header", "Row Labels", MISSchemaGenerator.DefaultOffEvaluator, null).getSchema());

		// Recurse and generate the necessary stuff
		List<GenericJSON> sectionRows = new ArrayList<GenericJSON>();
		
		processSections(rowSection, new ArrayList<KeyValuePair>(), sectionRows);
		
		retval.addAll(sectionRows);
		
		// last Row
		retval.add(new MISRowSchema("grand-total", "Grand Total", MISSchemaGenerator.DefaultOnEvaluator, null)
				.getSchema());

		return retval;
	}

	private void processSections(RecursiveCollection<Pair<MISSectionGroup,String>, MISSectionDefinition> sectionGroup, List<KeyValuePair> keyValues, List<GenericJSON> sectionRows) {
		Pair<MISSectionGroup,String> groupStyle = sectionGroup.getCurrent();
		for (MISSectionDefinition misSectionDefinition : sectionGroup.getItems()) {
			KeyValuePair keyVal = misSectionDefinition.getKeyValuePair();
			keyValues.add(keyVal);

			sectionRows.add(new MISRowSchema(groupStyle.getSecond(), misSectionDefinition.getLabel(), 
							MISSchemaGenerator.KeyValueListEvaluator,
							keyValues).getSchema());
			
			RecursiveCollection<Pair<MISSectionGroup,String>, MISSectionDefinition> nextGroup = sectionGroup.getNext();

			if (nextGroup != null)
				processSections(nextGroup, keyValues, sectionRows);
			
			// Pop my keyvalue
			keyValues.remove(keyValues.size() - 1);
		}

	}
}
