package com.cariq.toolkit.coreiq.exporter;

import com.cariq.toolkit.utils.GenericJSON;

/**
 * @author hrishi
 * Basic simple class to give Exporter Work to Workitem system
 * for sequential execution
 */
public class ExporterWorkDefinition {
	String exporter;
	String queryType;
	String fileType;
	GenericJSON json;

	// Oversimplified basic one
	public ExporterWorkDefinition(String exporter) {
		this(exporter, "All", "csv", new GenericJSON());
	}
	
	public ExporterWorkDefinition(String exporter, String queryType, String fileType, GenericJSON json) {
		super();
		this.exporter = exporter;
		this.queryType = queryType;
		this.fileType = fileType;
		this.json = json;
	}

	public String getExporter() {
		return exporter;
	}

	public String getQueryType() {
		return queryType;
	}

	public String getFileType() {
		return fileType;
	}

	public GenericJSON getJson() {
		return json;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((exporter == null) ? 0 : exporter.hashCode());
		result = prime * result + ((fileType == null) ? 0 : fileType.hashCode());
		result = prime * result + ((json == null) ? 0 : json.hashCode());
		result = prime * result + ((queryType == null) ? 0 : queryType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExporterWorkDefinition other = (ExporterWorkDefinition) obj;
		if (exporter == null) {
			if (other.exporter != null)
				return false;
		} else if (!exporter.equals(other.exporter))
			return false;
		if (fileType == null) {
			if (other.fileType != null)
				return false;
		} else if (!fileType.equals(other.fileType))
			return false;
		if (json == null) {
			if (other.json != null)
				return false;
		} else if (!json.equals(other.json))
			return false;
		if (queryType == null) {
			if (other.queryType != null)
				return false;
		} else if (!queryType.equals(other.queryType))
			return false;
		return true;
	}
}
