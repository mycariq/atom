package com.cariq.toolkit.coreiq.tableUtil.application.mis;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.cariq.toolkit.coreiq.tableUtil.application.mis.MISProcessingContext.ContextSetter;
import com.cariq.toolkit.coreiq.tableUtil.impl.Report;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.TableIfc;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.GenericJsonable;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.jsonProcessor.JSONLoadable;
import com.cariq.toolkit.utils.jsonProcessor.JSONObjectBuilder;
import com.cariq.toolkit.utils.jsonProcessor.JSONPath;
import com.cariq.toolkit.utils.jsonProcessor.JSONProcessorContext;
import com.cariq.toolkit.utils.jsonProcessor.JSONProcessorTemplate;

/**
 * Main class Driving MIS Report
 * @author hrishi
 *
 */
public class MISReport extends Report implements MISProcessable, GenericJsonable {
	public static final String MISREPORT = "MISREPORT";
	public static final String MISTABLE = "MISTABLE";
	public static final String MISROW = "MISROW";
	public static final String MISCOLUMN = "MISCOLUMN";
	
	public MISReport() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MISReport(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public MISReport process(MISProcessingContext context, GenericJSON json) {
		try (ContextSetter<MISReport> _setter = context.getContextSetter(this)) {

			for (TableIfc tableIfc : super.getTables()) {
				MISTable table = Utils.downCast(MISTable.class, tableIfc);
				table.process(context, json);
			}

			return this;
		}
	}

	// inner class to load the Report
	class MISReportLoader extends JSONLoadable {
		/**
		 * MISReport is loadable from json file
		 */
		@Override
		protected void registerHandlers(JSONObjectBuilder builder) {
			// tables array
			builder.subscribe("report", new JSONProcessorTemplate() {
				@Override
				protected void doProcess(JSONProcessorContext context, JSONPath node, Object obj) {
					// first attributes
					JSONObject reportJSON = (JSONObject) obj;

					// set packet sub types with associate type
					JSONArray tableArray = (JSONArray) reportJSON.get("tables");
					for (int i = 0; i < tableArray.size(); i++) {
						JSONObject tableJSON = (JSONObject) tableArray.get(i);
						GenericJSON tableGenericJSON = Utils.getJSonObject(tableJSON.toString(), GenericJSON.class);

						MISTable tbl = new MISTable();
						tbl.fromGenricJSON(tableGenericJSON);
						add(Report.TABLE, tbl);
					}
				}
			});
		}
	}
	
	public JSONLoadable getJSONLoadable() {
		return new MISReportLoader();
	}
}
