package com.cariq.toolkit.utils.wwwservice.api;

import com.cariq.toolkit.utils.wwwservice.api.W3Query.W3QueryJson;
import com.cariq.toolkit.utils.wwwservice.api.W3QueryResult.W3QueryResultJson;

public interface W3QueryInterface {
	W3QueryResultJson executeQuery(W3QueryJson query);
}
