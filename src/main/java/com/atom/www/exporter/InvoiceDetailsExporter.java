/**
 * 
 */
package com.atom.www.exporter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import com.cariq.toolkit.coreiq.CarIQRawQuery;
import com.cariq.toolkit.coreiq.exporter.CarIQExporterTemplate;
import com.cariq.toolkit.coreiq.exporter.QueryDefinitionAttribute;
import com.cariq.toolkit.model.Invoice;
import com.cariq.toolkit.model.Timeline;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * The Class InvoiceDetailsExporter.
 *
 * @author santosh
 */
public class InvoiceDetailsExporter extends CarIQExporterTemplate {

	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("InvoiceDetailsExporter");

	public static final String EXPORTER = "InvoiceDetailsExporter";

	private static final LinkedHashMap<String, String> attributeAliasMap = new LinkedHashMap<String, String>();
	private static final List<String> tableAliasArray = new ArrayList<>();

	private static final String reportId = "reportId", tableId = "tableId", billingTerm = "billingTerm",
			billingMonth = "billingMonth", carConnected = "carConnected", deviceShipped = "deviceShipped",
			percentage = "percentage";

	public static final String BY_REPORT = "ByReport", REPORT_ID = "reportId", BY_LATEST_REPORT = "ByLatestREport";

	static {
		attributeAliasMap.put("invoice.id", reportId);
		attributeAliasMap.put("invoiceUnit.id", tableId);
		attributeAliasMap.put("billingTerm.name", billingTerm);
		attributeAliasMap.put("invoiceUnit.billing_month", billingMonth);
		attributeAliasMap.put("json_extract(invoiceUnit.info_json, '$.Car Connected')", carConnected);
		attributeAliasMap.put("json_extract(invoiceUnit.info_json, '$.Devices Shipped')", deviceShipped);
		attributeAliasMap.put("json_extract(invoiceUnit.info_json, '$.Percentage')", percentage);

		tableAliasArray.add("invoice invoice,");
		tableAliasArray.add("invoice_unit invoiceUnit,");
		tableAliasArray.add("billing_term billingTerm");

	}

	public InvoiceDetailsExporter() {
		super(EXPORTER, "Invoice Details Exporter");

		// Declare Export attributes
		addExportAttributes(new ArrayList(attributeAliasMap.values()));

		addQueryType(BY_REPORT, "Get Invoice details by given report id",
				Arrays.asList(new QueryDefinitionAttribute(REPORT_ID, "report id", String.class)));
		
		addQueryType(BY_LATEST_REPORT, "Get Invoice details by latest report id", new String[] {});

		// Support roles
		addSupportedRoles(Arrays.asList(Utils.ADMIN_ROLE));
	}

	@Override
	protected List<GenericJSON> doExport(String queryType, GenericJSON json, int pageNo, int pageSize) {
		try (ProfilePoint _doExport = ProfilePoint.profileAction("InvoiceDetailsExporter_doExport")) {

			CarIQRawQuery qry = getExportQuery(queryType, json, pageNo, pageSize);
			return qry.getResult();
		}
	}

	private CarIQRawQuery getExportQuery(String queryType, GenericJSON json, int pageNo, int pageSize) {
		try (ProfilePoint _getExportQuery = ProfilePoint.profileAction("InvoiceDetailsExporter_getExportQuery")) {
			CarIQRawQuery qry = new CarIQRawQuery("getInvoiceDetailsExportQuery", Timeline.entityManager(),
					attributeAliasMap, tableAliasArray);
			qry.addRawCondition(
					"invoice.id = invoiceUnit.its_invoice and invoiceUnit.its_billing_term = billingTerm.id");
			
			if (BY_LATEST_REPORT.equals(queryType)) {
				Invoice invoice = Invoice.getLatestInvoice();
				Utils.checkNotNull(invoice, "invoice not available!");

				qry.addRawCondition("invoice.id = " + invoice.getId());
			} else if (BY_REPORT.equalsIgnoreCase(queryType)) {
				Long reportId = Utils.getValue(Long.class, json, REPORT_ID);
				Utils.checkNotNull(reportId, "Please provide report Id");

				qry.addRawCondition("invoice.id = " + reportId);
			} else
				Utils.handleException("Invalid Query Type: " + queryType);

			qry.orderBy("invoiceUnit.billing_month", "ASC");
			qry.setPaging(pageNo, pageSize);

			return qry;
		}
	}
}
