package com.cariq.toolkit.utils;

/**
 * Basic Pair of Key and Value
 * @author hrishi
 *
 */
public class KeyValuePair extends Pair<String, String> implements GenericJsonSchemable, GenericJsonable {
	public static final String KEY = "key";
	public static final String VALUE = "value";
	
	public KeyValuePair(String first, String second) {
		super(first, second);
	}

	String key, value;
	
	@Override
	public GenericJSON toGenericJSON() {
		return GenericJSON.build(KEY, getFirst(), VALUE, getSecond());
	}

	@Override
	public void fromGenricJSON(GenericJSON json) {
		super.first = Utils.downCast(String.class, json.get(KEY));
		super.second = Utils.downCast(String.class, json.get(VALUE));
	}

	@Override
	public GenericJSON getSchema() {
		return toGenericJSON();
	}

}
