package com.atom.www.loader;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import com.cariq.toolkit.coreiq.exporter.CarIQExporterService;
import com.cariq.toolkit.coreiq.exporter.ExporterWorkDefinition;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderContext;
import com.cariq.toolkit.coreiq.loader.CarIQLoaderTemplate;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

@Configurable
public abstract class MultipleExporterAsyncLoader extends CarIQLoaderTemplate {
	/** The logger. */
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("MultipleExporterAsyncLoader");

	public static final String TheLoader = "MultipleExporterAsyncLoader";

	/** Exporter Service to do the async download job **/
	@Autowired
	private CarIQExporterService exporterService;
	
	
	public MultipleExporterAsyncLoader(String name, String description) {
		super(name, description);
	}


	/** 
	 * MIS generation is typically a Series of Exporters run in async
	 * This is list of exporters needed for particular operation.
	 */
	protected abstract List<ExporterWorkDefinition> getExporterWorkerDefinitions(GenericJSON json);

	@Override
	protected void doLoad(GenericJSON json, CarIQLoaderContext context) {
		//	This loader just creates a chain of WorkItems and fires it
		List<ExporterWorkDefinition> exporterWorkDefinitions = getExporterWorkerDefinitions(json);

		if (Utils.isNullOrEmpty(exporterWorkDefinitions))
			Utils.handleException("No definition found for MIS: " + this.getName());
		
		
		context.addMessage(exporterService.chainAsyncExporters(exporterWorkDefinitions).toString());
	}

}
