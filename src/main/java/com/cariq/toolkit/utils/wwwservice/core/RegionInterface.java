/**
 * 
 */
package com.cariq.toolkit.utils.wwwservice.core;

/**
 * @author hrishi
 *
 */
public interface RegionInterface {
	public static final double EPSILON_AREA = 0.01*0.01; // less than 10 meter x 10  meter 

	/**
	 * @return the lowerLeft
	 */
	public GeoTile getLowerLeft();
	
	/**
	 * @return the upperRight
	 */
	public GeoTile getUpperRight();
	/**
	 * Check if Tile is inside the region
	 * 
	 * @param tile
	 * @return
	 */
	public boolean isInside(GeoTile tile);

	/**
	 * Find are of the region in Square Kilometers
	 * 
	 * @return
	 */
	public double getArea();

	/**
	 * Get Span of the region in horizontal direction on map i.e. EastWest
	 * 
	 * @return
	 */
	public double getEastWestSpan();
	/**
	 * Get Span of the region in vertical direction on map i.e. NorthSouth
	 * 
	 * @return
	 */
	public double getNorthSouthSpan();
}
