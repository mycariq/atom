/**
 * 
 */
package com.cariq.toolkit.model.async;

/**
 * @author hrishi
 *
 */
public enum WorkItemStatus {
	CREATED, READY, RUNNING, FAILED, COMPLETED, EXPIRED;
}