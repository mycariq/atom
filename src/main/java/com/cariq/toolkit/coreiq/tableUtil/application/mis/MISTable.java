package com.cariq.toolkit.coreiq.tableUtil.application.mis;

import java.util.List;

import com.cariq.toolkit.coreiq.tableUtil.application.mis.MISProcessingContext.ContextSetter;
import com.cariq.toolkit.coreiq.tableUtil.impl.Table;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.CellIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.CellValueAribiterIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.ColumnIfc;
import com.cariq.toolkit.coreiq.tableUtil.interfaces.RowIfc;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Utils;

public class MISTable extends Table implements MISProcessable {

	CellValueAribiterIfc arbiter;
	
	
	public MISTable() {
		super();
	}

	public MISTable(String header, String evaluatorClass, String evaluatorArgJSON) {
		super(header);

		arbiter = MISHelper.constructArbiter(evaluatorClass, evaluatorArgJSON);
	}

	@Override
	public MISTable process(MISProcessingContext context, GenericJSON json) {
		if (!arbiter.isApplicable(json))
			return this;

		try (ContextSetter<MISTable> _setter = context.getContextSetter(this)) {

			for (RowIfc rowIfc : super.getRows()) {
				MISRow misRow = Utils.downCast(MISRow.class, rowIfc);
				misRow.process(context, json);
			}
			// TODO Auto-generated method stub
			return this;
		}
	}

	@Override
	public Table add(String parameter, Object value) {
		// TODO Auto-generated method stub
		return super.add(parameter, value);
	}

	@Override
	public MISTable build() {
		// Construct the cells 
		int rowNum = 0;
		for (RowIfc row : getRows()) {
			int colNum = 0;
			for (ColumnIfc column : getColumns()) {
				MISRow misRow = Utils.downCast(MISRow.class, row);
				MISColumn misCol = Utils.downCast(MISColumn.class, column);
				Utils.checkNotNull(misRow, "Non MISRow in MIS Table", misCol, "Non MISColumn in MIS Table");
				
				CellIfc cell = new MISCell(row, column);
				if (rowNum == 0)
					cell.setValue(misCol.getLabel());
				if (colNum == 0)
					cell.setValue(misRow.getLabel());

				row.add(cell);
				column.add(cell);
				colNum++;
				
			}
			rowNum++;
		}
		return this;
	}

	@Override
	public void fromGenricJSON(GenericJSON json) {
		heading = Utils.getValue(String.class, json, "heading");
		
		String evaluatorClass = Utils.getValue(String.class, json, "evaluatorClass");
		Object argument = json.get("argumentJSON");
		
		List argumentList = Utils.getJSonObject(Utils.getJSonString(argument), List.class);

		this.arbiter = MISHelper.constructArbiter(evaluatorClass, argumentList);
		
		List rows = Utils.getValue(List.class, json, "rows");
		List<GenericJSON> rowsJSONs = Utils.getJSonObjectList(rows.toString(), GenericJSON.class);

		for (GenericJSON rowsJSON : rowsJSONs) {
//			GenericJSON rowsJSON = Utils.jsonO
			MISRow row = new MISRow();
			row.fromGenricJSON(rowsJSON);
			add(Table.ROW, row);
		}
		
		// Does it need to be done a little differently?
		List columns = Utils.getValue(List.class, json, "columns");
		List<GenericJSON> columnsJSONs = Utils.getJSonObjectList(columns.toString(), GenericJSON.class);

		for (GenericJSON columnsJSON : columnsJSONs) {
//			GenericJSON rowsJSON = Utils.jsonO
			MISColumn column = new MISColumn();
			column.fromGenricJSON(columnsJSON);
			add(Table.COLUMN, column);
		}
		
		// finally build
		build();
	}
}
