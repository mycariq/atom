package com.cariq.toolkit.utils.analytics.core;

public interface AnalyticsVisitor {

	void visit(AnalyticsVisitable visitable, String className);

}
