package com.cariq.toolkit.utils;

/**
 * Interface for comparing with Proximity.
 * It is often not sufficient to know how the two objects compare, 
 * but it is necessary to now if they are in close proximity with respect to time or other parameter.
 * 
 * @author HVN
 *
 * @param <T>
 */
public interface ProximityComparable<T> extends Comparable<T> {

}
