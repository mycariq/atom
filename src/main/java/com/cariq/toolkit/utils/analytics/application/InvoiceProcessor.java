package com.cariq.toolkit.utils.analytics.application;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.atom.www.helper.MISReportHelper;
import com.cariq.toolkit.model.BillingTerm;
import com.cariq.toolkit.model.Invoice;
import com.cariq.toolkit.model.Invoice.InvoiceStatusEnum;
import com.cariq.toolkit.model.InvoiceUnit;
import com.cariq.toolkit.model.InvoiceUnit.InvoiceUnitStatusEnum;
import com.cariq.toolkit.utils.CarIQFileUtils;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.Month;
import com.cariq.toolkit.utils.ObjectProcessor;
import com.cariq.toolkit.utils.ObjectProcessorTemplate;
import com.cariq.toolkit.utils.Utils;
import com.cariq.toolkit.utils.profile.ProfilePoint;

public class InvoiceProcessor extends ObjectProcessorTemplate<Invoice, GenericJSON> {
	private static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("InvoiceProcessor");
	
	public static final String INVOICEDOCUMENT_VM = "velocity/billing_invoice.vm";
	
	Month startMonth, endMonth;
	List<BillingTerm> billingTerms = new ArrayList<BillingTerm>();
	List<InvoiceUnit> invoiceUnits = new ArrayList<InvoiceUnit>();
	
	public InvoiceProcessor(Invoice invoice) {
		super(invoice);
		initializeContext();
	}
	
	private void initializeContext() {			
		String fromMonth = getModel().getStartMonth();
		String toMonth = getModel().getEndMonth();
		
		startMonth = new Month(fromMonth, Utils.INDIA_TIME_ZONE);
		endMonth = new Month(toMonth, Utils.INDIA_TIME_ZONE);
		
		String billingTermsList = getModel().getBillingTerms();
		for (String bt : billingTermsList.split(",")) {
			BillingTerm term = BillingTerm.getByName(bt);
			if (null == term)
				Utils.handleException("Invalid billing term: " + bt);
			billingTerms.add(term);
		}
		
		logger.debug("*** InvoiceProcessor.initializeContext(): startMonth: " 
				+ startMonth.getName() + ", endMonth: " + endMonth.getName() + ", billingTerms: " + billingTerms.size());
	}
	
	@Override
	public void doStartPrepare() {
		try (ProfilePoint _doStartPrepare = ProfilePoint.profileAction("ProfAction__doStartPrepare_" + this.getClass().getName())) {
			logger.debug("*** InvoiceProcessor: doStartPrepare()....");

			// Create InvoiceUnits		
			Month monthIterator = startMonth; 
			while (monthIterator.isBefore(endMonth.getEndTime())) {
				for (BillingTerm bt : billingTerms) {
					logger.debug("Creating InvoiceUnit for: " + monthIterator.getYear() + "-" + monthIterator.getMonth());
					InvoiceUnit iu = new InvoiceUnit(getModel(), bt, monthIterator.getName(), null, null, 
							InvoiceUnitStatusEnum.IN_PROGRESS.toString(), new Date(), new Date());
					iu.persist();
					invoiceUnits.add(iu);
				}
				monthIterator = monthIterator.getNextsMonth();
			}
			
			// Create InvoiceUnit Processors
			for (InvoiceUnit iu : invoiceUnits) {
				InvoiceUnitProcessor iuProcessor = new InvoiceUnitProcessor(iu);
				addChildProcessor(iuProcessor);
			}
			logger.debug("*** Created " + invoiceUnits.size() + " iuProcessors for invoice: " + getModel().getId());
		}
	}

	private String getURLToDocument(Invoice invoice) {
		try {
			if (null == invoice)
				Utils.handleException("***!!! getURLToDocument: got null invoice!");	
			
			// Get genericJSON from invoice and use the right VM file to render
			GenericJSON invoiceJSON = new GenericJSON();
			invoiceJSON.add("Invoice", invoice.toGenericJSON());
			
			String reportFileName = "BillingDetailsID" + invoice.getId() + "_" + invoice.getAgainstAccount().getName() 
					+ "_" + Utils.createDateId(true) + ".html";
			logger.debug("*** Invoice filename: " + reportFileName);
			MISReportHelper reportHelper = new MISReportHelper();
			try {
				String domainName = invoice.getItsBillingOperation().getItsDomain().getName();
				String userName = invoice.getItsBillingOperation().getItsUser().getUsername();
				logger.debug("*** current domain: " + domainName);
				GenericJSON inputJSON = GenericJSON.build("domain", domainName, "username", userName);
				reportHelper.configureDomainUser(inputJSON);
			} catch (Exception e) {
				Utils.handleException("Failed to get current domain for billingOperation: " + invoice.getItsBillingOperation().getId());
			}
			
			String localHTMLFilePath = reportHelper.convertJSONToHTMLFile(invoiceJSON.toString(), reportFileName, INVOICEDOCUMENT_VM);
			
			if (! CarIQFileUtils.exists(localHTMLFilePath)) {
				Utils.handleException("Failed to create HTML Invoice document from JSON");
			}			
			logger.debug("*** Invoice localHTMLFilePath: " + localHTMLFilePath);
//			return localHTMLFilePath;
			
			GenericJSON reportDetails = GenericJSON.build("InvoiceID", invoice.getId(), "BillingOperation", invoice.getItsBillingOperation().getId(),
					"Contract", invoice.getItsBillingContract().getName(), "BillingFrom" + invoice.getStartMonth() + "BillingTo" + invoice.getEndMonth(),
					"BillingTerms" + invoice.getBillingTerms());
			logger.debug("===>reportDetails: " + reportDetails.toString());
			GenericJSON outJSON = reportHelper.exportHTMLReport("BillingDetails", reportFileName, localHTMLFilePath, reportFileName, reportDetails.toString());
			logger.debug("*** outJSON" + outJSON.toString());
			return (Utils.getValue(outJSON, "ExportedDataURL"));
		} catch(Exception e) {
			logger.debug("*** !!! Exception while creating invoice document: " + e.getMessage());
		}
		
		return null;
	}
	
	@Override
	public void doEndFinish() {		
		try (ProfilePoint _doStartPrepare = ProfilePoint.profileAction("ProfAction__doEndFinish_" + this.getClass().getName())) {
			logger.debug("*** InvoiceProcessor: doEndFinish()....");
			Long totalAmountToBeInvoiced = (long) 0;
			Long totalBillablesToBeInvoiced = (long) 0;
			Integer validIUs = 0;
			
			// check invoice units
			for (InvoiceUnit iu : invoiceUnits) {		
				if (iu.getStatus().equals(InvoiceUnitStatusEnum.READY.toString())) {
					totalAmountToBeInvoiced += iu.getAmount();
					totalBillablesToBeInvoiced += iu.getNumberOfBillables();
					logger.debug("*** Invoice unit Ready to be billed: " + iu.toString());
					validIUs++;
				} else {
					logger.debug("***!!! Invalid invoice unit: " + iu.toString());
				}
			}
			
			// Update invoice
			if (totalAmountToBeInvoiced > 0 && totalBillablesToBeInvoiced > 0) {
				getModel().setAmount(totalAmountToBeInvoiced);
				getModel().setStatus(InvoiceStatusEnum.READY.toString());				
				// Create urlToDocument.
				String urlToDocument = getURLToDocument(getModel());
				getModel().setUrlToDocument(urlToDocument);
				getModel().persist();
				logger.info("Invoice# " + getModel().getId() + " is ready for review. Total amount invoiced is Rs " + getModel().getAmount() 
					+ ", including " + validIUs + " invoice_units with total number of billables: " + totalBillablesToBeInvoiced);
			} else { // Invalid case	
				logger.info("Invoice# " + getModel().getId() + " is NOT valid for review. Total amount Rs " + getModel().getAmount() 
				+ ", Total billables included: " + totalBillablesToBeInvoiced);
				throw new RuntimeException("INVALID_INVOICE");
			}
			getModel().persist();
		}
	}
	
	@Override
	public void doProcessChildFinishException(ObjectProcessor invoiceUnitProcessor, RuntimeException e) {
		logger.debug("*** InvoiceUnitProcessor: doProcessChildFinishException()...");
		logger.debug("*** Caught exception: " + e.getMessage());
		invoiceUnitProcessor.destroyModel();
	}
	
	@Override
	public void destroyModel() {
		try (ProfilePoint _destroyModel = ProfilePoint.profileAction("ProfAction__destroyModel_" + this.getClass().getName())) {
			logger.debug("***!!! InvoiceProcessor.destroyModel()...");
			Invoice invoice = getModel();
			logger.debug("*** Deleting invoice: " + invoice.getId());
			invoice.remove();
		}
	}
	
}
