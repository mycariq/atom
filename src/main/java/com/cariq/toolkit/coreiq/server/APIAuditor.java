/**
 * 
 */
package com.cariq.toolkit.coreiq.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.Interval;

import com.atom.www.helper.SimpleTimeSlot;
import com.atom.www.helper.TimeSlot;
import com.cariq.toolkit.coreiq.CompletionCallback;
import com.cariq.toolkit.utils.CarIQLogger;
import com.cariq.toolkit.utils.CarIQToolkitHelper;
import com.cariq.toolkit.utils.GenericJSON;
import com.cariq.toolkit.utils.GenericJsonable;
import com.cariq.toolkit.utils.Utils;

/**
 * The Class APIAuditor.
 *
 * @author hrishi
 */
public class APIAuditor {
	/** The logger. */
	static CarIQLogger logger = CarIQToolkitHelper.logger.getLogger("APIAuditor");

	static final String API_AUDIT = "API_AUDIT";
	
	/**
	 * Class to capture the API Call count, frequency and time spent on it.
	 *
	 * @author hrishi
	 */
	public static class APIAudit implements GenericJsonable {
		
		/** The api name. */
		String apiName;
		
		/** The call count. */
		int callCount;
		
		/** The total time. */
		long totalTime;
		
		/** The average time ms. */
		double averageTimeMS;
		
		/** The max time ms. */
		long maxTimeMS;
		
		/**
		 * Instantiates a new API audit.
		 *
		 * @param apiName the api name
		 * @param elapsed the elapsed
		 */
		public APIAudit(String apiName, long elapsed) {
			this.apiName = apiName;
			callCount = 1;
			averageTimeMS = elapsed;
			maxTimeMS = elapsed;
			totalTime = elapsed;
		}

		/**
		 * @param apiName
		 * @param callCount
		 * @param totalTime
		 * @param averageTimeMS
		 * @param maxTimeMS
		 */
		APIAudit(String apiName, int callCount, long totalTime, double averageTimeMS, long maxTimeMS) {
			this.apiName = apiName;
			this.callCount = callCount;
			this.totalTime = totalTime;
			this.averageTimeMS = averageTimeMS;
			this.maxTimeMS = maxTimeMS;
		}


		/**
		 * Register.
		 *
		 * @param durationMS the duration ms
		 */
		// Two calls of same API cannot execute at the same time - hence synchro
		public synchronized void register(long durationMS) {
			totalTime += durationMS;
			callCount++;
			averageTimeMS = 1.0 * totalTime/callCount;
			
			if (maxTimeMS < durationMS)
				maxTimeMS = durationMS;
			
			callCount++;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "APIAudit [apiName=" + apiName + ", callCount=" + callCount + ", totalTime=" + totalTime
					+ ", averageTimeMS=" + averageTimeMS + ", maxTimeMS=" + maxTimeMS + "]";
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((apiName == null) ? 0 : apiName.hashCode());
			long temp;
			temp = Double.doubleToLongBits(averageTimeMS);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			result = prime * result + callCount;
			result = prime * result + (int) (maxTimeMS ^ (maxTimeMS >>> 32));
			temp = Double.doubleToLongBits(totalTime);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			return result;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			APIAudit other = (APIAudit) obj;
			if (apiName == null) {
				if (other.apiName != null)
					return false;
			} else if (!apiName.equals(other.apiName))
				return false;
			if (Double.doubleToLongBits(averageTimeMS) != Double.doubleToLongBits(other.averageTimeMS))
				return false;
			if (callCount != other.callCount)
				return false;
			if (maxTimeMS != other.maxTimeMS)
				return false;
			if (Double.doubleToLongBits(totalTime) != Double.doubleToLongBits(other.totalTime))
				return false;
			return true;
		}

		@Override
		public GenericJSON toGenericJSON() {
			GenericJSON json = new GenericJSON();
			json.put("apiName", apiName);
			json.put("callCount", callCount);
			json.put("totalTime", totalTime);
			json.put("averageTimeMS", averageTimeMS);
			json.put("maxTimeMS", maxTimeMS);
			
			return json;
		}

		@Override
		public void fromGenricJSON(GenericJSON json) {
			// TODO Auto-generated method stub
			
		}
	}
	
	/**
	 * The Class APIAuditRegistry.
	 * This is the main driver class responsible to manage the API life cycle
	 */
	public static class APIAuditRegistry {		
		static final SimpleTimeSlot.Interval DumpInterval = SimpleTimeSlot.Interval.HOUR;
//		static final TimeSlot.Interval DumpInterval = Interval.MINUTE; // for Test
		
		/** The instantiation lock for lazy instantiation. */
		static Object instantiationLock = new Object();
		
		/** The _registry. */
		static APIAuditRegistry _registry = null;
		
		/**
		 * Gets the single instance of APIAuditRegistry.
		 *
		 * @return single instance of APIAuditRegistry
		 */
		public static APIAuditRegistry getInstance() {
			if (_registry != null)
				return _registry;
			
			synchronized(instantiationLock) {
				// Double check to avoid multi-instance
				if (_registry != null)
					return _registry;
				
				_registry = new APIAuditRegistry();
			}

			return _registry;
		}
		
		/** The slot. */
		// Instance method
		SimpleTimeSlot slot = new SimpleTimeSlot(DumpInterval, new Date());
		
		/** The audit lock. */
		Object auditLock = new Object();
		
		/** The time slot lock. */
		Object timeSlotLock = new Object();
		
		/** The audit map. */
		Map<String, APIAudit> auditMap = new HashMap<String, APIAudit>();
		
		
		/**
		 * Checks if is time slot expired. Based on Expiry of timeslot, the logging/dumping is done
		 *
		 * @return true, if is time slot expired
		 */
		private boolean isTimeSlotExpired() {
			return slot.contains(new Date()) == false;
		}
		
		/**
		 * Flush.
		 *
		 * @param callback the callback
		 */
		@SuppressWarnings({ "rawtypes", "unchecked" })
		private void flush(CompletionCallback callback) {
			// @ICICI_HOTFIX
			if (true)
				return;
			
			Set<APIAudit> apiAuditSet = new HashSet<APIAudit>();
			// Get top 3 by Time
			apiAuditSet.addAll(this.getTopAPIs(TOP_DURATION, 3));
			
			// Get top 3 by Count
			apiAuditSet.addAll(this.getTopAPIs(TOP_COUNT, 3));

			// Add total summary
			apiAuditSet.add(getSummaryAudit());
			
			callback.onComplete(new ArrayList<APIAudit>(apiAuditSet));
			
			// destroy map - Garbage collect everything
			auditMap.clear();
		}
		
		/**
		 * Register.
		 *
		 * @param apiName the api name
		 * @param elapsed the elapsed
		 * @param callback the callback
		 */
		@SuppressWarnings("rawtypes")
		void register(String apiName, long elapsed, CompletionCallback callback) {
			APIAudit audit = auditMap.get(apiName);
			if (audit == null) {
				synchronized(auditLock) {
					audit = auditMap.get(apiName);
					if (audit == null) {
						audit = new APIAudit(apiName, elapsed);
						auditMap.put(apiName, audit);
					}
				}
			} else {
				audit.register(elapsed);
			}
			
			// Timeslot thing 
			// if timeslot has expired, flush lock and flush the contents and reset timeslot
			if (isTimeSlotExpired()) {
				synchronized(timeSlotLock) {
					// double check to avoid double action by racing threads
					if (isTimeSlotExpired()) {
						flush(callback);
						slot = new SimpleTimeSlot(DumpInterval, new Date());
					}
				}
			}
		}

		public APIAudit getSummaryAudit() {
			List<APIAudit> audits = new ArrayList<APIAudit> (auditMap.values());
			long totalTime = 0;
			int callCount = 0;
			long maxTimeMS = 0;

			for (APIAudit apiAudit : audits) {
				totalTime += apiAudit.totalTime;
				callCount += apiAudit.callCount;
				
				if (apiAudit.maxTimeMS > maxTimeMS)
					maxTimeMS = apiAudit.maxTimeMS;
			}
			
			String apiName = "All APIs" ;
			double averageTimeMS = 1.0 * totalTime/callCount;

			return new APIAudit(apiName, callCount, totalTime, averageTimeMS, maxTimeMS);

		}
		
		/**
		 * Gets the top ap is.
		 *
		 * @param category the category
		 * @param topCount the top count
		 * @return the top ap is
		 */
		public List<APIAudit> getTopAPIs(String category, int topCount) {
			List<APIAudit> audits = new ArrayList<APIAudit> (auditMap.values());
			
			// sort based on category through anonymous class
			if (TOP_COUNT.equals(category)) {
				Collections.sort(audits, new Comparator<APIAudit>() {
					  @Override
					  public int compare(APIAudit audit1, APIAudit audit2) {
						if (audit1.callCount == audit2.callCount)
						 return 0;
						
						return audit1.callCount < audit2.callCount ? 1 : -1;
					}
				});
			} else if (TOP_DURATION.equals(category)) {
				Collections.sort(audits, new Comparator<APIAudit>() {
					  @Override
					  public int compare(APIAudit audit1, APIAudit audit2) {
						if (audit1.totalTime == audit2.totalTime)
						 return 0;
						
						return audit1.totalTime < audit2.totalTime ? 1 : -1;
					}
				});			}
		
			// Return TOP few
			List<APIAudit> retval = new ArrayList<APIAudit>();
			topCount = audits.size() > topCount ? topCount : audits.size();
			
			for(int i = 0; i < topCount; i++) {
				retval.add(audits.get(i));
			}
			
			return retval;
		}

		public void flush() {
			flush(new TimeSlotCompletionSystemLogCallback());
		}
	}
	
//
//	/**
//	 * The Class TimeSlotCompletionLoggerCallback.
//	 */
//	static class TimeSlotCompletionLoggerCallback implements CompletionCallback<List<APIAudit>> {
//
//		/* (non-Javadoc)
//		 * @see com.cariq.www.utils.APIAuditor.TimeSlotCompletionCallback#onTimeSlotComplete(java.util.List)
//		 */
//		@Override
//		public void onComplete(List<APIAudit> topAudits) {
//			logger.debug("[APIAudit]============== TOP APIs of Last TimeSlot ===============");
//			for (APIAudit apiAudit : topAudits) {
//				logger.debug(apiAudit.toString());
//			}
//			logger.debug("[APIAudit]=============================\n");
//		}
//		
//	}
	
	/**
	 * The Class TimeSlotCompletionLoggerCallback.
	 */
	static class TimeSlotCompletionSystemLogCallback implements CompletionCallback<List<APIAudit>> {

		/* (non-Javadoc)
		 * @see com.cariq.www.utils.APIAuditor.TimeSlotCompletionCallback#onTimeSlotComplete(java.util.List)
		 */
		@Override
		public void onComplete(List<APIAudit> topAudits) {
			Server server = Server.getInstance();
			
			String host = server.getHost();
			Date startedOn = server.getStartedOn();

			// Store the Audits in the SystemLog Table
			logger.debug("[APIAudit]============== Dumping TOP APIs of Last TimeSlot into Database ===============");
			for (APIAudit apiAudit : topAudits) {
				logger.debug(host + API_AUDIT + " API Audit from last hour " +  Utils.getJSonString(apiAudit.toGenericJSON()) +  startedOn);
			}
			logger.debug("[APIAudit]=============================\n");
		}
		
	}
	
	/**
	 * Register.
	 *
	 * @param apiName the api name
	 * @param elapsed the elapsed
	 */
	public static void register(String apiName, long elapsed) {
//		APIHelper.logger.getLogger("APIAuditor").debug("API: " + apiName + " duration: " + elapsed);
		
		APIAuditRegistry registry = APIAuditRegistry.getInstance();
		registry.register(apiName, elapsed, new TimeSlotCompletionSystemLogCallback());
	}

	/** The Constant TOP_COUNT. */
	public static final String TOP_COUNT = "TopCount";
	
	/** The Constant TOP_DURATION. */
	public static final String TOP_DURATION = "TopDuration";
	
	/**
	 * Process.
	 *
	 * @param category the category
	 * @param topCount the top count
	 * @return the list
	 */
	public static List<APIAudit> process(String category, int topCount) {
		return APIAuditRegistry.getInstance().getTopAPIs(category, topCount);
	}
}
