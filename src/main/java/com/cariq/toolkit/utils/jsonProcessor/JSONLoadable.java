/**
 * 
 */
package com.cariq.toolkit.utils.jsonProcessor;

import com.cariq.toolkit.utils.profile.ProfilePoint;

/**
 * @author hrishi
 *
 */
public abstract class JSONLoadable implements JSONLoadableIfc {
	protected abstract void registerHandlers(JSONObjectBuilder builder);

	@Override
	public void register(JSONObjectBuilder builder) {
		try (ProfilePoint _JSONLoadable_register = ProfilePoint.profileAction("ProfAction_JSONLoadable_register")) {
			registerHandlers(builder);
		}
		
	}
}
